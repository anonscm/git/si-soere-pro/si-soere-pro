/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.produit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class ProduitsTest {
    public static final String CODE_USER_LONG =  "BIO-UMR ECOSYS-1998-78";
    public static final String CODE_USER_COURT =  "BIO";
    public static final String CODE_DETENTEUR =  "UMR ECOSYS";
    public static final int CODE_ANNEE =  1998;
    public static final String CODE_DEPARTEMENT =  "78";
    
    public ProduitsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createCodeProduit method, of class Produits.
     */
    @Test
    public void testCreateCodeComposant() {
        Assert.assertEquals(CODE_USER_LONG, Produits.createCodeComposant(CODE_USER_COURT, CODE_DETENTEUR, CODE_ANNEE, CODE_DEPARTEMENT));
       Assert.assertEquals(CODE_USER_LONG, Produits.createCodeComposant(CODE_USER_LONG, CODE_DETENTEUR, CODE_ANNEE, CODE_DEPARTEMENT));
    }
    @Test
    public void testCreateCodeProduit() {
       Assert.assertEquals(CODE_USER_COURT, Produits.createCodeProduits(CODE_USER_COURT, CODE_DETENTEUR, CODE_ANNEE, CODE_DEPARTEMENT));
       Assert.assertEquals(CODE_USER_COURT, Produits.createCodeProduits(CODE_USER_LONG, CODE_DETENTEUR, CODE_ANNEE, CODE_DEPARTEMENT));
    }
}
