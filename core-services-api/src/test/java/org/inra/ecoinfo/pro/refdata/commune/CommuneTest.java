/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.commune;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class CommuneTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    Commune instance;
    
    /**
     *
     */
    public CommuneTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        instance=new Commune();
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testCetNomCommune(){
        System.out.println("test CetNomCommune : standard");
        String nomCommune_codePostal = "Orgeval (78630)";
        String nomCommune = Commune.getNomCommune(nomCommune_codePostal);
        assertEquals("Orgeval", nomCommune);
        System.out.println("test CetNomCommune : standard with spaces");
        nomCommune_codePostal = "  Orgeval   (  78630  )  ";
        nomCommune = Commune.getNomCommune(nomCommune_codePostal);
        assertEquals("Orgeval", nomCommune);
        System.out.println("test CetNomCommune : no code");
        nomCommune_codePostal = "Orgeval";
        nomCommune = Commune.getNomCommune(nomCommune_codePostal);
        assertEquals("Orgeval", nomCommune);
        nomCommune_codePostal = "  Orgeval  ";
        nomCommune = Commune.getNomCommune(nomCommune_codePostal);
        assertEquals("Orgeval", nomCommune);
    }

    /**
     *
     */
    @Test
    public void testCetCodePostal(){
        System.out.println("test CetNomCommune : standard");
        String nomCommune_codePostal = "Orgeval (78630)";
        String nomCommune = Commune.getCodePostal(nomCommune_codePostal);
        assertEquals("78630", nomCommune);
        System.out.println("test CetNomCommune : standard with spaces");
        nomCommune_codePostal = "  Orgeval   (  78630  )  ";
        nomCommune = Commune.getCodePostal(nomCommune_codePostal);
        assertEquals("78630", nomCommune);
        System.out.println("test CetNomCommune : no code");
        nomCommune_codePostal = "Orgeval";
        nomCommune = Commune.getCodePostal(nomCommune_codePostal);
        assertNull(nomCommune);
        nomCommune_codePostal = "  Orgeval  ";
        nomCommune = Commune.getCodePostal(nomCommune_codePostal);
        assertNull(nomCommune);
    }
    
    /**
     *
     */
    public void testGetNomCommune_codePostal(){
        instance.setNomCommune("Orgeval");
        instance.setCodePostal("78630");
        assertEquals("Orgeval", instance.getNomCommune());
        assertEquals("78630", instance.getCodePostal());
        assertEquals("Orgeval (78630)", instance.getNomCommune_codePostal());
    }
}
