/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementplante;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class PrelevementPlanteTest {
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public PrelevementPlanteTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of creationCodePrelevementFinal method, of class Recorder.
     */
    @Test
    public void testCreationCodePrelevementFinal() {
//27_07_2000_QA_DVB avec N_Bloc 1_104_monitoring_Grain_Ble tendre hiver
        //27_07_2000_QA (Feucherolles)_DVB avec N_Bloc 1_104_monitoring_Grain_Ble tendre hiver
        //DD_MM_YYYY_codeDispositif (lieu)_codeTraitement_codeBloc_nomParcelleElementaire_codePlacette_codePartiePrelevee_codeCulture
                String date = "20/07/2014";
        String codeDispositif = "codeDispositif";
        String codeLieu = "lieu";
        String codetrait = "codeTraitement";
        String bloc = "codeBloc";
        String pelementaire = "nomParcelleElementaire";
        String placette = "codePlacette";
        String culture = "codeCulture";
        String partie = "codePartiePrelevee";
        String expResult = "20_07_2014_codedispositif_lieu_codetraitement_codebloc_nomparcelleelementaire_codeplacette_codeculture_codepartieprelevee";
        String result = PrelevementPlante.buildCodePlante(date, codeDispositif, codeLieu, codetrait, bloc, pelementaire, placette, culture, partie);
        assertEquals(expResult, result);

        //placette null
        placette = "sans";
        expResult = "20_07_2014_codedispositif_lieu_codetraitement_codebloc_nomparcelleelementaire_codeculture_codepartieprelevee";
        result = PrelevementPlante.buildCodePlante(date, codeDispositif, codeLieu, codetrait, bloc, pelementaire, placette, culture, partie);
        assertEquals(expResult, result);

        //placette null && parcelle null
        pelementaire = "sans";
        expResult = "20_07_2014_codedispositif_lieu_codetraitement_codebloc_codeculture_codepartieprelevee";
        result = PrelevementPlante.buildCodePlante(date, codeDispositif, codeLieu, codetrait, bloc, pelementaire, placette, culture, partie);
        assertEquals(expResult, result);

        //placette null && parcelle null && bloc null
        bloc = "sans";
        expResult = "20_07_2014_codedispositif_lieu_codetraitement_codeculture_codepartieprelevee";
        result = PrelevementPlante.buildCodePlante(date, codeDispositif, codeLieu, codetrait, bloc, pelementaire, placette, culture, partie);
        assertEquals(expResult, result);

    }
    
}
