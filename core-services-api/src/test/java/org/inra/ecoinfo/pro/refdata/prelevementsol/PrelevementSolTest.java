/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementsol;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class PrelevementSolTest {
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public PrelevementSolTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testCreationCodePrelevementFinal() {
//27_07_2000_QA_DVB avec N_Bloc 1_104_monitoring_Grain_Ble tendre hiver
        //27_07_2000_QA (Feucherolles)_DVB avec N_Bloc 1_104_monitoring_Grain_Ble tendre hiver
        //DD_MM_YYYY_codeDispositif (lieu)_codeTraitement_codeBloc_nomParcelleElementaire_codePlacette_codePartiePrelevee_codeCulture
                String date = "20/07/2014";
        String codeDispositif = "codeDispositif";
        String nomlieu = "nomlieu";
        String codetrait = "codeTraitement";
        String bloc = "codeBloc";
        String pelementaire = "nomParcelleElementaire";
        String placette = "codePlacette";
        int ls=15;
        int li=11;
        String expResult = "20_07_2014_codedispositif_nomlieu_codetraitement_codebloc_nomparcelleelementaire_codeplacette_15_11";
        String result = PrelevementSol.buildCodePrelevement(date, codeDispositif, nomlieu, codetrait, bloc, pelementaire, placette, ls, li);
        assertEquals(expResult, result);

        //placette null
        placette = "sans";
        expResult = "20_07_2014_codedispositif_nomlieu_codetraitement_codebloc_nomparcelleelementaire_15_11";
        result = PrelevementSol.buildCodePrelevement(date, codeDispositif, nomlieu, codetrait, bloc, pelementaire, placette, ls, li);
        assertEquals(expResult, result);

        //placette null && parcelle null
        pelementaire = "sans";
        expResult = "20_07_2014_codedispositif_nomlieu_codetraitement_codebloc_15_11";
        result = PrelevementSol.buildCodePrelevement(date, codeDispositif, nomlieu, codetrait, bloc, pelementaire, placette, ls, li);
        assertEquals(expResult, result);

        //placette null && parcelle null && bloc null
        bloc = "sans";
        expResult = "20_07_2014_codedispositif_nomlieu_codetraitement_15_11";
        result = PrelevementSol.buildCodePrelevement(date, codeDispositif, nomlieu, codetrait, bloc, pelementaire, placette, ls, li);
        assertEquals(expResult, result);

    }
}
