/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.environnement;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class EnvironnementTest {
    private static final String CODE_ENVIRONNEMENT = "nord-ouest_45.3_245.21";

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
            final double distanceAxeRoutier = 45.3;
            final double distanceAgglomeration = 245.21;
            final String directionVentDominant = "nord-ouest";
    
    /**
     *
     */
    public EnvironnementTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }
    
    /**
     *
     */
    @Test
    public void testBuilCode(){
        String result = Environnement.buildCodeEnvironnement(
                directionVentDominant,
                distanceAxeRoutier,
                distanceAgglomeration);
        assertEquals(CODE_ENVIRONNEMENT, result);
        result = Environnement.buildCodeEnvironnement(
                directionVentDominant,
                Double.toString(distanceAxeRoutier),
                Double.toString(distanceAgglomeration));
        assertEquals(CODE_ENVIRONNEMENT, result);
    }
    
    /**
     *
     */
    @Test
    public void testisCode(){
        List<String> parseCode = Stream.of(CODE_ENVIRONNEMENT.split("_")).collect(Collectors.toList());
        String directionDuvent=parseCode.get(0);
        Double axe = axe = Double.parseDouble(parseCode.get(1));
        Double agglo = Double.parseDouble(parseCode.get(2));
        assertEquals(CODE_ENVIRONNEMENT, Environnement.buildCodeEnvironnement(directionDuvent, axe, agglo));
    }
    
}
