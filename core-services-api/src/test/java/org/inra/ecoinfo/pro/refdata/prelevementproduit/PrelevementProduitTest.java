/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementproduit;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author ptcherniati
 */
public class PrelevementProduitTest {
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public PrelevementProduitTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of buildCodePrelevementProduit method, of class PrelevementProduit.
     */
    @Test
    public void testBuildCodePrelevementProduit() {
        System.out.println("buildCodePrelevementProduit");
        String date = "20/07/2014";
        int numero = 24;
        String codeDispositif = "codeDispositif";
        String codeLieu = "codeLieu";
        String codeProduit = "produit";
        String codetrait = "codetrait";
        String bloc = "bloc";
        String pelementaire = "pelementaire";
        String placette = "placette";
        String expResult = "20_07_2014_produit_codedispositif_codelieu_codetrait_bloc_pelementaire_placette_24";
        String result = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, codeLieu, codeProduit, codetrait, bloc, pelementaire, placette);
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);

        //placette null
        bloc = "bloc";
        pelementaire = "pelementaire";
        placette = "sans";
        expResult = "20_07_2014_produit_codedispositif_codelieu_codetrait_bloc_pelementaire_24";
        result = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, codeLieu, codeProduit, codetrait, bloc, pelementaire, placette);
        System.out.println(expResult);
        System.out.println(result);
        assertEquals(expResult, result);

        //placette null && parcelle null
        bloc = "bloc";
        pelementaire = "sans";
        placette = "sans";
        expResult = "20_07_2014_produit_codedispositif_codelieu_codetrait_bloc_24";
        result = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, codeLieu, codeProduit, codetrait, bloc, pelementaire, placette);
        System.out.println(expResult);
        assertEquals(expResult, result);

        //placette null && parcelle null && bloc null
        bloc = "sans";
        pelementaire = "sans";
        placette = "sans";
        expResult = "20_07_2014_produit_codedispositif_codelieu_codetrait_24";
        result = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, codeLieu, codeProduit, codetrait, bloc, pelementaire, placette);
        assertEquals(expResult, result);

        //bloc null && placette null
        bloc = "sans";
        pelementaire = "pelementaire";
        placette = "sans";
        pelementaire = "pelementaire";
        expResult = "20_07_2014_produit_codedispositif_codelieu_codetrait_pelementaire_24";
        result = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, codeLieu, codeProduit, codetrait, bloc, pelementaire, placette);
        assertEquals(expResult, result);

        //bloc null
        bloc = "sans";
        pelementaire = "pelementaire";
        placette = "placette";
        expResult = "20_07_2014_produit_codedispositif_codelieu_codetrait_pelementaire_placette_24";
        result = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, codeLieu, codeProduit, codetrait, bloc, pelementaire, placette);
        assertEquals(expResult, result);
    }
    
}
