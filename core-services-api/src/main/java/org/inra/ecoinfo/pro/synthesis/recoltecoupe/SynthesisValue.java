/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.recoltecoupe;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;

/**
 *
 * @author adiankha
 */
@Entity(name = "RecoltecoupeSynthesisValue")
@Table(indexes = {
    @Index(name = "RecoltecoupeSynthesisValue_idNode_idx", columnList = "idNode"),
    @Index(name = "RecoltecoupeSynthesisValue_disp_idx", columnList = "site"),
    @Index(name = "RecoltecoupeSynthesisValue_disp_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends AbstractProSynthesisValue{

    /**
     *
     */
    public SynthesisValue() {
        
    }
    
    

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueString
   
     
     */
    public SynthesisValue(LocalDate date, String site, String variable,String valueString, Long idNode) {
        super(date.atStartOfDay(), site, variable, null, valueString, idNode);
    }
    
}
