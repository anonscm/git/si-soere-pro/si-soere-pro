package org.inra.ecoinfo.pro.refdata.horizonsystematique;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.lesprofils.LesProfils;

/**
 * @author Vivianne
 * 
 */


@Entity
@Table(name = HorizonSystematique.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = LesProfils.JPA_ID)
public class HorizonSystematique extends LesProfils implements Serializable{
	
    private static final long serialVersionUID = 1L; 
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "horizon_systematique";
   // public static final String JPA_COLUMN_ID = "code_interne";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "horizon_systematique_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NOM = "horizon_systematique_nom";

    
    /*@Id
    @Column(name = HorizonSystematique.JPA_COLUMN_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long code_interne;*/
    @Column(name = HorizonSystematique.JPA_COLUMN_CODE, unique = true, nullable = false, length = 150)
    private String horizon_systematique_code;
    @Column(name = HorizonSystematique.JPA_COLUMN_NOM, unique = true, nullable = false, length = 150)
    private String horizon_systematique_nom;
    
    /**
     *
     */
    public HorizonSystematique() {
		super();
	
	}

    /**
     *
     * @param horizon_systematique_code
     * @param horizon_systematique_nom
     */
    public HorizonSystematique(String horizon_systematique_code, String horizon_systematique_nom) {
		
		super(horizon_systematique_nom);
		this.horizon_systematique_code = horizon_systematique_code;
		this.horizon_systematique_nom = horizon_systematique_nom;
	}
	
	/*public long getCode_interne() {
		return code_interne;
	}

	public void setCode_interne(long code_interne) {
		this.code_interne = code_interne;
	}*/

    /**
     *
     * @return
     */


	public String getHorizon_systematique_code() {
		return horizon_systematique_code;
	}

    /**
     *
     * @param horizon_systematique_code
     */
    public void setHorizon_systematique_code(String horizon_systematique_code) {
		this.horizon_systematique_code = horizon_systematique_code;
	}

    /**
     *
     * @return
     */
    public String getHorizon_systematique_nom() {
		return horizon_systematique_nom;
	}

    /**
     *
     * @param horizon_systematique_nom
     */
    public void setHorizon_systematique_nom(String horizon_systematique_nom) {
		this.horizon_systematique_nom = horizon_systematique_nom;
	}  

}