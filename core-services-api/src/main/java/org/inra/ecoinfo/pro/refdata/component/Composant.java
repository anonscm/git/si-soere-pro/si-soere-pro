package org.inra.ecoinfo.pro.refdata.component;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 * @param <T>
 */
@Entity
@Table(name = Composant.NAME_ENTITY_JPA)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Composant<T extends Composant> implements Serializable, Comparable<T> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "composant";

    /**
     *
     */
    public static final String JPA_ID = "component_id";

    /**
     *
     */
    public static final String JPA_CODCOMPOSANT = "codecomposant";

    /**
     *
     */
    public static final String JPA_IS_PRO = "ispro";

    /**
     *
     */
    public static final String BOOLEAN_OUI = "oui";

    /**
     *
     */
    public static final String BOOLEAN_NON = "non";

    @Id
    @Column(name = Composant.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long component_id;

    /**
     *
     */
    @Column(name = Composant.JPA_CODCOMPOSANT, nullable = true, unique = true)
    protected String codecomposant = "";

    /**
     *
     */
    @Column(name = Composant.JPA_IS_PRO)
    protected boolean ispro;

    /**
     *
     */
    public Composant() {
        super();
    }

    /**
     *
     * @param code
     * @param ispro
     */
    public Composant(String code, boolean ispro) {
        this.codecomposant = code;
        this.ispro = ispro;
    }

    /**
     *
     * @return
     */
    public String getCodecomposant() {
        return codecomposant;
    }

    /**
     *
     * @return
     */
    public long getComponent_id() {
        return component_id;
    }

    /**
     *
     * @param codecomposant
     */
    public void setCodecomposant(String codecomposant) {
        this.codecomposant = codecomposant;
    }

    /**
     *
     * @param component_id
     */
    public void setComponent_id(long component_id) {
        this.component_id = component_id;
    }

    /**
     *
     * @return
     */
    public boolean getIspro() {
        return ispro;
    }

    /**
     *
     * @param ispro
     */
    public void setIspro(boolean ispro) {
        this.ispro = ispro;
    }

    /**
     *
     * @return 
     */
    public String getIsProString() {
        return this.ispro ? "oui" : "non";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.codecomposant);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Composant other = (Composant) obj;
        return Objects.equals(this.codecomposant, other.codecomposant);
    }

    @Override
    public int compareTo(Composant o) {
        return getCodecomposant().compareTo(o.codecomposant);
    }

}
