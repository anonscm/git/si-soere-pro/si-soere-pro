/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.famille;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=Famille.NAME_ENTITY_JPA)

public class Famille implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "famille";

    /**
     *
     */
    public static final String JPA_ID = "famille_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "famille_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "famille_code";
    
    @Id
    @Column(name = Famille.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long famille_id;
    
    @Column(name = Famille.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String famille_nom;
    
     @Column(name = Famille.JPA_COLUMN_KEY, unique = true, nullable = false, length = 150)
    private String famille_code;
   
    /**
     *
     * @param famille_id
     * @param famille_nom
     * @param categorieITK
     */
    public Famille(long famille_id,String famille_nom) {
        this.famille_id = famille_id;
        this.famille_nom = famille_nom;
        this.famille_code = Utils.createCodeFromString(famille_nom);
    }

    /**
     *
     * @param famille_nom
     * @param categorieITK
     */
    public Famille(String famille_nom) {
        this.famille_nom = famille_nom;
    }
  
    /**
     *
     */
    public Famille() {
        super();
    }

    /**
     *
     * @return
     */
    public long getFamille_id() {
        return famille_id;
    }

    /**
     *
     * @param famille_id
     */
    public void setFamille_id(long famille_id) {
        this.famille_id = famille_id;
    }

    /**
     *
     * @return
     */
    public String getFamille_nom() {
        return famille_nom;
    }

    /**
     *
     * @param famille_nom
     */
    public void setFamille_nom(String famille_nom) {
        this.famille_nom = famille_nom;
    }

    /**
     *
     * @return
     */
    public String getFamille_code() {
        return famille_code;
    }

    /**
     *
     * @param famille_code
     */
    public void setFamille_code(String famille_code) {
        this.famille_code = famille_code;
    }
    
    
}
