package org.inra.ecoinfo.pro.refdata.raisonnementprotocole;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.raisonnement.Raisonnement;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = RaisonnementProtocole.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.RAISONNEMENT_ID, RefDataConstantes.PROTOCOLE_ID}))
public class RaisonnementProtocole implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.RAISONNEMENTPROTOCOLE_ID; // raisprt_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.RAISONNEMENTPROTOCOLE_TABLE_NAME; // raisonnement_protocole
    @Id
    @Column(name = RaisonnementProtocole.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Raisonnement.class)
    @JoinColumn(name = RefDataConstantes.RAISONNEMENT_ID, referencedColumnName = Raisonnement.ID_JPA, nullable = false)
    private Raisonnement raisonnement;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Protocole.class)
    @JoinColumn(name = RefDataConstantes.PROTOCOLE_ID, referencedColumnName = Protocole.ID_JPA, nullable = false)
    private Protocole protocole;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMMENT_RAISPRT, length = 100)
    private String commentaire;

    /**
     *
     */
    public RaisonnementProtocole() {
        super();
    }

    /**
     *
     * @param raisonnement
     * @param protocole
     * @param commentaire
     */
    public RaisonnementProtocole(Raisonnement raisonnement, Protocole protocole, String commentaire) {
        super();
        this.raisonnement = raisonnement;
        this.protocole = protocole;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Protocole getProtocole() {
        return protocole;
    }

    /**
     *
     * @return
     */
    public Raisonnement getRaisonnement() {
        return raisonnement;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param protocole
     */
    public void setProtocole(Protocole protocole) {
        this.protocole = protocole;
    }

    /**
     *
     * @param raisonnement
     */
    public void setRaisonnement(Raisonnement raisonnement) {
        this.raisonnement = raisonnement;
    }

}
