/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.observationqualitative;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.typeobservationqualitative.TypeObservationQualitative;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=ObservationQalitative.NAME_ENTITY_JPA,uniqueConstraints = 
        @UniqueConstraint(columnNames = {  TypeObservationQualitative.JPA_ID,
                                                                                                ObservationQalitative.JPA_COLUMN_NAME
}))
public class ObservationQalitative implements Serializable {
 
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "observation_qua";

    /**
     *
     */
    public static final String JPA_ID = "observation_qua_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "observation_qua_nom"; 
    
    @Id
    @Column(name = ObservationQalitative.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long observation_qua_id;
    
    @Column(name = ObservationQalitative.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String observation_qua_nom;
    
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeObservationQualitative.JPA_ID,  nullable = false)
    private TypeObservationQualitative  typeObservationQualitative;

    /**
     *
     * @param observation_qua_nom
     * @param typeObservationQualitative
     */
    public ObservationQalitative( String observation_qua_nom, TypeObservationQualitative typeObservationQualitative) {
        
        this.observation_qua_nom = observation_qua_nom;
        this.typeObservationQualitative = typeObservationQualitative;
    }

    /**
     *
     */
    public ObservationQalitative() {
        super();
    }

    /**
     *
     * @return
     */
    public long getObservation_qua_id() {
        return observation_qua_id;
    }

    /**
     *
     * @param observation_qua_id
     */
    public void setObservation_qua_id(long observation_qua_id) {
        this.observation_qua_id = observation_qua_id;
    }

    /**
     *
     * @return
     */
    public String getObservation_qua_nom() {
        return observation_qua_nom;
    }

    /**
     *
     * @param observation_qua_nom
     */
    public void setObservation_qua_nom(String observation_qua_nom) {
        this.observation_qua_nom = observation_qua_nom;
    }

    /**
     *
     * @return
     */
    public TypeObservationQualitative getTypeObservationQualitative() {
        return typeObservationQualitative;
    }

    /**
     *
     * @param typeObservationQualitative
     */
    public void setTypeObservationQualitative(TypeObservationQualitative typeObservationQualitative) {
        this.typeObservationQualitative = typeObservationQualitative;
    }
    
    
    
}
