
package org.inra.ecoinfo.pro.refdata.pays;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.region.Region;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Pays.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_PAYS}))
public class Pays implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PAYS_ID; // pa_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PAYS_TABLE_NAME; // pays
    
    @Id
    @Column(name = Pays.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_PAYS, length = 50)
    private String nom;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_PAYS, length = 50)
    private String code;
    
    @OneToMany(mappedBy = RefDataConstantes.NAME_ATTRIBUT_PAYS, cascade = {PERSIST, MERGE, REFRESH, REMOVE})
    private List<Region> lstRegion = new LinkedList<Region>();
    
    /**
     *
     */
    public Pays() 
    {
        super();
    }
    
    /**
     *
     * @param nom
     */
    public Pays(String nom) 
    {
        super();
        this.nom = nom;
    }
    
    /**
	 * @param nom
	 * @param code
	 */
	public Pays(String nom, String code) {
		super();
		this.nom = nom;
		this.code = code;
	}

    /**
     *
     * @param region
     */
    public void addRegion(Region region) 
    {
        this.lstRegion.add(region);
        region.setPays(this);
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public List<Region> getLstRegion() {
        return lstRegion;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param lstRegion
     */
    public void setLstRegion(List<Region> lstRegion) {
        this.lstRegion = lstRegion;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}
