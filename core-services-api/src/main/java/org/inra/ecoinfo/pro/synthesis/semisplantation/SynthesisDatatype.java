/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.semisplantation;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author adiankha
 */
@Entity(name = "SemisplantationSynthesisDatatype")
@Table(indexes = {
    @Index(name = "SemisplantationSynthesisdatatype_disp_idx",columnList = "site")})
public class SynthesisDatatype  extends GenericSynthesisDatatype{

    /**
     *
     */
    public SynthesisDatatype() {
        super();
    }

    /**
     *
     * @param minDate
     * @param maxDate
     * @param site
     * @param idNode
     */
    public SynthesisDatatype(String site, LocalDateTime minDate, LocalDateTime maxDate, String idNodes) {
        super(minDate, maxDate, site, idNodes);
    }
}