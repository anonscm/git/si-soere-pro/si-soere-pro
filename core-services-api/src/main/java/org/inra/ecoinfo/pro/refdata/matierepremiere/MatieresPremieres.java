
package org.inra.ecoinfo.pro.refdata.matierepremiere;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MatieresPremieres.JPA_NAME_ENTITY)
@PrimaryKeyJoinColumn(name = Composant.JPA_ID)
public class MatieresPremieres extends Composant {

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "matierepremiere";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "matierep_code";

    /**
     *
     */
    public static final String JPA_COLUMN_MYNAME= "matierep_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT="commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_NO ="non";
    @Size(min = 1, max = 255)
    @Column(name = MatieresPremieres.JPA_COLUMN_NAME, unique = true, nullable = false, length = 255)
    private String matierep_code = "";
    @Size(min = 1, max = 255)
    @Column(name = MatieresPremieres.JPA_COLUMN_MYNAME, unique = true, nullable = false, length = 255)
    private String matierep_nom = "";
    @Column(name=MatieresPremieres.JPA_COLUMN_COMMENT)
    private String commentaire=null;

    /**
     *
     */
    public MatieresPremieres() {
        super();
    }

    /**
     *
     * @param nom
     * @param code
     * @param commentaire
     */
    public MatieresPremieres(String nom, String code,String commentaire) {
        super(nom,Boolean.FALSE);
        this.matierep_code = Utils.createCodeFromString(nom);
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getMatierep_code() {
        return matierep_code;
    }

    /**
     *
     * @param matierep_code
     */
    public void setMatierep_code(String matierep_code) {
        this.matierep_code = matierep_code;
    }

    /**
     *
     * @return
     */
    public String getMatierep_nom() {
        return matierep_nom;
    }

    /**
     *
     * @param matierep_nom
     */
    public void setMatierep_nom(String matierep_nom) {
        this.matierep_nom = matierep_nom;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
}
