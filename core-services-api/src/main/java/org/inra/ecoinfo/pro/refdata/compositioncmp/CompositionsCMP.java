package org.inra.ecoinfo.pro.refdata.compositioncmp;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.ValeurCaracteristiqueMP;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 *   Melange.JPA_ID, 
 *   ValeurCaracteristiqueMP.ID_JPA})
 * @author ptcherniati
 */
@Entity
@Table(name = CompositionsCMP.NAME_ENTITY_JPA, 
        uniqueConstraints = @UniqueConstraint(columnNames = {
    Melange.JPA_ID, 
    ValeurCaracteristiqueMP.ID_JPA})
)
public class CompositionsCMP implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "compositioncmpremiere";

    /**
     *
     */
    public static final String ID_JPA = "ccmp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = CompositionsCMP.ID_JPA, updatable = false)
    private long ccmp_id = 0;
     @Column(name=CompositionsCMP.JPA_COLUMN_COMENT,length = 300)
    private String commentaire ="";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Melange.JPA_ID, nullable = false)
    private Melange melange;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = ValeurCaracteristiqueMP.ID_JPA, nullable = false)
    private ValeurCaracteristiqueMP valeurcaracteristiquemp;
    
    /**
     *
     */
    public CompositionsCMP() {
        super();
    }

    /**
     *
     * @param melange
     * @param vcmp
     * @param commentaire
     */
    public CompositionsCMP(Melange melange, ValeurCaracteristiqueMP vcmp,String commentaire) {
        this.valeurcaracteristiquemp = vcmp;
        this.melange = melange;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getCcmp_id() {
        return ccmp_id;
    }

    /**
     *
     * @return
     */
    public Melange getMelange() {
        return melange;
    }

    /**
     *
     * @return
     */
    public ValeurCaracteristiqueMP getValeurcaracteristiquemp() {
        return valeurcaracteristiquemp;
    }

    /**
     *
     * @param ccmp_id
     */
    public void setCcmp_id(long ccmp_id) {
        this.ccmp_id = ccmp_id;
    }

    /**
     *
     * @param melange
     */
    public void setMelange(Melange melange) {
        this.melange = melange;
    }

    /**
     *
     * @param valeurcaracteristiquemp
     */
    public void setValeurcaracteristiquemp(ValeurCaracteristiqueMP valeurcaracteristiquemp) {
        this.valeurcaracteristiquemp = valeurcaracteristiquemp;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
