package org.inra.ecoinfo.pro.refdata.dispositifobservatoire;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.DISPOSITIF_ID, RefDataConstantes.OBSERVATOIRE_ID}
 * @author ptcherniati
 */
@Entity
@Table(name = DispositifObservatoire.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.DISPOSITIF_ID, RefDataConstantes.OBSERVATOIRE_ID}))
public class DispositifObservatoire implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.DISPOSITIFOBSERV_ID; // dispobs_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.DISPOSITIFOBSERV_TABLE_NAME; // dispositif_observatoire
    @Id
    @Column(name = DispositifObservatoire.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Dispositif.class)
    @JoinColumn(name =  Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Observatoire.class)
    @JoinColumn(name = RefDataConstantes.OBSERVATOIRE_ID, referencedColumnName = Observatoire.ID_JPA, nullable = false)
    private Observatoire observatoire;

    /**
     *
     */
    public DispositifObservatoire() {
        super();
    }

    /**
     *
     * @param dispositif
     * @param observatoire
     */
    public DispositifObservatoire(Dispositif dispositif, Observatoire observatoire) {
        super();
        this.dispositif = dispositif;
        this.observatoire = observatoire;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Observatoire getObservatoire() {
        return observatoire;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param observatoire
     */
    public void setObservatoire(Observatoire observatoire) {
        this.observatoire = observatoire;
    }

}
