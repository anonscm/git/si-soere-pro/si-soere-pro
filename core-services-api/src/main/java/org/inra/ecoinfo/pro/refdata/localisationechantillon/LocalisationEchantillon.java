/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationechantillon;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=LocalisationEchantillon.NAME_ENTITY_JPA)
public class LocalisationEchantillon implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "localisationechantillon";

    /**
     *
     */
    public static final String ID_JPA = "le_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_LOCALISATION="code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME_LOCALISATION="nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "comentaire";
      
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = LocalisationEchantillon.ID_JPA)
     private long es_id = 0;
     @Column(name=LocalisationEchantillon.JPA_COLUMN_CODE_LOCALISATION,unique=true,nullable = false)
     private String code="";
     @Column(name=LocalisationEchantillon.JPA_COLUMN_NAME_LOCALISATION, unique = true,nullable = false)
     private String nom="";
     @Column(name=LocalisationEchantillon.JPA_COLUMN_COMMENT,nullable = true,unique = false)
     private String commentaire ="";
     
    /**
     *
     */
    public LocalisationEchantillon(){
         super();
     }
     
    /**
     *
     * @param code
     * @param nom
     * @param commentaire
     */
    public LocalisationEchantillon(String code, String nom,String commentaire){
         this.code = code;
         this.nom = nom;
         this.commentaire = commentaire;
     }

    /**
     *
     * @return
     */
    public long getEs_id() {
        return es_id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param es_id
     */
    public void setEs_id(long es_id) {
        this.es_id = es_id;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
     
     
    
}
