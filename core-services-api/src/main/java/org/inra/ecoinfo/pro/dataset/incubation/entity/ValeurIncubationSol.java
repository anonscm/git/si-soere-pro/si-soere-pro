/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurIncubationSol.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurIncubationSol.ATTRIBUTE_JPA_STATUT_VALEUR,
    RealNode.ID_JPA,
    MesureIncubationSol.ID_JPA
}
),
        indexes = {
            @Index(name = "valeur_incub_mesure_idx", columnList = MesureIncubationSol.ID_JPA),
            @Index(name = "valeur_incub_variable", columnList = RealNode.ID_JPA)
        }
)
@PrimaryKeyJoinColumn(name = ValeurIncubation.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurIncubationSol.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurIncubation.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurIncubationSol.ID_JPA))})
public class ValeurIncubationSol extends ValeurIncubation<MesureIncubationSol, ValeurIncubationSol> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurincubationsol";

    /**
     *
     */
    public static final String ID_JPA = "vis_id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";

    @Column(name = ValeurIncubationSol.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureIncubationSol.class)
    @JoinColumn(name = MesureIncubationSol.ID_JPA, referencedColumnName = MesureIncubationSol.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureIncubationSol mesureIncubationSol;

    /**
     *
     */
    public ValeurIncubationSol() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param mesureIncubationSol
     * @param realNode
     */
    public ValeurIncubationSol(Float valeur, String statutvaleur, MesureIncubationSol mesureIncubationSol, RealNode realNode) {
        super(valeur, realNode);
        this.statutvaleur = statutvaleur;
        this.mesureIncubationSol = mesureIncubationSol;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public MesureIncubationSol getMesureIncubationSol() {
        return mesureIncubationSol;
    }

    /**
     *
     * @param mesureIncubationSol
     */
    public void setMesureIncubationSol(MesureIncubationSol mesureIncubationSol) {
        this.mesureIncubationSol = mesureIncubationSol;
    }
}
