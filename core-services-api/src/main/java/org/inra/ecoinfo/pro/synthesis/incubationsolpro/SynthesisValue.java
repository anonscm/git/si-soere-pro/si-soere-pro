/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.incubationsolpro;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;

/**
 *
 * @author vjkoyao
 */
@Entity(name = "IncubationsolproSynthesisValue")
@Table(indexes = {
    @Index(name = "IncubationsolproSynthesisValue_disp_idx", columnList = "site"),
    @Index(name = "IncubationsolproSynthesisValue_idNode_idx", columnList = "idNode"),
    @Index(name = "IncubationsolproSynthesisValue_disp_variable_idx", columnList = "site,variable")})
public class SynthesisValue  extends AbstractProSynthesisValue {

    
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public SynthesisValue() {
        super();
    }

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valuefloat
     * @param idNode
     */
     public SynthesisValue(LocalDate date, String site, String variable, Double valuefloat, Long idNode) {
        super(date.atStartOfDay(), site, variable, valuefloat, null, idNode);
    }
}