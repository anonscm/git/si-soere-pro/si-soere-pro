/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureTravailDuSol.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    MesureTravailDuSol.ATTRIBUTE_JPA_DISPOSITIF,
    DescriptionTraitement.ID_JPA,
    MesureTravailDuSol.ATTRIBUTE_JPA_PARCELLE,
    MesureTravailDuSol.ATTRIBUTE_JPA_PLACETTE,
    MesureTravailDuSol.ATTRIBUTE_JPA_LocalDate_DEBUT,
    MesureTravailDuSol.ATTRIBUTE_INTERVENTION,
    MesureTravailDuSol.ATTRIBUTE_JPA_CULTURE}))

public class MesureTravailDuSol extends MesureITK<MesureApport, ValeurApport> {

    /**
     *
     */
    public static final String TABLE_NAME = "mesureinterventiontravailsol";

    /**
     *
     */
    public static final String ATTRIBUTE_INTERVENTION = "nomintervention";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIELTRAVAIL1 = "materieltravailsol1";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIELTRAVAIL2 = "materieltravailsol2";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIELTRAVAIL3 = "materieltravailsol3";
    /**
     *
     */
    public static final String ATTRIBUTE_NIVEAU = "niveauatteint";

    /**
     *
     */
    public static final String ATTRIBUTE_CODEBBCH = "codebbch";

    /**
     *
     */
    public static final String ATTRIBUTE_PRECISION_STADE = "stadeprecision";

    @Column(name = MesureTravailDuSol.ATTRIBUTE_INTERVENTION, nullable = false)
    private String nomintervention = "";

    @Column(name = MesureTravailDuSol.ATTRIBUTE_MATERIELTRAVAIL1, nullable = false)
    private String materieltravailsol1 = "";

    @Column(name = MesureTravailDuSol.ATTRIBUTE_MATERIELTRAVAIL2, nullable = true)
    private String materieltravailsol2 = "";

    @Column(name = MesureTravailDuSol.ATTRIBUTE_MATERIELTRAVAIL3, nullable = true)
    private String materieltravailsol3;

    @Column(name = MesureTravailDuSol.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint;
    @Column(name = MesureTravailDuSol.ATTRIBUTE_CODEBBCH, nullable = true)
    private String codebbch;

    @Column(name = MesureTravailDuSol.ATTRIBUTE_PRECISION_STADE, nullable = true)
    private String precisionstade = "";

    @OneToMany(mappedBy = "mesuretravaildusol", cascade = ALL)
    List<ValeurTravailDuSol> valeurtravailsol = new LinkedList();

    /**
     *
     */
    public MesureTravailDuSol() {
        super();
    }

    /**
     *
     * @param codedispositif
     * @param traitement
     * @param nomparcelle
     * @param nomplacette
     * @param LocalDatedebut
     * @param LocalDatefin
     * @param nomintervension
     * @param materieltravailsol1
     * @param materieltravailsol2
     * @param materieltravailsol3
     * @param niveauatteint
     * @param nomculture
     * @param codebbch
     * @param stade
     * @param typeobservation
     * @param nomobservation
     * @param versionfile
     * @param commentaire
     */
    public MesureTravailDuSol(String codedispositif, DescriptionTraitement traitement, String nomparcelle, String nomplacette,
            LocalDate LocalDatedebut, LocalDate LocalDatefin, String nomintervension, String materieltravailsol1, String materieltravailsol2,
            String materieltravailsol3, String niveauatteint, String nomculture, String codebbch, String stade,
            String typeobservation, String nomobservation, VersionFile versionfile, String commentaire) {
        super(nomculture, codedispositif, commentaire, nomparcelle, nomplacette, LocalDatedebut, LocalDatefin, typeobservation, nomobservation, versionfile, traitement);

        this.nomintervention = nomintervension;
        this.materieltravailsol1 = materieltravailsol1;
        this.materieltravailsol2 = materieltravailsol2;
        this.materieltravailsol3 = materieltravailsol3;
        this.niveauatteint = niveauatteint;
        this.codebbch = codebbch;
        this.precisionstade = stade;
    }

    /**
     *
     * @return
     */
    public String getNomintervention() {
        return nomintervention;
    }

    /**
     *
     * @param nomintervention
     */
    public void setNomintervention(String nomintervention) {
        this.nomintervention = nomintervention;
    }

    /**
     *
     * @return
     */
    public String getMaterieltravailsol1() {
        return materieltravailsol1;
    }

    /**
     *
     * @param materieltravailsol1
     */
    public void setMaterieltravailsol1(String materieltravailsol1) {
        this.materieltravailsol1 = materieltravailsol1;
    }

    /**
     *
     * @return
     */
    public String getMaterieltravailsol2() {
        return materieltravailsol2;
    }

    /**
     *
     * @param materieltravailsol2
     */
    public void setMaterieltravailsol2(String materieltravailsol2) {
        this.materieltravailsol2 = materieltravailsol2;
    }

    /**
     *
     * @return
     */
    public String getMaterieltravailsol3() {
        return materieltravailsol3;
    }

    /**
     *
     * @param materieltravailsol3
     */
    public void setMaterieltravailsol3(String materieltravailsol3) {
        this.materieltravailsol3 = materieltravailsol3;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteint() {
        return niveauatteint;
    }

    /**
     *
     * @param niveauatteint
     */
    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getPrecisionstade() {
        return precisionstade;
    }

    /**
     *
     * @param precisionstade
     */
    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    /**
     *
     * @return
     */
    public List<ValeurTravailDuSol> getValeurtravailsol() {
        return valeurtravailsol;
    }

    /**
     *
     * @param valeurtravailsol
     */
    public void setValeurtravailsol(List<ValeurTravailDuSol> valeurtravailsol) {
        this.valeurtravailsol = valeurtravailsol;
    }
}
