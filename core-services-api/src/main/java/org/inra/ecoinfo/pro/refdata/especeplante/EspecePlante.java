/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.especeplante;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.inra.ecoinfo.pro.refdata.famille.Famille;


/**
 *
 * @author adiankha
 */
@Entity
@Table(name = EspecePlante.NAME_ENTITY_JPA)
public class EspecePlante implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "especeplante";

    /**
     *
     */
    public static final String ID_JPA = "ep_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NOM = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_DESCRIPTIF ="descriptif";

    /**
     *
     */
    public static final String JPA_COLUMN_SOURCE ="source";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = EspecePlante.ID_JPA, unique = true, nullable = false, updatable = false)
    private long ep_id = 0;
    @Size(min = 1, max = 80)
    @Column(name = EspecePlante.JPA_COLUMN_NOM, unique = true, nullable = false, length = 80)
    private String nom = "";
    @Column(name = EspecePlante.JPA_COLUMN_CODE, unique = true, nullable = false)
    private String code = "";
    @Column(name=EspecePlante.JPA_COLUMN_COMMENT)
    private String commentaire="";
    @Column(name=EspecePlante.JPA_COLUMN_DESCRIPTIF)
    private String descriptif="";
    @Column(name=EspecePlante.JPA_COLUMN_SOURCE)
    private String source="";
    
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Famille.JPA_ID,  nullable = false)
    private Famille  famille;

    /**
     *
     */
    public EspecePlante() {
        super();
    }

    EspecePlante(String nom, String code, String commentaire, String descriptif, String source, Famille famille) {
        this.nom = nom;
        this.code = code;
        this.commentaire = commentaire;
        this.descriptif=descriptif;
        this.source=source;
        this.famille=famille;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getDescriptif() {
        return descriptif;
    }

    /**
     *
     * @param descriptif
     */
    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    /**
     *
     * @return
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     *
     * @return
     */
    public Famille getFamille() {
        return famille;
    }

    /**
     *
     * @param famille
     */
    public void setFamille(Famille famille) {
        this.famille = famille;
    }

    

    
    
    
}
