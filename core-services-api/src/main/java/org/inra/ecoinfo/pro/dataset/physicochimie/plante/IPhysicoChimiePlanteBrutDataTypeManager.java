/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante;

/**
 *
 * @author adiankha
 */
public interface IPhysicoChimiePlanteBrutDataTypeManager {

    /**
     *
     */
    String CODE_DATATYPE_PLANTE_BRUT = "plante_physico-chimie_donnees_elementaires";
    
}
