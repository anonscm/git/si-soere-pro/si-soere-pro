/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureSemisPlantation.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    MesureSemisPlantation.ATTRIBUTE_JPA_DISPOSITIF,
    DescriptionTraitement.ID_JPA,
    MesureSemisPlantation.ATTRIBUTE_JPA_LocalDate_DEBUT,
    MesureSemisPlantation.ATTRIBUTE_JPA_PARCELLE,
    MesureSemisPlantation.ATTRIBUTE_JPA_PLACETTE,
    MesureSemisPlantation.ATTRIBUTE_JPA_CULTURE}))

public class MesureSemisPlantation extends MesureITK<MesureApport, ValeurApport> {

    /**
     *
     */
    public static final String TABLE_NAME = "mesureinterventionsemisplantation";

    /**
     *
     */
    public static final String ATTRIBUTE_VARIETE = "varietecepage";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL_1 = "materiel1";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL_2 = "materiel2";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL_3 = "materiel3";

    /**
     *
     */
    public static final String ATTRIBUTE_NIVEAU = "niveauatteint";

    static final long serialVersionUID = 1L;
    @Column(name = MesureSemisPlantation.ATTRIBUTE_VARIETE, nullable = false)
    private String varietecepage = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_MATERIEL_1, nullable = false)
    private String materiel1 = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_MATERIEL_2, nullable = true)
    private String materiel2 = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_MATERIEL_3, nullable = true)
    private String materiel3 = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint = "";

    @OneToMany(mappedBy = "mesureinterventionsemis", cascade = ALL)
    List<ValeurSemisPlantation> valeursemis = new LinkedList();

    /**
     *
     */
    public MesureSemisPlantation() {
        super();
    }

    /**
     *
     * @param LocalDatedebutmesure
     * @param codedispositif
     * @param codetraitement
     * @param nomparcelle
     * @param nomplacette
     * @param varietecepage
     * @param nomculture
     * @param materiel1
     * @param materiel2
     * @param materiel3
     * @param niveau
     * @param versionfile
     * @param typeobservation
     * @param nomobservation
     * @param commentaire
     */
    public MesureSemisPlantation(LocalDate LocalDatedebutmesure, String codedispositif, DescriptionTraitement codetraitement, String nomparcelle, String nomplacette, String varietecepage,
            String nomculture, String materiel1, String materiel2, String materiel3, String niveau,
            VersionFile versionfile, String typeobservation, String nomobservation, String commentaire
    ) {
        super(nomculture, codedispositif, commentaire, nomparcelle, nomplacette, LocalDatedebutmesure, null, typeobservation, nomobservation, versionfile, codetraitement);
        this.varietecepage = varietecepage;
        this.materiel1 = materiel1;
        this.materiel2 = materiel2;
        this.materiel3 = materiel3;
        this.niveauatteint = niveau;
    }

    public String getVarietecepage() {
        return varietecepage;
    }

    public void setVarietecepage(String varietecepage) {
        this.varietecepage = varietecepage;
    }

    public String getMateriel1() {
        return materiel1;
    }

    public void setMateriel1(String materiel1) {
        this.materiel1 = materiel1;
    }

    public String getMateriel2() {
        return materiel2;
    }

    public void setMateriel2(String materiel2) {
        this.materiel2 = materiel2;
    }

    public String getMateriel3() {
        return materiel3;
    }

    public void setMateriel3(String materiel3) {
        this.materiel3 = materiel3;
    }

    public String getNiveauatteint() {
        return niveauatteint;
    }

    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    public List<ValeurSemisPlantation> getValeursemis() {
        return valeursemis;
    }

    public void setValeursemis(List<ValeurSemisPlantation> valeursemis) {
        this.valeursemis = valeursemis;
    }
}
