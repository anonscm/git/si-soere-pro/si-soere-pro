/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurIncubationSolMoy.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurIncubationSolMoy.ATTRIBUTE_JPA_ECART_TYPE,
    RealNode.ID_JPA,
    MesureIncubationSolMoy.ID_JPA
}
),
        indexes = {
            @Index(name = "valeur_incub_mesure_moy_idx", columnList = MesureIncubationSolMoy.ID_JPA),
            @Index(name = "valeur_incub_variable", columnList = RealNode.ID_JPA)
        }
)
@PrimaryKeyJoinColumn(name = ValeurIncubation.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurIncubationSolMoy.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurIncubation.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurIncubationSolMoy.ID_JPA)),
    @AttributeOverride(name = ValeurIncubation.ATTRIBUTE_JPA_QUANTITE, column = @Column(name = ValeurIncubationSolMoy.COLUMN_JPA_VALUE))
})
public class ValeurIncubationSolMoy extends ValeurIncubation<MesureIncubationSolMoy, ValeurIncubationSolMoy> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurincubationsolmoy";

    /**
     *
     */
    public static final String ID_JPA = "vism_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeurmoy";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ECART_TYPE = "ecart_type";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";

    @Column(name = ValeurIncubationSolMoy.ATTRIBUTE_JPA_ECART_TYPE)
    private float ecart_type;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureIncubationSolMoy.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureIncubationSolMoy mesureIncubationSolMoy;

    /**
     *
     */
    public ValeurIncubationSolMoy() {
        super();
    }

    /**
     *
     * @param valeurmoy
     * @param ecart_type
     * @param mesureIncubationSolMoy
     * @param realNode
     */
    public ValeurIncubationSolMoy(float valeurmoy, float ecart_type, MesureIncubationSolMoy mesureIncubationSolMoy, RealNode realNode) {
        super(valeurmoy, realNode);
        this.ecart_type = ecart_type;
        this.mesureIncubationSolMoy = mesureIncubationSolMoy;
    }

    /**
     *
     * @return
     */
    public float getEcart_type() {
        return ecart_type;
    }

    /**
     *
     * @param ecart_type
     */
    public void setEcart_type(float ecart_type) {
        this.ecart_type = ecart_type;
    }

    /**
     *
     * @return
     */
    public MesureIncubationSolMoy getMesureIncubationSolMoy() {
        return mesureIncubationSolMoy;
    }

    /**
     *
     * @param mesureIncubationSolMoy
     */
    public void setMesureIncubationSolMoy(MesureIncubationSolMoy mesureIncubationSolMoy) {
        this.mesureIncubationSolMoy = mesureIncubationSolMoy;
    }

}
