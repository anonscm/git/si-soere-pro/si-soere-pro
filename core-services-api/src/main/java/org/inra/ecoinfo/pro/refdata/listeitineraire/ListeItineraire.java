/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeitineraire;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=ListeItineraire.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {ListeItineraire.JPA_COLUMN_LISTE_NAME,
                                                                                          ListeItineraire.JPA_COLUMN_LISTE_VALEUR
}))

public class ListeItineraire implements IValeurQualitative{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "listeitineraire";

    /**
     *
     */
    public static final String JPA_ID = "listeitineraire_id";

    /**
     *
     */
    public static final String JPA_COLUMN_LISTE_NAME = "liste_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_LISTE_KEY = "knom";

    /**
     *
     */
    public static final String JPA_COLUMN_LISTE_VALEUR = "liste_valeur";

    /**
     *
     */
    public static final String JPA_COLUMN_LISTE_VALUE = "kvaleur";

    /**
     *
     */
    public static final String JPA_COLUMN_LISTE_COMMENTAIRE = "liste_commentaire";
    
    @Id
    @Column(name = ListeItineraire.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long listeitineraire_id;
    
    @Column(name = ListeItineraire.JPA_COLUMN_LISTE_NAME, unique = false, nullable = false, length = 150)
    private String liste_nom;
    @Column(name = ListeItineraire.JPA_COLUMN_LISTE_VALEUR, unique = false, nullable = true, length = 150)
    private String liste_valeur;
     @Column(name = ListeItineraire.JPA_COLUMN_LISTE_KEY)
    private String knom;
    @Column(name = ListeItineraire.JPA_COLUMN_LISTE_VALUE)
    private String kvaleur;
    @Column(name = ListeItineraire.JPA_COLUMN_LISTE_COMMENTAIRE, unique = false, nullable = true, length = 150)
    private String liste_commentaire;

    /**
     *
     */
    public ListeItineraire() {
        super();
    }

    /**
     *
     * @param liste_nom
     * @param liste_valeur
     * @param liste_commentaire
     */
    public ListeItineraire(String liste_nom, String liste_valeur, String liste_commentaire) {
        this.knom = Utils.createCodeFromString(liste_nom);
        this.liste_nom = liste_nom;
        this.liste_valeur = liste_valeur;
         this.kvaleur = Utils.createCodeFromString(liste_valeur);
        this.liste_commentaire = liste_commentaire;
    }

    /**
     *
     * @return
     */
    public long getListeitineraire_id() {
        return listeitineraire_id;
    }

    /**
     *
     * @param listeitineraire_id
     */
    public void setListeitineraire_id(long listeitineraire_id) {
        this.listeitineraire_id = listeitineraire_id;
    }

    /**
     *
     * @return
     */
    public String getListe_nom() {
        return liste_nom;
    }

    /**
     *
     * @param liste_nom
     */
    public void setListe_nom(String liste_nom) {
        this.liste_nom = liste_nom;
    }

    /**
     *
     * @return
     */
    public String getListe_valeur() {
        return liste_valeur;
    }

    /**
     *
     * @param liste_valeur
     */
    public void setListe_valeur(String liste_valeur) {
        this.liste_valeur = liste_valeur;
    }

    /**
     *
     * @return
     */
    public String getListe_commentaire() {
        return liste_commentaire;
    }

    /**
     *
     * @param liste_commentaire
     */
    public void setListe_commentaire(String liste_commentaire) {
        this.liste_commentaire = liste_commentaire;
    }

    /**
     *
     * @return
     */
    public String getKnom() {
        return knom;
    }

    /**
     *
     * @param knom
     */
    public void setKnom(String knom) {
        this.knom = knom;
    }

    /**
     *
     * @return
     */
    public String getKvaleur() {
        return kvaleur;
    }

    /**
     *
     * @param kvaleur
     */
    public void setKvaleur(String kvaleur) {
        this.kvaleur = kvaleur;
    }

    /**
     *
     * @return
     */
    @Override
    public String getCode() {
        return getKnom();
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return getListeitineraire_id();
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return getListe_nom();
    }

    /**
     *
     * @return
     */
    @Override
    public String getValeur() {
        return getListe_valeur();
    }

    /**
     *
     * @param valeur
     */
    @Override
    public void setValeur(String valeur) {
        setListe_valeur(liste_valeur);
    }
     
    
}
