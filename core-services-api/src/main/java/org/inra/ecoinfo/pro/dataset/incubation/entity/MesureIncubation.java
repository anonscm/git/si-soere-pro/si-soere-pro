/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
@MappedSuperclass
public class MesureIncubation<M extends MesureIncubation, V extends ValeurIncubation>  implements Serializable{

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate_PREL_SOL = "LocalDate_prel_sol";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate_DEB = "LocalDate_debut_incub";

    /**
     *
     */
    public static final String COLUMN_MASSE_SOL = "masse_de_sol";

    /**
     *
     */
    public static final String COLUMN_NUM_REP = "numero_rep_analyse";

    /**
     *
     */
    public static final String COLUMN_ORDRE_MANIP = "ordre_manip";

    /**
     *
     */
    public static final String COLUMN_JOUR_INCUB = "jour_incub";

    /**
     *
     */
    public static final String COLUMN_KEY_MESURE = "keymesure";

    /**
     *
     */
    public static final String COLUMN_LIGNE_FICHIER_ECHANGE = "ligne_fichier_echange";



    @Id
    @Column(name = MesureIncubation.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = MesureIncubation.COLUMN_JPA_LocalDate_PREL_SOL, nullable = false)
    private LocalDate LocalDate_prel_sol;

    @Column(name = MesureIncubation.COLUMN_JPA_LocalDate_DEB, nullable = false)
    private LocalDate LocalDate_debut_incub;

    @Column(name = MesureIncubation.COLUMN_NUM_REP, nullable = false)
    private int numero_rep_analyse = 0;

    @Column(name = MesureIncubation.COLUMN_MASSE_SOL, nullable = false)
    private float masse_de_sol = 0;

    @Column(name = MesureIncubation.COLUMN_JOUR_INCUB, nullable = false)
    private int jour_incub = 0;

    @Column(name = MesureIncubation.COLUMN_ORDRE_MANIP, nullable = false)
    private int ordre_manip = 0;

    @Column(name = MesureIncubation.COLUMN_KEY_MESURE, nullable = false, unique = true)
    private String keymesure = null;

    @Column(name = MesureIncubation.COLUMN_LIGNE_FICHIER_ECHANGE)
    private Long ligneFichierEchange;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private VersionFile versionfile;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = EchantillonsSol.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private EchantillonsSol echantillonsSol;

    /**
     *
     */
    public MesureIncubation() {
    }

    /**
     *
     * @param LocalDate_prel_sol
     * @param LocalDate_debut_incub
     * @param numero_rep_analyse
     * @param masse_de_sol
     * @param jour_incub
     * @param ordre_manip
     * @param ligneFichierEchange
     * @param versionfile
     * @param echantillonsSol
     */
    public MesureIncubation(
            LocalDate LocalDate_prel_sol, LocalDate LocalDate_debut_incub, 
            int numero_rep_analyse, float masse_de_sol, int jour_incub, int ordre_manip, 
            Long ligneFichierEchange, 
            VersionFile versionfile, 
            EchantillonsSol echantillonsSol) {
        this.LocalDate_prel_sol = LocalDate_prel_sol;
        this.LocalDate_debut_incub = LocalDate_debut_incub;
        this.numero_rep_analyse = numero_rep_analyse;
        this.masse_de_sol = masse_de_sol;
        this.jour_incub=jour_incub;
        this.ordre_manip=ordre_manip;
        this.ligneFichierEchange = ligneFichierEchange;
        this.versionfile = versionfile;
        this.echantillonsSol = echantillonsSol;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDate_prel_sol() {
        return LocalDate_prel_sol;
    }

    /**
     *
     * @param LocalDate_prel_sol
     */
    public void setLocalDate_prel_sol(LocalDate LocalDate_prel_sol) {
        this.LocalDate_prel_sol = LocalDate_prel_sol;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDate_debut_incub() {
        return LocalDate_debut_incub;
    }

    /**
     *
     * @param LocalDate_debut_incub
     */
    public void setLocalDate_debut_incub(LocalDate LocalDate_debut_incub) {
        this.LocalDate_debut_incub = LocalDate_debut_incub;
    }

    /**
     *
     * @return
     */
    public int getNumero_rep_analyse() {
        return numero_rep_analyse;
    }

    /**
     *
     * @param numero_rep_analyse
     */
    public void setNumero_rep_analyse(int numero_rep_analyse) {
        this.numero_rep_analyse = numero_rep_analyse;
    }

    /**
     *
     * @return
     */
    public float getMasse_de_sol() {
        return masse_de_sol;
    }

    /**
     *
     * @param masse_de_sol
     */
    public void setMasse_de_sol(float masse_de_sol) {
        this.masse_de_sol = masse_de_sol;
    }

    /**
     *
     * @return
     */
    public int getJour_incub() {
        return jour_incub;
    }

    /**
     *
     * @param jour_incub
     */
    public void setJour_incub(int jour_incub) {
        this.jour_incub = jour_incub;
    }

    /**
     *
     * @return
     */
    public int getOrdre_manip() {
        return ordre_manip;
    }

    /**
     *
     * @param ordre_manip
     */
    public void setOrdre_manip(int ordre_manip) {
        this.ordre_manip = ordre_manip;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionfile() {
        return versionfile;
    }

    /**
     *
     * @param versionfile
     */
    public void setVersionfile(VersionFile versionfile) {
        this.versionfile = versionfile;
    }

    /**
     *
     * @return
     */
    public EchantillonsSol getEchantillonsSol() {
        return echantillonsSol;
    }

    /**
     *
     * @param echantillonsSol
     */
    public void setEchantillonsSol(EchantillonsSol echantillonsSol) {
        this.echantillonsSol = echantillonsSol;
    }
}
