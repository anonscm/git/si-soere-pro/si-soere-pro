/**
 *
 */
package org.inra.ecoinfo.pro.refdata;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


/**
 * @author sophie
 * 
 */
/**
 * Permettra d'utiliser dans les données expérimentales, zone de prélèvement (ou d'intervention) de façon générique). La table Mesure (pour les données expérimentales) aura une colonne id_prelevt. Cette colonne correspondra à l'identifiant soit d'un
 * dispositif, soit d'un bloc, soit d'une parcelle élémentaire, soit d'une placette, soit d'une description tratement grâce au mécanisme du mapping relationnel objet. (Faire un code générique pour les données expérimentales qui utiiseront les les
 * zones de prélèvement (ou d'intervention))
 */
@MappedSuperclass
public class ZonePrelevement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    /**
     * empty constructor
     */
    public ZonePrelevement() {
        super();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

}
