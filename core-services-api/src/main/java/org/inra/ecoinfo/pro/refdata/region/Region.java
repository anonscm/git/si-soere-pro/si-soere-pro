
package org.inra.ecoinfo.pro.refdata.region;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.departement.Departement;
import org.inra.ecoinfo.pro.refdata.pays.Pays;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Region.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_REGION}))
public class Region implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.REGION_ID; // rg_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.REGION_TABLE_NAME; // region
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = Region.ID_JPA)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_REGION, length = 50)
    private String nom;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_REGION, length = 50)
    private String code;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Pays.class)
    @JoinColumn(name = Pays.ID_JPA, referencedColumnName = Pays.ID_JPA, nullable = false)
    private Pays pays;
    
    @OneToMany(mappedBy = RefDataConstantes.NAME_ATTRIBUT_REGION, cascade = {PERSIST, MERGE, REFRESH, REMOVE})
    private List<Departement> lstDepartement = new LinkedList<Departement>();
    
    /**
     *
     */
    public Region() 
    {
        super();
    }
    
    /**
     *
     * @param nom
     */
    public Region(String nom) 
    {
        super();
        this.nom = nom;
    }
    
	/**
	 * @param nom
	 * @param pays
	 */
	public Region(String nom, Pays pays) {
		super();
		this.nom = nom;
		this.pays = pays;
	}
	
	/**
	 * @param nom
	 * @param code
	 * @param pays
	 */
	public Region(String nom, String code, Pays pays) {
		super();
		this.nom = nom;
		this.code = code;
		this.pays = pays;
	}

    /**
     *
     * @param departement
     */
    public void addDepartement(Departement departement) 
    {
        this.lstDepartement.add(departement);
        departement.setRegion(this);
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public List<Departement> getLstDepartement() {
        return lstDepartement;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @return
     */
    public Pays getPays() {
        return pays;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param lstDepartement
     */
    public void setLstDepartement(List<Departement> lstDepartement) {
        this.lstDepartement = lstDepartement;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
     *
     * @param pays
     */
    public void setPays(Pays pays) {
        this.pays = pays;
    }
}
