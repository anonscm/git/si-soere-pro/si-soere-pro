/**
 *
 */
package org.inra.ecoinfo.pro.refdata.facteur;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Facteur.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_FCT}))
public class Facteur implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.FACTEUR_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.FACTEUR_TABLE_NAME;
    
    @Id
    @Column(name = Facteur.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = TypeFacteur.class)
    @JoinColumn(name = RefDataConstantes.TYPEFACTEUR_ID, referencedColumnName = TypeFacteur.ID_JPA, nullable = false)
    private TypeFacteur typeFacteur;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_LIBELLE_FCT, length = 60)
    private String libelle;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_CODE_FCT, length = 60)
    private String code;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ISINSOSUPPLFERT_FCT)
    private Boolean isInfosSupplApportFert;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ISASSOCNOMENCLATURE)
    private Boolean isAssocNomenclature;
    
    /**
     *
     */
    public Facteur() 
    {
        super();
    }
    
    /**
     * @param typeFacteur
     * @param libelle
     */
    public Facteur(TypeFacteur typeFacteur, String libelle) 
    {
        super();
        this.typeFacteur = typeFacteur;
        this.libelle = libelle;
    }
    
    /**
	 * @param typeFacteur
	 * @param libelle
	 * @param code
	 */
	public Facteur(TypeFacteur typeFacteur, String libelle, String code) {
		super();
		this.typeFacteur = typeFacteur;
		this.libelle = libelle;
		this.code = code;
	}
    
    /**
	 * @param typeFacteur
	 * @param libelle
	 * @param code
	 * @param isInfosSupplApportFert
	 * @param isAssocNomenclature
	 */
	public Facteur(TypeFacteur typeFacteur, String libelle, String code,
			Boolean isInfosSupplApportFert, Boolean isAssocNomenclature) {
		super();
		this.typeFacteur = typeFacteur;
		this.libelle = libelle;
		this.code = code;
		this.isInfosSupplApportFert = isInfosSupplApportFert;
		this.isAssocNomenclature = isAssocNomenclature;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the typeFacteur
	 */
	public TypeFacteur getTypeFacteur() {
		return typeFacteur;
	}

	/**
	 * @param typeFacteur the typeFacteur to set
	 */
	public void setTypeFacteur(TypeFacteur typeFacteur) {
		this.typeFacteur = typeFacteur;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the isInfosSupplApportFert
	 */
	public Boolean getIsInfosSupplApportFert() {
		return isInfosSupplApportFert;
	}

	/**
	 * @param isInfosSupplApportFert the isInfosSupplApportFert to set
	 */
	public void setIsInfosSupplApportFert(Boolean isInfosSupplApportFert) {
		this.isInfosSupplApportFert = isInfosSupplApportFert;
	}

	/**
	 * @return the isAssocNomenclature
	 */
	public Boolean getIsAssocNomenclature() {
		return isAssocNomenclature;
	}

	/**
	 * @param isAssocNomenclature the isAssocNomenclature to set
	 */
	public void setIsAssocNomenclature(Boolean isAssocNomenclature) {
		this.isAssocNomenclature = isAssocNomenclature;
	}
}
