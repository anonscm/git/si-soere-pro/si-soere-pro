/**
 *
 */
package org.inra.ecoinfo.pro.refdata.protocole;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Protocole.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_PROTOCOLE}))
public class Protocole implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PROTOCOLE_ID; // prt_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PROTOCOLE_TABLE_NAME; // protocole
    @Id
    @Column(name = Protocole.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_PROTOCOLE, length = 255)
    private String nom;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ANNEEDEBVAL_PROTOCOLE, length = 4)
    private String anneeDebutVal;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEFINVAL_PROTOCOLE, length = 4)
    private String anneeFinVal;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_UTILGUIDEMETH_PROTOCOLE)
    private Boolean utiliseGuideMethodologique;

    /**
     *
     */
    public Protocole() {
        super();
    }

    /**
     *
     * @param nom
     * @param anneeDebutVal
     * @param anneeFinVal
     * @param utiliseGuideMethodologique
     */
    public Protocole(String nom, String anneeDebutVal, String anneeFinVal, Boolean utiliseGuideMethodologique) {
        super();
        this.nom = nom;
        this.anneeDebutVal = anneeDebutVal;
        this.anneeFinVal = anneeFinVal;
        this.utiliseGuideMethodologique = utiliseGuideMethodologique;
    }

    /**
     *
     * @return
     */
    public String getAnneeDebutVal() {
        return anneeDebutVal;
    }

    /**
     *
     * @return
     */
    public String getAnneeFinVal() {
        return anneeFinVal;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public Boolean getUtiliseGuideMethodologique() {
        return utiliseGuideMethodologique;
    }

    /**
     *
     * @param anneeDebutVal
     */
    public void setAnneeDebutVal(String anneeDebutVal) {
        this.anneeDebutVal = anneeDebutVal;
    }

    /**
     *
     * @param anneeFinVal
     */
    public void setAnneeFinVal(String anneeFinVal) {
        this.anneeFinVal = anneeFinVal;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @param utiliseGuideMethodologique
     */
    public void setUtiliseGuideMethodologique(Boolean utiliseGuideMethodologique) {
        this.utiliseGuideMethodologique = utiliseGuideMethodologique;
    }
}
