/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementproduit;

import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;

/**
 * uniqueConstraints =@UniqueConstraint(columnNames = {
 * PrelevementProduit.JPA_COLUMN_DATE, PrelevementProduit.JPA_COLUMN_NU_REP_PRO,
 * ProduitDispositif.JPA_COLUMN_ID, DescriptionTraitement.ID_JPA, Bloc.ID_JPA,
 * Placette.ID_JPA
 *
 * @author adiankha
 */
@Entity
@Table(name = PrelevementProduit.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    PrelevementProduit.JPA_COLUMN_DATE,
    ProduitDispositif.JPA_COLUMN_ID,
    DescriptionTraitement.ID_JPA,
    Bloc.ID_JPA,//peut être bloc X
    ParcelleElementaire.ID_JPA, //peut être parcelle élémentaire X
    Placette.ID_JPA, //peut être placette X
    PrelevementProduit.JPA_COLUMN_NU_REP_PRO
}))
public class PrelevementProduit implements Serializable {

    /**
     *
     */
    public static final String UNDERSCORE = "_";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "prelevementproduit";

    /**
     *
     */
    public static final String ID_JPA = "pp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_DATE = "date_prelevement";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_PRELEVEMENT = "codeprelevement";

    /**
     *
     */
    public static final String JPA_COLUMN_OUTIL = "outils";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_NU_REP_PRO = "numerorepetition";

    /**
     *
     */
    public static final String JPA_COLUMN_ENDROIT = "endroit";
    /**
     * 02_09_1998_DVB-UMR ECOSYS-1998-78_QA_DVB avec N_X_X_X_1
     *
     * @param date
     * @param numero
     * @param codeDispositif
     * @param produit
     * @param codetrait
     * @param bloc
     * @param nomParcelle
     * @param placette
     * @return
     */
    public static String buildCodePrelevementProduit(
            String date,
            int numero,
            String codeDispositif,
            String nomLieu,
            String produit,
            final String codetrait,
            String bloc,
            String nomParcelle,
            String placette) {
        StringBuilder codeprel=new StringBuilder();
        date=DateUtil.getUTCDateTextFromLocalDateTime(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, date), "dd_MM_yyyy");
        codeprel.append(date).append(UNDERSCORE)
                .append(produit).append(UNDERSCORE)
                .append(codeDispositif).append(UNDERSCORE)
                .append(nomLieu).append(UNDERSCORE)
                .append(codetrait).append(UNDERSCORE)
                .append(bloc).append(UNDERSCORE)
                .append(nomParcelle).append(UNDERSCORE)
                .append(placette).append(UNDERSCORE)
                .append(numero);
        return Utils.createCodeFromString(codeprel.toString().replaceAll("_sans(?=[_$])", ""));
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PrelevementProduit.ID_JPA)
    private long pp_id = 0;
    @Column(name = PrelevementProduit.JPA_COLUMN_DATE, nullable = false, unique = false)
    private LocalDate date_prelevement = null;

    @Column(name = PrelevementProduit.JPA_COLUMN_CODE_PRELEVEMENT, unique = true, nullable = false)
    private String codeprelevement = "";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Bloc.ID_JPA, nullable = true, unique = false)
    private Bloc bloc;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = ProduitDispositif.JPA_COLUMN_ID, nullable = true, unique = false)
    private ProduitDispositif produitdispositif;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, nullable = false, unique = false)
    private DescriptionTraitement traitement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = ParcelleElementaire.ID_JPA, nullable = true, unique = false)
    private ParcelleElementaire pelementaire;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Placette.ID_JPA, nullable = true, unique = false)
    private Placette placette;

    @Column(name = PrelevementProduit.JPA_COLUMN_OUTIL, nullable = true)
    private String outils = "";
    @Column(name = PrelevementProduit.JPA_COLUMN_COMENT, length = 300, nullable = true)
    private String commentaire = "";
    @Column(name = PrelevementProduit.JPA_COLUMN_NU_REP_PRO, nullable = false, unique = false)
    private int numerorepetition = 0;
    @Column(name = PrelevementProduit.JPA_COLUMN_ENDROIT)
    private String endroit = "";

    /**
     *
     */
    public PrelevementProduit() {
        super();
    }

    /**
     *
     * @param datep
     * @param codeprel
     * @param outil
     * @param commentaire
     * @param produitdispositif
     * @param traitement
     * @param bloc
     * @param pelementaire
     * @param placette
     * @param numero
     * @param endroit
     */
    public PrelevementProduit(LocalDate datep, String codeprel,
            String outil, String commentaire, ProduitDispositif produitdispositif, DescriptionTraitement traitement,
            Bloc bloc, ParcelleElementaire pelementaire, Placette placette, int numero, String endroit) {

        this.date_prelevement = datep;
        this.codeprelevement = codeprel;
        this.outils = outil;
        this.commentaire = commentaire;
        this.bloc = bloc;
        this.pelementaire = pelementaire;
        this.placette = placette;
        this.produitdispositif = produitdispositif;
        this.traitement = traitement;
        this.numerorepetition = numero;
        this.endroit = endroit;

    }

    /**
     *
     * @return
     */
    public long getPp_id() {
        return pp_id;
    }

    /**
     *
     * @param pp_id
     */
    public void setPp_id(long pp_id) {
        this.pp_id = pp_id;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_prelevement() {
        return date_prelevement;
    }

    /**
     *
     * @param date_prelevement
     */
    public void setDate_prelevement(LocalDate date_prelevement) {
        this.date_prelevement = date_prelevement;
    }

    /**
     *
     * @return
     */
    public String getCodeprelevement() {
        return codeprelevement;
    }

    /**
     *
     * @param codeprelevement
     */
    public void setCodeprelevement(String codeprelevement) {
        this.codeprelevement = codeprelevement;
    }

    /**
     *
     * @return
     */
    public String getOutils() {
        return outils;
    }

    /**
     *
     * @param bloc
     */
    public void setBloc(Bloc bloc) {
        this.bloc = bloc;
    }

    /**
     *
     * @param outils
     */
    public void setOutils(String outils) {
        this.outils = outils;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Bloc getBloc() {
        return bloc;
    }

    /**
     *
     * @return
     */
    public ParcelleElementaire getPelementaire() {
        return pelementaire;
    }

    /**
     *
     * @return
     */
    public Placette getPlacette() {
        return placette;
    }

    /**
     *
     * @param pelementaire
     */
    public void setPelementaire(ParcelleElementaire pelementaire) {
        this.pelementaire = pelementaire;
    }

    /**
     *
     * @param placette
     */
    public void setPlacette(Placette placette) {
        this.placette = placette;
    }

    /**
     *
     * @return
     */
    public ProduitDispositif getProduitdispositif() {
        return produitdispositif;
    }

    /**
     *
     * @param produitdispositif
     */
    public void setProduitdispositif(ProduitDispositif produitdispositif) {
        this.produitdispositif = produitdispositif;
    }

    /**
     *
     * @return
     */
    public DescriptionTraitement getTraitement() {
        return traitement;
    }

    /**
     *
     * @param traitement
     */
    public void setTraitement(DescriptionTraitement traitement) {
        this.traitement = traitement;
    }

    /**
     *
     * @return
     */
    public int getNumerorepetition() {
        return numerorepetition;
    }

    /**
     *
     * @param numerorepetition
     */
    public void setNumerorepetition(int numerorepetition) {
        this.numerorepetition = numerorepetition;
    }

    /**
     *
     * @return
     */
    public String getEndroit() {
        return endroit;
    }

    /**
     *
     * @param endroit
     */
    public void setEndroit(String endroit) {
        this.endroit = endroit;
    }


}
