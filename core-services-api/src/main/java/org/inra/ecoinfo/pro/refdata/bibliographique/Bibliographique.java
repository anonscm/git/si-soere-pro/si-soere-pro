/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.bibliographique;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Bibliographique.NAME_ENTITY_JPA)
public class Bibliographique implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "bibliographique";

    /**
     *
     */
    public static final String JPA_ID = "biblio_id";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE = "biblio_mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_DOI = "biblio_doi";

    /**
     *
     */
    public static final String JPA_COLUMN_AUTEUR = "fist_auteur";

    /**
     *
     */
    public static final String JPA_COLUMN_DATE = "annee";

    /**
     *
     */
    public static final String JPA_COLUMN_URL = "url";
    
     @Id
    @Column(name = Bibliographique.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long biblio_id;
    @Column(name = Bibliographique.JPA_COLUMN_DOI, unique = true, nullable = false, length = 150)
    private String biblio_doi="";
    @Column(name = Bibliographique.JPA_COLUMN_MYCODE, unique = true, nullable = false, length = 150)
    private String biblio_mycode="";
    @Column(name = Bibliographique.JPA_COLUMN_AUTEUR)
    private String fist_auteur="";
    @Column(name = Bibliographique.JPA_COLUMN_DATE,length = 4)
    private String annee="";
    @Column(name = Bibliographique.JPA_COLUMN_URL,length = 50)
    private String url="";
    
    /**
     *
     */
    public Bibliographique(){
        super();
    }

    /**
     *
     * @param doi
     * @param auteur
     * @param annee
     * @param url
     */
    public Bibliographique(String doi,String auteur,String annee,String url){
        this.biblio_doi = doi;
        this.fist_auteur = auteur;
        this.annee = annee;
        this.url = url;
    }

    /**
     *
     * @return
     */
    public long getBiblio_id() {
        return biblio_id;
    }

    /**
     *
     * @return
     */
    public String getBiblio_doi() {
        return biblio_doi;
    }

    /**
     *
     * @return
     */
    public String getBiblio_mycode() {
        return biblio_mycode;
    }

    /**
     *
     * @return
     */
    public String getFist_auteur() {
        return fist_auteur;
    }

    /**
     *
     * @return
     */
    public String getAnnee() {
        return annee;
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param biblio_id
     */
    public void setBiblio_id(long biblio_id) {
        this.biblio_id = biblio_id;
    }

    /**
     *
     * @param biblio_doi
     */
    public void setBiblio_doi(String biblio_doi) {
        this.biblio_doi = biblio_doi;
    }

    /**
     *
     * @param biblio_mycode
     */
    public void setBiblio_mycode(String biblio_mycode) {
        this.biblio_mycode = biblio_mycode;
    }

    /**
     *
     * @param fist_auteur
     */
    public void setFist_auteur(String fist_auteur) {
        this.fist_auteur = fist_auteur;
    }

    /**
     *
     * @param annee
     */
    public void setAnnee(String annee) {
        this.annee = annee;
    }

    /**
     *
     * @param url
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    
}
