/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.incubationsol;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;

/**
 *
 * @author vjkoyao
 */

@Entity(name = "IncubationsolSynthesisValue")
@Table(indexes = {
    @Index(name = "IncubationsolSynthesisValue_disp_idx", columnList = "site"),
    @Index(name = "IncubationsolSynthesisValue_idNode_idx", columnList = "idNode"),
    @Index(name = "IncubationsolSynthesisValue_disp_variable_idx", columnList = "site,variable")})
public class SynthesisValue  extends AbstractProSynthesisValue {

    /**
     *
     */
    public SynthesisValue() {
        super();
    } 

//    public SynthesisValue(LocalDateTime date, String site, String variable, Double valuefloat) {
//        super(date, site, variable, value);
//    }



    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valuefloat
     * @param idNode
     */
     public SynthesisValue(LocalDate date, String site, String variable, Double valuefloat, Long idNode) {
        super(date.atStartOfDay(), site, variable, valuefloat, null, idNode);
    }
}
