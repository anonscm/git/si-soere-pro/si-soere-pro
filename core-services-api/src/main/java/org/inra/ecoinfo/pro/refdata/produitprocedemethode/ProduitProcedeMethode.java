package org.inra.ecoinfo.pro.refdata.produitprocedemethode;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.methodeprocess.MethodeProcess;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.unites.Unites;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = { Produits.JPA_ID,
 * MethodeProcess.ID_JPA, ProduitProcedeMethode.JPA_COLUMN_DURATION
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProduitProcedeMethode.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    Produits.JPA_ID,
    MethodeProcess.ID_JPA,
    ProduitProcedeMethode.JPA_COLUMN_DURATION
}))
public class ProduitProcedeMethode implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "produitprocedemethode";

    /**
     *
     */
    public static final String JPA_ID = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_DURATION = "duree";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @Column(name = ProduitProcedeMethode.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Produits.JPA_ID, nullable = false)
    private Produits produits;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodeProcess.ID_JPA, nullable = false)
    private MethodeProcess methodeprocess;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Unites.JPA_ID, nullable = false)
    private Unites unite;
    @Column(name = ProduitProcedeMethode.JPA_COLUMN_DURATION, nullable = false, unique = false)
    private int duree = 0;
    @Column(name = ProduitProcedeMethode.JPA_COLUMN_COMENT, length = 300)
    private String commentaire = "";

    /**
     *
     */
    public ProduitProcedeMethode() {
        super();
    }

    /**
     *
     * @param produit
     * @param mprocess
     * @param duree
     * @param unite
     * @param commentaire
     */
    public ProduitProcedeMethode(Produits produit, MethodeProcess mprocess, int duree, Unites unite, String commentaire) {
        this.produits = produit;
        this.methodeprocess = mprocess;
        this.duree = duree;
        this.unite = unite;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public MethodeProcess getMethodeprocess() {
        return methodeprocess;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @param methodeprocess
     */
    public void setMethodeprocess(MethodeProcess methodeprocess) {
        this.methodeprocess = methodeprocess;
    }

    /**
     *
     * @return
     */
    public Produits getProduits() {
        return produits;
    }

    /**
     *
     * @return
     */
    public int getDuree() {
        return duree;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param produits
     */
    public void setProduits(Produits produits) {
        this.produits = produits;
    }

    /**
     *
     * @return
     */
    public Unites getUnite() {
        return unite;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(Unites unite) {
        this.unite = unite;
    }

    /**
     *
     * @param duree
     */
    public void setDuree(int duree) {
        this.duree = duree;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
