
package org.inra.ecoinfo.pro.refdata.modalite;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Modalite.TABLE_NAME, uniqueConstraints = 
        @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_VALEUR_MDT, RefDataConstantes.FACTEUR_ID})) //Nomenclatures.JPA_ID
public class Modalite implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.MODALITE_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.MODALITE_TABLE_NAME;
    
    @Id
    @Column(name = Modalite.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Facteur.class)
    @JoinColumn(name = RefDataConstantes.FACTEUR_ID, referencedColumnName = Facteur.ID_JPA, nullable = false)
    private Facteur facteur;
    
    /*@ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = TypeNomenclature.class)
    @JoinColumn(name = TypeNomenclature.ID_JPA, referencedColumnName = TypeNomenclature.ID_JPA, nullable = true)
    private TypeNomenclature typeNomenclatures;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Nomenclatures.class)
    @JoinColumn(name = Nomenclatures.JPA_ID, referencedColumnName = Nomenclatures.JPA_ID, nullable = true)
    private Nomenclatures nomenclatures;
    */

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_VALEUR_MDT, length = 150)
    private String valeur;
    
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMENT_MDT, length = 500)
    private String commentaire;
    
    /**
     *
     */
    public Modalite() 
    {
        super();
    }
    
	/**
	 * @param facteur
	 * @param typeNomenclatures
	 * @param nomenclatures
	 * @param valeur
	 * @param commentaire
	 */
	/*public Modalite(Facteur facteur, TypeNomenclature typeNomenclatures,
			Nomenclatures nomenclatures, String valeur, String commentaire) {
		super();
		this.facteur = facteur;
		this.typeNomenclatures = typeNomenclatures;
		this.nomenclatures = nomenclatures;
		this.valeur = valeur;
		this.commentaire = commentaire;
	}
	*/

    /**
	 * @param facteur
	 * @param valeur
	 * @param commentaire
	 */
	public Modalite(Facteur facteur, String valeur, String commentaire) {
		super();
		this.facteur = facteur;
		this.valeur = valeur;
		this.commentaire = commentaire;
	}
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the facteur
	 */
	public Facteur getFacteur() {
		return facteur;
	}

	/**
	 * @param facteur the facteur to set
	 */
	public void setFacteur(Facteur facteur) {
		this.facteur = facteur;
	}

	/**
	 * @return the nomenclatures
	 */
	/*public Nomenclatures getNomenclatures() {
		return nomenclatures;
	}
	*/

	/**
	 * @param nomenclatures the nomenclatures to set
	 */
	/*public void setNomenclatures(Nomenclatures nomenclatures) {
		this.nomenclatures = nomenclatures;
	}
	*/
	
	/**
	 * @return the typeNomenclatures
	 */
	/*public TypeNomenclature getTypeNomenclatures() {
		return typeNomenclatures;
	}
	*/

	/**
	 * @param typeNomenclatures the typeNomenclatures to set
	 */
	
	/*public void setTypeNomenclatures(TypeNomenclature typeNomenclatures) {
		this.typeNomenclatures = typeNomenclatures;
	}
	*/

	/**
	 * @return the valeur
	 */
	public String getValeur() {
		return valeur;
	}

	/**
	 * @param valeur the valeur to set
	 */
	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}

	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
 
}
