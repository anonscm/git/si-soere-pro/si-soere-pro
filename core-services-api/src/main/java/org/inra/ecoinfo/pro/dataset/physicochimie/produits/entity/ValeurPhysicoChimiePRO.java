/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurPhysicoChimiePRO.JPA_NAME_TABLE,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurPhysicoChimiePRO.ATTRIBUTE_JPA_STATUT_VALEUR,
    RealNode.ID_JPA,
    MesurePhysicoChimiePRO.ID_JPA
}
        ),
        indexes = {
            @Index(name = "valeur_physico_chimie_pro_mesure_idx", columnList = MesurePhysicoChimiePRO.ID_JPA),
            @Index(name = "valeur_physico_chimie_pro_variable", columnList = RealNode.ID_JPA)
        }
)
@PrimaryKeyJoinColumn(name = ValeurPhysicoChimie.ID_JPA, referencedColumnName = ValeurPhysicoChimiePRO.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurPhysicoChimie.ID_JPA, column = @Column(name = ValeurPhysicoChimiePRO.ID_JPA))})
public class ValeurPhysicoChimiePRO extends ValeurPhysicoChimie<MesurePhysicoChimiePRO, ValeurPhysicoChimiePRO> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurphysicochimiepro";

    /**
     *
     */
    public static final String ID_JPA = "vpc_pro_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    @Column(name = ValeurPhysicoChimiePRO.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesurePhysicoChimiePRO.class)
    @JoinColumn(name = MesurePhysicoChimiePRO.ID_JPA, referencedColumnName = MesurePhysicoChimiePRO.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesurePhysicoChimiePRO mesurePhysicoChimiePRO;

    /**
     *
     */
    public ValeurPhysicoChimiePRO() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param mesurePhysicoChimiePRO
     * @param realNode
     */
    public ValeurPhysicoChimiePRO(Float valeur, String statutvaleur, MesurePhysicoChimiePRO mesurePhysicoChimiePRO, RealNode realNode) {
        super(valeur, realNode);
        this.statutvaleur = statutvaleur;
        this.mesurePhysicoChimiePRO = mesurePhysicoChimiePRO;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public MesurePhysicoChimiePRO getMesurePhysicoChimiePRO() {
        return mesurePhysicoChimiePRO;
    }

    /**
     *
     * @param mesurePhysicoChimiePRO
     */
    public void setMesurePhysicoChimiePRO(MesurePhysicoChimiePRO mesurePhysicoChimiePRO) {
        this.mesurePhysicoChimiePRO = mesurePhysicoChimiePRO;
    }
}
