/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeechantillon;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=TypeEchantillon.NAME_ENTITY_JPA)
public class TypeEchantillon implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "typeechantillon";

    /**
     *
     */
    public static final String JPA_ID = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "te_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "te_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "te_comment";
    
    @Id
    @Column(name = TypeEchantillon.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = TypeEchantillon.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String te_code = "";
    @Column(name = TypeEchantillon.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String te_nom = "";
    @Column(name=TypeEchantillon.JPA_COLUMN_COMMENT,length = 1000,nullable = true,unique = false)
    private String te_comment="";
    
    /**
     *
     */
    public TypeEchantillon(){
        super();
    }
   
    /**
     *
     * @param nom
     * @param comment
     */
    public TypeEchantillon( String nom,String comment) {
        this.te_nom = nom;
        this.te_comment = comment;
    }

    /**
     *
     * @return
     */
    public String getTe_code() {
        return te_code;
    }

    /**
     *
     * @return
     */
    public String getTe_nom() {
        return te_nom;
    }

    /**
     *
     * @return
     */
    public String getTe_comment() {
        return te_comment;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @param te_code
     */
    public void setTe_code(String te_code) {
        this.te_code = te_code;
    }

    /**
     *
     * @param te_nom
     */
    public void setTe_nom(String te_nom) {
        this.te_nom = te_nom;
    }

    /**
     *
     * @param te_comment
     */
    public void setTe_comment(String te_comment) {
        this.te_comment = te_comment;
    }
     
     
     
}
