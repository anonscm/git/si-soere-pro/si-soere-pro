/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typeculture;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeCulture.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_TPC}))
public class TypeCulture implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.TYPECULTURE_ID; // tpc_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.TYPECULTURE_TABLE_NAME; // type_culture
    
    @Id
    @Column(name = TypeCulture.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_TPC, length = 150)
    private String libelle;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_TPC, length = 150)
    private String code;
    
    /**
     *
     */
    public TypeCulture() 
    {
        super();
    }
    
    /**
	 * @param libelle
	 * @param code
	 */
	public TypeCulture(String libelle, String code) {
		super();
		this.libelle = libelle;
		this.code = code;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		return libelle;
	}

	/**
	 * @param libelle the libelle to set
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	
}
