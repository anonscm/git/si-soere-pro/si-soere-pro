/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.categorievariable;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = CategorieVariable.NAME_ENTITY_JPA)
public class CategorieVariable implements Serializable {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "categorievariable";

    /**
     *
     */
    public static final String JPA_ID = "cvariable_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "cvariable_code";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE = "cvariable_mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "cvariable_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_DEFINITION = "cvariable_definition";

    /**
     *
     */
    public static final String ATTRIBUTE_PARENT_NAME = "gro_gro_id";

    @Id
    @Column(name = CategorieVariable.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cvariable_id;
    @Column(name = CategorieVariable.JPA_COLUMN_KEY, unique = true, nullable = false, length = 150)
    private String cvariable_code = "";
    @Column(name = CategorieVariable.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String cvariable_nom = "";
    @Column(name = CategorieVariable.JPA_COLUMN_MYCODE, unique = true, nullable = false, length = 150)
    private String cvariable_mycode = "";
    @Column(name = CategorieVariable.JPA_COLUMN_DEFINITION)
    private String cvariable_definition = "";

    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH})
    @JoinColumn(name = ATTRIBUTE_PARENT_NAME, referencedColumnName = JPA_ID, nullable = true)
    private CategorieVariable parent;

    @OneToMany(mappedBy = "parent", cascade = {MERGE, PERSIST, REFRESH})
    @OrderBy(CategorieVariable.JPA_COLUMN_NAME)
    private List<CategorieVariable> enfants = new LinkedList<CategorieVariable>();

    @OneToMany(mappedBy = "categorievariable", cascade = {MERGE, PERSIST, REFRESH})
    @OrderBy(VariablesPRO.ID_JPA)
    private List<VariablesPRO> variables = new LinkedList<VariablesPRO>();

    /**
     *
     * @param cnom
     * @param cdefinition
     * @param ccode
     */
    public CategorieVariable(String cnom, String cdefinition, String ccode) {
        this.cvariable_nom = cnom;
        this.cvariable_definition = cdefinition;
        this.cvariable_code = ccode;
    }

    /**
     *
     */
    public CategorieVariable() {

    }

    /**
     *
     * @return
     */
    public long getCvariable_id() {
        return cvariable_id;
    }

    /**
     *
     * @return
     */
    public String getCvariable_nom() {
        return cvariable_nom;
    }

    /**
     *
     * @return
     */
    public String getCvariable_mycode() {
        return cvariable_mycode;
    }

    /**
     *
     * @return
     */
    public String getCvariable_definition() {
        return cvariable_definition;
    }

    /**
     *
     * @param cvariable_id
     */
    public void setCvariable_id(long cvariable_id) {
        this.cvariable_id = cvariable_id;
    }

    /**
     *
     * @param cvariable_nom
     */
    public void setCvariable_nom(String cvariable_nom) {
        this.cvariable_nom = cvariable_nom;
    }

    /**
     *
     * @param cvariable_mycode
     */
    public void setCvariable_mycode(String cvariable_mycode) {
        this.cvariable_mycode = cvariable_mycode;
    }

    /**
     *
     * @param cvariable_definition
     */
    public void setCvariable_definition(String cvariable_definition) {
        this.cvariable_definition = cvariable_definition;
    }

    /**
     *
     * @return
     */
    public String getCvariable_code() {
        return cvariable_code;
    }

    /**
     *
     * @param cvariable_code
     */
    public void setCvariable_code(String cvariable_code) {
        this.cvariable_code = cvariable_code;
    }

    /**
     *
     * @return
     */
    public CategorieVariable getParent() {
        return parent;
    }

    /**
     *
     * @param parent
     */
    public void setParent(CategorieVariable parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    public List<CategorieVariable> getEnfants() {
        return enfants;
    }

    /**
     *
     * @param enfants
     */
    public void setEnfants(List<CategorieVariable> enfants) {
        this.enfants = enfants;
    }

    /**
     *
     * @return
     */
    public List<VariablesPRO> getVariables() {
        return variables;
    }

    /**
     *
     * @param variables
     */
    public void setVariables(List<VariablesPRO> variables) {
        this.variables = variables;
    }

}
