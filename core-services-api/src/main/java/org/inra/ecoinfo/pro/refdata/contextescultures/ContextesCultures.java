/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contextescultures;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.inra.ecoinfo.pro.refdata.amenagementavantplantation.AmenagementAvantPlantation;
import org.inra.ecoinfo.pro.refdata.cepagevariete.CepageVariete;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.portegreffe.PorteGreffe;
import org.inra.ecoinfo.pro.refdata.typeculture.TypeCulture;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=ContextesCultures.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                      Dispositif.ID_JPA,
                                                                      TypeCulture.ID_JPA,
                                                                      ContextesCultures.JPA_COLUMN_DENSITE,
                                                                      ContextesCultures.JPA_COLUMN_ANNEE_DEBUT
                                                                  
       
}))
public class ContextesCultures implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "contexteculture";

    /**
     *
     */
    public static final String JPA_ID = "cp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_DENSITE = "cp_densite";

    /**
     *
     */
    public static final String JPA_COLUMN_EIR = "cp_ecartinterrang";

    /**
     *
     */
    public static final String JPA_COLUMN_EIP ="cp_ecartinterplan";

    /**
     *
     */
    public static final String JPA_COLUMN_ORIENTATION="cp_orientation";

    /**
     *
     */
    public static final String JPA_COLUMN_ANNEE_DEBUT="cc_anneedebut";

    /**
     *
     */
    public static final String JPA_COLUMN_ANNEE_FIN="cc_anneefin";

    /**
     *
     */
    public static final String JPA_COLUMN_CLONE = "cc_clone";

    /**
     *
     */
    public static final String JPA_COLUMN_ER = "cc_enherbementrang";

    /**
     *
     */
    public static final String JPA_COLUMN_ENIR = "cc_enherbementinterrang";
    
    @Id
    @Column(name = ContextesCultures.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cp_id;
    @Column(name = ContextesCultures.JPA_COLUMN_DENSITE, nullable = false, unique = false)
    private Integer cp_densite = 0;
    @Column(name = ContextesCultures.JPA_COLUMN_EIR, nullable = false, unique = false)
    private Double cp_ecartinterrang = 0.0;
    @Column(name = ContextesCultures.JPA_COLUMN_EIP, nullable = false, unique = false)
    private Double cp_ecartinterplan = 0.0;
    @Column(name = ContextesCultures.JPA_COLUMN_ORIENTATION, nullable = false, unique = false)
    private String cp_orientation = "";
     @Column(name = ContextesCultures.JPA_COLUMN_ANNEE_DEBUT, nullable = false, unique = false,length = 4)
    private Integer cc_anneedebut=0;
     @Column(name = ContextesCultures.JPA_COLUMN_ANNEE_FIN, nullable = true, unique = false,length = 4)
    private Integer cc_anneefin;
      @Column(name = ContextesCultures.JPA_COLUMN_CLONE, nullable = true, unique = false)
    private String cc_clone = "";
    @Column(name = ContextesCultures.JPA_COLUMN_ER, nullable = false, unique = false)
    private String cc_enherbementrang =null;
    @Column(name = ContextesCultures.JPA_COLUMN_ENIR, nullable = false, unique = false)
    private String cc_enherbementinterrang = null;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeCulture.ID_JPA, nullable = false, unique = false)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TypeCulture typeculture; 
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Dispositif dispositif;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = PorteGreffe.JPA_ID, nullable = true)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PorteGreffe portegreffe;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = CepageVariete.JPA_ID, nullable = true)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private CepageVariete cepagevariete;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = AmenagementAvantPlantation.JPA_ID, nullable = true)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private AmenagementAvantPlantation amenagementavantplantation;
    
    /**
     *
     */
    public ContextesCultures(){
        super();
    }
    
    /**
     *
     * @param densite
     * @param eir
     * @param eip
     * @param orientation
     * @param dispositif
     * @param typeculture
     * @param and
     * @param anf
     * @param ehr
     * @param ehir
     * @param clone
     * @param aap
     * @param cepage
     * @param porte
     */
    public ContextesCultures(Integer densite,Double eir,Double eip,String orientation,Dispositif dispositif,TypeCulture typeculture,
                               Integer and,Integer anf,String ehr,String ehir,String clone,
                                AmenagementAvantPlantation aap, CepageVariete cepage,PorteGreffe porte){
        this.cp_densite = densite;
        this.cp_ecartinterrang = eir;
        this.cp_ecartinterplan = eip;
        this.cp_orientation = orientation;
        this.dispositif = dispositif;
        this.typeculture = typeculture;
        this.amenagementavantplantation = aap;
        this.cepagevariete = cepage;
        this.portegreffe = porte;
        this.cc_anneedebut = and;
        this.cc_anneefin = anf;
        this.cc_clone = clone;
        this.cc_enherbementrang = ehr;
        this.cc_enherbementinterrang = ehir;
    }

    /**
     *
     * @return
     */
    public long getCp_id() {
        return cp_id;
    }

    /**
     *
     * @return
     */
    public Integer getCp_densite() {
        return cp_densite;
    }

    /**
     *
     * @return
     */
    public Double getCp_ecartinterrang() {
        return cp_ecartinterrang;
    }

    /**
     *
     * @return
     */
    public Double getCp_ecartinterplan() {
        return cp_ecartinterplan;
    }

    /**
     *
     * @return
     */
    public String getCp_orientation() {
        return cp_orientation;
    }

    /**
     *
     * @return
     */
    public TypeCulture getTypeculture() {
        return typeculture;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @param cp_id
     */
    public void setCp_id(long cp_id) {
        this.cp_id = cp_id;
    }

    /**
     *
     * @param cp_densite
     */
    public void setCp_densite(Integer cp_densite) {
        this.cp_densite = cp_densite;
    }

    /**
     *
     * @param cp_ecartinterrang
     */
    public void setCp_ecartinterrang(Double cp_ecartinterrang) {
        this.cp_ecartinterrang = cp_ecartinterrang;
    }

    /**
     *
     * @param cp_ecartinterplan
     */
    public void setCp_ecartinterplan(Double cp_ecartinterplan) {
        this.cp_ecartinterplan = cp_ecartinterplan;
    }

    /**
     *
     * @param cp_orientation
     */
    public void setCp_orientation(String cp_orientation) {
        this.cp_orientation = cp_orientation;
    }

    /**
     *
     * @param typeculture
     */
    public void setTypeculture(TypeCulture typeculture) {
        this.typeculture = typeculture;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @return
     */
    public PorteGreffe getPortegreffe() {
        return portegreffe;
    }

    /**
     *
     * @return
     */
    public CepageVariete getCepagevariete() {
        return cepagevariete;
    }

    /**
     *
     * @return
     */
    public AmenagementAvantPlantation getAmenagementavantplantation() {
        return amenagementavantplantation;
    }

    /**
     *
     * @param portegreffe
     */
    public void setPortegreffe(PorteGreffe portegreffe) {
        this.portegreffe = portegreffe;
    }

    /**
     *
     * @param cepagevariete
     */
    public void setCepagevariete(CepageVariete cepagevariete) {
        this.cepagevariete = cepagevariete;
    }

    /**
     *
     * @param amenagementavantplantation
     */
    public void setAmenagementavantplantation(AmenagementAvantPlantation amenagementavantplantation) {
        this.amenagementavantplantation = amenagementavantplantation;
    }

    /**
     *
     * @return
     */
    public Integer getCc_anneedebut() {
        return cc_anneedebut;
    }

    /**
     *
     * @return
     */
    public Integer getCc_anneefin() {
        return cc_anneefin;
    }

    /**
     *
     * @return
     */
    public String getCc_clone() {
        return cc_clone;
    }

    /**
     *
     * @return
     */
    public String getCc_enherbementrang() {
        return cc_enherbementrang;
    }

    /**
     *
     * @return
     */
    public String getCc_enherbementinterrang() {
        return cc_enherbementinterrang;
    }

    /**
     *
     * @param cc_anneedebut
     */
    public void setCc_anneedebut(Integer cc_anneedebut) {
        this.cc_anneedebut = cc_anneedebut;
    }

    /**
     *
     * @param cc_anneefin
     */
    public void setCc_anneefin(Integer cc_anneefin) {
        this.cc_anneefin = cc_anneefin;
    }

    /**
     *
     * @param cc_clone
     */
    public void setCc_clone(String cc_clone) {
        this.cc_clone = cc_clone;
    }

    /**
     *
     * @param cc_enherbementrang
     */
    public void setCc_enherbementrang(String cc_enherbementrang) {
        this.cc_enherbementrang = cc_enherbementrang;
    }

    /**
     *
     * @param cc_enherbementinterrang
     */
    public void setCc_enherbementinterrang(String cc_enherbementinterrang) {
        this.cc_enherbementinterrang = cc_enherbementinterrang;
    }
    
    
    
}
