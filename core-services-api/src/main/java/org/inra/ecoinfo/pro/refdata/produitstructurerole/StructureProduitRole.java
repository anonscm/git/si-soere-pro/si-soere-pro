package org.inra.ecoinfo.pro.refdata.produitstructurerole;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.rolestructure.RoleStructure;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames= { Produits.ID_JPA,
 * Structure.ID_JPA, RoleStructure.ID_JPA
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StructureProduitRole.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    Produits.ID_JPA,
    Structure.ID_JPA,
    RoleStructure.ID_JPA
}))
public class StructureProduitRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "produitstructurerole";

    /**
     *
     */
    public static final String JPA_COLUMN_ID = "spr_id";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @Column(name = StructureProduitRole.JPA_COLUMN_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long spr_id;
    @Column(name = StructureProduitRole.JPA_COLUMN_COMENT, length = 300)
    private String commentaire = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Produits.ID_JPA, nullable = false)
    private Produits produits;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Structure.ID_JPA, nullable = false)
    private Structure structure;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = RoleStructure.ID_JPA, nullable = false)
    private RoleStructure role;

    /**
     *
     */
    public StructureProduitRole() {
        super();
    }

    /**
     *
     * @param produits
     * @param structure
     * @param role
     * @param commentaire
     */
    public StructureProduitRole(Produits produits, Structure structure, RoleStructure role, String commentaire) {
        super();
        this.produits = produits;
        this.role = role;
        this.structure = structure;
        this.commentaire = commentaire;

    }

    /**
     *
     * @return
     */
    public Produits getProduits() {
        return produits;
    }

    /**
     *
     * @return
     */
    public RoleStructure getRole() {
        return role;
    }

    /**
     *
     * @return
     */
    public long getSpr_id() {
        return spr_id;
    }

    /**
     *
     * @return
     */
    public Structure getStructure() {
        return structure;
    }

    /**
     *
     * @param produits
     */
    public void setProduits(Produits produits) {
        this.produits = produits;
    }

    /**
     *
     * @param role
     */
    public void setRole(RoleStructure role) {
        this.role = role;
    }

    /**
     *
     * @param spr_id
     */
    public void setSpr_id(long spr_id) {
        this.spr_id = spr_id;
    }

    /**
     *
     * @param structure
     */
    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
