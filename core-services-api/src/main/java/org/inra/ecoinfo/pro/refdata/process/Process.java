
package org.inra.ecoinfo.pro.refdata.process;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Process.NAME_ENTITY_JPA)
public class Process implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "process";

    /**
     *
     */
    public static final String JPA_COLUM_ID = "process_id";

    /**
     *
     */
    public static final String JPA_COLUM_CODE = "process_code";

    /**
     *
     */
    public static final String JPA_COLUMN_INTITULE = "process_intitule";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT ="commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Process.JPA_COLUM_ID, unique = true, updatable = false, nullable = false)
    private long process_id = 0;
    @Column(name = Process.JPA_COLUM_CODE, nullable = false, unique = true)
    private String process_code = "";
    @Column(name = Process.JPA_COLUMN_INTITULE, nullable = false, length = 50, unique = true)
    private String process_intitule = "";
    @Column(name=Process.JPA_COLUMN_COMENT)
    private String commentaire="";

    /**
     *
     */
    public Process() {
    }

    /**
     *
     * @param code
     * @param intitule
     * @param commentaire
     */
    public Process(String code, String intitule,String commentaire) {
        super();
        this.process_code = code;
        this.process_intitule = intitule;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getProcess_code() {
        return process_code;
    }

    /**
     *
     * @return
     */
    public long getProcess_id() {
        return process_id;
    }

    /**
     *
     * @return
     */
    public String getProcess_intitule() {
        return process_intitule;
    }

    /**
     *
     * @param process_code
     */
    public void setProcess_code(String process_code) {
        this.process_code = process_code;
    }

    /**
     *
     * @param process_id
     */
    public void setProcess_id(long process_id) {
        this.process_id = process_id;
    }

    /**
     *
     * @param process_intitule
     */
    public void setProcess_intitule(String process_intitule) {
        this.process_intitule = process_intitule;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
