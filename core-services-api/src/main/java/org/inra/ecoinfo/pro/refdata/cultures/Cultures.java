/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cultures;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.especeplante.EspecePlante;
import org.inra.ecoinfo.pro.refdata.typeculture.TypeCulture;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = Cultures.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    Cultures.JPA_COLUMN_NAME
}))
public class Cultures {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "cultures";

    /**
     *
     */
    public static final String JPA_ID = "culture_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE = "mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";
    // public static final String JPA_COLUMN_ESPECE="espece";

    /**
     *
     */
    public static final String JPA_COLUMN_ESPECE2 = "espece2";

    /**
     *
     */
    public static final String JPA_COLUMN_ESPECE3 = "espece3";

    /**
     *
     */
    public static final String JPA_COLUMN_ESPECE4 = "espece4";

    /**
     *
     */
    public static final String JPA_COLUMN_ESPECE5 = "espece5";

    /**
     *
     */
    public static final String JPA_COLUMN_ESPECE6 = "espece6";

    /**
     *
     */
    public static final String JPA_COLUMN_PERIODE_SEMIS = "periodesemis";

    /**
     *
     */
    public static final String JPA_COLUMN_PERIODE_RECOLTE = "perioderecolte";

    /**
     *
     */
    public static final String JPA_COLUMN_DUREE_VEGETATION = "dureevegetation";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "commentaire";

    @Id
    @Column(name = Cultures.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long culture_id;
    @Column(name = Cultures.JPA_COLUMN_KEY, nullable = false, unique = false)
    private String code = "";
    @Column(name = Cultures.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String nom = "";
    @Column(name = Cultures.JPA_COLUMN_MYCODE, nullable = false, unique = false)
    private String mycode = "";
    @Column(name = Cultures.JPA_COLUMN_ESPECE2, nullable = true, unique = false)
    private String espece2 = "";
    @Column(name = Cultures.JPA_COLUMN_ESPECE3, nullable = true, unique = false)
    private String espece3 = "";
    @Column(name = Cultures.JPA_COLUMN_ESPECE4, nullable = true, unique = false)
    private String espece4 = "";
    @Column(name = Cultures.JPA_COLUMN_ESPECE5, nullable = true, unique = false)
    private String espece5 = "";
    @Column(name = Cultures.JPA_COLUMN_ESPECE6, nullable = true, unique = false)
    private String espece6 = "";
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeCulture.ID_JPA, nullable = false, unique = false)
    private TypeCulture typeculture;
    @Column(name = Cultures.JPA_COLUMN_PERIODE_SEMIS)
    private String periodesemis = "";
    @Column(name = Cultures.JPA_COLUMN_PERIODE_RECOLTE)
    private String perioderecolte = "";
    @Column(name = Cultures.JPA_COLUMN_DUREE_VEGETATION)
    private String dureevegetation = "";
    @Column(name = Cultures.JPA_COLUMN_COMMENT)
    private String commentaire = "";

    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = EspecePlante.ID_JPA, nullable = false)
    private EspecePlante especePlante;

    /**
     *
     */
    public Cultures() {
        super();
    }

    /**
     *
     * @param code
     * @param nom
     * @param especePlante
     * @param espece2
     * @param espece3
     * @param espece4
     * @param espece5
     * @param espece6
     * @param typeculture
     * @param semis
     * @param recolte
     * @param vegetation
     * @param commentaire
     */
    public Cultures(String code, String nom, EspecePlante especePlante, String espece2, String espece3, String espece4, String espece5, String espece6,
            TypeCulture typeculture, String semis, String recolte, String vegetation, String commentaire) {
        this.code = code;
        this.nom = nom;
        this.especePlante = especePlante;
        this.espece2 = espece2;
        this.espece3 = espece3;
        this.espece4 = espece4;
        this.espece5 = espece5;
        this.espece6 = espece6;
        this.typeculture = typeculture;
        this.periodesemis = semis;
        this.perioderecolte = recolte;
        this.dureevegetation = vegetation;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getCulture_id() {
        return culture_id;
    }

    /**
     *
     * @param culture_id
     */
    public void setCulture_id(long culture_id) {
        this.culture_id = culture_id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getMycode() {
        return mycode;
    }

    /**
     *
     * @param mycode
     */
    public void setMycode(String mycode) {
        this.mycode = mycode;
    }

    /**
     *
     * @return
     */
    public TypeCulture getTypeculture() {
        return typeculture;
    }

    /**
     *
     * @param typeculture
     */
    public void setTypeculture(TypeCulture typeculture) {
        this.typeculture = typeculture;
    }

    /**
     *
     * @return
     */
    public String getPeriodesemis() {
        return periodesemis;
    }

    /**
     *
     * @param periodesemis
     */
    public void setPeriodesemis(String periodesemis) {
        this.periodesemis = periodesemis;
    }

    /**
     *
     * @return
     */
    public String getPerioderecolte() {
        return perioderecolte;
    }

    /**
     *
     * @param perioderecolte
     */
    public void setPerioderecolte(String perioderecolte) {
        this.perioderecolte = perioderecolte;
    }

    /**
     *
     * @return
     */
    public String getDureevegetation() {
        return dureevegetation;
    }

    /**
     *
     * @param dureevegetation
     */
    public void setDureevegetation(String dureevegetation) {
        this.dureevegetation = dureevegetation;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getEspece2() {
        return espece2;
    }

    /**
     *
     * @param espece2
     */
    public void setEspece2(String espece2) {
        this.espece2 = espece2;
    }

    /**
     *
     * @return
     */
    public String getEspece3() {
        return espece3;
    }

    /**
     *
     * @param espece3
     */
    public void setEspece3(String espece3) {
        this.espece3 = espece3;
    }

    /**
     *
     * @return
     */
    public String getEspece4() {
        return espece4;
    }

    /**
     *
     * @param espece4
     */
    public void setEspece4(String espece4) {
        this.espece4 = espece4;
    }

    /**
     *
     * @return
     */
    public String getEspece5() {
        return espece5;
    }

    /**
     *
     * @param espece5
     */
    public void setEspece5(String espece5) {
        this.espece5 = espece5;
    }

    /**
     *
     * @return
     */
    public String getEspece6() {
        return espece6;
    }

    /**
     *
     * @param espece6
     */
    public void setEspece6(String espece6) {
        this.espece6 = espece6;
    }

    /**
     *
     * @return
     */
    public EspecePlante getEspecePlante() {
        return especePlante;
    }

    /**
     *
     * @param especePlante
     */
    public void setEspecePlante(EspecePlante especePlante) {
        this.especePlante = especePlante;
    }

}
