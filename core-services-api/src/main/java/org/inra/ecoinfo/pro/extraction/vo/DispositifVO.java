/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.vo;

import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author adiankha
 */
public class DispositifVO {
    private Long id;
    private String code;
    private String nom;

    /**
     *
     */
    public DispositifVO() {
    }

    /**
     *
     * @param dispositif
     */
    public DispositifVO(Dispositif dispositif) {
        this.id = dispositif.getId();
        this.code = dispositif.getCode();
        this.nom = dispositif.getName();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

   
   
    
    
    
}