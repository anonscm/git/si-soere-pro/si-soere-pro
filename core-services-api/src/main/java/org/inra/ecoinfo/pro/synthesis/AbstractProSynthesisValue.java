/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis;

import com.google.common.base.Strings;
import java.time.LocalDateTime;
import javax.persistence.MappedSuperclass;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author vjkoyao
 */
@MappedSuperclass
public abstract class AbstractProSynthesisValue extends GenericSynthesisValue {
    
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public AbstractProSynthesisValue() {
        super();
    }

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueString
     * @param valueFloat
     * @param idNode
     */
    public AbstractProSynthesisValue(LocalDateTime date, String site, String variable, Double value, String valueString, Long idNode) {
        super(date, site, variable, value==null?null:value.floatValue(), valueString==null?value.toString():valueString,  idNode, Strings.isNullOrEmpty(valueString));
    }
}
