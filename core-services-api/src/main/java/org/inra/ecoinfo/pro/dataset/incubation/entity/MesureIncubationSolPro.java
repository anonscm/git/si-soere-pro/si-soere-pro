/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureIncubationSolPro.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    EchantillonsSol.ID_JPA,
    EchantillonsProduit.ID_JPA,
    MesureIncubationSol.COLUMN_JPA_LocalDate_PREL_SOL,
    MesureIncubationSolPro.COLUMN_JPA_LocalDate_PREL_PRO,
    MesureIncubationSol.COLUMN_ORDRE_MANIP,
    MesureIncubationSol.COLUMN_NUM_REP,
    MesureIncubationSol.COLUMN_JOUR_INCUB}),
        indexes = {
            @Index(name = "mesure_incub_sol_pro_idx", columnList = MesureIncubationSolPro.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesureIncubation.ATTRIBUTE_JPA_ID, referencedColumnName = MesureIncubationSolPro.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesureIncubation.ATTRIBUTE_JPA_ID, column = @Column(name = MesureIncubationSolPro.ID_JPA))})

public class MesureIncubationSolPro extends MesureIncubation<MesureIncubationSolPro, ValeurIncubationSolPro> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesureincubationsolpro";

    /**
     *
     */
    public static final String ID_JPA = "misp_id";
    /**
     *
     */
    public static final String COLUMN_COND_INC = "condition_incub";

    /**
     *
     */
    public static final String COLUMN_CODE_INT_LABO = "code_interne_labo";
    /**
     *
     */
    public static final String COLUMN_N_MINERAL = "n_mineral";

    /**
     *
     */
    public static final String COLUMN_CODE_PRO = "code_produit";

    /**
     *
     */
    public static final String COLUMN_MASSE_PRO = "masse_produit";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate_PREL_PRO = "LocalDate_prel_pro";
    static final String RESOURCE_PATH = "%s/%s";

    @Column(name = MesureIncubationSolPro.COLUMN_COND_INC, nullable = false)
    private String condition_incub = "";

//    @Column(name = MesureIncubationSolPro.COLUMN_N_MINERAL, nullable = false)
//    private String n_mineral = "";
    @Column(name = MesureIncubationSolPro.COLUMN_MASSE_PRO, nullable = false)
    private float masse_de_pro = 0;
    @Column(name = MesureIncubationSolPro.COLUMN_JPA_LocalDate_PREL_PRO, nullable = false)
    private LocalDate LocalDate_prel_pro;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = EchantillonsProduit.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private EchantillonsProduit echantillonsProduit;

    @OneToMany(mappedBy = "mesureIncubationSolPro", cascade = ALL)
    List<ValeurIncubationSolPro> valeurIncubationSolPro = new LinkedList();

    /**
     *
     */
    public MesureIncubationSolPro() {
        super();
    }

    /**
     *
     * @param LocalDate_prel_sol
     * @param echantillonsSol
     * @param ordre_manip
     * @param masse_de_sol
     * @param LocalDate_prel_pro
     * @param echantillonsProduit
     * @param masse_de_pro
     * @param condition_incub
     * @param LocalDate_debut_incub
     * @param numero_rep_analyse
     * @param jours_incub
     * @param ligneFichierEchange
     * @param versionfile
     */
    public MesureIncubationSolPro(LocalDate LocalDate_prel_sol, EchantillonsSol echantillonsSol, int ordre_manip, float masse_de_sol, LocalDate LocalDate_prel_pro, EchantillonsProduit echantillonsProduit, float masse_de_pro,
            String condition_incub, LocalDate LocalDate_debut_incub, int numero_rep_analyse, int jours_incub, Long ligneFichierEchange, VersionFile versionfile) {

        super(LocalDate_prel_sol, LocalDate_debut_incub, numero_rep_analyse, masse_de_sol, jours_incub, ordre_manip, ligneFichierEchange, versionfile, echantillonsSol);
        this.LocalDate_prel_pro = LocalDate_prel_pro;
        this.echantillonsProduit = echantillonsProduit;
        this.masse_de_pro = masse_de_pro;
        this.condition_incub = condition_incub;

    }

    /**
     *
     * @return
     */
    public String getCondition_incub() {
        return condition_incub;
    }

    /**
     *
     * @param condition_incub
     */
    public void setCondition_incub(String condition_incub) {
        this.condition_incub = condition_incub;
    }

    /**
     *
     * @return
     */
    public float getMasse_de_pro() {
        return masse_de_pro;
    }

    /**
     *
     * @param masse_de_pro
     */
    public void setMasse_de_pro(float masse_de_pro) {
        this.masse_de_pro = masse_de_pro;
    }

    /**
     *
     * @return
     */
    public List<ValeurIncubationSolPro> getValeurIncubationSolPro() {
        return valeurIncubationSolPro;
    }

    /**
     *
     * @param valeurIncubationSolPro
     */
    public void setValeurIncubationSolPro(List<ValeurIncubationSolPro> valeurIncubationSolPro) {
        this.valeurIncubationSolPro = valeurIncubationSolPro;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDate_prel_pro() {
        return LocalDate_prel_pro;
    }

    /**
     *
     * @param LocalDate_prel_pro
     */
    public void setLocalDate_prel_pro(LocalDate LocalDate_prel_pro) {
        this.LocalDate_prel_pro = LocalDate_prel_pro;
    }

    /**
     *
     * @return
     */
    public EchantillonsProduit getEchantillonsProduit() {
        return echantillonsProduit;
    }

    /**
     *
     * @param echantillonsProduit
     */
    public void setEchantillonsProduit(EchantillonsProduit echantillonsProduit) {
        this.echantillonsProduit = echantillonsProduit;
    }

}
