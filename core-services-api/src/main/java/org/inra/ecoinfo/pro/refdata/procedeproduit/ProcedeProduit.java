package org.inra.ecoinfo.pro.refdata.procedeproduit;
import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProcedeProduit.NAME_ENTITY_JAP)
public class ProcedeProduit implements Serializable {
    private static final long serialVersionUID = 1L;
    static final String NAME_ENTITY_JAP = "procede_produit";
    static final String JPA_ID = "pp_id";
    static final String JPA_COLUMN_NAME_DAY = "pp_date";
    static final String JPA_COLUMN_NAME_ORDER = "pp_ordre";
    static final String JPA_COLUMN_NAME_DUREE = "pp_duree";
    static final String JPA_COLUMN_NAME_UNITE = "pp_unite";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ProcedeProduit.JPA_ID, nullable = false, updatable = false, unique = true)
    private final long pp_id = 0;
    @Column(name = ProcedeProduit.JPA_COLUMN_NAME_DAY, nullable = false)
    private LocalDate pp_date = null;
    @Column(name = ProcedeProduit.JPA_COLUMN_NAME_ORDER)
    private int pp_ordre = 0;
    @Column(name = ProcedeProduit.JPA_COLUMN_NAME_DUREE)
    private String pp_duree = "";
    @Column(name = ProcedeProduit.JPA_COLUMN_NAME_UNITE)
    private String pp_unite = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Produits.ID_JPA, nullable = false)
    private Produits produits;

    /**
     *
     */
    public ProcedeProduit() {
        super();
    }

    /**
     *
     * @param produits
     * @param date
     * @param ordre
     * @param duree
     * @param unite
     */
    public ProcedeProduit(Produits produits, LocalDate date, int ordre, String duree, String unite) {
        super();

        this.produits = produits;
        this.pp_date = date;
        this.pp_ordre = ordre;
        this.pp_duree = duree;
        this.pp_unite = unite;
    }

    /**
     *
     * @return
     */
    public LocalDate getPp_date() {
        return pp_date;
    }

    /**
     *
     * @return
     */
    public String getPp_duree() {
        return pp_duree;
    }

    /**
     *
     * @return
     */
    public long getPp_id() {
        return pp_id;
    }

    /**
     *
     * @return
     */
    public int getPp_ordre() {
        return pp_ordre;
    }

    /**
     *
     * @return
     */
    public String getPp_unite() {
        return pp_unite;
    }

    /**
     *
     * @return
     */
    public Produits getProduits() {
        return produits;
    }

    /**
     *
     * @param pp_date
     */
    public void setPp_date(LocalDate pp_date) {
        this.pp_date = pp_date;
    }

    /**
     *
     * @param pp_duree
     */
    public void setPp_duree(String pp_duree) {
        this.pp_duree = pp_duree;
    }

    /**
     *
     * @param pp_ordre
     */
    public void setPp_ordre(int pp_ordre) {
        this.pp_ordre = pp_ordre;
    }

    /**
     *
     * @param pp_unite
     */
    public void setPp_unite(String pp_unite) {
        this.pp_unite = pp_unite;
    }

    /**
     *
     * @param produits
     */
    public void setProduits(Produits produits) {
        this.produits = produits;
    }

}
