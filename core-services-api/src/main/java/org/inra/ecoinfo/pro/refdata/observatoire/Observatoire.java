
package org.inra.ecoinfo.pro.refdata.observatoire;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Observatoire.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_OBS}))
public class Observatoire implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.OBSERVATOIRE_ID; // obs_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.OBSERVATOIRE_TABLE_NAME; // observatoire
    
    @Id
    @Column(name = Observatoire.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_OBS, length = 100)
    private String nom;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_OBS, length = 100)
    private String code;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_DEFINITION_OBS, length = 500)
    private String definition;
    
    /**
     *
     */
    public Observatoire() 
    {
        super();
    }
    
    /**
     *
     * @param nom
     * @param definition
     */
    public Observatoire(String nom, String definition) 
    {
        super();
        this.nom = nom;
        this.definition = definition;
    }
    
    /**
	 * @param nom
	 * @param code
	 * @param definition
	 */
	public Observatoire(String nom, String code, String definition) {
		super();
		this.nom = nom;
		this.code = code;
		this.definition = definition;
	}

    /**
     *
     * @return
     */
    public String getDefinition() 
    {
        return definition;
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @param definition
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}
