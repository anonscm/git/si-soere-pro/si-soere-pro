/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionfiliere;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Precisionfiliere.NAME_ENTITY_JPA)
public class Precisionfiliere implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "precisionfiliere";

    /**
     *
     */
    public static final String JPA_ID = "pfiliere_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "pfiliere_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "pfiliere_nom";
    
    @Id
    @Column(name = Precisionfiliere.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pfiliere_id;
    @Column(name = Precisionfiliere.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String pfiliere_code = "";
    @Column(name = Precisionfiliere.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String pfiliere_nom = "";
    
    /**
     *
     */
    public Precisionfiliere(){
        super();
    }
    
    /**
     *
     * @param nom
     */
    public Precisionfiliere(String nom){
        this.pfiliere_nom = nom;
    }

    /**
     *
     * @return
     */
    public long getPfiliere_id() {
        return pfiliere_id;
    }

    /**
     *
     * @return
     */
    public String getPfiliere_code() {
        return pfiliere_code;
    }

    /**
     *
     * @return
     */
    public String getPfiliere_nom() {
        return pfiliere_nom;
    }

    /**
     *
     * @param pfiliere_id
     */
    public void setPfiliere_id(long pfiliere_id) {
        this.pfiliere_id = pfiliere_id;
    }

    /**
     *
     * @param pfiliere_code
     */
    public void setPfiliere_code(String pfiliere_code) {
        this.pfiliere_code = pfiliere_code;
    }

    /**
     *
     * @param pfiliere_nom
     */
    public void setPfiliere_nom(String pfiliere_nom) {
        this.pfiliere_nom = pfiliere_nom;
    }
    
}
