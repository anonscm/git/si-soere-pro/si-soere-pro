package org.inra.ecoinfo.pro.refdata.produitetapecaracteristique;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.CaracteristiqueEtapes;
import org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape.MethodeProcedeEtapes;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 *           EtapeCaracterisitique.JPA_COLUMN_VALUE, 
 *           EtapeCaracterisitique.JPA_COLUMN_UNITE, 
 *           CaracteristiqueEtapes.ID_JPA, 
 *           MethodeProcedeEtapes.JPA_ID
 * @author ptcherniati
 */
@Entity
@Table(name = EtapeCaracterisitique.NAME_ENTITY_JAP, 
        uniqueConstraints = @UniqueConstraint(columnNames = {
            EtapeCaracterisitique.JPA_COLUMN_VALUE, 
            EtapeCaracterisitique.JPA_COLUMN_UNITE, 
            CaracteristiqueEtapes.ID_JPA, 
            MethodeProcedeEtapes.JPA_ID
        }))
public class EtapeCaracterisitique implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JAP = "produitetapecaracteristique";

    /**
     *
     */
    public static final String JPA_ID = "ec_id";

    /**
     *
     */
    public static final String JPA_COLUMN_VALUE = "ece_valeur";

    /**
     *
     */
    public static final String JPA_COLUMN_UNITE = "ece_unite";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = EtapeCaracterisitique.JPA_ID, nullable = false, updatable = false)
    private long ec_id = 0;
    @Column(name = EtapeCaracterisitique.JPA_COLUMN_VALUE, nullable = false)
    private int ece_valeur = 0;
    @Column(name = EtapeCaracterisitique.JPA_COLUMN_UNITE)
    private String ece_unite = "";
     @Column(name=MethodeProcedeEtapes.JPA_COLUMN_COMENT,length = 300)
    private String commentaire ="";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodeProcedeEtapes.JPA_ID, nullable = false)
    private MethodeProcedeEtapes methodeprocedeetapes;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = CaracteristiqueEtapes.ID_JPA, nullable = false)
    private CaracteristiqueEtapes caracteristiqueetapes;

    /**
     *
     */
    public EtapeCaracterisitique() {
        super();
    }

    /**
     *
     * @param mpe
     * @param caracteristiqueetapes
     * @param valeur
     * @param unite
     * @param commentaire
     */
    public EtapeCaracterisitique(MethodeProcedeEtapes mpe, CaracteristiqueEtapes caracteristiqueetapes, int valeur, String unite,String commentaire) {
        this.methodeprocedeetapes = mpe;
        this.caracteristiqueetapes = caracteristiqueetapes;
        this.ece_valeur = valeur;
        this.ece_unite = unite;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public CaracteristiqueEtapes getCaracteristiqueetapes() {
        return caracteristiqueetapes;
    }

    /**
     *
     * @return
     */
    public long getEc_id() {
        return ec_id;
    }

    /**
     *
     * @return
     */
    public String getEce_unite() {
        return ece_unite;
    }

    /**
     *
     * @return
     */
    public int getEce_valeur() {
        return ece_valeur;
    }

    /**
     *
     * @return
     */
    public MethodeProcedeEtapes getMethodeprocedeetapes() {
        return methodeprocedeetapes;
    }

    /**
     *
     * @param caracteristiqueetapes
     */
    public void setCaracteristiqueetapes(CaracteristiqueEtapes caracteristiqueetapes) {
        this.caracteristiqueetapes = caracteristiqueetapes;
    }

    /**
     *
     * @param ec_id
     */
    public void setEc_id(long ec_id) {
        this.ec_id = ec_id;
    }

    /**
     *
     * @param ece_unite
     */
    public void setEce_unite(String ece_unite) {
        this.ece_unite = ece_unite;
    }

    /**
     *
     * @param ece_valeur
     */
    public void setEce_valeur(int ece_valeur) {
        this.ece_valeur = ece_valeur;
    }

    /**
     *
     * @param methodeprocedeetapes
     */
    public void setMethodeprocedeetapes(MethodeProcedeEtapes methodeprocedeetapes) {
        this.methodeprocedeetapes = methodeprocedeetapes;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
