/**
 *
 */
package org.inra.ecoinfo.pro.refdata.thematiqueetudiee;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ThematiqueEtudiee.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_THEMEETUDIEE}))
public class ThematiqueEtudiee implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.THEMEETUDIEE_ID; // the_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.THEMEETUDIEE_TABLE_NAME; // thematique_etudiee
    @Id
    @Column(name = ThematiqueEtudiee.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_THEMEETUDIEE, length = 200)
    private String libelle;

    /**
     *
     */
    public ThematiqueEtudiee() {
        super();
    }

    /**
     *
     * @param libelle
     */
    public ThematiqueEtudiee(String libelle) {
        super();
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
