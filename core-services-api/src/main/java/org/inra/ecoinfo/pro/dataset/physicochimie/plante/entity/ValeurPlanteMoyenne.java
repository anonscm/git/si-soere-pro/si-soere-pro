/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = ValeurPlanteMoyenne.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    MesurePlanteMoyenne.ID_JPA,
    RealNode.ID_JPA,
    ValeurPlanteMoyenne.ATTRIBUTE_JPA_ECARTTYPE}
),
        indexes = {
            @Index(name = "valeur_plante_moy_mesure_idx", columnList = MesurePlanteMoyenne.ID_JPA),
            @Index(name = "valeur_plante_moy_variable", columnList = RealNode.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = ValeurPhysicoChimie.ID_JPA, referencedColumnName = ValeurPlanteMoyenne.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurPhysicoChimie.ID_JPA, column = @Column(name = ValeurPlanteMoyenne.ID_JPA))})
public class ValeurPlanteMoyenne extends ValeurPhysicoChimie<MesurePlanteMoyenne, ValeurPlanteMoyenne> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurplantemoyenne";

    /**
     *
     */
    public static final String ID_JPA = "vm_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ECARTTYPE = "ecarttype";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    @Column(name = ValeurPlanteMoyenne.ATTRIBUTE_JPA_ECARTTYPE, nullable = false)
    private float ecarttype;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesurePlanteMoyenne.class)
    @JoinColumn(name = MesurePlanteMoyenne.ID_JPA, referencedColumnName = MesurePlanteMoyenne.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesurePlanteMoyenne mesurePlanteMoyenne;

    /**
     *
     */
    public ValeurPlanteMoyenne() {
        super();
    }

    /**
     *
     * @param valeur
     * @param ecarttype
     * @param mesuremoyenne
     * @param realNode
     */
    public ValeurPlanteMoyenne(Float valeur, float ecarttype, MesurePlanteMoyenne mesuremoyenne, RealNode realNode) {
        super(valeur, realNode);
        this.ecarttype = ecarttype;
        this.mesurePlanteMoyenne = mesuremoyenne;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

    /**
     *
     * @return
     */
    public MesurePlanteMoyenne getMesurePlanteMoyenne() {
        return mesurePlanteMoyenne;
    }

    /**
     *
     * @param mesurePlanteMoyenne
     */
    public void setMesurePlanteMoyenne(MesurePlanteMoyenne mesurePlanteMoyenne) {
        this.mesurePlanteMoyenne = mesurePlanteMoyenne;
    }

}
