
package org.inra.ecoinfo.pro.refdata.etape;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Etapes.NAME_ENTITY_JPA)
public class Etapes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "etape";

    /**
     *
     */
    public static final String ID_JPA = "etape_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "etape_code";

    /**
     *
     */
    public static final String JPA_COLUMN_INTITULE = "etape_intitule";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Etapes.ID_JPA, unique = true, nullable = false, updatable = false)
    private long etape_id = 0;
    @Size(min = 1, max = 80)
    @Column(name = Etapes.JPA_COLUMN_INTITULE, unique = true, nullable = false, length = 80)
    private String etape_intitule = "";
    @Column(name = Etapes.JPA_COLUMN_CODE, unique = true, nullable = false)
    private String etape_code = "";
    @Column(name=Etapes.JPA_COLUMN_COMMENT)
    private String commentaire="";

    /**
     *
     */
    public Etapes() {
    }

    /**
     *
     * @param intitule
     * @param commentaire
     */
    public Etapes( String intitule,String commentaire) {
        this.etape_intitule = intitule;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getEtape_code() {
        return etape_code;
    }

    /**
     *
     * @return
     */
    public long getEtape_id() {
        return etape_id;
    }

    /**
     *
     * @return
     */
    public String getEtape_intitule() {
        return etape_intitule;
    }

    /**
     *
     * @param etape_code
     */
    public void setEtape_code(String etape_code) {
        this.etape_code = etape_code;
    }

    /**
     *
     * @param etape_id
     */
    public void setEtape_id(long etape_id) {
        this.etape_id = etape_id;
    }

    /**
     *
     * @param etape_intitule
     */
    public void setEtape_intitule(String etape_intitule) {
        this.etape_intitule = etape_intitule;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
