/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

/**
 *
 * @author adiankha
 */
public class VariableDescriptor {
     String  name;

   
    boolean hasQualityClass;

    
    String  qualityClassName;

    /**
     *
     * @param name
     */
    public VariableDescriptor(final String name) {
        super();
        this.name = name;
    }

   
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final VariableDescriptor other = (VariableDescriptor) obj;
        if (this.hasQualityClass != other.hasQualityClass) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     *
     * @return
     */
    public String getQualityClassName() {
        return this.qualityClassName;
    }

    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.hasQualityClass ? 1_231 : 1_237);
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        return result;
    }

    /**
     *
     * @return
     */
    public boolean hasQualityClass() {
        return this.hasQualityClass;
    }

    /**
     *
     * @param hasQualityClass
     */
    public void setHasQualityClass(final boolean hasQualityClass) {
        this.hasQualityClass = hasQualityClass;
    }

    /**
     *
     * @param name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     *
     * @param qualityClassName
     */
    public void setQualityClassName(final String qualityClassName) {
        this.qualityClassName = qualityClassName;
    }

   
    @Override
    public String toString() {
        return "VariableDescriptor [name=" + this.name + ", hasQualityClass="
                + this.hasQualityClass + "]";
    }
}
