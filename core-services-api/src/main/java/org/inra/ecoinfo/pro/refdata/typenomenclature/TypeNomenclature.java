/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typenomenclature;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=TypeNomenclature.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
        TypeNomenclature.ID_JPA, 
        GrandTypeProduits.JPA_COLUMN_ID
        

}))
public class TypeNomenclature implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "typenomenclature";

    /**
     *
     */
    public static final String ID_JPA = "tnid";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "tn_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "tn_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT ="commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = TypeNomenclature.ID_JPA, updatable = false)
    private long tnid = 0;
    @Column(name= TypeNomenclature.JPA_COLUMN_KEY)
    private String tn_code = "";
    @Column(name=  TypeNomenclature.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String tn_nom = "";
    @Column(name=TypeNomenclature.JPA_COLUMN_COMENT,nullable = true,unique = false,length = 255)
    private String commentaire="";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = GrandTypeProduits.JPA_COLUMN_ID,nullable=false)
    private GrandTypeProduits grandtypeproduits;
    
    /**
     *
     */
    public TypeNomenclature(){
        super();
    }
    
    /**
     *
     * @param nom
     * @param gtp
     * @param commentaire
     */
    public TypeNomenclature(String nom,GrandTypeProduits gtp,String commentaire){
        this.tn_nom = nom;
        this.grandtypeproduits = gtp;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getTnid() {
        return tnid;
    }

    /**
     *
     * @return
     */
    public String getTn_code() {
        return tn_code;
    }

    /**
     *
     * @return
     */
    public String getTn_nom() {
        return tn_nom;
    }

    /**
     *
     * @return
     */
    public GrandTypeProduits getGrandtypeproduits() {
        return grandtypeproduits;
    }

    /**
     *
     * @param tnid
     */
    public void setTnid(long tnid) {
        this.tnid = tnid;
    }

    /**
     *
     * @param tn_code
     */
    public void setTn_code(String tn_code) {
        this.tn_code = tn_code;
    }

    /**
     *
     * @param tn_nom
     */
    public void setTn_nom(String tn_nom) {
        this.tn_nom = tn_nom;
    }

    /**
     *
     * @param grandtypeproduits
     */
    public void setGrandtypeproduits(GrandTypeProduits grandtypeproduits) {
        this.grandtypeproduits = grandtypeproduits;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
   
    
}
