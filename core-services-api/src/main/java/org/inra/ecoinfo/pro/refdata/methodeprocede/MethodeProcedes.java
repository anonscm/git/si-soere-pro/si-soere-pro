package org.inra.ecoinfo.pro.refdata.methodeprocede;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MethodeProcedes.NAME_ENTITY_JAP, uniqueConstraints = 
        @UniqueConstraint(columnNames = {Produits.ID_JPA,Process.JPA_COLUM_ID, MethodeProcedes.JPA_NAME_COLUMN_ORDER
}))
public class MethodeProcedes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JAP = "produitprocede";

    /**
     *
     */
    public static final String JPA_ID = "mp_id";

    /**
     *
     */
    public static final String JPA_NAME_COLUMN_ORDER = "pmp_ordre";

    /**
     *
     */
    public static final String JPA_NAME_COLUMN_COMMENT = "commentaire";

    /**
     *
     */
    public static final String JPA_NAME_COLUMN_DUREE = "pmp_duree";

    /**
     *
     */
    public static final String JPA_NAME_COLUMN_UNITE = "pmp_unite";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = MethodeProcedes.JPA_ID, nullable = false, updatable = false)
    private long mp_id = 0;
    @Column(name = MethodeProcedes.JPA_NAME_COLUMN_ORDER)
    private int pmp_ordre = 0;
    @Column(name = MethodeProcedes.JPA_NAME_COLUMN_COMMENT)
    private String commentaire = "";
    @Column(name = MethodeProcedes.JPA_NAME_COLUMN_DUREE)
    private String pmp_duree = "";
    @Column(name = MethodeProcedes.JPA_NAME_COLUMN_UNITE)
    private String pmp_unite = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Process.JPA_COLUM_ID, nullable = false)
    @PrimaryKeyJoinColumn(name = Process.JPA_COLUM_ID)
    private Process process;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Produits.ID_JPA, nullable = false)
    @PrimaryKeyJoinColumn(name = Produits.ID_JPA)
    private Produits produits;

    /**
     *
     */
    public MethodeProcedes() {
        super();
    }

    /**
     *
     * @param produits
     * @param process
     * @param ordre
     * @param commentaire
     * @param duree
     * @param unite
     */
    public MethodeProcedes(Produits produits, Process process, int ordre, String commentaire, String duree, String unite) {
        this.produits = produits;
        this.process = process;
        this.pmp_ordre = ordre;
        this.commentaire = commentaire;
        this.pmp_duree = duree;
        this.pmp_unite = unite;
    }

    /**
     *
     * @return
     */
    public long getMp_id() {
        return mp_id;
    }

    /**
     *
     * @return
     */
    public String getPmp_duree() {
        return pmp_duree;
    }

    /**
     *
     * @return
     */
    public int getPmp_ordre() {
        return pmp_ordre;
    }

    /**
     *
     * @return
     */
    public String getPmp_unite() {
        return pmp_unite;
    }
  
    /**
     *
     * @return
     */
    public Process getProcess() {
        return process;
    }

    /**
     *
     * @return
     */
    public Produits getProduits() {
        return produits;
    }

    /**
     *
     * @param mp_id
     */
    public void setMp_id(long mp_id) {
        this.mp_id = mp_id;
    }

    /**
     *
     * @param pmp_duree
     */
    public void setPmp_duree(String pmp_duree) {
        this.pmp_duree = pmp_duree;
    }

    /**
     *
     * @param pmp_ordre
     */
    public void setPmp_ordre(int pmp_ordre) {
        this.pmp_ordre = pmp_ordre;
    }

    /**
     *
     * @param pmp_unite
     */
    public void setPmp_unite(String pmp_unite) {
        this.pmp_unite = pmp_unite;
    }
   
    /**
     *
     * @param process
     */
    public void setProcess(Process process) {
        this.process = process;
    }

    /**
     *
     * @param produits
     */
    public void setProduits(Produits produits) {
        this.produits = produits;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
}
