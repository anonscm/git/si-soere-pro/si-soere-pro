
package org.inra.ecoinfo.pro.refdata.infosapportfertilisanttrait;
/*
@Entity
@Table(name = InfosApportFertilisantTrait.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.MODALITE_ID, RefDataConstantes.DESCRIPTTRAITEMENT_ID, Nomenclatures.JPA_ID}))
public class InfosApportFertilisantTrait implements Serializable 
{
    private static final long serialVersionUID = 1L;
    public static final String ID_JPA = RefDataConstantes.INFOSCONDUITETRAIT_ID; // inftr_id
    public static final String TABLE_NAME = RefDataConstantes.INFOSCONDUITETRAIT_TABLE_NAME; // infos_conduite_trait
    
    @Id
    @Column(name = InfosApportFertilisantTrait.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Modalite.class)
    @JoinColumn(name = RefDataConstantes.MODALITE_ID, referencedColumnName = Modalite.ID_JPA, nullable = true)
    private Modalite modalite;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Nomenclatures.class)
    @JoinColumn(name = Nomenclatures.JPA_ID, referencedColumnName = Nomenclatures.JPA_ID, nullable = true)
    private Nomenclatures nomenclature;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = DescriptionTraitement.class)
    @JoinColumn(name = RefDataConstantes.DESCRIPTTRAITEMENT_ID, referencedColumnName = DescriptionTraitement.ID_JPA, nullable = false)
    private DescriptionTraitement descriptionTraitement;
    
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ISETUDIE_INFOSCONDUITETRAIT)
    private Boolean isEtudie;
    
    @Transient
    private Map<Integer, Modalite> lstModaliteParTraitement = new TreeMap<Integer, Modalite>();
    
    @Transient
    private Map<Integer, Nomenclatures> lstNomenParTraitement = new TreeMap<Integer, Nomenclatures>();
    
    @Transient
    private Map<Integer, Boolean> lstIsFactEtudieMod = new TreeMap<Integer, Boolean>();
    
    @Transient
    private Map<Integer, Boolean> lstIsFactEtudieNom = new TreeMap<Integer, Boolean>();
    
    //Pour conserver l'ordre des facteurs et modalités
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_RANG_INFOSCONDUITETRAIT)
    private int rang;
    
    public InfosApportFertilisantTrait() 
    {
        super();
    }
    
    public InfosApportFertilisantTrait(DescriptionTraitement descriptionTraitement) 
    {
        super();
        //this.descriptionTraitement = descriptionTraitement;
    }
    
    *//**
	 * @param modalite
	 * @param nomenclature
	 * @param descriptionTraitement
	 *//*
	public InfosApportFertilisantTrait(Modalite modalite,
			Nomenclatures nomenclature,
			DescriptionTraitement descriptionTraitement) {
		super();
		this.modalite = modalite;
		this.nomenclature = nomenclature;
		//this.descriptionTraitement = descriptionTraitement;
	}
    
    *//**
	 * @param modalite
	 * @param nomenclature
	 * @param descriptionTraitement
	 * @param isEtudie
	 *//*
	public InfosApportFertilisantTrait(Modalite modalite,
			Nomenclatures nomenclature,
			DescriptionTraitement descriptionTraitement, Boolean isEtudie) {
		super();
		this.modalite = modalite;
		this.nomenclature = nomenclature;
	//	this.descriptionTraitement = descriptionTraitement;
		this.isEtudie = isEtudie;
	}

	*//**
	 * @return the id
	 *//*
	public Long getId() {
		return id;
	}

	*//**
	 * @param id the id to set
	 *//*
	public void setId(Long id) {
		this.id = id;
	}

	*//**
	 * @return the modalite
	 *//*
	public Modalite getModalite() {
		return modalite;
	}

	*//**
	 * @param modalite the modalite to set
	 *//*
	public void setModalite(Modalite modalite) {
		this.modalite = modalite;
	}

	*//**
	 * @return the nomenclature
	 *//*
	public Nomenclatures getNomenclature() {
		return nomenclature;
	}

	*//**
	 * @param nomenclature the nomenclature to set
	 *//*
	public void setNomenclature(Nomenclatures nomenclature) {
		this.nomenclature = nomenclature;
	}

	*//**
	 * @return the descriptionTraitement
	 *//*
	//public DescriptionTraitement getDescriptionTraitement() {
	//	return descriptionTraitement;
	//}

	*//**
	 * @param descriptionTraitement the descriptionTraitement to set
	 *//*
	public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
	//	this.descriptionTraitement = descriptionTraitement;
	}

	*//**
	 * @return the isEtudie
	 *//*
	public Boolean getIsEtudie() {
		return isEtudie;
	}

	*//**
	 * @param isEtudie the isEtudie to set
	 *//*
	public void setIsEtudie(Boolean isEtudie) {
		this.isEtudie = isEtudie;
	}

	*//**
	 * @return the lstModaliteParTraitement
	 *//*
	public Map<Integer, Modalite> getLstModaliteParTraitement() {
		return lstModaliteParTraitement;
	}

	*//**
	 * @param lstModaliteParTraitement the lstModaliteParTraitement to set
	 *//*
	public void setLstModaliteParTraitement(
			Map<Integer, Modalite> lstModaliteParTraitement) {
		this.lstModaliteParTraitement = lstModaliteParTraitement;
	}

	*//**
	 * @return the lstNomenParTraitement
	 *//*
	public Map<Integer, Nomenclatures> getLstNomenParTraitement() {
		return lstNomenParTraitement;
	}

	*//**
	 * @param lstNomenParTraitement the lstNomenParTraitement to set
	 *//*
	public void setLstNomenParTraitement(
			Map<Integer, Nomenclatures> lstNomenParTraitement) {
		this.lstNomenParTraitement = lstNomenParTraitement;
	}

	*//**
	 * @return the lstIsFactEtudieNom
	 *//*
	public Map<Integer, Boolean> getLstIsFactEtudieNom() {
		return lstIsFactEtudieNom;
	}

	*//**
	 * @param lstIsFactEtudieNom the lstIsFactEtudieNom to set
	 *//*
	public void setLstIsFactEtudieNom(Map<Integer, Boolean> lstIsFactEtudieNom) {
		this.lstIsFactEtudieNom = lstIsFactEtudieNom;
	}

	*//**
	 * @return the lstIsFactEtudieMod
	 *//*
	public Map<Integer, Boolean> getLstIsFactEtudieMod() {
		return lstIsFactEtudieMod;
	}

	*//**
	 * @param lstIsFactEtudieMod the lstIsFactEtudieMod to set
	 *//*
	public void setLstIsFactEtudieMod(Map<Integer, Boolean> lstIsFactEtudieMod) {
		this.lstIsFactEtudieMod = lstIsFactEtudieMod;
	}

	*//**
	 * @return the rang
	 *//*
	public int getRang() {
		return rang;
	}

	*//**
	 * @param rang the rang to set
	 *//*
	public void setRang(int rang) {
		this.rang = rang;
	}
	
	
}
*/