/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cepagevariete;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=CepageVariete.NAME_ENTITY_JPA)
public class CepageVariete implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "cepagevariete";

    /**
     *
     */
    public static final String JPA_ID = "cv_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "cv_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "cv_libelle";
    
    @Id
    @Column(name = CepageVariete.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cv_id;
    @Column(name = CepageVariete.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String cv_code = "";
    @Column(name = CepageVariete.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String cv_libelle = "";
    
    /**
     *
     */
    public CepageVariete(){
        super();
    }
    
    /**
     *
     * @param libelle
     */
    public CepageVariete(String libelle){
        this.cv_libelle = libelle;
    }

    /**
     *
     * @return
     */
    public long getCv_id() {
        return cv_id;
    }

    /**
     *
     * @return
     */
    public String getCv_code() {
        return cv_code;
    }

    /**
     *
     * @return
     */
    public String getCv_libelle() {
        return cv_libelle;
    }

    /**
     *
     * @param cv_id
     */
    public void setCv_id(long cv_id) {
        this.cv_id = cv_id;
    }

    /**
     *
     * @param cv_code
     */
    public void setCv_code(String cv_code) {
        this.cv_code = cv_code;
    }

    /**
     *
     * @param cv_libelle
     */
    public void setCv_libelle(String cv_libelle) {
        this.cv_libelle = cv_libelle;
    }
   
    
}
