/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.echantillonplante.EchantillonPlante;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = MesurePlanteElementaire.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    EchantillonPlante.ID_JPA,
    MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE,
    MesurePlanteElementaire.COLUMN_NOM_LABO,
    MesurePlanteElementaire.COLUMN_REP_ANALYSE}),
        indexes = {
            @Index(name = "mesure_plante_brut_echan_idx", columnList = MesurePlanteElementaire.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, referencedColumnName = MesurePlanteElementaire.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, column = @Column(name = MesurePlanteElementaire.ID_JPA))})
public class MesurePlanteElementaire extends MesurePhysicoChimie<MesurePlanteElementaire, ValeurPlanteElementaire> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesureplantebrut";

    /**
     *
     */
    public static final String ID_JPA = "mp_id";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "nomlabo";

    /**
     *
     */
    public static final String COLUMN_LAB_CODE = "codelabo";

    /**
     *
     */
    public static final String COLUMN_REP_ANALYSE = "numerorep";

    /**
     *
     */
    public static final String COLUMN_KEY_MESURE = "keymesure";

    static final String RESOURCE_PATH = "%s/%s";

    @Column(name = MesurePlanteElementaire.COLUMN_NOM_LABO, nullable = false)
    private String nomlabo = "";

    @Column(name = MesurePlanteElementaire.COLUMN_REP_ANALYSE, nullable = false)
    private int numero_repetition = 0;

    @Column(name = MesurePlanteElementaire.COLUMN_LAB_CODE, nullable = false)
    private String codelabo = "";

    @Column(name = MesurePlanteElementaire.COLUMN_KEY_MESURE, nullable = false, unique = true)
    private String keymesure = null;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = EchantillonPlante.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private EchantillonPlante echantillonPlante;

    @OneToMany(mappedBy = "mesurePlanteElementaire", cascade = ALL)
    List<ValeurPlanteElementaire> valeurPlanteElementaire = new LinkedList();

    /**
     *
     */
    public MesurePlanteElementaire() {
        super();
    }

    /**
     *
     * @param localDatePrelevement
     * @param echantillonPlante
     * @param codelabo
     * @param nomlabo
     * @param repet
     * @param ligneFichierEchange
     * @param versionfile
     */
    public MesurePlanteElementaire(LocalDate localDatePrelevement, EchantillonPlante echantillonPlante, String codelabo, String nomlabo, int repet, Long ligneFichierEchange, VersionFile versionfile) {
        super(localDatePrelevement, ligneFichierEchange, versionfile);
        this.codelabo = codelabo;
        this.nomlabo = nomlabo;
        this.numero_repetition = repet;
        this.echantillonPlante = echantillonPlante;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public int getNumero_repetition() {
        return numero_repetition;
    }

    /**
     *
     * @param numero_repetition
     */
    public void setNumero_repetition(int numero_repetition) {
        this.numero_repetition = numero_repetition;
    }

    /**
     *
     * @return
     */
    public String getCodelabo() {
        return codelabo;
    }

    /**
     *
     * @param codelabo
     */
    public void setCodelabo(String codelabo) {
        this.codelabo = codelabo;
    }

    /**
     *
     * @return
     */
    public EchantillonPlante getEchantillonPlante() {
        return echantillonPlante;
    }

    /**
     *
     * @param echantillonPlante
     */
    public void setEchantillonPlante(EchantillonPlante echantillonPlante) {
        this.echantillonPlante = echantillonPlante;
    }

    /**
     *
     * @return
     */
    public List<ValeurPlanteElementaire> getValeurPlanteElementaire() {
        return valeurPlanteElementaire;
    }

    /**
     *
     * @param valeurPlanteElementaire
     */
    public void setValeurPlanteElementaire(List<ValeurPlanteElementaire> valeurPlanteElementaire) {
        this.valeurPlanteElementaire = valeurPlanteElementaire;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

}
