/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.fluxchambres.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureFluxChambres.NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    MesureFluxChambres.ATTRIBUTE_JPA_LocalDate
}))

public class MesureFluxChambres implements Serializable {

    /**
     *
     */
    public static final String NAME_TABLE = "mesure_flux_chambres_mfc";

    /**
     *
     */
    public static final String ID_JPA = "mfc_id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LocalDate = "LocalDate_mesure";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NB_CHAMBRE = "nb_chambre";

    static final long serialVersionUID = 1L;
    static final String RESOURCE_PATH = "%s/%s/%s/%s";

    @Id
    @Column(name = MesureFluxChambres.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = MesureFluxChambres.ATTRIBUTE_JPA_LocalDate, nullable = false)
    LocalDate LocalDate;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    VersionFile versionFile;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = ParcelleElementaire.ID_JPA, referencedColumnName = ParcelleElementaire.ID_JPA, nullable = false)
    ParcelleElementaire parcelleElementaire;

    @OneToMany(mappedBy = "mesureFluxChambres", cascade = ALL)
    List<ValeurFluxChambres> valeurs = new LinkedList();

    @Column(name = "ligne_fichier_echange")
    Long ligneFichierEchange;

    @Column(name = MesureFluxChambres.ATTRIBUTE_JPA_NB_CHAMBRE, nullable = false)
    int nbrChambre;

    @Column
    int nbrCycle;

    /**
     *
     */
    public MesureFluxChambres() {
        super();
    }

    /**
     *
     * @param LocalDate
     * @param versionFile
     * @param parcelleElementaire
     * @param ligneFichierEchange
     * @param nbrChambre
     * @param nbrCycle
     */
    public MesureFluxChambres(LocalDate LocalDate, VersionFile versionFile, ParcelleElementaire parcelleElementaire, Long ligneFichierEchange, int nbrChambre, int nbrCycle) {
        this.LocalDate = LocalDate;
        this.versionFile = versionFile;
        this.parcelleElementaire = parcelleElementaire;
        this.ligneFichierEchange = ligneFichierEchange;
        this.nbrChambre = nbrChambre;
        this.nbrCycle = nbrCycle;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public int getNbrChambre() {
        return nbrChambre;
    }

    /**
     *
     * @param nbrChambre
     */
    public void setNbrChambre(int nbrChambre) {
        this.nbrChambre = nbrChambre;
    }

    /**
     *
     * @return
     */
    public int getNbrCycle() {
        return nbrCycle;
    }

    /**
     *
     * @param nbrCycle
     */
    public void setNbrCycle(int nbrCycle) {
        this.nbrCycle = nbrCycle;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDate() {
        return this.LocalDate;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionFile() {
        return versionFile;
    }

    /**
     *
     * @param versionFile
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     *
     * @return
     */
    public ParcelleElementaire getParcelleElementaire() {
        return parcelleElementaire;
    }

    /**
     *
     * @param parcelleElementaire
     */
    public void setParcelleElementaire(ParcelleElementaire parcelleElementaire) {
        this.parcelleElementaire = parcelleElementaire;
    }

    /**
     *
     * @return
     */
    public List<ValeurFluxChambres> getValeurs() {
        return valeurs;
    }

    /**
     *
     * @param valeurs
     */
    public void setValeurs(List<ValeurFluxChambres> valeurs) {
        this.valeurs = valeurs;
    }

    /**
     *
     * @param LocalDate
     */
    public void setLocalDate(LocalDate LocalDate) {
        this.LocalDate = LocalDate;
    }

}
