/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurSemisPlantation.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    RealNode.ID_JPA,
    MesureSemisPlantation.ATTRIBUTE_JPA_ID}))
@PrimaryKeyJoinColumn(name = ValeurITK.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurSemisPlantation.ATTRIBUTE_JPA_ID)
@AttributeOverrides({
    @AttributeOverride(name = ValeurITK.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurSemisPlantation.ATTRIBUTE_JPA_ID))})

public class ValeurSemisPlantation extends ValeurITK<MesureSemisPlantation, ValeurSemisPlantation> {

    /**
     *
     */
    protected static final String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s";

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurinterventionsemis";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "int_semis_id";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureSemisPlantation.class)
    @JoinColumn(name = MesureSemisPlantation.ATTRIBUTE_JPA_ID, referencedColumnName = MesureSemisPlantation.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureSemisPlantation mesureinterventionsemis;

    /**
     *
     */
    public ValeurSemisPlantation() {
        super();
    }

    /**
     *
     * @param valeur
     * @param mesureinterventionsemis
     * @param realNode
     */
    public ValeurSemisPlantation(Float valeur, MesureSemisPlantation mesureinterventionsemis,
            RealNode realNode) {
        super(valeur, realNode);
        this.mesureinterventionsemis = mesureinterventionsemis;
    }

    /**
     *
     * @param mesureinterventionsemis
     * @param listeItineraires
     * @param realNode
     */
    public ValeurSemisPlantation(MesureSemisPlantation mesureinterventionsemis, ListeItineraire listeItineraires,
            RealNode realNode) {
        super(listeItineraires, realNode);
        this.mesureinterventionsemis = mesureinterventionsemis;
    }

    /**
     *
     * @return
     */
    public MesureSemisPlantation getMesureinterventionsemis() {
        return mesureinterventionsemis;
    }

    /**
     *
     * @param mesureinterventionsemis
     */
    public void setMesureinterventionsemis(MesureSemisPlantation mesureinterventionsemis) {
        this.mesureinterventionsemis = mesureinterventionsemis;
    }
}
