/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.lavage;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = Lavage.NAME_ENTITY_JPA)
public class Lavage implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "lavage";

    /**
     *
     */
    public static final String ID_JPA = "l_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NOM = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Lavage.ID_JPA, unique = true, nullable = false)
    private long l_id = 0;
    @Size(min = 1, max = 80)
    @Column(name = Lavage.JPA_COLUMN_NOM, unique = true, nullable = false, length = 80)
    private String nom = "";
    @Column(name = Lavage.JPA_COLUMN_CODE, unique = true, nullable = false)
    private String code = "";
    @Column(name=Lavage.JPA_COLUMN_COMMENT)
    private String commentaire="";

    /**
     *
     */
    public Lavage() {
        super();
    }
    
    /**
     *
     * @param code
     * @param nom
     * @param commentaire
     */
    public Lavage(String code,String nom,String commentaire){
        this.code = code;
        this.nom = nom;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return l_id;
    }

    /**
     *
     * @param l_id
     */
    public void setId(long l_id) {
        this.l_id = l_id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
    
}
