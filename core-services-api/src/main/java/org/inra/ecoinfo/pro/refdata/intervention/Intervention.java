/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.intervention;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.typeintervention.TypeIntervention;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=Intervention.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {  TypeIntervention.JPA_ID,
                                                                                                Intervention.JPA_COLUMN_NAME
}))

public class Intervention implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "intervention";

    /**
     *
     */
    public static final String JPA_ID = "intervention_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "intervention_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_DESCRIPTION = "intervention_description";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENTAIRE = "intervention_commentaire";
    
    @Id
    @Column(name = Intervention.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long intervention_id;
    
    @Column(name = Intervention.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String intervention_nom;
    @Column(name = Intervention.JPA_COLUMN_DESCRIPTION, unique = false, nullable = true, length = 1024)
    private String intervention_description;
    @Column(name = Intervention.JPA_COLUMN_COMMENTAIRE, unique = false, nullable = true, length = 1024)
    private String intervention_commentaire;
    
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeIntervention.JPA_ID,  nullable = false)
    private TypeIntervention  typeIntervention;

    /**
     *
     */
    public Intervention() {
        super();
    }

    /**
     *
     * @param intervention_nom
     * @param intervention_description
     * @param intervention_commentaire
     * @param typeIntervention
     */
    public Intervention(String intervention_nom, String intervention_description, String intervention_commentaire, TypeIntervention typeIntervention) {
        this.intervention_nom = intervention_nom;
        this.intervention_description = intervention_description;
        this.intervention_commentaire = intervention_commentaire;
        this.typeIntervention = typeIntervention;
    }

    /**
     *
     * @return
     */
    public long getIntervention_id() {
        return intervention_id;
    }

    /**
     *
     * @param intervention_id
     */
    public void setIntervention_id(long intervention_id) {
        this.intervention_id = intervention_id;
    }

    /**
     *
     * @return
     */
    public String getIntervention_nom() {
        return intervention_nom;
    }

    /**
     *
     * @param intervention_nom
     */
    public void setIntervention_nom(String intervention_nom) {
        this.intervention_nom = intervention_nom;
    }

    /**
     *
     * @return
     */
    public TypeIntervention getTypeIntervention() {
        return typeIntervention;
    }

    /**
     *
     * @param typeIntervention
     */
    public void setTypeIntervention(TypeIntervention typeIntervention) {
        this.typeIntervention = typeIntervention;
    }

    /**
     *
     * @return
     */
    public String getIntervention_description() {
        return intervention_description;
    }

    /**
     *
     * @param intervention_description
     */
    public void setIntervention_description(String intervention_description) {
        this.intervention_description = intervention_description;
    }

    /**
     *
     * @return
     */
    public String getIntervention_commentaire() {
        return intervention_commentaire;
    }

    /**
     *
     * @param intervention_commentaire
     */
    public void setIntervention_commentaire(String intervention_commentaire) {
        this.intervention_commentaire = intervention_commentaire;
    }
    
    
}
