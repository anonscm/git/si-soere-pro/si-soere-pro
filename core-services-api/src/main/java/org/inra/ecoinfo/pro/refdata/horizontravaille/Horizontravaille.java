package org.inra.ecoinfo.pro.refdata.horizontravaille;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Horizontravaille.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
        Horizontravaille.JPA_COLUMN_PH_INITIAL, 
        Horizontravaille.JPA_COLUMN_HORIZON,
        Horizontravaille.JPA_COLUMN_GROSSIER,
        Texturesol.ID_JPA
        

}))
public class Horizontravaille implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "horizontravaille";

    /**
     *
     */
    public static final String ID_JPA = "htid";

    /**
     *
     */
    public static final String JPA_COLUMN_PH_INITIAL = "phinitial";

    /**
     *
     */
    public static final String JPA_COLUMN_HORIZON = "horizonsurface";

    /**
     *
     */
    public static final String JPA_COLUMN_TENEUR_CARBONE_ORGANIQUE = "teneurcarbone";

    /**
     *
     */
    public static final String JPA_COLUMN_PERCENT = "pourcentagecailloux";

    /**
     *
     */
    public static final String JPA_COLUMN_GROSSIER = "elementgrossiers";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Horizontravaille.ID_JPA, updatable = false)
    private long htid = 0;
    @Column(name= Horizontravaille.JPA_COLUMN_PH_INITIAL)
    private double phinitial = 0.0;
    @Column(name= Horizontravaille.JPA_COLUMN_HORIZON ,unique = false,nullable=false)
    private double horizonsurface = 0.0;
    @Column(name= Horizontravaille.JPA_COLUMN_TENEUR_CARBONE_ORGANIQUE ,unique = false,nullable=false)
    private double teneurcarbone = 0.0;
    @Column(name= Horizontravaille.JPA_COLUMN_PERCENT,unique = false,nullable=false)
    private double pourcentagecailloux = 0.0;
    @Column(name= Horizontravaille.JPA_COLUMN_GROSSIER,unique = false,nullable=false)
    private boolean elementgrossiers;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Texturesol.ID_JPA,nullable=false)
    private Texturesol texturesol;
    
    /**
     *
     */
    public Horizontravaille(){
        super();
    }
    
    /**
     *
     * @param ph
     * @param surface
     * @param carbone
     * @param cailloux
     * @param grossiers
     * @param texture
     */
    public Horizontravaille(double ph,double surface,double carbone,double cailloux,boolean grossiers,Texturesol texture){
        this.phinitial = ph;
        this.horizonsurface = surface;
        this.teneurcarbone = carbone;
        this.pourcentagecailloux = cailloux;
        this.elementgrossiers = grossiers;
        this.texturesol = texture;
    }

    /**
     *
     * @return
     */
    public long getHtid() {
        return htid;
    }

    /**
     *
     * @param htid
     */
    public void setHtid(long htid) {
        this.htid = htid;
    }

    /**
     *
     * @return
     */
    public double getPhinitial() {
        return phinitial;
    }

    /**
     *
     * @param phinitial
     */
    public void setPhinitial(double phinitial) {
        this.phinitial = phinitial;
    }

    /**
     *
     * @return
     */
    public double getHorizonsurface() {
        return horizonsurface;
    }

    /**
     *
     * @param horizonsurface
     */
    public void setHorizonsurface(double horizonsurface) {
        this.horizonsurface = horizonsurface;
    }

    /**
     *
     * @return
     */
    public double getTeneurcarbone() {
        return teneurcarbone;
    }

    /**
     *
     * @param teneurcarbone
     */
    public void setTeneurcarbone(double teneurcarbone) {
        this.teneurcarbone = teneurcarbone;
    }

    /**
     *
     * @return
     */
    public double getPourcentagecailloux() {
        return pourcentagecailloux;
    }

    /**
     *
     * @param pourcentagecailloux
     */
    public void setPourcentagecailloux(double pourcentagecailloux) {
        this.pourcentagecailloux = pourcentagecailloux;
    }

    /**
     *
     * @return
     */
    public boolean isElementgrossiers() {
        return elementgrossiers;
    }

    /**
     *
     * @param elementgrossiers
     */
    public void setElementgrossiers(boolean elementgrossiers) {
        this.elementgrossiers = elementgrossiers;
    }

    /**
     *
     * @return
     */
    public Texturesol getTexturesol() {
        return texturesol;
    }

    /**
     *
     * @param texturesol
     */
    public void setTexturesol(Texturesol texturesol) {
        this.texturesol = texturesol;
    }
    
    

}
