/**
 *
 */
package org.inra.ecoinfo.pro.refdata;

/**
 * @author sophie
 *
 */
public class RefDataConstantes {

    /* Noms des tables */

    /**
     *
     */

    public static final String TYPEDISPOSITIF_TABLE_NAME = "type_dispositif";

    /**
     *
     */
    public static final String TYPECULTURE_TABLE_NAME = "type_culture";

    /**
     *
     */
    public static final String TYPETRAITEMENT_TABLE_NAME = "type_traitement";

    /**
     *
     */
    public static final String TYPEFACTEUR_TABLE_NAME = "type_facteur";

    /**
     *
     */
    public static final String FACTEUR_TABLE_NAME = "facteur";

    /**
     *
     */
    public static final String MODALITE_TABLE_NAME = "modalite";

    /**
     *
     */
    public static final String PAYS_TABLE_NAME = "pays";

    /**
     *
     */
    public static final String REGION_TABLE_NAME = "region";

    /**
     *
     */
    public static final String DEPARTEMENT_TABLE_NAME = "departement";

    /**
     *
     */
    public static final String COMMUNE_TABLE_NAME = "commune";

    /**
     *
     */
    public static final String TYPESTRUCTURE_TABLE_NAME = "type_structure";

    /**
     *
     */
    public static final String DESCRIPTTRAITEMENT_TABLE_NAME = "description_traitement";

    /**
     *
     */
    public static final String FACTEURETUDIE_TABLE_NAME = "modalite_facteur_etudie_traitement";

    /**
     *
     */
    public static final String PROGRAMME_TABLE_NAME = "programme";

    /**
     *
     */
    public static final String OBSERVATOIRE_TABLE_NAME = "observatoire";

    /**
     *
     */
    public static final String ROLESTR_TABLE_NAME = "role_structure";

    /**
     *
     */
    public static final String THEMEETUDIEE_TABLE_NAME = "thematique_etudiee";

    /**
     *
     */
    public static final String SYSTCONDUITEESSAI_TABLE_NAME = "systeme_conduite_essai";

    /**
     *
     */
    public static final String LSTRAISONNEMENT_TABLE_NAME = "liste_raisonnement";

    /**
     *
     */
    public static final String RAISONNEMENT_TABLE_NAME = "raisonnement";

    /**
     *
     */
    public static final String GEOLOC_TABLE_NAME = "geolocalisation";

    /**
     *
     */
    public static final String ADRESSE_TABLE_NAME = "adresse";

    /**
     *
     */
    public static final String STRUCTURE_TABLE_NAME = "structure";

    /**
     *
     */
    public static final String PERSONNERESSOURCE_TABLE_NAME = "personne_ressource";

    /**
     *
     */
    public static final String DISPOSITIF_TABLE_NAME = "dispositif";

    /**
     *
     */
    public static final String BLOC_TABLE_NAME = "bloc";

    /**
     *
     */
    public static final String PARCELLEELT_TABLE_NAME = "parcelle_elementaire";

    /**
     *
     */
    public static final String PLACETTE_TABLE_NAME = "placette";

    /**
     *
     */
    public static final String PROTOCOLE_TABLE_NAME = "protocole";

    /**
     *
     */
    public static final String TYPELIEU_TABLE_NAME = "type_lieu";

    /**
     *
     */
    public static final String LIEU_TABLE_NAME = "lieu";

    /**
     *
     */
    public static final String STATIONEXP_TABLE_NAME = "station_experimentale";

    /**
     *
     */
    public static final String STATUTPLACETTE_TABLE_NAME = "statut_placette";

    /**
     *
     */
    public static final String ECHELLEPRELEVEMENT_TABLE_NAME = "echelle_prelevement";

    /**
     *
     */
    public static final String INFOSCONDUITETRAIT_TABLE_NAME = "infos_conduite_trait";

    /**
     *
     */
    public static final String STRUCTURESTATIONEXP_TABLE_NAME = "structure_station_experimentale";

    /**
     *
     */
    public static final String STRUCTUREOBSERV_TABLE_NAME = "structure_observatoire";

    /**
     *
     */
    public static final String DISPOSITIFOBSERV_TABLE_NAME = "dispositif_observatoire";

    /**
     *
     */
    public static final String ROLEPERSONNERESSOURCE_TABLE_NAME = "role_personne_ressource";

    /**
     *
     */
    public static final String ROLEPERSONNERSCDISPOSITIF_TABLE_NAME = "role_personne_ressource_dispositif";

    /**
     *
     */
    public static final String PROTOCOLEDISPOSITIF_TABLE_NAME = "protocole_dispositif";

    /**
     *
     */
    public static final String TYPEDOCUMENT_TABLE_NAME = "type_document";

    /**
     *
     */
    public static final String RAISONNEMENTPROTOCOLE_TABLE_NAME = "raisonnement_protocole";

    /**
     *
     */
    public static final String CODEPARCELLEELT_TABLE_NAME = "code_parcelle_elementaire";

    /**
     *
     */
    public static final String CODEBLOC_TABLE_NAME = "code_bloc";

    /**
     *
     */
    public static final String SYSTEMEPROJECTION_TABLE_NAME = "systeme_projection";

    /**
     *
     */
    public static final String APPLICATIONTRAITEMENTPARCELT_TABLE_NAME = "application_traitement_parcelle_elt";

    /**
     *
     */
    public static final String OBJECTIFTHEMATIQUEPROTOCOLE_TABLE_NAME = "objectif_thematique_protocole";

    /**
     *
     */
    public static final String STRUCTURELIEU_TABLE_NAME = "structure_lieu";

    /**
     *
     */
    public static final String STATUTASSOCIEPLACETTE_TABLE_NAME = "statutplacettes_associes_placette";

    /* Id des tables */

    /**
     *
     */

    public static final String TYPEDISPOSITIF_ID = "tpd_id";

    /**
     *
     */
    public static final String TYPECULTURE_ID = "tpc_id";

    /**
     *
     */
    public static final String TYPETRAITEMENT_ID = "tptrt_id";

    /**
     *
     */
    public static final String TYPEFACTEUR_ID = "tpfct_id";

    /**
     *
     */
    public static final String FACTEUR_ID = "fct_id";

    /**
     *
     */
    public static final String MODALITE_ID = "mdt_id";

    /**
     *
     */
    public static final String PAYS_ID = "pa_id";

    /**
     *
     */
    public static final String REGION_ID = "rg_id";

    /**
     *
     */
    public static final String DEPARTEMENT_ID = "dpt_id";

    /**
     *
     */
    public static final String COMMUNE_ID = "cm_id";

    /**
     *
     */
    public static final String TYPESTRUCTURE_ID = "tpstr_id";

    /**
     *
     */
    public static final String DESCRIPTTRAITEMENT_ID = "dctrt_id";

    /**
     *
     */
    public static final String FACTEURETUDIE_ID = "mftrt_id";

    /**
     *
     */
    public static final String PROGRAMME_ID = "prg_id";

    /**
     *
     */
    public static final String OBSERVATOIRE_ID = "obs_id";

    /**
     *
     */
    public static final String ROLESTR_ID = "rstr_id";

    /**
     *
     */
    public static final String THEMEETUDIEE_ID = "the_id";

    /**
     *
     */
    public static final String SYSTCONDUITEESSAI_ID = "stc_id";

    /**
     *
     */
    public static final String LSTRAISONNEMENT_ID = "lrais_id";

    /**
     *
     */
    public static final String RAISONNEMENT_ID = "rais_id";

    /**
     *
     */
    public static final String GEOLOC_ID = "glc_id";

    /**
     *
     */
    public static final String ADRESSE_ID = "adr_id";

    /**
     *
     */
    public static final String STRUCTURE_ID = "str_id";

    /**
     *
     */
    public static final String PERSONNERESSOURCE_ID = "prs_id";

    /**
     *
     */
    public static final String DISPOSITIF_ID = "disp_id";

    /**
     *
     */
    public static final String BLOC_ID = "blc_id";

    /**
     *
     */
    public static final String PARCELLEELT_ID = "prc_id";

    /**
     *
     */
    public static final String PLACETTE_ID = "plc_id";

    /**
     *
     */
    public static final String PROTOCOLE_ID = "prt_id";

    /**
     *
     */
    public static final String TYPELIEU_ID = "tpl_id";

    /**
     *
     */
    public static final String LIEU_ID = "lie_id";

    /**
     *
     */
    public static final String STATIONEXP_ID = "stx_id";

    /**
     *
     */
    public static final String STATUTPLACETTE_ID = "stp_id";

    /**
     *
     */
    public static final String ECHELLEPRELEVEMENT_ID = "ech_id";

    /**
     *
     */
    public static final String INFOSCONDUITETRAIT_ID = "inftr_id";

    /**
     *
     */
    public static final String STRUCTURESTATIONEXP_ID = "strstx_id";

    /**
     *
     */
    public static final String STRUCTUREOBSERV_ID = "strobs_id";

    /**
     *
     */
    public static final String DISPOSITIFOBSERV_ID = "dispobs_id";

    /**
     *
     */
    public static final String ROLEPERSONNERESSOURCE_ID = "rprc_id";

    /**
     *
     */
    public static final String ROLEPERSONNERSCDISPOSITIF_ID = "rprdisp_id";

    /**
     *
     */
    public static final String PROTOCOLEDISPOSITIF_ID = "prtdisp_id";

    /**
     *
     */
    public static final String TYPEDOCUMENT_ID = "tpdcr_id";

    /**
     *
     */
    public static final String RAISONNEMENTPROTOCOLE_ID = "raisprt_id";

    /**
     *
     */
    public static final String CODEPARCELLEELT_ID = "cprc_id";

    /**
     *
     */
    public static final String CODEBLOC_ID = "cblc_id";

    /**
     *
     */
    public static final String SYSTEMEPROJECTION_ID = "sypj_id";

    /**
     *
     */
    public static final String APPLICATIONTRAITEMENTPARCELT_ID = "dtrtprc_id";

    /**
     *
     */
    public static final String OBJECTIFTHEMATIQUEPROTOCOLE_ID = "obthp_id";

    /**
     *
     */
    public static final String STRUCTURELIEU_ID = "strl_id";

    /**
     *
     */
    public static final String STATUTASSOCIEPLACETTE_ID = "stpl_id";

    /* Noms des colonnes pour la table type_dispositif */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPD = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_TPD = "code";

    /* Noms des colonnes pour la table type_culture */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPC = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_TPC = "code";

    /* Noms des colonnes pour la table type_traitement */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPTRT = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_TPTRT = "code";

    /* Noms des colonnes pour la table type_facteur */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPFCT = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_TPFCT = "code";

    /* Noms des colonnes pour la table facteur */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_FCT = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_FCT = "code";

    /**
     *
     */
    public static final String COLUMN_ISINSOSUPPLFERT_FCT = "est_infos_suppl_apport_fert";

    /**
     *
     */
    public static final String COLUMN_ISASSOCNOMENCLATURE = "est_associe_a_nomenclature";

    /* Noms des colonnes pour la table modalite */

    /**
     *
     */

    public static final String COLUMN_VALEUR_MDT = "valeur";

    /**
     *
     */
    public static final String COLUMN_COMENT_MDT = "commentaire";

    /* Noms des colonnes pour la table pays */

    /**
     *
     */

    public static final String COLUMN_NOM_PAYS = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_PAYS = "code";

    /* Noms des colonnes pour la table region */

    /**
     *
     */

    public static final String COLUMN_NOM_REGION = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_REGION = "code";

    /* Noms des colonnes pour la table departement */

    /**
     *
     */

    public static final String COLUMN_NOM_DEPARTEMENT = "nom";

    /**
     *
     */
    public static final String COLUMN_NUMERO_DEPARTEMENT = "numero";

    /**
     *
     */
    public static final String COLUMN_CODE_DEPARTEMENT = "code";

    /* Noms des colonnes pour la table commune */

    /**
     *
     */

    public static final String COLUMN_NOM_COMMUNE = "nom";

    /**
     *
     */
    public static final String COLUMN_CODEPOSTAL_COMMUNE = "code_postal";

    /* Noms des colonnes pour la table type_structure */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPSTR = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_TPSTR = "code";

    /* Noms des colonnes pour la table description_traitement */

    /**
     *
     */

    public static final String COLUMN_NOM_DESCRPTTRT = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_DESCRPTTRT = "code";

    /**
     *
     */
    public static final String COLUMN_NBREREP_DESCRPTTRT = "nbre_repetition";

    /**
     *
     */
    public static final String COLUMN_ANNEEDEBVAL_DESCRPTTRT = "annee_validite_debut";

    /**
     *
     */
    public static final String COLUMN_ANNEEFINVAL_DESCRPTTRT = "annee_validite_fin";

    /**
     *
     */
    public static final String COLUMN_TRTORIGINE_DESCRPTTRT = "traitement_origine_id";

    /**
     *
     */
    public static final String COLUMN_COMMENT_DESCRPTTRT = "commentaire";

    /**
     *
     */
    public static final String COLUMN_ANNEEMODIFMIN_DESCRPTTRT = "annee_modif_mineure";

    /* Noms des colonnes pour la table modalite_facteur_etudie */

    /**
     *
     */

    public static final String COLUMN_RANGMOD_MODFACTETUDIE = "rang_mod";

    /**
     *
     */
    public static final String COLUMN_RANGMON_MODFACTETUDIE = "rang_nom";

    /* Noms des colonnes pour la table type_partenaire */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPPAR = "libelle";

    /* Noms des colonnes pour la table programme */

    /**
     *
     */

    public static final String COLUMN_ACRONYME_PRG = "acronyme";

    /**
     *
     */
    public static final String COLUMN_NOM_PRG = "nom";

    /**
     *
     */
    public static final String COLUMN_DATEDEBUT_PRG = "date_debut";

    /**
     *
     */
    public static final String COLUMN_DATEFIN_PRG = "date_fin";

    /**
     *
     */
    public static final String COLUMN_COMMENTNUMCONV_PRG = "commentaire_numero_convention";

    /**
     *
     */
    public static final String COLUMN_COMMENTFINANCT_ORIG_PRG = "commentaire_financement_origine";

    /* Noms des colonnes pour la table observatoire */

    /**
     *
     */

    public static final String COLUMN_NOM_OBS = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_OBS = "code";

    /**
     *
     */
    public static final String COLUMN_DEFINITION_OBS = "definition";

    /* Noms des colonnes pour la table role_structure */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_ROLESTR = "libelle";

    /* Noms des colonnes pour la table thematique_etudiee */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_THEMEETUDIEE = "libelle";

    /* Noms des colonnes pour la table systeme_conduite_essai */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_STC = "libelle";

    /* Noms des colonnes pour la table liste_raisonnement */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_LRAIS = "libelle";

    /* Noms des colonnes pour la table raisonnement */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_RAIS = "libelle";

    /* Noms des colonnes pour la table geolocalisation */

    /**
     *
     */

    public static final String COLUMN_LATITUDE_GEOLOC = "latitude";

    /**
     *
     */
    public static final String COLUMN_LONGITUDE_GEOLOC = "longitude";

    /* Noms des colonnes pour la table adresse */

    /**
     *
     */

    public static final String COLUMN_NONOMRUE_ADR = "no_nom_rue";

    /* Noms des colonnes pour la table structure */

    /**
     *
     */

    public static final String COLUMN_NOM_STR = "nom";

    /**
     *
     */
    public static final String COLUMN_PRECISION_STR = "precision";

    /**
     *
     */
    public static final String COLUMN_NOTEL_STR = "no_tel";

    /**
     *
     */
    public static final String COLUMN_NOFAX_STR = "no_fax";

    /**
     *
     */
    public static final String COLUMN_ESTSIGNATAIRE_STR = "est_signataire";

    /**
     *
     */
    public static final String COLUMN_COMMENTAIRE_STR = "commentaire";

    /* Noms des colonnes pour la table personne_ressource */

    /**
     *
     */

    public static final String COLUMN_NOM_PERSONNERESSOURCE = "nom";

    /**
     *
     */
    public static final String COLUMN_PRENOM_PERSONNERESSOURCE = "prenom";

    /**
     *
     */
    public static final String COLUMN_EMAIL_PERSONNERESSOURCE = "email";

    /**
     *
     */
    public static final String COLUMN_FONCTION_PERSONNERESSOURCE = "fonction";

    /**
     *
     */
    public static final String COLUMN_NOTELPOSTE_PERSONNERESSOURCE = "no_tel";

    /* Noms des colonnes pour la table protocole */

    /**
     *
     */

    public static final String COLUMN_NOM_PROTOCOLE = "nom";

    /**
     *
     */
    public static final String COLUMN_ANNEEDEBVAL_PROTOCOLE = "annee_debut_application";

    /**
     *
     */
    public static final String COLUMN_ANNEEFINVAL_PROTOCOLE = "annee_fin_application";

    /**
     *
     */
    public static final String COLUMN_UTILGUIDEMETH_PROTOCOLE = "utilise_guide_methodologique";

    /* Noms des colonnes pour la table dispositif */

    /**
     *
     */

    public static final String COLUMN_NOM_DISP = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_DISP = "code";

    /**
     *
     */
    public static final String COLUMN_SURFACETOT_DISP = "surface_totale";

    /**
     *
     */
    public static final String COLUMN_ISCHGTSYSTCONDUITE_DISP = "is_chgt_systeme_conduite";

    /**
     *
     */
    public static final String COLUMN_ANNEEDEBUT_DISP = "annee_debut";

    /**
     *
     */
    public static final String COLUMN_ANNEEFIN_DISP = "annee_fin";

    /**
     *
     */
    public static final String COLUMN_ANNEEARRET_EPANDAGE = "annee_arret_epandage";

    /**
     *
     */
    public static final String COLUMN_ANNEE_CHGT_SYSTCONDUITE_DISP = "annee_chgt_systeme_conduite";

    /**
     *
     */
    public static final String COLUMN_RAISON_CHGT_SYSTCONDUITE_DISP = "raison_chgt_systeme_conduite";

    /* Noms des colonnes pour la table bloc */

    /**
     *
     */

    public static final String COLUMN_NOM_BLOC = "nom";

    /* Noms des colonnes pour la table parcelle_elementaire */

    /**
     *
     */

    public static final String COLUMN_NOM_PRCELT = "nom";
    public static final String COLUMN_MY_CODE_PRCELT = "mycode";

    /**
     *
     */
    public static final String COLUMN_NUMEROREPETITION_PRCELT = "numero_repetition";

    /* Noms des colonnes pour la table placette */

    /**
     *
     */

    public static final String COLUMN_NOM_PLACT = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_PLACT = "code";

    /**
     *
     */
    public static final String COLUMN_STATUT_PLACT = "statut";

    /**
     *
     */
    public static final String COLUMN_ANNEEDEBUT_PLACT = "annee_debut";

    /**
     *
     */
    public static final String COLUMN_ANNEEFIN_PLACT = "annee_fin";

    /**
     *
     */
    public static final String COLUMN_COMMENTAIRE_PLACT = "commentaire";

    /* Noms des colonnes pour la table type_lieu */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TPLIEU = "libelle";

    /**
     *
     */
    public static final String COLUMN_CODE_TPLIEU = "code";

    /* Noms des colonnes pour la table lieu */

    /**
     *
     */

    public static final String COLUMN_NOM_LIEU = "nom";

    /**
     *
     */
    public static final String COLUMN_NUMCADASTRE_LIEU = "numero_cadastre";

    /* Noms des colonnes pour la table station_experimentale */

    /**
     *
     */

    public static final String COLUMN_NOM_STATIONEXP = "nom";

    /* Noms des colonnes pour la table statut placette */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_STATPLC = "libelle";

    /* Noms des colonnes pour la table echelle_prelevement */

    /**
     *
     */

    public static final String COLUMN_NOM_ECH = "nom";

    /**
     *
     */
    public static final String COLUMN_CODE_ECH = "code";
    
    /* Noms des colonnes pour la table infos_conduite_trait */

    /**
     *
     */

    public static final String COLUMN_ISETUDIE_INFOSCONDUITETRAIT = "is_etudie";

    /**
     *
     */
    public static final String COLUMN_RANG_INFOSCONDUITETRAIT = "rang";

    /* Noms des colonnes pour la table role_structure */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_ROLEPERSRESSOURCE = "libelle";

    /* Noms des colonnes pour la table type_document */

    /**
     *
     */

    public static final String COLUMN_LIBELLE_TYPEDOCUMENT = "libelle";

    /* Noms des colonnes pour la table raisonnement_protocole */

    /**
     *
     */

    public static final String COLUMN_COMMENT_RAISPRT = "commentaire";

    /* Noms des colonnes pour la table code_parcelle_elementaire */

    /**
     *
     */

    public static final String COLUMN_CODE_CODEPARCELLEELT = "code";

    /* Noms des colonnes pour la table code_bloc */

    /**
     *
     */

    public static final String COLUMN_CODE_CODEBLOC = "code";

    /* Noms des colonnes pour la table systeme_projection */

    /**
     *
     */

    public static final String COLUMN_NOM_SYSTEMEPROJ = "nom";

    /**
     *
     */
    public static final String COLUMN_DEFINITION_SYSTEMEPROJ = "definition";

    /**
     *
     */
    public static final String COLUMN_CODE_SYSTEMEPROJ = "code";

    /* Noms des colonnes pour la table application_traitement_parcelle_elt */

    /**
     *
     */

    public static final String COLUMN_ANNEEDEBUT_APPLICATIONTRAIPARCELT = "annee_debut_application";

    /**
     *
     */
    public static final String COLUMN_ANNEEFIN_APPLICATIONTRAITPARCELT = "annee_fin_application";

    /**
     *
     */
    public static final String COLUMN_NUMEROREPETITION_APPLICATIONTRAITPARCELT = "numero_repetition";

    /* Noms des colonnes pour la table objectif_thematique_protocole */

    /**
     *
     */

    public static final String COLUMN_OBJECTIF_OBJTHEMPROTOCOLE = "objectif";

    /**
     *
     */
    public static final String NAME_PK_ID_ZONEPRELEVT = "id";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_PAYS = "pays";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_REGION = "region";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_DEPARTEMENT = "departement";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_SYSTCHGTCONDUITEESSAI = "chgt_stc_id";

    /**
     *
     */
    public static final String[] valBooleanObligatoire = {"oui","non"};

    /**
     *
     */
    public static final String[] valBooleanNonObligatoire = {"","oui","non"};

    /**
     *
     */
    public static final String[] valBooleanObligatoireInv = {"non","oui"};

}
