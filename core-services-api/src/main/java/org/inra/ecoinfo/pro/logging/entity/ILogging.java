package org.inra.ecoinfo.pro.logging.entity;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.JoinPoint.StaticPart;

/**
 *
 * @author ptcherniati
 */
public interface ILogging {

    /**
     *
     * @param joinPoint
     */
    public abstract void logMethodEntry(JoinPoint joinPoint);

    /**
     *
     * @param staticPart
     * @param result
     */
    public abstract void logMethodExit(StaticPart staticPart, Object result);

}