/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurIncubationSolPro.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurIncubationSolPro.ATTRIBUTE_JPA_STATUT_VALEUR,
    RealNode.ID_JPA,
    MesureIncubationSolPro.ID_JPA
}
),
        indexes = {
            @Index(name = "valeur_incub_mesure_sol_pro_idx", columnList = MesureIncubationSolPro.ID_JPA),
            @Index(name = "valeur_incub_variable_sol_pro", columnList = RealNode.ID_JPA)
        }
)
@PrimaryKeyJoinColumn(name = ValeurIncubation.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurIncubationSolPro.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurIncubation.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurIncubationSolPro.ID_JPA))})
public class ValeurIncubationSolPro extends ValeurIncubation<MesureIncubationSolPro, ValeurIncubationSolPro> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurincubationsolpro";

    /**
     *
     */
    public static final String ID_JPA = "vispro_id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";

    @Column(name = ValeurIncubationSolPro.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureIncubationSolPro.class)
    @JoinColumn(name = MesureIncubationSolPro.ID_JPA, referencedColumnName = MesureIncubationSolPro.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureIncubationSolPro mesureIncubationSolPro;

    /**
     *
     */
    public ValeurIncubationSolPro() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param mesureIncubationSolPro
     * @param realNode
     */
    public ValeurIncubationSolPro(Float valeur, String statutvaleur, MesureIncubationSolPro mesureIncubationSolPro, RealNode realNode) {
        super(valeur, realNode);
        this.statutvaleur = statutvaleur;
        this.mesureIncubationSolPro = mesureIncubationSolPro;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public MesureIncubationSolPro getMesureIncubationSolPro() {
        return mesureIncubationSolPro;
    }

    /**
     *
     * @param mesureIncubationSolPro
     */
    public void setMesureIncubationSolPro(MesureIncubationSolPro mesureIncubationSolPro) {
        this.mesureIncubationSolPro = mesureIncubationSolPro;
    }
}
