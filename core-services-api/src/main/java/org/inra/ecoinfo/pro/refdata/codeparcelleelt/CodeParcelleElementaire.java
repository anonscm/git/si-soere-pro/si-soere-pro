package org.inra.ecoinfo.pro.refdata.codeparcelleelt;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = CodeParcelleElementaire.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_CODE_CODEPARCELLEELT}))
public class CodeParcelleElementaire implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.CODEPARCELLEELT_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.CODEPARCELLEELT_TABLE_NAME; 
    @Id
    @Column(name = CodeParcelleElementaire.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_CODEPARCELLEELT, length = 10)
    private String code;

    /**
     *
     */
    public CodeParcelleElementaire() {
        super();
    }

    /**
     *
     * @param code
     */
    public CodeParcelleElementaire(String code) {
        super();
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

}
