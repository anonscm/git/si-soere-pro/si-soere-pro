/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.stadedeveloppement;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = StadeDeveloppement.NAME_ENTITY_JPA)
public class StadeDeveloppement implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "stadedeveloppement";

    /**
     *
     */
    public static final String ID_JPA = "sd_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE = "mycode";
   // public static final String JPA_COLUMN_NOM = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_CODEBBCH="codebbch";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = StadeDeveloppement.ID_JPA, unique = true, nullable = false, updatable = false)
    private long sd_id = 0;
   // @Size(min = 1, max = 80)
   /* @Column(name = StadeDeveloppement.JPA_COLUMN_NOM, unique = true, nullable = false, length = 80)
    private String nom = "";*/
    @Column(name = StadeDeveloppement.JPA_COLUMN_MYCODE, unique = true, nullable = false, length = 510)
    private String mycode = "";
    @Column(name = StadeDeveloppement.JPA_COLUMN_CODE, unique = true, nullable = false,length = 510)
    private String code = "";
    @Column(name = StadeDeveloppement.JPA_COLUMN_CODEBBCH, unique = true, nullable = false, length =80)
    private String codebbch = "";
    @Column(name=StadeDeveloppement.JPA_COLUMN_COMMENT)
    private String commentaire="";
    
    /**
     *
     */
    public StadeDeveloppement(){
        super();
    }
    
    /**
     *
     * @param code
     * @param mycode
     * @param bbch
     * @param commentaire
     */
    public StadeDeveloppement(String code,String mycode,String bbch,String commentaire){
        this.code = code;
        this.mycode = mycode;
      //  this.nom = nom;
        this.codebbch=bbch;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getSd_id() {
        return sd_id;
    }

    /**
     *
     * @param sd_id
     */
    public void setSd_id(long sd_id) {
        this.sd_id = sd_id;
    }

   

  /*  public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }*/

    /**
     *
     * @return
     */


    public String getMycode() {
        return mycode;
    }

    /**
     *
     * @param mycode
     */
    public void setMycode(String mycode) {
        this.mycode = mycode;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }
    
    
    
}
