/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.entity;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;

/**
 *
 * @author ptcherniati
 * @param <M> la mesure associée
 * @param <V> la valeur (itself)
 */
@MappedSuperclass
public class ValeurPhysicoChimie<M extends MesurePhysicoChimie, V extends ValeurPhysicoChimie> implements Serializable {

    /**
     *
     */
    public static final String ID_JPA = "vpc_id";

    @Id
    @Column(name = ValeurPhysicoChimie.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = ValeurPlanteElementaire.COLUMN_JPA_VALUE, nullable = false)
    private Float valeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private RealNode realNode;

    /**
     *
     */
    public ValeurPhysicoChimie() {
    }

    /**
     *
     * @param valeur
     * @param realNode
     */
    public ValeurPhysicoChimie(Float valeur, RealNode realNode) {
        this.valeur = valeur;
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

}
