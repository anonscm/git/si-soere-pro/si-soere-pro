/*
 *
 */
package org.inra.ecoinfo.pro.refdata.caracteristiqueetape;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = CaracteristiqueEtapes.NAME_ENTITY_JPA)
public class CaracteristiqueEtapes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "caracteristiqueetape";

    /**
     *
     */
    public static final String ID_JPA = "cetape_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "cetape_code";

    /**
     *
     */
    public static final String JPA_COLUMN_INTITULE = "cetape_intitule";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = CaracteristiqueEtapes.ID_JPA, unique = true, updatable = false, nullable = false)
    private long cetape_id = 0;
    @Column(name = CaracteristiqueEtapes.JPA_COLUMN_CODE, nullable = false, unique = true)
    private String cetape_code = "";
    @Size(min = 1, max = 200)
    @Column(name = CaracteristiqueEtapes.JPA_COLUMN_INTITULE, unique = true, nullable = false, length = 200)
    private String cetape_intitule = "";
    @Column(name=CaracteristiqueEtapes.JPA_COLUMN_COMENT)
    private String commentaire="";

    /**
     *
     */
    public CaracteristiqueEtapes() {
    }

    /**
     *
     * @param intitule
     * @param commentaire
     */
    public CaracteristiqueEtapes( String intitule,String commentaire) {
        super();
        this.cetape_intitule = intitule;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getCetape_code() {
        return cetape_code;
    }

    /**
     *
     * @return
     */
    public long getCetape_id() {
        return cetape_id;
    }

    /**
     *
     * @return
     */
    public String getCetape_intitule() {
        return cetape_intitule;
    }

    /**
     *
     * @param cetape_code
     */
    public void setCetape_code(String cetape_code) {
        this.cetape_code = cetape_code;
    }

    /**
     *
     * @param cetape_id
     */
    public void setCetape_id(long cetape_id) {
        this.cetape_id = cetape_id;
    }

    /**
     *
     * @param cetape_intitule
     */
    public void setCetape_intitule(String cetape_intitule) {
        this.cetape_intitule = cetape_intitule;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    
}
