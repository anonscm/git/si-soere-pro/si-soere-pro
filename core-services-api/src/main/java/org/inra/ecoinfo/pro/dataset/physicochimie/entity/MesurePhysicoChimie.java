/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.entity;

import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
@MappedSuperclass
public class MesurePhysicoChimie<M extends MesurePhysicoChimie, V extends ValeurPhysicoChimie> implements Serializable{

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     *
     */
    public static final String COLUMN_JPA_LIGNE_FICHIER_ECHANGE = "ligne_fichier_echange";

    /**
     *
     */
    public static final String COLUMN_JPA_LOCAL_DATE = "localDatePrelevement";
    @Id
    @Column(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE, nullable = false)
    private LocalDate localDatePrelevement;

    @Column(name = COLUMN_JPA_LIGNE_FICHIER_ECHANGE)
    Long ligneFichierEchange;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private VersionFile versionfile;

    /**
     *
     */
    public MesurePhysicoChimie() {
    }

    /**
     *
     * @param localDatePrelevement
     * @param ligneFichierEchange
     * @param versionfile
     */
    public MesurePhysicoChimie(LocalDate localDatePrelevement, Long ligneFichierEchange, VersionFile versionfile) {
        this.localDatePrelevement = localDatePrelevement;
        this.ligneFichierEchange = ligneFichierEchange;
        this.versionfile = versionfile;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDatePrelevement() {
        return localDatePrelevement;
    }

    /**
     *
     * @param localDatePrelevement
     */
    public void setLocalDatePrelevement(LocalDate localDatePrelevement) {
        this.localDatePrelevement = localDatePrelevement;
    }


    /**
     *
     * @return
     */
    public LocalDate getDatePrelevement() {
        return localDatePrelevement;
    }

    /**
     *
     * @param localDatePrelevement
     */
    public void setDatePrelevement(LocalDate localDatePrelevement) {
        this.localDatePrelevement = localDatePrelevement;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionfile() {
        return versionfile;
    }

    /**
     *
     * @param versionfile
     */
    public void setVersionfile(VersionFile versionfile) {
        this.versionfile = versionfile;
    }
    
}
