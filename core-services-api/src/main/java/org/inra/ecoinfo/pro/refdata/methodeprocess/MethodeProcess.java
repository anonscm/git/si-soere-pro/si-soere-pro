
package org.inra.ecoinfo.pro.refdata.methodeprocess;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MethodeProcess.JPA_NAME_ENTITY)
public class MethodeProcess implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "methodeprocess";

    /**
     *
     */
    public static final String ID_JPA = "methodep_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "methodep_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "methodep_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT="commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = MethodeProcess.ID_JPA, nullable = false, unique = true, updatable = false)
    private long methodep_id = 0;
    @Column(name = MethodeProcess.JPA_COLUMN_CODE, nullable = false, unique = true)
    private String methodep_code = "";
    @Column(name = MethodeProcess.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String methodep_nom = "";
    @Column(name=MethodeProcess.JPA_COLUMN_COMENT)
    private String commentaire ="";

    /**
     *
     */
    public MethodeProcess() {

    }

    /**
     *
     * @param nom
     * @param commentaire
     */
    public MethodeProcess( String nom,String commentaire) {
        this.methodep_nom = nom;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getMethodep_code() {
        return methodep_code;
    }

    /**
     *
     * @return
     */
    public long getMethodep_id() {
        return methodep_id;
    }

    /**
     *
     * @return
     */
    public String getMethodep_nom() {
        return methodep_nom;
    }

    /**
     *
     * @param methodep_code
     */
    public void setMethodep_code(String methodep_code) {
        this.methodep_code = methodep_code;
    }

    /**
     *
     * @param methodep_id
     */
    public void setMethodep_id(long methodep_id) {
        this.methodep_id = methodep_id;
    }

    /**
     *
     * @param methodep_nom
     */
    public void setMethodep_nom(String methodep_nom) {
        this.methodep_nom = methodep_nom;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
