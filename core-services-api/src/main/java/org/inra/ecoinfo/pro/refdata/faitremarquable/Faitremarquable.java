package org.inra.ecoinfo.pro.refdata.faitremarquable;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Faitremarquable.NAME_ENTITY_JPA )
public class Faitremarquable implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "faitremarquable";

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Faitremarquable.ID_JPA, updatable = false)
    private long id = 0;
    @Column(name=Faitremarquable.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name= Faitremarquable.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String nom = "";
    
    /**
     *
     */
    public Faitremarquable (){
        super();
    }

    /**
     *
     * @param code
     * @param nom
     */
    public Faitremarquable (String code,String nom){
        this.code = code;
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}
