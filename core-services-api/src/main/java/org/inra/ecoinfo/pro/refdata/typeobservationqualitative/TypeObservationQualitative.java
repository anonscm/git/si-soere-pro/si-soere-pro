/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeobservationqualitative;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=TypeObservationQualitative.NAME_ENTITY_JPA)
public class TypeObservationQualitative implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "type_observation_qua";

    /**
     *
     */
    public static final String JPA_ID = "type_observation_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "type_observation_nom"; 

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "type_observation_code";
    
    @Id
    @Column(name = TypeObservationQualitative.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long type_observation_id;

    
    @Column(name = TypeObservationQualitative.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String type_observation_nom;
    @Column(name = TypeObservationQualitative.JPA_COLUMN_CODE, unique = true, nullable = false, length = 150)
    private String type_observation_code;

    /**
     *
     */
    public TypeObservationQualitative() {
        super();
    }

    /**
     *
     * @param type_observation_id
     * @param type_observation_nom
     * @param type_observation_code
     */
    public TypeObservationQualitative(long type_observation_id, String type_observation_nom, String type_observation_code) {
        this.type_observation_id = type_observation_id;
        this.type_observation_nom = type_observation_nom;
        this.type_observation_code = type_observation_code;
    }

    /**
     *
     * @param type_observation_nom
     * @param type_observation_code
     */
    public TypeObservationQualitative(String type_observation_nom, String type_observation_code) {
        this.type_observation_nom = type_observation_nom;
        this.type_observation_code = type_observation_code;
    }

    /**
     *
     * @return
     */
    public long getType_observation_id() {
        return type_observation_id;
    }

    /**
     *
     * @param type_observation_id
     */
    public void setType_observation_id(long type_observation_id) {
        this.type_observation_id = type_observation_id;
    }

    /**
     *
     * @return
     */
    public String getType_observation_nom() {
        return type_observation_nom;
    }

    /**
     *
     * @param type_observation_nom
     */
    public void setType_observation_nom(String type_observation_nom) {
        this.type_observation_nom = type_observation_nom;
    }

    /**
     *
     * @return
     */
    public String getType_observation_code() {
        return type_observation_code;
    }

    /**
     *
     * @param type_observation_code
     */
    public void setType_observation_code(String type_observation_code) {
        this.type_observation_code = type_observation_code;
    }
    
    
}
