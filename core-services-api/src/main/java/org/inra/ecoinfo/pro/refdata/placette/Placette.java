/**
 *
 */
package org.inra.ecoinfo.pro.refdata.placette;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Placette.TABLE_NAME, uniqueConstraints = 
        @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_CODE_PLACT, RefDataConstantes.PARCELLEELT_ID}))
public class Placette implements Serializable
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PLACETTE_ID; // plc_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PLACETTE_TABLE_NAME; // placette
    
    @Id
    @Column(name = Placette.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = ParcelleElementaire.class)
    @JoinColumn(name = RefDataConstantes.PARCELLEELT_ID, referencedColumnName = ParcelleElementaire.ID_JPA, nullable = false)
    private ParcelleElementaire parcelleElementaire;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Geolocalisation.class)
    @JoinColumn(name = RefDataConstantes.GEOLOC_ID, referencedColumnName = Geolocalisation.ID_JPA, nullable = true)
    private Geolocalisation geolocalisation;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_PLACT, length = 100)
    private String nom;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_CODE_PLACT, length = 2)
    private String code;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ANNEEDEBUT_PLACT, length = 4)
    private String anneeDebut;
    
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEFIN_PLACT, length = 4)
    private String anneeFin;
    
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMMENTAIRE_PLACT, length = 500)
    private String commentaire;
    
    /**
     *
     */
    public Placette() 
    {
        super();
    }
    
    /**
	 * @param parcelleElementaire
	 * @param geolocalisation
	 * @param nom
	 * @param code
	 * @param anneeDebut
	 * @param anneeFin
	 * @param commentaire
	 */
	public Placette(ParcelleElementaire parcelleElementaire,
			Geolocalisation geolocalisation, String nom, String code,
			String anneeDebut, String anneeFin, String commentaire) {
		super();
		this.parcelleElementaire = parcelleElementaire;
		this.geolocalisation = geolocalisation;
		this.nom = nom;
		this.code = code;
		this.anneeDebut = anneeDebut;
		this.anneeFin = anneeFin;
		this.commentaire = commentaire;
	}

    /**
     *
     * @return
     */
    public String getAnneeDebut() {
        return anneeDebut;
    }
    
    /**
     *
     * @return
     */
    public String getAnneeFin() {
        return anneeFin;
    }
    
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    
    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }
    
    /**
     *
     * @return
     */
    public Geolocalisation getGeolocalisation() {
        return geolocalisation;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @return
     */
    public ParcelleElementaire getParcelleElementaire() {
        return parcelleElementaire;
    }
    
    /**
     *
     * @param anneeDebut
     */
    public void setAnneeDebut(String anneeDebut) {
        this.anneeDebut = anneeDebut;
    }
    
    /**
     *
     * @param anneeFin
     */
    public void setAnneeFin(String anneeFin) {
        this.anneeFin = anneeFin;
    }
    
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    /**
     *
     * @param geolocalisation
     */
    public void setGeolocalisation(Geolocalisation geolocalisation) {
        this.geolocalisation = geolocalisation;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
     *
     * @param parcelleElementaire
     */
    public void setParcelleElementaire(ParcelleElementaire parcelleElementaire) {
        this.parcelleElementaire = parcelleElementaire;
    }

    /**
     *
     * @return
     */
    public Long getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
		this.id = id;
	}
    
    
}
