/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.vo;

import java.io.Serializable;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;

/**
 *
 * @author adiankha
 */
public class VariableVO implements Serializable {

    String localizedName;

    private VariablesPRO variablesPRO;
    private String group;
    private GroupeVariableVO groupeVariable;
    private Boolean isDataAvailable = false;

    /**
     *
     */
    public VariableVO() {
        super();
    }

    /**
     *
     * @param variable
     */
    public VariableVO(final VariablesPRO variable) {
        super();
        this.variablesPRO = variable;
        this.setLocalizedName(variable.getName());
    }

    /**
     *
     * @param variable
     * @param group
     */
    public VariableVO(VariablesPRO variable, String group) {
        this.variablesPRO = variable;
        // this.ordreAffichageGroupe = ordreAffichageGroupe;
        this.group = group;
    }

    /**
     *
     * @return
     */
    public String getLocalizedName() {
        return localizedName;
    }

    /**
     *
     * @param localizedName
     */
    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    /**
     *
     * @return
     */
    public GroupeVariableVO getGroupeVariable() {
        return groupeVariable;
    }

    /**
     *
     * @param groupeVariable
     */
    public void setGroupeVariable(GroupeVariableVO groupeVariable) {
        this.groupeVariable = groupeVariable;
    }

    /**
     *
     * @return
     */
    public Boolean getIsDataAvailable() {
        return isDataAvailable;
    }

    /**
     *
     * @param isDataAvailable
     */
    public void setIsDataAvailable(Boolean isDataAvailable) {
        this.isDataAvailable = isDataAvailable;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return variablesPRO.getId();
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return variablesPRO.getName();
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return variablesPRO.getCode();
    }

    /**
     *
     * @return
     */
    public String getGroup() {
        return group;
    }

    /**
     *
     * @param group
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     *
     * @return
     */
    public String getAffichage() {
        return variablesPRO.getAffichage();
    }

    /**
     *
     * @return
     */
    public VariablesPRO getVariablesPRO() {
        return variablesPRO;
    }
}