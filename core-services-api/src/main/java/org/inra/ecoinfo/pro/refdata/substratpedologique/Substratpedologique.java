package org.inra.ecoinfo.pro.refdata.substratpedologique;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Substratpedologique.NAME_ENTITY_JPA )
public class Substratpedologique implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "substratpedologique";

    /**
     *
     */
    public static final String ID_JPA = "sp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Substratpedologique.ID_JPA)
    private long sp_id = 0;
    @Column(name=Substratpedologique.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name= Substratpedologique.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String nom = "";
    
    /**
     *
     */
    public Substratpedologique(){
        super();
    }

    /**
     *
     * @param nom
     */
    public Substratpedologique (String nom){
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getSp_id() {
        return sp_id;
    }

    /**
     *
     * @param sp_id
     */
    public void setSp_id(long sp_id) {
        this.sp_id = sp_id;
    }
    
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    

}
