package org.inra.ecoinfo.pro.refdata.nomregionalsol;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Nomregionalsol.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    Typepedologique.ID_JPA,
    Nomregionalsol.JPA_COLUMN_NAME,
    Typesolarvalis.ID_JPA,
    Region.ID_JPA
}))
public class Nomregionalsol implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "nomregionalsol";

    /**
     *
     */
    public static final String ID_JPA = "nr_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Nomregionalsol.ID_JPA)
    private long nr_id = 0;
    @Column(name = Nomregionalsol.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name = Nomregionalsol.JPA_COLUMN_NAME, unique = false, nullable = false)
    private String nom = "";
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Typepedologique.ID_JPA, nullable = false, unique = false)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Typepedologique typepedologique;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Region.ID_JPA, nullable = false, unique = false)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Region region;

    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Typesolarvalis.ID_JPA, nullable = false, unique = false)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Typesolarvalis typesolarvalis;

    /**
     *
     */
    public Nomregionalsol() {
        super();
    }

    /**
     *
     * @param nom
     * @param typepedo
     * @param typearvalis
     * @param region
     */
    public Nomregionalsol(String nom, Typepedologique typepedo, Typesolarvalis typearvalis, Region region) {
        this.nom = nom;
        this.typepedologique = typepedo;
        this.typesolarvalis = typearvalis;
        this.region = region;

    }

    /**
     *
     * @return
     */
    public long getNr_id() {
        return nr_id;
    }

    /**
     *
     * @param nr_id
     */
    public void setNr_id(long nr_id) {
        this.nr_id = nr_id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public Typepedologique getTypepedologique() {
        return typepedologique;
    }

    /**
     *
     * @param typepedologique
     */
    public void setTypepedologique(Typepedologique typepedologique) {
        this.typepedologique = typepedologique;
    }

    /**
     *
     * @return
     */
    public Typesolarvalis getTypesolarvalis() {
        return typesolarvalis;
    }

    /**
     *
     * @param typesolarvalis
     */
    public void setTypesolarvalis(Typesolarvalis typesolarvalis) {
        this.typesolarvalis = typesolarvalis;
    }

    /**
     *
     * @return
     */
    public Region getRegion() {
        return region;
    }

    /**
     *
     * @param region
     */
    public void setRegion(Region region) {
        this.region = region;
    }

}
