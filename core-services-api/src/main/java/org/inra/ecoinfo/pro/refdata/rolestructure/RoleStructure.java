/**
 *
 */
package org.inra.ecoinfo.pro.refdata.rolestructure;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.COLUMN_LIBELLE_ROLESTR
 *
 * @author ptcherniati
 */
@Entity
@Table(name = RoleStructure.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_LIBELLE_ROLESTR}))
public class RoleStructure implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.ROLESTR_ID; // rstr_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.ROLESTR_TABLE_NAME; // role_structure
    @Id
    @Column(name = RoleStructure.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_ROLESTR, length = 100)
    private String libelle;

    /**
     *
     */
    public RoleStructure() {
        super();
    }

    /**
     *
     * @param libelle
     */
    public RoleStructure(String libelle) {
        super();
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
