/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;


/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name = ValeurTravailDuSol.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {RealNode.ID_JPA,
                                                                                                        MesureTravailDuSol.ATTRIBUTE_JPA_ID}))
@PrimaryKeyJoinColumn(name = ValeurITK.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurTravailDuSol.ATTRIBUTE_JPA_ID)
@AttributeOverrides({
    @AttributeOverride(name = ValeurITK.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurTravailDuSol.ATTRIBUTE_JPA_ID))})


public class ValeurTravailDuSol extends ValeurITK<MesureTravailDuSol, ValeurTravailDuSol> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurinterventiontravaildusol";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "valeurid";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureTravailDuSol.class)
    @JoinColumn(name = MesureTravailDuSol.ATTRIBUTE_JPA_ID, referencedColumnName = MesureTravailDuSol.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureTravailDuSol mesuretravaildusol;

    /**
     *
     */
    public ValeurTravailDuSol() {
        super();
    }

    /**
     *
     * @param valeur
     * @param mesuretravaildusol
     * @param realNode
     */
    public ValeurTravailDuSol(Float valeur, MesureTravailDuSol mesuretravaildusol, RealNode realNode) {
        super(valeur, realNode);
        this.mesuretravaildusol = mesuretravaildusol;
    }

    /**
     *
     * @param mesuretravaildusol
     * @param listeItineraires
     * @param realNode
     */
    public ValeurTravailDuSol(MesureTravailDuSol mesuretravaildusol, ListeItineraire listeItineraires, RealNode realNode) {
        super(listeItineraires, realNode);
        this.mesuretravaildusol = mesuretravaildusol;
    }

    public MesureTravailDuSol getMesuretravaildusol() {
        return mesuretravaildusol;
    }

    public void setMesuretravaildusol(MesureTravailDuSol mesuretravaildusol) {
        this.mesuretravaildusol = mesuretravaildusol;
    }
}
