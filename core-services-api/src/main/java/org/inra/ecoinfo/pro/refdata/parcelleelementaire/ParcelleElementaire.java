package org.inra.ecoinfo.pro.refdata.parcelleelementaire;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.utils.StringUtils;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ParcelleElementaire.TABLE_NAME, uniqueConstraints
        = {
            @UniqueConstraint(columnNames = {CodeParcelleElementaire.ID_JPA, Dispositif.ID_JPA}),
            @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_MY_CODE_PRCELT, Bloc.ID_JPA})
        })
@PrimaryKeyJoinColumn(name = ParcelleElementaire.ID_JPA)
public class ParcelleElementaire extends Nodeable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PARCELLEELT_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PARCELLEELT_TABLE_NAME;

    /**
     *
     */
    public static final Pattern REGEXP_PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU = Pattern.compile("\\s*(.*!?)\\s*-\\s*((.*!?)s*\\(\\s*(.*!?)\\s*\\))\\s*");

    /**
     *
     */
    public static final String PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU = "%s - %s (%s)";

    /**
     *
     * @param codeDispositif_nomLieu
     * @return
     */
    public static String getCodeParcelle(String codeParcelle_Dispositif_nomLieu){
        Matcher matcher = REGEXP_PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU.matcher(codeParcelle_Dispositif_nomLieu);
        if(matcher.matches()){
            return matcher.group(1).trim();
        }
        return codeParcelle_Dispositif_nomLieu.trim();
    }

    public static String getNomDispositif_nomLieu(String codeParcelle_Dispositif_nomLieu){
        Matcher matcher = REGEXP_PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU.matcher(codeParcelle_Dispositif_nomLieu);
        if(matcher.matches()){
            return matcher.group(2).trim();
        }
        return codeParcelle_Dispositif_nomLieu.trim();
    }
    
    public static String getNomDispositif(String codeParcelle_Dispositif_nomLieu){
        Matcher matcher = REGEXP_PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU.matcher(codeParcelle_Dispositif_nomLieu);
        if(matcher.matches()){
            return matcher.group(3).trim();
        }
        return codeParcelle_Dispositif_nomLieu.trim();
    }
    
    public static String getNomLieu(String codeParcelle_Dispositif_nomLieu){
        Matcher matcher = REGEXP_PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU.matcher(codeParcelle_Dispositif_nomLieu);
        if(matcher.matches()){
            return matcher.group(4).trim();
        }
        return codeParcelle_Dispositif_nomLieu.trim();
    }
    
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Bloc.class)
    @JoinColumn(name = RefDataConstantes.BLOC_ID, referencedColumnName = Bloc.ID_JPA, nullable = true)
    private Bloc bloc;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Geolocalisation.class)
    @JoinColumn(name = RefDataConstantes.GEOLOC_ID, referencedColumnName = Geolocalisation.ID_JPA, nullable = true)
    private Geolocalisation geolocalisation;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_PRCELT, length = 100)
    private String nom;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_MY_CODE_PRCELT, length = 100)
    private String myCode;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = CodeParcelleElementaire.class)
    @JoinColumn(name = RefDataConstantes.CODEPARCELLEELT_ID, referencedColumnName = CodeParcelleElementaire.ID_JPA, nullable = false)
    private CodeParcelleElementaire codeParcelleElementaire;

    /**
     *
     */
    public ParcelleElementaire() {
        super();
    }

    /**
     *
     * @param dispositif
     * @param bloc
     * @param geolocalisation
     * @param nom
     * @param codeParcelleElementaire
     */
    public ParcelleElementaire(Dispositif dispositif, Bloc bloc, Geolocalisation geolocalisation, String nom, CodeParcelleElementaire codeParcelleElementaire) {
        super(Utils.createCodeFromString(String.format("%s_%s", dispositif.getCode(), codeParcelleElementaire.getCode())));
        this.dispositif = dispositif;
        this.bloc = bloc;
        this.geolocalisation = geolocalisation;
        this.nom = nom;
        this.myCode= Utils.createCodeFromString(nom);
        this.codeParcelleElementaire = codeParcelleElementaire;
    }

    /**
     *
     * @return
     */
    public Bloc getBloc() {
        return bloc;
    }

    /**
     *
     * @return
     */
    public CodeParcelleElementaire getCodeParcelleElementaire() {
        return codeParcelleElementaire;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Geolocalisation getGeolocalisation() {
        return geolocalisation;
    }

    /**
     *
     * @param bloc
     */
    public void setBloc(Bloc bloc) {
        this.bloc = bloc;
    }

    /**
     *
     * @param codeParcelleElementaire
     */
    public void setCodeParcelleElementaire(CodeParcelleElementaire codeParcelleElementaire) {
        this.codeParcelleElementaire = codeParcelleElementaire;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param geolocalisation
     */
    public void setGeolocalisation(Geolocalisation geolocalisation) {
        this.geolocalisation = geolocalisation;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMyCode() {
        return myCode;
    }

    public void setMyCode(String myCode) {
        this.myCode = myCode;
    }

    /**
     *
     * @return
     */
    protected String getAlphanumeCode() {
        return StringUtils.orderedString(nom);
    }

    @Override
    public int compareTo(final INodeable o) {
        if (o == null) {
            return -1;
        } else if (!(o instanceof ParcelleElementaire)) {
            return -1;
        }
        ParcelleElementaire oo = (ParcelleElementaire) o;
        if (this.getDispositif().compareTo(oo.getDispositif()) != 0) {
            return this.getDispositif().compareTo(oo.getDispositif());

        }
        return this.getAlphanumeCode().compareTo(oo.getAlphanumeCode());
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final ParcelleElementaire other = (ParcelleElementaire) obj;
        if (this.codeParcelleElementaire == null && other.codeParcelleElementaire != null) {
            return false;
        } else if (!this.codeParcelleElementaire.equals(other.codeParcelleElementaire)) {
            return false;
        } else if (!this.dispositif.getCode().equals(other.dispositif.getCode())) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return nom;
    }

    /**
     *
     * @return
     */
    @Override
    public Class<ParcelleElementaire> getNodeableType() {
        return ParcelleElementaire.class;
    }

    /**
     *
     * @return
     */
    public String getCodeParcelleNomDispositif_nomLieu(){
        String codeDipositif = Optional.ofNullable(getDispositif())
                .map(d -> d.getCodeDispo())
                .orElse("");
        String nomLieu = Optional.ofNullable(getDispositif())
                .map(d -> d.getLieu())
                .map(l -> l.getNom())
                .orElse("");
        return String.format(PATTERN_CODE_PARCELLE_NOM_DISPOSITIF_NOM_LIEU, 
                getCodeParcelleElementaire().getCode(),
                codeDipositif,
                nomLieu
        );
    }

}
