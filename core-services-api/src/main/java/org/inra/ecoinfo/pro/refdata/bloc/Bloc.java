/**
 *
 */
package org.inra.ecoinfo.pro.refdata.bloc;

import java.io.Serializable;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.codebloc.CodeBloc;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author ptcherniati
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = Bloc.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_BLOC, Dispositif.ID_JPA}))
public class Bloc  implements Serializable
{

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.BLOC_ID; 

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.BLOC_TABLE_NAME;
    
    @Id
    @Column(name = Bloc.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
     @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_BLOC, length = 155)
    private String nom;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = CodeBloc.class)
    @JoinColumn(name = RefDataConstantes.CODEBLOC_ID, referencedColumnName = CodeBloc.ID_JPA, nullable = false)
    private CodeBloc code;
    
    /**
     *
     */
    public Bloc() 
    {
        super();
    }
    
    /**
     *
     * @param dispositif
     * @param nom
     * @param code
     */
    public Bloc(Dispositif dispositif, String nom, CodeBloc code) 
    {
        super();
        this.dispositif = dispositif;
        this.nom = nom;
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public CodeBloc getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param code
     */
    public void setCode(CodeBloc code) {
        this.code = code;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public Long getId() {
		return id;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
		this.id = id;
	}
    

}
