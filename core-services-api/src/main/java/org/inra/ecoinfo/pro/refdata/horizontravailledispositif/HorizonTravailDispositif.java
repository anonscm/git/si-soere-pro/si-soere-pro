/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizontravailledispositif;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.Teneurcalcaireinitial;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=HorizonTravailDispositif.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                      Dispositif.ID_JPA,
                                                                      Teneurcalcaireinitial.ID_JPA,
                                                                      HorizonTravailDispositif.COLUMN_TENEURCARBONEORGENIQUE_JPA
                                                                  
       
}))
public class HorizonTravailDispositif implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "horizontravaildispositif";

    /**
     *
     */
    public static final String ID_JPA = "htd_id";

    /**
     *
     */
    public static final String COLUMN_HORIZONSURFACE_JPA ="horizon_surface";

    /**
     *
     */
    public static final String COLUMN_TENEURCARBONEORGENIQUE_JPA = "teneurcarbone";

    /**
     *
     */
    public static final String COLUMN_POURCENTCAILLOUX_JPA ="pourcentcailloux";

    /**
     *
     */
    public static final String COLUMN_ELEMENTGROSSIERS_JPA ="elementgrossiers";

    /**
     *
     */
    public static final String COLUMN_PHINITIALPH_JPA ="phinitial"; 

    /**
     *
     */
    public static final String COLUMN_COMMENT_JPA = "commentaire";

    /**
     *
     * @return
     */
    public static String getID_JPA() {
         return ID_JPA;
     }
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name=HorizonTravailDispositif.ID_JPA,updatable=false)
     private long htd_id =0;
     
     @Column(name=HorizonTravailDispositif.COLUMN_HORIZONSURFACE_JPA,nullable = false ,unique = true)
     private double horizon_surface=0.0;
     
     @Column(name=HorizonTravailDispositif.COLUMN_TENEURCARBONEORGENIQUE_JPA,nullable = false , unique = false)
     private double teneurcarbone =0.0;
     
     @Column(name=HorizonTravailDispositif.COLUMN_POURCENTCAILLOUX_JPA,nullable = true,unique = false)
     private double pourcentcailloux = 0.0;
     
     @Column(name=HorizonTravailDispositif.COLUMN_ELEMENTGROSSIERS_JPA,nullable = true,unique = false)
     private  String elementgrossiers;
     
     @Column(name=HorizonTravailDispositif.COLUMN_PHINITIALPH_JPA,nullable = true,unique = false)
     private double phinitial = 0.0;
     
     @Column(name=HorizonTravailDispositif.COLUMN_COMMENT_JPA)
     private String commentaire ="";
     
     @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
     @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
     private Dispositif dispositif;
     
     @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Teneurcalcaireinitial.ID_JPA, nullable = false)
    private Teneurcalcaireinitial teneur;
     
    /**
     *
     */
    public HorizonTravailDispositif(){
         super();
     }
     
    /**
     *
     * @param phinitial
     * @param horizon
     * @param carbone
     * @param cailloux
     * @param grossiers
     * @param comment
     * @param dispositif
     * @param teneur
     */
    public HorizonTravailDispositif(double phinitial,double horizon,double carbone,double cailloux,String grossiers,String comment
                                    ,Dispositif dispositif,Teneurcalcaireinitial teneur ){
         this.phinitial = phinitial;
         this.horizon_surface = horizon;
         this.pourcentcailloux = cailloux;
         this.teneurcarbone = carbone;
         this.elementgrossiers = grossiers;
         this.commentaire = comment;
         this.dispositif = dispositif;
         this.teneur = teneur;
     }

    /**
     *
     * @return
     */
    public double getHorizon_surface() {
        return horizon_surface;
    }

    /**
     *
     * @return
     */
    public double getTeneurcarbone() {
        return teneurcarbone;
    }

    /**
     *
     * @return
     */
    public double getPourcentcailloux() {
        return pourcentcailloux;
    }

    /**
     *
     * @return
     */
    public double getPhinitial() {
        return phinitial;
    }

    /**
     *
     * @return
     */
    public long getHtd_id() {
        return htd_id;
    }

    /**
     *
     * @param htd_id
     */
    public void setHtd_id(long htd_id) {
        this.htd_id = htd_id;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param horizon_surface
     */
    public void setHorizon_surface(double horizon_surface) {
        this.horizon_surface = horizon_surface;
    }

    /**
     *
     * @param teneurcarbone
     */
    public void setTeneurcarbone(double teneurcarbone) {
        this.teneurcarbone = teneurcarbone;
    }

    /**
     *
     * @param pourcentcailloux
     */
    public void setPourcentcailloux(double pourcentcailloux) {
        this.pourcentcailloux = pourcentcailloux;
    }

    /**
     *
     * @param phinitial
     */
    public void setPhinitial(double phinitial) {
        this.phinitial = phinitial;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @return
     */
    public String getElementgrossiers() {
        return elementgrossiers;
    }

    /**
     *
     * @param elementgrossiers
     */
    public void setElementgrossiers(String elementgrossiers) {
        this.elementgrossiers = elementgrossiers;
    }
    
    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Teneurcalcaireinitial getTeneur() {
        return teneur;
    }

    /**
     *
     * @param teneur
     */
    public void setTeneur(Teneurcalcaireinitial teneur) {
        this.teneur = teneur;
    }

   
     
    
}
