/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeintervention;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=TypeIntervention.NAME_ENTITY_JPA)

public class TypeIntervention implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "type_intervention";

    /**
     *
     */
    public static final String JPA_ID = "type_intervention_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "type_intervention_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_OBJECTIF = "type_intervention_objectif";

    /**
     *
     */
    public static final String JPA_COLUMN_SOURCE = "type_intervention_source";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "type_intervention_code";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENTAIRE = "type_intervention_commentaire";
    
    @Id
    @Column(name = TypeIntervention.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long type_intervention_id;

    
    @Column(name = TypeIntervention.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String type_intervention_nom;
    
    @Column(name = TypeIntervention.JPA_COLUMN_CODE, unique = true, nullable = false, length = 150)
    private String type_intervention_code;
    
    @Column(name = TypeIntervention.JPA_COLUMN_OBJECTIF, unique = false, nullable = true, length = 150)
    private String type_intervention_objectif;
    
    @Column(name = TypeIntervention.JPA_COLUMN_SOURCE, unique = false, nullable = true, length = 150)
    private String type_intervention_source;
    
    @Column(name = TypeIntervention.JPA_COLUMN_COMMENTAIRE, unique = false, nullable = true, length = 150)
    private String type_intervention_commentaire;

    /**
     *
     * @param type_intervention_nom
     * @param type_intervention_code
     * @param type_intervention_objectif
     * @param type_intervention_source
     * @param type_intervention_commentaite
     */
    public TypeIntervention(String type_intervention_nom, String type_intervention_code, String type_intervention_objectif, String type_intervention_source, String type_intervention_commentaite) {
        this.type_intervention_nom = type_intervention_nom;
        this.type_intervention_code = type_intervention_code;
        this.type_intervention_objectif = type_intervention_objectif;
        this.type_intervention_source = type_intervention_source;
        this.type_intervention_commentaire = type_intervention_commentaite;
    }

    /**
     *
     */
    public TypeIntervention() {
        super();
    }
   
    /**
     *
     * @return
     */
    public long getType_intervention_id() {
        return type_intervention_id;
    }

    /**
     *
     * @param type_intervention_id
     */
    public void setType_intervention_id(long type_intervention_id) {
        this.type_intervention_id = type_intervention_id;
    }

    /**
     *
     * @return
     */
    public String getType_intervention_nom() {
        return type_intervention_nom;
    }

    /**
     *
     * @param type_intervention_nom
     */
    public void setType_intervention_nom(String type_intervention_nom) {
        this.type_intervention_nom = type_intervention_nom;
    }

    /**
     *
     * @return
     */
    public String getType_intervention_objectif() {
        return type_intervention_objectif;
    }

    /**
     *
     * @param type_intervention_objectif
     */
    public void setType_intervention_objectif(String type_intervention_objectif) {
        this.type_intervention_objectif = type_intervention_objectif;
    }

    /**
     *
     * @return
     */
    public String getType_intervention_source() {
        return type_intervention_source;
    }

    /**
     *
     * @param type_intervention_source
     */
    public void setType_intervention_source(String type_intervention_source) {
        this.type_intervention_source = type_intervention_source;
    }

    /**
     *
     * @return
     */
    public String getType_intervention_code() {
        return type_intervention_code;
    }

    /**
     *
     * @param type_intervention_code
     */
    public void setType_intervention_code(String type_intervention_code) {
        this.type_intervention_code = type_intervention_code;
    }

    /**
     *
     * @return
     */
    public String getType_intervention_commentaire() {
        return type_intervention_commentaire;
    }

    /**
     *
     * @param type_intervention_commentaire
     */
    public void setType_intervention_commentaire(String type_intervention_commentaire) {
        this.type_intervention_commentaire = type_intervention_commentaire;
    }

   
    
    
     
}
