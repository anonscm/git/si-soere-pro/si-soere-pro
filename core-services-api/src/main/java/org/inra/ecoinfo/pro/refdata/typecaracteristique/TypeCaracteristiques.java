
package org.inra.ecoinfo.pro.refdata.typecaracteristique;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeCaracteristiques.JPA_ENTITY_NAME, uniqueConstraints = {@UniqueConstraint(columnNames = TypeCaracteristiques.JPA_ID), @UniqueConstraint(columnNames = TypeCaracteristiques.JPA_COLUMN_NAME)})
public class TypeCaracteristiques implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String JPA_ENTITY_NAME = "typecaracteristique";

    /**
     *
     */
    public static final String JPA_ID = "tcar_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "tcar_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "tcar_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @GeneratedValue
    @Column(name = TypeCaracteristiques.JPA_ID, nullable = false, updatable = false, unique = true)
    private long tcar_id = 0;
    @Column(name = TypeCaracteristiques.JPA_COLUMN_CODE, nullable = false, unique = true)
    private String tcar_code = "";
    @Column(name = TypeCaracteristiques.JPA_COLUMN_NAME, unique = true, nullable = false, length = 200)
    private String tcar_nom = "";
    @Column(name = TypeCaracteristiques.JPA_COLUMN_COMENT)
    private String commentaire ="";

    /**
     *
     */
    public TypeCaracteristiques() {

    }

    /**
     *
     * @param nom
     * @param commentaire
     */
    public TypeCaracteristiques(String nom,String commentaire) {
        super();
        this.tcar_nom = nom;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getTcar_code() {
        return tcar_code;
    }

    /**
     *
     * @return
     */
    public long getTcar_id() {
        return tcar_id;
    }

    /**
     *
     * @return
     */
    public String getTcar_nom() {
        return tcar_nom;
    }

    /**
     *
     * @param tcar_code
     */
    public void setTcar_code(String tcar_code) {
        this.tcar_code = tcar_code;
    }

    /**
     *
     * @param tcar_id
     */
    public void setTcar_id(long tcar_id) {
        this.tcar_id = tcar_id;
    }

    /**
     *
     * @param tcar_nom
     */
    public void setTcar_nom(String tcar_nom) {
        this.tcar_nom = tcar_nom;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
