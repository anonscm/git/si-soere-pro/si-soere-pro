/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.statutvaleur;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;


/**
 *
 * @author adiankha
 */
@Entity
@Table(name=StatutValeur.NAME_ENTITY_JPA)
public class StatutValeur implements Serializable{
    
     private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "statutvaleur";

    /**
     *
     */
    public static final String JPA_ID = "sv_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "sv_code";

    /**
     *
     */
    public static final String JPA_COLUMN_MYKEY = "sv_mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "sv_nom";
    
    @Id
    @Column(name = StatutValeur.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long sv_id;
    @Column(name = StatutValeur.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String sv_code = "";
     @Column(name = StatutValeur.JPA_COLUMN_MYKEY, nullable = false, unique = true)
    private String sv_mycode = "";
    @Column(name = StatutValeur.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String sv_nom = "";
    
    /**
     *
     */
    public StatutValeur(){
        super();
    }
    
    /**
     *
     * @param code
     * @param nom
     */
    public StatutValeur(String code,String nom){
        this.sv_code = code;
        this.sv_nom = nom;
        this.sv_mycode = Utils.createCodeFromString(code);
    }

    /**
     *
     * @return
     */
    public long getSv_id() {
        return sv_id;
    }

    /**
     *
     * @return
     */
    public String getSv_code() {
        return sv_code;
    }

    /**
     *
     * @return
     */
    public String getSv_nom() {
        return sv_nom;
    }

    /**
     *
     * @param sv_id
     */
    public void setSv_id(long sv_id) {
        this.sv_id = sv_id;
    }

    /**
     *
     * @param sv_code
     */
    public void setSv_code(String sv_code) {
        this.sv_code = sv_code;
    }

    /**
     *
     * @param sv_nom
     */
    public void setSv_nom(String sv_nom) {
        this.sv_nom = sv_nom;
    }

    /**
     *
     * @return
     */
    public String getSv_mycode() {
        return sv_mycode;
    }

    /**
     *
     * @param sv_mycode
     */
    public void setSv_mycode(String sv_mycode) {
        this.sv_mycode = sv_mycode;
    }
    
    
}
