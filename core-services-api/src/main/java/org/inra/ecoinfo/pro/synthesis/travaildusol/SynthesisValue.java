/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.travaildusol;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "TravaildusolSynthesisValue")
@Table(indexes = {
    @Index(name = "TravaildusolSynthesisValue_idNode_idx", columnList = "idNode"),
    @Index(name = "TravaildusolSynthesisValue_disp_idx", columnList = "site"),
    @Index(name = "TravaildusolSynthesisValue_disp_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends AbstractProSynthesisValue{

    /**
     *
     */
    public SynthesisValue() {
        super();
    }
    
    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueString
     */
    public SynthesisValue(LocalDate date, String site, String variable, String valueString, Long idNode) {
        super(date.atStartOfDay(), site, variable, null, valueString, idNode);
    }
}