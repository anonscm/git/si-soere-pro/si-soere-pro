/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

import java.io.Serializable;

/**
 *
 * @author adiankha
 */
public interface IErrorsReport extends Serializable{

    /**
     *
     * @param errorMessage
     */
    void addErrorMessage(String errorMessage);

    /**
     *
     * @param errorMessage
     * @param e
     */
    void addErrorMessage(String errorMessage, Throwable e);

    /**
     *
     * @param infoMessage
     */
    void addInfoMessage(String infoMessage);

    /**
     *
     * @return
     */
    String getErrorsMessages();

    /**
     *
     * @return
     */
    String getInfosMessages();

    /**
     *
     * @return
     */
    boolean hasErrors();

    /**
     *
     * @return
     */
    boolean hasInfos();
    
}
