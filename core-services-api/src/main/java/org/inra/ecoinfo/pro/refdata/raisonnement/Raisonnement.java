
package org.inra.ecoinfo.pro.refdata.raisonnement;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Raisonnement.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {
            RefDataConstantes.COLUMN_LIBELLE_RAIS,
            RefDataConstantes.LSTRAISONNEMENT_ID
        }))
public class Raisonnement implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.RAISONNEMENT_ID; // rais_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.RAISONNEMENT_TABLE_NAME; // raisonnement
    @Id
    @Column(name = Raisonnement.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_LIBELLE_RAIS, length = 80)
    private String libelle;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = ListeRaisonnement.class)
    @JoinColumn(name = RefDataConstantes.LSTRAISONNEMENT_ID, referencedColumnName = ListeRaisonnement.ID_JPA, nullable = false)
    private ListeRaisonnement listeRaisonnement;

    /**
     *
     */
    public Raisonnement() {
        super();
    }

    /**
     *
     * @param libelle
     * @param listeRaisonnement
     */
    public Raisonnement(String libelle, ListeRaisonnement listeRaisonnement) {
        super();
        this.libelle = libelle;
        this.listeRaisonnement = listeRaisonnement;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @return
     */
    public ListeRaisonnement getListeRaisonnement() {
        return listeRaisonnement;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     *
     * @param listeRaisonnement
     */
    public void setListeRaisonnement(ListeRaisonnement listeRaisonnement) {
        this.listeRaisonnement = listeRaisonnement;
    }
}
