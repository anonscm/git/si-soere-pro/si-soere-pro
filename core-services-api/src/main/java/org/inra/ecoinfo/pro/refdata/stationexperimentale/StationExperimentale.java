package org.inra.ecoinfo.pro.refdata.stationexperimentale;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.COLUMN_NOM_STATIONEXP}
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StationExperimentale.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_NOM_STATIONEXP}))
public class StationExperimentale implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STATIONEXP_ID; // stx_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STATIONEXP_TABLE_NAME; // station_experimentale
    @Id
    @Column(name = StationExperimentale.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_STATIONEXP, length = 100)
    private String nom;

    /**
     *
     */
    public StationExperimentale() {
        super();
    }

    /**
     *
     * @param nom
     */
    public StationExperimentale(String nom) {
        super();
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}
