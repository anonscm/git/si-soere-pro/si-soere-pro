package org.inra.ecoinfo.pro.refdata.nomenclature;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;
import org.inra.ecoinfo.pro.refdata.filiere.Filiere;
import org.inra.ecoinfo.pro.refdata.precisionfiliere.Precisionfiliere;
import org.inra.ecoinfo.pro.refdata.typenomenclature.TypeNomenclature;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Nomenclatures._NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    Nomenclatures.JPA_COLUMN_NAME,
    TypeNomenclature.ID_JPA

}))
public class Nomenclatures implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String _NAME_ENTITY_JPA = "nomenclature";

    /**
     *
     */
    public static final String JPA_ID = "nomen_id";
    static final String JPA_COLUMN_KEY = "nomen_code";

    /**
     *
     */
    public static final String JPA_COLUMN_MYKEY = "nomen_mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nomen_nom";

    /**
     *
     */
    public static final String JPA_COLUMN__COMMENT = "nomen_comment";

    /**
     *
     */
    public static final String JPA_COLUMN_ETAT = "etatcoproduit";

    /**
     *
     */
    public static final String JPA_COLUMN_AFFICHAGE = "Intitule_Affichage";

    /**
     *
     */
    public static final String JPA_COLUMN_PROVENANCE = "provenance";

    /**
     *
     */
    public static final String JPA_COLUMN_PRECISIONPRO = "precisionpro";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Nomenclatures.JPA_ID, nullable = false, updatable = false)
    private long nomen_id = 0;
    @Column(name = Nomenclatures.JPA_COLUMN_KEY, unique = true, nullable = false, updatable = false)
    private String nomen_code = "";

    @Column(name = Nomenclatures.JPA_COLUMN_MYKEY, unique = true, nullable = false, updatable = false)
    private String nomen_mycode = "";
    @Column(name = Nomenclatures.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String nomen_nom = "";
    @Column(name = Nomenclatures.JPA_COLUMN__COMMENT, length = 300)
    private String nomen_comment = "";
    @Column(name = Nomenclatures.JPA_COLUMN_ETAT)
    private String etatcoproduit = "";
    @Column(name = Nomenclatures.JPA_COLUMN_AFFICHAGE, unique = true, nullable = false)
    private String intitule_affichage = "";
    @Column(name = Nomenclatures.JPA_COLUMN_PROVENANCE, nullable = true, unique = false)
    private String provenance = "";
    @Column(name = Nomenclatures.JPA_COLUMN_PRECISIONPRO)
    private String precisionpro = "";

    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeNomenclature.ID_JPA, nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private TypeNomenclature typenomenclature;

    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Filiere.JPA_ID, nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Filiere filiere;

    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Precisionfiliere.JPA_ID, nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Precisionfiliere precisionfiliere;

    /* @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Precisionpro.JPA_ID,nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Precisionpro precisionpro;*/
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Facteur.ID_JPA, nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Facteur facteur;

    /**
     *
     */
    public Nomenclatures() {
    }

    /**
     *
     * @param nomen_code
     * @param nomen_mycode
     * @param nomen_nom
     * @param nomen_comment
     * @param etatcoproduit
     * @param typenomenclature
     * @param filiere
     * @param precisionfiliere
     * @param precisionpro
     * @param facteur
     * @param provenance
     * @param affichage
     */
    public Nomenclatures(String nomen_code, String nomen_mycode,
            String nomen_nom, String nomen_comment, String etatcoproduit,
            TypeNomenclature typenomenclature, Filiere filiere,
            Precisionfiliere precisionfiliere, String precisionpro,
            Facteur facteur, String provenance, String affichage) {
        super();
        this.nomen_code = nomen_code;
        this.nomen_mycode = nomen_mycode;
        this.nomen_nom = nomen_nom;
        this.nomen_comment = nomen_comment;
        this.etatcoproduit = etatcoproduit;
        this.typenomenclature = typenomenclature;
        this.filiere = filiere;
        this.precisionfiliere = precisionfiliere;
        this.precisionpro = precisionpro;
        this.facteur = facteur;
        this.intitule_affichage = affichage;
        this.provenance = provenance;
    }

    /**
     *
     * @return
     */
    public String getNomen_code() {
        return nomen_code;
    }

    /**
     *
     * @return
     */
    public String getNomen_comment() {
        return nomen_comment;
    }

    /**
     *
     * @return
     */
    public long getNomen_id() {
        return nomen_id;
    }

    /**
     *
     * @return
     */
    public String getNomen_mycode() {
        return nomen_mycode;
    }

    /**
     *
     * @return
     */
    public String getNomen_nom() {
        return nomen_nom;
    }

    /**
     *
     * @param nomen_code
     */
    public void setNomen_code(String nomen_code) {
        this.nomen_code = nomen_code;
    }

    /**
     *
     * @param nomen_comment
     */
    public void setNomen_comment(String nomen_comment) {
        this.nomen_comment = nomen_comment;
    }

    /**
     *
     * @param nomen_id
     */
    public void setNomen_id(long nomen_id) {
        this.nomen_id = nomen_id;
    }

    /**
     *
     * @param nomen_mycode
     */
    public void setNomen_mycode(String nomen_mycode) {
        this.nomen_mycode = nomen_mycode;
    }

    /**
     *
     * @param nomen_nom
     */
    public void setNomen_nom(String nomen_nom) {
        this.nomen_nom = nomen_nom;
    }

    @Override
    public String toString() {
        return "Nomenclature code: " + getNomen_code() + " nom: " + getNomen_nom();
    }

    /**
     *
     * @return
     */
    public Filiere getFiliere() {
        return filiere;
    }

    /**
     *
     * @return
     */
    public Precisionfiliere getPrecisionfiliere() {
        return precisionfiliere;
    }

    /**
     *
     * @param filiere
     */
    public void setFiliere(Filiere filiere) {
        this.filiere = filiere;
    }

    /**
     *
     * @param precisionfiliere
     */
    public void setPrecisionfiliere(Precisionfiliere precisionfiliere) {
        this.precisionfiliere = precisionfiliere;
    }

    /**
     *
     * @return
     */
    public String getPrecisionpro() {
        return precisionpro;
    }

    /**
     *
     * @param precisionpro
     */
    public void setPrecisionpro(String precisionpro) {
        this.precisionpro = precisionpro;
    }

    /**
     *
     * @return
     */
    public String getEtatcoproduit() {
        return etatcoproduit;
    }

    /**
     *
     * @param etatcoproduit
     */
    public void setEtatcoproduit(String etatcoproduit) {
        this.etatcoproduit = etatcoproduit;
    }

    /**
     *
     * @return
     */
    public TypeNomenclature getTypenomenclature() {
        return typenomenclature;
    }

    /**
     *
     * @param typenomenclature
     */
    public void setTypenomenclature(TypeNomenclature typenomenclature) {
        this.typenomenclature = typenomenclature;
    }

    /**
     *
     * @return
     */
    public Facteur getFacteur() {
        return facteur;
    }

    /**
     *
     * @param facteur
     */
    public void setFacteur(Facteur facteur) {
        this.facteur = facteur;
    }

    /**
     *
     * @return
     */
    public String getIntitule_affichage() {
        return intitule_affichage;
    }

    /**
     *
     * @return
     */
    public String getProvenance() {
        return provenance;
    }

    /**
     *
     * @param intitule_affichage
     */
    public void setIntitule_affichage(String intitule_affichage) {
        this.intitule_affichage = intitule_affichage;
    }

    /**
     *
     * @param provenance
     */
    public void setProvenance(String provenance) {
        this.provenance = provenance;
    }

}
