package org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Teneurcalcaireinitial.NAME_ENTITY_JPA ,uniqueConstraints = @UniqueConstraint(columnNames = {
        Teneurcalcaireinitial.ID_JPA, 
        Texturesol.ID_JPA}))
public class Teneurcalcaireinitial implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "teneurcalcaireinitial";

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Teneurcalcaireinitial.ID_JPA, updatable = false)
    private long id = 0;
    @Column(name= Teneurcalcaireinitial.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name=  Teneurcalcaireinitial.JPA_COLUMN_NAME,unique = false,nullable=false)
    private String nom = "";
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Texturesol.ID_JPA,nullable=false)
    private Texturesol texturesol;
    
    /**
     *
     */
    public Teneurcalcaireinitial(){
        super();
    }

    /**
     *
     * @param code
     * @param nom
     * @param texturesol
     */
    public  Teneurcalcaireinitial (String code,String nom,Texturesol texturesol){
        this.code = code;
        this.nom = nom;
        this.texturesol=texturesol;
    }
    
    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }
    
    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public Texturesol getTexturesol() {
        return texturesol;
    }

    /**
     *
     * @param texturesol
     */
    public void setTexturesol(Texturesol texturesol) {
        this.texturesol = texturesol;
    }
    

}
