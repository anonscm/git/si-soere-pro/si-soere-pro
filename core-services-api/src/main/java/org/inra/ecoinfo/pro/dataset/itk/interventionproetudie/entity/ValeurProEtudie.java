/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurProEtudie.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    RealNode.ID_JPA,
    MesureProEtudie.ATTRIBUTE_JPA_ID,}))
@PrimaryKeyJoinColumn(name = ValeurITK.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurProEtudie.ATTRIBUTE_JPA_ID)
@AttributeOverrides({
    @AttributeOverride(name = ValeurITK.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurProEtudie.ATTRIBUTE_JPA_ID))})

public class ValeurProEtudie extends ValeurITK<MesureProEtudie, ValeurProEtudie> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurproetudie";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "vpe_id";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureProEtudie.class)
    @JoinColumn(name = MesureProEtudie.ATTRIBUTE_JPA_ID, referencedColumnName = MesureProEtudie.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureProEtudie mesureinterventionproetudie;

    /**
     *
     */
    public ValeurProEtudie() {
        super();
    }

    /**
     *
     * @param mesureinterventionproetudie
     * @param realNode
     * @param valeur
     */
    public ValeurProEtudie(MesureProEtudie mesureinterventionproetudie, RealNode realNode, Float valeur) {
        super(valeur, realNode);
        this.mesureinterventionproetudie = mesureinterventionproetudie;
    }

    /**
     *
     * @param realNode
     * @param mesureinterventionproetudie
     * @param listeItineraire
     */
    public ValeurProEtudie(RealNode realNode, MesureProEtudie mesureinterventionproetudie, ListeItineraire listeItineraire) {
        super(listeItineraire, realNode);
        this.mesureinterventionproetudie = mesureinterventionproetudie;
    }

    /**
     *
     * @return
     */
    public MesureProEtudie getMesureinterventionproetudie() {
        return mesureinterventionproetudie;
    }

    /**
     *
     * @param mesureinterventionproetudie
     */
    public void setMesureinterventionproetudie(MesureProEtudie mesureinterventionproetudie) {
        this.mesureinterventionproetudie = mesureinterventionproetudie;
    }

}
