/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.environnementdispositif;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.environnement.Environnement;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=EnvironnementDispositif.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                                Dispositif.ID_JPA,
    Environnement.ID_JPA
}))
public class EnvironnementDispositif implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "environnementdispositif";

    /**
     *
     */
    public static final String ID_JPA = "ed_id";

    /**
     *
     */
    public static final String COLUMN_COMMENT_JPA = "commentaire";
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name=EnvironnementDispositif.ID_JPA,updatable=false)
     private long ed_id =0;
     
      @Column(name=EnvironnementDispositif.COLUMN_COMMENT_JPA,nullable = false,unique=false)
     private String commentaire = "";
     
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
     @JoinColumn(name=Dispositif.ID_JPA,nullable = false,unique=false)
     private Dispositif dispositif;
      
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
     @JoinColumn(name=Environnement.ID_JPA,nullable = false,unique=false)
     private Environnement environnement;
     
    /**
     *
     */
    public EnvironnementDispositif(){
         super();
     }
     
    /**
     *
     * @param commentaire
     * @param dispositif
     * @param environnement
     */
    public EnvironnementDispositif(String commentaire,Dispositif dispositif,Environnement environnement){
         this.commentaire=commentaire;
         this.dispositif = dispositif;
         this.environnement = environnement;
     }

    /**
     *
     * @return
     */
    public long getEd_id() {
        return ed_id;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Environnement getEnvironnement() {
        return environnement;
    }

    /**
     *
     * @param ed_id
     */
    public void setEd_id(long ed_id) {
        this.ed_id = ed_id;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param environnement
     */
    public void setEnvironnement(Environnement environnement) {
        this.environnement = environnement;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
     
     
    
}
