/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.historiquedispositif;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.faitremarquable.Faitremarquable;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=HistoriqueDispositif.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
    Dispositif.ID_JPA,
    Faitremarquable.ID_JPA
   
}))
public class HistoriqueDispositif implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "historiquedispositif";

    /**
     *
     */
    public static final String ID_JPA = "hid_id";

    /**
     *
     */
    public static final String COLUMN_DATE_JPA = "hid_date";

    /**
     *
     */
    public static final String COLUMN_COMMENT_JPA = "commentaire";
    
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name=HistoriqueDispositif.ID_JPA,updatable=false)
     private long hid_id =0;
     
     @Column(name=HistoriqueDispositif.COLUMN_DATE_JPA,nullable = false,unique=false,length = 4)
     private int hid_date = 0;
      @Column(name=HistoriqueDispositif.COLUMN_COMMENT_JPA,nullable = false,unique=false)
     private String commentaire = "";
     
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
     @JoinColumn(name=Dispositif.ID_JPA,nullable = false,unique=false)
     private Dispositif dispositif;
      
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
     @JoinColumn(name=Faitremarquable.ID_JPA,nullable = false,unique=false)
     private Faitremarquable faitremarquable;
       
    /**
     *
     */
    public HistoriqueDispositif()  {
         super();
     }
     
    /**
     *
     * @param date
     * @param commentaire
     * @param dispositif
     * @param faitremarquable
     */
    public HistoriqueDispositif(int date,String commentaire,Dispositif dispositif,Faitremarquable faitremarquable){
         this.hid_date = date;
         this.commentaire = commentaire;
         this.dispositif = dispositif;
         this.faitremarquable = faitremarquable;
     }

    /**
     *
     * @return
     */
    public long getHid_id() {
        return hid_id;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Faitremarquable getFaitremarquable() {
        return faitremarquable;
    }

    /**
     *
     * @param hid_id
     */
    public void setHid_id(long hid_id) {
        this.hid_id = hid_id;
    }

    /**
     *
     * @return
     */
    public int getHid_date() {
        return hid_date;
    }

    /**
     *
     * @param hid_date
     */
    public void setHid_date(int hid_date) {
        this.hid_date = hid_date;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param faitremarquable
     */
    public void setFaitremarquable(Faitremarquable faitremarquable) {
        this.faitremarquable = faitremarquable;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
     
     
     
}
