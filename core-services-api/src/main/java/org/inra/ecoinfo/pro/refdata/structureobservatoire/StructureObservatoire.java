package org.inra.ecoinfo.pro.refdata.structureobservatoire;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.STRUCTURE_ID, RefDataConstantes.OBSERVATOIRE_ID
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StructureObservatoire.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.STRUCTURE_ID,
    RefDataConstantes.OBSERVATOIRE_ID}))
public class StructureObservatoire implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STRUCTUREOBSERV_ID; // strobs_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STRUCTUREOBSERV_TABLE_NAME; // structure_observatoire
    @Id
    @Column(name = StructureObservatoire.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Structure.class)
    @JoinColumn(name = RefDataConstantes.STRUCTURE_ID, referencedColumnName = Structure.ID_JPA, nullable = false)
    private Structure structure;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Observatoire.class)
    @JoinColumn(name = RefDataConstantes.OBSERVATOIRE_ID, referencedColumnName = Observatoire.ID_JPA, nullable = false)
    private Observatoire observatoire;

    /**
     *
     */
    public StructureObservatoire() {
        super();
    }

    /**
     *
     * @param structure
     * @param observatoire
     */
    public StructureObservatoire(Structure structure, Observatoire observatoire) {
        super();
        this.structure = structure;
        this.observatoire = observatoire;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Observatoire getObservatoire() {
        return observatoire;
    }

    /**
     *
     * @return
     */
    public Structure getStructure() {
        return structure;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param observatoire
     */
    public void setObservatoire(Observatoire observatoire) {
        this.observatoire = observatoire;
    }

    /**
     *
     * @param structure
     */
    public void setStructure(Structure structure) {
        this.structure = structure;
    }
}
