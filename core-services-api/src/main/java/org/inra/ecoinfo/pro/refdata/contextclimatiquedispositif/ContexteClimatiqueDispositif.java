/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contextclimatiquedispositif;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.horizontravailledispositif.HorizonTravailDispositif;
import org.inra.ecoinfo.pro.refdata.typeclimat.Typeclimat;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=ContexteClimatiqueDispositif.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                                    
                                                                                     ContexteClimatiqueDispositif.COLUMN_PRECIPITATION_JPA,
                                                                                     ContexteClimatiqueDispositif.COLUMN_NOMSM_JPA,
                                                                                     Typeclimat.ID_JPA,
                                                                                     Dispositif.ID_JPA
                                                                  
       
}))
public class ContexteClimatiqueDispositif implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "contexteclimatiquedispositif";

    /**
     *
     */
    public static final String ID_JPA = "ccd_id";

    /**
     *
     */
    public static final String COLUMN_PRECIPITATION_JPA = "ccd_precipitation";

    /**
     *
     */
    public static final String COLUMN_TEMPERATURE_JPA = "ccd_temperature";

    /**
     *
     */
    public static final String COLUMN_ETP_JPA = "ccd_etp";

    /**
     *
     */
    public static final String COLUMN_STATION_METEO_JPA = "ccd_stationmeteo";

    /**
     *
     */
    public static final String COLUMN_NOMSM_JPA = "ccd_nomstationmeteo";

    /**
     *
     */
    public static final String COLUMN_ANNEES_CONCIDEREES_JPA = "annee";

    /**
     *
     */
    public static final String COLUMN_COMMENT="comment";
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name=HorizonTravailDispositif.ID_JPA,updatable=false)
     private long ccd_id =0;
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_PRECIPITATION_JPA,nullable = false,unique = false)
     private double ccd_precipitation=0.0;
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_TEMPERATURE_JPA,nullable = false,unique = false)
     private double ccd_temperature = 0.0;
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_ETP_JPA,nullable = true,unique = false)
     private double ccd_etp =0.0;
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_STATION_METEO_JPA,nullable = false,unique = false)
     private String ccd_stationmeteo ;
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_NOMSM_JPA, nullable = false,unique = false)
     private String ccd_nomstationmeteo ="";
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_ANNEES_CONCIDEREES_JPA,nullable = false,unique = false)
     private long annee =0;
     
     @Column(name=ContexteClimatiqueDispositif.COLUMN_COMMENT)
     private String comment="";
     
     @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;
     @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Typeclimat.ID_JPA, nullable = false)
    private Typeclimat typeclimat;
     
    /**
     *
     */
    public ContexteClimatiqueDispositif(){
         super();
     }
     
    /**
     *
     * @param dispositif
     * @param typeclimat
     * @param precipitation
     * @param temperature
     * @param etp
     * @param stationmeteo
     * @param nomsm
     * @param annee
     * @param comment
     */
    public ContexteClimatiqueDispositif(Dispositif dispositif , Typeclimat typeclimat,double precipitation,double temperature,double etp,String stationmeteo,String nomsm,
            long annee, String comment)
     {
         this.ccd_precipitation = precipitation;
         this.ccd_temperature = temperature;
         this.ccd_etp = etp;
         this.ccd_stationmeteo = stationmeteo;
         this.ccd_nomstationmeteo =nomsm;
         this.annee = annee;
         this.dispositif = dispositif;
         this.typeclimat = typeclimat;
         this.comment = comment;
         
     }

    /**
     *
     * @return
     */
    public long getCcd_id() {
        return ccd_id;
    }

    /**
     *
     * @return
     */
    public double getCcd_precipitation() {
        return ccd_precipitation;
    }

    /**
     *
     * @return
     */
    public double getCcd_temperature() {
        return ccd_temperature;
    }

    /**
     *
     * @return
     */
    public double getCcd_etp() {
        return ccd_etp;
    }

    /**
     *
     * @return
     */
    public String getCcd_nomstationmeteo() {
        return ccd_nomstationmeteo;
    }

    /**
     *
     * @param ccd_id
     */
    public void setCcd_id(long ccd_id) {
        this.ccd_id = ccd_id;
    }

    /**
     *
     * @param ccd_precipitation
     */
    public void setCcd_precipitation(double ccd_precipitation) {
        this.ccd_precipitation = ccd_precipitation;
    }

    /**
     *
     * @param ccd_temperature
     */
    public void setCcd_temperature(double ccd_temperature) {
        this.ccd_temperature = ccd_temperature;
    }

    /**
     *
     * @param ccd_etp
     */
    public void setCcd_etp(double ccd_etp) {
        this.ccd_etp = ccd_etp;
    }

    /**
     *
     * @return
     */
    public String getCcd_stationmeteo() {
        return ccd_stationmeteo;
    }

    /**
     *
     * @param ccd_stationmeteo
     */
    public void setCcd_stationmeteo(String ccd_stationmeteo) {
        this.ccd_stationmeteo = ccd_stationmeteo;
    }

    /**
     *
     * @param ccd_nomstationmeteo
     */
    public void setCcd_nomstationmeteo(String ccd_nomstationmeteo) {
        this.ccd_nomstationmeteo = ccd_nomstationmeteo;
    }

    /**
     *
     * @return
     */
    public long getAnnee() {
        return annee;
    }

    /**
     *
     * @param annee
     */
    public void setAnnee(long annee) {
        this.annee = annee;
    }

    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Typeclimat getTypeclimat() {
        return typeclimat;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param typeclimat
     */
    public void setTypeclimat(Typeclimat typeclimat) {
        this.typeclimat = typeclimat;
    }
    
    
}
