package org.inra.ecoinfo.pro.refdata.unites;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Unites.NAME_ENTITY_JPA)
public class Unites implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "unites";

    /**
     *
     */
    public static final String JPA_ID = "unite_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "unite";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";
    @Id
    @Column(name = Unites.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long unite_id;
    @Column(name = Unites.JPA_COLUMN_KEY, nullable = false)
    private String mycode = "";
    @Column(name = Unites.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String unite = "";
     @Column(name = Unites.JPA_COLUMN_CODE, nullable = false, unique = true)
    private String code ="";

    /**
     *
     */
    public Unites() {
        super();
    }

    /**
     *
     * @param unite
     * @param code
     */
    public Unites( String unite,String code) {
        this.unite = unite;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     *
     * @return
     */
    public long getUnite_id() {
        return unite_id;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

    /**
     *
     * @param unite_id
     */
    public void setUnite_id(long unite_id) {
        this.unite_id = unite_id;
    }

    /**
     *
     * @return
     */
    public String getMycode() {
        return mycode;
    }

    /**
     *
     * @param mycode
     */
    public void setMycode(String mycode) {
        this.mycode = mycode;
    }
    
    
}
