package org.inra.ecoinfo.pro.refdata.mpremieremprocess;
import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.matierepremiere.MatieresPremieres;
import org.inra.ecoinfo.pro.refdata.methodeprocess.MethodeProcess;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MPremiereMProcess.NAME_ENTITY_JAP)
public class MPremiereMProcess implements Serializable {
    private static final long serialVersionUID = 1L;
    static final String NAME_ENTITY_JAP = "mpremiere_mprocess";
    static final String JPA_ID = "mpmp_id";
    static final String JPA_COLUMN_NAME_ORDER = "mpmp_ordre";
    static final String JPA_COLUMN_NAME_DAY = "mpmp_date";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = MPremiereMProcess.JPA_ID, nullable = false, updatable = false, unique = true)
    private long mpmp_id = 0;
    @Column(name = MPremiereMProcess.JPA_COLUMN_NAME_ORDER, nullable = false)
    private int mpmp_ordre = 0;
    @Column(name = MPremiereMProcess.JPA_COLUMN_NAME_DAY, nullable = false)
    private LocalDate mpmp_date = null;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MatieresPremieres.JPA_ID, nullable = false)
    private MatieresPremieres matierepremieres;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodeProcess.ID_JPA, nullable = false)
    private MethodeProcess methodeprocess;

    /**
     *
     */
    public MPremiereMProcess() {
    }

    /**
     *
     * @param id
     * @param ordre
     * @param date
     * @param matierepremieres
     * @param methodeprocess
     */
    public MPremiereMProcess(long id, int ordre, LocalDate date, MatieresPremieres matierepremieres, MethodeProcess methodeprocess) {
        this.mpmp_id = id;
        this.mpmp_ordre = ordre;
        this.mpmp_date = date;
        this.matierepremieres = matierepremieres;
        this.methodeprocess = methodeprocess;
    }

    /**
     *
     * @return
     */
    public MatieresPremieres getMatieresPremieres() {
        return matierepremieres;
    }

    /**
     *
     * @return
     */
    public MethodeProcess getMethodeprocess() {
        return methodeprocess;
    }

    /**
     *
     * @return
     */
    public LocalDate getMpmp_date() {
        return mpmp_date;
    }

    /**
     *
     * @return
     */
    public long getMpmp_id() {
        return mpmp_id;
    }

    /**
     *
     * @return
     */
    public int getMpmp_ordre() {
        return mpmp_ordre;
    }

    /**
     *
     * @param matierepremieres
     */
    public void setMatieresPremieres(MatieresPremieres matierepremieres) {
        this.matierepremieres = matierepremieres;
    }

    /**
     *
     * @param methodeprocess
     */
    public void setMethodeprocess(MethodeProcess methodeprocess) {
        this.methodeprocess = methodeprocess;
    }

    /**
     *
     * @param mpmp_date
     */
    public void setMpmp_date(LocalDate mpmp_date) {
        this.mpmp_date = mpmp_date;
    }

    /**
     *
     * @param mpmp_id
     */
    public void setMpmp_id(long mpmp_id) {
        this.mpmp_id = mpmp_id;
    }

    /**
     *
     * @param mpmp_ordre
     */
    public void setMpmp_ordre(int mpmp_ordre) {
        this.mpmp_ordre = mpmp_ordre;
    }
}
