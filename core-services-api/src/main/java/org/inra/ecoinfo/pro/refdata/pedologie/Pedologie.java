package org.inra.ecoinfo.pro.refdata.pedologie;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.nomregionalsol.Nomregionalsol;
import org.inra.ecoinfo.pro.refdata.pente.Pente;
import org.inra.ecoinfo.pro.refdata.substratpedologique.Substratpedologique;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Pedologie.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
               Dispositif.ID_JPA,
               Substratpedologique.ID_JPA
             
       
}) )
public class Pedologie implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "pedologie";

    /**
     *
     */
    public static final String ID_JPA = "pedo_id";

    /**
     *
     */
    public static final String JPA_COLUMN_RESERVE = "reserve_Utile_Initiale";

    /**
     *
     */
    public static final String JPA_COLUMN_PENTE = "orientation_Pente";

    /**
     *
     */
    public static final String JPA_COLUMN_PROFONDEUR = "profondeur_Sol";
    public static final String JPA_COLUMN_NOM_REGIONAL_SOL = "nom_regional_Sol";

    /**
     *
     */
    public static final String JPA_COMMENT = "comment";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Pedologie.ID_JPA)
    private long pedo_id = 0;
    @Column(name= Pedologie.JPA_COLUMN_RESERVE,nullable=false,unique=false)
    private double reserve_Utile_Initiale = 0.0;
    @Column(name=  Pedologie.JPA_COLUMN_PENTE,unique = false,nullable=true)
    private String orientation_Pente = "";
    @Column(name=Pedologie.JPA_COLUMN_PROFONDEUR)
    private double profondeur_Sol =0.0;
    
     @Column(name=  Pedologie.JPA_COLUMN_NOM_REGIONAL_SOL,unique = false,nullable=true)
    private String nom_regional_Sol = "";
     
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Pente.ID_JPA, nullable = false)
    private Pente pente;
//    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
//    @JoinColumn(name = Nomregionalsol.ID_JPA, nullable = false)
//    private Nomregionalsol nomregionalsol;
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;
     @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Substratpedologique.ID_JPA, nullable = false)
     private Substratpedologique substratpedologique;
    @Column(name=Pedologie.JPA_COMMENT)
    private String comment="";
    
    /**
     *
     */
    public Pedologie(){
        super();
    }

    /**
     *
     * @param reserve
     * @param orientation
     * @param profondeur
     * @param comment
     * @param pente
     * @param dispositif
     * @param regionalsol
     * @param subspedo
     */
    public Pedologie(double reserve,String orientation,double profondeur,String comment,Pente pente,
            Dispositif dispositif,String nom_regional_Sol,Substratpedologique subspedo){
        this.reserve_Utile_Initiale = reserve;
        this.orientation_Pente = orientation;
        this.profondeur_Sol = profondeur;
        this.comment =comment;
        this.pente = pente;
        this.dispositif = dispositif;
        this.nom_regional_Sol =nom_regional_Sol;
        this.substratpedologique = subspedo;
    }

    
    /**
     *
     * @return
     */
    public long getPedo_id() {
        return pedo_id;
    }
    
    /**
     *
     * @param pedo_id
     */
    public void setPedo_id(long pedo_id) {
        this.pedo_id = pedo_id;
    }

    /**
     *
     * @return
     */
    public double getReserve_Utile_Initiale() {
        return reserve_Utile_Initiale;
    }

    /**
     *
     * @return
     */
    public String getOrientation_Pente() {
        return orientation_Pente;
    }

    /**
     *
     * @param reserve_Utile_Initiale
     */
    public void setReserve_Utile_Initiale(double reserve_Utile_Initiale) {
        this.reserve_Utile_Initiale = reserve_Utile_Initiale;
    }

    /**
     *
     * @param orientation_Pente
     */
    public void setOrientation_Pente(String orientation_Pente) {
        this.orientation_Pente = orientation_Pente;
    }
   
    /**
     *
     * @return
     */
    public double getProfondeur_Sol() {
        return profondeur_Sol;
    }
    
    /**
     *
     * @param profondeur_Sol
     */
    public void setProfondeur_Sol(double profondeur_Sol) {
        this.profondeur_Sol = profondeur_Sol;
    }
    
    /**
     *
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     */
    public Pente getPente() {
        return pente;
    }

    /**
     *
     * @param pente
     */
    public void setPente(Pente pente) {
        this.pente = pente;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

  

    /**
     *
     * @return
     */
    public Substratpedologique getSubstratpedologique() {
        return substratpedologique;
    }

    /**
     *
     * @param substratpedologique
     */
    public void setSubstratpedologique(Substratpedologique substratpedologique) {
        this.substratpedologique = substratpedologique;
    }

    public String getNom_regional_Sol() {
        return nom_regional_Sol;
    }

    public void setNom_regional_Sol(String nom_regional_Sol) {
        this.nom_regional_Sol = nom_regional_Sol;
    }
    
    
}
