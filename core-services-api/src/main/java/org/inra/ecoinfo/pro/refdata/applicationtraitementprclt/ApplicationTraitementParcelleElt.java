package org.inra.ecoinfo.pro.refdata.applicationtraitementprclt;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.modalite.Modalite;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = ApplicationTraitementParcelleElt.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.DESCRIPTTRAITEMENT_ID,
                                                                                                                RefDataConstantes.PARCELLEELT_ID,
                                                                                                                 RefDataConstantes.MODALITE_ID
                                                                                                                 }))
public class ApplicationTraitementParcelleElt implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.APPLICATIONTRAITEMENTPARCELT_ID; // dtrtprc_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.APPLICATIONTRAITEMENTPARCELT_TABLE_NAME; // application_traitement_parcelle_elt

    @Id
    @Column(name = ApplicationTraitementParcelleElt.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = DescriptionTraitement.class)
    @JoinColumn(name = RefDataConstantes.DESCRIPTTRAITEMENT_ID, referencedColumnName = DescriptionTraitement.ID_JPA, nullable = false)
    private DescriptionTraitement descriptionTraitement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = ParcelleElementaire.class)
    @JoinColumn(name = RefDataConstantes.PARCELLEELT_ID, referencedColumnName = ParcelleElementaire.ID_JPA, nullable = false)
    private ParcelleElementaire parcelleElementaire;
    
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Modalite.class)
    @JoinColumn(name = RefDataConstantes.MODALITE_ID, referencedColumnName = Modalite.ID_JPA, nullable = false)
    private Modalite modalite;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ANNEEDEBUT_APPLICATIONTRAIPARCELT, length = 4)
    private String anneeDebut;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEFIN_APPLICATIONTRAITPARCELT, length = 4)
    private String anneeFin;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NUMEROREPETITION_APPLICATIONTRAITPARCELT)
    private Integer numeroRepetition;

    /**
     * empty constructor
     */
    public ApplicationTraitementParcelleElt() {
        super();
    }

    /**
     * @param descriptionTraitement
     * @param parcelleElementaire
     * @param modalite
     * @param anneeDebut
     * @param anneeFin
     * @param numeroRepetition
     */
    public ApplicationTraitementParcelleElt(DescriptionTraitement descriptionTraitement, ParcelleElementaire parcelleElementaire, Modalite modalite,String anneeDebut, String anneeFin, Integer numeroRepetition) {
        super();
        this.descriptionTraitement = descriptionTraitement;
        this.parcelleElementaire = parcelleElementaire;
        this.modalite = modalite;
        this.anneeDebut = anneeDebut;
        this.anneeFin = anneeFin;
        this.numeroRepetition = numeroRepetition;
    }

    /**
     * @return the anneeDebut
     */
    public String getAnneeDebut() {
        return anneeDebut;
    }

    /**
     * @return the anneeFin
     */
    public String getAnneeFin() {
        return anneeFin;
    }

    /**
     * @return the descriptionTraitement
     */
    public DescriptionTraitement getDescriptionTraitement() {
        return descriptionTraitement;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the numeroRepetition
     */
    public Integer getNumeroRepetition() {
        return numeroRepetition;
    }

    /**
     * @return the parcelleElementaire
     */
    public ParcelleElementaire getParcelleElementaire() {
        return parcelleElementaire;
    }

    /**
     * @param anneeDebut
     *            the anneeDebut to set
     */
    public void setAnneeDebut(String anneeDebut) {
        this.anneeDebut = anneeDebut;
    }

    /**
     * @param anneeFin
     *            the anneeFin to set
     */
    public void setAnneeFin(String anneeFin) {
        this.anneeFin = anneeFin;
    }

    /**
     * @param descriptionTraitement
     *            the descriptionTraitement to set
     */
    public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @param numeroRepetition
     *            the numeroRepetition to set
     */
    public void setNumeroRepetition(Integer numeroRepetition) {
        this.numeroRepetition = numeroRepetition;
    }

    /**
     * @param parcelleElementaire
     *            the parcelleElementaire to set
     */
    public void setParcelleElementaire(ParcelleElementaire parcelleElementaire) {
        this.parcelleElementaire = parcelleElementaire;
    }

    /**
     *
     * @return
     */
    public Modalite getModalite() {
        return modalite;
    }

    /**
     *
     * @param modalite
     */
    public void setModalite(Modalite modalite) {
        this.modalite = modalite;
    }
    
    

}
