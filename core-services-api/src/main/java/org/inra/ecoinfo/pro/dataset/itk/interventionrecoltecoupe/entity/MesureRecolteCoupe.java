/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureRecolteCoupe.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    MesureRecolteCoupe.ATTRIBUTE_JPA_LocalDate_DEBUT,
    MesureRecolteCoupe.ATTRIBUTE_JPA_DISPOSITIF,
    DescriptionTraitement.ID_JPA,
    MesureRecolteCoupe.ATTRIBUTE_JPA_PARCELLE,
    MesureRecolteCoupe.ATTRIBUTE_JPA_PLACETTE,
    MesureRecolteCoupe.ATTRIBUTE_JPA_CULTURE,
    MesureRecolteCoupe.ATTRIBUTE_INTERVENTION}))

public class MesureRecolteCoupe extends MesureITK<MesureApport, ValeurApport> {

    /**
     *
     */
    public static final String ATTRIBUTE_CODEBBCH = "codebbch";

    /**
     *
     */
    public static final String ATTRIBUTE_PRECISION_STADE = "stadeprecision";

    /**
     *
     */
    public static final String ATTRIBUTE_TYPEINTERVENTION = "typeintervention";

    /**
     *
     */
    public static final String ATTRIBUTE_INTERVENTION = "nomintervention";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL_1 = "materielplante1";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL_2 = "materielplante2";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL_3 = "materielplante3";
    /**
     *
     */
    public static final String ATTRIBUTE_NIVEAU = "niveauatteint";

    /**
     *
     */
    public static final String TABLE_NAME = "mesureineterventionrecoltecoupe";

    static final long serialVersionUID = 1L;
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_CODEBBCH, nullable = false)
    private String codebbch = "";
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_MATERIEL_1, nullable = false)
    private String materielplante1 = "";
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_MATERIEL_2)
    private String materielplante2 = "";
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_MATERIEL_3)
    private String materielplante3 = "";
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_PRECISION_STADE)
    private String stadeprecision = "";
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_INTERVENTION, nullable = false)
    private String nomintervention = "";
    @Column(name = MesureRecolteCoupe.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint = "";
    @OneToMany(mappedBy = "mesureinterventionrecolte", cascade = ALL)
    List<ValeurRecolteCoupe> valeurrecolte = new LinkedList();

    /**
     *
     */
    public MesureRecolteCoupe() {
        super();
    }

    /**
     *
     * @param LocalDatedebut
     * @param LocalDatefin
     * @param codedispositif
     * @param traitement
     * @param nomparcelle
     * @param nomplacette
     * @param nomculture
     * @param codebbch
     * @param mp1
     * @param mp2
     * @param mp3
     * @param stade
     * @param intervention
     * @param typeobservation
     * @param nomobservation
     * @param niveau
     * @param commentaire
     * @param versionfile
     */
    public MesureRecolteCoupe(LocalDate LocalDatedebut, LocalDate LocalDatefin, String codedispositif, DescriptionTraitement traitement, String nomparcelle, String nomplacette,
            String nomculture, String codebbch, String mp1, String mp2, String mp3, String stade, String intervention,
            String typeobservation, String nomobservation, String niveau, String commentaire, VersionFile versionfile) {
        super(nomculture, codedispositif, commentaire, nomparcelle, nomplacette, LocalDatedebut, LocalDatefin, typeobservation, nomobservation, versionfile, traitement);
        this.codebbch = codebbch;
        this.materielplante1 = mp1;
        this.materielplante2 = mp2;
        this.materielplante3 = mp3;
        this.stadeprecision = stade;
        this.nomintervention = intervention;
        this.niveauatteint = niveau;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getMaterielplante1() {
        return materielplante1;
    }

    /**
     *
     * @param materielplante1
     */
    public void setMaterielplante1(String materielplante1) {
        this.materielplante1 = materielplante1;
    }

    /**
     *
     * @return
     */
    public String getMaterielplante2() {
        return materielplante2;
    }

    /**
     *
     * @param materielplante2
     */
    public void setMaterielplante2(String materielplante2) {
        this.materielplante2 = materielplante2;
    }

    /**
     *
     * @return
     */
    public String getMaterielplante3() {
        return materielplante3;
    }

    /**
     *
     * @param materielplante3
     */
    public void setMaterielplante3(String materielplante3) {
        this.materielplante3 = materielplante3;
    }

    /**
     *
     * @return
     */
    public String getStadeprecision() {
        return stadeprecision;
    }

    /**
     *
     * @param stadeprecision
     */
    public void setStadeprecision(String stadeprecision) {
        this.stadeprecision = stadeprecision;
    }

    /**
     *
     * @return
     */
    public String getNomintervention() {
        return nomintervention;
    }

    /**
     *
     * @param nomintervention
     */
    public void setNomintervention(String nomintervention) {
        this.nomintervention = nomintervention;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteint() {
        return niveauatteint;
    }

    /**
     *
     * @param niveauatteint
     */
    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    /**
     *
     * @return
     */
    public List<ValeurRecolteCoupe> getValeurrecolte() {
        return valeurrecolte;
    }

    /**
     *
     * @param valeurrecolte
     */
    public void setValeurrecolte(List<ValeurRecolteCoupe> valeurrecolte) {
        this.valeurrecolte = valeurrecolte;
    }
}
