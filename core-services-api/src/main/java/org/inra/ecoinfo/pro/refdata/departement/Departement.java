package org.inra.ecoinfo.pro.refdata.departement;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.commune.Commune;
import org.inra.ecoinfo.pro.refdata.region.Region;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Departement.TABLE_NAME)
public class Departement implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.DEPARTEMENT_ID; // dpt_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.DEPARTEMENT_TABLE_NAME; // departement
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = Departement.ID_JPA)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_DEPARTEMENT, length = 30)
    private String nomDepartement;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_DEPARTEMENT, length = 30)
    private String code;
    
    @Column(nullable = true, unique = true, name = RefDataConstantes.COLUMN_NUMERO_DEPARTEMENT, length = 3)
    private String noDepartement;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Region.class)
    @JoinColumn(name = Region.ID_JPA, referencedColumnName = Region.ID_JPA, nullable = false)
    private Region region;
    
    @OneToMany(mappedBy = RefDataConstantes.NAME_ATTRIBUT_DEPARTEMENT, cascade = {PERSIST, MERGE, REFRESH, REMOVE})
    private List<Commune> lstCommune = new LinkedList<Commune>();
    
    /**
     *
     */
    public Departement() 
    {
        super();
    }
    
    /**
     *
     * @param nomDepartement
     * @param noDepartement
     */
    public Departement(String nomDepartement, String noDepartement) 
    {
        super();
        this.nomDepartement = nomDepartement;
        this.noDepartement = noDepartement;
    }
    
    /**
	 * @param nomDepartement
	 * @param noDepartement
	 * @param region
	 */
	public Departement(String nomDepartement, String noDepartement,
			Region region) {
		super();
		this.nomDepartement = nomDepartement;
		this.noDepartement = noDepartement;
		this.region = region;
	}
	
	/**
	 * @param nomDepartement
	 * @param code
	 * @param noDepartement
	 * @param region
	 */
	public Departement(String nomDepartement, String code,
			String noDepartement, Region region) {
		super();
		this.nomDepartement = nomDepartement;
		this.code = code;
		this.noDepartement = noDepartement;
		this.region = region;
	}

    /**
     *
     * @param commune
     */
    public void addCommune(Commune commune) 
    {
        this.lstCommune.add(commune);
        commune.setDepartement(this);
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public List<Commune> getLstCommune() {
        return lstCommune;
    }
    
    /**
     *
     * @return
     */
    public String getNoDepartement() {
        return noDepartement;
    }
    
    /**
     *
     * @return
     */
    public String getNomDepartement() {
        return nomDepartement;
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @return
     */
    public Region getRegion() {
        return region;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param lstCommune
     */
    public void setLstCommune(List<Commune> lstCommune) {
        this.lstCommune = lstCommune;
    }
    
    /**
     *
     * @param noDepartement
     */
    public void setNoDepartement(String noDepartement) {
        this.noDepartement = noDepartement;
    }
    
    /**
     *
     * @param nomDepartement
     */
    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }
    
    /**
     *
     * @param region
     */
    public void setRegion(Region region) {
        this.region = region;
    }
}
