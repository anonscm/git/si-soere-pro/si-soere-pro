package org.inra.ecoinfo.pro.refdata.FormePhysique;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = FormePhysiques.NAME_ENTITY_JPA)
public class FormePhysiques implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "formephysique";

    /**
     *
     */
    public static final String JPA_ID = "fp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "fp_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "fp_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "fp_comment";
    @Id
    @Column(name = FormePhysiques.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long fp_id;
    @Column(name = FormePhysiques.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String fp_code = "";
    @Column(name = FormePhysiques.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String fp_nom = "";
    @Column(name=FormePhysiques.JPA_COLUMN_COMMENT)
    private String fp_comment = "";

    /**
     *
     */
    public FormePhysiques() {
        super();
    }

    /**
     *
     * @param nom
     * @param comment
     */
    public FormePhysiques(String nom,String comment) {
        this.fp_nom = nom;
        this.fp_comment = comment;
    }

    /**
     *
     * @return
     */
    public String getFp_code() {
        return fp_code;
    }

    /**
     *
     * @return
     */
    public long getFp_id() {
        return fp_id;
    }

    /**
     *
     * @return
     */
    public String getFp_nom() {
        return fp_nom;
    }

    /**
     *
     * @param fp_code
     */
    public void setFp_code(String fp_code) {
        this.fp_code = fp_code;
    }

    /**
     *
     * @param fp_id
     */
    public void setFp_id(long fp_id) {
        this.fp_id = fp_id;
    }

    /**
     *
     * @param fp_nom
     */
    public void setFp_nom(String fp_nom) {
        this.fp_nom = fp_nom;
    }

    /**
     *
     * @return
     */
    public String getFp_comment() {
        return fp_comment;
    }

    /**
     *
     * @param fp_comment
     */
    public void setFp_comment(String fp_comment) {
        this.fp_comment = fp_comment;
    }
    
    
}
