/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpro;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Precisionpro.NAME_ENTITY_JPA)
public class Precisionpro implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "precisionpro";

    /**
     *
     */
    public static final String JPA_ID = "p_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "p_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "p_nom";
    
    @Id
    @Column(name = Precisionpro.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long p_id;
    @Column(name = Precisionpro.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String p_code = "";
    @Column(name = Precisionpro.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String p_nom = "";
    
    /**
     *
     */
    public Precisionpro(){
        super();
    }
    
    /**
     *
     * @param nom
     */
    public Precisionpro(String nom){
        this.p_nom = nom;
    }

    /**
     *
     * @return
     */
    public long getP_id() {
        return p_id;
    }

    /**
     *
     * @return
     */
    public String getP_code() {
        return p_code;
    }

    /**
     *
     * @return
     */
    public String getP_nom() {
        return p_nom;
    }

    /**
     *
     * @param p_id
     */
    public void setP_id(long p_id) {
        this.p_id = p_id;
    }

    /**
     *
     * @param p_code
     */
    public void setP_code(String p_code) {
        this.p_code = p_code;
    }

    /**
     *
     * @param p_nom
     */
    public void setP_nom(String p_nom) {
        this.p_nom = p_nom;
    }
    
    
}
