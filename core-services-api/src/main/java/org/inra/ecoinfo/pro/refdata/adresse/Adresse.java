/**
 *
 */
package org.inra.ecoinfo.pro.refdata.adresse;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.commune.Commune;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Adresse.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NONOMRUE_ADR, RefDataConstantes.COMMUNE_ID}))
public class Adresse implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.ADRESSE_ID; // adr_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.ADRESSE_TABLE_NAME; // adresse
    
    @Id
    @Column(name = Adresse.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NONOMRUE_ADR, length = 100)
    private String noNomRue;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Commune.class)
    @JoinColumn(name = RefDataConstantes.COMMUNE_ID, referencedColumnName = Commune.ID_JPA, nullable = false)
    private Commune commune;
    
    /**
     *
     */
    public Adresse() 
    {
        super();
    }
    
    /**
     *
     * @param noNomRue
     * @param commune
     */
    public Adresse(String noNomRue, Commune commune) 
    {
        super();
        this.noNomRue = noNomRue;
        this.commune = commune;
    }
    
    /**
     *
     * @return
     */
    public Commune getCommune() {
        return commune;
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getNoNomRue() {
        return noNomRue;
    }
    
    /**
     *
     * @param commune
     */
    public void setCommune(Commune commune) {
        this.commune = commune;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param noNomRue
     */
    public void setNoNomRue(String noNomRue) {
        this.noNomRue = noNomRue;
    }

}
