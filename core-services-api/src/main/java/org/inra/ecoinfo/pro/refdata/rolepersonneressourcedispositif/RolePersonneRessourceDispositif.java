package org.inra.ecoinfo.pro.refdata.rolepersonneressourcedispositif;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.personneressource.PersonneRessource;
import org.inra.ecoinfo.pro.refdata.rolepersonneressource.RolePersonneRessource;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 *            RefDataConstantes.PERSONNERESSOURCE_ID, 
 *            RefDataConstantes.DISPOSITIF_ID, 
 *            RefDataConstantes.ROLEPERSONNERESSOURCE_ID
 *
 * @author ptcherniati
 */
@Entity
@Table(name = RolePersonneRessourceDispositif.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {
            RefDataConstantes.PERSONNERESSOURCE_ID, 
            RefDataConstantes.DISPOSITIF_ID, 
            RefDataConstantes.ROLEPERSONNERESSOURCE_ID
        }))
public class RolePersonneRessourceDispositif implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.ROLEPERSONNERSCDISPOSITIF_ID; 

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.ROLEPERSONNERSCDISPOSITIF_TABLE_NAME; 
    @Id
    @Column(name = RolePersonneRessourceDispositif.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = PersonneRessource.class)
    @JoinColumn(name = RefDataConstantes.PERSONNERESSOURCE_ID, referencedColumnName = PersonneRessource.ID_JPA, nullable = false)
    private PersonneRessource personneRessource;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Dispositif.class)
    @JoinColumn(name =  Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = RolePersonneRessource.class)
    @JoinColumn(name = RefDataConstantes.ROLEPERSONNERESSOURCE_ID, referencedColumnName = RolePersonneRessource.ID_JPA, nullable = false)
    private RolePersonneRessource role;

    /**
     *
     */
    public RolePersonneRessourceDispositif() {
        super();
    }

    /**
     *
     * @param personneRessource
     * @param dispositif
     * @param role
     */
    public RolePersonneRessourceDispositif(PersonneRessource personneRessource, Dispositif dispositif, RolePersonneRessource role) {
        super();
        this.personneRessource = personneRessource;
        this.dispositif = dispositif;
        this.role = role;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public PersonneRessource getPersonneRessource() {
        return personneRessource;
    }

    /**
     *
     * @return
     */
    public RolePersonneRessource getRole() {
        return role;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param personneRessource
     */
    public void setPersonneRessource(PersonneRessource personneRessource) {
        this.personneRessource = personneRessource;
    }

    /**
     *
     * @param role
     */
    public void setRole(RolePersonneRessource role) {
        this.role = role;
    }
}
