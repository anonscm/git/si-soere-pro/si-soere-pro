package org.inra.ecoinfo.pro.refdata.dispositif;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.systemeconduite.SystemeConduiteEssai;
import org.inra.ecoinfo.pro.refdata.typedispositif.TypeDispositif;
import org.inra.ecoinfo.utils.Utils;

/**
 *@UniqueConstraint(columnNames = {
 *   RefDataConstantes.COLUMN_CODE_DISP, RefDataConstantes.LIEU_ID})
 * @author ptcherniati
 */
@Entity
@Table(name = Dispositif.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_CODE_DISP, RefDataConstantes.LIEU_ID}))
@PrimaryKeyJoinColumn(name = Dispositif.ID_JPA)
public class Dispositif extends Nodeable  implements Comparable<INodeable>, Serializable  {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.DISPOSITIF_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.DISPOSITIF_TABLE_NAME;

    /**
     *
     */
    public static final Pattern REGEXP_PATTERN_NOM_DISPOSITIF_NOM_LIEU = Pattern.compile("\\s*(.*?)\\s*\\(\\s*(.*?)\\s*\\)\\s*");

    /**
     *
     */
    public static final String PATTERN_NOM_DISPOSITIF_NOM_LIEU = "%s (%s)";

    /**
     *
     * @param codeDispositif_nomLieu
     * @return
     */
    public static String getNomDispositif(String codeDispositif_nomLieu){
        Matcher matcher = REGEXP_PATTERN_NOM_DISPOSITIF_NOM_LIEU.matcher(codeDispositif_nomLieu);
        if(matcher.matches()){
            return matcher.group(1).trim();
        }
        return codeDispositif_nomLieu.trim();
    }
    
    /**
     *
     * @param codeDispositif_nomLieu
     * @return
     */
    public static String getNomLieu(String codeDispositif_nomLieu){
        Matcher matcher = REGEXP_PATTERN_NOM_DISPOSITIF_NOM_LIEU.matcher(codeDispositif_nomLieu);
        if(matcher.matches()){
            return matcher.group(2).trim();
        }
        return null;
    }

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = TypeDispositif.class)
    @JoinColumn(name = RefDataConstantes.TYPEDISPOSITIF_ID, referencedColumnName = TypeDispositif.ID_JPA, nullable = false)
    private TypeDispositif typeDispositif;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Lieu.class)
    @JoinColumn(name = RefDataConstantes.LIEU_ID, referencedColumnName = Lieu.ID_JPA, nullable = false)
    private Lieu lieu;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = SystemeConduiteEssai.class)
    @JoinColumn(name = RefDataConstantes.SYSTCONDUITEESSAI_ID, referencedColumnName = SystemeConduiteEssai.ID_JPA, nullable = false)
    private SystemeConduiteEssai systemeConduite;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = SystemeConduiteEssai.class)
    @JoinColumn(name = RefDataConstantes.NAME_ATTRIBUT_SYSTCHGTCONDUITEESSAI, referencedColumnName = SystemeConduiteEssai.ID_JPA, nullable = true)
    private SystemeConduiteEssai chgtSystemeConduite;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Geolocalisation.class)
    @JoinColumn(name = RefDataConstantes.GEOLOC_ID, referencedColumnName = Geolocalisation.ID_JPA, nullable = true)
    private Geolocalisation geolocalisation;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_DISP,  length = 255)
    private String nomDispo;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_CODE_DISP,  length = 25)
    private String codeDispo;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_SURFACETOT_DISP)
    private Float surfaceTotale;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ISCHGTSYSTCONDUITE_DISP)
    private Boolean isChgtSystemeConduite;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ANNEEDEBUT_DISP, length = 4)
    private String anneeDebutDispositif;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEFIN_DISP, length = 4)
    private String anneeFinDispositif;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEARRET_EPANDAGE, length = 4)
    private String anneeArretEpandage;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEE_CHGT_SYSTCONDUITE_DISP, length = 4)
    private String anneeChgtSystemeConduite;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_RAISON_CHGT_SYSTCONDUITE_DISP)
    private String raisonChgtSystemeConduite;

    /**
     *
     */
    public Dispositif() {
    }

    /**
     * @param typeDispositif
     * @param lieu
     * @param systemeConduite
     * @param chgtSystemeConduite
     * @param geolocalisation
     * @param nomDispo
     * @param code
     * @param surfaceTotale
     * @param isChgtSystemeConduite
     * @param anneeDebutDispositif
     * @param anneeFinDispositif
     * @param anneeArretEpandage
     * @param anneeChgtSystemeConduite
     * @param raisonChgtSystemeConduite
     */
    public Dispositif(TypeDispositif typeDispositif, Lieu lieu,
            SystemeConduiteEssai systemeConduite,
            SystemeConduiteEssai chgtSystemeConduite,
            Geolocalisation geolocalisation, String nomDispo, String code,
            Float surfaceTotale, Boolean isChgtSystemeConduite,
            String anneeDebutDispositif, String anneeFinDispositif,
            String anneeArretEpandage, String anneeChgtSystemeConduite,
            String raisonChgtSystemeConduite) {
        super(Utils.createCodeFromString(String.format("%s_%s", lieu.getNom(), code)));
        this.codeDispo = code;
        this.typeDispositif = typeDispositif;
        this.lieu = lieu;
        this.systemeConduite = systemeConduite;
        this.chgtSystemeConduite = chgtSystemeConduite;
        this.geolocalisation = geolocalisation;
        this.nomDispo = nomDispo;
        this.surfaceTotale = surfaceTotale;
        this.isChgtSystemeConduite = isChgtSystemeConduite;
        this.anneeDebutDispositif = anneeDebutDispositif;
        this.anneeFinDispositif = anneeFinDispositif;
        this.anneeArretEpandage = anneeArretEpandage;
        this.anneeChgtSystemeConduite = anneeChgtSystemeConduite;
        this.raisonChgtSystemeConduite = raisonChgtSystemeConduite;
    }

    /**
     * @return the typeDispositif
     */
    public TypeDispositif getTypeDispositif() {
        return typeDispositif;
    }

    /**
     * @param typeDispositif the typeDispositif to set
     */
    public void setTypeDispositif(TypeDispositif typeDispositif) {
        this.typeDispositif = typeDispositif;
    }

    /**
     * @return the lieu
     */
    public Lieu getLieu() {
        return lieu;
    }

    /**
     * @param lieu the lieu to set
     */
    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    /**
     * @return the systemeConduite
     */
    public SystemeConduiteEssai getSystemeConduite() {
        return systemeConduite;
    }

    /**
     * @param systemeConduite the systemeConduite to set
     */
    public void setSystemeConduite(SystemeConduiteEssai systemeConduite) {
        this.systemeConduite = systemeConduite;
    }

    /**
     * @return the chgtSystemeConduite
     */
    public SystemeConduiteEssai getChgtSystemeConduite() {
        return chgtSystemeConduite;
    }

    /**
     * @param chgtSystemeConduite the chgtSystemeConduite to set
     */
    public void setChgtSystemeConduite(SystemeConduiteEssai chgtSystemeConduite) {
        this.chgtSystemeConduite = chgtSystemeConduite;
    }

    /**
     * @return the geolocalisation
     */
    public Geolocalisation getGeolocalisation() {
        return geolocalisation;
    }

    /**
     * @param geolocalisation the geolocalisation to set
     */
    public void setGeolocalisation(Geolocalisation geolocalisation) {
        this.geolocalisation = geolocalisation;
    }

    /**
     *
     * @return
     */
    public String getNomDispo() {
        return nomDispo;
    }

    /**
     *
     * @param nomDispo
     */
    public void setNomDispo(String nomDispo) {
        this.nomDispo = nomDispo;
    }

    /**
     *
     * @return
     */
    public String getCodeDispo() {
        return codeDispo;
    }

    /**
     *
     * @param codeDispo
     */
    public void setCodeDispo(String codeDispo) {
        this.codeDispo = codeDispo;
    }

    /**
     * @return the surfaceTotale
     */
    public Float getSurfaceTotale() {
        return surfaceTotale;
    }

    /**
     * @param surfaceTotale the surfaceTotale to set
     */
    public void setSurfaceTotale(Float surfaceTotale) {
        this.surfaceTotale = surfaceTotale;
    }

    /**
     * @return the isChgtSystemeConduite
     */
    public Boolean getIsChgtSystemeConduite() {
        return isChgtSystemeConduite;
    }

    /**
     * @param isChgtSystemeConduite the isChgtSystemeConduite to set
     */
    public void setIsChgtSystemeConduite(Boolean isChgtSystemeConduite) {
        this.isChgtSystemeConduite = isChgtSystemeConduite;
    }

    /**
     * @return the anneeDebutDispositif
     */
    public String getAnneeDebutDispositif() {
        return anneeDebutDispositif;
    }

    /**
     * @param anneeDebutDispositif the anneeDebutDispositif to set
     */
    public void setAnneeDebutDispositif(String anneeDebutDispositif) {
        this.anneeDebutDispositif = anneeDebutDispositif;
    }

    /**
     * @return the anneeFinDispositif
     */
    public String getAnneeFinDispositif() {
        return anneeFinDispositif;
    }

    /**
     * @param anneeFinDispositif the anneeFinDispositif to set
     */
    public void setAnneeFinDispositif(String anneeFinDispositif) {
        this.anneeFinDispositif = anneeFinDispositif;
    }

    /**
     * @return the anneeArretEpandage
     */
    public String getAnneeArretEpandage() {
        return anneeArretEpandage;
    }

    /**
     * @param anneeArretEpandage the anneeArretEpandage to set
     */
    public void setAnneeArretEpandage(String anneeArretEpandage) {
        this.anneeArretEpandage = anneeArretEpandage;
    }

    /**
     * @return the anneeChgtSystemeConduite
     */
    public String getAnneeChgtSystemeConduite() {
        return anneeChgtSystemeConduite;
    }

    /**
     * @param anneeChgtSystemeConduite the anneeChgtSystemeConduite to set
     */
    public void setAnneeChgtSystemeConduite(String anneeChgtSystemeConduite) {
        this.anneeChgtSystemeConduite = anneeChgtSystemeConduite;
    }

    /**
     * @return the raisonChgtSystemeConduite
     */
    public String getRaisonChgtSystemeConduite() {
        return raisonChgtSystemeConduite;
    }

    /**
     * @param raisonChgtSystemeConduite the raisonChgtSystemeConduite to set
     */
    public void setRaisonChgtSystemeConduite(String raisonChgtSystemeConduite) {
        this.raisonChgtSystemeConduite = raisonChgtSystemeConduite;
    }

    /**
     *
     * @return
     */
    @Transient
    public String getCodeforFileName() {
        return String.format("%s-%s", codeDispo, lieu.getNom());
    }

    /**
     *
     * @return
     */
    public String getName() {
        return getNomDispo();
    }

    /**
     *
     * @param nomDispo
     * @param name
     */
    public void setName(String nomDispo) {
        setNomDispo(nomDispo);
    }

//    public Long getDispoId() {
//        return dispoId;
//    }
//
//    public void setDispoId(Long dispoId) {
//        this.dispoId = dispoId;
//    }
    @Override
    public int compareTo(INodeable o) {
        return this.getName().compareTo(o.getName());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(getCode());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dispositif other = (Dispositif) obj;
        return Objects.equals(getCode(), other.getCode());
    }

    /**
     *
     * @return
     */
    @Override
    public Class<Dispositif> getNodeableType() {
        return Dispositif.class;
    }

    /**
     *
     * @return
     */
    public String getNomDispositif_nomLieu(){
        return String.format(PATTERN_NOM_DISPOSITIF_NOM_LIEU, codeDispo, Optional.ofNullable(lieu).map(Lieu::getNom).orElse(""));
    }

}
