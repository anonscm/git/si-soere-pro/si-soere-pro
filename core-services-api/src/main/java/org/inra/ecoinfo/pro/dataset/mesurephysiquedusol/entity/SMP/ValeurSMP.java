/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;



/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name = ValeurSMP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    MesureSMP.ATTRIBUTE_JPA_ID, 
    RealNode.ID_JPA, 
    ValeurSMP.ATTRIBUTE_JPA_NUM_REPETITION}))

public class ValeurSMP implements Serializable{
    
    /**
     *
     */
    public static final String ID_JPA = "vsmp_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_smp";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_MEASURE_SMP = "mesureSMP";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String  ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NUM_REPETITION = "num_repetition";
    
   
    static final long serialVersionUID = 1L;
    
    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long  vsmp_id;
    
    @Column(name = ValeurSMP.COLUMN_JPA_VALUE, nullable = false)
    private Float valeur_smp;
    
    @Column(name=ValeurSMP.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;
    
    @Column(name = ValeurSMP.ATTRIBUTE_JPA_NUM_REPETITION, nullable = false)
    int                        numRepetition;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureSMP.ATTRIBUTE_JPA_ID, referencedColumnName = MesureSMP.ATTRIBUTE_JPA_ID)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureSMP mesureSMP;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
     private RealNode realNode;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    /**
     *
     */
    public ValeurSMP() {
        super();
    }

    /**
     *
     * @param valeur_smp
     * @param statutvaleur
     * @param numRepetition
     * @param mesureSMP
     * @param realNode
     */
    public ValeurSMP(Float valeur_smp, String statutvaleur, int numRepetition, MesureSMP mesureSMP, RealNode realNode) {
        this.valeur_smp = valeur_smp;
        this.statutvaleur = statutvaleur;
        this.numRepetition = numRepetition;
        this.mesureSMP = mesureSMP;
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Long getVsmp_id() {
        return vsmp_id;
    }

    /**
     *
     * @param vsmp_id
     */
    public void setVsmp_id(Long vsmp_id) {
        this.vsmp_id = vsmp_id;
    }

    /**
     *
     * @return
     */
    public Float getValeur_smp() {
        return valeur_smp;
    }

    /**
     *
     * @param valeur_smp
     */
    public void setValeur_smp(Float valeur_smp) {
        this.valeur_smp = valeur_smp;
    }

    /**
     *
     * @return
     */
    public MesureSMP getMesureSMP() {
        return mesureSMP;
    }

    /**
     *
     * @param mesureSMP
     */
    public void setMesureSMP(MesureSMP mesureSMP) {
        this.mesureSMP = mesureSMP;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public int getNumRepetition() {
        return numRepetition;
    }

    /**
     *
     * @param numRepetition
     */
    public void setNumRepetition(int numRepetition) {
        this.numRepetition = numRepetition;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }
    
    
    
}
