
package org.inra.ecoinfo.pro.dataset.itk.entity;

import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
@MappedSuperclass
public class MesureITK<M extends MesureITK, V extends ValeurITK>  implements Serializable{

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "mesureid";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_DISPOSITIF = "codedispositif";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PARCELLE = "nomparcelle";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PLACETTE = "nomplacette";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LocalDate_DEBUT = "LocalDatedebut";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LocalDate_FIN = "LocalDatefin";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_CULTURE = "nomculture";

    /**
     *
     */
    public static final String ATTRIBUTE_TYPEOBSERVATION = "typeobservation";

    /**
     *
     */
    public static final String ATTRIBUTE_NOMOBSERVATION = "nomobservation";

    /**
     *
     */
    public static final String ATTRIBUTE_COMMENTAIRE = "commentaire";

    /**
     *
     */
    public static final String ATTRIBUTE_KEYMESURE = "keymesure";


    @Id
    @Column(name = ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long mesure_id;

    @Column(name = ATTRIBUTE_JPA_CULTURE, nullable = false)
    private String nomculture;

    @Column(name = ATTRIBUTE_JPA_DISPOSITIF, nullable = false)
    private String codedispositif;

    @Column(name = ATTRIBUTE_KEYMESURE, nullable = false, unique = true)
    private String keymesure;

    @Column(name = ATTRIBUTE_JPA_PARCELLE, nullable = false)
    private String nomparcelle;

    @Column(name = ATTRIBUTE_JPA_PLACETTE, nullable = false)
    private String nomplacette;

    @Column(name = ATTRIBUTE_JPA_LocalDate_DEBUT, nullable = false)
    LocalDate datedebut;

    @Column(name = ATTRIBUTE_JPA_LocalDate_FIN, nullable = true)
    LocalDate datefin;
    @Column(name = ATTRIBUTE_TYPEOBSERVATION, nullable = false)
    private String typeobservation;
    @Column(name = ATTRIBUTE_NOMOBSERVATION, nullable = false)
    private String nomobservation;
    @Column(name = ATTRIBUTE_COMMENTAIRE, length = 300)
    private String commentaire = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
            VersionFile versionfile;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = DescriptionTraitement.class)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, referencedColumnName = DescriptionTraitement.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
            DescriptionTraitement descriptionTraitement;

    /**
     *
     */
    public MesureITK() {
    }

    /**
     *
     * @param nomculture
     * @param codedispositif
     * @param keymesure
     * @param nomparcelle
     * @param nomplacette
     * @param datedebut
     * @param datefin
     * @param typeobservation
     * @param nomobservation
     * @param versionfile
     * @param descriptionTraitement
     */
    public MesureITK(String nomculture, String codedispositif, String keymesure, String nomparcelle, String nomplacette, LocalDate datedebut, LocalDate datefin, String typeobservation, String nomobservation, VersionFile versionfile, DescriptionTraitement descriptionTraitement) {
        this.nomculture = nomculture;
        this.codedispositif = codedispositif;
        this.keymesure = keymesure;
        this.nomparcelle = nomparcelle;
        this.nomplacette = nomplacette;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.typeobservation = typeobservation;
        this.nomobservation = nomobservation;
        this.versionfile = versionfile;
        this.descriptionTraitement = descriptionTraitement;
    }

    /**
     *
     * @return
     */
    public Long getMesure_id() {
        return mesure_id;
    }

    /**
     *
     * @param mesure_id
     */
    public void setMesure_id(Long mesure_id) {
        this.mesure_id = mesure_id;
    }

    /**
     *
     * @return
     */
    public String getNomculture() {
        return nomculture;
    }

    /**
     *
     * @param nomculture
     */
    public void setNomculture(String nomculture) {
        this.nomculture = nomculture;
    }

    /**
     *
     * @return
     */
    public String getCodedispositif() {
        return codedispositif;
    }

    /**
     *
     * @param codedispositif
     */
    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

    /**
     *
     * @return
     */
    public String getNomparcelle() {
        return nomparcelle;
    }

    /**
     *
     * @param nomparcelle
     */
    public void setNomparcelle(String nomparcelle) {
        this.nomparcelle = nomparcelle;
    }

    /**
     *
     * @return
     */
    public String getNomplacette() {
        return nomplacette;
    }

    /**
     *
     * @param nomplacette
     */
    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatedebut() {
        return datedebut;
    }

    /**
     *
     * @param datedebut
     */
    public void setDatedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatefin() {
        return datefin;
    }

    /**
     *
     * @param datefin
     */
    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    /**
     *
     * @return
     */
    public String getTypeobservation() {
        return typeobservation;
    }

    /**
     *
     * @param typeobservation
     */
    public void setTypeobservation(String typeobservation) {
        this.typeobservation = typeobservation;
    }

    /**
     *
     * @return
     */
    public String getNomobservation() {
        return nomobservation;
    }

    /**
     *
     * @param nomobservation
     */
    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersionfile() {
        return versionfile;
    }

    /**
     *
     * @param versionfile
     */
    public void setVersionfile(VersionFile versionfile) {
        this.versionfile = versionfile;
    }

    /**
     *
     * @return
     */
    public DescriptionTraitement getDescriptionTraitement() {
        return descriptionTraitement;
    }

    /**
     *
     * @param descriptionTraitement
     */
    public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    
}
