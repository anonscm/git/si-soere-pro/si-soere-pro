/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author tcherniatinsky
 * @param <T>
 */
public interface IVariableManager<T> {

    /**
     *
     * @param linkedList
     * @param intervals
     * @return
     */
    Map<String, List<NodeDataSet>> getAvailableVariablesByDispositif(List<Dispositif> linkedList);
 }
