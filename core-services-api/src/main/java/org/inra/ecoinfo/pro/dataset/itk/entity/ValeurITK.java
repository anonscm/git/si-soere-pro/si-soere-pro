/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.entity;

import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
@MappedSuperclass
public class ValeurITK<M extends MesureITK, V extends ValeurITK> {

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_QUANTITE = "valeur";


    @Id
    @Column(name = ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = ATTRIBUTE_JPA_QUANTITE)
    private Float valeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private RealNode realNode;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, targetEntity = ListeItineraire.class)
    @JoinColumn(name = ListeItineraire.JPA_ID, referencedColumnName = ListeItineraire.JPA_ID)
    ListeItineraire listeItineraire;

    /**
     *
     * @param valeur
     * @param realNode
     */
    public ValeurITK(Float valeur, RealNode realNode) {
        this.valeur = valeur;
        this.realNode = realNode;
        this.listeItineraire = listeItineraire;
    }

    /**
     *
     * @param listeItineraire
     * @param realNode
     */
    public ValeurITK(ListeItineraire listeItineraire, RealNode realNode) {
        this.valeur = valeur;
        this.realNode = realNode;
        this.listeItineraire = listeItineraire;
    }

    /**
     *
     */
    public ValeurITK() {
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public ListeItineraire getListeItineraire() {
        return listeItineraire;
    }

    /**
     *
     * @param listeItineraire
     */
    public void setListeItineraire(ListeItineraire listeItineraire) {
        this.listeItineraire = listeItineraire;
    }
    
}
