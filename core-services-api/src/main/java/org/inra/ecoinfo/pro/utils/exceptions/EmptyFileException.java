/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils.exceptions;

import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class EmptyFileException  extends BusinessException{
    static final long serialVersionUID = 1L;

    /**
     *
     */
    public EmptyFileException() {
        super();
    }

    /**
     *
     * @param arg0
     */
    public EmptyFileException(final String arg0) {
        super(arg0);
    }

    /**
     *
     * @param arg0
     * @param arg1
     */
    public EmptyFileException(final String arg0, final Throwable arg1) {
        super(arg0, arg1);
    }

    /**
     *
     * @param arg0
     */
    public EmptyFileException(final Throwable arg0) {
        super(arg0);
    }
    
}
