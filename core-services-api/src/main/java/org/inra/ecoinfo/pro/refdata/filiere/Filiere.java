/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.filiere;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Filiere.NAME_ENTITY_JPA)
public class Filiere implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "filiere";

    /**
     *
     */
    public static final String JPA_ID = "filiere_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "filiere_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "filiere_nom";
    
    @Id
    @Column(name = Filiere.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long filiere_id;
    @Column(name = Filiere.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String filiere_code = "";
    @Column(name = Filiere.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String filiere_nom = "";
    
    /**
     *
     */
    public Filiere(){
        super();
    }
    
    /**
     *
     * @param nom
     */
    public Filiere(String nom){
        this.filiere_nom = nom;
    }

    /**
     *
     * @return
     */
    public long getFiliere_id() {
        return filiere_id;
    }

    /**
     *
     * @return
     */
    public String getFiliere_code() {
        return filiere_code;
    }

    /**
     *
     * @return
     */
    public String getFiliere_nom() {
        return filiere_nom;
    }

    /**
     *
     * @param filiere_id
     */
    public void setFiliere_id(long filiere_id) {
        this.filiere_id = filiere_id;
    }

    /**
     *
     * @param filiere_code
     */
    public void setFiliere_code(String filiere_code) {
        this.filiere_code = filiere_code;
    }

    /**
     *
     * @param filiere_nom
     */
    public void setFiliere_nom(String filiere_nom) {
        this.filiere_nom = filiere_nom;
    }
    
    
}
