/*
 *
 */
package org.inra.ecoinfo.pro.refdata.produit;

import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.nomenclature.Nomenclatures;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Produits.NAME_ENTITY_JPA,
        uniqueConstraints
        = @UniqueConstraint(
                columnNames = {Produits.JPA_COLUMN_CODEUSER,
                    Produits.JPA_COLUMN_LIEU,
                    Produits.JPA_COLUMN_ANNEE,
                    Produits.JPA_COLUMN_STRUCTURE_DETENTEUR
                }
        )
)
@PrimaryKeyJoinColumn(name = Composant.JPA_ID)
public class Produits extends Composant<Produits> {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "produit";

    /**
     *
     */
    public static final String ID_JPA = "prod_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "prod_key";

    /**
     *
     */
    public static final String JPA_COLUMN_CODEUSER = "prod_codeuser";

    /**
     *
     */
    public static final String JPA_COLUMN_STATUT = "prod_statut";

    /**
     *
     */
    public static final String JPA_COLUMN_ANNEE = "prod_annee";

    /**
     *
     */
    public static final String JPA_COLUMN_LIEU = "prod_lieu";

    /**
     *
     */
    public static final String JPA_COLUMN_FPHYSIQUE = "prod_formephysique";

    /**
     *
     */
    public static final String JPA_COLUMN_NOMCIAL = "prod_nomcial";

    /**
     *
     */
    public static final String JPA_COLUMN__TENEURNPK = "prod_teneurnpk";

    /**
     *
     */
    public static final String JPA_COLUMN_ETUDIE = "pro_etudie";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "prod_comment";

    /**
     *
     */
    public static final String JPA_COLUMN_EPANDU = "prod_epandu";

    /**
     *
     */
    public static final String JPA_COLUMN_STRUCTURE_DETENTEUR = "stc_Detenteur";

    public static final String PATTERN_CODE_PRODUIT = "%s-%s-%s-%s";
    public static final String PATTERN_DETENTEUR_ANNEE_DEPARTEMENT = "-%s-%s-%s";

    public static String createCodeComposant(String codeUser, String detenteur, int annee, String noDepartement) {
        return String.format(
                PATTERN_CODE_PRODUIT,
                codeUser.replace(String.format(PATTERN_DETENTEUR_ANNEE_DEPARTEMENT, detenteur, annee,noDepartement), ""),
                detenteur,
                annee,
                noDepartement
        );
    }

    public static String createCodeProduits(String codeUser, String detenteur, int annee, String noDepartement) {
        return codeUser.replace(String.format(PATTERN_DETENTEUR_ANNEE_DEPARTEMENT, detenteur, annee,noDepartement), "");
    }
    
    @Column(name = Produits.JPA_COLUMN_CODEUSER, nullable = false)
    private String prod_codeuser = "";
    @Column(name = Produits.JPA_COLUMN_LIEU, nullable = false, length = 3)
    private String prod_lieu = null;
    @Column(name = Produits.JPA_COLUMN_ANNEE, nullable = false)
    private int prod_annee = 0;
    @Column(name = Produits.JPA_COLUMN_STATUT, length = 50)
    private String prod_statut = "";
    @Column(name = Produits.JPA_COLUMN_FPHYSIQUE, length = 50)
    private String prod_formephysique = "";
    @Column(name = Produits.JPA_COLUMN_NOMCIAL, length = 50)
    private String prod_nomcial = "";
    @Column(name = Produits.JPA_COLUMN__TENEURNPK, length = 50)
    private String prod_teneurnpk = "";
    @Column(name = Produits.JPA_COLUMN_ETUDIE)
    private String pro_etudie;
    @Column(name = Produits.JPA_COLUMN_COMMENT, length = 255)
    private String prod_comment = "";
    @NotNull
    @Column(name = Produits.JPA_COLUMN_EPANDU, nullable = false)
    private String prod_epandu;
    @NotNull
    @Column(name = Produits.JPA_COLUMN_STRUCTURE_DETENTEUR, nullable = false, length = 255)
    private String stc_Detenteur = "";
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = Nomenclatures.JPA_ID, referencedColumnName = Nomenclatures.JPA_ID, nullable = false)
    private Nomenclatures nomenclatures;

    /**
     *
     */
    public Produits() {
        super();
    }

    /**
     *
     * @param codeComposant
     * @param codeuser
     * @param nomenclatures
     * @param statut
     * @param annee
     * @param lieu
     * @param fphysique
     * @param nomcial
     * @param tnpk
     * @param etudie
     * @param comment
     * @param epandu
     * @param detenteur
     */
    public Produits(String codeComposant, String codeuser, Nomenclatures nomenclatures, String statut, int annee, String lieu, String fphysique, String nomcial, String tnpk, String etudie, String comment, String epandu,
            String detenteur) {
        super(codeComposant, Boolean.TRUE);
        this.prod_codeuser = codeuser;
        this.prod_statut = statut;
        this.prod_annee = annee;
        this.prod_lieu = lieu;
        this.prod_formephysique = fphysique;
        this.prod_nomcial = nomcial;
        this.prod_teneurnpk = tnpk;
        this.pro_etudie = etudie;
        this.prod_comment = comment;
        this.prod_epandu = epandu;
        this.nomenclatures = nomenclatures;
        this.stc_Detenteur = detenteur;
    }

    /**
     *
     * @return
     */
    public Nomenclatures getNomenclatures() {
        return nomenclatures;
    }

    /**
     *
     * @return
     */
    public int getProd_annee() {
        return prod_annee;
    }

    /**
     *
     * @return
     */
    public String getProd_codeuser() {
        return prod_codeuser;
    }

    /**
     *
     * @return
     */
    public String getProd_comment() {
        return prod_comment;
    }

    /**
     *
     * @return
     */
    public String getProd_formephysique() {
        return prod_formephysique;
    }

    /**
     *
     * @return
     */
    public String getProd_lieu() {
        return prod_lieu;
    }

    /**
     *
     * @return
     */
    public String getProd_nomcial() {
        return prod_nomcial;
    }

    /**
     *
     * @return
     */
    public String getProd_statut() {
        return prod_statut;
    }

    /**
     *
     * @return
     */
    public String getProd_teneurnpk() {
        return prod_teneurnpk;
    }

    /**
     *
     * @return
     */
    public String getPro_etudie() {
        return pro_etudie;
    }

    /**
     *
     * @return
     */
    public String getProd_epandu() {
        return prod_epandu;
    }

    /**
     *
     * @param nomenclatures
     */
    public void setNomenclatures(Nomenclatures nomenclatures) {
        this.nomenclatures = nomenclatures;
    }

    /**
     *
     * @param prod_annee
     */
    public void setProd_annee(int prod_annee) {
        this.prod_annee = prod_annee;
    }

    /**
     *
     * @param prod_codeuser
     */
    public void setProd_codeuser(String prod_codeuser) {
        this.prod_codeuser = prod_codeuser;
    }

    /**
     *
     * @param prod_comment
     */
    public void setProd_comment(String prod_comment) {
        this.prod_comment = prod_comment;
    }

    /**
     *
     * @param pro_etudie
     */
    public void setPro_etudie(String pro_etudie) {
        this.pro_etudie = pro_etudie;
    }

    /**
     *
     * @param prod_epandu
     */
    public void setProd_epandu(String prod_epandu) {
        this.prod_epandu = prod_epandu;
    }

    /**
     *
     * @param prod_formephysique
     */
    public void setProd_formephysique(String prod_formephysique) {
        this.prod_formephysique = prod_formephysique;
    }

    /**
     *
     * @param prod_lieu
     */
    public void setProd_lieu(String prod_lieu) {
        this.prod_lieu = prod_lieu;
    }

    /**
     *
     * @param prod_nomcial
     */
    public void setProd_nomcial(String prod_nomcial) {
        this.prod_nomcial = prod_nomcial;
    }

    /**
     *
     * @param prod_statut
     */
    public void setProd_statut(String prod_statut) {
        this.prod_statut = prod_statut;
    }

    /**
     *
     * @param prod_teneurnpk
     */
    public void setProd_teneurnpk(String prod_teneurnpk) {
        this.prod_teneurnpk = prod_teneurnpk;
    }

    /**
     *
     * @return
     */
    public String getStc_Detenteur() {
        return stc_Detenteur;
    }

    /**
     *
     * @param stc_Detenteur
     */
    public void setStc_Detenteur(String stc_Detenteur) {
        this.stc_Detenteur = stc_Detenteur;
    }

    /**
     *
     * @return
     */
    public String getProd_key() {
        return codecomposant;
    }

    /**
     *
     * @param prod_key
     */
    public void setProd_key(String prod_key) {
        this.codecomposant = prod_key;
    }

    @Override
    public int compareTo(Produits o) {
        return codecomposant.compareTo(o.codecomposant);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.codecomposant);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produits other = (Produits) obj;
        return Objects.equals(this.codecomposant, other.codecomposant);
    }

}
