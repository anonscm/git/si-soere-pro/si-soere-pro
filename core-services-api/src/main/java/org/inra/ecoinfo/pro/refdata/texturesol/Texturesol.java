package org.inra.ecoinfo.pro.refdata.texturesol;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Texturesol.NAME_ENTITY_JPA)
        
public class Texturesol implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "texturesol";

    /**
     *
     */
    public static final String ID_JPA = "tsid";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Texturesol.ID_JPA, updatable = false)
    private long tsid = 0;
    @Column(name= Texturesol.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name=  Texturesol.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String nom = "";
    
    /**
     *
     */
    public Texturesol(){
        super();
    }
    
    /**
     *
     * @param code
     * @param nom
     */
    public Texturesol(String code, String nom) {
        this.nom=nom;
        this.code=code;
        
    }

    /**
     *
     * @return
     */
    public long getTsid() {
        return tsid;
    }

    /**
     *
     * @param tsid
     */
    public void setTsid(long tsid) {
        this.tsid = tsid;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

}
