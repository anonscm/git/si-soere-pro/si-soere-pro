/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 *
 * @author vjkoyao
 */

@Entity 
@Table(name = SequenceSWC.NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    SequenceSWC.ATTRIBUTE_JPA_LocalDate, VersionFile.ID_JPA }))

public class SequenceSWC implements Serializable{
    
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID   = "sswc_id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LocalDate = "LocalDate";
    
    static final long          serialVersionUID   = 1L;
    
    /**
     *
     */
    public static final String    NAME_TABLE         = "sequence_swc";

    
    
    @Id
    @Column(name=SequenceSWC.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long                       id;

    @Column(name = SequenceSWC.ATTRIBUTE_JPA_LocalDate, nullable = false)
    LocalDate                          LocalDate;
    
    
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    VersionFile                version;

   
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = ParcelleElementaire.class)
    @JoinColumn(name = ParcelleElementaire.ID_JPA, referencedColumnName = ParcelleElementaire.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    ParcelleElementaire             parcelleElementaire;
    
    @OneToMany(mappedBy = SousSequenceSWC.ATTRIBUTE_JPA_ID, cascade = {CascadeType.ALL})
    private List<SousSequenceSWC> sousSequences = new LinkedList<SousSequenceSWC>();

    /**
     *
     */
    public SequenceSWC() {
        super();
    }

    /**
     *
     * @param LocalDate
     * @param version
     * @param parcelleElementaire
     */
    public SequenceSWC(LocalDate LocalDate, VersionFile version, ParcelleElementaire parcelleElementaire) {
        this.LocalDate = LocalDate;
        this.version = version;
        this.parcelleElementaire = parcelleElementaire;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDate() {
        return LocalDate;
    }

    /**
     *
     * @param LocalDate
     */
    public void setLocalDate(LocalDate LocalDate) {
        this.LocalDate = LocalDate;
    }

    /**
     *
     * @return
     */
    public VersionFile getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(VersionFile version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public ParcelleElementaire getParcelleElementaire() {
        return parcelleElementaire;
    }

    /**
     *
     * @param parcelleElementaire
     */
    public void setParcelleElementaire(ParcelleElementaire parcelleElementaire) {
        this.parcelleElementaire = parcelleElementaire;
    }

    /**
     *
     * @return
     */
    public List<SousSequenceSWC> getSousSequences() {
        return sousSequences;
    }

    /**
     *
     * @param sousSequences
     */
    public void setSousSequences(List<SousSequenceSWC> sousSequences) {
        this.sousSequences = sousSequences;
    }
    
    
    
}
