/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = ValeurPhysicoChimieSols.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    MesurePhysicoChimieSols.ID_JPA,
    RealNode.ID_JPA,
    ValeurPhysicoChimieSols.ATTRIBUTE_JPA_STATUT_VALEUR}
),
        indexes = {
            @Index(name = "valeur_physico_chimie_sol_mesure_idx", columnList = MesurePhysicoChimieSols.ID_JPA),
            @Index(name = "valeur_physico_chimie_sol_variable", columnList = RealNode.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = ValeurPhysicoChimie.ID_JPA, referencedColumnName = ValeurPhysicoChimieSols.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurPhysicoChimie.ID_JPA, column = @Column(name = ValeurPhysicoChimieSols.ID_JPA))})
public class ValeurPhysicoChimieSols extends ValeurPhysicoChimie<MesurePhysicoChimieSols, ValeurPhysicoChimieSols> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurphysicochimiesols";

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    @Column(name = ValeurPhysicoChimieSols.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesurePhysicoChimieSols.class)
    @JoinColumn(name = MesurePhysicoChimieSols.ID_JPA, referencedColumnName = MesurePhysicoChimieSols.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesurePhysicoChimieSols mesurePhysicoChimieSols;

    /**
     *
     */
    public ValeurPhysicoChimieSols() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param mesure_physicochimie
     * @param realNode
     */
    public ValeurPhysicoChimieSols(Float valeur, String statutvaleur, MesurePhysicoChimieSols mesure_physicochimie, RealNode realNode) {
        super(valeur, realNode);
        this.statutvaleur = statutvaleur;
        this.mesurePhysicoChimieSols = mesure_physicochimie;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public MesurePhysicoChimieSols getMesure_physicochimiesols() {
        return mesurePhysicoChimieSols;
    }

    /**
     *
     * @param mesure_physicochimiesols
     */
    public void setMesure_physicochimiesols(MesurePhysicoChimieSols mesure_physicochimiesols) {
        this.mesurePhysicoChimieSols = mesure_physicochimiesols;
    }

    /**
     *
     * @return
     */
    public MesurePhysicoChimieSols getMesurePhysicoChimieSols() {
        return mesurePhysicoChimieSols;
    }

    /**
     *
     * @param mesurePhysicoChimieSols
     */
    public void setMesurePhysicoChimieSols(MesurePhysicoChimieSols mesurePhysicoChimieSols) {
        this.mesurePhysicoChimieSols = mesurePhysicoChimieSols;
    }

}
