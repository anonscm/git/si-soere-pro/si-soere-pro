/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import static org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK.ATTRIBUTE_JPA_DISPOSITIF;
import static org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK.ATTRIBUTE_JPA_LocalDate_DEBUT;
import static org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK.ATTRIBUTE_JPA_PARCELLE;
import static org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK.ATTRIBUTE_JPA_PLACETTE;
import static org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport.ATTRIBUTE_JPA_INTERVENTION;
import static org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport.ATTRIBUTE_JPA_NOMCOMMERCIAL_PRODUIT;
import static org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport.ATTRIBUTE_JPA_TYPEINTERVENTION;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureApport.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    ATTRIBUTE_JPA_DISPOSITIF,
    DescriptionTraitement.ID_JPA,
    ATTRIBUTE_JPA_PARCELLE,
    ATTRIBUTE_JPA_LocalDate_DEBUT,
    ATTRIBUTE_JPA_PLACETTE,
    ATTRIBUTE_JPA_NOMCOMMERCIAL_PRODUIT,
    ATTRIBUTE_JPA_TYPEINTERVENTION,
    ATTRIBUTE_JPA_INTERVENTION,}))
public class MesureApport extends MesureITK<MesureApport, ValeurApport> {

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_TYPEINTERVENTION = "typeintervention";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_INTERVENTION = "intervention";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NOMCOMMERCIAL_PRODUIT = "nomcommercialproduit";

    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL = "materielapport";

    /**
     *
     */
    public static final String ATTRIBUTE_NIVEAU = "niveauatteint";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_BBCH = "codebbch";

    /**
     *
     */
    public static final String ATTRIBUTE_PRECISION_STADE = "precisionstade";

    /**
     *
     */
    public static final String TABLE_NAME = "mesureinterventionapport";

    static final long serialVersionUID = 1L;

    @Column(name = MesureApport.ATTRIBUTE_JPA_TYPEINTERVENTION, nullable = false)
    private String typeintervention;

    @Column(name = MesureApport.ATTRIBUTE_JPA_INTERVENTION, nullable = false)
    private String intervention;

    @Column(name = MesureApport.ATTRIBUTE_JPA_NOMCOMMERCIAL_PRODUIT, nullable = false)
    private String nomcommercialproduit;

    @Column(name = MesureApport.ATTRIBUTE_MATERIEL, length = 300)
    private String materielapport = "";

    @Column(name = MesureApport.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint;

    @Column(name = MesureApport.ATTRIBUTE_JPA_BBCH, nullable = true)
    private String codebbch;

    @Column(name = MesureApport.ATTRIBUTE_PRECISION_STADE, nullable = true)
    private String precisionstade = "";

    @OneToMany(mappedBy = "mesureinterventionapport", cascade = ALL)
    List<ValeurApport> valeurapport = new LinkedList();

    /**
     *
     */
    public MesureApport() {
        super();
    }

    /**
     *
     * @param codedispositif
     * @param codetraitement
     * @param nomparcelle
     * @param nomplacette
     * @param LocalDatedebut
     * @param LocalDatefin
     * @param typeintervention
     * @param intervention
     * @param nomcommercialproduit
     * @param materiel
     * @param precision
     * @param nomculture
     * @param codebbch
     * @param typeobservation
     * @param nomobservation
     * @param niveauatteint
     * @param versionfile
     * @param commentaire
     */
    public MesureApport(String codedispositif, DescriptionTraitement codetraitement, String nomparcelle, String nomplacette, LocalDate LocalDatedebut, LocalDate LocalDatefin,
            String typeintervention, String intervention, String nomcommercialproduit, String materiel, String precision,
            String nomculture, String codebbch, String typeobservation, String nomobservation, String niveauatteint, VersionFile versionfile, String commentaire) {
        super(nomculture, codedispositif, commentaire, nomparcelle, nomplacette, LocalDatedebut, LocalDatefin, typeobservation, nomobservation, versionfile, codetraitement);
        this.typeintervention = typeintervention;
        this.intervention = intervention;
        this.nomcommercialproduit = nomcommercialproduit;
        this.materielapport = materiel;
        this.niveauatteint = niveauatteint;
        this.codebbch = codebbch;
        this.precisionstade = precision;

    }

    public String getTypeintervention() {
        return typeintervention;
    }

    public void setTypeintervention(String typeintervention) {
        this.typeintervention = typeintervention;
    }

    public String getIntervention() {
        return intervention;
    }

    public void setIntervention(String intervention) {
        this.intervention = intervention;
    }

    public String getNomcommercialproduit() {
        return nomcommercialproduit;
    }

    public void setNomcommercialproduit(String nomcommercialproduit) {
        this.nomcommercialproduit = nomcommercialproduit;
    }

    public String getMaterielapport() {
        return materielapport;
    }

    public void setMaterielapport(String materielapport) {
        this.materielapport = materielapport;
    }

    public String getNiveauatteint() {
        return niveauatteint;
    }

    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    public String getCodebbch() {
        return codebbch;
    }

    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    public String getPrecisionstade() {
        return precisionstade;
    }

    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    public List<ValeurApport> getValeurapport() {
        return valeurapport;
    }

    public void setValeurapport(List<ValeurApport> valeurapport) {
        this.valeurapport = valeurapport;
    }
}
