/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = ValeurPlanteElementaire.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurPlanteElementaire.ATTRIBUTE_JPA_STATUT_VALEUR,
    ValeurPlanteElementaire.ID_JPA,
    MesurePlanteElementaire.ID_JPA
}
),
        indexes = {
            @Index(name = "valeur_plante_mesure_idx", columnList = MesurePlanteElementaire.ID_JPA),
            @Index(name = "valeur_plante_variable", columnList = MesurePlanteElementaire.ID_JPA)
        }
)
@PrimaryKeyJoinColumn(name = ValeurPhysicoChimie.ID_JPA, referencedColumnName = ValeurPlanteElementaire.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurPhysicoChimie.ID_JPA, column = @Column(name = ValeurPlanteElementaire.ID_JPA))})
public class ValeurPlanteElementaire extends ValeurPhysicoChimie<MesurePlanteElementaire, ValeurPlanteElementaire> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurplantebrut";

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    @Column(name = ValeurPlanteElementaire.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesurePlanteElementaire.class)
    @JoinColumn(name = MesurePlanteElementaire.ID_JPA, referencedColumnName = MesurePlanteElementaire.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesurePlanteElementaire mesurePlanteElementaire;

    /**
     *
     */
    public ValeurPlanteElementaire() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param mesurePlanteElementaire
     * @param realNode
     */
    public ValeurPlanteElementaire(Float valeur, String statutvaleur, MesurePlanteElementaire mesurePlanteElementaire, RealNode realNode) {
        super(valeur,realNode);
        this.statutvaleur = statutvaleur;
        this.mesurePlanteElementaire = mesurePlanteElementaire;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public MesurePlanteElementaire getMesurePlanteElementaire() {
        return mesurePlanteElementaire;
    }

    /**
     *
     * @param mesurePlanteElementaire
     */
    public void setMesurePlanteElementaire(MesurePlanteElementaire mesurePlanteElementaire) {
        this.mesurePlanteElementaire = mesurePlanteElementaire;
    }

}
