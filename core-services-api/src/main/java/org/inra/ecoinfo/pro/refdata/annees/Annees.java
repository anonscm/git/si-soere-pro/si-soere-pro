package org.inra.ecoinfo.pro.refdata.annees;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Annees.NAME_ENTITY_JPA)
public class Annees implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "annee";

    /**
     *
     */
    public static final String JPA_ID = "annee_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "annee_valeur";
    @Id
    @Column(name = Annees.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long annee_id;
    @Column(name = Annees.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String annee_valeur = "";

    /**
     *
     */
    public Annees() {
        super();
    }

    /**
     *
     * @param valeur
     */
    public Annees( String valeur) {
        this.annee_valeur = valeur;
    }

    /**
     *
     * @return
     */
    public long getAnnee_id() {
        return annee_id;
    }

    /**
     *
     * @return
     */
    public String getAnnee_valeur() {
        return annee_valeur;
    }

    /**
     *
     * @param annee_id
     */
    public void setAnnee_id(long annee_id) {
        this.annee_id = annee_id;
    }

    /**
     *
     * @param annee_valeur
     */
    public void setAnnee_valeur(String annee_valeur) {
        this.annee_valeur = annee_valeur;
    }
}
