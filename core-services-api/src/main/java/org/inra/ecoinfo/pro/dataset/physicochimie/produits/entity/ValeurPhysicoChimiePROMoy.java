/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurPhysicoChimiePROMoy.JPA_NAME_TABLE, uniqueConstraints
        = @UniqueConstraint(columnNames = {
    MesurePhysicoChimiePROMoy.ID_JPA,
    RealNode.ID_JPA,
    ValeurPhysicoChimiePROMoy.ATTRIBUTE_JPA_ECARTTYPE}
        ),
        indexes = {
            @Index(name = "valeur_physico_chimie_promoy_mesure_idx", columnList = MesurePhysicoChimiePROMoy.ID_JPA),
            @Index(name = "valeur_physico_chimie_promoy_variable", columnList = RealNode.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = ValeurPhysicoChimie.ID_JPA, referencedColumnName = ValeurPhysicoChimiePROMoy.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurPhysicoChimie.ID_JPA, column = @Column(name = ValeurPhysicoChimiePROMoy.ID_JPA))})
public class ValeurPhysicoChimiePROMoy extends ValeurPhysicoChimie<MesurePhysicoChimiePROMoy, ValeurPhysicoChimiePROMoy> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurphysicochimiepro_moy";

    /**
     *
     */
    public static final String ID_JPA = "vpc_pro_moy_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ECARTTYPE = "ecarttype";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    @Column(name = ValeurPhysicoChimiePROMoy.ATTRIBUTE_JPA_ECARTTYPE, nullable = false)
    private float ecarttype;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesurePhysicoChimiePROMoy.class)
    @JoinColumn(name = MesurePhysicoChimiePROMoy.ID_JPA, referencedColumnName = MesurePhysicoChimiePROMoy.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesurePhysicoChimiePROMoy mesurePhysicoChimiePROMoy;

    /**
     *
     */
    public ValeurPhysicoChimiePROMoy() {
        super();
    }

    /**
     *
     * @param valeur
     * @param ecart
     * @param mesurePhysicoChimiePROMoy
     * @param realNode
     */
    public ValeurPhysicoChimiePROMoy(Float valeur, float ecart, MesurePhysicoChimiePROMoy mesurePhysicoChimiePROMoy, RealNode realNode) {
        super(valeur, realNode);
        this.mesurePhysicoChimiePROMoy = mesurePhysicoChimiePROMoy;
        this.ecarttype = ecart;
    }

    /**
     *
     * @return
     */
    public MesurePhysicoChimiePROMoy getMesurePhysicoChimiePROMoy() {
        return mesurePhysicoChimiePROMoy;
    }

    /**
     *
     * @param mesurePhysicoChimiePROMoy
     */
    public void setMesurePhysicoChimiePROMoy(MesurePhysicoChimiePROMoy mesurePhysicoChimiePROMoy) {
        this.mesurePhysicoChimiePROMoy = mesurePhysicoChimiePROMoy;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

}
