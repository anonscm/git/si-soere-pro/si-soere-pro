/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation;

/**
 *
 * @author vjkoyao
 */
public interface IIncubationDatatypeManager {

    /**
     *
     */
    String CODE_DATATYPE_INCUBATION_SOL = "incubations_sol_seul_donnees_elementaires";

    /**
     *
     */
    String CODE_DATATYPE_INCUBATION_SOL_PRO = "incubations_sol_pro_donnees_elemenataires";

    /**
     *
     */
    String CODE_DATATYPE_INCUBATION_SOL_MOY = "incubations_sol_seul_donnees_moyennees";

    /**
     *
     */
    String CODE_DATATYPE_INCUBATION_SOL_PRO_MOY = "incubations_sol_pro_donnees_moyennees";
}
