package org.inra.ecoinfo.pro.refdata.pente;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Pente.NAME_ENTITY_JPA )
public class Pente implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "pente";

    /**
     *
     */
    public static final String ID_JPA = "pente_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Pente.ID_JPA)
    private long pente_id = 0;
    @Column(name=Pente.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name= Pente.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String nom = "";
    
    /**
     *
     */
    public Pente(){
        super();
    }

    /**
     *
     * @param code
     * @param nom
     */
    public Pente (String code,String nom){
        this.code = code;
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getPente_id() {
        return pente_id;
    }

    /**
     *
     * @param pente_id
     */
    public void setPente_id(long pente_id) {
        this.pente_id = pente_id;
    }
    
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    
}
