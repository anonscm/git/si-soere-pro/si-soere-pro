/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = SousSequenceSMP.NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    SequenceSMP.ATTRIBUTE_JPA_ID,
    SousSequenceSMP.ATTRIBUTE_JPA_PROFONDEUR}))

public class SousSequenceSMP implements Serializable {

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PROFONDEUR = "profondeur";

    static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_TABLE = "sous_sequence_smp";

    @Id
    @Column(name = SousSequenceSMP.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = SousSequenceSMP.ATTRIBUTE_JPA_PROFONDEUR, nullable = false)
    Integer profondeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = SequenceSMP.class)
    @JoinColumn(name = SequenceSMP.ATTRIBUTE_JPA_ID, referencedColumnName = SequenceSMP.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceSMP sequenceSMP;

    @OneToMany(mappedBy = MesureSMP.ATTRIBUTE_JPA_ID, cascade = ALL)
    private List<MesureSMP> mesures = new LinkedList<MesureSMP>();

    /**
     *
     */
    public SousSequenceSMP() {
        super();
    }

    /**
     *
     * @param profondeur
     * @param sequenceSMP
     */
    public SousSequenceSMP(Integer profondeur, SequenceSMP sequenceSMP) {
        this.profondeur = profondeur;
        this.sequenceSMP = sequenceSMP;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Integer getProfondeur() {
        return this.profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public SequenceSMP getSequenceSMP() {
        return sequenceSMP;
    }

    /**
     *
     * @param sequenceSMP
     */
    public void setSequenceSMP(SequenceSMP sequenceSMP) {
        this.sequenceSMP = sequenceSMP;
    }

    /**
     *
     * @return
     */
    public List<MesureSMP> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureSMP> mesures) {
        this.mesures = mesures;
    }

}
