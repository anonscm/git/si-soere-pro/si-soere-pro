/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

/**
 *
 * @author vjkoyao
 */


@Entity 
@Table(name = SousSequenceTS.NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceTS.ATTRIBUTE_JPA_ID, SousSequenceTS.ATTRIBUTE_JPA_PROFONDEUR }))

public class SousSequenceTS implements Serializable{
    
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID           = "id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PROFONDEUR   = "profondeur";

  
    static final long          serialVersionUID   = 1L;
    
    /**
     *
     */
    public static final String    NAME_TABLE         = "sous_sequence_ts";

    
    
    
         
    @Id
    @Column(name=SousSequenceTS.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long                       id;
    

    @Column(name = SousSequenceTS.ATTRIBUTE_JPA_PROFONDEUR, nullable = false)
    Integer                    profondeur;
    
    
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = SequenceTS.class)
    @JoinColumn(name = SequenceTS.ATTRIBUTE_JPA_ID, referencedColumnName = SequenceTS.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceTS             sequenceTS;
    
    
    @OneToMany(mappedBy = MesureTS.ATTRIBUTE_JPA_ID, cascade = ALL)
    private List<MesureTS> mesures = new LinkedList<MesureTS>();

    /**
     *
     */
    public SousSequenceTS() {
        super();
    }

    /**
     *
     * @param profondeur
     * @param sequenceTS
     */
    public SousSequenceTS(Integer profondeur, SequenceTS sequenceTS) {
        this.profondeur = profondeur;
        this.sequenceTS = sequenceTS;
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Integer getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public SequenceTS getSequenceTS() {
        return sequenceTS;
    }

    /**
     *
     * @param sequenceTS
     */
    public void setSequenceTS(SequenceTS sequenceTS) {
        this.sequenceTS = sequenceTS;
    }

    /**
     *
     * @return
     */
    public List<MesureTS> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureTS> mesures) {
        this.mesures = mesures;
    }
    
      
    
}
