/**
 *
 *        uniqueConstraints = @UniqueConstraint(columnNames = {
 *            RefDataConstantes.COLUMN_NOM_PRG, 
 *            RefDataConstantes.COLUMN_ACRONYME_PRG}
        ))
 */
package org.inra.ecoinfo.pro.refdata.programme;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Programme.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {
            RefDataConstantes.COLUMN_NOM_PRG, 
            RefDataConstantes.COLUMN_ACRONYME_PRG}
        ))
public class Programme implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PROGRAMME_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PROGRAMME_TABLE_NAME; 
    @Id
    @Column(name = Programme.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ACRONYME_PRG, length = 10)
    private String acronyme;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_PRG, length = 500)
    private String nom;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_DATEDEBUT_PRG, length = 4)
    private String dateDebut;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_DATEFIN_PRG, length = 4)
    private String dateFin;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMMENTNUMCONV_PRG, length = 50)
    private String numConvention;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMMENTFINANCT_ORIG_PRG, length = 500)
    private String commentaireFinanctOrig;

    /**
     *
     */
    public Programme() {
        super();
    }

    /**
     *
     * @param acronyme
     * @param nom
     * @param dateDebut
     * @param dateFin
     * @param numConvention
     * @param commentaireFinanctOrig
     */
    public Programme(String acronyme, String nom, String dateDebut, String dateFin, String numConvention, String commentaireFinanctOrig) {
        super();
        this.acronyme = acronyme;
        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.numConvention = numConvention;
        this.commentaireFinanctOrig = commentaireFinanctOrig;
    }

    /**
     *
     * @return
     */
    public String getAcronyme() {
        return acronyme;
    }

    /**
     *
     * @return
     */
    public String getCommentaireFinanctOrig() {
        return commentaireFinanctOrig;
    }

    /**
     *
     * @return
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     *
     * @return
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getNumConvention() {
        return numConvention;
    }

    /**
     *
     * @param acronyme
     */
    public void setAcronyme(String acronyme) {
        this.acronyme = acronyme;
    }

    /**
     *
     * @param commentaireFinanctOrig
     */
    public void setCommentaireFinanctOrig(String commentaireFinanctOrig) {
        this.commentaireFinanctOrig = commentaireFinanctOrig;
    }

    /**
     *
     * @param dateDebut
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     *
     * @param dateFin
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @param numConvention
     */
    public void setNumConvention(String numConvention) {
        this.numConvention = numConvention;
    }

}
