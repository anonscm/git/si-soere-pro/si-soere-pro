package org.inra.ecoinfo.pro.refdata.typepedelogique;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Typepedologique.NAME_ENTITY_JPA )
public class Typepedologique implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "typepedologique";

    /**
     *
     */
    public static final String ID_JPA = "tp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Typepedologique.ID_JPA, updatable = false)
    private long tp_id = 0;
    @Column(name=Typepedologique.JPA_COLUMN_KEY)
    private String code = "";
    @Column(name= Typepedologique.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String nom = "";
    
    /**
     *
     */
    public Typepedologique(){
        super();
    }

    /**
     *
     * @param code
     * @param nom
     */
    public Typepedologique(String code,String nom){
        this.code = code;
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getTp_id() {
        return tp_id;
    }

    /**
     *
     * @param tp_id
     */
    public void setTp_id(long tp_id) {
        this.tp_id = tp_id;
    }
    
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    

}
