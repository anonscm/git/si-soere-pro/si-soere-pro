/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;



/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name = ValeurSWC.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    MesureSWC.ATTRIBUTE_JPA_ID, RealNode.ID_JPA, ValeurSWC.ATTRIBUTE_JPA_NUM_REPETITION}))

public class ValeurSWC implements Serializable{
    
    /**
     *
     */
    public static final String ID_JPA = "vswc_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_swc";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String  ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NUM_REPETITION = "num_repetition";
    
   
    static final long serialVersionUID = 1L;


     
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long  vswc_id;
    
    @Column(name = ValeurSWC.COLUMN_JPA_VALUE, nullable = false)
    private Float valeur;
    
    @Column(name=ValeurSWC.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;
    
    @Column(name = ValeurSWC.ATTRIBUTE_JPA_NUM_REPETITION, nullable = false)
    int                        numRepetition;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureSWC.ATTRIBUTE_JPA_ID, referencedColumnName = MesureSWC.ATTRIBUTE_JPA_ID)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureSWC mesureSWC;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
     private RealNode realNode;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    /**
     *
     */
    public ValeurSWC() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param numRepetition
     * @param mesureSWC
     * @param realNode
     * @param ligneFichierEchange
     */
    public ValeurSWC(Float valeur, String statutvaleur, int numRepetition, MesureSWC mesureSWC, RealNode realNode, Long ligneFichierEchange) {
        this.valeur = valeur;
        this.statutvaleur = statutvaleur;
        this.numRepetition = numRepetition;
        this.mesureSWC = mesureSWC;
        this.realNode = realNode;
        this.ligneFichierEchange = ligneFichierEchange;
    }
    
    /**
     *
     * @return
     */
    public Long getVswc_id() {
        return vswc_id;
    }

    /**
     *
     * @param vswc_id
     */
    public void setVswc_id(Long vswc_id) {
        this.vswc_id = vswc_id;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public int getNumRepetition() {
        return numRepetition;
    }

    /**
     *
     * @param numRepetition
     */
    public void setNumRepetition(int numRepetition) {
        this.numRepetition = numRepetition;
    }

    /**
     *
     * @return
     */
    public MesureSWC getMesureSWC() {
        return mesureSWC;
    }

    /**
     *
     * @param mesureSWC
     */
    public void setMesureSWC(MesureSWC mesureSWC) {
        this.mesureSWC = mesureSWC;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }
}
