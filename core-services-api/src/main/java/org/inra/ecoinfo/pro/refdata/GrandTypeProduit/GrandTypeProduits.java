/*
 *
 */
package org.inra.ecoinfo.pro.refdata.GrandTypeProduit;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.origine.Origines;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = GrandTypeProduits.NAME_ENTITY_JPA)
public class GrandTypeProduits implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "grandtypeproduit";

    /**
     *
     */
    public static final String JPA_COLUMN_ID = "gtp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "gtp_bycode";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "gtp_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "gtp_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "gtp_comment";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = GrandTypeProduits.JPA_COLUMN_ID, nullable = false)
    private long gtp_id = 0;
    @Column(name = GrandTypeProduits.JPA_COLUMN_CODE, unique = true, nullable = false)
    private String gtp_bycode;
    @Column(name = GrandTypeProduits.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String gtp_code = "";
    @Column(name = GrandTypeProduits.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String gtp_nom = "";
    @Column(name = GrandTypeProduits.JPA_COLUMN_COMENT, length = 300)
    private String gtp_comment = "";
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Origines.JPA_COLUMN_ID, nullable = false, unique = false)
    private Origines origines;

    /**
     *
     */
    public GrandTypeProduits() {
        super();
    }

    /**
     *
     * @param bycode
     * @param code
     * @param nom
     * @param comment
     * @param origines
     */
    public GrandTypeProduits(String bycode, String code, String nom, String comment, Origines origines) {
        this.gtp_bycode = bycode;
        this.gtp_code = code;
        this.gtp_nom = nom;
        this.gtp_comment = comment;
        this.origines = origines;
    }

    /**
     *
     * @return
     */
    public String getGtp_bycode() {
        return gtp_bycode;
    }

    /**
     *
     * @return
     */
    public String getGtp_code() {
        return gtp_code;
    }

    /**
     *
     * @return
     */
    public String getGtp_comment() {
        return gtp_comment;
    }

    /**
     *
     * @return
     */
    public long getGtp_id() {
        return gtp_id;
    }

    /**
     *
     * @return
     */
    public String getGtp_nom() {
        return gtp_nom;
    }
    
    /**
     *
     * @return
     */
    public Origines getOrigines() {
        return origines;
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return false;
    }

    /**
     *
     * @param gtp_bycode
     */
    public void setGtp_bycode(String gtp_bycode) {
        this.gtp_bycode = gtp_bycode;
    }

    /**
     *
     * @param gtp_code
     */
    public void setGtp_code(String gtp_code) {
        this.gtp_code = gtp_code;
    }

    /**
     *
     * @param gtp_comment
     */
    public void setGtp_comment(String gtp_comment) {
        this.gtp_comment = gtp_comment;
    }

    /**
     *
     * @param gtp_id
     */
    public void setGtp_id(long gtp_id) {
        this.gtp_id = gtp_id;
    }

    /**
     *
     * @param gtp_nom
     */
    public void setGtp_nom(String gtp_nom) {
        this.gtp_nom = gtp_nom;
    }

    /**
     *
     * @param origines
     */
    public void setOrigines(Origines origines) {
        this.origines = origines;
    }
    @Override
    public String toString() {
        return " nom: " + getGtp_nom();
    }
}
