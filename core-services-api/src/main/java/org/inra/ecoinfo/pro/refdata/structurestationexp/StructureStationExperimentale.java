/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structurestationexp;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.StationExperimentale;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.STRUCTURE_ID, RefDataConstantes.STATIONEXP_ID
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StructureStationExperimentale.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.STRUCTURE_ID,
    RefDataConstantes.STATIONEXP_ID}))
public class StructureStationExperimentale implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STRUCTURESTATIONEXP_ID; // strstx_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STRUCTURESTATIONEXP_TABLE_NAME; // structure_station_experimentale
    @Id
    @Column(name = StructureStationExperimentale.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Structure.class)
    @JoinColumn(name = RefDataConstantes.STRUCTURE_ID, referencedColumnName = Structure.ID_JPA, nullable = false)
    private Structure structure;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = StationExperimentale.class)
    @JoinColumn(name = RefDataConstantes.STATIONEXP_ID, referencedColumnName = StationExperimentale.ID_JPA, nullable = false)
    private StationExperimentale stationExperimentale;

    /**
     *
     */
    public StructureStationExperimentale() {
        super();
    }

    /**
     *
     * @param structure
     * @param stationExperimentale
     */
    public StructureStationExperimentale(Structure structure, StationExperimentale stationExperimentale) {
        super();
        this.structure = structure;
        this.stationExperimentale = stationExperimentale;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public StationExperimentale getStationExperimentale() {
        return stationExperimentale;
    }

    /**
     *
     * @return
     */
    public Structure getStructure() {
        return structure;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param stationExperimentale
     */
    public void setStationExperimentale(StationExperimentale stationExperimentale) {
        this.stationExperimentale = stationExperimentale;
    }

    /**
     *
     * @param structure
     */
    public void setStructure(Structure structure) {
        this.structure = structure;
    }
}
