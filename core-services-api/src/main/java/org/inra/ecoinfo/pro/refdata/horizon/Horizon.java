/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizon;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Horizon.JPA_NAME_ENTITY)
public class Horizon implements Serializable{

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "horizon";

    /**
     *
     */
    public static final String ID_JPA = "horizon_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_HORIZON = "horizon_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME_HORIZON = "horizon_nom";
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = Horizon.ID_JPA)
     private long horizon_id;
     @Column(name = Horizon.JPA_COLUMN_CODE_HORIZON, nullable = false, updatable = false, unique = true)
     private String horizon_code ="";
     @Column(name = Horizon.JPA_COLUMN_NAME_HORIZON, nullable = false, updatable = false, unique = true)
     private  String horizon_nom="";
     
    /**
     *
     */
    public Horizon(){
         super();
     }
     
    /**
     *
     * @param nom
     */
    public Horizon (String nom){
         this.horizon_code = Utils.createCodeFromString(nom);
         this.horizon_nom = nom;
     }

    /**
     *
     * @return
     */
    public long getHorizon_id() {
        return horizon_id;
    }

    /**
     *
     * @param horizon_id
     */
    public void setHorizon_id(long horizon_id) {
        this.horizon_id = horizon_id;
    }

    /**
     *
     * @return
     */
    public String getHorizon_code() {
        return horizon_code;
    }

    /**
     *
     * @return
     */
    public String getHorizon_nom() {
        return horizon_nom;
    }

    /**
     *
     * @param horizon_code
     */
    public void setHorizon_code(String horizon_code) {
        this.horizon_code = horizon_code;
    }

    /**
     *
     * @param horizon_nom
     */
    public void setHorizon_nom(String horizon_nom) {
        this.horizon_nom = horizon_nom;
    }
     
     
}
