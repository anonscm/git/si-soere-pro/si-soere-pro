/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typelieu;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeLieu.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_TPLIEU}))
public class TypeLieu implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.TYPELIEU_ID; // tpl_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.TYPELIEU_TABLE_NAME; // type_lieu
    
    @Id
    @Column(name = TypeLieu.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_TPLIEU, length = 100)
    private String libelle;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_TPLIEU, length = 100)
    private String code;
    
    /**
     *
     */
    public TypeLieu() 
    {
        super();
    }
    
    /**
     *
     * @param libelle
     */
    public TypeLieu(String libelle) 
    {
        super();
        this.libelle = libelle;
    }
    
    /**
	 * @param libelle
	 * @param code
	 */
	public TypeLieu(String libelle, String code) {
		super();
		this.libelle = libelle;
		this.code = code;
	}

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
