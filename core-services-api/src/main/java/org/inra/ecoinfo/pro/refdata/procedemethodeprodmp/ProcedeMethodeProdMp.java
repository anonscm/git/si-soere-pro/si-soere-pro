package org.inra.ecoinfo.pro.refdata.procedemethodeprodmp;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.process.Process;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 *   Melange.JPA_ID,
 *   Process.JPA_COLUM_ID,
 *   ProcedeMethodeProdMp.JPA_COLUMN_ORDREPROCEDE
 * @author ptcherniati
 */
@Entity
@Table(name = ProcedeMethodeProdMp.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    Melange.JPA_ID,
    Process.JPA_COLUM_ID,
    ProcedeMethodeProdMp.JPA_COLUMN_ORDREPROCEDE

}))
public class ProcedeMethodeProdMp implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "matierepprocede";

    /**
     *
     */
    public static final String ID_JPA = "p_id";

    /**
     *
     */
    public static final String JPA_COLUMN_PRODUIT = "produit";

    /**
     *
     */
    public static final String JPA_COLUMN_COMPOSANT = "composant";

    /**
     *
     */
    public static final String JPA_COLUMN_PROCEDE = "procede";

    /**
     *
     */
    public static final String JPA_COLUMN_ORDREPROCEDE = "odreprocede";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_DURATION = "duree";

    /**
     *
     */
    public static final String JPA_COLUMN_UNITE = "unite";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ProcedeMethodeProdMp.ID_JPA, updatable = false)
    private long p_id = 0;
    @Column(name = ProcedeMethodeProdMp.JPA_COLUMN_ORDREPROCEDE, nullable = false)
    private int odreprocede = 0;
    @Column(name = ProcedeMethodeProdMp.JPA_COLUMN_COMMENT, nullable = true)
    private String commentaire = null;
    @Column(name = ProcedeMethodeProdMp.JPA_COLUMN_DURATION, nullable = false)
    private String duree = "";
    @Column(name = ProcedeMethodeProdMp.JPA_COLUMN_UNITE, nullable = false)
    private String unite = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Melange.JPA_ID, nullable = false)
    private Melange melange;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Process.JPA_COLUM_ID, nullable = false)
    private Process process;

    /**
     *
     */
    public ProcedeMethodeProdMp() {
        super();
    }

    /**
     *
     * @param melange
     * @param process
     * @param ordreprocede
     * @param commentaire
     * @param duree
     * @param unite
     */
    public ProcedeMethodeProdMp(Melange melange, Process process, int ordreprocede, String commentaire, String duree, String unite) {
        this.odreprocede = ordreprocede;
        this.commentaire = commentaire;
        this.duree = duree;
        this.unite = unite;
        this.process = process;
        this.melange = melange;
    }

    /**
     *
     * @return
     */
    public Melange getMelange() {
        return melange;
    }

    /**
     *
     * @return
     */
    public long getMppm_id() {
        return p_id;
    }

    /**
     *
     * @return
     */
    public int getOdreprocede() {
        return odreprocede;
    }

    /**
     *
     * @return
     */
    public long getP_id() {
        return p_id;
    }

    /**
     *
     * @return
     */
    public Process getProcess() {
        return process;
    }

    /**
     *
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getDuree() {
        return duree;
    }

    /**
     *
     * @param duree
     */
    public void setDuree(String duree) {
        this.duree = duree;
    }

    /**
     *
     * @param melange
     */
    public void setMelange(Melange melange) {
        this.melange = melange;
    }

    /**
     *
     * @param mppm_id
     */
    public void setMppm_id(long mppm_id) {
        this.p_id = mppm_id;
    }

    /**
     *
     * @param odreprocede
     */
    public void setOdreprocede(int odreprocede) {
        this.odreprocede = odreprocede;
    }

    /**
     *
     * @param p_id
     */
    public void setP_id(long p_id) {
        this.p_id = p_id;
    }

    /**
     *
     * @param process
     */
    public void setProcess(Process process) {
        this.process = process;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }
}
