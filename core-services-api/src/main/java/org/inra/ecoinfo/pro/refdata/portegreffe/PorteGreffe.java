/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.portegreffe;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=PorteGreffe.NAME_ENTITY_JPA)
public class PorteGreffe implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "portegreffe";

    /**
     *
     */
    public static final String JPA_ID = "pg_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "pg_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "pg_libelle";
    
    @Id
    @Column(name = PorteGreffe.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pg_id;
    @Column(name = PorteGreffe.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String pg_code = "";
    @Column(name = PorteGreffe.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String pg_libelle = "";
    
    /**
     *
     */
    public PorteGreffe(){
        super();
    }
    
    /**
     *
     * @param libelle
     */
    public PorteGreffe(String libelle){
        this.pg_libelle = libelle;
    }

    /**
     *
     * @return
     */
    public String getPg_code() {
        return pg_code;
    }

    /**
     *
     * @return
     */
    public String getPg_libelle() {
        return pg_libelle;
    }

    /**
     *
     * @return
     */
    public long getPg_id() {
        return pg_id;
    }

    /**
     *
     * @param pg_id
     */
    public void setPg_id(long pg_id) {
        this.pg_id = pg_id;
    }

    /**
     *
     * @param pg_code
     */
    public void setPg_code(String pg_code) {
        this.pg_code = pg_code;
    }

    /**
     *
     * @param pg_libelle
     */
    public void setPg_libelle(String pg_libelle) {
        this.pg_libelle = pg_libelle;
    }
    
    
}
