package org.inra.ecoinfo.pro.refdata.listespro;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.NaturalId;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;

/**
 * The Class ListePro.
 */
@Entity
@Table(name = ValeurQualitative.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {ValeurQualitative.ATTRIBUTE_JPA_VALUE, ValeurQualitative.ATTRIBUTE_JPA_CODE}))
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorFormula(value = "code")
@DiscriminatorValue("null")
public class ListePro implements IValeurQualitative, Serializable, Comparable<ListePro> {

    /**
     * The Constant ATTRIBUTE_JPA_CODE.
     */
    public static final String ATTRIBUTE_JPA_CODE = ValeurQualitative.ATTRIBUTE_JPA_CODE;

    /**
     * The Constant ATTRIBUTE_JPA_VALEUR.
     */
    public static final String ATTRIBUTE_JPA_VALEUR = ValeurQualitative.ATTRIBUTE_JPA_VALUE;

    /**
     * The Constant NAME_ENTITY_JPA.
     */
    public static final String NAME_ENTITY_JPA = ValeurQualitative.NAME_ENTITY_JPA;

    /**
     * The Constant ID_JPA.
     */
    public static final String ID_JPA = ValeurQualitative.ID_JPA;

    /**
     * The Constant SEPARATOR_CASES.
     */
    protected static final String SEPARATOR_CASES = "', '";

    /**
     * The Constant serialVersionUID <long>.
     */
    /**
     * Default serial version UID.
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    protected String typeListe = "null";
    // ATTRIBUTS
    /**
     * The valeur @link(String).
     */
    @Column(nullable = false, name = IValeurQualitative.ATTRIBUTE_JPA_VALUE)
    @NaturalId
    private String valeur;
    /**
     * The code @link(String).
     */
    @Column(nullable = false, name = IValeurQualitative.ATTRIBUTE_JPA_CODE)
    @NaturalId
    private String code;
    /**
     * The id @link(Long).
     */
    /**
     * Instantiates a new liste itineraire.
     */
    /**
     * The id @link(Long).
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = IValeurQualitative.ID_JPA)
    protected Long id;

    /**
     *
     */
    public ListePro() {
        super();
    }

    /**
     * Instantiates a new valeur qualitative.
     *
     * @param code
     * @param value the value
     * @link(String) the value
     */
    public ListePro(final String code, final String value) {
        super();
        this.code = code;
        this.valeur = value;
    }

    /**
     * Gets the code.
     *
     * @return the code
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getCode()
     */
    @Override
    public String getCode() {
        return code;
    }

    /**
     * Gets the id.
     *
     * @return the id
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Gets the valeur.
     *
     * @return the valeur
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#getValeur()
     */
    @Override
    public String getValeur() {
        return valeur;
    }

    /**
     * Sets the valeur.
     *
     * @param valeur the new valeur
     * @see
     * org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitative#setValeur(java.lang.String)
     */
    @Override
    public void setValeur(final String valeur) {
        this.valeur = valeur;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.valeur);
        hash = 67 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public int compareTo(ListePro o) {
        if (o == null) {
            return -1;
        }
        if (this.code != null && this.valeur != null && this.code.compareTo(o.code) == 0) {
            return this.valeur.compareTo(o.valeur);
        } else if (this.code != null) {
            return this.code.compareTo(o.code);
        } else {
            return +1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ListePro other = (ListePro) obj;
        if (!Objects.equals(this.valeur, other.valeur)) {
            return false;
        }
        return Objects.equals(this.code, other.code);
    }

    /**
     *
     * @param typeListe
     */
    public void setTypeListe(String typeListe) {
        this.typeListe = typeListe;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
}
