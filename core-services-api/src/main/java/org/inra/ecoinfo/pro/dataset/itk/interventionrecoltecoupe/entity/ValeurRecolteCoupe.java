/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurRecolteCoupe.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    RealNode.ID_JPA,
    MesureRecolteCoupe.ATTRIBUTE_JPA_ID}))
@PrimaryKeyJoinColumn(name = ValeurITK.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurRecolteCoupe.ATTRIBUTE_JPA_ID)
@AttributeOverrides({
    @AttributeOverride(name = ValeurITK.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurRecolteCoupe.ATTRIBUTE_JPA_ID))})

public class ValeurRecolteCoupe extends ValeurITK<MesureRecolteCoupe, ValeurRecolteCoupe> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurinterventionrecolte";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "valeurid";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureRecolteCoupe.class)
    @JoinColumn(name = MesureRecolteCoupe.ATTRIBUTE_JPA_ID, referencedColumnName = MesureRecolteCoupe.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureRecolteCoupe mesureinterventionrecolte;

    /**
     *
     */
    public ValeurRecolteCoupe() {
        super();
    }

    /**
     *
     * @param valeur
     * @param mesureinterventionrecolte
     * @param realNode
     */
    public ValeurRecolteCoupe(Float valeur, MesureRecolteCoupe mesureinterventionrecolte, RealNode realNode) {
        super(valeur, realNode);
        this.mesureinterventionrecolte = mesureinterventionrecolte;
    }

    /**
     *
     * @param mesureinterventionrecolte
     * @param listeItineraires
     * @param realNode
     */
    public ValeurRecolteCoupe(MesureRecolteCoupe mesureinterventionrecolte, ListeItineraire listeItineraires, RealNode realNode) {
        super(listeItineraires, realNode);
        this.mesureinterventionrecolte = mesureinterventionrecolte;
    }

    /**
     *
     * @return
     */
    public MesureRecolteCoupe getMesureinterventionrecolte() {
        return mesureinterventionrecolte;
    }

    /**
     *
     * @param mesureinterventionrecolte
     */
    public void setMesureinterventionrecolte(MesureRecolteCoupe mesureinterventionrecolte) {
        this.mesureinterventionrecolte = mesureinterventionrecolte;
    }
}
