
package org.inra.ecoinfo.pro.refdata.heterogeneitedispositif;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.heterogeinite.Heterogeinite;


/**
 *
 * @author adiankha
 */

@Entity
@Table(name =HeterogeineteDispositif.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
        Heterogeinite.ID_JPA, 
        Dispositif.ID_JPA
      
        

}))
public class HeterogeineteDispositif implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "heterogeneitedispositif";

    /**
     *
     */
    public static final String ID_JPA = "hd_id";

    /**
     *
     */
    public static final String COLUMN_COMMENT_JPA = "hd_comment";
     
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Column(name = HeterogeineteDispositif.ID_JPA, updatable = false)
     private long hd_id = 0;
     @Column(name= HeterogeineteDispositif.COLUMN_COMMENT_JPA)
     private String hd_comment = "";
     
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
     @JoinColumn(name = Heterogeinite.ID_JPA)
     private Heterogeinite heterogeinite ;
     
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
     @JoinColumn(name=Dispositif.ID_JPA,nullable = false,unique=false)
     private Dispositif dispositif;
     
    /**
     *
     */
    public HeterogeineteDispositif(){
         super();
     }
     
    /**
     *
     * @param dispositif
     * @param heterogeinite
     * @param comment
     */
    public HeterogeineteDispositif(Dispositif dispositif,Heterogeinite heterogeinite,String comment){
         this.dispositif = dispositif;
         this.heterogeinite =heterogeinite;
         this.hd_comment = comment;
     }

    /**
     *
     * @return
     */
    public long getHd_id() {
        return hd_id;
    }

    /**
     *
     * @return
     */
    public String getHd_comment() {
        return hd_comment;
    }

    /**
     *
     * @param hd_id
     */
    public void setHd_id(long hd_id) {
        this.hd_id = hd_id;
    }

    /**
     *
     * @param hd_comment
     */
    public void setHd_comment(String hd_comment) {
        this.hd_comment = hd_comment;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @return
     */
    public Heterogeinite getHeterogeinite() {
        return heterogeinite;
    }

    /**
     *
     * @param heterogeinite
     */
    public void setHeterogeinite(Heterogeinite heterogeinite) {
        this.heterogeinite = heterogeinite;
    }
       
}
