/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adiankha
 */
public class ExtractionParametersVO implements Serializable, Cloneable{
    
     private String name;
    private String localizedName;
    private String unit;
    private Boolean valueMin = false;
    private Boolean valueMax = false;
    private List<String> targetValues = new ArrayList<>();
    private List<String> uncertainties = new ArrayList<>();

    /**
     *
     */
    public ExtractionParametersVO() {
        super();
    }

    /**
     *
     * @param name
     * @param unit
     */
    public ExtractionParametersVO(String name, String unit) {
        super();
        this.name = name;
        this.setUnit(unit);
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public Boolean getValueMin() {
        return valueMin;
    }

    /**
     *
     * @param valueMin
     */
    public void setValueMin(Boolean valueMin) {
        this.valueMin = valueMin;
    }

    /**
     *
     * @return
     */
    public Boolean getValueMax() {
        return valueMax;
    }

    /**
     *
     * @param valueMax
     */
    public void setValueMax(Boolean valueMax) {
        this.valueMax = valueMax;
    }

    /**
     *
     * @return
     */
    public List<String> getTargetValues() {
        return targetValues;
    }

    /**
     *
     * @param targetValues
     */
    public void setTargetValues(List<String> targetValues) {
        this.targetValues = targetValues;
    }

    /**
     *
     * @param targetValue
     */
    public void addTargetValue(String targetValue) {
        this.targetValues.add(targetValue);
    }

    /**
     *
     * @return
     */
    public List<String> getUncertainties() {
        return uncertainties;
    }

    /**
     *
     * @param uncertainties
     */
    public void setUncertainties(List<String> uncertainties) {
        this.uncertainties = uncertainties;
    }

    /**
     *
     * @param uncertainty
     */
    public void addUncertainty(String uncertainty) {
        this.uncertainties.add(uncertainty);
    }

    /**
     *
     * @param unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     *
     * @return
     */
    public String getUnit() {
        return unit;
    }

    /**
     *
     * @return
     */
    public String getLocalizedName() {
        return localizedName;
    }

    /**
     *
     * @param localizedName
     */
    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public ExtractionParametersVO clone() {
        ExtractionParametersVO extractVO = new ExtractionParametersVO();
        extractVO.setLocalizedName(this.localizedName);
        extractVO.setName(this.name);
        extractVO.setUnit(this.unit);
        extractVO.setValueMin(this.valueMin);
        extractVO.setValueMax(this.valueMax);
        List<String> targetVO = new ArrayList<String>();
        targetValues.forEach((target) -> {
            targetVO.add(target);
        });
        extractVO.setTargetValues(targetVO);
        List<String> uncertaintiesVO = new ArrayList<String>();
        uncertainties.forEach((uncertainty) -> {
            uncertaintiesVO.add(uncertainty);
        });
        extractVO.setUncertainties(uncertaintiesVO);
        return extractVO;
    }

    
}