package org.inra.ecoinfo.pro.refdata.typesolarvalis;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name =Typesolarvalis.NAME_ENTITY_JPA )
public class Typesolarvalis implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "typesolarvalis";

    /**
     *
     */
    public static final String ID_JPA = "ta_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Typesolarvalis.ID_JPA, updatable = false)
    private long ta_id = 0;
    @Column(name=Typesolarvalis.JPA_COLUMN_KEY,unique = true , nullable = false)
    private String code = "";
    @Column(name= Typesolarvalis.JPA_COLUMN_NAME,unique = true,nullable=false)
    private String nom = "";
    
    /**
     *
     */
    public Typesolarvalis(){
        super();
    }

    /**
     *
     * @param code
     * @param nom
     */
    public Typesolarvalis (String code,String nom){
        this.code = code;
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getTa_id() {
        return ta_id;
    }

    /**
     *
     * @param ta_id
     */
    public void setTa_id(long ta_id) {
        this.ta_id = ta_id;
    }
    
    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }
    
    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }    
}
