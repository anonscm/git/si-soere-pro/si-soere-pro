/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesurePhysicoChimiePRO.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    EchantillonsProduit.ID_JPA,
    MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE,
    MesurePhysicoChimiePRO.COLUMN_NOM_LABO,
    MesurePhysicoChimiePRO.COLUMN_REP_ANALYSE}),
        indexes = {
            @Index(name = "mesure_physico_chimie_pro_echan_idx", columnList = EchantillonsProduit.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, referencedColumnName = MesurePhysicoChimiePRO.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, column = @Column(name = MesurePhysicoChimiePRO.ID_JPA))})
public class MesurePhysicoChimiePRO extends MesurePhysicoChimie<MesurePhysicoChimiePRO, ValeurPhysicoChimiePRO> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesurephysicochimiepro";

    /**
     *
     */
    public static final String ID_JPA = "mpc_pro_id";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate = "LocalDate_prelevement";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "nom_laboratoire";

    /**
     *
     */
    public static final String COLUMN_LAB_NUM_ECH = "numero_laboratoire";

    /**
     *
     */
    public static final String COLUMN_REP_ANALYSE = "numero_repetition";

    /**
     *
     */
    public static final String COLUMN_KEY_MESURE = "keymesure";

    static final String RESOURCE_PATH = "%s/%s";

    @Column(name = MesurePhysicoChimiePRO.COLUMN_NOM_LABO, nullable = false)
    private String nom_laboratoire = "";

    @Column(name = MesurePhysicoChimiePRO.COLUMN_REP_ANALYSE, nullable = false)
    private int numero_repetition = 0;

    @Column(name = MesurePhysicoChimiePRO.COLUMN_LAB_NUM_ECH, nullable = false)
    private int numero_laboratoire = 0;

    @Column(name = MesurePhysicoChimiePRO.COLUMN_KEY_MESURE, nullable = false, unique = true)
    private String keymesure = null;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = EchantillonsProduit.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private EchantillonsProduit echantillonpro;

    @OneToMany(mappedBy = "mesurePhysicoChimiePRO", cascade = ALL)
    List<ValeurPhysicoChimiePRO> valeurPhysicoChimiePRO = new LinkedList();

    /**
     *
     */
    public MesurePhysicoChimiePRO() {
        super();
    }

    /**
     *
     * @param LocalDate_prelevement
     * @param versionfile
     * @param echantillonpro
     * @param repetition
     * @param numlabo
     * @param nomlabo
     * @param ligneFichierEchange
     */
    public MesurePhysicoChimiePRO(LocalDate LocalDate_prelevement, VersionFile versionfile, EchantillonsProduit echantillonpro,
            int repetition, int numlabo, String nomlabo,
            Long ligneFichierEchange) {
        super(LocalDate_prelevement, ligneFichierEchange, versionfile);
        this.echantillonpro = echantillonpro;
        this.nom_laboratoire = nomlabo;
        this.numero_repetition = repetition;
        this.numero_laboratoire = numlabo;
    }

    /**
     *
     * @return
     */
    public String getNom_laboratoire() {
        return nom_laboratoire;
    }

    /**
     *
     * @param nom_laboratoire
     */
    public void setNom_laboratoire(String nom_laboratoire) {
        this.nom_laboratoire = nom_laboratoire;
    }

    /**
     *
     * @return
     */
    public int getNumero_repetition() {
        return numero_repetition;
    }

    /**
     *
     * @param numero_repetition
     */
    public void setNumero_repetition(int numero_repetition) {
        this.numero_repetition = numero_repetition;
    }

    /**
     *
     * @return
     */
    public int getNumero_laboratoire() {
        return numero_laboratoire;
    }

    /**
     *
     * @param numero_laboratoire
     */
    public void setNumero_laboratoire(int numero_laboratoire) {
        this.numero_laboratoire = numero_laboratoire;
    }

    /**
     *
     * @return
     */
    public EchantillonsProduit getEchantillonpro() {
        return echantillonpro;
    }

    /**
     *
     * @param echantillonpro
     */
    public void setEchantillonpro(EchantillonsProduit echantillonpro) {
        this.echantillonpro = echantillonpro;
    }

    /**
     *
     * @return
     */
    public List<ValeurPhysicoChimiePRO> getValeurPhysicoChimiePRO() {
        return valeurPhysicoChimiePRO;
    }

    /**
     *
     * @param valeurPhysicoChimiePRO
     */
    public void setValeurPhysicoChimiePRO(List<ValeurPhysicoChimiePRO> valeurPhysicoChimiePRO) {
        this.valeurPhysicoChimiePRO = valeurPhysicoChimiePRO;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

}
