package org.inra.ecoinfo.pro.refdata.commune;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.departement.Departement;

/**
 *@UniqueConstraint(columnNames = {
 *   RefDataConstantes.COLUMN_CODEPOSTAL_COMMUNE, 
 *   RefDataConstantes.COLUMN_NOM_COMMUNE}))
 * @author ptcherniati
 */
@Entity
@Table(name = Commune.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_CODEPOSTAL_COMMUNE, 
    RefDataConstantes.COLUMN_NOM_COMMUNE}))
public class Commune implements Serializable 
{
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.COMMUNE_ID; // cm_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.COMMUNE_TABLE_NAME; // commune

    /**
     *
     */
    public static final Pattern REGEXP_PATTERN_NOM_COMMUNE_CODE_POSTAL = Pattern.compile("\\s*(.*?)\\s*\\(\\s*(.*?)\\s*\\)\\s*");

    /**
     *
     */
    public static final String PATTERN_NOM_COMMUNE_CODE_POSTAL = "%s (%s)";

    /**
     *
     * @param nomCommune_codePostal
     * @return
     */
    public static String getNomCommune(String nomCommune_codePostal){
        Matcher matcher = REGEXP_PATTERN_NOM_COMMUNE_CODE_POSTAL.matcher(nomCommune_codePostal);
        if(matcher.matches()){
            return matcher.group(1);
        }
        return nomCommune_codePostal.trim();
    }
    
    /**
     *
     * @param nomCommune_codePostal
     * @return
     */
    public static String getCodePostal(String nomCommune_codePostal){
        Matcher matcher = REGEXP_PATTERN_NOM_COMMUNE_CODE_POSTAL.matcher(nomCommune_codePostal);
        if(matcher.matches()){
            return matcher.group(2);
        }
        return null;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = Commune.ID_JPA)
    private Long id;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_CODEPOSTAL_COMMUNE, length = 15)
    private String codePostal;
    
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_COMMUNE, length = 100)
    private String nomCommune;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Departement.class)
    @JoinColumn(name = RefDataConstantes.DEPARTEMENT_ID, referencedColumnName = Departement.ID_JPA, nullable = false)
    private Departement departement;
    
    /**
     *
     */
    public Commune() 
    {
        super();
    }
    
    /**
     *
     * @param codePostal
     * @param nomCommune
     */
    public Commune(String codePostal, String nomCommune) 
    {
        super();
        this.codePostal = codePostal;
        this.nomCommune = nomCommune;
    }
    
    /**
	 * @param codePostal
	 * @param nomCommune
	 * @param departement
	 */
	public Commune(String codePostal, String nomCommune, Departement departement) {
		super();
		this.codePostal = codePostal;
		this.nomCommune = nomCommune;
		this.departement = departement;
	}

    /**
     *
     * @return
     */
    public String getCodePostal() {
        return codePostal;
    }
    
    /**
     *
     * @return
     */
    public Departement getDepartement() {
        return departement;
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getNomCommune() {
        return nomCommune;
    }
    
    /**
     *
     * @param codePostal
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }
    
    /**
     *
     * @param departement
     */
    public void setDepartement(Departement departement) {
        this.departement = departement;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param nomCommune
     */
    public void setNomCommune(String nomCommune) {
        this.nomCommune = nomCommune;
    }

    /**
     *
     * @return
     */
    public String getNomCommune_codePostal(){
        return String.format(PATTERN_NOM_COMMUNE_CODE_POSTAL, nomCommune, codePostal);
    }
}
