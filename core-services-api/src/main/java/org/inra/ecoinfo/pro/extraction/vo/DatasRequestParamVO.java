/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.vo;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author adiankha
 */
public class DatasRequestParamVO implements Serializable, Cloneable{
    
    private static final String PATTERN_STRING_EXTRACTION_TYPE = "   %s";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.glacpe.ui.messages";

    private static final String MSG_MAX_VALUE_EXTRACTION_TYPE = "PROPERTY_MSG_MAX_VALUE_EXTRACTION_TYPE";
    private static final String MSG_MAX_VALUE_EXTRACTION_TYPE_BY_VARIABLE = "PROPERTY_MSG_MAX_VALUE_EXTRACTION_TYPE_BY_VARIABLE";
    private static final String MSG_MIN_VALUE_EXTRACTION_TYPE = "PROPERTY_MSG_MIN_VALUE_EXTRACTION_TYPE";
    private static final String MSG_MIN_VALUE_EXTRACTION_TYPE_BY_VARIABLE = "PROPERTY_MSG_MIN_VALUE_EXTRACTION_TYPE_BY_VARIABLE";
    private static final String MSG_DATA_BALANDED_BY_DEPTH_EXTRACTION_TYPE = "PROPERTY_MSG_DATA_BALANDED_BY_DEPTH_EXTRACTION_TYPE";
    private static final String MSG_RAW_EXTRACTION_TYPE = "PROPERTY_MSG_RAW_EXTRACTION_TYPE";
    private static final String MSG_EXTRACTIONS_TYPES = "PROPERTY_MSG_EXTRACTIONS_TYPES";
    private static final String MSG_TARGET_VALUE_TYPE = "PROPERTY_MSG_TARGET_VALUE_TYPE";
    private static final String CST_UL = "<ul>";
    private static final String CST_UL_END = "</ul>";
    private static final String CST_LI = "<li>";
    private static final String CST_LI_END = "</li>";
    private static final String CST_DOUBLE_DOT = " : ";
    private static final String CST_TAB = "&nbsp;&nbsp;&nbsp;";
    private static final String MSG_MIN = "min";
    private static final String MSG_MAX = "max ";
    private static final long serialVersionUID = 1L;

    private Boolean rawData = false;
    private Boolean dataBalancedByDepth = false;
    private Boolean minValueAndAssociatedDepth = false;
    private Boolean maxValueAndAssociatedDepth = false;
    private List<ExtractionParametersVO> extractionParameters = new LinkedList<>();

    /**
     *
     */
    protected ILocalizationManager localizationManager;


    /**
     *
     * @param localizationManager
     */
    public DatasRequestParamVO(ILocalizationManager localizationManager) {
        super();
        this.localizationManager = localizationManager;
    }

    /**
     *
     */
    public DatasRequestParamVO() {
        super();
    }
    /**
     *
     * @return
     */
    public String getExtractionParametersToString() {
        if (extractionParameters.isEmpty()) {
            return "";
        }
        StringBuilder returnString = new StringBuilder(CST_UL);
        for (ExtractionParametersVO extractionParametersVO : extractionParameters) {
            if (extractionParametersVO.getValueMin() || extractionParametersVO.getValueMax() || !extractionParametersVO.getTargetValues().isEmpty()) {
                returnString = returnString.append(CST_LI).append(extractionParametersVO.getLocalizedName()).append(CST_DOUBLE_DOT);
                if (extractionParametersVO.getValueMin()) {
                    returnString = returnString.append(MSG_MIN);
                }
                if (extractionParametersVO.getValueMin() && extractionParametersVO.getValueMax()) {
                    returnString = returnString.append(CST_TAB);
                }
                if (extractionParametersVO.getValueMax()) {
                    returnString = returnString.append(MSG_MAX);
                }
                if (!extractionParametersVO.getTargetValues().isEmpty()) {
                    for (String targetValue : extractionParametersVO.getTargetValues()) {
                        returnString.append(CST_TAB).append(targetValue);
                    }
                }
                returnString = returnString.append(CST_LI_END);
            }
        }
        return returnString.append(CST_UL_END).toString();
    }

    /**
     *
     * @return
     */
    public List<ExtractionParametersVO> getExtractionParameters() {
        return extractionParameters;
    }

    /**
     *
     * @param extractionParameters
     */
    public void setExtractionParameters(List<ExtractionParametersVO> extractionParameters) {
        this.extractionParameters = extractionParameters;
    }

    /**
     *
     * @return
     */
    public Boolean getDataBalancedByDepth() {
        return dataBalancedByDepth;
    }

    /**
     *
     * @param dataBalancedByDepth
     */
    public void setDataBalancedByDepth(Boolean dataBalancedByDepth) {
        this.dataBalancedByDepth = dataBalancedByDepth;
    }

    /**
     *
     * @return
     */
    public Boolean getMinValueAndAssociatedDepth() {
        return minValueAndAssociatedDepth;
    }

    /**
     *
     * @param minValueAndAssociatedDepth
     */
    public void setMinValueAndAssociatedDepth(Boolean minValueAndAssociatedDepth) {
        this.minValueAndAssociatedDepth = minValueAndAssociatedDepth;
    }

    /**
     *
     * @return
     */
    public Boolean getMaxValueAndAssociatedDepth() {
        return maxValueAndAssociatedDepth;
    }

    /**
     *
     * @param maxValueAndAssociatedDepth
     */
    public void setMaxValueAndAssociatedDepth(Boolean maxValueAndAssociatedDepth) {
        this.maxValueAndAssociatedDepth = maxValueAndAssociatedDepth;
    }

    /**
     *
     * @return
     */
    public Boolean getRawData() {
        return rawData;
    }

    /**
     *
     * @param rawData
     */
    public void setRawData(Boolean rawData) {
        this.rawData = rawData;
    }

    /**
     *
     * @param printStream
     */
    public void buildSummary(PrintStream printStream) {
        printStream.println(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_EXTRACTIONS_TYPES));
        processExtractionType(getRawData(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_RAW_EXTRACTION_TYPE), printStream);
        processExtractionType(getDataBalancedByDepth(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_DATA_BALANDED_BY_DEPTH_EXTRACTION_TYPE), printStream);
        processExtractionType(getMinValueAndAssociatedDepth(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN_VALUE_EXTRACTION_TYPE), printStream);
        processExtractionType(getMaxValueAndAssociatedDepth(), getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX_VALUE_EXTRACTION_TYPE), printStream);

        getExtractionParameters().stream().map((extractionParametersVO) -> {
            processExtractionType(extractionParametersVO.getValueMin(), String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MIN_VALUE_EXTRACTION_TYPE_BY_VARIABLE), extractionParametersVO.getName()), printStream);
            return extractionParametersVO;
        }).map((extractionParametersVO) -> {
            processExtractionType(extractionParametersVO.getValueMax(), String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_MAX_VALUE_EXTRACTION_TYPE_BY_VARIABLE), extractionParametersVO.getName()), printStream);
            return extractionParametersVO;
        }).forEachOrdered((extractionParametersVO) -> {
            extractionParametersVO.getTargetValues().forEach((targetValue) -> {
                processExtractionType(
                        true,
                        String.format(getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, MSG_TARGET_VALUE_TYPE), targetValue, extractionParametersVO.getName(),
                                extractionParametersVO.getUncertainties().get(extractionParametersVO.getTargetValues().indexOf(targetValue))), printStream);
            });
        });
        printStream.println();

    }

    private void processExtractionType(Boolean condition, String message, PrintStream reminderPrintStream) {
        if (condition) {
            reminderPrintStream.println(String.format(PATTERN_STRING_EXTRACTION_TYPE, message));
        }
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public DatasRequestParamVO clone() {
        DatasRequestParamVO returnVO = new DatasRequestParamVO(this.localizationManager);
        returnVO.setRawData(this.rawData);
        returnVO.setDataBalancedByDepth(this.dataBalancedByDepth);
        returnVO.setMinValueAndAssociatedDepth(this.minValueAndAssociatedDepth);
        returnVO.setMaxValueAndAssociatedDepth(this.maxValueAndAssociatedDepth);
        List<ExtractionParametersVO> parametersVO = new LinkedList<ExtractionParametersVO>();
        this.extractionParameters.forEach((parameter) -> {
            parametersVO.add(parameter.clone());
        });
        returnVO.setExtractionParameters(parametersVO);
        return returnVO;
    }
    
}