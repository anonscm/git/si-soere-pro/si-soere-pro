package org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere.CaracteristiqueMatierePremieres;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.CaracteristiqueValeur;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * CaracteristiqueValeur.JPA_ID, CaracteristiqueMatierePremieres.ID_JPA
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurCaracteristiqueMP.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    CaracteristiqueValeur.JPA_ID,
    CaracteristiqueMatierePremieres.ID_JPA}))
public class ValeurCaracteristiqueMP implements Serializable {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "valeurcaracteristiquemp";

    /**
     *
     */
    public static final String ID_JPA = "vcmp_id";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ValeurCaracteristiqueMP.ID_JPA, updatable = false)
    private long vcmp_id = 0;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = CaracteristiqueMatierePremieres.ID_JPA, nullable = false)
    private CaracteristiqueMatierePremieres caracteristiquematierepremieres;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = CaracteristiqueValeur.JPA_ID, nullable = false)
    private CaracteristiqueValeur caracteristiquevaleur;

    /**
     *
     */
    public ValeurCaracteristiqueMP() {
    }

    /**
     *
     * @param cmp
     * @param cv
     */
    public ValeurCaracteristiqueMP(CaracteristiqueMatierePremieres cmp, CaracteristiqueValeur cv) {
        this.caracteristiquematierepremieres = cmp;
        this.caracteristiquevaleur = cv;
    }

    /**
     *
     * @return
     */
    public CaracteristiqueMatierePremieres getCaracteristiquematierepremieres() {
        return caracteristiquematierepremieres;
    }

    /**
     *
     * @return
     */
    public CaracteristiqueValeur getCaracteristiquevaleur() {
        return caracteristiquevaleur;
    }

    /**
     *
     * @return
     */
    public long getVcmp_id() {
        return vcmp_id;
    }

    /**
     *
     * @param caracteristiquematierepremieres
     */
    public void setCaracteristiquematierepremieres(CaracteristiqueMatierePremieres caracteristiquematierepremieres) {
        this.caracteristiquematierepremieres = caracteristiquematierepremieres;
    }

    /**
     *
     * @param caracteristiquevaleur
     */
    public void setCaracteristiquevaleur(CaracteristiqueValeur caracteristiquevaleur) {
        this.caracteristiquevaleur = caracteristiquevaleur;
    }

    /**
     *
     * @param vcmp_id
     */
    public void setVcmp_id(long vcmp_id) {
        this.vcmp_id = vcmp_id;
    }

}
