/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.humiditeexpression;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=HumiditeExpression.NAME_ENTITY_JPA)
public class HumiditeExpression implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "humiditeexpression";

    /**
     *
     */
    public static final String JPA_ID = "hum_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE  ="code";
    
     @Id
    @Column(name = HumiditeExpression.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = HumiditeExpression.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String mycode = "";
    @Column(name = HumiditeExpression.JPA_COLUMN_CODE, nullable = false, unique = true)
    private String code = "";
    @Column(name=HumiditeExpression.JPA_COLUMN_NAME, nullable = false,unique = true)
    private String nom = "";

    /**
     *
     */
    public HumiditeExpression() {
        super();
    }
    
    /**
     *
     * @param code
     * @param nom
     */
    public HumiditeExpression(String code,String nom){
        this.code = code;
        this.nom = nom;
        this.mycode = Utils.createCodeFromString(code);
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getMycode() {
        return mycode;
    }

    /**
     *
     * @param mycode
     */
    public void setMycode(String mycode) {
        this.mycode = mycode;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
