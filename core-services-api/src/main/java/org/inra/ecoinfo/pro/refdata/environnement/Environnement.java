package org.inra.ecoinfo.pro.refdata.environnement;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Formula;
import org.inra.ecoinfo.pro.refdata.activiteindustrielle.Activiteindustrielle;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Environnement.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                  Environnement.JPA_COLUMN_DISTANCE_AXE_ROUTIER, 
                                                                  Environnement.JPA_COLUMN_DISTANCE_AGGLOMERATION,
                                                                  Environnement.JPA_COLUMN_DIRECTION_VENT_DOMINANT
       
}))
public class Environnement implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "environnement";

    /**
     *
     */
    public static final String ID_JPA = "env_id";

    /**
     *
     */
    public static final String JPA_COLUMN_DISTANCE_AXE_ROUTIER = "distanceaxeroutier";

    /**
     *
     */
    public static final String JPA_COLUMN_DISTANCE_AGGLOMERATION = "distanceagglomeration";

    /**
     *
     */
    public static final String JPA_COLUMN_DIRECTION_VENT_DOMINANT = "directionventdominant";

    /**
     *
     */
    public static final String JPA_COLUMN_TYPE_EMISSION = "typeemission";

    /**
     *
     */
    public static final String JPA_COLUMN_DISTANCE_AI = "distanceai";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";

    /**
     *
     * @param directionVentDominant
     * @param distanceAgglomeration
     * @param distanceAxeRoutier
     * @return
     */
    public static String buildCodeEnvironnement(String directionVentDominant, String distanceAgglomeration, String distanceAxeRoutier) {
        return String.format("%s_%s_%s", directionVentDominant, distanceAgglomeration, distanceAxeRoutier);
    }

    /**
     *
     * @param directionVentDominant
     * @param distanceAgglomeration
     * @param distanceAxeRoutier
     * @return
     */
    public static String buildCodeEnvironnement(String directionVentDominant, Double distanceAgglomeration, Double distanceAxeRoutier) {
        return String.format("%s_%s_%s", directionVentDominant, distanceAgglomeration, distanceAxeRoutier);
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Environnement.ID_JPA)
    private long env_id = 0;
    @Column(name=Environnement.JPA_COLUMN_DISTANCE_AXE_ROUTIER,nullable=false)
    private double distanceaxeroutier =0.0;
    @Column(name=Environnement.JPA_COLUMN_DISTANCE_AGGLOMERATION,nullable=false)
    private double distanceagglomeration =0.0;
    @Column (name=Environnement.JPA_COLUMN_DIRECTION_VENT_DOMINANT)
    private String directionventdominant ="";
    @Column(name=Environnement.JPA_COLUMN_TYPE_EMISSION)
    private String typeemission="";
    @Column(name=Environnement.JPA_COLUMN_DISTANCE_AI,nullable = true,unique=false)
    private double distanceai =0.0;
    @Column(name=Environnement.JPA_COLUMN_COMMENT,nullable=true,unique=false)
    private String commentaire ="";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Activiteindustrielle.ID_JPA)
    private Activiteindustrielle activiteindustrielle;
    @Formula("directionventdominant || '_'||distanceagglomeration ||  '_'||distanceaxeroutier")
    private String codeEnvironnement;
    
    /**
     *
     */
    public Environnement(){
        super();
    }
    
    /**
     *
     * @param axeroutier
     * @param agglo
     * @param direction
     * @param typemission
     * @param ai
     * @param distanceai
     * @param comment
     */
    public Environnement(double axeroutier, double agglo ,String direction,String typemission,Activiteindustrielle ai,
                             double distanceai,String comment){
        this.distanceaxeroutier = axeroutier;
        this.distanceagglomeration = agglo;
        this.directionventdominant = direction;
        this.typeemission = typemission;
        this.activiteindustrielle = ai;
        this.distanceai =distanceai;
        this.commentaire = comment;
    }

    /**
     *
     * @return
     */
    public long getEnv_id() {
        return env_id;
    }

    /**
     *
     * @param env_id
     */
    public void setEnv_id(long env_id) {
        this.env_id = env_id;
    }

    /**
     *
     * @return
     */
    public double getDistanceaxeroutier() {
        return distanceaxeroutier;
    }

    /**
     *
     * @param distanceaxeroutier
     */
    public void setDistanceaxeroutier(double distanceaxeroutier) {
        this.distanceaxeroutier = distanceaxeroutier;
    }

    /**
     *
     * @return
     */
    public double getDistanceagglomeration() {
        return distanceagglomeration;
    }

    /**
     *
     * @param distanceagglomeration
     */
    public void setDistanceagglomeration(double distanceagglomeration) {
        this.distanceagglomeration = distanceagglomeration;
    }

    /**
     *
     * @return
     */
    public String getDirectionventdominant() {
        return directionventdominant;
    }

    /**
     *
     * @param directionventdominant
     */
    public void setDirectionventdominant(String directionventdominant) {
        this.directionventdominant = directionventdominant;
    }

    /**
     *
     * @return
     */
    public String getTypeemission() {
        return typeemission;
    }

    /**
     *
     * @param typeemission
     */
    public void setTypeemission(String typeemission) {
        this.typeemission = typeemission;
    }

    /**
     *
     * @return
     */
    public Activiteindustrielle getActiviteindustrielle() {
        return activiteindustrielle;
    }

    /**
     *
     * @param activiteindustrielle
     */
    public void setActiviteindustrielle(Activiteindustrielle activiteindustrielle) {
        this.activiteindustrielle = activiteindustrielle;
    } 

    /**
     *
     * @return
     */
    public double getDistanceai() {
        return distanceai;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }
    

    /**
     *
     * @param distanceai
     */
    public void setDistanceai(double distanceai) {
        this.distanceai = distanceai;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
    

}
