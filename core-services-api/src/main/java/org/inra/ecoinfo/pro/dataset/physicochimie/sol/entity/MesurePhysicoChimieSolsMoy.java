/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesurePhysicoChimieSolsMoy.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    DescriptionTraitement.ID_JPA,
    MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE,
    MesurePhysicoChimieSolsMoy.COLUMN_NOM_LABO,
    MesurePhysicoChimieSolsMoy.COLUMN_LIMIT_SUP,
    MesurePhysicoChimieSolsMoy.COLUMN_LIMIT_INF,
    MesurePhysicoChimieSolsMoy.COLUMN_NOM_COUCHE}),
        indexes = {
            @Index(name = "mesure_physico_chimie_solmoy_traitement_idx", columnList = DescriptionTraitement.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, referencedColumnName = MesurePhysicoChimieSolsMoy.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, column = @Column(name = MesurePhysicoChimieSolsMoy.ID_JPA))})
public class MesurePhysicoChimieSolsMoy extends MesurePhysicoChimie<MesurePhysicoChimieSolsMoy, ValeurPhysicoChimieSolsMoy> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesurephysicochimiesolsmoy";

    /**
     *
     */
    public static final String ID_JPA = "msols_moy_id";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate = "LocalDateprelevement";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "nom_laboratoire";

    /**
     *
     */
    public static final String COLUMN_NOM_COUCHE = "nom_couche";

    /**
     *
     */
    public static final String COLUMN_REP_ANALYSE = "nbre_repetition";

    /**
     *
     */
    public static final String COLUMN_LIMIT_SUP = "limit_sup";

    /**
     *
     */
    public static final String COLUMN_LIMIT_INF = "limit_inf";

    /**
     *
     */
    public static final String COLUMN_KEY_MOYENNE = "keymesuremoyenne";

    static final String RESOURCE_PATH = "%s/%s";

    @Column(name = MesurePhysicoChimieSolsMoy.COLUMN_NOM_LABO, nullable = false)
    private String nom_laboratoire = "";

    @Column(name = MesurePhysicoChimieSolsMoy.COLUMN_NOM_COUCHE, nullable = false)
    private String nom_couche = "";

    @Column(name = MesurePhysicoChimieSolsMoy.COLUMN_REP_ANALYSE, nullable = false)
    private int nbre_repetition = 0;

    @Column(name = MesurePhysicoChimieSolsMoy.COLUMN_LIMIT_SUP, nullable = false)
    private double limit_sup = 0;

    @Column(name = MesurePhysicoChimieSolsMoy.COLUMN_LIMIT_INF, nullable = false)
    private double limit_inf = 0;

    @Column(name = MesurePhysicoChimieSolsMoy.COLUMN_KEY_MOYENNE, nullable = false, unique = true)
    private String keymesuremoyenne = null;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private DescriptionTraitement descriptionTraitement;

    @OneToMany(mappedBy = "mesurePhysicoChimieSolsMoy", cascade = ALL)
    List<ValeurPhysicoChimieSolsMoy> valeurPhysicoChimieSolMoy = new LinkedList();

    /**
     *
     */
    public MesurePhysicoChimieSolsMoy() {
        super();
    }

    /**
     *
     * @param LocalDate_prelevement
     * @param nom_laboratoire
     * @param nom_couche
     * @param nbre_repetition
     * @param limit_sup
     * @param limit_inf
     * @param versionfile
     * @param descriptionTraitement
     * @param fichier
     */
    public MesurePhysicoChimieSolsMoy(LocalDate LocalDate_prelevement, String nom_laboratoire, String nom_couche, int nbre_repetition, double limit_sup, double limit_inf,
            VersionFile versionfile, DescriptionTraitement descriptionTraitement, Long fichier) {
        super(LocalDate_prelevement, fichier, versionfile);
        this.nom_laboratoire = nom_laboratoire;
        this.nom_couche = nom_couche;
        this.nbre_repetition = nbre_repetition;
        this.limit_sup = limit_sup;
        this.limit_inf = limit_inf;
        this.descriptionTraitement = descriptionTraitement;
    }

    /**
     *
     * @return
     */
    public String getNom_laboratoire() {
        return nom_laboratoire;
    }

    /**
     *
     * @param nom_laboratoire
     */
    public void setNom_laboratoire(String nom_laboratoire) {
        this.nom_laboratoire = nom_laboratoire;
    }

    /**
     *
     * @return
     */
    public String getNom_couche() {
        return nom_couche;
    }

    /**
     *
     * @param nom_couche
     */
    public void setNom_couche(String nom_couche) {
        this.nom_couche = nom_couche;
    }

    /**
     *
     * @return
     */
    public int getNbre_repetition() {
        return nbre_repetition;
    }

    /**
     *
     * @param nbre_repetition
     */
    public void setNbre_repetition(int nbre_repetition) {
        this.nbre_repetition = nbre_repetition;
    }

    /**
     *
     * @return
     */
    public double getLimit_sup() {
        return limit_sup;
    }

    /**
     *
     * @param limit_sup
     */
    public void setLimit_sup(double limit_sup) {
        this.limit_sup = limit_sup;
    }

    /**
     *
     * @return
     */
    public double getLimit_inf() {
        return limit_inf;
    }

    /**
     *
     * @param limit_inf
     */
    public void setLimit_inf(double limit_inf) {
        this.limit_inf = limit_inf;
    }
    /**
     *
     * @return
     */
    public DescriptionTraitement getDescriptionTraitement() {
        return descriptionTraitement;
    }

    /**
     *
     * @param descriptionTraitement
     */
    public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    /**
     *
     * @return
     */
    public List<ValeurPhysicoChimieSolsMoy> getValeurPhysicoChimieSolMoy() {
        return valeurPhysicoChimieSolMoy;
    }

    /**
     *
     * @param valeurPhysicoChimieSolMoy
     */
    public void setValeurPhysicoChimieSolMoy(List<ValeurPhysicoChimieSolsMoy> valeurPhysicoChimieSolMoy) {
        this.valeurPhysicoChimieSolMoy = valeurPhysicoChimieSolMoy;
    }

    /**
     *
     * @return
     */
    public String getKeymesuremoyenne() {
        return keymesuremoyenne;
    }

    /**
     *
     * @param keymesuremoyenne
     */
    public void setKeymesuremoyenne(String keymesuremoyenne) {
        this.keymesuremoyenne = keymesuremoyenne;
    }

}
