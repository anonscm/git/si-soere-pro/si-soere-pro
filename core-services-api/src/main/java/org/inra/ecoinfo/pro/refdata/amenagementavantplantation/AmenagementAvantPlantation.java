/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.amenagementavantplantation;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.cepagevariete.CepageVariete;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=AmenagementAvantPlantation.NAME_ENTITY_JPA)
public class AmenagementAvantPlantation implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "amenagementavantplantation";

    /**
     *
     */
    public static final String JPA_ID = "aap_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "aap_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "aap_libelle";
    
    @Id
    @Column(name = CepageVariete.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long aap_id;
    @Column(name = AmenagementAvantPlantation.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String aap_code = "";
    @Column(name = AmenagementAvantPlantation.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String aap_libelle = "";
    
    /**
     *
     */
    public AmenagementAvantPlantation(){
        super();
    }
    
    /**
     *
     * @param libelle
     */
    public AmenagementAvantPlantation(String libelle){
        this.aap_libelle = libelle;
    }

    /**
     *
     * @return
     */
    public long getAap_id() {
        return aap_id;
    }

    /**
     *
     * @return
     */
    public String getAap_code() {
        return aap_code;
    }

    /**
     *
     * @return
     */
    public String getAap_libelle() {
        return aap_libelle;
    }

    /**
     *
     * @param aap_id
     */
    public void setAap_id(long aap_id) {
        this.aap_id = aap_id;
    }

    /**
     *
     * @param aap_code
     */
    public void setAap_code(String aap_code) {
        this.aap_code = aap_code;
    }

    /**
     *
     * @param aap_libelle
     */
    public void setAap_libelle(String aap_libelle) {
        this.aap_libelle = aap_libelle;
    }
            
    
}
