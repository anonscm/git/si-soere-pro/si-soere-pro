/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;


/**
 *
 * @author adiankha
 */
public class InfoReport implements IInfoReport{
     static final String CST_HYPHEN       = "-";
     
      static final String CST_NEW_LINE     = "\n";
      
      String infosMessages = org.apache.commons.lang.StringUtils.EMPTY;
      
    /**
     *
     */
    public  InfoReport(){
          super();
      }

    /**
     *
     * @param infoMessage
     */
    @Override
    public void addInfoMessage(String infoMessage) {
        this.infosMessages = this.infosMessages.concat(InfoReport.CST_HYPHEN).concat(infoMessage).concat(InfoReport.CST_NEW_LINE);
    }

    /**
     *
     * @return
     */
    @Override
    public String getInfoMessages() {
        return this.infosMessages;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean hasInfo() {
        return this.infosMessages.length()>0;
    }
}
