/**
 *
 */
package org.inra.ecoinfo.pro.dataset;

/**
 * @author sophie
 *
 */
public class Constantes {

    /*** Application ***/
    /*
     * public static final String NEW_LINE = "\n"; public static final String TIRET = "-"; public static final String COMMA = ","; public static final String DOT = "."; public static final String SPACE = " "; public static final String VIDE = "";
     */
    public static final String STRING_EMPTY = "";
    // accepte le numéro au format international (+33) x xx xx xx xx pour les
    // numéros français

    /**
     *
     */
    public static final String REGEX_TEL_FAX = "\\(?\\+?[0-9]{1,3}?\\)?[ ]?([ ]?[0-9]){4,}$";

    /**
     *
     */
    public static final String REGEX_EMAIL = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";

    /**
     *
     */
    public static final String FRANCE = "France";
    /*
     * public static final String UNDERSCORE = "_"; public static final String STAR = "*"; public static final char SEPARATOR = ';'; public static final String MIN = "min"; public static final String MAX = "max";
     */
}
