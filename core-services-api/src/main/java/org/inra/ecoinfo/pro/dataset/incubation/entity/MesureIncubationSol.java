/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureIncubationSol.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    EchantillonsSol.ID_JPA,
    MesureIncubation.COLUMN_JPA_LocalDate_PREL_SOL,
    MesureIncubation.COLUMN_ORDRE_MANIP,
    MesureIncubation.COLUMN_NUM_REP,
    MesureIncubation.COLUMN_JOUR_INCUB}),
        indexes = {
            @Index(name = "mesure_incub_sol_idx", columnList = MesureIncubationSol.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesureIncubation.ATTRIBUTE_JPA_ID, referencedColumnName = MesureIncubationSol.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesureIncubation.ATTRIBUTE_JPA_ID, column = @Column(name = MesureIncubationSol.ID_JPA))})
public class MesureIncubationSol extends MesureIncubation<MesureIncubationSol, ValeurIncubationSol> {

    /**
     *
     */
    public static final String ID_JPA = "mis_id";

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesureincubationsol";

    /**
     *
     */
    public static final String COLUMN_TEMPERATURE_INC = "temperature_incub";

    /**
     *
     */
    public static final String COLUMN_HUMIDITE_INC = "humidite_incub";

    /**
     *
     */
    public static final String COLUMN_COND_INC = "condition_incub";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "labo_analyse";

    /**
     *
     */
    public static final String COLUMN_CODE_INT_LABO = "code_interne_labo";

    /**
     *
     */
    public static final String COLUMN_N_MINERAL = "n_mineral_apporte";

    static final String RESOURCE_PATH = "%s/%s";

    @Column(name = MesureIncubationSol.COLUMN_NOM_LABO, nullable = false)
    private String labo_analyse = "";

    @Column(name = MesureIncubationSol.COLUMN_CODE_INT_LABO, nullable = false)
    private String code_interne_labo = "";

    @Column(name = MesureIncubationSol.COLUMN_COND_INC, nullable = false)
    private String condition_incub = "";

    @Column(name = MesureIncubationSol.COLUMN_TEMPERATURE_INC, nullable = false)
    private float temperature_incub = 0;

    @Column(name = MesureIncubationSol.COLUMN_N_MINERAL, nullable = false)
    private float n_mineral_apporte = 0;

    @Column(name = MesureIncubationSol.COLUMN_HUMIDITE_INC, nullable = false)
    private String humidite_incub = "";

    @OneToMany(mappedBy = "mesureIncubationSol", cascade = ALL)
    List<ValeurIncubationSol> valeurIncubationSol = new LinkedList();

    /**
     *
     */
    public MesureIncubationSol() {
        super();
    }

    /**
     *
     * @param LocalDate_prel_sol
     * @param LocalDate_debut_incub
     * @param jour_incub
     * @param humidite_incub
     * @param temperature_incub
     * @param n_mineral_apporte
     * @param masse_de_sol
     * @param condition_incub
     * @param code_interne_labo
     * @param numero_rep_analyse
     * @param labo_analyse
     * @param ligneFichierEchange
     * @param versionfile
     * @param echantillonsSol
     * @param ordre_manip
     */
    public MesureIncubationSol(LocalDate LocalDate_prel_sol, LocalDate LocalDate_debut_incub, int jour_incub, String humidite_incub,
            float temperature_incub, float n_mineral_apporte,
            float masse_de_sol, String condition_incub, String code_interne_labo, int numero_rep_analyse, String labo_analyse, Long ligneFichierEchange, VersionFile versionfile, EchantillonsSol echantillonsSol, int ordre_manip) {
        super(LocalDate_prel_sol, LocalDate_debut_incub, numero_rep_analyse, masse_de_sol, jour_incub, ordre_manip, ligneFichierEchange, versionfile, echantillonsSol);
        this.humidite_incub = humidite_incub;
        this.temperature_incub = temperature_incub;
        this.n_mineral_apporte = n_mineral_apporte;
        this.condition_incub = condition_incub;
        this.code_interne_labo = code_interne_labo;
        this.labo_analyse = labo_analyse;
    }

    /**
     *
     * @return
     */
    public String getLabo_analyse() {
        return labo_analyse;
    }

    /**
     *
     * @param labo_analyse
     */
    public void setLabo_analyse(String labo_analyse) {
        this.labo_analyse = labo_analyse;
    }

    /**
     *
     * @return
     */
    public String getCode_interne_labo() {
        return code_interne_labo;
    }

    /**
     *
     * @param code_interne_labo
     */
    public void setCode_interne_labo(String code_interne_labo) {
        this.code_interne_labo = code_interne_labo;
    }

    /**
     *
     * @return
     */
    public String getCondition_incub() {
        return condition_incub;
    }

    /**
     *
     * @param condition_incub
     */
    public void setCondition_incub(String condition_incub) {
        this.condition_incub = condition_incub;
    }

    /**
     *
     * @return
     */
    public float getTemperature_incub() {
        return temperature_incub;
    }

    /**
     *
     * @param temperature_incub
     */
    public void setTemperature_incub(float temperature_incub) {
        this.temperature_incub = temperature_incub;
    }

    /**
     *
     * @return
     */
    public float getN_mineral_apporte() {
        return n_mineral_apporte;
    }

    /**
     *
     * @param n_mineral_apporte
     */
    public void setN_mineral_apporte(float n_mineral_apporte) {
        this.n_mineral_apporte = n_mineral_apporte;
    }

    /**
     *
     * @return
     */
    public String getHumidite_incub() {
        return humidite_incub;
    }

    /**
     *
     * @param humidite_incub
     */
    public void setHumidite_incub(String humidite_incub) {
        this.humidite_incub = humidite_incub;
    }

    /**
     *
     * @return
     */
    public List<ValeurIncubationSol> getValeurIncubationSol() {
        return valeurIncubationSol;
    }

    /**
     *
     * @param valeurIncubationSol
     */
    public void setValeurIncubationSol(List<ValeurIncubationSol> valeurIncubationSol) {
        this.valeurIncubationSol = valeurIncubationSol;
    }

}
