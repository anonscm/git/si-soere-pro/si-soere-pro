/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonssol;

import java.io.Serializable;
import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.prelevementsol.PrelevementSol;
import org.inra.ecoinfo.pro.refdata.typeechantillon.TypeEchantillon;
import org.inra.ecoinfo.utils.Utils;

/**
 * {
 * EchantillonsSol.JPA_COLUMN_CODE_ECHANTILLONSSOL, PrelevementSol.ID_JPA }
 *
 * @author adiankha
 */
@Entity
@Table(name = EchantillonsSol.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    EchantillonsSol.JPA_COLUMN_CODE_ECHANTILLONSSOL,
    PrelevementSol.ID_JPA
}))
public class EchantillonsSol implements Serializable, Comparable<EchantillonsSol> {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "echantillonssol";

    /**
     *
     */
    public static final String ID_JPA = "es_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_LABORATOIRE = "codelabo";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_ECHANTILLONSSOL = "codebaseechantillon";

    /**
     *
     */
    public static final String JPA_COLUMN_STATUT_ECHANTILLON = "statut";

    /**
     *
     */
    public static final String JPA_COLUMN_TAMISSAGE_ECHANTILLON = "tamisage";

    /**
     *
     */
    public static final String JPA_COLUMN_EMOTTAGE = "emottage";

    /**
     *
     */
    public static final String JPA_COLUMN_BROYAGE = "broyage";

    /**
     *
     */
    public static final String JPA_COLUMN_TYPE_ECHANTILLON = "typeechantillon";

    /**
     *
     */
    public static final String JPA_COLUMN_MASSE_ECHANTILLON = "masseprelevee";

    /**
     *
     */
    public static final String JPA_COLUMN_TEMPERATURE_SECHAGE_ECHANTILLON = "temperaturesechage";

    /**
     *
     */
    public static final String JPA_COLUMN_HEMOGENEISATION = "homogeneisation";

    /**
     *
     */
    public static final String JPA_COLUMN_TEMPERATURE_CONSERVATION = "tempconservation";

    /**
     *
     */
    public static final String JPA_COLUMN_TEMPERATURE_CONSERVATION_ECHANTILLONTHEQUE = "tempconservationechantilonntheque";

    /**
     *
     */
    public static final String JPA_COLUMN_CONSERVATION_APRES_ANALYSE = "conservationapresanalyse";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENTAIRE = "commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_ECH_SOL = "codeechsol";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE_ECH_SOL = "codeesol";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = EchantillonsSol.ID_JPA)
    private long es_id = 0;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = PrelevementSol.ID_JPA, nullable = false, unique = false)
    private PrelevementSol prelevementsol;
    @Column(name = EchantillonsSol.JPA_COLUMN_CODE_ECHANTILLONSSOL, nullable = false, unique = false)
    private long codebaseechantillon = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_CODE_ECH_SOL, nullable = false, unique = true)
    private String codeechsol = "";
    @Column(name = EchantillonsSol.JPA_COLUMN_MYCODE_ECH_SOL, nullable = false, unique = true)
    private String codeesol = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeEchantillon.JPA_ID, nullable = false, unique = false)
    private TypeEchantillon typeechantillon;
    @Column(name = EchantillonsSol.JPA_COLUMN_CODE_LABORATOIRE, nullable = false, unique = false)
    private String codelabo = "";
    @Column(name = EchantillonsSol.JPA_COLUMN_STATUT_ECHANTILLON, nullable = false, unique = false)
    private String statut = "";
    @Column(name = EchantillonsSol.JPA_COLUMN_HEMOGENEISATION, nullable = false, unique = false)
    private String homogeneisation = "";
    @Column(name = EchantillonsSol.JPA_COLUMN_EMOTTAGE, nullable = true, unique = false)
    private String emottage = null;
    @Column(name = EchantillonsSol.JPA_COLUMN_TAMISSAGE_ECHANTILLON, nullable = true, unique = false)
    private float tamisage = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_TEMPERATURE_SECHAGE_ECHANTILLON, nullable = true, unique = false)
    private float temperaturesechage = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_BROYAGE, nullable = true, unique = false)
    private float broyage = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_TEMPERATURE_CONSERVATION, nullable = true, unique = false)
    private float tempConservation = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_CONSERVATION_APRES_ANALYSE, nullable = true, unique = false)
    private String conservationapresanalyse = "";
    @Column(name = EchantillonsSol.JPA_COLUMN_TEMPERATURE_CONSERVATION_ECHANTILLONTHEQUE, nullable = true, unique = false)
    private float tempconservationEchantillontheque = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_MASSE_ECHANTILLON, nullable = true, unique = false)
    private float masseavantenvoie = 0;
    @Column(name = EchantillonsSol.JPA_COLUMN_COMMENTAIRE, nullable = true, unique = false)
    private String commentaire = "";

    /**
     *
     */
    public EchantillonsSol() {
        super();
    }

    /**
     *
     * @param codelabo
     * @param codebaseechantillon
     * @param statut
     * @param tamisage
     * @param broyage
     * @param masseavantenvoie
     * @param temperaturesechage
     * @param homogeneisation
     * @param tempconcervation
     * @param prelevementsol
     * @param emottage
     * @param conservationapresanalyse
     * @param commentaire
     * @param codeechsol
     * @param typeechantillon
     */
    public EchantillonsSol(PrelevementSol prelevementsol,long codebaseechantillon,String codeechsol, TypeEchantillon typeechantillon, String codelabo, 
            String statut, String homogeneisation, String emottage, float tamisage, float temperaturesechage,  float broyage,
            float tempconcervation, String conservationapresanalyse, float tempconservationEchantillontheque,
            float masseavantenvoie, String commentaire) {
        this.prelevementsol = prelevementsol;
        this.codebaseechantillon = codebaseechantillon;
        this.codeechsol = codeechsol;
        this.codeesol = Utils.createCodeFromString(codeechsol);
        this.typeechantillon = typeechantillon;
        this.codelabo = codelabo;
        this.statut = statut;
        this.homogeneisation = homogeneisation;
        this.emottage = emottage;
        this.tamisage = tamisage;
        this.temperaturesechage = temperaturesechage;
        this.broyage = broyage;
        this.tempConservation = tempconcervation;
        this.conservationapresanalyse = conservationapresanalyse;
        this.tempconservationEchantillontheque= tempconservationEchantillontheque;
        this.masseavantenvoie = masseavantenvoie;
        this.commentaire = commentaire;
    }

    public long getEs_id() {
        return es_id;
    }

    public void setEs_id(long es_id) {
        this.es_id = es_id;
    }

    public PrelevementSol getPrelevementsol() {
        return prelevementsol;
    }

    public void setPrelevementsol(PrelevementSol prelevementsol) {
        this.prelevementsol = prelevementsol;
    }

    public long getCodebaseechantillon() {
        return codebaseechantillon;
    }

    public void setCodebaseechantillon(long codebaseechantillon) {
        this.codebaseechantillon = codebaseechantillon;
    }

    public String getCodeechsol() {
        return codeechsol;
    }

    public void setCodeechsol(String codeechsol) {
        this.codeechsol = codeechsol;
    }

    public String getCodeesol() {
        return codeesol;
    }

    public void setCodeesol(String codeesol) {
        this.codeesol = codeesol;
    }

    public TypeEchantillon getTypeechantillon() {
        return typeechantillon;
    }

    public void setTypeechantillon(TypeEchantillon typeechantillon) {
        this.typeechantillon = typeechantillon;
    }

    public String getCodelabo() {
        return codelabo;
    }

    public void setCodelabo(String codelabo) {
        this.codelabo = codelabo;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getHomogeneisation() {
        return homogeneisation;
    }

    public void setHomogeneisation(String homogeneisation) {
        this.homogeneisation = homogeneisation;
    }

    public String getEmottage() {
        return emottage;
    }

    public void setEmottage(String emottage) {
        this.emottage = emottage;
    }

    public float getTamisage() {
        return tamisage;
    }

    public void setTamisage(float tamisage) {
        this.tamisage = tamisage;
    }

    public float getTemperaturesechage() {
        return temperaturesechage;
    }

    public void setTemperaturesechage(float temperaturesechage) {
        this.temperaturesechage = temperaturesechage;
    }

    public float getBroyage() {
        return broyage;
    }

    public void setBroyage(float broyage) {
        this.broyage = broyage;
    }

    public float getTempConservation() {
        return tempConservation;
    }

    public void setTempConservation(float tempConservation) {
        this.tempConservation = tempConservation;
    }

    public String getConservationapresanalyse() {
        return conservationapresanalyse;
    }

    public void setConservationapresanalyse(String conservationapresanalyse) {
        this.conservationapresanalyse = conservationapresanalyse;
    }

    public float getTempconservationEchantillontheque() {
        return tempconservationEchantillontheque;
    }

    public void setTempconservationEchantillontheque(float tempconservationEchantillontheque) {
        this.tempconservationEchantillontheque = tempconservationEchantillontheque;
    }

    public float getMasseavantenvoie() {
        return masseavantenvoie;
    }

    public void setMasseavantenvoie(float masseavantenvoie) {
        this.masseavantenvoie = masseavantenvoie;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.codeesol);
        return hash;
    }

    @Override
    public int compareTo(EchantillonsSol o) {
        return getCodeesol().compareTo(o.getCodeesol());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EchantillonsSol other = (EchantillonsSol) obj;
        return Objects.equals(this.codeesol, other.codeesol);
    }

}
