/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeintrants;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = ListeIntrants.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    ListeIntrants.ATTRIBUTE_JPA_NOM_COMMERCIAL,
    ListeIntrants.ATTRIBUTE_JPA_NOM_ELEMENT}))
public class ListeIntrants implements Serializable {

    /**
     *
     */
    protected static final String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s/%s";

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "listeintrants";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "intrantid";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NOM_COMMERCIAL = "nomcommercial";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_KNOM_COMMERCIAL = "knomcommercial";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NOM_ELEMENT = "nomelement";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_KNOM_ELEMENT = "knomelement";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_CONCENTRATION = "concentration";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_UNITE = "unite";
    
    @Id
    @Column(name = ListeIntrants.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long intrantid;

    @Column(name = ListeIntrants.ATTRIBUTE_JPA_NOM_COMMERCIAL,nullable = false)
    private String nomcommercial;
    @Column(name = ListeIntrants.ATTRIBUTE_JPA_KNOM_COMMERCIAL,nullable = false)
    private String knomcommercial;
    @Column(name = ListeIntrants.ATTRIBUTE_JPA_NOM_ELEMENT,nullable = false)
    private String nomelement;
    @Column(name = ListeIntrants.ATTRIBUTE_JPA_KNOM_ELEMENT,nullable = false)
    private String knomelement;
     @Column(name = ListeIntrants.ATTRIBUTE_JPA_CONCENTRATION)
    private int concentration;
     @Column(name = ListeIntrants.ATTRIBUTE_JPA_UNITE)
    private String unite;

    /**
     *
     */
    public ListeIntrants() {
        super();
    }

    /**
     *
     * @param nomcommercial
     * @param knomcommercial
     * @param nomelement
     * @param knomelement
     * @param concentration
     * @param unite
     */
    public ListeIntrants(String nomcommercial, String knomcommercial, String nomelement, String knomelement, int concentration, String unite) {
        this.nomcommercial = nomcommercial;
        this.knomcommercial = knomcommercial;
        this.nomelement = nomelement;
        this.knomelement = knomelement;
        this.concentration = concentration;
        this.unite = unite;
    }

    /**
     *
     * @return
     */
    public Long getIntrantid() {
        return intrantid;
    }

    /**
     *
     * @param intrantid
     */
    public void setIntrantid(Long intrantid) {
        this.intrantid = intrantid;
    }

    /**
     *
     * @return
     */
    public String getNomcommercial() {
        return nomcommercial;
    }

    /**
     *
     * @param nomcommercial
     */
    public void setNomcommercial(String nomcommercial) {
        this.nomcommercial = nomcommercial;
    }

    /**
     *
     * @return
     */
    public String getKnomcommercial() {
        return knomcommercial;
    }

    /**
     *
     * @param knomcommercial
     */
    public void setKnomcommercial(String knomcommercial) {
        this.knomcommercial = knomcommercial;
    }

    /**
     *
     * @return
     */
    public String getNomelement() {
        return nomelement;
    }

    /**
     *
     * @param nomelement
     */
    public void setNomelement(String nomelement) {
        this.nomelement = nomelement;
    }

    /**
     *
     * @return
     */
    public String getKnomelement() {
        return knomelement;
    }

    /**
     *
     * @param knomelement
     */
    public void setKnomelement(String knomelement) {
        this.knomelement = knomelement;
    }

    /**
     *
     * @return
     */
    public int getConcentration() {
        return concentration;
    }

    /**
     *
     * @param concentration
     */
    public void setConcentration(int concentration) {
        this.concentration = concentration;
    }

    /**
     *
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }
     
     

}
