/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsproduit;

import java.io.Serializable;
import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.prelevementproduit.PrelevementProduit;
import org.inra.ecoinfo.utils.Utils;

/**
 * @UniqueConstraint(columnNames = { PrelevementProduit.ID_JPA,
 * EchantillonsProduit.JPA_COLUMN_CODE_ECHANTILLON_UNIQUE }
 * @author adiankha
 */
@Entity
@Table(name = EchantillonsProduit.JPA_NAME_ENTITY, uniqueConstraints = @UniqueConstraint(columnNames = {
    PrelevementProduit.ID_JPA,
    EchantillonsProduit.JPA_COLUMN_NUM_ECHANTILLON
}))
public class EchantillonsProduit implements Serializable, Comparable<EchantillonsProduit> {

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "echantillonsproduit";

    /**
     *
     */
    public static final String ID_JPA = "ep_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_ECHANTILLON = "codelabo";

    /**
     *
     */
    public static final String JPA_COLUMN_TYPE_ECHANTILLON = "typeechantillon";

    /**
     *
     */
    public static final String JPA_COLUMN_TEMP_CONSER = "temperatureconservation";

    /**
     *
     */
    public static final String JPA_COLUMN_DURATION = "dureeconservationprevue";

    /**
     *
     */
    public static final String JPA_COLUMN_NUM_ECHANTILLON = "numeroechantillon";

    /**
     *
     */
    public static final String JPA_COLUMN_BROYAGE = "broyage";

    /**
     *
     */
    public static final String JPA_COLUMN_STATUT = "statut";

    /**
     *
     */
    public static final String JPA_COLUMN_SECHAGE = "sechage";

    /**
     *
     */
    public static final String JPA_COLUMN_TEMPERATUREAPRESANALYSE = "temperatureconservationapresanalyse";

    /**
     *
     */
    public static final String JPA_COLUMN_HOMOGENEISATION = "homageneisation";

    /**
     *
     */
    public static final String JPA_COLUMN_TAMISAGE = "tamisage";

    /**
     *
     */
    public static final String JPA_COLUMN_CONSERVATION_APRES_ANALYSE = "conservationapresanalyse";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_ECHANTILLON_UNIQUE = "code_unique";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE_ECH_SOL = "codeepro";

    /**
     *
     */
    public static final String JPA_COLUMN_MASSE_ECHANTILLON = "masseprelevee";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";

    /**
     *
     * @param prelevementProduitCode
     * @param numEchantillon
     * @return
     */
    public static final String buildCodeUnique(String prelevementProduitCode, long numEchantillon) {
        return String.format("%s_%s", prelevementProduitCode, numEchantillon);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = EchantillonsProduit.ID_JPA, nullable = false, updatable = false, unique = true)
    private long ep_id = 0;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = PrelevementProduit.ID_JPA, nullable = false, unique = false)
    private PrelevementProduit prelevementproduit;
    @Column(name = EchantillonsProduit.JPA_COLUMN_NUM_ECHANTILLON, nullable = false, unique = false)
    private long numeroechantillon;
    @Column(name = EchantillonsProduit.JPA_COLUMN_MYCODE_ECH_SOL)
    private String codePro = "";
    @Column(name = EchantillonsProduit.JPA_COLUMN_CODE_ECHANTILLON_UNIQUE, nullable = false, unique = true)
    private String code_unique = "";
    @Column(name = EchantillonsProduit.JPA_COLUMN_TYPE_ECHANTILLON, nullable = false, unique = false)
    private String typeechantillon = null;
    @Column(name = EchantillonsProduit.JPA_COLUMN_CODE_ECHANTILLON, nullable = true, unique = false)
    private String codelabo = null;
    @Column(name = EchantillonsProduit.JPA_COLUMN_STATUT, nullable = false, unique = false)
    private String statut = "";
    @Column(name = EchantillonsProduit.JPA_COLUMN_HOMOGENEISATION, nullable = false, unique = false)
    private String homogeneisation;
    @Column(name = EchantillonsProduit.JPA_COLUMN_TAMISAGE)
    private float tamisage = 0;
    @Column(name = EchantillonsProduit.JPA_COLUMN_SECHAGE)
    private float sechage = 0;
    @Column(name = EchantillonsProduit.JPA_COLUMN_BROYAGE)
    private float broyage = 0;
    @Column(name = EchantillonsProduit.JPA_COLUMN_TEMP_CONSER)
    private double temperatureConservationAvantAnalyse = 0.0;
    @Column(name = EchantillonsProduit.JPA_COLUMN_CONSERVATION_APRES_ANALYSE)
    private String conservationapresanalyse = null;
    @Column(name = EchantillonsProduit.JPA_COLUMN_TEMPERATUREAPRESANALYSE)
    private double temperatureConservationApresAnalyse = 0;
    @Column(name = EchantillonsProduit.JPA_COLUMN_MASSE_ECHANTILLON, nullable = true, unique = false)
    private float masseavantenvoie = 0;
    @Column(name = EchantillonsProduit.JPA_COLUMN_COMENT)
    private String commentaire = "";
    
    




    /**
     *
     */
    public EchantillonsProduit() {
        super();
    }

    /**
     *
     * @param codeLabo
     * @param tempAvantAnalyse
     * @param prelevementproduit
     * @param type
     * @param hemoge
     * @param broyage
     * @param statut
     * @param sechage
     * @param tempApresAnalyse
     * @param tamisage
     * @param conservationapresanalyse
     * @param numeroechantillon
     * @param codeEchantillon
     * @param commentairei
     */
    public EchantillonsProduit(PrelevementProduit prelevementproduit, long numeroechantillon, String codeEchantillon,
            String type, String codeLabo, String statut,
            String hemoge, float tamisage, float sechage, float broyage, 
            double tempAvantAnalyse, String conservationapresanalyse,  float tempApresAnalyse,
            float masseavantenvoie, String commentaire) {
        this.prelevementproduit = prelevementproduit;
        this.numeroechantillon = numeroechantillon;
        this.code_unique = Utils.createCodeFromString(codeEchantillon);
        this.codePro = codeEchantillon;
        this.typeechantillon = type;
        this.codelabo = codeLabo;
        this.statut = statut;
        this.homogeneisation = hemoge;
        this.tamisage = tamisage;
        this.sechage = sechage;
        this.broyage = broyage;
        this.temperatureConservationAvantAnalyse = tempAvantAnalyse;
        this.conservationapresanalyse = conservationapresanalyse;
        this.temperatureConservationApresAnalyse = tempApresAnalyse;
        this.masseavantenvoie = masseavantenvoie;
        this.commentaire = commentaire;
    }

    public long getEp_id() {
        return ep_id;
    }

    public void setEp_id(long ep_id) {
        this.ep_id = ep_id;
    }

    public PrelevementProduit getPrelevementproduit() {
        return prelevementproduit;
    }

    public void setPrelevementproduit(PrelevementProduit prelevementproduit) {
        this.prelevementproduit = prelevementproduit;
    }

    public long getNumeroechantillon() {
        return numeroechantillon;
    }

    public void setNumeroechantillon(long numeroechantillon) {
        this.numeroechantillon = numeroechantillon;
    }

    public String getCode_unique() {
        return code_unique;
    }

    public void setCode_unique(String code_unique) {
        this.code_unique = code_unique;
    }

    public String getTypeechantillon() {
        return typeechantillon;
    }

    public void setTypeechantillon(String typeechantillon) {
        this.typeechantillon = typeechantillon;
    }

    public String getCodelabo() {
        return codelabo;
    }

    public String getCodePro() {
        return codePro;
    }

    public void setCodePro(String codePro) {
        this.codePro = codePro;
    }

    public void setCodelabo(String codelabo) {
        this.codelabo = codelabo;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getHomogeneisation() {
        return homogeneisation;
    }

    public void setHomogeneisation(String homogeneisation) {
        this.homogeneisation = homogeneisation;
    }

    public float getTamisage() {
        return tamisage;
    }

    public void setTamisage(float tamisage) {
        this.tamisage = tamisage;
    }

    public float getSechage() {
        return sechage;
    }

    public void setSechage(float sechage) {
        this.sechage = sechage;
    }

    public float getBroyage() {
        return broyage;
    }

    public void setBroyage(float broyage) {
        this.broyage = broyage;
    }

    public double getTemperatureConservationAvantAnalyse() {
        return temperatureConservationAvantAnalyse;
    }

    public void setTemperatureConservationAvantAnalyse(double temperatureConservationAvantAnalyse) {
        this.temperatureConservationAvantAnalyse = temperatureConservationAvantAnalyse;
    }

    public String getConservationapresanalyse() {
        return conservationapresanalyse;
    }

    public void setConservationapresanalyse(String conservationapresanalyse) {
        this.conservationapresanalyse = conservationapresanalyse;
    }

    public double getTemperatureConservationApresAnalyse() {
        return temperatureConservationApresAnalyse;
    }

    public void setTemperatureConservationApresAnalyse(double temperatureConservationApresAnalyse) {
        this.temperatureConservationApresAnalyse = temperatureConservationApresAnalyse;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.codePro);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EchantillonsProduit other = (EchantillonsProduit) obj;
        return Objects.equals(this.codePro, other.codePro);
    }

    @Override
    public int compareTo(EchantillonsProduit o) {
        return getCode_unique().compareTo(o.getCode_unique());
    }

    public float getMasseavantenvoie() {
        return masseavantenvoie;
    }

    public void setMasseavantenvoie(float masseavantenvoie) {
        this.masseavantenvoie = masseavantenvoie;
    }

}
