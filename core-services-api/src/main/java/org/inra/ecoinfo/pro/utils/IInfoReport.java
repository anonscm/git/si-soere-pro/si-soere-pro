/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

import java.io.Serializable;

/**
 *
 * @author adiankha
 */
public interface IInfoReport extends Serializable{
    
    /**
     *
     * @param infoMessage
     */
    void addInfoMessage(String infoMessage);

    /**
     *
     * @return
     */
    String getInfoMessages();

    /**
     *
     * @return
     */
    boolean hasInfo(); 
}
