/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methode;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Methode.NAME_ENTITY_JPA)
public class Methode implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "methode";

    /**
     *
     */
    public static final String JPA_ID = "methode_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "methode_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "methode_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE = "methode_mycode";

    /**
     *
     */
    public static final String JPA_COLUMN_PRETRAITEMENT = "pretraittement";

    /**
     *
     */
    public static final String JPA_COLUMN_SOLUTION = "mise_ensolution";

    /**
     *
     */
    public static final String JPA_COLUMN_REFERENCE_DONSOL = "reference_donsol";

    /**
     *
     */
    public static final String JPA_COLUMN_DOSAGE ="dosage";

    /**
     *
     */
    public static final String JPA_COLUMN_BIBLIO = "reference_bibliographique";

    /**
     *
     */
    public static final String JPA_COLUMN_NORME = "methode_norme";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    
    @Id
    @Column(name = Methode.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long methode_id;
    @Column(name=Methode.JPA_COLUMN_CODE, nullable = false,unique = true,length = 50)
    private String methode_code="";
    @Column(name=Methode.JPA_COLUMN_NAME,length = 510)
    private String methode_nom="";
    @Column(name=Methode.JPA_COLUMN_MYCODE)
    private String methode_mycode="";
    @Column(name=Methode.JPA_COLUMN_PRETRAITEMENT)
    private String pretraittement="";
    @Column(name=Methode.JPA_COLUMN_SOLUTION)
    private String mise_ensolution="";
    @Column(name=Methode.JPA_COLUMN_REFERENCE_DONSOL)
    private String reference_donsol="";
    @Column(name=Methode.JPA_COLUMN_DOSAGE)
    private String dosage="";
    @Column(name=Methode.JPA_COLUMN_BIBLIO)
    private String reference_bibliographique="";
    @Column(name=Methode.JPA_COLUMN_NORME)
    private String methode_norme="";
    @Column(name=Methode.JPA_COLUMN_COMENT)
    private String commentaire="";
    
    /**
     *
     */
    public Methode(){
        super();
    }
     
    /**
     *
     * @param code
     * @param nom
     * @param mycode
     * @param pretraitement
     * @param solution
     * @param donsol
     * @param dosage
     * @param biblio
     * @param norme
     * @param commentaire
     */
    public Methode (String code,String nom,String mycode,String pretraitement,String solution,String donsol,
            String dosage,String biblio,String norme,String commentaire){
        this.methode_code = code;
        this.methode_nom = nom;
        this.methode_mycode = mycode;
        this.pretraittement = pretraitement;
        this.mise_ensolution = solution;
        this.reference_donsol = donsol;
        this.dosage = dosage;
        this.reference_bibliographique = biblio;
        this.methode_norme = norme;
        this.commentaire = commentaire;
    }

    /**
     *
     * @param methode_id
     */
    public void setMethode_id(long methode_id) {
        this.methode_id = methode_id;
    }

    /**
     *
     * @param methode_code
     */
    public void setMethode_code(String methode_code) {
        this.methode_code = methode_code;
    }

    /**
     *
     * @param methode_nom
     */
    public void setMethode_nom(String methode_nom) {
        this.methode_nom = methode_nom;
    }

    /**
     *
     * @param methode_mycode
     */
    public void setMethode_mycode(String methode_mycode) {
        this.methode_mycode = methode_mycode;
    }

    /**
     *
     * @param pretraittement
     */
    public void setPretraittement(String pretraittement) {
        this.pretraittement = pretraittement;
    }

    /**
     *
     * @param mise_ensolution
     */
    public void setMise_ensolution(String mise_ensolution) {
        this.mise_ensolution = mise_ensolution;
    }

    /**
     *
     * @param reference_donsol
     */
    public void setReference_donsol(String reference_donsol) {
        this.reference_donsol = reference_donsol;
    }

    /**
     *
     * @param dosage
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    /**
     *
     * @param reference_bibliographique
     */
    public void setReference_bibliographique(String reference_bibliographique) {
        this.reference_bibliographique = reference_bibliographique;
    }

    /**
     *
     * @param methode_norme
     */
    public void setMethode_norme(String methode_norme) {
        this.methode_norme = methode_norme;
    }

    /**
     *
     * @return
     */
    public long getMethode_id() {
        return methode_id;
    }

    /**
     *
     * @return
     */
    public String getMethode_code() {
        return methode_code;
    }

    /**
     *
     * @return
     */
    public String getMethode_nom() {
        return methode_nom;
    }

    /**
     *
     * @return
     */
    public String getMethode_mycode() {
        return methode_mycode;
    }

    /**
     *
     * @return
     */
    public String getPretraittement() {
        return pretraittement;
    }

    /**
     *
     * @return
     */
    public String getMise_ensolution() {
        return mise_ensolution;
    }

    /**
     *
     * @return
     */
    public String getReference_donsol() {
        return reference_donsol;
    }

    /**
     *
     * @return
     */
    public String getDosage() {
        return dosage;
    }

    /**
     *
     * @return
     */
    public String getReference_bibliographique() {
        return reference_bibliographique;
    }

    /**
     *
     * @return
     */
    public String getMethode_norme() {
        return methode_norme;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
   
    
}
