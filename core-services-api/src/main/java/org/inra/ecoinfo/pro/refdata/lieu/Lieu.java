
package org.inra.ecoinfo.pro.refdata.lieu;

import java.io.Serializable;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.commune.Commune;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.StationExperimentale;
import org.inra.ecoinfo.pro.refdata.typelieu.TypeLieu;

/**
 * @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_LIEU}
 * @author ptcherniati
 */
@Entity
@Table(name = Lieu.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_LIEU}))
public class Lieu implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.LIEU_ID; // lie_id

    /**
     *
     */
    public static final Pattern REGEXP_PATTERN_NOM_LIEU_NOM_COMMUNE = Pattern.compile("\\s*(.*?)\\s*\\(\\s*(.*?)\\s*\\)\\s*");

    /**
     *
     */
    public static final String PATTERN_NOM_LIEU_NOM_COMMUNE = "%s (%s)";

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.LIEU_TABLE_NAME; // lieu

    /**
     *
     * @param nomCommune_codePostal
     * @return
     */
    public static String getNomLieu(String nomCommune_codePostal){
        Matcher matcher = REGEXP_PATTERN_NOM_LIEU_NOM_COMMUNE.matcher(nomCommune_codePostal);
        if(matcher.matches()){
            return matcher.group(1);
        }
        return nomCommune_codePostal.trim();
    }
    
    /**
     *
     * @param nomCommune_codePostal
     * @return
     */
    public static String getNomCommune(String nomCommune_codePostal){
        Matcher matcher = REGEXP_PATTERN_NOM_LIEU_NOM_COMMUNE.matcher(nomCommune_codePostal);
        if(matcher.matches()){
            return matcher.group(2);
        }
        return null;
    }
    
    @Id
    @Column(name = Lieu.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = TypeLieu.class)
    @JoinColumn(name = RefDataConstantes.TYPELIEU_ID, referencedColumnName = TypeLieu.ID_JPA, nullable = false)
    private TypeLieu typeLieu;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = StationExperimentale.class)
    @JoinColumn(name = RefDataConstantes.STATIONEXP_ID, referencedColumnName = StationExperimentale.ID_JPA, nullable = true)
    private StationExperimentale stationExperimentale;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Commune.class)
    @JoinColumn(name = RefDataConstantes.COMMUNE_ID, referencedColumnName = Commune.ID_JPA, nullable = true)
    private Commune commune;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Geolocalisation.class)
    @JoinColumn(name = RefDataConstantes.GEOLOC_ID, referencedColumnName = Geolocalisation.ID_JPA, nullable = true)
    private Geolocalisation geolocalisation;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_LIEU, length = 100)
    private String nom;
    
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_NUMCADASTRE_LIEU, length = 10)
    private String numeroCadastre;
    
    /**
     *
     */
    public Lieu() 
    {
        super();
    }
    
    /**
     *
     * @param typeLieu
     * @param stationExperimentale
     * @param commune
     * @param geolocalisation
     * @param nom
     * @param numeroCadastre
     */
    public Lieu(TypeLieu typeLieu, StationExperimentale stationExperimentale, Commune commune, Geolocalisation geolocalisation, String nom, String numeroCadastre) 
    {
        super();
        this.typeLieu = typeLieu;
        this.stationExperimentale = stationExperimentale;
        this.commune = commune;
        this.geolocalisation = geolocalisation;
        this.nom = nom;
        this.numeroCadastre = numeroCadastre;
    }
    
    /**
     *
     * @return
     */
    public Commune getCommune() {
        return commune;
    }
    
    /**
     *
     * @return
     */
    public Geolocalisation getGeolocalisation() {
        return geolocalisation;
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }
    
    /**
     *
     * @return
     */
    public String getNumeroCadastre() {
        return numeroCadastre;
    }

    /**
     *
     * @return
     */
    public StationExperimentale getStationExperimentale() {
        return stationExperimentale;
    }
    
    /**
     *
     * @return
     */
    public TypeLieu getTypeLieu() {
        return typeLieu;
    }
    
    /**
     *
     * @param commune
     */
    public void setCommune(Commune commune) {
        this.commune = commune;
    }
    
    /**
     *
     * @param geolocalisation
     */
    public void setGeolocalisation(Geolocalisation geolocalisation) {
        this.geolocalisation = geolocalisation;
    }
    
    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
     *
     * @param numeroCadastre
     */
    public void setNumeroCadastre(String numeroCadastre) {
        this.numeroCadastre = numeroCadastre;
    }
    
    /**
     *
     * @param stationExperimentale
     */
    public void setStationExperimentale(StationExperimentale stationExperimentale) {
        this.stationExperimentale = stationExperimentale;
    }
    
    /**
     *
     * @param typeLieu
     */
    public void setTypeLieu(TypeLieu typeLieu) {
        this.typeLieu = typeLieu;
    }

    /**
     *
     * @return
     */
    public String getNomLieu_nomCommune(){
        return String.format(PATTERN_NOM_LIEU_NOM_COMMUNE, nom, Optional.ofNullable(commune).map(Commune::getNomCommune).orElse(null));
    }
}
