/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonplante;

import java.io.Serializable;
import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.lavage.Lavage;
import org.inra.ecoinfo.pro.refdata.prelevementplante.PrelevementPlante;
import org.inra.ecoinfo.pro.refdata.typeechantillon.TypeEchantillon;
import org.inra.ecoinfo.utils.Utils;

/**
 * @UniqueConstraint(columnNames = { PrelevementPlante.ID_JPA,
 * EchantillonPlante.JPA_COLUMN_NUM_ECHANTILLON }
 * @author adiankha
 */
@Entity
@Table(name = EchantillonPlante.JPA_NAME_ENTITY, uniqueConstraints = @UniqueConstraint(columnNames = {
    PrelevementPlante.ID_JPA,
    EchantillonPlante.JPA_COLUMN_NUM_ECHANTILLON
}))
public class EchantillonPlante implements Serializable, Comparable<EchantillonPlante> {

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "echantillonplante";

    /**
     *
     */
    public static final String ID_JPA = "ep_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_ECHANTILLON = "codelabo";

    /**
     *
     */
    public static final String JPA_COLUMN_TEMP_CONSER = "tempconseravant";

    /**
     *
     */
    public static final String JPA_COLUMN_NUM_ECHANTILLON = "numeroechantillon";

    /**
     *
     */
    public static final String JPA_COLUMN_BROYAGE = "broyage";

    /**
     *
     */
    public static final String JPA_COLUMN_SECHAGE = "sechage";

    /**
     *
     */
    public static final String JPA_COLUMN_HOMOGENEISATION = "homageneisation";

    /**
     *
     */
    public static final String JPA_COLUMN_CONSERVATION_APRES_ANALYSE = "tempconserapres";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_ECHANTILLON_UNIQUE = "codeeplante";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE_ECH_SOL = "codeplante";

    /**
     *
     */
    public static final String JPA_COLUMN_MASSE_ECHANTILLON = "masseprelevee";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_CONAPRES_ANA = "conserapresanalyse";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = EchantillonPlante.ID_JPA)
    private long ep_id = 0;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = PrelevementPlante.ID_JPA, nullable = false, unique = false)
    private PrelevementPlante prelevementplante;
    @Column(name = EchantillonPlante.JPA_COLUMN_NUM_ECHANTILLON, nullable = false, unique = false)
    private long numeroechantillon;
    @Column(name = EchantillonPlante.JPA_COLUMN_CODE_ECHANTILLON_UNIQUE, unique = true, nullable = false)
    private String codeeplante = "";
    @Column(name = EchantillonPlante.JPA_COLUMN_MYCODE_ECH_SOL, unique = true, nullable = false)
    private String codeplante = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeEchantillon.JPA_ID, nullable = false, unique = false)
    private TypeEchantillon typeEchantillon;
    @Column(name = EchantillonPlante.JPA_COLUMN_CODE_ECHANTILLON, nullable = true, unique = false)
    private String codelabo = null;
    @Column(name = EchantillonPlante.JPA_COLUMN_HOMOGENEISATION, nullable = false, unique = false)
    private String homogeneisation;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Lavage.ID_JPA, nullable = false, unique = false)
    private Lavage lavage;
    @Column(name = EchantillonPlante.JPA_COLUMN_SECHAGE)
    private float sechage = 0;
    @Column(name = EchantillonPlante.JPA_COLUMN_BROYAGE)
    private float broyage = 0;
    @Column(name = EchantillonPlante.JPA_COLUMN_TEMP_CONSER)
    private float tempconseravant = 0;
    @Column(name = EchantillonPlante.JPA_COLUMN_CONAPRES_ANA)
    private String conserapresanalyse = null;
    @Column(name = EchantillonPlante.JPA_COLUMN_CONSERVATION_APRES_ANALYSE)
    private float tempconserapres = 0;
    @Column(name = EchantillonPlante.JPA_COLUMN_MASSE_ECHANTILLON, nullable = true, unique = false)
    private float masseavantenvoie = 0;
    @Column(name = EchantillonPlante.JPA_COLUMN_COMENT)
    private String commentaire = "";


    /**
     *
     */
    public EchantillonPlante() {
    }

    /**
     *
     * @param codelabo
     * @param tempconseravant
     * @param sechage
     * @param broyage
     * @param caanalyse
     * @param ordreechan
     * @param commentaire
     * @param codeunique
     * @param codepro
     * @param homogeneisation
     * @param numeroechantillon
     * @param prelevementplante
     * @param lavage
     * @param typeEchantillon
     * @param tcapresa
     */
    public EchantillonPlante(PrelevementPlante prelevementplante, long numeroechantillon, String codepro,  TypeEchantillon typeEchantillon,
            String codelabo, String homogeneisation, Lavage lavage, float sechage, float broyage, float tempconseravant, 
            String caanalyse,  float tcapresa, float masseavantenvoie, String commentaire) {
        this.prelevementplante = prelevementplante;
        this.numeroechantillon = numeroechantillon;
        this.codeplante = codepro;
        this.codeeplante = Utils.createCodeFromString(codepro);
        this.typeEchantillon = typeEchantillon;
        this.codelabo = codelabo;
        this.homogeneisation = homogeneisation;
        this.lavage = lavage;
        this.sechage = sechage;
        this.broyage = broyage;
        this.tempconseravant = tempconseravant;
        this.conserapresanalyse = caanalyse;
        this.tempconserapres = tcapresa;
        this.masseavantenvoie = masseavantenvoie;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getEp_id() {
        return ep_id;
    }

    /**
     *
     * @param ep_id
     */
    public void setEp_id(long ep_id) {
        this.ep_id = ep_id;
    }

    /**
     *
     * @return
     */
    public String getCodelabo() {
        return codelabo;
    }

    /**
     *
     * @param codelabo
     */
    public void setCodelabo(String codelabo) {
        this.codelabo = codelabo;
    }

    /**
     *
     * @return
     */
    public float getSechage() {
        return sechage;
    }

    /**
     *
     * @param sechage
     */
    public void setSechage(float sechage) {
        this.sechage = sechage;
    }

    /**
     *
     * @return
     */
    public String getHomogeneisation() {
        return homogeneisation;
    }

    /**
     *
     * @param homogeneisation
     */
    public void setHomogeneisation(String homogeneisation) {
        this.homogeneisation = homogeneisation;
    }

    /**
     *
     * @return
     */
    public float getBroyage() {
        return broyage;
    }

    /**
     *
     * @param broyage
     */
    public void setBroyage(float broyage) {
        this.broyage = broyage;
    }

    /**
     *
     * @return
     */
    public long getNumeroechantillon() {
        return numeroechantillon;
    }

    /**
     *
     * @param numeroechantillon
     */
    public void setNumeroechantillon(long numeroechantillon) {
        this.numeroechantillon = numeroechantillon;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public PrelevementPlante getPrelevementplante() {
        return prelevementplante;
    }

    /**
     *
     * @param prelevementplante
     */
    public void setPrelevementplante(PrelevementPlante prelevementplante) {
        this.prelevementplante = prelevementplante;
    }

    /**
     *
     * @return
     */
    public Lavage getLavage() {
        return lavage;
    }

    /**
     *
     * @param lavage
     */
    public void setLavage(Lavage lavage) {
        this.lavage = lavage;
    }

    /**
     *
     * @return
     */
    public TypeEchantillon getTypeEchantillon() {
        return typeEchantillon;
    }

    /**
     *
     * @param typeEchantillon
     */
    public void setTypeEchantillon(TypeEchantillon typeEchantillon) {
        this.typeEchantillon = typeEchantillon;
    }

    /**
     *
     * @return
     */
    public float getTempconseravant() {
        return tempconseravant;
    }

    /**
     *
     * @param tempconseravant
     */
    public void setTempconseravant(float tempconseravant) {
        this.tempconseravant = tempconseravant;
    }

    /**
     *
     * @return
     */
    public float getTempconserapres() {
        return tempconserapres;
    }

    /**
     *
     * @param tempconserapres
     */
    public void setTempconserapres(float tempconserapres) {
        this.tempconserapres = tempconserapres;
    }

    /**
     *
     * @return
     */
    public String getConserapresanalyse() {
        return conserapresanalyse;
    }

    /**
     *
     * @param conserapresanalyse
     */
    public void setConserapresanalyse(String conserapresanalyse) {
        this.conserapresanalyse = conserapresanalyse;
    }

    /**
     *
     * @return
     */
    public String getCodeeplante() {
        return codeeplante;
    }

    /**
     *
     * @param codeeplante
     */
    public void setCodeeplante(String codeeplante) {
        this.codeeplante = codeeplante;
    }

    /**
     *
     * @return
     */
    public String getCodeplante() {
        return codeplante;
    }

    /**
     *
     * @param codeplante
     */
    public void setCodeplante(String codeplante) {
        this.codeplante = codeplante;
    }

    @Override
    public int compareTo(EchantillonPlante o) {
        return getCodeplante().compareTo(o.getCodeplante());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.codeplante);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EchantillonPlante other = (EchantillonPlante) obj;
        return Objects.equals(this.codeeplante, other.codeplante);
    }

    public float getMasseavantenvoie() {
        return masseavantenvoie;
    }

    public void setMasseavantenvoie(float masseavantenvoie) {
        this.masseavantenvoie = masseavantenvoie;
    }

}
