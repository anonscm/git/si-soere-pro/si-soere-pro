/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.partieprelevee;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.inra.ecoinfo.utils.Utils;


/**
 *
 * @author adiankha
 */
@Entity
@Table(name = PartiePrelevee.NAME_ENTITY_JPA)
public class PartiePrelevee implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "partieprelevee";

    /**
     *
     */
    public static final String ID_JPA = "pp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NOM = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_UTILISATEUR = "code_util";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_UTILISATEUR_MY_CODE = "code_util_my_code";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PartiePrelevee.ID_JPA, unique = true, nullable = false, updatable = false)
    private long pp_id = 0;
    @Size(min = 1, max = 80)
    @Column(name = PartiePrelevee.JPA_COLUMN_NOM, unique = true, nullable = false, length = 80)
    private String nom = "";
    @Column(name = PartiePrelevee.JPA_COLUMN_CODE, unique = true, nullable = false)
    private String code = "";
    @Column(name = PartiePrelevee.JPA_COLUMN_CODE_UTILISATEUR, unique = true, nullable = false)
    private String code_util = "";
    @Column(name = PartiePrelevee.JPA_COLUMN_CODE_UTILISATEUR_MY_CODE, unique = true, nullable = false)
    private String code_util_mycode = "";
    @Column(name=PartiePrelevee.JPA_COLUMN_COMMENT)
    private String commentaire="";

    /**
     *
     */
    public PartiePrelevee() {
        super();
    }

    /**
     *
     * @param code
     * @param nom
     * @param code_util
     * @param commentaire
     */
    public PartiePrelevee(String code, String nom,String code_util, String commentaire) {
        this.code = code;
        this.nom = nom;
        this.code_util= code_util;
        this.code_util_mycode= Utils.createCodeFromString(code_util);
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public long getPp_id() {
        return pp_id;
    }

    /**
     *
     * @param pp_id
     */
    public void setPp_id(long pp_id) {
        this.pp_id = pp_id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getCode_util() {
        return code_util;
    }

    /**
     *
     * @param code_util
     */
    public void setCode_util(String code_util) {
        this.code_util = code_util;
    }

    public String getCode_util_mycode() {
        return code_util_mycode;
    }

    public void setCode_util_mycode(String code_util_mycode) {
        this.code_util_mycode = code_util_mycode;
    }
    
    
    
    
    
}
