/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

import java.util.Collection;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author ptcherniati
 */
public interface IDispositifManager {

    /**
     *
     * @return
     */
    Collection<Dispositif> getAvailablesDispositifs();
}
