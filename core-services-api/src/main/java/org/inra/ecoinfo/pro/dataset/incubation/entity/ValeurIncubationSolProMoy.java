/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurIncubationSolProMoy.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurIncubationSolProMoy.ATTRIBUTE_JPA_ECART_TYPE,
    RealNode.ID_JPA,
    MesureIncubationSolProMoy.ID_JPA
}
),
        indexes = {
            @Index(name = "valeur_incub_mesure_sol_pro_moy_idx", columnList = MesureIncubationSolProMoy.ID_JPA),
            @Index(name = "valeur_incub_variable_sol_pro_moy", columnList = RealNode.ID_JPA)
        }
)
@PrimaryKeyJoinColumn(name = ValeurIncubation.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurIncubationSolProMoy.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurIncubation.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurIncubationSolProMoy.ID_JPA)),
    @AttributeOverride(name = ValeurIncubation.ATTRIBUTE_JPA_QUANTITE, column = @Column(name = ValeurIncubationSolProMoy.COLUMN_JPA_VALUE)),})
public class ValeurIncubationSolProMoy extends ValeurIncubation<MesureIncubationSolProMoy, ValeurIncubationSolProMoy> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurincubationsolpromoy";

    /**
     *
     */
    public static final String ID_JPA = "vispro_moy_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur_moyenne";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ECART_TYPE = "ecart_type";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";

    @Column(name = ValeurIncubationSolProMoy.ATTRIBUTE_JPA_ECART_TYPE)
    private Float ecart_type;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureIncubationSolProMoy.class)
    @JoinColumn(name = MesureIncubationSolProMoy.ID_JPA, referencedColumnName = MesureIncubationSolProMoy.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureIncubationSolProMoy mesureIncubationSolProMoy;

    /**
     *
     */
    public ValeurIncubationSolProMoy() {
        super();
    }

    /**
     *
     * @param valeur_moyenne
     * @param ecart_type
     * @param mesureIncubationSolProMoy
     * @param realNode
     */
    public ValeurIncubationSolProMoy(Float valeur_moyenne, Float ecart_type, MesureIncubationSolProMoy mesureIncubationSolProMoy, RealNode realNode) {
        super(valeur_moyenne, realNode);
        this.ecart_type = ecart_type;
        this.mesureIncubationSolProMoy = mesureIncubationSolProMoy;
    }

    /**
     *
     * @return
     */
    public Float getEcart_type() {
        return ecart_type;
    }

    /**
     *
     * @param ecart_type
     */
    public void setEcart_type(Float ecart_type) {
        this.ecart_type = ecart_type;
    }

    /**
     *
     * @return
     */
    public MesureIncubationSolProMoy getMesureIncubationSolProMoy() {
        return mesureIncubationSolProMoy;
    }

    /**
     *
     * @param mesureIncubationSolProMoy
     */
    public void setMesureIncubationSolProMoy(MesureIncubationSolProMoy mesureIncubationSolProMoy) {
        this.mesureIncubationSolProMoy = mesureIncubationSolProMoy;
    }
}
