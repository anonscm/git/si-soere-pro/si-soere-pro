package org.inra.ecoinfo.pro.refdata.prelevementsol;

import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.horizon.Horizon;
import org.inra.ecoinfo.pro.refdata.localisationprelevement.LocalisationPrelevement;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;

/*
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 *    PrelevementSol.JPA_COLUMN_DATE_PRELEVEMENT,
 *    PrelevementSol.JPA_COLUMN_LIMIT_INF,
 *    PrelevementSol.JPA_COLUMN_LIMIT_SUP,
 *    Dispositif.ID_JPA,
 *    DescriptionTraitement.ID_JPA,
 *    Bloc.ID_JPA,
 *    ParcelleElementaire.ID_JPA,
 *    Placette.ID_JPA
 *
 * @author Vivianne
 */

/**
 *
 * @author ptcherniati
 */

@Entity
@Table(name = PrelevementSol.JPA_NAME_ENTITY, uniqueConstraints = @UniqueConstraint(columnNames = {
    PrelevementSol.JPA_COLUMN_DATE_PRELEVEMENT,
    Dispositif.ID_JPA,
    DescriptionTraitement.ID_JPA,
    Bloc.ID_JPA,
    ParcelleElementaire.ID_JPA,
    Placette.ID_JPA,
    PrelevementSol.JPA_COLUMN_LIMIT_INF,
    PrelevementSol.JPA_COLUMN_LIMIT_SUP

}))
public class PrelevementSol implements Serializable {

    /**
     *
     */
    public static final String UNDERSCORE = "_";

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "prelevement_sol";

    /**
     *
     */
    public static final String ID_JPA = "prelevement_id";

    /**
     *
     */
    public static final String JPA_COLUMN_DATE_PRELEVEMENT = "date_prelevement";

    /**
     *
     */
    public static final String JPA_COLUMN_OUTIL_PRELEVEMENT = "outils_prelevement";

    /**
     *
     */
    public static final String JPA_COLUMN_MASSE_GEOREF = "referencement";

    /**
     *
     */
    public static final String JPA_COLUMN_LIMIT_SUP = "limit_superieur";

    /**
     *
     */
    public static final String JPA_COLUMN_LIMIT_INF = "limit_inferieur";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_SOL = "codesol";

    /**
     *
     */
    public static final String JPA_COLUMN_LOCALISATION = "localisation";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENTAIRE = "commentaire";

    /**
     *
     * @return
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     *
     * @param date
     * @param codeDispositif
     * @param codetrait
     * @param bloc
     * @param pelementaire
     * @param placette
     * @param ls
     * @param li
     * @return
     */
    public static String buildCodePrelevement(String date, String codeDispositif, String nomLieu, final String codetrait, String bloc, String pelementaire, String placette, int ls, int li) {
        // creation du code prélevement sol
        StringBuilder codeprel = new StringBuilder();
        String mega = "_";
        date = DateUtil.getUTCDateTextFromLocalDateTime(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, date), "dd_MM_yyyy");
        codeprel.append(date).append(UNDERSCORE)
                .append(codeDispositif).append(UNDERSCORE)
                .append(nomLieu).append(UNDERSCORE)
                .append(codetrait).append(UNDERSCORE)
                .append(bloc).append(UNDERSCORE)
                .append(pelementaire).append(UNDERSCORE)
                .append(placette).append(UNDERSCORE)
                .append(ls).append(UNDERSCORE)
                .append(li);
        return Utils.createCodeFromString(codeprel.toString().replaceAll("_sans(?=[_$])", ""));
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PrelevementSol.ID_JPA, nullable = false, updatable = false, unique = true)
    private long prelevement_id;
    @Column(name = PrelevementSol.JPA_COLUMN_DATE_PRELEVEMENT, nullable = false, unique = false)
    private LocalDate date_prelevement;
    @Column(name = PrelevementSol.JPA_COLUMN_OUTIL_PRELEVEMENT, nullable = true, unique = false)
    private String outils_prelevement;
    @Column(name = PrelevementSol.JPA_COLUMN_MASSE_GEOREF, nullable = true, unique = false)
    private String referencement = null;
    @Column(name = PrelevementSol.JPA_COLUMN_LIMIT_SUP, nullable = false, unique = false)
    private int limit_superieur = 0;
    @Column(name = PrelevementSol.JPA_COLUMN_LIMIT_INF, nullable = false, unique = false)
    private int limit_inferieur = 0;
    @Column(name = PrelevementSol.JPA_COLUMN_CODE_SOL, nullable = false, unique = true)
    private String codesol = "";
    @Column(name = PrelevementSol.JPA_COLUMN_COMMENTAIRE, length = 300)
    private String commentaire = "";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, nullable = false, unique = false)
    private DescriptionTraitement traitement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Bloc.ID_JPA, nullable = true, unique = false)
    private Bloc bloc;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false, unique = false)
    private Dispositif dispositif;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = LocalisationPrelevement.ID_JPA, nullable = true, unique = false)
    private LocalisationPrelevement localisationprelevement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Horizon.ID_JPA, nullable = true, unique = false)
    private Horizon horizon;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = ParcelleElementaire.ID_JPA, nullable = true, unique = false)
    private ParcelleElementaire pelementaire;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Placette.ID_JPA, nullable = true, unique = false)
    private Placette placette;

    /**
     *
     */
    public PrelevementSol() {
        super();
    }

    /**
     *
     * @param date_prelevement
     * @param outils_prelevement
     * @param point
     * @param limit_superieur
     * @param limit_inferieur
     * @param codesol
     * @param commentaire
     * @param traitement
     * @param dispositif
     * @param localisation
     * @param horizon
     * @param bloc
     * @param pelementaire
     * @param placette
     */
    public PrelevementSol(LocalDate date_prelevement,
            String outils_prelevement, String point, int limit_superieur,
            int limit_inferieur, String codesol,
            String commentaire, DescriptionTraitement traitement,
            Dispositif dispositif, LocalisationPrelevement localisation, Horizon horizon, Bloc bloc, ParcelleElementaire pelementaire, Placette placette
    ) {
        this.date_prelevement = date_prelevement;
        this.outils_prelevement = outils_prelevement;
        this.referencement = point;
        this.limit_superieur = limit_superieur;
        this.limit_inferieur = limit_inferieur;
        this.codesol = codesol;
        this.commentaire = commentaire;
        this.traitement = traitement;
        this.dispositif = dispositif;
        this.horizon = horizon;
        this.bloc = bloc;
        this.pelementaire = pelementaire;
        this.placette = placette;
        this.localisationprelevement = localisation;

    }

    /**
     *
     * @param prelevement_id
     */
    public void setPrelevement_id(long prelevement_id) {
        this.prelevement_id = prelevement_id;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_prelevement() {
        return date_prelevement;
    }

    /**
     *
     * @param date_prelevement
     */
    public void setDate_prelevement(LocalDate date_prelevement) {
        this.date_prelevement = date_prelevement;
    }

    /**
     *
     * @return
     */
    public String getOutils_prelevement() {
        return outils_prelevement;
    }

    /**
     *
     * @param outils_prelevement
     */
    public void setOutils_prelevement(String outils_prelevement) {
        this.outils_prelevement = outils_prelevement;
    }

    /**
     *
     * @return
     */
    public String getReferencement() {
        return referencement;
    }

    /**
     *
     * @param referencement
     */
    public void setReferencement(String referencement) {
        this.referencement = referencement;
    }

    /**
     *
     * @return
     */
    public int getLimit_superieur() {
        return limit_superieur;
    }

    /**
     *
     * @param limit_superieur
     */
    public void setLimit_superieur(int limit_superieur) {
        this.limit_superieur = limit_superieur;
    }

    /**
     *
     * @return
     */
    public int getLimit_inferieur() {
        return limit_inferieur;
    }

    /**
     *
     * @param limit_inferieur
     */
    public void setLimit_inferieur(int limit_inferieur) {
        this.limit_inferieur = limit_inferieur;
    }

    /**
     *
     * @return
     */
    public long getPrelevement_id() {
        return prelevement_id;
    }

    /**
     *
     * @return
     */
    public Bloc getBloc() {
        return bloc;
    }

    /**
     *
     * @param bloc
     */
    public void setBloc(Bloc bloc) {
        this.bloc = bloc;
    }

    /**
     *
     * @return
     */
    public LocalisationPrelevement getLocalisationprelevement() {
        return localisationprelevement;
    }

    /**
     *
     * @param localisationprelevement
     */
    public void setLocalisationprelevement(LocalisationPrelevement localisationprelevement) {
        this.localisationprelevement = localisationprelevement;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @return
     */
    public Horizon getHorizon() {
        return horizon;
    }

    /**
     *
     * @param horizon
     */
    public void setHorizon(Horizon horizon) {
        this.horizon = horizon;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public ParcelleElementaire getPelementaire() {
        return pelementaire;
    }

    /**
     *
     * @return
     */
    public Placette getPlacette() {
        return placette;
    }

    /**
     *
     * @param pelementaire
     */
    public void setPelementaire(ParcelleElementaire pelementaire) {
        this.pelementaire = pelementaire;
    }

    /**
     *
     * @param placette
     */
    public void setPlacette(Placette placette) {
        this.placette = placette;
    }

    /**
     *
     * @return
     */
    public DescriptionTraitement getTraitement() {
        return traitement;
    }

    /**
     *
     * @param traitement
     */
    public void setTraitement(DescriptionTraitement traitement) {
        this.traitement = traitement;
    }

    /**
     *
     * @return
     */
    public String getCodesol() {
        return codesol;
    }

    /**
     *
     * @param codesol
     */
    public void setCodesol(String codesol) {
        this.codesol = codesol;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }


}
