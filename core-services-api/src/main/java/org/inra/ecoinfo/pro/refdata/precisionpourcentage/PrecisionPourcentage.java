/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpourcentage;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=PrecisionPourcentage.NAME_ENTITY_JPA)
public class PrecisionPourcentage implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "precisionpourcentage";

    /**
     *
     */
    public static final String JPA_ID = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "libelle";
    
    @Id
    @Column(name = PrecisionPourcentage.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = PrecisionPourcentage.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String code = "";
    @Column(name = PrecisionPourcentage.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String libelle = "";
    
    /**
     *
     */
    public PrecisionPourcentage(){
        super();
    }
    
    /**
     *
     * @param libelle
     */
    public PrecisionPourcentage (String libelle){
        this.code = Utils.createCodeFromString(libelle);
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    } 
    
}
