/**
 *
 */
package org.inra.ecoinfo.pro.refdata.descriptiontraitement;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.typetraitement.TypeTraitement;
import org.inra.ecoinfo.utils.Utils;

/**
 * @UniqueConstraint(columnNames = { Dispositif.ID_JPA,
 * RefDataConstantes.COLUMN_CODE_DESCRPTTRT})
 * @author sophie
 *
 */
@Entity
@Table(name = DescriptionTraitement.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(columnNames = {Dispositif.ID_JPA, RefDataConstantes.COLUMN_CODE_DESCRPTTRT}),
    @UniqueConstraint(columnNames = {Dispositif.ID_JPA, RefDataConstantes.COLUMN_TRTORIGINE_DESCRPTTRT})
})
public class DescriptionTraitement implements Serializable, Comparable<DescriptionTraitement> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.DESCRIPTTRAITEMENT_ID; // dctrt_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.DESCRIPTTRAITEMENT_TABLE_NAME; // description_traitement

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "codeunique";

    /**
     *
     * @param codeLieuDispositif
     * @param codeTraitement
     * @return
     */
    public static String getCodeUnique(String codeLieuDispositif, String codeTraitement) {
        return Utils.createCodeFromString(codeLieuDispositif + "_" + codeTraitement);
    }

    @Id
    @Column(name = DescriptionTraitement.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = TypeTraitement.class)
    @JoinColumn(name = RefDataConstantes.TYPETRAITEMENT_ID, referencedColumnName = TypeTraitement.ID_JPA, nullable = false)
    private TypeTraitement typeTraitement;

    @Column(name = DescriptionTraitement.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String codeunique = "";

    @OneToOne(mappedBy = "traitementOrigine")
    private DescriptionTraitement traitementModifie;

    @OneToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true)
    // {CascadeType.ALL}
    @JoinColumn(name = RefDataConstantes.COLUMN_TRTORIGINE_DESCRPTTRT, referencedColumnName = DescriptionTraitement.ID_JPA, nullable = true, unique = true)
    private DescriptionTraitement traitementOrigine; // 0 ou 1 père

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_DESCRPTTRT, length = 500)
    private String nom;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_CODE_DESCRPTTRT, length = 30)
    private String code;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NBREREP_DESCRPTTRT)
    private Integer nbreRepetition;

    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_ANNEEDEBVAL_DESCRPTTRT, length = 4)
    private String anneeDebutValidite;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEFINVAL_DESCRPTTRT, length = 4)
    private String anneeFinValidite;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMMENT_DESCRPTTRT)
    @Lob
    private String commentaire;

    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ANNEEMODIFMIN_DESCRPTTRT, length = 53)
    private String anneesModifMineure; // "aaaa - aaaa - aaaa ..."

    /**
     * empty constructor
     */
    public DescriptionTraitement() {
        super();
    }

    /**
     * @param dispositif
     * @param typeTraitement
     * @param traitementOrigine
     * @param nom
     * @param code
     * @param nbreRepetition
     * @param anneeDebutValidite
     * @param anneeFinValidite
     * @param commentaire
     * @param anneesModifMineure
     */
    public DescriptionTraitement(Dispositif dispositif, TypeTraitement typeTraitement, DescriptionTraitement traitementOrigine, String nom, String code, Integer nbreRepetition, String anneeDebutValidite, String anneeFinValidite, String commentaire,
            String anneesModifMineure) {
        super();
        this.dispositif = dispositif;
        this.typeTraitement = typeTraitement;
        this.traitementOrigine = traitementOrigine;
        this.nom = nom;
        this.code = code;
        this.nbreRepetition = nbreRepetition;
        this.anneeDebutValidite = anneeDebutValidite;
        this.anneeFinValidite = anneeFinValidite;
        this.commentaire = commentaire;
        this.anneesModifMineure = anneesModifMineure;
    }

    /**
     * @return the id
     */
    /*
     * public Long getId() { return id; }
     */
    /**
     * @param id the id to set
     */
    /*
     * public void setId(Long id) { this.id = id; }
     */
    /**
     * @return the anneeDebutValidite
     */
    public String getAnneeDebutValidite() {
        return anneeDebutValidite;
    }

    /**
     * @return the anneeFinValidite
     */
    public String getAnneeFinValidite() {
        return anneeFinValidite;
    }

    /**
     * @return the anneesModifMineure
     */
    public String getAnneesModifMineure() {
        return anneesModifMineure;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the commentaire
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     * @return the dispositif
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     * @return the nbreRepetition
     */
    public Integer getNbreRepetition() {
        return nbreRepetition;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return the traitementOrigine
     */
    public DescriptionTraitement getTraitementOrigine() {
        return traitementOrigine;
    }

    /**
     * @return the typeTraitement
     */
    public TypeTraitement getTypeTraitement() {
        return typeTraitement;
    }

    /**
     * @param anneeDebutValidite the anneeDebutValidite to set
     */
    public void setAnneeDebutValidite(String anneeDebutValidite) {
        this.anneeDebutValidite = anneeDebutValidite;
    }

    /**
     * @param anneeFinValidite the anneeFinValidite to set
     */
    public void setAnneeFinValidite(String anneeFinValidite) {
        this.anneeFinValidite = anneeFinValidite;
    }

    /**
     * @param anneesModifMineure the anneesModifMineure to set
     */
    public void setAnneesModifMineure(String anneesModifMineure) {
        this.anneesModifMineure = anneesModifMineure;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * @param dispositif the dispositif to set
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     * @param nbreRepetition the nbreRepetition to set
     */
    public void setNbreRepetition(Integer nbreRepetition) {
        this.nbreRepetition = nbreRepetition;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @param traitementOrigine the traitementOrigine to set
     */
    public void setTraitementOrigine(DescriptionTraitement traitementOrigine) {
        this.traitementOrigine = traitementOrigine;
    }

    /**
     * @param typeTraitement the typeTraitement to set
     */
    public void setTypeTraitement(TypeTraitement typeTraitement) {
        this.typeTraitement = typeTraitement;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public DescriptionTraitement getTraitementModifie() {
        return traitementModifie;
    }

    /**
     *
     * @param traitementModifie
     */
    public void setTraitementModifie(DescriptionTraitement traitementModifie) {
        this.traitementModifie = traitementModifie;
    }

    /**
     *
     * @return
     */
    public String getCodeunique() {
        return codeunique;
    }

    /**
     *
     * @param codeunique
     */
    public void setCodeunique(String codeunique) {
        this.codeunique = codeunique;
    }

    @Override
    public int compareTo(DescriptionTraitement o) {
        return getCode().compareTo(o.getCode());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DescriptionTraitement other = (DescriptionTraitement) obj;
        return Objects.equals(this.code, other.code);
    }
}
