/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.fluxchambres.entity;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurFluxChambres.JPA_NAME_TABLE,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    ValeurFluxChambres.ATTRIBUTE_JPA_STATUT_VALEUR,
    RealNode.ID_JPA,
    MesureFluxChambres.ID_JPA
}),
        indexes = {
            @Index(name = "mesure_fc_idx", columnList = MesureFluxChambres.ID_JPA),
            @Index(name = "dvu_idx", columnList = RealNode.ID_JPA)
        })
public class ValeurFluxChambres implements Serializable {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurfluxchambres";

    /**
     *
     */
    public static final String ID_JPA = "vfc_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s/%s/%s";

    @Id
    @Column(name = ValeurFluxChambres.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long vpc_id;

    @Column(name = ValeurFluxChambres.COLUMN_JPA_VALUE, nullable = false)
    private Float valeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureFluxChambres.class)
    @JoinColumn(name = MesureFluxChambres.ID_JPA, referencedColumnName = MesureFluxChambres.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureFluxChambres mesureFluxChambres;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private RealNode realNode;

    @Column(name = ValeurFluxChambres.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;

    /**
     *
     */
    public ValeurFluxChambres() {
        super();
    }

//    public ValeurFluxChambres( Float valeur, MesureFluxChambres mesureFluxChambres, VariablesPRO variablePro) {
//        this.valeur = valeur;
//        this.mesureFluxChambres = mesureFluxChambres;
//        this.variablePro = variablePro;
//    }

    /**
     *
     * @param valeur
     * @param mesureFluxChambres
     * @param realNode
     * @param statutvaleur
     */
    public ValeurFluxChambres(Float valeur, MesureFluxChambres mesureFluxChambres, RealNode realNode, String statutvaleur) {
        this.valeur = valeur;
        this.mesureFluxChambres = mesureFluxChambres;
        this.realNode = realNode;
        this.statutvaleur = statutvaleur;
    }
    
    /**
     *
     * @return
     */
    public Long getVpc_id() {
        return vpc_id;
    }

    /**
     *
     * @param vpc_id
     */
    public void setVpc_id(Long vpc_id) {
        this.vpc_id = vpc_id;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public MesureFluxChambres getMesureFluxChambres() {
        return mesureFluxChambres;
    }

    /**
     *
     * @param mesureFluxChambres
     */
    public void setMesureFluxChambres(MesureFluxChambres mesureFluxChambres) {
        this.mesureFluxChambres = mesureFluxChambres;
    }

//    public VariablesPRO getVariablePro() {
//        return variablePro;
//    }
//
//    public void setVariablePro(VariablesPRO variablePro) {
//        this.variablePro = variablePro;
//    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

}
