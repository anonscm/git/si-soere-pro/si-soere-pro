/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureSMP.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    SousSequenceSMP.ATTRIBUTE_JPA_ID,
    MesureSMP.ATTRIBUTE_JPA_TIME}))

public class MesureSMP implements Serializable {

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "id_mesure_smp";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_TIME = "heure";

    /**
     *
     */
    public static final String TABLE_NAME = "mesure_smp";

    static final long serialVersionUID = 1L;

    @Id
    @Column(name = MesureSMP.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id_mesure_smp;

    @Column(name = MesureSMP.ATTRIBUTE_JPA_TIME, nullable = false)
    LocalTime heure;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = SousSequenceSMP.class)
    @JoinColumn(name = SousSequenceSMP.ATTRIBUTE_JPA_ID, referencedColumnName = SousSequenceSMP.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SousSequenceSMP sousSequenceSMP;

    @OneToMany(mappedBy = ValeurSMP.ID_JPA, cascade = {PERSIST, MERGE, REFRESH})
    List<ValeurSMP> valeursSMP = new LinkedList<ValeurSMP>();

    int lineNumber;

    /**
     *
     */
    public MesureSMP() {
        super();
    }

    /**
     *
     * @param heure
     * @param sousSequenceMPS
     */
    public MesureSMP(LocalTime heure, SousSequenceSMP sousSequenceMPS) {

        this.heure = heure;
        this.sousSequenceSMP = sousSequenceMPS;

    }

    /**
     *
     * @return
     */
    public Long getId_mesure_smp() {
        return id_mesure_smp;
    }

    /**
     *
     * @param id_mesure_smp
     */
    public void setId_mesure_smp(Long id_mesure_smp) {
        this.id_mesure_smp = id_mesure_smp;
    }

    /**
     *
     * @return
     */
    public List<ValeurSMP> getValeursSMP() {
        return valeursSMP;
    }

    /**
     *
     * @param valeursSMP
     */
    public void setValeursSMP(List<ValeurSMP> valeursSMP) {
        this.valeursSMP = valeursSMP;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     *
     * @param lineNumber
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     *
     * @return
     */
    public SousSequenceSMP getSousSequenceSMP() {
        return sousSequenceSMP;
    }

    /**
     *
     * @param sousSequenceSMP
     */
    public void setSousSequenceSMP(SousSequenceSMP sousSequenceSMP) {
        this.sousSequenceSMP = sousSequenceSMP;
    }

}
