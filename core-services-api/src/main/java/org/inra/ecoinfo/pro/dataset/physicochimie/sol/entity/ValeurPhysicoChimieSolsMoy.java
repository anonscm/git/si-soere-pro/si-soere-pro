/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurPhysicoChimieSolsMoy.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    MesurePhysicoChimieSolsMoy.ID_JPA,
    RealNode.ID_JPA,
    ValeurPhysicoChimieSolsMoy.JPA_COLUMN_ECARTTYPE}),
        indexes = {
            @Index(name = "valeur_physico_chimie_solmoy_mesure_idx", columnList = MesurePhysicoChimieSolsMoy.ID_JPA),
            @Index(name = "valeur_physico_chimie_solmoy_variable", columnList = RealNode.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = ValeurPhysicoChimie.ID_JPA, referencedColumnName = ValeurPhysicoChimieSolsMoy.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = ValeurPhysicoChimie.ID_JPA, column = @Column(name = ValeurPhysicoChimieSolsMoy.ID_JPA))})
public class ValeurPhysicoChimieSolsMoy extends ValeurPhysicoChimie<MesurePhysicoChimieSolsMoy, ValeurPhysicoChimieSolsMoy> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurphysicochimiesols_moy";

    /**
     *
     */
    public static final String ID_JPA = "vpc_moy_id";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String JPA_COLUMN_ECARTTYPE = "ecarttype";

    /**
     *
     */
    public static String RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    @Column(name = ValeurPhysicoChimieSolsMoy.JPA_COLUMN_ECARTTYPE, nullable = false)
    private float ecarttype;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesurePhysicoChimieSolsMoy.class)
    @JoinColumn(name = MesurePhysicoChimieSolsMoy.ID_JPA, referencedColumnName = MesurePhysicoChimieSolsMoy.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesurePhysicoChimieSolsMoy mesurePhysicoChimieSolsMoy;

    /**
     *
     */
    public ValeurPhysicoChimieSolsMoy() {
        super();
    }

    /**
     *
     * @param valeur
     * @param ecart
     * @param mesurePhysicoChimieSolsMoy
     * @param realNode
     */
    public ValeurPhysicoChimieSolsMoy(Float valeur, float ecart, MesurePhysicoChimieSolsMoy mesurePhysicoChimieSolsMoy, RealNode realNode) {
        super(valeur, realNode);
        this.mesurePhysicoChimieSolsMoy = mesurePhysicoChimieSolsMoy;
        this.ecarttype = ecart;
    }

    /**
     *
     * @return
     */
    public MesurePhysicoChimieSolsMoy getMesurePhysicoChimieSolsMoy() {
        return mesurePhysicoChimieSolsMoy;
    }

    /**
     *
     * @param mesurePhysicoChimieSolsMoy
     */
    public void setMesurePhysicoChimieSolsMoy(MesurePhysicoChimieSolsMoy mesurePhysicoChimieSolsMoy) {
        this.mesurePhysicoChimieSolsMoy = mesurePhysicoChimieSolsMoy;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

}
