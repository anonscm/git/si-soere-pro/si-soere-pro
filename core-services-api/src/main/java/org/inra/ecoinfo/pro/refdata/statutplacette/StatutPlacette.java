package org.inra.ecoinfo.pro.refdata.statutplacette;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.COLUMN_LIBELLE_STATPLC
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StatutPlacette.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_LIBELLE_STATPLC}))
public class StatutPlacette implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STATUTPLACETTE_ID; // stp_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STATUTPLACETTE_TABLE_NAME; // statut_placette

    @Id
    @Column(name = StatutPlacette.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_STATPLC, length = 150)
    private String libelle;

    /**
     *
     */
    public StatutPlacette() {
        super();
    }

    /**
     *
     * @param libelle
     */
    public StatutPlacette(String libelle) {
        super();
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
