package org.inra.ecoinfo.pro.refdata.objectifthematiqueprotocole;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.thematiqueetudiee.ThematiqueEtudiee;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ObjectifThematiqueProtocole.TABLE_NAME, uniqueConstraints =
        @UniqueConstraint(columnNames = {
            RefDataConstantes.PROTOCOLE_ID, RefDataConstantes.THEMEETUDIEE_ID, RefDataConstantes.COLUMN_OBJECTIF_OBJTHEMPROTOCOLE}))
public class ObjectifThematiqueProtocole implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.OBJECTIFTHEMATIQUEPROTOCOLE_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.OBJECTIFTHEMATIQUEPROTOCOLE_TABLE_NAME;
    @Id
    @Column(name = ObjectifThematiqueProtocole.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Protocole.class)
    @JoinColumn(name = RefDataConstantes.PROTOCOLE_ID, referencedColumnName = Protocole.ID_JPA, nullable = false)
    private Protocole protocole;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = ThematiqueEtudiee.class)
    @JoinColumn(name = RefDataConstantes.THEMEETUDIEE_ID, referencedColumnName = ThematiqueEtudiee.ID_JPA, nullable = false)
    private ThematiqueEtudiee thematiqueEtudiee;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_OBJECTIF_OBJTHEMPROTOCOLE, length = 500)
    private String objectif;

    /**
     *
     */
    public ObjectifThematiqueProtocole() {
        super();
    }

    /**
     *
     * @param protocole
     * @param thematiqueEtudiee
     * @param objectif
     */
    public ObjectifThematiqueProtocole(Protocole protocole, ThematiqueEtudiee thematiqueEtudiee, String objectif) {
        super();
        this.protocole = protocole;
        this.thematiqueEtudiee = thematiqueEtudiee;
        this.objectif = objectif;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getObjectif() {
        return objectif;
    }

    /**
     *
     * @return
     */
    public Protocole getProtocole() {
        return protocole;
    }

    /**
     *
     * @return
     */
    public ThematiqueEtudiee getThematiqueEtudiee() {
        return thematiqueEtudiee;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param objectif
     */
    public void setObjectif(String objectif) {
        this.objectif = objectif;
    }

    /**
     *
     * @param protocole
     */
    public void setProtocole(Protocole protocole) {
        this.protocole = protocole;
    }

    /**
     *
     * @param thematiqueEtudiee
     */
    public void setThematiqueEtudiee(ThematiqueEtudiee thematiqueEtudiee) {
        this.thematiqueEtudiee = thematiqueEtudiee;
    }
}
