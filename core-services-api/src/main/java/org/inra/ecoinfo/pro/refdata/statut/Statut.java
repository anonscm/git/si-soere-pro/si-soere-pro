package org.inra.ecoinfo.pro.refdata.statut;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Statut.NAME_ENTITY_JPA)
public class Statut implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "statut";

    /**
     *
     */
    public static final String JPA_ID = "statut_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "statut_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "statut_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "statut_comment";
    @Id
    @Column(name = Statut.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long statut_id;
    @Column(name = Statut.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String statut_code = "";
    @Column(name = Statut.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String statut_nom = "";
    @Column(name=Statut.JPA_COLUMN_COMMENT)
    private String statut_comment="";

    /**
     *
     */
    public Statut() {
        super();
    }

    /**
     *
     * @param nom
     * @param comment
     */
    public Statut( String nom,String comment) {
        this.statut_nom = nom;
        this.statut_comment = comment;
    }

    /**
     *
     * @return
     */
    public String getStatut_code() {
        return statut_code;
    }

    /**
     *
     * @return
     */
    public long getStatut_id() {
        return statut_id;
    }

    /**
     *
     * @return
     */
    public String getStatut_nom() {
        return statut_nom;
    }

    /**
     *
     * @param statut_code
     */
    public void setStatut_code(String statut_code) {
        this.statut_code = statut_code;
    }

    /**
     *
     * @param statut_id
     */
    public void setStatut_id(long statut_id) {
        this.statut_id = statut_id;
    }

    /**
     *
     * @param statut_nom
     */
    public void setStatut_nom(String statut_nom) {
        this.statut_nom = statut_nom;
    }

    /**
     *
     * @return
     */
    public String getStatut_comment() {
        return statut_comment;
    }

    /**
     *
     * @param statut_comment
     */
    public void setStatut_comment(String statut_comment) {
        this.statut_comment = statut_comment;
    }
    
}
