/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant;

import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.DataType;

/**
 *
 * @author adiankha
 */
@Entity
@Table(
        name = DatatypeVariableUnitePRO.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    DataType.ID_JPA,
    VariablesPRO.ID_JPA,
    Unitepro.ID_JPA,
    Methode.JPA_ID,
    HumiditeExpression.JPA_ID
}),
        indexes = {
            @Index(name = "dvu_uni_idx", columnList = Unitepro.ID_JPA),
            @Index(name = "dvu_var_idx", columnList = VariablesPRO.ID_JPA),
            @Index(name = "dvu_dty_idx", columnList = DataType.ID_JPA),
            @Index(name = "dvu_met_idx", columnList = Methode.JPA_ID),
            @Index(name = "dvu_hum_idx", columnList = HumiditeExpression.JPA_ID)})
@PrimaryKeyJoinColumn(name = DatatypeVariableUnitePRO.JPA_ID)
public class DatatypeVariableUnitePRO extends Nodeable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "datatypevariableunitepro";

    /**
     *
     */
    public static final String JPA_ID = "dvu_id";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Methode.JPA_ID, nullable = false)
    private Methode methode;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = HumiditeExpression.JPA_ID, nullable = false)
    private HumiditeExpression humiditeexpression;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Unitepro.ID_JPA, nullable = false)
    private Unitepro unitepro;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = VariablesPRO.ID_JPA, nullable = false)
    private VariablesPRO variablespro;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = DataType.ID_JPA, nullable = false)
    private DataType datatype;

    /**
     *
     */
    public DatatypeVariableUnitePRO() {
        super();
    }

    /**
     *
     * @param datatype
     * @param unitepro
     * @param variablespro
     * @param methode
     * @param humidite
     */
    public DatatypeVariableUnitePRO(DataType datatype, Unitepro unitepro, VariablesPRO variablespro, Methode methode, HumiditeExpression humidite) {
        super(String.format("%s,%s,%s,%s,%s", datatype.getCode(), variablespro.getCode(), unitepro.getCode(), methode.getMethode_mycode(), humidite.getCode()));
        this.unitepro = unitepro;
        this.variablespro = variablespro;
        this.methode = methode;
        this.humiditeexpression = humidite;
        this.datatype = datatype;
    }

    /**
     *
     * @return
     */
    public Methode getMethode() {
        return methode;
    }

    /**
     *
     * @param methode
     */
    public void setMethode(Methode methode) {
        this.methode = methode;
    }

    /**
     *
     * @return
     */
    public HumiditeExpression getHumiditeexpression() {
        return humiditeexpression;
    }

    /**
     *
     * @param humiditeexpression
     */
    public void setHumiditeexpression(HumiditeExpression humiditeexpression) {
        this.humiditeexpression = humiditeexpression;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DatatypeVariableUnitePRO other = (DatatypeVariableUnitePRO) obj;
        return !(!Objects.equals(this.getDatatype(), other.getDatatype())
                && !Objects.equals(this.getVariablespro(), other.getVariablespro())
                && !Objects.equals(this.getUnitepro().getCode(), other.getUnitepro().getCode())
                && !Objects.equals(this.getMethode().getMethode_code(), other.getMethode().getMethode_code())
                && !Objects.equals(this.getHumiditeexpression().getCode(), other.getHumiditeexpression().getCode()));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.getDatatype().getCode());
        hash = 311 * hash + Objects.hashCode(this.getVariablespro().getCode());
        hash = 3_111 * hash + Objects.hashCode(this.getUnitepro().getCode());
        hash = 3_1111 * hash + Objects.hashCode(this.getMethode().getMethode_code());
        hash = 3_1111 * hash + Objects.hashCode(this.getHumiditeexpression().getCode());

        return hash;
    }

    @Override
    public int compareTo(INodeable o) {
        if (o == null) {
            return -1;
        }
        if (!(o instanceof DatatypeVariableUnitePRO)) {
            return -1;
        }
        DatatypeVariableUnitePRO oo = (DatatypeVariableUnitePRO) o;
        if (this.getDatatype().compareTo(oo.getDatatype()) != 0) {
            return this.getDatatype().compareTo(oo.getDatatype());
        }
        if (this.getVariablespro().compareTo(oo
                .getVariablespro()) != 0) {
            return (this.getVariablespro()).compareTo(oo
                    .getVariablespro());
        }
        return this.getUnitepro().getCode().compareTo(oo.getUnitepro().getCode());
    }

    /**
     *
     * @return
     */
    public Unitepro getUnitepro() {
        return unitepro;
    }

    /**
     *
     * @param unitepro
     */
    public void setUnitepro(Unitepro unitepro) {
        this.unitepro = unitepro;
    }

    /**
     *
     * @return
     */
    public VariablesPRO getVariablespro() {
        return variablespro;
    }

    /**
     *
     * @param variablespro
     */
    public void setVariablespro(VariablesPRO variablespro) {
        this.variablespro = variablespro;
    }

    /**
     *
     * @return
     */
    public long getDvu_id() {
        return getId();
    }

    /**
     *
     * @param dvu_id
     */
    public void setDvu_id(long dvu_id) {
        setId(dvu_id);
    }

    /**
     *
     * @return
     */
    public DataType getDatatype() {
        return datatype;
    }

    /**
     *
     * @param datatype
     */
    public void setDatatype(DataType datatype) {
        this.datatype = datatype;
    }

    /**
     *
     * @return
     */
    @Override
    public String getName() {
        return getCode();
    }

    /**
     *
     * @return
     */
    @Override
    public Class<DatatypeVariableUnitePRO> getNodeableType() {
        return DatatypeVariableUnitePRO.class;
    }

}
