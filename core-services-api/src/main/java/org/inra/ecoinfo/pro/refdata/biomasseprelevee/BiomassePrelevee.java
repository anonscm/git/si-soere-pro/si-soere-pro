/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.biomasseprelevee;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = BiomassePrelevee.NAME_ENTITY_JPA)
public class BiomassePrelevee  implements Serializable{
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "biomasseprelevee";

    /**
     *
     */
    public static final String ID_JPA = "bm_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NOM = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT ="commentaire";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = BiomassePrelevee.ID_JPA, unique = true, nullable = false, updatable = false)
    private long bm_id = 0;
    @Size(min = 1, max = 80)
    @Column(name = BiomassePrelevee.JPA_COLUMN_NOM, unique = true, nullable = false, length = 80)
    private String nom = "";
    @Column(name = BiomassePrelevee.JPA_COLUMN_CODE, unique = true, nullable = false)
    private String code = "";
    @Column(name=BiomassePrelevee.JPA_COLUMN_COMMENT)
    private String commentaire="";

    /**
     *
     */
    public BiomassePrelevee() {
        super();
    }

    /**
     *
     * @param code
     * @param nom
     * @param commentaire
     */
    public BiomassePrelevee(String code, String nom, String commentaire) {
        this.code = code;
        this.commentaire= commentaire;
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getBm_id() {
        return bm_id;
    }

    /**
     *
     * @param bm_id
     */
    public void setBm_id(long bm_id) {
        this.bm_id = bm_id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
    
}
