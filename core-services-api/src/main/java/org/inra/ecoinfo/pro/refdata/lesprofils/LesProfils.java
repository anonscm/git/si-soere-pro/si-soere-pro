package org.inra.ecoinfo.pro.refdata.lesprofils;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = LesProfils.NAME_ENTITY_JPA)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class LesProfils implements Serializable{
private static final long serialVersionUID = 1L;
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "lesprofils";

    /**
     *
     */
    public static final String JPA_ID = "lesprofils_id";

    /**
     *
     */
    public static final String JPA_CODE_PROFIL = "code_profil";
    

    @Id
    @Column(name = LesProfils.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long lesprofils_id;

    /**
     *
     */
    @Column(name = LesProfils.JPA_CODE_PROFIL, nullable = true, unique = true)
    protected String code_profil = "";
    
    /**
     *
     */
    public LesProfils() {
	super();
		// TODO Auto-generated constructor stub
	}
    
    /**
     *
     * @param code_profil
     */
    public LesProfils( String code_profil) {
		
		this.code_profil = code_profil;
	}
	
    /**
     *
     * @return
     */
    public long getLesprofils_id() {
		return lesprofils_id;
	}

    /**
     *
     * @param lesprofils_id
     */
    public void setLesprofils_id(long lesprofils_id) {
		this.lesprofils_id = lesprofils_id;
	}

    /**
     *
     * @return
     */
    public String getCode_profil() {
		return code_profil;
	}

    /**
     *
     * @param code_profil
     */
    public void setCode_profil(String code_profil) {
		this.code_profil = code_profil;
	}
    

}
