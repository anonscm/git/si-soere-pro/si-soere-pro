package org.inra.ecoinfo.pro.refdata.caracteristiquevaleur;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = CaracteristiqueValeur.NAME_ENTITY_JPA)
public class CaracteristiqueValeur implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "caracteristiquevaleur";

    /**
     *
     */
    public static final String JPA_ID = "cv_id";

    /**
     *
     */
    public static final String JPA_KEY_COLUMN = "cv_code";

    /**
     *
     */
    public static final String JPA_VALUE_COLUMN = "cv_nom";

    /**
     *
     */
    public static final String JPA_COMMENT_COLUMN ="commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = CaracteristiqueValeur.JPA_ID, updatable = false)
    private long cv_id = 0;
    @Column(name = CaracteristiqueValeur.JPA_KEY_COLUMN, nullable = false)
    private String cv_code;
    @Column(name = CaracteristiqueValeur.JPA_VALUE_COLUMN, nullable = false, unique = true)
    private String cv_nom;
    @Column(name=CaracteristiqueValeur.JPA_COMMENT_COLUMN)
    private String commentaire="";

    /**
     *
     */
    public CaracteristiqueValeur() {
        super();
    }

    /**
     *
     * @param nom
     * @param commentaire
     */
    public CaracteristiqueValeur(String nom,String commentaire) {
        this.cv_nom = nom;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getCv_code() {
        return cv_code;
    }

    /**
     *
     * @return
     */
    public long getCv_id() {
        return cv_id;
    }

    /**
     *
     * @return
     */
    public String getCv_nom() {
        return cv_nom;
    }

    /**
     *
     * @param cv_code
     */
    public void setCv_code(String cv_code) {
        this.cv_code = cv_code;
    }

    /**
     *
     * @param cv_id
     */
    public void setCv_id(long cv_id) {
        this.cv_id = cv_id;
    }

    /**
     *
     * @param cv_nom
     */
    public void setCv_nom(String cv_nom) {
        this.cv_nom = cv_nom;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
