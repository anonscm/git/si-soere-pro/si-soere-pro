package org.inra.ecoinfo.pro.refdata.profillocalisation;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.lesprofils.LesProfils;

/**
 * @author Vivianne
 * 
 */


@Entity
@Table(name = ProfilLocalisation.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = LesProfils.JPA_ID)
public class ProfilLocalisation extends LesProfils implements Serializable{
	
    private static final long serialVersionUID = 1L;
    
    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "profil_localisation";
   // public static final String JPA_COLUMN_ID = "code_profil_interne";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "profil_localisation_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NOM = "profil_localisation_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_IDDONESOL = "code_profil_donesol";

    /**
     *
     */
    public static final String JPA_COLUMN_X = "x";

    /**
     *
     */
    public static final String JPA_COLUMN_Y = "y";

    /**
     *
     */
    public static final String JPA_COLUMN_GEOM = "geom";

    
   /* @Id
    @Column(name = ProfilLocalisation.JPA_COLUMN_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long code_profil_interne;*/
    @Column(name = ProfilLocalisation.JPA_COLUMN_CODE, unique = true, nullable = false, length = 150)
    private String profil_localisation_code;
    @Column(name = ProfilLocalisation.JPA_COLUMN_NOM, unique = true, nullable = false, length = 150)
    private String profil_localisation_nom;
    @Column(name = ProfilLocalisation.JPA_COLUMN_IDDONESOL, unique = true, nullable = false)
    private int code_profil_donesol = 0;
    @Column(name = ProfilLocalisation.JPA_COLUMN_X, unique = false, nullable = false)
    private int x = 0;
    @Column(name = ProfilLocalisation.JPA_COLUMN_Y, unique = false, nullable = false)
    private int y = 0 ;
    @Column(name = ProfilLocalisation.JPA_COLUMN_GEOM, unique = false, nullable = true)
    private int geom  = 0;
    
    /**
     *
     * @param profil_localisation_code
     * @param profil_localisation_nom
     * @param code_profil_donesol
     * @param x
     * @param y
     * @param geom
     */
    public ProfilLocalisation(String profil_localisation_code,
			String profil_localisation_nom, int code_profil_donesol, int x,
			int y, int geom) {
		super(profil_localisation_nom);
		this.profil_localisation_code = profil_localisation_code;
		this.profil_localisation_nom = profil_localisation_nom;
		this.code_profil_donesol = code_profil_donesol;
		this.x = x;
		this.y = y;
		this.geom = geom;
	}

    /**
     *
     */
    public ProfilLocalisation() {
		super();

	}



	/*public void setCode_profil_interne(long code_profil_interne) {
		this.code_profil_interne = code_profil_interne;
	}

	
	public long getCode_profil_interne() {
		return code_profil_interne;
	}*/

    /**
     *
     * @return
     */


	public String getProfil_localisation_code() {
		return profil_localisation_code;
	}

    /**
     *
     * @param profil_localisation_code
     */
    public void setProfil_localisation_code(String profil_localisation_code) {
		this.profil_localisation_code = profil_localisation_code;
	}

    /**
     *
     * @return
     */
    public String getProfil_localisation_nom() {
		return profil_localisation_nom;
	}

    /**
     *
     * @param profil_localisation_nom
     */
    public void setProfil_localisation_nom(String profil_localisation_nom) {
		this.profil_localisation_nom = profil_localisation_nom;
	}

    /**
     *
     * @return
     */
    public int getCode_profil_donesol() {
		return code_profil_donesol;
	}

    /**
     *
     * @param code_profil_donesol
     */
    public void setCode_profil_donesol(int code_profil_donesol) {
		this.code_profil_donesol = code_profil_donesol;
	}

    /**
     *
     * @return
     */
    public int getX() {
		return x;
	}

    /**
     *
     * @param x
     */
    public void setX(int x) {
		this.x = x;
	}

    /**
     *
     * @return
     */
    public int getY() {
		return y;
	}

    /**
     *
     * @param y
     */
    public void setY(int y) {
		this.y = y;
	}

    /**
     *
     * @return
     */
    public int getGeom() {
		return geom;
	}

    /**
     *
     * @param geom
     */
    public void setGeom(int geom) {
		this.geom = geom;
	}


}
