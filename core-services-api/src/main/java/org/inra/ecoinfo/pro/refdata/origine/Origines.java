package org.inra.ecoinfo.pro.refdata.origine;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Origines.NAME_ENTITY_JPA)
public class Origines implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "origine";

    /**
     *
     */
    public static final String JPA_COLUMN_ID = "origine_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "origine_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "origine_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "origine_comment";
    @Id
    @Column(name = Origines.JPA_COLUMN_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long origine_id;
    @Column(name = Origines.JPA_COLUMN_CODE, unique = true, nullable = false, length = 150)
    private String origine_code;
    @Column(name = Origines.JPA_COLUMN_NAME, unique = true, nullable = false, length = 150)
    private String origine_nom;
    
    @Column(name=Origines.JPA_COLUMN_COMMENT,length = 1000)
    private String origine_comment ="";
    
    /**
     *
     */
    public Origines() {
        super();
    }

    /**
     *
     * @param nom
     * @param comment
     */
    public Origines(String nom,String comment) {
        super();
       this.origine_nom = nom;
        this.origine_comment = comment;
    }
    
    /**
     *
     * @return
     */
    public String getOrigine_code() {
        return origine_code;
    }

    /**
     *
     * @return
     */
    public long getOrigine_id() {
        return origine_id;
    }

    /**
     *
     * @return
     */
    public String getOrigine_nom() {
        return origine_nom;
    }

    /**
     *
     * @param origine_code
     */
    public void setOrigine_code(String origine_code) {
        this.origine_code = origine_code;
    }

    /**
     *
     * @param origine_id
     */
    public void setOrigine_id(long origine_id) {
        this.origine_id = origine_id;
    }

    /**
     *
     * @param origine_nom
     */
    public void setOrigine_nom(String origine_nom) {
        this.origine_nom = origine_nom;
    }

    /**
     *
     * @return
     */
    public String getOrigine_comment() {
        return origine_comment;
    }

    /**
     *
     * @param origine_comment
     */
    public void setOrigine_comment(String origine_comment) {
        this.origine_comment = origine_comment;
    }
    
    @Override
    public String toString() {
        return " nom: " + getOrigine_nom();
    }
}
