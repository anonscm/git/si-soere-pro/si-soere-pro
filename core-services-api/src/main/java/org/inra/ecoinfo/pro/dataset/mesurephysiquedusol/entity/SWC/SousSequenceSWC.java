/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

/**
 *
 * @author vjkoyao
 */

@Entity 
@Table(name = SousSequenceSWC.NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
        SequenceSWC.ATTRIBUTE_JPA_ID, SousSequenceSWC.ATTRIBUTE_JPA_PROFONDEUR }))

public class SousSequenceSWC implements Serializable{
    
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID           = "id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PROFONDEUR   = "profondeur";

  
    static final long          serialVersionUID   = 1L;
    
    /**
     *
     */
    public static final String    NAME_TABLE         = "sous_sequence_swc";


    
    
    @Id
    @Column(name=SousSequenceSWC.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long                       id;
    

    @Column(name = SousSequenceSWC.ATTRIBUTE_JPA_PROFONDEUR, nullable = false)
    Integer                    profondeur;
    
    
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = SequenceSWC.class)
    @JoinColumn(name = SequenceSWC.ATTRIBUTE_JPA_ID, referencedColumnName = SequenceSWC.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SequenceSWC             sequenceSWC;
    
        
    @OneToMany(mappedBy = MesureSWC.ATTRIBUTE_JPA_ID, cascade = ALL)
    private List<MesureSWC> mesures = new LinkedList<MesureSWC>();

    /**
     *
     */
    public SousSequenceSWC() {
        super();
    }

    /**
     *
     * @param profondeur
     * @param sequenceSWC
     */
    public SousSequenceSWC(Integer profondeur, SequenceSWC sequenceSWC) {
        this.profondeur = profondeur;
        this.sequenceSWC = sequenceSWC;
    }
    
    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Integer getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public SequenceSWC getSequenceSWC() {
        return sequenceSWC;
    }

    /**
     *
     * @param sequenceSWC
     */
    public void setSequenceSWC(SequenceSWC sequenceSWC) {
        this.sequenceSWC = sequenceSWC;
    }

    /**
     *
     * @return
     */
    public List<MesureSWC> getMesures() {
        return mesures;
    }

    /**
     *
     * @param mesures
     */
    public void setMesures(List<MesureSWC> mesures) {
        this.mesures = mesures;
    }
    
    
}
