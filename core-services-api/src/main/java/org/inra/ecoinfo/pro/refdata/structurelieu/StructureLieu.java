package org.inra.ecoinfo.pro.refdata.structurelieu;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames =
 * {RefDataConstantes.STRUCTURE_ID, RefDataConstantes.LIEU_ID
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StructureLieu.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames
                = {RefDataConstantes.STRUCTURE_ID,
            RefDataConstantes.LIEU_ID}))
public class StructureLieu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STRUCTURELIEU_ID; // strl_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STRUCTURELIEU_TABLE_NAME; // structure_lieu
    @Id
    @Column(name = StructureLieu.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Structure.class)
    @JoinColumn(name = RefDataConstantes.STRUCTURE_ID, referencedColumnName = Structure.ID_JPA, nullable = false)
    private Structure structure;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Lieu.class)
    @JoinColumn(name = RefDataConstantes.LIEU_ID, referencedColumnName = Lieu.ID_JPA, nullable = false)
    private Lieu lieu;

    /**
     *
     */
    public StructureLieu() {
        super();
    }

    /**
     *
     * @param structure
     * @param lieu
     */
    public StructureLieu(Structure structure, Lieu lieu) {
        super();
        this.structure = structure;
        this.lieu = lieu;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Lieu getLieu() {
        return lieu;
    }

    /**
     *
     * @return
     */
    public Structure getStructure() {
        return structure;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param lieu
     */
    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    /**
     *
     * @param structure
     */
    public void setStructure(Structure structure) {
        this.structure = structure;
    }
}
