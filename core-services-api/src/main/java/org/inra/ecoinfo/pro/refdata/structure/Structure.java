package org.inra.ecoinfo.pro.refdata.structure;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.adresse.Adresse;
import org.inra.ecoinfo.pro.refdata.typestructure.TypeStructure;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.COLUMN_NOM_STR, RefDataConstantes.COLUMN_PRECISION_STR
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Structure.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_NOM_STR,
    RefDataConstantes.COLUMN_PRECISION_STR}))
public class Structure implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STRUCTURE_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STRUCTURE_TABLE_NAME;
    @Id
    @Column(name = Structure.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_STR, length = 100)
    private String nom;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_PRECISION_STR, length = 100)
    private String precision;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_NOTEL_STR)
    private String noTel;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_NOFAX_STR)
    private String noFax;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Adresse.class)
    @JoinColumn(name = RefDataConstantes.ADRESSE_ID, referencedColumnName = Adresse.ID_JPA, nullable = false)
    private Adresse adresse;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = TypeStructure.class)
    @JoinColumn(name = RefDataConstantes.TYPESTRUCTURE_ID, referencedColumnName = TypeStructure.ID_JPA, nullable = false)
    private TypeStructure typeStructure;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_ESTSIGNATAIRE_STR)
    private Boolean estSignataire;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_COMMENTAIRE_STR, length = 500)
    private String commentaire;

    /**
     *
     */
    public Structure() {
        super();
    }

    /**
     *
     * @param nom
     * @param precision
     * @param noTel
     * @param noFax
     * @param adresse
     * @param typeStructure
     * @param estSignataire
     * @param commentaire
     */
    public Structure(String nom, String precision, String noTel, String noFax, Adresse adresse, TypeStructure typeStructure, Boolean estSignataire, String commentaire) {
        super();
        this.nom = nom;
        this.precision = precision;
        this.noTel = noTel;
        this.noFax = noFax;
        this.adresse = adresse;
        this.typeStructure = typeStructure;
        this.estSignataire = estSignataire;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Adresse getAdresse() {
        return adresse;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @return
     */
    public Boolean getEstSignataire() {
        return estSignataire;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNoFax() {
        return noFax;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getNoTel() {
        return noTel;
    }

    /**
     *
     * @return
     */
    public String getPrecision() {
        return precision;
    }

    /**
     *
     * @return
     */
    public TypeStructure getTypeStructure() {
        return typeStructure;
    }

    /**
     *
     * @param adresse
     */
    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @param estSignataire
     */
    public void setEstSignataire(Boolean estSignataire) {
        this.estSignataire = estSignataire;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param noFax
     */
    public void setNoFax(String noFax) {
        this.noFax = noFax;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @param noTel
     */
    public void setNoTel(String noTel) {
        this.noTel = noTel;
    }

    /**
     *
     * @param precision
     */
    public void setPrecision(String precision) {
        this.precision = precision;
    }

    /**
     *
     * @param typeStructure
     */
    public void setTypeStructure(TypeStructure typeStructure) {
        this.typeStructure = typeStructure;
    }

}
