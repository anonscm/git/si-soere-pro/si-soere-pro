package org.inra.ecoinfo.pro.refdata.matierepetapemethode;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.procedemethodeprodmp.ProcedeMethodeProdMp;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProcedeMethodeEtapeMP.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                                      ProcedeMethodeProdMp.ID_JPA, 
                                                                                      Etapes.ID_JPA,
                                                                                      MethodeEtapes.ID_JPA,
                                                                                      ProcedeMethodeEtapeMP.JPA_COLUMN_ORDER
}))
public class ProcedeMethodeEtapeMP implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "procedemethodeetapemp";

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_ORDER = "ordre";

    /**
     *
     */
    public static final String JPA_COLUMN_SAMMERY = "commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ProcedeMethodeEtapeMP.ID_JPA, updatable = false)
    private long id = 0;
    @Column(name = ProcedeMethodeEtapeMP.JPA_COLUMN_ORDER)
    private int ordre = 0;
    @Column(name=ProcedeMethodeEtapeMP.JPA_COLUMN_SAMMERY,length = 300)
    private String commentaire ="";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodeEtapes.ID_JPA, nullable = false)
    private MethodeEtapes methodeetapes;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Etapes.ID_JPA, nullable = false)
    private Etapes etapes;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = ProcedeMethodeProdMp.ID_JPA, nullable = false)
    private ProcedeMethodeProdMp procedemethodepropm;

    /**
     *
     */
    public ProcedeMethodeEtapeMP() {
        super();
    }

    /**
     *
     * @param mpp
     * @param etapes
     * @param methodeetapes
     * @param ordre
     * @param commentaire
     */
    public ProcedeMethodeEtapeMP(ProcedeMethodeProdMp mpp, Etapes etapes, MethodeEtapes methodeetapes, int ordre,String commentaire) {
        this.procedemethodepropm = mpp;
        this.etapes = etapes;
        this.methodeetapes = methodeetapes;
        this.ordre = ordre;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Etapes getEtapes() {
        return etapes;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public MethodeEtapes getMethodeetapes() {
        return methodeetapes;
    }

    /**
     *
     * @return
     */
    public int getOrdre() {
        return ordre;
    }

    /**
     *
     * @return
     */
    public ProcedeMethodeProdMp getProcedemethodepropm() {
        return procedemethodepropm;
    }

    /**
     *
     * @param etapes
     */
    public void setEtapes(Etapes etapes) {
        this.etapes = etapes;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @param methodeetapes
     */
    public void setMethodeetapes(MethodeEtapes methodeetapes) {
        this.methodeetapes = methodeetapes;
    }

    /**
     *
     * @param ordre
     */
    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    /**
     *
     * @param procedemethodepropm
     */
    public void setProcedemethodepropm(ProcedeMethodeProdMp procedemethodepropm) {
        this.procedemethodepropm = procedemethodepropm;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
}
