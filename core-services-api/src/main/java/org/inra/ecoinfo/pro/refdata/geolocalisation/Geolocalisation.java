
package org.inra.ecoinfo.pro.refdata.geolocalisation;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Geolocalisation.TABLE_NAME)
public class Geolocalisation implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.GEOLOC_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.GEOLOC_TABLE_NAME;
    @Id
    @Column(name = Geolocalisation.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = SystemeProjection.class)
    @JoinColumn(name = RefDataConstantes.SYSTEMEPROJECTION_ID, referencedColumnName = SystemeProjection.ID_JPA, nullable = false)
    private SystemeProjection systemeProjection;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_LATITUDE_GEOLOC)
    private String latitude;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_LONGITUDE_GEOLOC)
    private String longitude;

    /**
     *
     */
    public Geolocalisation() {
        super();
    }

    /**
     *
     * @param systemeProjection
     * @param latitude
     * @param longitude
     */
    public Geolocalisation(SystemeProjection systemeProjection, String latitude, String longitude) {
        super();
        this.systemeProjection = systemeProjection;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @return
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @return
     */
    public SystemeProjection getSystemeProjection() {
        return systemeProjection;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @param systemeProjection
     */
    public void setSystemeProjection(SystemeProjection systemeProjection) {
        this.systemeProjection = systemeProjection;
    }
}
