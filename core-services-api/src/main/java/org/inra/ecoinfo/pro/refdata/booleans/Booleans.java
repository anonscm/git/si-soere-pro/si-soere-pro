package org.inra.ecoinfo.pro.refdata.booleans;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Booleans.NAME_ENTITY_JPA)
public class Booleans implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "boolean";

    /**
     *
     */
    public static final String JPA_ID = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_LIBELLE = "libelle";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY ="code";
    @Id
    @Column(name = Booleans.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = Booleans.JPA_COLUMN_LIBELLE, nullable = false, unique = true)
    private String libelle = "";
     @Column(name = Booleans.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String code = "";

    /**
     *
     */
    public Booleans() {
        super();
    }

    /**
     *
     * @param libelle
     */
    public Booleans(String libelle) {
      this.libelle = libelle;
      this.code = Utils.createCodeFromString(libelle);
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }
   
    
}
