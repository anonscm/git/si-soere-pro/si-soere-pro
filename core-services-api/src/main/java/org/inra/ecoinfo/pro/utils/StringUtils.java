/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author adiankha
 */
public class StringUtils {

    /**
     *
     */
    public static final String NEW_LINE = "%n";

    /**
     *
     */
    public static final String ALPHA_NUM_REGEX = "^(.*?)([0-9]+)$";

    /**
     *
     */
    public static final Pattern ALPHA_NUM_PATTERN = Pattern.compile(ALPHA_NUM_REGEX);

    /**
     *
     */
    public static final String EXTRA_LEFT__NUM = "000";

    /**
     *
     */
    public static final String EXTRA_RIGHT_ALPHA = "**********";

    /**
     *
     * @param str
     * @return
     */
    public static String orderedString(String str) {
        Matcher m = ALPHA_NUM_PATTERN.matcher(str);
        if (m.matches()) {
            String alpha = (m.group(1) + EXTRA_RIGHT_ALPHA).substring(0, 10);
            final String addNum = EXTRA_LEFT__NUM + m.group(2);
            String num = addNum.substring(addNum.length() - 3, addNum.length());
            str = alpha + num;
        }
        return str;
    }
    
}
