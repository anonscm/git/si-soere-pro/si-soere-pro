/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
@MappedSuperclass
public class ValeurIncubation<M extends MesureIncubation, V extends ValeurIncubation> {

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "id";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_QUANTITE = "valeur";


    @Id
    @Column(name = ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = ATTRIBUTE_JPA_QUANTITE)
    private Float valeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private RealNode realNode;

    /**
     *
     * @param valeur
     * @param realNode
     */
    public ValeurIncubation(Float valeur, RealNode realNode) {
        this.valeur = valeur;
        this.realNode = realNode;
    }

    /**
     *
     */
    public ValeurIncubation() {
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

}
