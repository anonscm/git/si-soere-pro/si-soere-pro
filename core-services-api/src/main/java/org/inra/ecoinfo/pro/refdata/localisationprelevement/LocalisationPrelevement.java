/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationprelevement;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=LocalisationPrelevement.JPA_NAME_ENTITY)
public class LocalisationPrelevement implements Serializable{

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "localisationprelevement";

    /**
     *
     */
    public static final String ID_JPA = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";
    
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = LocalisationPrelevement.ID_JPA, nullable = false, updatable = false, unique = true)
   private long id;
   @Column(name = LocalisationPrelevement.JPA_COLUMN_KEY, nullable = false, unique = true)
   private String code;
   @Column(name = LocalisationPrelevement.JPA_COLUMN_NAME, nullable = false, unique = true)
   private String nom;
   
    /**
     *
     */
    public LocalisationPrelevement(){
       super();
   }
   
    /**
     *
     * @param nom
     */
    public LocalisationPrelevement(String nom){
       this.nom = nom;
       this.code = Utils.createCodeFromString(nom);
   }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }
   
   
}
