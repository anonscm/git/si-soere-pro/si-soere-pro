/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.cultures.Cultures;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.partieprelevee.PartiePrelevee;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = MesurePlanteMoyenne.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    DescriptionTraitement.ID_JPA,
    MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE,
    MesurePlanteMoyenne.COLUMN_NOM_LABO,
    MesurePlanteMoyenne.COLUMN_NB_REP,
    Cultures.JPA_ID, PartiePrelevee.ID_JPA}),
        indexes = {
            @Index(name = "mesure_plante_prodisp_idx", columnList = DescriptionTraitement.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, referencedColumnName = MesurePlanteMoyenne.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, column = @Column(name = MesurePlanteMoyenne.ID_JPA))})
public class MesurePlanteMoyenne extends MesurePhysicoChimie<MesurePlanteMoyenne, ValeurPlanteMoyenne> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesureplantemoyenne";

    /**
     *
     */
    public static final String ID_JPA = "pm_id";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate = "localDatePrelevement";

    /**
     *
     */
    public static final String COLUMN_NB_REP = "repetition";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "nomlabo";

    /**
     *
     */
    public static final String COLUMN_HAUTEURCOUPE = "hauteurcoup";

    /**
     *
     */
    public static final String COLUMN_KEY_MESURE = "keymesure";

    @Column(name = MesurePlanteMoyenne.COLUMN_NOM_LABO, nullable = false)
    private String nomlabo = "";

    @Column(name = MesurePlanteMoyenne.COLUMN_NB_REP, nullable = false)
    private int repetition = 0;

    @Column(name = MesurePlanteMoyenne.COLUMN_HAUTEURCOUPE)
    private double hauteurcoup = 0;

    @Column(name = MesurePlanteElementaire.COLUMN_KEY_MESURE, nullable = false, unique = true)
    private String keymesure = null;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private DescriptionTraitement descriptionTraitement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = Cultures.JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private Cultures cultures;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = PartiePrelevee.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private PartiePrelevee partie;

    @OneToMany(mappedBy = "mesurePlanteMoyenne", cascade = ALL)
    List<ValeurPlanteMoyenne> valeurPlanteMoyennes = new LinkedList();

    /**
     *
     */
    public MesurePlanteMoyenne() {
        super();
    }

    /**
     *
     * @param ligneFichierEchange
     * @param LocalDateprelevement
     * @param traitement
     * @param cultures
     * @param partie
     * @param hauteur
     * @param nomlabo
     * @param repet
     * @param versionfile
     * @param line
     */
    public MesurePlanteMoyenne(Long ligneFichierEchange,  LocalDate LocalDateprelevement, DescriptionTraitement traitement, Cultures cultures, PartiePrelevee partie, double hauteur, String nomlabo, int repet, VersionFile versionfile, long line) {
        super(LocalDateprelevement, ligneFichierEchange, versionfile);
        this.descriptionTraitement = traitement;
        this.nomlabo = nomlabo;
        this.repetition = repet;
        this.cultures = cultures;
        this.partie = partie;
        this.hauteurcoup = hauteur;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public int getRepetition() {
        return repetition;
    }

    /**
     *
     * @param repetition
     */
    public void setRepetition(int repetition) {
        this.repetition = repetition;
    }

    /**
     *
     * @return
     */
    public DescriptionTraitement getDescriptionTraitement() {
        return descriptionTraitement;
    }

    /**
     *
     * @param descriptionTraitement
     */
    public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    /**
     *
     * @return
     */
    public List<ValeurPlanteMoyenne> getValeurPlanteMoyennes() {
        return valeurPlanteMoyennes;
    }

    /**
     *
     * @param valeurPlanteMoyennes
     */
    public void setValeurPlanteMoyennes(List<ValeurPlanteMoyenne> valeurPlanteMoyennes) {
        this.valeurPlanteMoyennes = valeurPlanteMoyennes;
    }
    /**
     *
     * @return
     */
    public Cultures getCultures() {
        return cultures;
    }

    /**
     *
     * @param cultures
     */
    public void setCultures(Cultures cultures) {
        this.cultures = cultures;
    }

    /**
     *
     * @return
     */
    public PartiePrelevee getPartie() {
        return partie;
    }

    /**
     *
     * @param partie
     */
    public void setPartie(PartiePrelevee partie) {
        this.partie = partie;
    }

    /**
     *
     * @return
     */
    public double getHauteurcoup() {
        return hauteurcoup;
    }

    /**
     *
     * @param hauteurcoup
     */
    public void setHauteurcoup(double hauteurcoup) {
        this.hauteurcoup = hauteurcoup;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

}
