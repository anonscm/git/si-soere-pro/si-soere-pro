/**
 *
 */
package org.inra.ecoinfo.pro.refdata.statutplassocieplacette;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.statutplacette.StatutPlacette;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.STATUTPLACETTE_ID, RefDataConstantes.PLACETTE_ID
 *
 * @author ptcherniati
 */
@Entity
@Table(name = StatutPlacettesAssociesPlacette.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.STATUTPLACETTE_ID,
    RefDataConstantes.PLACETTE_ID}))
public class StatutPlacettesAssociesPlacette implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.STATUTASSOCIEPLACETTE_ID; // stpl_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.STATUTASSOCIEPLACETTE_TABLE_NAME; // statutplacettes_associes_placette

    @Id
    @Column(name = StatutPlacettesAssociesPlacette.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = StatutPlacette.class)
    @JoinColumn(name = RefDataConstantes.STATUTPLACETTE_ID, referencedColumnName = StatutPlacette.ID_JPA, nullable = false)
    private StatutPlacette statutPlacette;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Placette.class)
    @JoinColumn(name = RefDataConstantes.PLACETTE_ID, referencedColumnName = Placette.ID_JPA, nullable = false)
    private Placette placette;

    /**
     *
     */
    public StatutPlacettesAssociesPlacette() {
        super();
    }

    /**
     * @param statutPlacette
     * @param placette
     */
    public StatutPlacettesAssociesPlacette(StatutPlacette statutPlacette,
            Placette placette) {
        super();
        this.statutPlacette = statutPlacette;
        this.placette = placette;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the statutPlacette
     */
    public StatutPlacette getStatutPlacette() {
        return statutPlacette;
    }

    /**
     * @param statutPlacette the statutPlacette to set
     */
    public void setStatutPlacette(StatutPlacette statutPlacette) {
        this.statutPlacette = statutPlacette;
    }

    /**
     * @return the placette
     */
    public Placette getPlacette() {
        return placette;
    }

    /**
     * @param placette the placette to set
     */
    public void setPlacette(Placette placette) {
        this.placette = placette;
    }

}
