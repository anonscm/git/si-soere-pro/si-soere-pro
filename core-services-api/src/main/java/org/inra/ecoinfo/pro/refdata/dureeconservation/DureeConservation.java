/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dureeconservation;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=DureeConservation.NAME_ENTITY_JPA)
public class DureeConservation implements Serializable{
     private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "dureeconservation";

    /**
     *
     */
    public static final String JPA_ID = "dc_id";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "dc_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "dc_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENT = "dc_comment";
    
     @Id
    @Column(name = DureeConservation.JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long dc_id;
    @Column(name = DureeConservation.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String dc_code = "";
    @Column(name = DureeConservation.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String dc_nom = "";
    @Column(name=DureeConservation.JPA_COLUMN_COMMENT)
    private String dc_comment="";
    
    /**
     *
     */
    public DureeConservation(){
        super();
    }
    
    /**
     *
     * @param nom
     * @param comment
     */
    public DureeConservation( String nom,String comment) {
        this.dc_nom = nom;
        this.dc_comment = comment;
    }

    /**
     *
     * @return
     */
    public long getDc_id() {
        return dc_id;
    }

    /**
     *
     * @return
     */
    public String getDc_code() {
        return dc_code;
    }

    /**
     *
     * @return
     */
    public String getDc_nom() {
        return dc_nom;
    }

    /**
     *
     * @return
     */
    public String getDc_comment() {
        return dc_comment;
    }

    /**
     *
     * @param dc_id
     */
    public void setDc_id(long dc_id) {
        this.dc_id = dc_id;
    }

    /**
     *
     * @param dc_code
     */
    public void setDc_code(String dc_code) {
        this.dc_code = dc_code;
    }

    /**
     *
     * @param dc_nom
     */
    public void setDc_nom(String dc_nom) {
        this.dc_nom = dc_nom;
    }

    /**
     *
     * @param dc_comment
     */
    public void setDc_comment(String dc_comment) {
        this.dc_comment = dc_comment;
    }
      
      
}
