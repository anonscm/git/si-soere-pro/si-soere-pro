/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typefacteur;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = TypeFacteur.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_TPFCT}))
public class TypeFacteur implements Serializable 
{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.TYPEFACTEUR_ID; // tpfct_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.TYPEFACTEUR_TABLE_NAME; // type_facteur
    
    @Id
    @Column(name = TypeFacteur.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_TPFCT, length = 50)
    private String libelle;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_CODE_TPFCT, length = 50)
    private String code;
    
    /**
     *
     */
    public TypeFacteur() 
    {
        super();
    }

    /**
	 * @param libelle
	 * @param code
	 */
	public TypeFacteur(String libelle, String code) {
		super();
		this.libelle = libelle;
		this.code = code;
	}

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }
    
    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }
    
    /**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
