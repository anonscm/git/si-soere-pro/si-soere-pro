/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.unitepro;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=Unitepro.NAME_ENTITY_JPA)
public class Unitepro extends Unite implements Serializable{

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "unitepro";

    /**
     *
     */
    public static final String JPA_ID = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "nom";

    /**
     *
     */
    public static final String JPA_COLUMN_MYCODE="mycode";
    
//    @Column(name = Unitepro.JPA_COLUMN_CODE, nullable = false,unique=true)
//    private String code ="";
//    @Column(name = Unitepro.JPA_COLUMN_CODE, nullable = false,unique = true)
//    private String nom="";
    @Column(name=Unitepro.JPA_COLUMN_MYCODE, nullable = false,unique = true)
    private String mycode="";
    
    /**
     *
     */
    public Unitepro(){
        super();
    }

    /**
     *
     * @param code
     * @param nom
     */
    public Unitepro(String code,String nom){
        super(code,nom);
        this.mycode = Utils.createCodeFromString(code);
    }

    /**
     *
     * @return
     */
    public String getMycode() {
        return mycode;
    }

    /**
     *
     * @param mycode
     */
    public void setMycode(String mycode) {
        this.mycode = mycode;
    }

    /**
     *
     * @param code
     */
    @Override
    public void setCode(String code) {
        super.setCode(code); 
    }

    /**
     *
     * @return
     */
    @Override
    public String getCode() {
        return super.getCode();
    }
    
    
    
}
