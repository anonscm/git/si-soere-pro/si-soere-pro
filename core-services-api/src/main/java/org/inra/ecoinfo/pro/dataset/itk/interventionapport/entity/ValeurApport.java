/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = ValeurApport.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    RealNode.ID_JPA,
    MesureApport.ATTRIBUTE_JPA_ID}))
@PrimaryKeyJoinColumn(name = ValeurITK.ATTRIBUTE_JPA_ID, referencedColumnName = ValeurApport.ATTRIBUTE_JPA_ID)
@AttributeOverrides({
    @AttributeOverride(name = ValeurITK.ATTRIBUTE_JPA_ID, column = @Column(name = ValeurApport.ATTRIBUTE_JPA_ID))})
public class ValeurApport extends ValeurITK<MesureApport, ValeurApport>{

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "valeurinterventionapport";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID = "apportid";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = MesureApport.class)
    @JoinColumn(name = MesureApport.ATTRIBUTE_JPA_ID, referencedColumnName = MesureApport.ATTRIBUTE_JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureApport mesureinterventionapport;

    /**
     *
     */
    public ValeurApport() {
        super();
    }

    /**
     *
     * @param valeur
     * @param realNode
     * @param mesureinterventionapport
     */
    public ValeurApport(Float valeur, RealNode realNode, MesureApport mesureinterventionapport) {
        super(valeur, realNode);
        this.mesureinterventionapport = mesureinterventionapport;
    }

    /**
     *
     * @param realNode
     * @param listeItineraire
     * @param mesureinterventionapport
     */
    public ValeurApport(ListeItineraire listeItineraire, RealNode realNode, MesureApport mesureinterventionapport) {
        super(listeItineraire, realNode);
        this.mesureinterventionapport = mesureinterventionapport;
    }

    public MesureApport getMesureinterventionapport() {
        return mesureinterventionapport;
    }

    public void setMesureinterventionapport(MesureApport mesureinterventionapport) {
        this.mesureinterventionapport = mesureinterventionapport;
    }
}
