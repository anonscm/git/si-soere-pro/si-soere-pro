package org.inra.ecoinfo.pro.refdata.methodeetape;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MethodeEtapes.NAME_ENTITY_JPA)
public class MethodeEtapes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "methodeetape";

    /**
     *
     */
    public static final String ID_JPA = "me_id";

    /**
     *
     */
    public static final String JPA_COLUNM_KEY = "me_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "me_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT="commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = MethodeEtapes.ID_JPA, nullable = false, updatable = false)
    private long me_id = 0;
    @Column(name = MethodeEtapes.JPA_COLUNM_KEY, nullable = false, unique = true)
    private String me_code = "";
    @Column(name = MethodeEtapes.JPA_COLUMN_NAME, nullable = false, unique = true)
    private String me_nom = "";
    @Column(name=MethodeEtapes.JPA_COLUMN_COMENT)
    private String commentaire="";

    /**
     *
     */
    public MethodeEtapes() {
        super();
    }

    /**
     *
     * @param nom
     */
    public MethodeEtapes(String nom) {
        this.me_nom = nom;
    }

    /**
     *
     * @return
     */
    public String getMe_code() {
        return me_code;
    }

    /**
     *
     * @return
     */
    public long getMe_id() {
        return me_id;
    }

    /**
     *
     * @return
     */
    public String getMe_nom() {
        return me_nom;
    }

    /**
     *
     * @param me_code
     */
    public void setMe_code(String me_code) {
        this.me_code = me_code;
    }

    /**
     *
     * @param me_id
     */
    public void setMe_id(long me_id) {
        this.me_id = me_id;
    }

    /**
     *
     * @param me_nom
     */
    public void setMe_nom(String me_nom) {
        this.me_nom = me_nom;
   }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
