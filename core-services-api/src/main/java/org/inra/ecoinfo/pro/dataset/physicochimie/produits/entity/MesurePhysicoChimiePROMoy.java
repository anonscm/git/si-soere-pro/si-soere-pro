/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesurePhysicoChimiePROMoy.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    ProduitDispositif.JPA_COLUMN_ID,
    MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE,
    MesurePhysicoChimiePROMoy.COLUMN_NOM_LABO,
    MesurePhysicoChimiePROMoy.COLUMN_NB_REP}),
        indexes = {
            @Index(name = "mesure_physico_chimie_promoy_prodisp_idx", columnList = ProduitDispositif.JPA_COLUMN_ID)
        })
@PrimaryKeyJoinColumn(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, referencedColumnName = MesurePhysicoChimiePROMoy.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, column = @Column(name = MesurePhysicoChimiePROMoy.ID_JPA))})
public class MesurePhysicoChimiePROMoy extends MesurePhysicoChimie<MesurePhysicoChimiePROMoy, ValeurPhysicoChimiePROMoy> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesurephysicochimiepro_moy";

    /**
     *
     */
    public static final String ID_JPA = "mpc_pro_moy_id";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate = "LocalDateprelevement";

    /**
     *
     */
    public static final String COLUMN_NB_REP = "nbre_repetition";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "nom_laboratoire";

    /**
     *
     */
    public static final String COLUMN_KEY_MESURE = "keymesure";

    @Column(name = MesurePhysicoChimiePROMoy.COLUMN_NOM_LABO, nullable = false)
    private String nom_laboratoire = "";

    @Column(name = MesurePhysicoChimiePROMoy.COLUMN_NB_REP, nullable = false)
    private int nbre_repetition = 0;

    @Column(name = MesurePhysicoChimiePROMoy.COLUMN_KEY_MESURE, nullable = false, unique = true)
    private String keymesure = null;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = ProduitDispositif.JPA_COLUMN_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private ProduitDispositif disppro;

    @OneToMany(mappedBy = "mesurePhysicoChimiePROMoy", cascade = ALL)
    List<ValeurPhysicoChimiePROMoy> valeurPhysicoChimiePROMoy = new LinkedList();

    /**
     *
     */
    public MesurePhysicoChimiePROMoy() {
        super();
    }

    /**
     *
     * @param ligneFichierEchange
     * @param LocalDateprelevement
     * @param nom_laboratoire
     * @param nbre_repetition
     * @param versionfile
     * @param disppro
     */
    public MesurePhysicoChimiePROMoy(Long ligneFichierEchange, LocalDate LocalDateprelevement, String nom_laboratoire, int nbre_repetition, VersionFile versionfile, ProduitDispositif disppro) {
        super(LocalDateprelevement, ligneFichierEchange, versionfile);
        this.nom_laboratoire = nom_laboratoire;
        this.nbre_repetition = nbre_repetition;
        this.disppro = disppro;

    }

    /**
     *
     * @return
     */
    public String getNom_laboratoire() {
        return nom_laboratoire;
    }

    /**
     *
     * @param nom_laboratoire
     */
    public void setNom_laboratoire(String nom_laboratoire) {
        this.nom_laboratoire = nom_laboratoire;
    }

    /**
     *
     * @return
     */
    public int getNbre_repetition() {
        return nbre_repetition;
    }

    /**
     *
     * @param nbre_repetition
     */
    public void setNbre_repetition(int nbre_repetition) {
        this.nbre_repetition = nbre_repetition;
    }

    /**
     *
     * @return
     */
    public ProduitDispositif getDisppro() {
        return disppro;
    }

    /**
     *
     * @param disppro
     */
    public void setDisppro(ProduitDispositif disppro) {
        this.disppro = disppro;
    }

    /**
     *
     * @return
     */
    public List<ValeurPhysicoChimiePROMoy> getValeurPhysicoChimiePROMoy() {
        return valeurPhysicoChimiePROMoy;
    }

    /**
     *
     * @param valeurPhysicoChimiePROMoy
     */
    public void setValeurPhysicoChimiePROMoy(List<ValeurPhysicoChimiePROMoy> valeurPhysicoChimiePROMoy) {
        this.valeurPhysicoChimiePROMoy = valeurPhysicoChimiePROMoy;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

}
