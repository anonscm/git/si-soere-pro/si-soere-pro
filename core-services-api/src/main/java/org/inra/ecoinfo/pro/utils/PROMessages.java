/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author adiankha
 */
public class PROMessages {
private static ILocalizationManager localizationManager;

    /**
     *
     */
    public static final String          PRO_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.pro.dataset.messages";
    
    /**
     *
     * @param key
     * @return
     */
    public static String getPROMessage(final String key){
        return PROMessages.getPROMessageWithBundle(PROMessages.PRO_DATASET_BUNDLE_NAME,key);
    }

    /**
     *
     * @param bundlePath
     * @param key
     * @return
     */
    public static String getPROMessageWithBundle(final String bundlePath, final String key) {
        return PROMessages.localizationManager == null ? key : PROMessages.localizationManager.getMessage(bundlePath, key);
    }

    /**
     *
     * @param localizationManager
     */
    public static void setLocalizationManager(ILocalizationManager localizationManager) {
        PROMessages.localizationManager = localizationManager;
    }

    /**
     *
     */
    public PROMessages() {
    }
    
}
