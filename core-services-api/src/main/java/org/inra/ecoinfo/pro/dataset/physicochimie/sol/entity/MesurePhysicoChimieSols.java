/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name = MesurePhysicoChimieSols.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    EchantillonsSol.ID_JPA,
    MesurePhysicoChimie.COLUMN_JPA_LOCAL_DATE,
    MesurePhysicoChimieSols.COLUMN_NOM_LABO,
    MesurePhysicoChimieSols.COLUMN_REP_ANALYSE}),
        indexes = {
            @Index(name = "mesure_physico_chimie_sol_echan_idx", columnList = EchantillonsSol.ID_JPA)
        })
@PrimaryKeyJoinColumn(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, referencedColumnName = MesurePhysicoChimieSols.ID_JPA)
@AttributeOverrides({
    @AttributeOverride(name = MesurePhysicoChimie.ATTRIBUTE_JPA_ID, column = @Column(name = MesurePhysicoChimieSols.ID_JPA))})
public class MesurePhysicoChimieSols extends MesurePhysicoChimie<MesurePhysicoChimieSols, ValeurPhysicoChimieSols> {

    /**
     *
     */
    public static final String JPA_NAME_TABLE = "mesurephysicochimiesols";

    /**
     *
     */
    public static final String ID_JPA = "msols_id";

    /**
     *
     */
    public static final String COLUMN_JPA_LocalDate = "LocalDate_prelevement";

    /**
     *
     */
    public static final String COLUMN_NOM_LABO = "nom_laboratoire";

    /**
     *
     */
    public static final String COLUMN_LAB_NUM_ECH = "numero_laboratoire";

    /**
     *
     */
    public static final String COLUMN_REP_ANALYSE = "numero_repetition";

    /**
     *
     */
    public static final String COLUMN_KEY_MESURE = "keymesure";

    static final String RESOURCE_PATH = "%s/%s";

    @Column(name = MesurePhysicoChimieSols.COLUMN_NOM_LABO, nullable = false)
    private String nom_laboratoire = "";

    @Column(name = MesurePhysicoChimieSols.COLUMN_REP_ANALYSE, nullable = false)
    private int numero_repetition = 0;

    @Column(name = MesurePhysicoChimieSols.COLUMN_LAB_NUM_ECH, nullable = false)
    private int numero_laboratoire = 0;

    @Column(name = MesurePhysicoChimieSols.COLUMN_KEY_MESURE, nullable = false, unique = true)
    private String keymesure = null;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = EchantillonsSol.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private EchantillonsSol echantillon;

    @OneToMany(mappedBy = "mesurePhysicoChimieSols", cascade = ALL)
    List<ValeurPhysicoChimieSols> valeurPhysicoChimieSols = new LinkedList();

    /**
     *
     */
    public MesurePhysicoChimieSols() {
        super();
    }

    /**
     *
     * @param LocalDate_prelevement
     * @param versionfile
     * @param echantillon
     * @param repetition
     * @param numlabo
     * @param nomlabo
     * @param ligneFichierEchange
     */
    public MesurePhysicoChimieSols(LocalDate LocalDate_prelevement, VersionFile versionfile, EchantillonsSol echantillon,
            int repetition, int numlabo, String nomlabo,
            Long ligneFichierEchange) {
        super(LocalDate_prelevement, ligneFichierEchange, versionfile);
        this.echantillon = echantillon;
        this.nom_laboratoire = nomlabo;
        this.numero_repetition = repetition;
        this.numero_laboratoire = numlabo;
    }

    /**
     *
     * @return
     */
    public String getNom_laboratoire() {
        return nom_laboratoire;
    }

    /**
     *
     * @param nom_laboratoire
     */
    public void setNom_laboratoire(String nom_laboratoire) {
        this.nom_laboratoire = nom_laboratoire;
    }

    /**
     *
     * @return
     */
    public int getNumero_repetition() {
        return numero_repetition;
    }

    /**
     *
     * @param numero_repetition
     */
    public void setNumero_repetition(int numero_repetition) {
        this.numero_repetition = numero_repetition;
    }

    /**
     *
     * @return
     */
    public int getNumero_laboratoire() {
        return numero_laboratoire;
    }

    /**
     *
     * @param numero_laboratoire
     */
    public void setNumero_laboratoire(int numero_laboratoire) {
        this.numero_laboratoire = numero_laboratoire;
    }

    /**
     *
     * @return
     */
    public EchantillonsSol getEchantillon() {
        return echantillon;
    }

    /**
     *
     * @param echantillon
     */
    public void setEchantillon(EchantillonsSol echantillon) {
        this.echantillon = echantillon;
    }

    /**
     *
     * @return
     */
    public List<ValeurPhysicoChimieSols> getValeurPhysicoChimieSols() {
        return valeurPhysicoChimieSols;
    }

    /**
     *
     * @param valeurPhysicoChimieSols
     */
    public void setValeurPhysicoChimieSols(List<ValeurPhysicoChimieSols> valeurPhysicoChimieSols) {
        this.valeurPhysicoChimieSols = valeurPhysicoChimieSols;
    }

    /**
     *
     * @return
     */
    public String getKeymesure() {
        return keymesure;
    }

    /**
     *
     * @param keymesure
     */
    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

}
