/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.proetudie;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;

/**
 *
 * @author adiankha
 */
@Entity(name = "ProetudieSynthesisValue")
@Table(indexes = {
    @Index(name = "ProetudieSynthesisValue_disp_idx", columnList = "site"),
    @Index(name = "ProetudieSynthesisValue_idNode_idx", columnList = "idNode"),
    @Index(name = "ProetudieSynthesisValue_disp_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends AbstractProSynthesisValue{
    
    /**
     *
     */
    public SynthesisValue() {
        super();
    }
    
    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param valueFloat
     * @param valueString
     * @param idNode
     */
    public SynthesisValue(LocalDate date, String site, String variable,String valueString, Double value, Long idNode) {
        super(date.atStartOfDay(), site, variable, value, valueString, idNode);
    }
}
