package org.inra.ecoinfo.pro.refdata.systemeconduite;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * RefDataConstantes.COLUMN_LIBELLE_STC
 *
 * @author ptcherniati
 */
@Entity
@Table(name = SystemeConduiteEssai.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.COLUMN_LIBELLE_STC}))
public class SystemeConduiteEssai implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.SYSTCONDUITEESSAI_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.SYSTCONDUITEESSAI_TABLE_NAME;
    @Id
    @Column(name = SystemeConduiteEssai.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_STC, length = 35)
    private String libelle;

    /**
     *
     */
    public SystemeConduiteEssai() {
        super();
    }

    /**
     *
     * @param libelle
     */
    public SystemeConduiteEssai(String libelle) {
        super();
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

}
