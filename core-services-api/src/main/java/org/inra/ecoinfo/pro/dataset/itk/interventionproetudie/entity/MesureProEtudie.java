/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureProEtudie.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    MesureProEtudie.ATTRIBUTE_JPA_DISPOSITIF,
    DescriptionTraitement.ID_JPA,
    MesureProEtudie.ATTRIBUTE_JPA_PARCELLE,
    MesureProEtudie.ATTRIBUTE_JPA_LocalDate_DEBUT,
    MesureProEtudie.ATTRIBUTE_JPA_PLACETTE,
    MesureProEtudie.ATTRIBUTE_TYPEPRO,
    MesureProEtudie.ATTRIBUTE_JPA_PRODUIT}))

public class MesureProEtudie  extends MesureITK<MesureApport, ValeurApport> {

    /**
     *
     */
    public static final String ATTRIBUTE_TYPEPRO = "typeproduit";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_PRODUIT = "codeproduit";
    /**
     *
     */
    public static final String ATTRIBUTE_PRECISION_STADE = "precisionstade";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_BBCH = "codebbch";
    /**
     *
     */
    public static final String ATTRIBUTE_MATERIEL = "materielapport";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_LocalDate_ENFOUISSEMENT = "LocalDateenfouissement";

    /**
     *
     */
    public static final String ATTRIBUTE_NIVEAU = "niveauatteint";

    /**
     *
     */
    public static final String TABLE_NAME = "mesureinterventionproetudie";

    static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(MesureProEtudie.class);
    
    @Column(name = MesureProEtudie.ATTRIBUTE_TYPEPRO, nullable = false)
    private String typeproduit;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_PRODUIT, nullable = false)
    private String codeproduit;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_BBCH, nullable = false)
    private String codebbch;

    @Column(name = MesureProEtudie.ATTRIBUTE_MATERIEL, length = 300)
    private String materielapport = "";

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_LocalDate_ENFOUISSEMENT, nullable = true)
    LocalDate LocalDateenfouissement;

    @Column(name = MesureProEtudie.ATTRIBUTE_PRECISION_STADE, nullable = false)
    private String precisionstade = "";

    @Column(name = MesureProEtudie.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint = "";

    @OneToMany(mappedBy = "mesureinterventionproetudie", cascade = ALL)
    List<ValeurProEtudie> valeurproetudie = new LinkedList();

    /**
     *
     */
    public MesureProEtudie() {
        super();
    }

    /**
     *
     * @param codedispositif
     * @param traitement
     * @param nomparcelle
     * @param nomplacette
     * @param typeproduit
     * @param codeproduit
     * @param LocalDatedebut
     * @param LocalDatefin
     * @param codebbch
     * @param nomculture
     * @param typeobservation
     * @param nomobservation
     * @param precisionstade
     * @param materiel
     * @param niveau
     * @param LocalDateenfouissement
     * @param versionfile
     * @param commentaire
     */
    public MesureProEtudie(String codedispositif, DescriptionTraitement traitement, String nomparcelle, String nomplacette, String typeproduit, String codeproduit,
            LocalDate LocalDatedebut, LocalDate LocalDatefin, String codebbch, String nomculture, String typeobservation, String nomobservation, String precisionstade, String materiel, String niveau,
            LocalDate LocalDateenfouissement, VersionFile versionfile, String commentaire) {
        super(nomculture, codedispositif, commentaire, nomparcelle, nomplacette, LocalDatedebut, LocalDatefin, typeobservation, nomobservation, versionfile, traitement);
        this.typeproduit = typeproduit;
        this.codeproduit = codeproduit;
        this.codebbch = codebbch;
        this.niveauatteint = niveau;
        this.LocalDateenfouissement = LocalDateenfouissement;
        this.precisionstade = precisionstade;
        this.materielapport = materiel;
    }

    /**
     *
     * @return
     */
    public List<ValeurProEtudie> getValeurproetudie() {
        return valeurproetudie;
    }

    /**
     *
     * @param valeurproetudie
     */
    public void setValeurproetudie(List<ValeurProEtudie> valeurproetudie) {
        this.valeurproetudie = valeurproetudie;
    }

    /**
     *
     * @return
     */
    public String getTypeproduit() {
        return typeproduit;
    }

    /**
     *
     * @param typeproduit
     */
    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    /**
     *
     * @return
     */
    public String getCodeproduit() {
        return codeproduit;
    }

    /**
     *
     * @param codeproduit
     */
    public void setCodeproduit(String codeproduit) {
        this.codeproduit = codeproduit;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getMaterielapport() {
        return materielapport;
    }

    /**
     *
     * @param materielapport
     */
    public void setMaterielapport(String materielapport) {
        this.materielapport = materielapport;
    }

    /**
     *
     * @return
     */
    public LocalDate getLocalDateenfouissement() {
        return LocalDateenfouissement;
    }

    /**
     *
     * @param LocalDateenfouissement
     */
    public void setLocalDateenfouissement(LocalDate LocalDateenfouissement) {
        this.LocalDateenfouissement = LocalDateenfouissement;
    }

    /**
     *
     * @return
     */
    public String getPrecisionstade() {
        return precisionstade;
    }

    /**
     *
     * @param precisionstade
     */
    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteint() {
        return niveauatteint;
    }

    /**
     *
     * @param niveauatteint
     */
    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }
}
