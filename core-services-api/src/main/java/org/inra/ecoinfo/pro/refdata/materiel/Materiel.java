/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.materiel;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.typeintervention.TypeIntervention;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=Materiel.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {Materiel.JPA_COLUMN_NAME,
                                                                                          //Materiel.JPA_COLUMN_INTERVENTION
                                                                                          TypeIntervention.JPA_ID
}))

public class Materiel implements Serializable {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "materiel_itk";

    /**
     *
     */
    public static final String JPA_ID = "materiel_id";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "materiel_nom";

    /**
     *
     */
    public static final String JPA_COLUMN_DESCRIPTION = "materiel_description";

    /**
     *
     */
    public static final String JPA_COLUMN_SOURCE = "materiel_source";
    //public static final String JPA_COLUMN_INTERVENTION = "materiel_intervention";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENTAIRE = "materiel_commentaire";
    
    @Id
    @Column(name = Materiel.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long materiel_id;
    
    @Column(name = Materiel.JPA_COLUMN_NAME, unique = false, nullable = false, length = 60)
    private String materiel_nom;
    @Column(name = Materiel.JPA_COLUMN_DESCRIPTION, unique = false, nullable = true, length = 2048 )
    private String materiel_description;
    @Column(name = Materiel.JPA_COLUMN_SOURCE, unique = false, nullable = true, length = 2048 )
    private String materiel_source;
//    @Column(name = Materiel.JPA_COLUMN_INTERVENTION, unique = false, nullable = false, length = 50)
//    private String materiel_intervention;
    @Column(name = Materiel.JPA_COLUMN_COMMENTAIRE, unique = false, nullable = false, length = 2048)
    private String materiel_commentaire;
    
    @ManyToOne(cascade = CascadeType.PERSIST, optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeIntervention.JPA_ID,  nullable = false)
    private TypeIntervention  typeIntervention;
    
    /**
     *
     */
    public Materiel() {
        super();
    }

    /**
     *
     * @param materiel_nom
     * @param materiel_description
     * @param materiel_source
     * @param typeIntervention
     * @param materiel_commentaire
     */
    public Materiel(String materiel_nom, String materiel_description, String materiel_source, TypeIntervention  typeIntervention, String materiel_commentaire) {
        this.materiel_nom = materiel_nom;
        this.materiel_description = materiel_description;
        this.materiel_source = materiel_source;
        this.typeIntervention = typeIntervention;
        this.materiel_commentaire = materiel_commentaire;
        
    }

    /**
     *
     * @return
     */
    public long getMateriel_id() {
        return materiel_id;
    }

    /**
     *
     * @param materiel_id
     */
    public void setMateriel_id(long materiel_id) {
        this.materiel_id = materiel_id;
    }

    /**
     *
     * @return
     */
    public String getMateriel_nom() {
        return materiel_nom;
    }

    /**
     *
     * @param materiel_nom
     */
    public void setMateriel_nom(String materiel_nom) {
        this.materiel_nom = materiel_nom;
    }

    /**
     *
     * @return
     */
    public String getMateriel_description() {
        return materiel_description;
    }

    /**
     *
     * @param materiel_description
     */
    public void setMateriel_description(String materiel_description) {
        this.materiel_description = materiel_description;
    }

    /**
     *
     * @return
     */
    public String getMateriel_source() {
        return materiel_source;
    }

    /**
     *
     * @param materiel_source
     */
    public void setMateriel_source(String materiel_source) {
        this.materiel_source = materiel_source;
    }

//    public String getMateriel_intervention() {
//        return materiel_intervention;
//    }
//
//    public void setMateriel_intervention(String materiel_intervention) {
//        this.materiel_intervention = materiel_intervention;
//    }

    /**
     *
     * @return
     */

    public String getMateriel_commentaire() {
        return materiel_commentaire;
    }

    /**
     *
     * @param materiel_commentaire
     */
    public void setMateriel_commentaire(String materiel_commentaire) {
        this.materiel_commentaire = materiel_commentaire;
    }

    /**
     *
     * @return
     */
    public TypeIntervention getTypeIntervention() {
        return typeIntervention;
    }

    /**
     *
     * @param typeIntervention
     */
    public void setTypeIntervention(TypeIntervention typeIntervention) {
        this.typeIntervention = typeIntervention;
    }


    
}
