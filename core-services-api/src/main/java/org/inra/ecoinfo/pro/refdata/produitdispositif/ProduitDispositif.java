package org.inra.ecoinfo.pro.refdata.produitdispositif;

import java.io.Serializable;
import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 * 
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 *    Produits.ID_JPA,
 *    Dispositif.ID_JPA
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProduitDispositif.NAME_ENTITY_JPA,
        uniqueConstraints = @UniqueConstraint(columnNames = {
    Produits.ID_JPA,
    Dispositif.ID_JPA
}))
public class ProduitDispositif implements Serializable, Comparable<ProduitDispositif> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "produitdispositif";

    /**
     *
     */
    public static final String JPA_COLUMN_ID = "id";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";

    /**
     *
     */
    public static final String JPA_COLUMN_KEY = "code";

    /**
     *
     */
    public static final String JPA_COLUMN_MONCOE = "codeproduit";
    @Id
    @Column(name = ProduitDispositif.JPA_COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Produits.ID_JPA, nullable = false)
    private Produits produits;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;
    @Column(name = ProduitDispositif.JPA_COLUMN_COMENT, length = 300)
    private String commentaire = "";
    @Column(name = ProduitDispositif.JPA_COLUMN_KEY, nullable = false, unique = true)
    private String code = "";

    @Column(name = ProduitDispositif.JPA_COLUMN_MONCOE)
    private String codeproduit = "";

    /**
     *
     */
    public ProduitDispositif() {
        super();
    }

    /**
     *
     * @param code
     * @param codeproduit
     * @param produits
     * @param dispositif
     * @param commentaire
     */
    public ProduitDispositif(String code, String codeproduit, Produits produits, Dispositif dispositif, String commentaire) {
        this.dispositif = dispositif;
        this.produits = produits;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Produits getProduits() {
        return produits;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @param produits
     */
    public void setProduits(Produits produits) {
        this.produits = produits;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     */
    public String getCodeproduit() {
        return codeproduit;
    }

    /**
     *
     * @param codeproduit
     */
    public void setCodeproduit(String codeproduit) {
        this.codeproduit = codeproduit;
    }

    @Override
    public int compareTo(ProduitDispositif o) {
        final int compareDispositif = getProduits().compareTo(o.getProduits());
        if (compareDispositif != 0) {
            return compareDispositif;
        }
        return getDispositif().compareTo(o.getDispositif());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.produits);
        hash = 13 * hash + Objects.hashCode(this.dispositif);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitDispositif other = (ProduitDispositif) obj;
        if (!Objects.equals(this.produits, other.produits)) {
            return false;
        }
        return Objects.equals(this.dispositif, other.dispositif);
    }

}
