package org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = CaracteristiqueMatierePremieres.NAME_ENTITY_JPA, uniqueConstraints = @UniqueConstraint(columnNames = {
    TypeCaracteristiques.JPA_ID, CaracteristiqueMatierePremieres.JPA_COLUMN_CODE,}))
public class CaracteristiqueMatierePremieres implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "caracteristiquematierepremiere";

    /**
     *
     */
    public static final String ID_JPA = "cmp_id";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE = "cmp_code";

    /**
     *
     */
    public static final String JPA_COLUMN_NAME = "cmp_nom";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = CaracteristiqueMatierePremieres.ID_JPA, unique = true, updatable = false, nullable = false)
    private final long cmp_id = 0;
    @Column(name = CaracteristiqueMatierePremieres.JPA_COLUMN_CODE, nullable = false)
    private String cmp_code = "";
    @Column(name = CaracteristiqueMatierePremieres.JPA_COLUMN_NAME, nullable = false, updatable = false)
    private String cmp_nom = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = TypeCaracteristiques.JPA_ID, nullable = false)
    private TypeCaracteristiques typecaracteristique;

    /**
     *
     */
    public CaracteristiqueMatierePremieres() {
        super();
    }

    /**
     *
     * @param cmp_code
     * @param cmp_nom
     * @param tc
     */
    public CaracteristiqueMatierePremieres(String cmp_code, String cmp_nom, TypeCaracteristiques tc) {
        super();
        this.cmp_code = cmp_code;
        this.cmp_nom = cmp_nom;
        this.typecaracteristique = tc;
    }

    /**
     *
     * @return
     */
    public String getCmp_code() {
        return cmp_code;
    }

    /**
     *
     * @return
     */
    public long getCmp_id() {
        return cmp_id;
    }

    /**
     *
     * @return
     */
    public String getCmp_nom() {
        return cmp_nom;
    }

    /**
     *
     * @return
     */
    public TypeCaracteristiques getTypecaracteristique() {
        return typecaracteristique;
    }

    /**
     *
     * @param cmp_code
     */
    public void setCmp_code(String cmp_code) {
        this.cmp_code = cmp_code;
    }

    /**
     *
     * @param cmp_nom
     */
    public void setCmp_nom(String cmp_nom) {
        this.cmp_nom = cmp_nom;
    }

    /**
     *
     * @param typecaracteristique
     */
    public void setTypecaracteristique(TypeCaracteristiques typecaracteristique) {
        this.typecaracteristique = typecaracteristique;
    }
}
