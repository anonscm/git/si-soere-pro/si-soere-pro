
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;






/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name = MesureSWC.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
        SousSequenceSWC.ATTRIBUTE_JPA_ID, MesureSWC.ATTRIBUTE_JPA_TIME }))
public class MesureSWC implements Serializable{
     
    /**
     *
     */
    public static final String ATTRIBUTE_JPA_ID                = "id_mesure_swc";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_TIME              = "heure";

    /**
     *
     */
    public static final String TABLE_NAME       = "mesure_swc";
   
    static final long          serialVersionUID                = 1L;

    
    
    
    @Id
    @Column(name=MesureSWC.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long  id_mesure_swc;
    
    
    @Column(name = MesureSWC.ATTRIBUTE_JPA_TIME, nullable = false)
    LocalTime                      heure;
    
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = true, targetEntity = SousSequenceSWC.class)
    @JoinColumn(name = SousSequenceSWC.ATTRIBUTE_JPA_ID, referencedColumnName = SousSequenceSWC.ATTRIBUTE_JPA_ID,nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    SousSequenceSWC            sousSequenceSWC;
    
    
    @OneToMany(mappedBy = ValeurSWC.ID_JPA, cascade = { PERSIST, MERGE, REFRESH })
    List<ValeurSWC>            valeursSWC       = new LinkedList<ValeurSWC>();
    
    int                        lineNumber;

    /**
     *
     */
    public MesureSWC() {
        super();
    }

    /**
     *
     * @param heure
     * @param sousSequenceSWC
     * @param lineNumber
     */
    public MesureSWC(LocalTime heure, SousSequenceSWC sousSequenceSWC, int lineNumber) {
        this.heure = heure;
        this.sousSequenceSWC = sousSequenceSWC;
        this.lineNumber = lineNumber;
    }

    /**
     *
     * @return
     */
    public Long getId_mesure_swc() {
        return id_mesure_swc;
    }

    /**
     *
     * @param id_mesure_swc
     */
    public void setId_mesure_swc(Long id_mesure_swc) {
        this.id_mesure_swc = id_mesure_swc;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public SousSequenceSWC getSousSequenceSWC() {
        return sousSequenceSWC;
    }

    /**
     *
     * @param sousSequenceSWC
     */
    public void setSousSequenceSWC(SousSequenceSWC sousSequenceSWC) {
        this.sousSequenceSWC = sousSequenceSWC;
    }

    /**
     *
     * @return
     */
    public List<ValeurSWC> getValeursSWC() {
        return valeursSWC;
    }

    /**
     *
     * @param valeursSWC
     */
    public void setValeursSWC(List<ValeurSWC> valeursSWC) {
        this.valeursSWC = valeursSWC;
    }

    /**
     *
     * @return
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     *
     * @param lineNumber
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
}
