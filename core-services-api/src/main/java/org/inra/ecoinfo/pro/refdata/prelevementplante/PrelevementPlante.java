/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementplante;

import java.io.Serializable;
import java.time.LocalDate;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.biomasseprelevee.BiomassePrelevee;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.cultures.Cultures;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.methodeprelevement.MethodePrelevement;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.partieprelevee.PartiePrelevee;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.stadedeveloppement.StadeDeveloppement;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;

/**
 * @UniqueConstraint(columnNames =
 * {PrelevementPlante.JPA_COLUMN_DATE_PRELEVEMENT, Dispositif.ID_JPA,
 * DescriptionTraitement.ID_JPA, ParcelleElementaire.ID_JPA, Placette.ID_JPA,
 * Bloc.ID_JPA, Cultures.JPA_ID, PartiePrelevee.ID_JPA,
 * @author adiankha
 */
@Entity
@Table(name = PrelevementPlante.JPA_NAME_ENTITY, uniqueConstraints = @UniqueConstraint(columnNames = {
    PrelevementPlante.JPA_COLUMN_DATE_PRELEVEMENT,
    Dispositif.ID_JPA,
    DescriptionTraitement.ID_JPA,
    ParcelleElementaire.ID_JPA,
    Placette.ID_JPA,
    Bloc.ID_JPA,
    Cultures.JPA_ID,
    PartiePrelevee.ID_JPA}))
public class PrelevementPlante implements Serializable {

    /**
     *
     */
    public static final String UNDERSCORE = "_";

    /**
     *
     */
    public static final String JPA_NAME_ENTITY = "prelevementplante";

    /**
     *
     */
    public static final String ID_JPA = "prel_id";

    /**
     *
     */
    public static final String JPA_COLUMN_DATE_PRELEVEMENT = "dateplante";

    /**
     *
     */
    public static final String JPA_COLUMN_HAUTEURCOUPE = "hauteurcoupe";

    /**
     *
     */
    public static final String JPA_COLUMN_MASSE_GEOREF = "referencement";

    /**
     *
     */
    public static final String JPA_COLUMN_CODE_PLANTE = "codeplante";

    /**
     *
     */
    public static final String JPA_COLUMN_ESPECE_BBCH = "codebbch";

    /**
     *
     */
    public static final String JPA_COLUMN_COMMENTAIRE = "commentaire";
    /**
     *
     * @param date au format dd/MM/yyyy
     * @param codeDispositif
     * @param codetrait
     * @param bloc
     * @param pelementaire
     * @param placette
     * @param culture
     * @param partie
     * @return
     */
    static public String buildCodePlante(String date, String codeDispositif, String nomLieu, final String codetrait, String bloc, String pelementaire, String placette,
            String culture, String partie) {
        StringBuilder codeprel=new StringBuilder();
        date=DateUtil.getUTCDateTextFromLocalDateTime(DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, date), "dd_MM_yyyy");
        codeprel.append(date).append(UNDERSCORE)
                .append(codeDispositif).append(UNDERSCORE)
                .append(nomLieu).append(UNDERSCORE)
                .append(codetrait).append(UNDERSCORE)
                .append(bloc).append(UNDERSCORE)
                .append(pelementaire).append(UNDERSCORE)
                .append(placette).append(UNDERSCORE)
                .append(culture).append(UNDERSCORE)
                .append(partie);
        return Utils.createCodeFromString(codeprel.toString().replaceAll("_sans(?=[_$])", ""));
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = PrelevementPlante.ID_JPA, nullable = false, unique = true)
    private long prel_id;
    @Column(name = PrelevementPlante.JPA_COLUMN_DATE_PRELEVEMENT, nullable = false, unique = false)
    private LocalDate dateplante = null;
    @Column(name = PrelevementPlante.JPA_COLUMN_MASSE_GEOREF, nullable = true, unique = false)
    private String referencement = null;
    @Column(name = PrelevementPlante.JPA_COLUMN_HAUTEURCOUPE, nullable = true, unique = false)
    private double hauteurcoupe = 0.0;
    @Column(name = PrelevementPlante.JPA_COLUMN_CODE_PLANTE, nullable = false, unique = true)
    private String codeplante = "";
    @Column(name = PrelevementPlante.JPA_COLUMN_ESPECE_BBCH)
    private String codebbch = "";
    @Column(name = PrelevementPlante.JPA_COLUMN_COMMENTAIRE, length = 300)
    private String commentaire = "";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, nullable = false, unique = false)
    private DescriptionTraitement traitement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Bloc.ID_JPA, nullable = true, unique = false)
    private Bloc bloc;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false, unique = false)
    private Dispositif dispositif;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = ParcelleElementaire.ID_JPA, nullable = true, unique = false)
    private ParcelleElementaire pelementaire;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Placette.ID_JPA, nullable = true, unique = false)
    private Placette placette;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Cultures.JPA_ID, nullable = false, unique = false)
    private Cultures cultures;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = PartiePrelevee.ID_JPA, nullable = false, unique = false)
    private PartiePrelevee partiePrelevee;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = StadeDeveloppement.ID_JPA, nullable = false, unique = false)
    private StadeDeveloppement stadeDeveloppement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodePrelevement.ID_JPA, nullable = false, unique = false)
    private MethodePrelevement methodePrelevement;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = BiomassePrelevee.ID_JPA, nullable = false, unique = false)
    private BiomassePrelevee biomassePrelevee;

    /**
     *
     */
    public PrelevementPlante() {
        super();
        this.dateplante = null;
    }

    /**
     *
     * @param date_prelevement
     * @param georefe
     * @param hauteur
     * @param commentaire
     * @param codeplante
     * @param bbch
     * @param traitement
     * @param bloc
     * @param dispositif
     * @param pelementaire
     * @param placette
     * @param cultures
     * @param partiePrelevee
     * @param stadeDeveloppement
     * @param methodePrelevement
     * @param biomassePrelevee
     */
    public PrelevementPlante(LocalDate date_prelevement, String georefe, double hauteur, String commentaire, String codeplante, String bbch,
            DescriptionTraitement traitement, Bloc bloc, Dispositif dispositif, ParcelleElementaire pelementaire, Placette placette,
            Cultures cultures, PartiePrelevee partiePrelevee, StadeDeveloppement stadeDeveloppement, MethodePrelevement methodePrelevement, BiomassePrelevee biomassePrelevee) {
        this.dateplante = date_prelevement;
        this.hauteurcoupe = hauteur;
        this.commentaire = commentaire;
        this.codeplante = codeplante;
        this.referencement = georefe;
        this.traitement = traitement;
        this.bloc = bloc;
        this.dispositif = dispositif;
        this.pelementaire = pelementaire;
        this.placette = placette;
        this.codebbch = bbch;
        this.cultures = cultures;
        this.partiePrelevee = partiePrelevee;
        this.stadeDeveloppement = stadeDeveloppement;
        this.methodePrelevement = methodePrelevement;
        this.biomassePrelevee = biomassePrelevee;
    }

    /**
     *
     * @return
     */
    public long getPrel_id() {
        return prel_id;
    }

    /**
     *
     * @param prel_id
     */
    public void setPrel_id(long prel_id) {
        this.prel_id = prel_id;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateplante() {
        return dateplante;
    }

    /**
     *
     * @param dateplante
     */
    public void setDateplante(LocalDate dateplante) {
        this.dateplante = dateplante;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getReferencement() {
        return referencement;
    }

    /**
     *
     * @param referencement
     */
    public void setReferencement(String referencement) {
        this.referencement = referencement;
    }

    /**
     *
     * @return
     */
    public double getHauteurcoupe() {
        return hauteurcoupe;
    }

    /**
     *
     * @param hauteurcoupe
     */
    public void setHauteurcoupe(double hauteurcoupe) {
        this.hauteurcoupe = hauteurcoupe;
    }

    /**
     *
     * @return
     */
    public String getCodeplante() {
        return codeplante;
    }

    /**
     *
     * @param codeplante
     */
    public void setCodeplante(String codeplante) {
        this.codeplante = codeplante;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public DescriptionTraitement getTraitement() {
        return traitement;
    }

    /**
     *
     * @param traitement
     */
    public void setTraitement(DescriptionTraitement traitement) {
        this.traitement = traitement;
    }

    /**
     *
     * @return
     */
    public Bloc getBloc() {
        return bloc;
    }

    /**
     *
     * @param bloc
     */
    public void setBloc(Bloc bloc) {
        this.bloc = bloc;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @return
     */
    public ParcelleElementaire getPelementaire() {
        return pelementaire;
    }

    /**
     *
     * @param pelementaire
     */
    public void setPelementaire(ParcelleElementaire pelementaire) {
        this.pelementaire = pelementaire;
    }

    /**
     *
     * @return
     */
    public Placette getPlacette() {
        return placette;
    }

    /**
     *
     * @param placette
     */
    public void setPlacette(Placette placette) {
        this.placette = placette;
    }

    /**
     *
     * @return
     */
    public Cultures getCultures() {
        return cultures;
    }

    /**
     *
     * @param cultures
     */
    public void setCultures(Cultures cultures) {
        this.cultures = cultures;
    }

    /**
     *
     * @return
     */
    public PartiePrelevee getPartiePrelevee() {
        return partiePrelevee;
    }

    /**
     *
     * @param partiePrelevee
     */
    public void setPartiePrelevee(PartiePrelevee partiePrelevee) {
        this.partiePrelevee = partiePrelevee;
    }

    /**
     *
     * @return
     */
    public StadeDeveloppement getStadeDeveloppement() {
        return stadeDeveloppement;
    }

    /**
     *
     * @param stadeDeveloppement
     */
    public void setStadeDeveloppement(StadeDeveloppement stadeDeveloppement) {
        this.stadeDeveloppement = stadeDeveloppement;
    }

    /**
     *
     * @return
     */
    public MethodePrelevement getMethodePrelevement() {
        return methodePrelevement;
    }

    /**
     *
     * @param methodePrelevement
     */
    public void setMethodePrelevement(MethodePrelevement methodePrelevement) {
        this.methodePrelevement = methodePrelevement;
    }

    /**
     *
     * @return
     */
    public BiomassePrelevee getBiomassePrelevee() {
        return biomassePrelevee;
    }

    /**
     *
     * @param biomassePrelevee
     */
    public void setBiomassePrelevee(BiomassePrelevee biomassePrelevee) {
        this.biomassePrelevee = biomassePrelevee;
    }

}
