package org.inra.ecoinfo.pro.refdata.melange;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.precisionpourcentage.PrecisionPourcentage;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 * uniqueConstraints = @UniqueConstraint(columnNames = {
 * Melange.JPA_COLUMN_CENT, Produits.ID_JPA, Composant.JPA_ID
 *
 * @author ptcherniati
 */
@Entity
@Table(name = Melange.NAME_ENTITY_JAP, uniqueConstraints
        = @UniqueConstraint(columnNames = {
    Melange.JPA_COLUMN_CENT,
    Produits.ID_JPA,
    Composant.JPA_ID
}))
public class Melange implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JAP = "melange";

    /**
     *
     */
    public static final String JPA_ID = "melange_id";

    /**
     *
     */
    public static final String JPA_COLUMN_REFERENCE = "reference";

    /**
     *
     */
    public static final String JPA_COLUMN_CENT = "pourcentage";

    /**
     *
     */
    public static final String JPA_COLUMN_VALUE = "commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = Melange.JPA_ID, nullable = false, unique = true, updatable = false)
    private long melange_id = 0;
    @Column(name = Melange.JPA_COLUMN_CENT, nullable = false)
    private double pourcentage = 0.0;
    @Column(name = Melange.JPA_COLUMN_VALUE)
    private String commentaire = "";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Composant.JPA_ID, updatable = true, nullable = false, unique = false)
    private Composant composant;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Produits.ID_JPA, updatable = true, nullable = false, unique = false)
    private Produits produits;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = PrecisionPourcentage.JPA_ID, updatable = true, nullable = false, unique = false)
    private PrecisionPourcentage precisionpourcentage;

    /**
     *
     */
    public Melange() {
        super();
    }

    /**
     *
     * @param produits
     * @param composant
     * @param precipourcent
     * @param pourcentage
     * @param commentaire
     */
    public Melange(Produits produits, Composant composant, PrecisionPourcentage precipourcent, double pourcentage, String commentaire) {
        super();
        this.composant = composant;
        this.produits = produits;
        this.commentaire = commentaire;
        this.pourcentage = pourcentage;
        this.precisionpourcentage = precipourcent;
    }

    /**
     *
     * @return
     */
    public String getCode_produit() {
        return produits.getProd_codeuser();
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @return
     */
    public Composant getComposant() {
        return composant;
    }

    /**
     *
     * @return
     */
    public long getMelange_id() {
        return melange_id;
    }

    /**
     *
     * @return
     */
    public double getPourcentage() {
        return pourcentage;
    }

    /**
     *
     * @return
     */
    public Produits getProduits() {
        return produits;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @param composant
     */
    public void setComposant(Composant composant) {
        this.composant = composant;
    }

    /**
     *
     * @param melange_id
     */
    public void setMelange_id(long melange_id) {
        this.melange_id = melange_id;
    }

    /**
     *
     * @param pourcentage
     */
    public void setPourcentage(double pourcentage) {
        this.pourcentage = pourcentage;
    }

    /**
     *
     * @param produits
     */
    public void setProduits(Produits produits) {
        this.produits = produits;
    }

    /**
     *
     * @return
     */
    public boolean getReference() {
        return (composant instanceof Produits);
    }

    /**
     *
     * @param precisionpourcentage
     */
    public void setPrecisionpourcentage(PrecisionPourcentage precisionpourcentage) {
        this.precisionpourcentage = precisionpourcentage;
    }

    /**
     *
     * @return
     */
    public PrecisionPourcentage getPrecisionpourcentage() {
        return precisionpourcentage;
    }

}
