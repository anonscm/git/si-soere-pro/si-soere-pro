
package org.inra.ecoinfo.pro.refdata.listeraisonnement;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ListeRaisonnement.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_LRAIS}))
public class ListeRaisonnement implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.LSTRAISONNEMENT_ID; // lrais_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.LSTRAISONNEMENT_TABLE_NAME; // liste_raisonnement
    @Id
    @Column(name = ListeRaisonnement.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_LRAIS, length = 80)
    private String libelle;

    /**
     *
     */
    public ListeRaisonnement() {
        super();
    }

    /**
     *
     * @param libelle
     */
    public ListeRaisonnement(String libelle) {
        super();
        this.libelle = libelle;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
