/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name = ValeurTS.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    MesureTS.ATTRIBUTE_JPA_ID, RealNode.ID_JPA, ValeurTS.ATTRIBUTE_JPA_NUM_REPETITION}))

public class ValeurTS implements Serializable {
    
    /**
     *
     */
    public static final String ID_JPA = "vts_id";

    /**
     *
     */
    public static final String TABLE_NAME = "valeur_ts";

    /**
     *
     */
    public static final String COLUMN_JPA_VALUE = "valeur";

    /**
     *
     */
    public static final String  ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";

    /**
     *
     */
    public static final String ATTRIBUTE_JPA_NUM_REPETITION = "num_repetition";
    
   
    static final long serialVersionUID = 1L;

    
       
      
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long  vts_id;
    
    @Column(name = ValeurTS.COLUMN_JPA_VALUE, nullable = false)
    private Float valeur;
    
    @Column(name=ValeurTS.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;
    
    @Column(name = ValeurTS.ATTRIBUTE_JPA_NUM_REPETITION, nullable = false)
    int                        numRepetition;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = MesureTS.ATTRIBUTE_JPA_ID, referencedColumnName = MesureTS.ATTRIBUTE_JPA_ID)
    @LazyToOne(LazyToOneOption.PROXY)
    MesureTS mesureTS;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = RealNode.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
     private RealNode realNode;

    @Column(name = "ligne_fichier_echange")
    private Long ligneFichierEchange;

    /**
     *
     */
    public ValeurTS() {
        super();
    }

    /**
     *
     * @param valeur
     * @param statutvaleur
     * @param numRepetition
     * @param mesureTS
     * @param realNode
     * @param ligneFichierEchange
     */
    public ValeurTS(Float valeur, String statutvaleur, int numRepetition, MesureTS mesureTS, RealNode realNode, Long ligneFichierEchange) {
        this.valeur = valeur;
        this.statutvaleur = statutvaleur;
        this.numRepetition = numRepetition;
        this.mesureTS = mesureTS;
        this.realNode = realNode;
        this.ligneFichierEchange = ligneFichierEchange;
    }
    
    /**
     *
     * @return
     */
    public Long getVts_id() {
        return vts_id;
    }

    /**
     *
     * @param vts_id
     */
    public void setVts_id(Long vts_id) {
        this.vts_id = vts_id;
    }

    /**
     *
     * @return
     */
    public Float getValeur() {
        return valeur;
    }

    /**
     *
     * @param valeur
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public int getNumRepetition() {
        return numRepetition;
    }

    /**
     *
     * @param numRepetition
     */
    public void setNumRepetition(int numRepetition) {
        this.numRepetition = numRepetition;
    }

    /**
     *
     * @return
     */
    public MesureTS getMesureTS() {
        return mesureTS;
    }

    /**
     *
     * @param mesureTS
     */
    public void setMesureTS(MesureTS mesureTS) {
        this.mesureTS = mesureTS;
    }

    /**
     *
     * @return
     */
    public RealNode getRealNode() {
        return realNode;
    }

    /**
     *
     * @param realNode
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    public Long getLigneFichierEchange() {
        return ligneFichierEchange;
    }

    /**
     *
     * @param ligneFichierEchange
     */
    public void setLigneFichierEchange(Long ligneFichierEchange) {
        this.ligneFichierEchange = ligneFichierEchange;
    }
    
    
}
