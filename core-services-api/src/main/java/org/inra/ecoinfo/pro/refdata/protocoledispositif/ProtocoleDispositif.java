package org.inra.ecoinfo.pro.refdata.protocoledispositif;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProtocoleDispositif.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
    RefDataConstantes.PROTOCOLE_ID, 
    RefDataConstantes.DISPOSITIF_ID
}))
public class ProtocoleDispositif implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PROTOCOLEDISPOSITIF_ID; // prtdisp_id

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PROTOCOLEDISPOSITIF_TABLE_NAME; // protocole_dispositif
    @Id
    @Column(name = ProtocoleDispositif.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Protocole.class)
    @JoinColumn(name = RefDataConstantes.PROTOCOLE_ID, referencedColumnName = Protocole.ID_JPA, nullable = false)
    private Protocole protocole;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Dispositif.class)
    @JoinColumn(name = Dispositif.ID_JPA, nullable = false)
    private Dispositif dispositif;

    /**
     *
     */
    public ProtocoleDispositif() {
        super();
    }

    /**
     *
     * @param protocole
     * @param dispositif
     */
    public ProtocoleDispositif(Protocole protocole, Dispositif dispositif) {
        super();
        this.protocole = protocole;
        this.dispositif = dispositif;
    }

    /**
     *
     * @return
     */
    public Dispositif getDispositif() {
        return dispositif;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public Protocole getProtocole() {
        return protocole;
    }

    /**
     *
     * @param dispositif
     */
    public void setDispositif(Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param protocole
     */
    public void setProtocole(Protocole protocole) {
        this.protocole = protocole;
    }
}
