package org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.methodeprocede.MethodeProcedes;

/**
 * MethodeProcedes.JPA_ID, 
  *    MethodeEtapes.ID_JPA, 
  *    Etapes.ID_JPA, 
  *    MethodeProcedeEtapes.NAME_COLUMN_ORDER_JPA
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MethodeProcedeEtapes.NAME_ENTITY_JPA, 
        uniqueConstraints = @UniqueConstraint(
columnNames = {
    MethodeProcedes.JPA_ID, 
    MethodeEtapes.ID_JPA, 
    Etapes.ID_JPA, 
    MethodeProcedeEtapes.NAME_COLUMN_ORDER_JPA
}))
public class MethodeProcedeEtapes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "produitprocedeetape";

    /**
     *
     */
    public static final String JPA_ID = "mpe_id";

    /**
     *
     */
    public static final String JPA_COLUMN_ETAPE = "etape";

    /**
     *
     */
    public static final String JPA_COLUMN_METAPE = "metape";

    /**
     *
     */
    public static final String NAME_COLUMN_ORDER_JPA = "ordre";

    /**
     *
     */
    public static final String JPA_COLUMN_COMENT = "commentaire";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = MethodeProcedeEtapes.JPA_ID, nullable = false, updatable = false)
    private long mpe_id = 0;
    @Column(name = MethodeProcedeEtapes.NAME_COLUMN_ORDER_JPA, nullable = false)
    private int ordre = 0;
     @Column(name=MethodeProcedeEtapes.JPA_COLUMN_COMENT,length = 300)
    private String commentaire ="";
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodeProcedes.JPA_ID, nullable = false)
    private MethodeProcedes methodeprocedes;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = MethodeEtapes.ID_JPA, nullable = false)
    private MethodeEtapes methodeetapes;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Etapes.ID_JPA, nullable = false)
    private Etapes etapes;

    /**
     *
     */
    public MethodeProcedeEtapes() {
        super();
    }

    /**
     *
     * @param methodeprocede
     * @param etapes
     * @param methodeetapes
     * @param ordre
     * @param commentaire
     */
    public MethodeProcedeEtapes(MethodeProcedes methodeprocede, Etapes etapes, MethodeEtapes methodeetapes, int ordre,String commentaire)
    {
        this.ordre = ordre;
        this.methodeprocedes = methodeprocede;
        this.etapes = etapes;
        this.methodeetapes = methodeetapes;
        this.commentaire = commentaire;
    }

    /**
     *
     * @return
     */
    public Etapes getEtapes() {
        return etapes;
    }

    /**
     *
     * @return
     */
    public MethodeEtapes getMethodeetapes() {
        return methodeetapes;
    }

    /**
     *
     * @return
     */
    public MethodeProcedes getMethodeprocedes() {
        return methodeprocedes;
    }

    /**
     *
     * @return
     */
    public long getMpe_id() {
        return mpe_id;
    }

    /**
     *
     * @return
     */
    public int getOrdre() {
        return ordre;
    }

    /**
     *
     * @param etapes
     */
    public void setEtapes(Etapes etapes) {
        this.etapes = etapes;
    }

    /**
     *
     * @param methodeetapes
     */
    public void setMethodeetapes(MethodeEtapes methodeetapes) {
        this.methodeetapes = methodeetapes;
    }

    /**
     *
     * @param methodeprocedes
     */
    public void setMethodeprocedes(MethodeProcedes methodeprocedes) {
        this.methodeprocedes = methodeprocedes;
    }

    /**
     *
     * @param mpe_id
     */
    public void setMpe_id(long mpe_id) {
        this.mpe_id = mpe_id;
    }

    /**
     *
     * @param ordre
     */
    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }
    
    
}
