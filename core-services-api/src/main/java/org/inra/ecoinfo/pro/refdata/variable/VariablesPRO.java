/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.variable;

import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.refdata.valeurqualitative.ValeurQualitative;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author adiankha
 */
@Entity()
@Table(name = VariablesPRO.NAME_ENTITY_JPA)
@PrimaryKeyJoinColumn(name = VariablesPRO.ID_JPA)
public class VariablesPRO extends Variable {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "variablesPRO";

    /**
     *
     */
    public static final String JPA_ID = "id";
    static final long serialVersionUID = 1L;

    @OneToMany(cascade = ALL)
    List<ValeurQualitative> valeursQualitatives = new LinkedList();
    /**
     * The affichage @link(String).
     */
    @Column(nullable = false, unique = true)
    private String codeUser;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = CategorieVariable.JPA_ID, nullable = true, unique = false)
    private CategorieVariable categorievariable;

    /**
     *
     */
    public VariablesPRO() {
        super();

    }

    /**
     *
     * @param id
     */
    public VariablesPRO(final Long id) {
        super(id);
    }

    /**
     *
     * @param mycode
     * @param nom
     * @param codeUser
     * @param categorie
     * @param definition
     * @param isQualitative
     */
    public VariablesPRO(String code, String affichage, String definition, final boolean isQualitative, String codeUser, CategorieVariable categorie) {
        super(code, definition, affichage, isQualitative);
        this.codeUser = codeUser;
        this.categorievariable = categorie;

    }

    @Override
    public int compareTo(INodeable o) {
        if (o instanceof VariablesPRO) {
            return this.getCode().compareTo(((Variable) o).getCode());
        }
        return -1;
    }

    /**
     *
     * @return
     */
    public List<ValeurQualitative> getValeursQualitatives() {
        return valeursQualitatives;
    }

    /**
     *
     * @param valeursQualitatives
     */
    public void setValeursQualitatives(List<ValeurQualitative> valeursQualitatives) {
        this.valeursQualitatives = valeursQualitatives;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj, new String[]{"id"});
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public CategorieVariable getCategorievariable() {
        return categorievariable;
    }

    /**
     *
     * @param categorievariable
     */
    public void setCategorievariable(CategorieVariable categorievariable) {
        this.categorievariable = categorievariable;
    }

    public String getCodeUser() {
        return codeUser;
    }

    public void setCodeUser(String codeUser) {
        this.codeUser = codeUser;
    }

}
