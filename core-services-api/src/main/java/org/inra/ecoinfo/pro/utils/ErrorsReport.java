/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;


/**
 *
 * @author adiankha
 */
public class ErrorsReport implements IErrorsReport{

   static final String CST_HYPHEN       = "-";
     
      static final String CST_NEW_LINE     = "\n";
      
      static final String CST_SPACE_TAB    = "      ";
    InfoReport          infoReport       = new InfoReport();
    
     String              errorsMessages   = org.apache.commons.lang.StringUtils.EMPTY;

    /**
     *
     * @param errorMessage
     */
    @Override
    public void addErrorMessage(String errorMessage) {
        this.errorsMessages = this.errorsMessages.concat(ErrorsReport.CST_HYPHEN)
                .concat(errorMessage).concat(ErrorsReport.CST_NEW_LINE);
    }

    /**
     *
     * @param errorMessage
     * @param e
     */
    @Override
    public void addErrorMessage(String errorMessage, Throwable e) {
         this.errorsMessages = this.errorsMessages.concat(ErrorsReport.CST_HYPHEN)
                .concat(errorMessage).concat(ErrorsReport.CST_NEW_LINE)
                .concat(ErrorsReport.CST_SPACE_TAB).concat(e.getMessage())
                .concat(ErrorsReport.CST_NEW_LINE);
    }

    /**
     *
     * @param infoMessage
     */
    @Override
    public void addInfoMessage(String infoMessage) {
        this.infoReport.addInfoMessage(infoMessage);
    }

    /**
     *
     * @return
     */
    @Override
    public String getErrorsMessages() {
        return this.errorsMessages;
    }

    /**
     *
     * @return
     */
    @Override
    public String getInfosMessages() {
        return this.infoReport.getInfoMessages();
    }

    /**
     *
     * @return
     */
    @Override
    public boolean hasErrors() {
        return this.errorsMessages.length() > 0;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean hasInfos() {
         return this.infoReport.hasInfo();
    } 
    
}
