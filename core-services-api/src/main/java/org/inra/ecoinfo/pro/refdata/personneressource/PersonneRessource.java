
package org.inra.ecoinfo.pro.refdata.personneressource;
import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = PersonneRessource.TABLE_NAME)
public class PersonneRessource implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public static final String ID_JPA = RefDataConstantes.PERSONNERESSOURCE_ID;

    /**
     *
     */
    public static final String TABLE_NAME = RefDataConstantes.PERSONNERESSOURCE_TABLE_NAME;
    @Id
    @Column(name = PersonneRessource.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_NOM_PERSONNERESSOURCE, length = 100)
    private String nom;
    @Column(nullable = false, unique = false, name = RefDataConstantes.COLUMN_PRENOM_PERSONNERESSOURCE, length = 50)
    private String prenom;
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_EMAIL_PERSONNERESSOURCE)
    private String email;
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = Structure.class)
    @JoinColumn(name = RefDataConstantes.STRUCTURE_ID, referencedColumnName = Structure.ID_JPA, nullable = false)
    private Structure structure;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_FONCTION_PERSONNERESSOURCE, length = 100)
    private String fonction;
    @Column(nullable = true, unique = false, name = RefDataConstantes.COLUMN_NOTELPOSTE_PERSONNERESSOURCE)
    private String noTelPoste;

    /**
     *
     */
    public PersonneRessource() {
        super();
    }

    /**
     *
     * @param nom
     * @param prenom
     * @param email
     * @param structure
     * @param fonction
     * @param noTelPoste
     */
    public PersonneRessource(String nom, String prenom, String email, Structure structure, String fonction, String noTelPoste) {
        super();
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.structure = structure;
        this.fonction = fonction;
        this.noTelPoste = noTelPoste;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @return
     */
    public String getFonction() {
        return fonction;
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @return
     */
    public String getNoTelPoste() {
        return noTelPoste;
    }

    /**
     *
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     *
     * @return
     */
    public Structure getStructure() {
        return structure;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @param fonction
     */
    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @param noTelPoste
     */
    public void setNoTelPoste(String noTelPoste) {
        this.noTelPoste = noTelPoste;
    }

    /**
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     *
     * @param structure
     */
    public void setStructure(Structure structure) {
        this.structure = structure;
    }
}
