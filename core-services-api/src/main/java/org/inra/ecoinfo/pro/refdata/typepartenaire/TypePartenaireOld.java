package org.inra.ecoinfo.pro.refdata.typepartenaire;
/**
 *
 */
/*package org.inra.ecoinfo.pro.refdata.typepartenaire;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;

@Entity
@Table(name = TypePartenaire.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_LIBELLE_TPPAR}))
public class TypePartenaire implements Serializable 
{
    private static final long serialVersionUID = 1L;
    public static final String ID_JPA = RefDataConstantes.TYPEPARTENAIRE_ID; // tppar_id
    public static final String TABLE_NAME = RefDataConstantes.TYPEPARTENAIRE_TABLE_NAME; // type_partenaire
    
    @Id
    @Column(name = TypePartenaire.ID_JPA)
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    
    @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_LIBELLE_TPPAR, length = 100)
    private String libelle;
    
    public TypePartenaire() 
    {
        super();
    }
    
    public TypePartenaire(String libelle) 
    {
        super();
        this.libelle = libelle;
    }
    
    public Long getId() {
        return id;
    }
    
    public String getLibelle() {
        return libelle;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
*/
