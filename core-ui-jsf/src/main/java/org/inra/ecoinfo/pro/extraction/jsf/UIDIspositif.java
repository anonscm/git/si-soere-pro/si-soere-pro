/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

import com.google.common.base.Strings;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author tcherniatinsky
 */
public class UIDIspositif {

    /**
     * The properties names dispositif @link(Properties).
     */
    static Properties propertiesNamesDispositifs;
    /**
     * The dispositifs dispositifs @link(Map<Long,TraitementVO>).
     */
    final Map<Long, DispositifVO> dispositifs = new HashMap();

    /**
     * The availables lieux.
     */
    TreeNode availablesLieux = new DefaultTreeNode("Root", null);
    private TreeNode selectedNode;
    private ILocalizationManager localizationManager;

    /**
     * Gets the dispositifs dispositifs.
     *
     * @return the dispositifs dispositifs
     */
    public Map<Long, DispositifVO> getDispositif() {
        return this.dispositifs;
    }

    /**
     * Adds the all dispositifs.
     *
     * @return the string
     */
    public final String addAllDispositifs() {
        dispositifs.clear();
        this.availablesLieux.getChildren().forEach((lieuNode) -> {
            lieuNode.getChildren().forEach((dispositifNode) -> {
                DispositifVO dispositif = (DispositifVO) dispositifNode.getData();
                dispositif.setSelected(true);
                dispositifs.put(dispositif.dispositif.getId(), dispositif);
                setExpanded(dispositifNode, true);
            });
        });
        return null;
    }

    /**
     * Removes the all dispositifs.
     *
     * @return the string
     */
    public final String removeAllDispositifs() {
        dispositifs.clear();
        this.availablesLieux.getChildren().forEach((lieuNode) -> {
            lieuNode.getChildren().forEach((dispositifNode) -> {
                DispositifVO dispositifVO = (DispositifVO) dispositifNode.getData();
                dispositifVO.setSelected(false);
                setExpanded(dispositifNode, false);
            });
        });
        return null;
    }

    /**
     * Inits the properties.
     *
     * @param localizationManager
     */
    public void initProperties(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
        this.propertiesNamesDispositifs = localizationManager.newProperties(Nodeable.getLocalisationEntite(Dispositif.class), Nodeable.ENTITE_COLUMN_NAME);
    }

    /**
     * Select dispositif.
     *
     * @param event
     */
    public final void selectDispositif(NodeSelectEvent event) {
        TreeNode dispositifLeaf = event.getTreeNode();
        if (NodeType.DISPOSITIF.equals(dispositifLeaf.getType())) {
            DispositifVO dispositifVO = (DispositifVO) dispositifLeaf.getData();
            if (dispositifVO.isSelected()) {
                dispositifs.remove(dispositifVO.getDispositif().getId());
                dispositifVO.setSelected(false);
            } else {
                dispositifs.put(dispositifVO.getDispositif().getId(), dispositifVO);
                dispositifVO.setSelected(true);
                setExpanded(dispositifLeaf, true);
            }
        }
    }

    /**
     * Update lieu availables.
     *
     * @param dispositifManager
     * @param intervalsDate
     */
    public final void updateLieuAvailables(IDispositifManager dispositifManager) {
        this.availablesLieux = new DefaultTreeNode("Root", null);
        final Set<Dispositif> availablesDispositifs = new TreeSet();
        availablesDispositifs.addAll(dispositifManager.getAvailablesDispositifs());
        Map<Lieu, TreeNode> lieuxNodes = new HashMap();
        TreeNode lieuTreeNode;
        for (final Dispositif dispositif : availablesDispositifs) {
            Lieu lieu = dispositif.getLieu();
            if (!lieuxNodes.containsKey(lieu)) {
                lieuxNodes.put(lieu, new DefaultTreeNode(new NodeableNode(new NodeableLieu(lieu)), availablesLieux));
            }
            lieuTreeNode = lieuxNodes.get(lieu);
            new DefaultTreeNode(NodeType.DISPOSITIF, new DispositifVO(dispositif), lieuTreeNode);
        }
    }

    /**
     * Gets the dispositif step is valid.
     *
     * @return the dispositif step is valid
     */
    public Boolean getDispositifStepIsValid() {
        return !this.dispositifs.isEmpty();
    }

    /**
     *
     * @param id
     */
    public final void selectDispositif(Long id) {
        DispositifVO dispositif = dispositifs.get(id);
        dispositifs.remove(id);
        dispositif.setSelected(false);
    }

    /**
     *
     * @param node
     * @param expanded
     */
    public void setExpanded(TreeNode node, boolean expanded) {
        node.setExpanded(expanded);
        if (node.getParent() != null) {
            setExpanded(node.getParent(), expanded);
        }
    }

    /**
     * Gets the availables lieux.
     *
     * @return the availables lieux
     */
    public final TreeNode getAvailablesLieux() {
        return this.availablesLieux;
    }

    /**
     *
     * @return
     */
    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    /**
     *
     * @param selectedNode
     */
    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public Map<Long, DispositifVO> getDispositifs() {
        return dispositifs;
    }

    /**
     * Gets the list dispositif.
     *
     * @return the list dispositif
     */
    public List<Nodeable> getListDispositifs() {
        final List<Nodeable> dispositifs = new LinkedList();
        this.dispositifs.values().forEach((dispositif) -> {
            dispositifs.add(dispositif.dispositif);
        });
        return dispositifs;
    }

    /**
     *
     * @param metadatasMap
     */
    public void addDispositifToMap(Map<String, Object> metadatasMap) {
        metadatasMap.put(Dispositif.class.getSimpleName(), getListDispositifs());
    }

    /**
     * The Class NodeType.
     */
    public static class NodeType {

        /**
         * The lieu.
         */
        public static final String LIEU = "lieu";
        /**
         * The dispositif.
         */
        public static final String DISPOSITIF = "dispositif";

        private NodeType() {
        }
    }

    /**
     *
     */
    public class NodeableNode {

        /**
         * The dispositif.
         */
        INodeable nodeable;

        boolean nodeableNodeSelected;

        /**
         *
         * @param nodeable
         */
        public NodeableNode(INodeable nodeable) {
            this.nodeable = nodeable;
        }

        /**
         *
         * @return
         */
        public boolean isSelected() {
            return nodeableNodeSelected;
        }

        /**
         *
         * @param selected
         */
        public void setSelected(boolean selected) {
            this.nodeableNodeSelected = selected;
        }

        /**
         *
         * @return
         */
        public String getName() {
            return localizationManager.getLocalName(nodeable);
        }
    }

    /**
     *
     */
    public class DispositifVO extends NodeableNode {

        /**
         * The dispositif.
         */
        Dispositif dispositif;

        /**
         * Instantiates a new dispositif vo.
         *
         * @param dispositif the dispositif
         */
        public DispositifVO(final Dispositif dispositif) {
            super(null);
            this.dispositif = dispositif;
        }

        /**
         * Gets the localized affichage.
         *
         * @return the localized affichage
         */
        public String getLocalizedAffichage() {
            return propertiesNamesDispositifs.getProperty(this.dispositif.getName(), this.dispositif.getName());
        }

        /**
         * Gets the localized display path.
         *
         * @return the localized display path
         */
        public String getLocalizedDisplayPath() {
            final String localizedName = propertiesNamesDispositifs.getProperty(this.dispositif.getLieu().getNom(), this.dispositif.getLieu().getNom());
            return Strings.isNullOrEmpty(localizedName)
                    ? this.dispositif.getLieu().getNom()
                    : localizedName.concat(PatternConfigurator.ANCESTOR_SEPARATOR).concat(this.dispositif.getName());
        }

        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return localizationManager.getLocalName(this.dispositif);
        }

        /**
         * Gets the dispositif.
         *
         * @return the dispositif
         */
        public Dispositif getDispositif() {
            return this.dispositif;
        }
    }

    /**
     *
     */
    public class NodeableLieu implements INodeable {

        Lieu lieu;

        /**
         *
         * @param lieu
         */
        public NodeableLieu(Lieu lieu) {
            this.lieu = lieu;
        }

        /**
         *
         * @param idNodeable
         */
        @Override
        public void setId(Long idNodeable) {
            //do nothing
        }

        /**
         *
         * @return
         */
        @Override
        public Long getId() {
            return lieu.getId();
        }

        /**
         *
         * @return
         */
        @Override
        public String getCode() {
            return lieu.getNomLieu_nomCommune();
        }

        /**
         *
         * @return
         */
        @Override
        public String getName() {
            return lieu.getNom();
        }

        /**
         *
         * @return
         */
        @Override
        public Class<Lieu> getNodeableType() {
            return Lieu.class;
        }

        /**
         *
         * @return
         */
        @Override
        public Long getOrder() {
            return null;
        }

        /**
         *
         * @return
         */
        @Override
        public String getUniqueCode() {
            return lieu.getNomLieu_nomCommune();
        }
    }
}
