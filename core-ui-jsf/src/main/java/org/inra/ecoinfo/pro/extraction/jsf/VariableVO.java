/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

import com.google.common.base.Strings;
import java.util.Properties;
import javax.faces.bean.ManagedProperty;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author vjkoyao
 */
public class VariableVO {
     /**
         * The localized name @link(String).
         */
        String         localizedName;
        /**
         * The variable @link(Variable).
         */
        final Variable variable;
        Properties                  propertiesVariablesNames;
        
        @ManagedProperty(value = "#{localizationManager}")
        ILocalizationManager         localizationManager;
        /**
         * Instantiates a new variable vo.
         *
         * @param variable
         *            the variable
         */
        public VariableVO(final Variable variable) {
            super();
            this.variable = variable;
            this.setLocalizedName(variable.getName());
        }

    /**
     *
     */
    public VariableVO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
        /**
         * Gets the affichage.
         *
         * @return the affichage
         */
        public String getAffichage() {
            return this.variable.getAffichage();
        }
        
        /**
         * Gets the code.
         *
         * @return the code
         */
        public String getCode() {
            return this.variable.getCode();
        }
        
        /**
         * Gets the id.
         *
         * @return the id
         */
        public Long getId() {
            return this.variable.getId();
        }
        
        /**
         * Gets the localized name.
         *
         * @return the localized name
         */
        public String getLocalizedName() {
            return this.localizedName;
        }
        
        /**
         * Gets the nom.
         *
         * @return the nom
         */
        public String getNom() {
            return this.variable.getName();
        }
        
        /**
         * Gets the variable.
         *
         * @return the variable
         */
        public Variable getVariable() {
            return this.variable;
        }

    /**
     *
     * @return
     */
    public Properties getPropertiesVariablesNames() {
        if (this.propertiesVariablesNames == null) {
            this.setPropertiesVariablesNames(this.localizationManager.newProperties(
                    Variable.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesVariablesNames;
    }

    /**
     *
     * @param propertiesVariablesNames
     */
    public void setPropertiesVariablesNames(Properties propertiesVariablesNames) {
        this.propertiesVariablesNames = propertiesVariablesNames;
    }
        
        
        
        /**
         * Sets the localized name.
         *
         * @param nom
         *            the new localized name
         */
        void setLocalizedName(final String nom) {
            if (nom != null) {
                localizedName = getPropertiesVariablesNames().getProperty(nom);
            }
            if (Strings.isNullOrEmpty(this.localizedName)) {
                this.localizedName = nom;
            }
        }
}
