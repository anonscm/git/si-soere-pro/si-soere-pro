/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

import com.google.common.base.Strings;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.apache.commons.collections4.map.HashedMap;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author tcherniatinsky
 */
public class UIVariable {

    /**
     * The variables @link(Map<Long,VariableJSF>).
     */
    protected final Map<String, Map<Long, VariableJSF>> variables = new HashMap();
    protected final List<VariableJSF> _variables = new LinkedList<>();

    /**
     *
     */
    public Map<String, String> localizedDatatypes = new HashMap();
    public Map<String, Map<String, String>> selectedVariablesByDatatypeAndGroupeVariable = new HashMap();

    public Map<String, String> selectedVariablesByDatatype = new HashMap();

    /**
     * The availables variables.
     */
    protected Map<String, List<VariableJSF>> availablesVariables = new HashMap();
    /**
     * The variable selected @link(VariableJSF).
     */
    protected VariableJSF variableSelected;
    /**
     * The properties variables names @link(Properties).
     */
    protected Properties propertiesVariablesNames;

    public Map<String, Map<String, String>> getSelectedVariablesByDatatypeAndGroupeVariable() {
        return selectedVariablesByDatatypeAndGroupeVariable;
    }

    public Map<String, String> getSelectedVariablesByDatatype() {
        return selectedVariablesByDatatype;
    }

    public String getLocalizedDatatype(String datatype) {
        return localizedDatatypes.getOrDefault(datatype, datatype);
    }

    /**
     *
     * @return
     */
    public Map<String, Map<Long, VariableJSF>> getVariables() {
        return variables;
    }

    public List<String> getDatatypes() {
        return _variables.stream()
                .map(v->v.getDatatype())
                .distinct()
                .collect(Collectors.toList());
    }

    /**
     * Adds the all variables.
     *
     * @return the string
     */
    public final String addAllVariables() {
        variables.clear();
        this.availablesVariables.entrySet().forEach((variableJSFEntry) -> {
            variableJSFEntry.getValue().stream().map((variableJSF) -> {
                variableJSF.selected = true;
                return variableJSF;
            }).forEachOrdered((variableJSF) -> {
                if (!variables.containsKey(variableJSFEntry.getKey())) {
                    variables.put(variableJSFEntry.getKey(),
                            new HashMap());
                }
                variables.get(variableJSFEntry.getKey()).put(variableJSF.getVariable().getId(), variableJSF);
            });
        });
        selectedVariablesByDatatype.keySet().stream()
                .forEach(k -> selectedVariablesByDatatype.put(k, "2"));
        selectedVariablesByDatatypeAndGroupeVariable.values().stream()
                .forEach(svbg -> svbg.keySet().stream().forEach(k -> svbg.put(k,"2")));
        return null;
    }

    /**
     * Removes the all variables.
     *
     * @return the string
     */
    public final String removeAllVariables() {
        variables.clear();
        availablesVariables
                .entrySet().forEach((variableJSFEntry) -> {
                    variableJSFEntry.getValue().forEach((variableJSF) -> {
                        variableJSF.selected = false;
                    });
                });
        selectedVariablesByDatatype.keySet().stream()
                .forEach(k -> selectedVariablesByDatatype.put(k, "0"));
        selectedVariablesByDatatypeAndGroupeVariable.values().stream()
                .forEach(svbg -> svbg.keySet().stream().forEach(k -> svbg.put(k,"0")));
        return null;
    }

    /**
     * if setSelected then select all variable verifying p else unselect they
     *
     * @param p
     * @param setSelected
     */
    protected void selectVariable(Predicate<VariableJSF> p, boolean setSelected) {
        _variables.stream()
                .filter(p)
                .forEach((v) -> {
                    if (!setSelected) {
                        variables.get(localizedDatatypes.get(v.datatype)).remove(
                                v.variable.getId());
                        v.setSelected(false);
                    } else {
                        if (!variables.containsKey(localizedDatatypes.get(v.datatype))) {
                            variables.put(localizedDatatypes.get(v.datatype),
                                    new HashMap());
                        }
                        variables.get(localizedDatatypes.get(v.datatype)).put(
                                v.variable.getId(), v);
                        v.setSelected(true);
                    }
                });

    }

    /**
     * return 2 if all variable verifying p are selected, 1 if some of them are
     * selected, otherwise 0.
     *
     * @param p
     * @return
     */
    protected String getState(Predicate<VariableJSF> p) {
        if (_variables.stream()
                .filter(p)
                .allMatch(v -> v.getSelected())) {
            return "2";
        } else if (_variables.stream()
                .filter(p)
                .noneMatch(v -> v.getSelected())) {
            return "0";
        }
        return "1";
    }

    /**
     * Select variable.
     *
     * @param variableSelected the variable selected
     */
    public final void selectVariable(final VariableJSF variableSelected) {
        selectVariable(v->v.getVariable().equals(variableSelected.getVariable()), !variableSelected.getSelected());
        selectedVariablesByDatatype.put(variableSelected.getDatatype(),
                getState(v -> variableSelected.getDatatype().equals(v.getDatatype()))
        );
        selectedVariablesByDatatypeAndGroupeVariable.get(variableSelected.getDatatype()).put(variableSelected.getVariable().getCategorievariable().getCvariable_nom(),
                getState(v -> variableSelected.getDatatype().equals(v.getDatatype()) && variableSelected.getVariable().getCategorievariable().getCvariable_nom().equals(v.getVariable().getCategorievariable().getCvariable_nom()))
        );
    }

    /**
     * Select variable.
     *
     * @param variableSelected the variable selected
     */
    public final void selectVariable(final String datatype) {
        selectVariable(v -> datatype.equals(v.getDatatype()), selectedVariablesByDatatype.get(datatype) != "2");
        
        selectedVariablesByDatatype.put(datatype, selectedVariablesByDatatype.get(datatype) != "2" ? "2" : "0");
        selectedVariablesByDatatypeAndGroupeVariable.get(datatype).keySet()
                .forEach(k->selectedVariablesByDatatypeAndGroupeVariable.get(datatype).put(k, selectedVariablesByDatatype.get(datatype) != "2" ? "2" : "0" ));
    }

    /**
     * Select variable.
     *
     * @param variableSelected the variable selected
     */
    public final void selectVariable(final String datatype, String groupeVariable) {
        selectVariable(v -> datatype.equals(v.getDatatype()) && groupeVariable.equals(v.getVariable().getCategorievariable().getCvariable_nom()), selectedVariablesByDatatypeAndGroupeVariable.get(datatype).get(groupeVariable) != "2");
        selectedVariablesByDatatype.put(datatype, getState(v->datatype.equals(v.getDatatype())));
        selectedVariablesByDatatypeAndGroupeVariable.get(datatype).put(groupeVariable, selectedVariablesByDatatypeAndGroupeVariable.get(datatype).get(groupeVariable) != "2" ? "2" : "0");
    }

    /**
     * Update variables availables.
     *
     * @param variableManager
     * @param uiTreatment
     * @param listContext
     * @param uiDate
     */
    public final void updateVariablesAvailables(IVariableManager variableManager, List listContext) {
        availablesVariables.clear();
        Map<String, List<NodeDataSet>> availableVariables = variableManager.getAvailableVariablesByDispositif(listContext);
        updateVariables(availableVariables);
    }

    /**
     *
     * @param availableVariables
     */
    protected void updateVariables(Map<String, List<NodeDataSet>> availableVariables) {
        _variables.clear();

        availableVariables.entrySet().stream().forEach((entry)
                -> {
            final String datatype = entry.getKey();
            selectedVariablesByDatatype.put(datatype, "0");
            selectedVariablesByDatatypeAndGroupeVariable.put(datatype, new HashedMap<>());
            List<NodeDataSet> nodes = entry.getValue();
            Map<Variable, Set<NodeDataSet>> nodesMap = new HashMap();
            nodes.stream().forEach(nds -> nodesMap.computeIfAbsent(((DatatypeVariableUnitePRO) nds.getRealNode().getNodeable()).getVariablespro(), k -> new HashSet()).add(nds));
            List<VariableJSF> variablesJSF = nodesMap.entrySet().stream()
                    .map(p -> new VariableJSF((VariablesPRO) p.getKey(), p.getValue(), datatype))
                    .peek(v -> _variables.add(v))
                    .peek(v -> selectedVariablesByDatatypeAndGroupeVariable.get(datatype).put(v.getVariable().getCategorievariable().getCvariable_nom(), "0"))
                    .collect(Collectors.toList());
            final String localeString = localizedDatatypes.containsKey(datatype) ? localizedDatatypes.get(datatype) : datatype;
            availablesVariables.put(localeString, variablesJSF);

            if (availablesVariables.get(localeString).isEmpty()) {
                availablesVariables.remove(localeString);
            }
        });
    }

    /**
     * Gets the id variable selected.
     *
     * @return the id variable selected
     */
    public Long getIdVariableSelected() {
        return variableSelected.variable.getId();
    }

    /**
     *
     * @param localizationManager
     */
    public void initVariable(ILocalizationManager localizationManager) {
        this.propertiesVariablesNames = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariablesPRO.class), Nodeable.ENTITE_COLUMN_NAME);
        Properties localizedDatatypes = localizationManager.newProperties(Nodeable.getLocalisationEntite(DataType.class), Nodeable.ENTITE_COLUMN_NAME);
        localizedDatatypes.entrySet().forEach((entry) -> {
            String key = Utils.createCodeFromString((String) entry.getKey());
            String value = (String) entry.getValue();
            this.localizedDatatypes.put(key, value);
        });
    }

    /**
     *
     * @param datatype
     * @return
     */
    public List<VariablesPRO> getVariablesForDatatype(String datatype) {
        datatype = localizedDatatypes.get(datatype);
        final List<VariablesPRO> variablesPRO = new LinkedList();
        if (variables.containsKey(datatype)) {
            for (final VariableJSF variable : this.variables.get(datatype).values()) {
                variablesPRO.add(variable.getVariable());
            }
        }
        return variablesPRO;
    }

    /**
     * Sets the variable selected.
     *
     * @param variableSelected the new variable selected
     */
    public final void setVariableSelected(final VariableJSF variableSelected) {
        this.variableSelected = variableSelected;
    }

    /**
     * Gets the variable selected.
     *
     * @return the variable selected
     */
    public VariableJSF getVariableSelected() {
        return this.variableSelected;
    }

    /**
     * Gets the variable step is valid.
     *
     * @return the variable step is valid
     */
    public Boolean getVariableStepIsValid() {
        return !this.variables.isEmpty();
    }

    /**
     * Gets the availables variables.
     *
     * @return the availables variables
     */
    public final Map<String, List<VariableJSF>> getAvailablesVariables() {
        return this.availablesVariables;
    }

    /**
     *
     * @param metadatasMap
     */
    public void addVariablestoMap(Map<String, Object> metadatasMap) {
        getVariables().forEach((datatypeName, variablesEntry) -> {
            variablesEntry.forEach((id, variableJSF) -> {
                String datatypeCode = Variable.class.getSimpleName().concat(variableJSF.getDatatype());
                if (metadatasMap.get(datatypeCode) == null) {
                    metadatasMap.put(datatypeCode, new LinkedList());
                }
                ((Collection<VariablesPRO>) metadatasMap.get(datatypeCode)).add(variableJSF.variable);
            });
        });
    }

    /**
     * The Class VariableJSF.
     */
    public class VariableJSF {

        /**
         *
         */
        protected final Set<NodeDataSet> nodesVariable;

        /**
         * The selected @link(boolean).
         */
        protected boolean selected = false;
        /**
         * The variable @link(VariableACBB).
         */
        protected VariablesPRO variable;

        /**
         *
         */
        protected String datatype;

        /**
         * Instantiates a new variable jsf.
         *
         * @param variable the variable
         * @param nodes
         * @param datatype
         */
        public VariableJSF(final VariablesPRO variable, Set<NodeDataSet> nodes, String datatype) {
            super();
            this.nodesVariable = nodes;
            this.variable = variable;
            this.datatype = datatype;
        }

        /**
         *
         * @return
         */
        public Set<NodeDataSet> getNodesVariable() {
            return nodesVariable;
        }

        /**
         * @return the datatype
         */
        public String getDatatype() {
            return this.datatype;
        }

        /**
         * Gets the localized name.
         *
         * @return the localized name
         */
        public String getLocalizedName() {
            final String localizedName = propertiesVariablesNames.getProperty(variable.getName(), variable.getName());
            return Strings.isNullOrEmpty(localizedName) ? this.variable.getName() : localizedName;
        }

        /**
         * Gets the selected.
         *
         * @return the selected
         */
        public boolean getSelected() {
            return this.selected;
        }

        /**
         * Gets the variable.
         *
         * @return the variable
         */
        public VariablesPRO getVariable() {
            return this.variable;
        }

        /**
         * @param datatype the datatype to set
         */
        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

        /**
         * Sets the selected.
         *
         * @param selected the new selected
         */
        public final void setSelected(final boolean selected) {
            this.selected = selected;
        }

        /**
         * Sets the variable.
         *
         * @param variable the new variable
         */
        public final void setVariable(final VariablesPRO variable) {
            this.variable = variable;
        }
    }

}
