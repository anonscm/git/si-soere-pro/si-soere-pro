
package org.inra.ecoinfo.pro.extraction.jsf;

import com.google.common.base.Strings;
import java.util.Properties;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author vjkoyao
 */
public class VariableJSF {
    
   
    boolean      selected = false;
    VariableVO variable;
    String                     type;


    
    Properties                  propertiesVariablesNames;

    /**
     *
     * @param variable
     * @param type
     */
    public VariableJSF(final Variable variable, final String type) {
        this.variable = new VariableVO(variable);
        this.type = type;
    }
    
    /**
     *
     * @return
     */
    public String getLocalizedName() {
            final String localizedName = propertiesVariablesNames.getProperty(this.variable
                    .getNom());
            return Strings.isNullOrEmpty(localizedName) ? this.variable.getNom() : localizedName;
    }

    /**
     *
     * @return
     */
    public boolean getSelected() {
            return this.selected;
    }
    
    

        /**
         * Sets the selected.
         *
         * @param selected
         *            the new selected
         */
    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    /**
     *
     * @return
     */
    public VariableVO getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariableVO variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType( final String type) {
        this.type = type;
    }

    
}
