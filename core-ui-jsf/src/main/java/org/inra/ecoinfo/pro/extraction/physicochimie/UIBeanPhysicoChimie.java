/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.pro.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.pro.extraction.jsf.HelperForm;
import org.inra.ecoinfo.pro.extraction.jsf.UIDIspositif;
import org.inra.ecoinfo.pro.extraction.jsf.UIDate;
import org.inra.ecoinfo.pro.extraction.jsf.UIVariable;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieDatasetManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieParameterVO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;
/**
 *
 * @author vjkoyao
 */
@ManagedBean(name = "uiBeanPhysicoChimie")
@ViewScoped
public class UIBeanPhysicoChimie extends AbstractUIBeanForSteps implements Serializable {

    static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{physicochimieDatasetManager}")
    PhysicoChimieDatasetManager physicochimieDatasetManager;

    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;

    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    @ManagedProperty(value = "#{policyManager}")
    IPolicyManager policyManager;

    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    @Transient
    private ParametersRequest parametersRequest = new ParametersRequest();

    @Transient
    HelperForm helperForm = new HelperForm();

    Long idVariableSelected;

    String affichage = "1";

    UIDIspositif uiDispositif;
    UIDate uiDate;
    UIVariable uiVariable;
    /**
     *
     */
    public UIBeanPhysicoChimie() {
        super();
    }

    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        switch (this.getStep()) {
            case 1:
                return uiDispositif.getDispositifStepIsValid();
            case 2:
                return uiVariable.getVariableStepIsValid();
            case 3:
                return this.uiDate.getDateStepIsValid();
            default:
                break;
        }
        return false;
    }

    private List<VariablesPRO> getListVariables() {
        List<VariablesPRO> variables = new LinkedList();
        uiVariable.getVariables()
                .forEach(((k,v)->v.forEach((m,n)-> variables.add(n.getVariable()))));
        return variables;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    final void initProperties() {
        uiDispositif = new UIDIspositif();
        uiDispositif.initProperties(localizationManager);
        uiDispositif.updateLieuAvailables(physicochimieDatasetManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager);
        uiVariable = new UIVariable();
        uiVariable.initVariable(localizationManager);
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return "physicochimie";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (this.getStep()) {
            case 1:
                uiDispositif.updateLieuAvailables(physicochimieDatasetManager);
                break;
            case 2:
                uiVariable.updateVariablesAvailables(physicochimieDatasetManager, uiDispositif.getListDispositifs());
                break;
            default:
                break;
        }
        return null;
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    public String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap<>();
        uiDispositif.addDispositifToMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        uiDate.addDatestoMap(metadatasMap);
        final PhysicoChimieParameterVO parameters = new PhysicoChimieParameterVO(metadatasMap);
        Integer aff = Integer.parseInt(affichage);
        this.extractionManager.extract(parameters, aff);
        return null;
    }

    public void setPhysicochimieDatasetManager(PhysicoChimieDatasetManager physicochimieDatasetManager) {
        this.physicochimieDatasetManager = physicochimieDatasetManager;
    }

    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public ParametersRequest getParametersRequest() {
        return parametersRequest;
    }

    public UIDIspositif getUiDispositif() {
        return uiDispositif;
    }

    public UIDate getUiDate() {
        return uiDate;
    }

    public UIVariable getUiVariable() {
        return uiVariable;
    }
    /**
     *
     */

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        
        /**
         * The european format @link(boolean).
         */
        boolean europeanFormat = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getEuropeanFormat() {
            return this.europeanFormat;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiDate.getDateStepIsValid() && uiDispositif.getDispositifStepIsValid()
                    && uiVariable.getVariableStepIsValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Sets the european format.
         *
         * @param europeanFormat the new european format
         */
        public final void setEuropeanFormat(final boolean europeanFormat) {
            this.europeanFormat = europeanFormat;
        }
    }
}
