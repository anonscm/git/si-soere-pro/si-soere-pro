/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

/**
 *
 * @author vjkoyao
 */
public class HelperForm {
    /**
         * The index @link(Integer).
         */
        Integer index;
        /**
         * The key @link(String).
         */
        String  key;
        /**
         * The value @link(String).
         */
        String  value;

        /**
         * Gets the index.
         *
         * @return the index
         */
        public Integer getIndex() {
            return this.index;
        }

        /**
         * Gets the key.
         *
         * @return the key
         */
        public String getKey() {
            return this.key;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }

        /**
         * Sets the index.
         *
         * @param index
         *            the new index
         */
        public void setIndex(final Integer index) {
            this.index = index;
        }

        /**
         * Sets the key.
         *
         * @param key
         *            the new key
         */
        public void setKey(final String key) {
            this.key = key;
        }

        /**
         * Sets the value.
         *
         * @param value
         *            the new value
         */
        public void setValue(final String value) {
            this.value = value;
        } 
}
