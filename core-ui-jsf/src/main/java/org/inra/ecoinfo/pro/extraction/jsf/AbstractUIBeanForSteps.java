package org.inra.ecoinfo.pro.extraction.jsf;

import java.io.Serializable;
import java.util.TimeZone;
import javax.faces.component.html.HtmlPanelGroup;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.wizard.Wizard;
import org.primefaces.event.FlowEvent;

/**
 * The Class AbstractUIBeanForSteps.
 */
public abstract class AbstractUIBeanForSteps implements Serializable {

    /**
     *
     */
    public static final TimeZone TIME_ZONE  = TimeZone.getTimeZone("UTC");

    /**
     * The Constant TITLE_STEP.
     */
    private static final String  TITLE_STEP = " %d : %s";
    /**
     * The step.
     */
    private int                  step       = 1;
    /**
     * The toggle panel.
     */
    private Wizard               wizard;
    /**
     * The accordion.
     */
    private AccordionPanel       accordion;
    /**
     * The panel.
     */
    private HtmlPanelGroup       panel;

    /**
     * Instantiates a new abstract ui bean for steps.
     */
    public AbstractUIBeanForSteps() {
        super();
    }

    /**
     * Gets the accordion.
     * 
     * @return the accordion
     */
    public AccordionPanel getAccordion() {
        return accordion;
    }

    /**
     * Gets the change step.
     * 
     * @return the change step
     */
    public String getChangeStep() {
        final Tab currentAccordion = accordion.findTab(accordion.getActiveIndex());
        final int currentAccordionRow = accordion.getChildren().indexOf(currentAccordion);
        // togglePanel.setActiveItem(((Panel) togglePanel.getChildren().get(currentAccordionRow)));
        step = currentAccordionRow + 1;
        return "";
    }

    /**
     * Gets the current toggle item.
     * 
     * @return the current toggle item
     */
    public Tab getCurrentToggleItem() {
        return null;// (Tab) togglePanel.gegetItem(togglePanel.getActiveItem());
    }

    /**
     * Gets the checks if is step valid.
     * 
     * @return the checks if is step valid
     */
    public abstract boolean getIsStepValid();

    /**
     * Gets the panel.
     * 
     * @return the panel
     */
    public HtmlPanelGroup getPanel() {
        return panel;
    }

    /**
     * Gets the step.
     * 
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * Gets the title for step.
     * 
     * @return the title for step
     */
    public String getTitleForStep() {
        return String.format(AbstractUIBeanForSteps.TITLE_STEP, step, ((Tab) wizard.getChildren()
                .get(step - 1)).getTitle());
    }

    /**
     * 
     * @param event
     * @return
     */
    public String onFlowProcess(FlowEvent event) {
        return wizard.getChildren().get(step).getId();
    }

    /**
     * Gets the toggle panel.
     * 
     * @return the toggle panel
     */
    public Wizard getWizard() {
        return wizard;
    }

    /**
     * Next step.
     * 
     * @return the string
     */
    public String nextStep() {
        if (wizard.getChildren().get(step) != null) {
            wizard.setStep(wizard.getChildren().get(step).getId());
            if (accordion.getChildren().get(step) != null) {
                accordion.setActiveIndex(Integer.toString(step));
            }
            step++;
        }
        return null;
    }

    /**
     * Prev step.
     * 
     * @return the string
     */
    public String prevStep() {
        if (wizard.getChildren().get(step - 2) != null) {
            wizard.setStep(wizard.getChildren().get(step - 2).getId());
            if (accordion.getChildren().get(step - 2) != null) {
                accordion.setActiveIndex(Integer.toString(step - 2));
            }
            step--;
        }
        return null;
    }

    /**
     * Sets the accordion.
     * 
     * @param accordion
     *            the new accordion
     */
    public void setAccordion(AccordionPanel accordion) {
        this.accordion = accordion;
    }

    /**
     * Sets the panel.
     * 
     * @param panel
     *            the new panel
     */
    public void setPanel(HtmlPanelGroup panel) {
        this.panel = panel;
    }

    /**
     * Sets the step.
     * 
     * @param step
     *            the new step
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * Sets the toggle panel.
     * 
     * @param panel
     *            the new toggle panel
     */
    public void setWizard(Wizard panel) {
        wizard = panel;
    }

    /**
     * 
     * @return
     */
    public TimeZone getTimeZone() {
        return TIME_ZONE;
    }
}
