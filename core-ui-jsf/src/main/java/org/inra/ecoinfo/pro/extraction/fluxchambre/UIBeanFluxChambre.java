/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.pro.extraction.fluxchambre.impl.FluxChambreDatasetManager;
import org.inra.ecoinfo.pro.extraction.fluxchambre.impl.FluxChambreParameterVO;
import org.inra.ecoinfo.pro.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.pro.extraction.jsf.UIDIspositif;
import org.inra.ecoinfo.pro.extraction.jsf.UIDate;
import org.inra.ecoinfo.pro.extraction.jsf.UIVariable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 *
 * @author vkoyao
 */
@ManagedBean(name = "uiBeanFluxChambre")
@ViewScoped
public class UIBeanFluxChambre extends AbstractUIBeanForSteps implements Serializable {

    /**
     *
     */
    @ManagedProperty(value = "#{fluxChambreDatatype}")
    public String fluxChambreDatatype;

    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    @ManagedProperty(value = "#{fluxchambreDatasetManager}")
    FluxChambreDatasetManager fluxchambreDatasetManager;

    @ManagedProperty(value = "#{notificationsManager}")
    @Transient
    INotificationsManager notificationsManager;

    ParametersRequest parametersRequest = new ParametersRequest();

    /**
     * The security context.
     */
    @ManagedProperty(value = "#{policyManager}")
    @Transient
    IPolicyManager policyManager;

    /**
     * The transaction manager.
     */
    @ManagedProperty(value = "#{transactionManager}")
    @Transient
    JpaTransactionManager transactionManager;

    UIDIspositif uiDispositif;
    UIDate uiDate;
    UIVariable uiVariable;

    /**
     *
     */
    public UIBeanFluxChambre() {
        super();
    }

    /**
     *
     * @return
     */
    public UIDIspositif getUiDispositif() {
        return uiDispositif;
    }

    /**
     *
     * @return
     */
    public UIDate getUiDate() {
        return uiDate;
    }

    /**
     *
     * @return
     */
    public UIVariable getUiVariable() {
        return uiVariable;
    }

    /**
     * Extract.
     *
     * @return the string
     * @throws BusinessException the business exception
     */
    public final String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        // TODO
        uiDate.addDatestoMap(metadatasMap);
        uiDispositif.addDispositifToMap(metadatasMap);
        uiVariable.addVariablestoMap(metadatasMap);
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS,
                this.parametersRequest.getCommentExtraction());
        final FluxChambreParameterVO parameters = new FluxChambreParameterVO(metadatasMap);
        this.extractionManager.extract(parameters, 1);
        return null;
    }

    /**
     *
     * @return
     */
    public List<UIVariable.VariableJSF> getAvailablesVariables() {
        if (uiVariable.getAvailablesVariables() != null
                && uiVariable.getAvailablesVariables().containsKey(fluxChambreDatatype)) {
            return uiVariable.getAvailablesVariables().get(fluxChambreDatatype);
        } else {
            return new LinkedList();
        }
    }

    /**
     * autre type de données
     */
    /**
     * Gets the checks if is step valid.
     *
     * @return the checks if is step valid
     * @see
     * org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#getIsStepValid()
     */
    @Override
    public final boolean getIsStepValid() {
        if (getStep() == 1) {
            return uiDispositif.getDispositifStepIsValid();
        } else if (getStep() == 2) {
            return uiVariable.getVariableStepIsValid();
        }
        return false;
    }

    /**
     * @return the parametersRequest
     */
    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    /**
     * Inits the properties.
     */
    @PostConstruct
    public void initProperties() {
        uiDispositif = new UIDIspositif();
        uiDispositif.initProperties(localizationManager);
        uiVariable = new UIVariable();
        uiVariable.initVariable(localizationManager);
        uiDate = new UIDate();
        uiDate.initDatesRequestParam(localizationManager);
    }

    /**
     * Navigate.
     *
     * @return the string
     */
    public final String navigate() {
        return "fluxchambres";
    }

    /**
     * Next step.
     *
     * @return the string
     * @see org.inra.ecoinfo.acbb.dataset.jsf.AbstractUIBeanForSteps#nextStep()
     */
    @Override
    public final String nextStep() {
        super.nextStep();
        switch (getStep()) {
            case 2:
                uiVariable.updateVariablesAvailables(fluxchambreDatasetManager, uiDispositif.getListDispositifs());
                break;
            case 3:
                //uiDate.updateDateYearContinuous(fluxchambreDatasetManager, uiDate.getDatesForm1ParamVO().intervalsDate(), uiDispositif.getListDispositifs());
                break;
            default:
                break;
        }
        return null;
    }

    public void setFluxChambreDatatype(String fluxChambreDatatype) {
        this.fluxChambreDatatype = fluxChambreDatatype;
    }

    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setFluxchambreDatasetManager(FluxChambreDatasetManager fluxchambreDatasetManager) {
        this.fluxchambreDatasetManager = fluxchambreDatasetManager;
    }

    public void setPolicyManager(IPolicyManager policyManager) {
        this.policyManager = policyManager;
    }

    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    /**
     *
     * @param variablesFluxChambreAvailables
     */

    /**
     * The Class ParametersRequest.
     */
    public class ParametersRequest {

        /**
         * The comment extraction @link(String).
         */
        String commentExtraction;
        /**
         * The european format @link(boolean).
         */
        boolean format = true;

        /**
         * Gets the comment extraction.
         *
         * @return the comment extraction
         */
        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        /**
         * Gets the european format.
         *
         * @return the european format
         */
        public boolean getFormat() {
            return this.format;
        }

        /**
         * Gets the form is valid.
         *
         * @return the form is valid
         */
        public boolean getFormIsValid() {
            return uiDate.getDateStepIsValid() && uiDispositif.getDispositifStepIsValid()
                    && uiVariable.getVariableStepIsValid();
        }

        /**
         * Sets the comment extraction.
         *
         * @param commentExtraction the new comment extraction
         */
        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        /**
         * Sets the european format.
         *
         * @param format
         */
        public final void setFormat(final boolean format) {
            this.format = format;
        }
    }
}
