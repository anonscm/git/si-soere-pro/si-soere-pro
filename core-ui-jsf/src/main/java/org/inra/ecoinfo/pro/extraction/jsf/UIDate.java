/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.jsf;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.inra.ecoinfo.extraction.jsf.AbstractDatesFormParam;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.extraction.DatesFormParamVO;
import org.inra.ecoinfo.pro.extraction.Periode;
import org.inra.ecoinfo.pro.utils.vo.DatesRequestParamVO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.slf4j.LoggerFactory;

/**
 *
 * @author tcherniatinsky
 */
public class UIDate {

    /**
     * The helper form @link(HelperForm).
     */
    HelperForm helperForm = new HelperForm();
    /**
     * The dates request param @link(DatesRequestParamVO).
     */
    DatesRequestParamVO datesRequestParam;

    /**
     *
     */
    public UIDate() {
    }

    // TODO
    /**
     * Adds the period years continuous.
     *
     * @return the string
     */
    public final String addPeriodYearsContinuous() {
        getDateYears().getPeriods().add(buildNewMapPeriod());
        return null;
    }

    /**
     * Builds the new map period.
     *
     * @return the map
     */
    protected final Map<String, String> buildNewMapPeriod() {
        final Map<String, String> firstMap = new HashMap<>();
        firstMap.put(AbstractDatesFormParam.START_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        firstMap.put(AbstractDatesFormParam.END_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        return firstMap;
    }

    /**
     * @return the helperForm
     */
    public HelperForm getHelperForm() {
        return this.helperForm;
    }

    /**
     * Removes the period years continuous.
     *
     * @return the string
     */
    public final String removePeriodYearsContinuous() {
        if (getDateYears().getPeriods().size() <= 1) {
            return null;
        }
        getDateYears().getPeriods().remove(getDateYears().getPeriods().get(getDateYears().getPeriods().size() - 1));
        return null;
    }

    /**
     * @param helperForm the helperForm to set
     */
    public void setHelperForm(HelperForm helperForm) {
        this.helperForm = helperForm;
    }

    /**
     * Update date year continuous.
     *
     * @return the string
     */
    public final String updateDateYearContinuous() {
        final String key = this.helperForm.getKey();
        final Integer index = this.helperForm.getIndex();
        final String value = this.helperForm.getValue();
        getDateYears()
                .getPeriods().get(index).put(key, value);
        return null;
    }

    private DatesFormParamVO getDateYears() {
        return (DatesFormParamVO) datesRequestParam.getDatesFormParam();
    }

    /**
     * Gets the dates form1 param vo.
     *
     * @return the dates form1 param vo
     */
    public DatesFormParamVO getDatesForm1ParamVO() {
        return (DatesFormParamVO) this.datesRequestParam.getDatesFormParam();
    }

    /**
     * Gets the date step is valid.
     *
     * @return the date step is valid
     */
    public Boolean getDateStepIsValid() {
        return getDateYears().getIsValid();
    }

    /**
     * Gets the dates request param.
     *
     * @return the dates request param
     */
    public DatesRequestParamVO getDatesRequestParam() {
        return this.datesRequestParam;
    }

    /**
     *
     * @param datesRequestParam
     */
    public void setDatesRequestParam(DatesRequestParamVO datesRequestParam) {
        this.datesRequestParam = datesRequestParam;
    }

    /**
     * Inits the dates request param.
     *
     * @param localizationManager
     */
    public void initDatesRequestParam(ILocalizationManager localizationManager) {
        this.datesRequestParam = new DatesRequestParamVO(new DatesFormParamVO(localizationManager));
        getDateYears().setPeriods(new LinkedList<>());
        getDateYears().getPeriods().add(buildNewMapPeriod());
    }

    /**
     *
     * @param metadatasMap
     */
    public void addDatestoMap(Map<String, Object> metadatasMap) {
        IntervalDate intervalDate = null;
        Periode periode = getDatesRequestParam().getDatesFormParam().getPeriodsFromDateFormParameter().get(0);
        try {
            intervalDate = new IntervalDate(periode.getDateStart(), periode.getDateEnd(), DateUtil.DD_MM_YYYY);
        } catch (Exception e) {
            LoggerFactory.getLogger("uiExtract").debug(e.getMessage());
        }
        metadatasMap.put(IntervalDate.class.getSimpleName(), intervalDate);
    }

    /**
     * The Class HelperForm.
     */
    public static class HelperForm {

        /**
         * The index @link(Integer).
         */
        Integer index;
        /**
         * The key @link(String).
         */
        String key;
        /**
         * The value @link(String).
         */
        String value;

        /**
         * Gets the index.
         *
         * @return the index
         */
        public Integer getIndex() {
            return this.index;
        }

        /**
         * Gets the key.
         *
         * @return the key
         */
        public String getKey() {
            return this.key;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }

        /**
         * Sets the index.
         *
         * @param index the new index
         */
        public final void setIndex(final Integer index) {
            this.index = index;
        }

        /**
         * Sets the key.
         *
         * @param key the new key
         */
        public final void setKey(final String key) {
            this.key = key;
        }

        /**
         * Sets the value.
         *
         * @param value the new value
         */
        public final void setValue(final String value) {
            this.value = value;
        }
    }

}
