package org.inra.ecoinfo.pro.logs;

import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
@ManagedBean(name = "uiLogs")
@ViewScoped
public class UIBeanLogs implements Serializable {

    private static final String LOGS_FOLDER = "%slogs";
    private static final String LOGS_FILE = "%slogs/%s";
    private static final String PATH_PATTERN = "%s/%s";
    private static final String COPY_PATTERN = "../../../../../../../resources/logs/%s";
    private static final String UI_BEAN_HELP_CLASS = "UIBeanLogs.class";
    private static final String CST_TEST_FILES = "testFiles";
    @ManagedProperty(value = "#{coreConfiguration}")
    private ICoreConfiguration configuration;

    /**
     *
     */
    public UIBeanLogs() {
    }

    /**
     *
     * @param configuration
     */
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
        copyFiles();
    }

    /**
     *
     * @return
     */
    public String testFiles() {
        return CST_TEST_FILES;
    }

    /**
     *
     * @return
     */
    public List<String> getLogsFiles() {
        File refdatas = getFile(String.format(LOGS_FOLDER, configuration.getRepositoryURI()));
        if(!refdatas.exists()){
            refdatas.mkdirs();
        }
        return Arrays.asList(refdatas.list());
    }

    private File getFile(String file) {
        return FileWithFolderCreator.createFile(file);
    }

    private void copyFiles() {
        getLogsFiles().forEach((fileName) -> {
            try {
                File file = getFile(String.format(LOGS_FILE,configuration.getRepositoryURI(),fileName));
                String path = getClass().getResource(UI_BEAN_HELP_CLASS).getFile().replaceAll(UI_BEAN_HELP_CLASS, "").concat(String.format(COPY_PATTERN, file.getName()));
                File copyFile = getFile(path);
                Files.copy(file, copyFile);
            } catch (IOException ex) {
                LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME).error(ex.getMessage(), ex);
            }
        });
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return "logsFiles";
    }
}
