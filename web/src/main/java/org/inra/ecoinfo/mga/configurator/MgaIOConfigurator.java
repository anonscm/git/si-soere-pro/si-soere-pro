package org.inra.ecoinfo.mga.configurator;

import org.inra.ecoinfo.mga.business.composite.INodeable;
import static org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator.*;
import org.inra.ecoinfo.mga.enums.Activities;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 *
 * @author yahiaoui
 */
public class MgaIOConfigurator extends AbstractMgaIOConfigurator {

    /**
     *
     */
    public static final String PRO= "PRO";

    /**
     * Configuration Zero
     */
    final static Integer[] ENTRY_ORDER_0_1_2_3 = new Integer[]{0, 1, 2, 3, 4};
    final static Integer[] ENTRY_ORDER_0_1_2 = new Integer[]{0, 1, 2, 3};
    final static Integer[] ENTRY_ORDER_SYNTHESIS_0_1_2 = new Integer[]{0, 1, 2};
    final static Integer[] ENTRY_ORDER_REF = new Integer[]{0};
    final static Class<INodeable>[] ENTRY_INSTANCE_STD = new Class[]{
            Dispositif.class,
            ParcelleElementaire.class,
            Theme.class,
            DataType.class
        };
    final static Class<INodeable>[] ENTRY_INSTANCE_ASTD = new Class[]{
            Dispositif.class,
            Theme.class,
            DataType.class
        };
    final static Class<INodeable>[] STDV = new Class[]{
            Dispositif.class,
            ParcelleElementaire.class,
            Theme.class,
            DataType.class,
            DatatypeVariableUnitePRO.class
        };
    final static Class<INodeable>[] ENTRY_INSTANCE_REF = new Class[]{
            Refdata.class
        };
    final static Activities[] ACTIVITIES_SAPDSE
                = new Activities[]{
                    Activities.synthese,
                    Activities.administration,
                    Activities.publication,
                    Activities.depot,
                    Activities.suppression,
                    Activities.extraction
                };
    final static Activities[] ACTIVITIES_A
                = new Activities[]{
                    Activities.associate
                };
    final static Activities[] ACTIVITIES_REF
                = new Activities[]{
                    Activities.edition,
                    Activities.suppression,
                    Activities.telechargement,
                    Activities.administration};

    final static int CODE_CONF_WITH_NO_PARCELLE = 7;

    /**
     *
     */
    public MgaIOConfigurator() {
        super(new MgaDisplayerConfiguration() {
        });
    }

    /**
     *
     */
    @Override
    protected void initConfigurations() {

        Class stickyLeafDatasetRights = DatatypeVariableUnitePRO.class;
        Configuration configDatasetRights
                = new Configuration(
                        DATASET_CONFIGURATION_RIGHTS,
                        DataType.class,
                        ENTRY_ORDER_0_1_2_3,
                        STDV,
                        ACTIVITIES_SAPDSE,
                        ENTRY_ORDER_0_1_2_3,
                        true,
                        WhichTree.TREEDATASET,
                        stickyLeafDatasetRights,
                        false);
        Configuration configDataset
                = new Configuration2(
                        DATASET_CONFIGURATION,
                        DataType.class,
                        ENTRY_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_STD,
                        ACTIVITIES_SAPDSE,
                        ENTRY_ORDER_0_1_2_3,
                        true,
                        WhichTree.TREEDATASET,
                        null,
                        true);

        Configuration configWithNoParcelle
                = new Configuration(
                        CODE_CONF_WITH_NO_PARCELLE,
                        DataType.class,
                        ENTRY_ORDER_0_1_2,
                        ENTRY_INSTANCE_ASTD,
                        ACTIVITIES_SAPDSE,
                        ENTRY_ORDER_0_1_2,
                        true,
                        WhichTree.TREEDATASET,
                        null,
                        true);

        AbstractMgaIOConfiguration configRefdataRights
                = new Configuration(REFDATA_CONFIGURATION_RIGHTS,
                        Refdata.class,
                        ENTRY_ORDER_REF,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_REF,
                        ENTRY_ORDER_REF,
                        true,
                        WhichTree.TREEREFDATA,
                        null,
                        false);

        AbstractMgaIOConfiguration configRefdata
                = new Configuration(REFDATA_CONFIGURATION,
                        Refdata.class,
                        ENTRY_ORDER_REF,
                        ENTRY_INSTANCE_REF,
                        ACTIVITIES_REF,
                        ENTRY_ORDER_REF,
                        true,
                        WhichTree.TREEREFDATA,
                        null,
                        true);

        AbstractMgaIOConfiguration configSynthesis
                = new ConfigurationSynthesis(SYNTHESIS_CONFIGURATION,
                        DataType.class,
                        ENTRY_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_STD,
                        ACTIVITIES_SAPDSE,
                        ENTRY_ORDER_SYNTHESIS_0_1_2,
                        false,
                        WhichTree.TREEDATASET,
                        null,
                        false);
        configSynthesis.setSkeletonBuilder("synthesisManager");

        AbstractMgaIOConfiguration configAssociate
                = new Configuration2(
                        ASSOCIATE_CONFIGURATION,
                        DataType.class,
                        ENTRY_ORDER_0_1_2_3,
                        ENTRY_INSTANCE_STD,
                        ACTIVITIES_A,
                        ENTRY_ORDER_0_1_2_3,
                        true,
                        WhichTree.TREEDATASET,
                        null,
                        false);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION_RIGHTS, k -> configDatasetRights);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION_RIGHTS, k -> configRefdataRights);
        getConfigurations().computeIfAbsent(DATASET_CONFIGURATION, k -> configDataset);
        getConfigurations().computeIfAbsent(REFDATA_CONFIGURATION, k -> configRefdata);
        getConfigurations().computeIfAbsent(ASSOCIATE_CONFIGURATION, k -> configAssociate);
        getConfigurations().computeIfAbsent(SYNTHESIS_CONFIGURATION, k -> configSynthesis);
        getConfigurations().computeIfAbsent(CODE_CONF_WITH_NO_PARCELLE, k -> configWithNoParcelle);

        /**
         * ------------------------------------------------------------------------------------
         * *
         */
    }

    class Configuration extends AbstractMgaIOConfiguration {

        public Configuration(int codeConfig, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfig, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

    }

    class Configuration2 extends Configuration {

        Integer[] parcelleSortOrder = new Integer[]{0, 1, 3, 4, 2};
        Integer[] siteSortOrder = new Integer[]{0, 1, 2, 3};

        public Configuration2(int codeConfig, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfig, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

        @Override
        public Integer[] getSortOrder(int maxIndex) {
            return 4 == maxIndex ? parcelleSortOrder : siteSortOrder;
        }

    }

    class ConfigurationSynthesis extends Configuration {

        Integer[] parcelleSortOrder = new Integer[]{0, 1, 3, 4};
        Integer[] siteSortOrder = new Integer[]{0, 1, 2 , 3};

        public ConfigurationSynthesis(int codeConfig, Class<? extends INodeable> leafType, Integer[] entryOrder, Class<INodeable>[] entryTypes, Activities[] activities, Integer[] sortOrder, boolean includeAncestor, WhichTree whichTree, Class<? extends INodeable> stickyLeafType, boolean displayColumnNames) {
            super(codeConfig, leafType, entryOrder, entryTypes, activities, sortOrder, includeAncestor, whichTree, stickyLeafType, displayColumnNames);
        }

        @Override
        public Integer[] getSortOrder(int maxIndex) {
            return 4 == maxIndex ? parcelleSortOrder : siteSortOrder;
        }

    }

}
