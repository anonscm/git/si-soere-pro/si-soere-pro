var NODE = "div[id$=myNode]";
var ROOT = "div[id$=treeResources]";
var PARENT_NODE = "div.rf-trn";
var NOT_PARENT_NODE = "div:not(.rf-trn)";
var INPUT_TEXT = " input[type=text]";
var INPUT_CHECKBOX = " input[type=checkBox]";
var INDETERMINATE = 'indeterminate';
var CLASS = 'class';
var TD = "td";
var TD_DOT = "td.";
var INDETERMINATE_DATE = "**/**/****";
var STRING_EMPTY = "";
var STRING_1 = "-1";
var ALL = "tout";
var EXTRACTION = "extraction";
var DEBUT = "debut";
var FIN = "fin";
var INPUT = "input";
var TR = "tr";
var NOT_TOUT_CHECKBOX = "td:not(.tout) input[type=checkBox]";
var BAD_FORMAT_DATE = "La date doit être au format dd/mm/yyyy";
var getType = function(el) {
	return $(el).parent().attr(CLASS);
};
var checkTreeNodeIndeterminate = function(checkBox, type) {
	var childrenInput = $(checkBox).parents(NODE).first().children(NODE)
			.children(PARENT_NODE).find(TD_DOT + type + INPUT_CHECKBOX);
	if (childrenInput.size() == 0) {
		childrenInput = $(checkBox).parents(NODE).first().children(
				NOT_PARENT_NODE).find(TD_DOT + type + INPUT_CHECKBOX);
		if (childrenInput.size() == 0) {
			checkBox.indeterminate = false;
			return checkBox.checked ? 1 : 0;
		} else if (childrenInput.filter(function(index, el) {
			return el.checked;
		}).size() == childrenInput.size()) {
			checkBox.checked = true;
			checkBox.indeterminate = false;
			return 1;
		} else if (childrenInput.filter(function(index, el) {
			return !el.checked;
		}).size() == childrenInput.size()) {
			checkBox.checked = false;
			checkBox.indeterminate = false;
			return 0;
		} else {
			checkBox.checked = false;
			checkBox.indeterminate = true;
			return -1;
		}
	} else {
		childrenInput = childrenInput.map(function(index, input) {
			return checkTreeNodeIndeterminate(input, type);
		});
		if (childrenInput.filter(function(index, el) {
			return el == 0;
		}).size() == childrenInput.size()) {
			checkBox.checked = false;
			checkBox.indeterminate = false;
			return 0;
		} else if (childrenInput.filter(function(index, el) {
			return el == 1;
		}).size() == childrenInput.size()) {
			checkBox.checked = true;
			checkBox.indeterminate = false;
			return 1;
		} else {
			checkBox.checked = false;
			checkBox.indeterminate = true;
			return -1;
		}

	}
};

var checkTreeNodeIndeterminateDate = function(input, type) {
	var childrenInput = $(input).parents(NODE).first().children(NODE).children(
			PARENT_NODE).find(TD_DOT + type + INPUT_TEXT);
	if (childrenInput.size() == 0) {
		childrenInput = $(input).parents(NODE).first()
				.children(NOT_PARENT_NODE).find(TD_DOT + type + INPUT_TEXT);
		childrenInput = childrenInput.map(function(i, el) {
			var extraction = $(el).parents(TR).first().children(
					TD_DOT + EXTRACTION).children()[0];
			el.disabled = !extraction.checked;
			return el.value;
		});
		if (childrenInput.size() == 0) {
			input.value = STRING_EMPTY;
			$(input).removeClass(INDETERMINATE);
			return 0;
		} else if (childrenInput.filter(function(index, el) {
			return el == INDETERMINATE_DATE;
		}).size() == childrenInput.size()) {
			input.value = INDETERMINATE_DATE;
			$(input).addClass(INDETERMINATE);
			return STRING_1;
		} else if (childrenInput.filter(function(index, el) {
			return el == STRING_EMPTY;
		}).size() == childrenInput.size()) {
			input.value = STRING_EMPTY;
			$(input).removeClass(INDETERMINATE);
			return STRING_EMPTY;
		} else if (childrenInput.filter(function(index, el) {
			return el == childrenInput[0];
		}).size() == childrenInput.size()) {
			input.value = childrenInput[0] == STRING_1 ? STRING_EMPTY
					: childrenInput[0];
			$(input).removeClass(INDETERMINATE);
			return childrenInput[0];
		} else {
			input.value = INDETERMINATE_DATE;
			$(input).addClass(INDETERMINATE);
			return STRING_1;
		}
	} else {
		childrenInput = childrenInput.map(function(index, input) {
			return checkTreeNodeIndeterminateDate(input, type);
		});
		if (childrenInput.filter(function(index, el) {
			return el == STRING_EMPTY;
		}).size() == childrenInput.size()) {
			input.value = STRING_EMPTY;
			$(input).removeClass(INDETERMINATE);
			return STRING_EMPTY;
		} else if (childrenInput.filter(function(index, el) {
			return el == childrenInput[0];
		}).size() == childrenInput.size()) {
			input.value = childrenInput[0] == STRING_1 ? STRING_EMPTY
					: childrenInput[0];
			$(input).removeClass(INDETERMINATE);
			return childrenInput[0];
		} else {
			input.value = INDETERMINATE_DATE;
			$(input).addClass(INDETERMINATE);
			return STRING_1;
		}
	}
};
var reload = false;
var toutIndeterminate = function(checkBox) {
	var cells = $(checkBox).parents(TR).find(NOT_TOUT_CHECKBOX);
	if (cells.filter(function(index, el) {
		return !el.checked;
	}).size() == cells.size()) {
		checkBox.indeterminate = false;
	} else if (cells.filter(function(index, el) {
		return el.checked;
	}).size() == cells.size()) {
		checkBox.indeterminate = false;
	} else {
		checkBox.indeterminate = true;
	}
};

var initInputs = function(input, type) {
	if (reload) {
		return;
	} else {
		reload = true;
	}
	if (type == ALL) {
		toutIndeterminate(input);
	} else if (type == DEBUT || type == FIN) {
		checkTreeNodeIndeterminateDate(input, type);
	} else {
		checkTreeNodeIndeterminate(input, type);
	}
	reload = false;
};

var initAllInputs = function() {
	// on supprime les noeuds vides
	/*
	 * var emptyNode=$(NODE).filter(function(){return
	 * $(this).children(NOT_PARENT_NODE).find(INPUT_CHECKBOX).size()==0;});
	 * while(emptyNode.size()>0){ emptyNode.remove();
	 * emptyNode=$(NODE).filter(function(){return
	 * $(this).children(NOT_PARENT_NODE).find(INPUT_CHECKBOX).size()==0;}); }
	 */
	// pour tous les noeuds roots, on remplit les indéterminates.
	$(ROOT).children(NODE).children(PARENT_NODE).find(INPUT).each(
			function(i, el) {
				type = getType(el);
				initInputs(el, type);
			});
};

var updateCheckBox = function(event) {
	var el = event.source;
	var type = getType(el);
	$(el).parents(PARENT_NODE).first().nextAll().find(
			type == ALL ? INPUT_CHECKBOX : TD_DOT + type + INPUT_CHECKBOX)
			.each(
					function(i, e) {
						e.checked = el.checked;
						if (getType(e) == EXTRACTION) {
							$(e).parents(TR).first().find(TD + INPUT_TEXT)
									.each(function(i, t) {
										t.disabled = !el.checked;
										if (el.checked) {
											$(t).removeClass(INDETERMINATE);
										}
									});
						}
					});
	if (ALL == type) {
		$(el).parents(NODE).last().find(PARENT_NODE).first().find(
				NOT_TOUT_CHECKBOX).each(function(i, c) {
			checkTreeNodeIndeterminate(c, getType(c));
		});
		$(el).parents(NODE).last().find(PARENT_NODE).first().find(INPUT_TEXT)
				.each(function(i, t) {
					checkTreeNodeIndeterminateDate(t, getType(t));
					t.disabled = !el.checked;
				});
	} else if (EXTRACTION == type) {
		$(el).parents(NODE).last().find(PARENT_NODE).first().find(INPUT_TEXT)
				.each(function(i, t) {
					checkTreeNodeIndeterminateDate(t, getType(t));
					t.disabled = !el.checked;
					if (el.checked) {
						$(t).removeClass(INDETERMINATE);
					}
				});
	} else {
		checkTreeNodeIndeterminate($(el).parents(NODE).last().find(
				TD_DOT + type + INPUT_CHECKBOX)[0], type);
	}
};

var updateTextBox = function(event) {
	var text = event.source;
	if (!text.value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/)
			&& text.value != INDETERMINATE_DATE && text.value != STRING_EMPTY) {
		alert(BAD_FORMAT_DATE);
		return false;
	}
	var type = getType(text);
	$(text).parents(PARENT_NODE).first().nextAll().find(
			TD_DOT + type + INPUT_TEXT).each(function(i, t) {
		t.value = text.value;
	});
	checkTreeNodeIndeterminateDate($(text).parents(NODE).last().find(
			TD_DOT + type + INPUT_TEXT)[0], type);
};