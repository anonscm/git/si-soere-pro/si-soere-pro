package org.inra.ecoinfo.pro.refdata.compositioncmp;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.refdata.component.IComposantDAO;
import org.inra.ecoinfo.pro.refdata.melange.IMelangeDAO;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.IValeurCaracteristiqueMPDAO;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.ValeurCaracteristiqueMP;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


public class Recorder extends AbstractCSVMetadataRecorder<CompositionsCMP> {

    private static final String PROPERTY_MSG_CMPV_BAD_COMP = "PROPERTY_MSG_CMPV_BAD_COMP";

    private static final String PROPERTY_MSG_CMPV_BAD_TNV = "PROPERTY_MSG_CMPV_BAD_TNV";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    protected ICompositionCMPDAO ccmpDAO;
    protected IMelangeDAO melangeDAO;
    IProduitDAO produitDAO;
    IComposantDAO composantDAO;
    IValeurCaracteristiqueMPDAO vcmpDAO;

    private String[] listeProduitsPossibles;
    private ConcurrentMap<String, String[]> listeComposantPPossibles;

    private String[] listeTypeCPMPossibles;
    private ConcurrentMap<String, String[]> listeCaracteristiquesCMPPossibles;
    private ConcurrentMap<String, String[]> listeValeurPossibles;
    Properties commentaireEn;

    private void afficheErreur(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    private void createCCMP(final CompositionsCMP compositioncmp) throws PersistenceException {
        ccmpDAO.saveOrUpdate(compositioncmp);
        ccmpDAO.flush();
    }

    private void createOrUpdateCCMP(final Melange melange,ValeurCaracteristiqueMP vcmp,String commentaire, final CompositionsCMP dbcompositioncmp) throws PersistenceException {
        if (dbcompositioncmp == null) {
            CompositionsCMP cmp = new CompositionsCMP(melange, vcmp, commentaire);
            cmp.setMelange(melange);
            cmp.setValeurcaracteristiquemp(vcmp);
            cmp.setCommentaire(commentaire);
            createCCMP(cmp);
        } else {
            updateBDCCMP(melange, vcmp, commentaire, dbcompositioncmp);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String code = tokenizerValues.nextToken();
                final String codec = tokenizerValues.nextToken();
                final String type = tokenizerValues.nextToken();
                final String nomc = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();

                Melange dbmelange = melangeDAO.getByNKeys(code, codec);
                if (dbmelange == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_COMP), code, codec));
                }
                ValeurCaracteristiqueMP dbvcmp = vcmpDAO.getCMPV(type, nomc, valeur);
                if (dbvcmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_TNV), type, nomc, valeur));
                }
               
                    final CompositionsCMP dbccmp = ccmpDAO.getByMixte(dbmelange, dbvcmp);
              
                        ccmpDAO.remove(dbccmp);
                    values = parser.getLine();

                
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<CompositionsCMP> getAllElements() throws PersistenceException {
        return ccmpDAO.getAll();
    }

    public ICompositionCMPDAO getCcmpDAO() {
        return ccmpDAO;
    }

    public IMelangeDAO getMelangeDAO() {
        return melangeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CompositionsCMP compositioncmp) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        ColumnModelGridMetadata produitColumn = new ColumnModelGridMetadata(compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getMelange().getProduits().getProd_key() != null
                ? compositioncmp.getMelange().getProduits().getProd_key()
                : "", listeProduitsPossibles, null, true, false, true);
        String composant = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getMelange().getComposant().getCodecomposant()!= null ? compositioncmp.getMelange().getComposant().getCodecomposant(): "";
        ColumnModelGridMetadata composantColumn = new ColumnModelGridMetadata(composant, listeComposantPPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> produitsRefs = new LinkedList<ColumnModelGridMetadata>();
        produitsRefs.add(composantColumn);
        produitColumn.setRefs(produitsRefs);
        composantColumn.setRefBy(produitColumn);
        composantColumn.setValue(composant);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(produitColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(composantColumn);

        String type = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur()!= null ? compositioncmp.getValeurcaracteristiquemp().getCaracteristiquematierepremieres().getTypecaracteristique().getTcar_nom(): "";
        ColumnModelGridMetadata columnType = new ColumnModelGridMetadata(type, listeTypeCPMPossibles, null, true, false, true);

        String caracteristique = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur()!= null ? compositioncmp.getValeurcaracteristiquemp().getCaracteristiquematierepremieres().getCmp_nom(): "";
        ColumnModelGridMetadata columnCaracteristique = new ColumnModelGridMetadata(caracteristique, listeCaracteristiquesCMPPossibles, null, true, false, true);

        String valeur = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur()!= null ? compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur().getCv_nom(): "";
        ColumnModelGridMetadata columnValeur = new ColumnModelGridMetadata(valeur, listeValeurPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> typeRefs = new LinkedList<ColumnModelGridMetadata>();
        typeRefs.add(columnCaracteristique);
        typeRefs.add(columnValeur);
        List<ColumnModelGridMetadata> nomrefs = new LinkedList<ColumnModelGridMetadata>();
        nomrefs.add(columnValeur);
        columnType.setRefs(typeRefs);
        columnCaracteristique.setRefs(nomrefs);
        columnCaracteristique.setRefBy(columnType);
        columnCaracteristique.setValue(caracteristique);
        columnValeur.setRefBy(columnCaracteristique);
        columnValeur.setValue(valeur);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnType);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnCaracteristique);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnValeur);
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        compositioncmp.getCommentaire(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(compositioncmp == null || compositioncmp.getCommentaire()== null ?
                        AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(compositioncmp.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    public IValeurCaracteristiqueMPDAO getVcmpDAO() {
        return vcmpDAO;
    }

    @Override
    protected ModelGridMetadata<CompositionsCMP> initModelGridMetadata() {
        try {
            listeDesProduitsPossibles();
            initTypeCMPPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        commentaireEn = localizationManager.newProperties(CompositionsCMP.NAME_ENTITY_JPA, CompositionsCMP.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void initTypeCMPPossibles() throws PersistenceException {
        Map<String, Map<String, List<String>>> valeursMap = new HashMap<String, Map<String, List<String>>>();
        List<ValeurCaracteristiqueMP> groupevcmp = vcmpDAO.getAll(ValeurCaracteristiqueMP.class);
        for (ValeurCaracteristiqueMP vcmp : groupevcmp) {
            String vcmptype = vcmp.getCaracteristiquematierepremieres().getTypecaracteristique().getTcar_nom();
            String vcmpnom = vcmp.getCaracteristiquematierepremieres().getCmp_nom();
            String vcmpvaleur = vcmp.getCaracteristiquevaleur().getCv_nom();
            if (!valeursMap.containsKey(vcmptype)) {
                valeursMap.put(vcmptype, new HashMap<String, List<String>>());
            }
            if (!valeursMap.get(vcmptype).containsKey(vcmpnom)) {
                valeursMap.get(vcmptype).put(vcmpnom, new LinkedList<String>());
            }
            valeursMap.get(vcmptype).get(vcmpnom).add(vcmpvaleur);
        }
        listeTypeCPMPossibles = valeursMap.keySet().toArray(new String[]{});
        listeCaracteristiquesCMPPossibles = new ConcurrentHashMap<String, String[]>();
        listeValeurPossibles = new ConcurrentHashMap<String, String[]>();
        for (Entry<String, Map<String, List<String>>> entryType : valeursMap.entrySet()) {
            String type = entryType.getKey();
            Set<String> caracteristiques = entryType.getValue().keySet();
            listeCaracteristiquesCMPPossibles.put(type, caracteristiques.toArray(new String[]{}));
            for (Entry<String, List<String>> entryCar : entryType.getValue().entrySet()) {
                String car = entryCar.getKey();
                List<String> values = entryCar.getValue();
                String typeCaracteristique = String.format("%s/%s", type, car);
                listeValeurPossibles.put(typeCaracteristique, values.toArray(new String[]{}));
            }
        }
    }

    private void listeDesProduitsPossibles() throws PersistenceException {
        List<Melange> groupecode = melangeDAO.getAll(Melange.class);
        List<String> produitsPossibles = new LinkedList<String>();
        Map<String, List<String>> composantsMapPossible = new ConcurrentHashMap<String, List<String>>();
        this.listeComposantPPossibles = new ConcurrentHashMap<String, String[]>();
        for (Melange melange : groupecode) {
            if (melange.getComposant() instanceof Produits) {
                continue;
            }
            String codeProduit = melange.getProduits().getProd_key();
            String codeComposant = melange.getComposant().getCodecomposant();
            if (!produitsPossibles.contains(codeProduit)) {
                produitsPossibles.add(codeProduit);
                composantsMapPossible.put(codeProduit, new LinkedList<String>());
            }
            composantsMapPossible.get(codeProduit).add(codeComposant);
        }
        listeProduitsPossibles = produitsPossibles.toArray(new String[]{});
        for (Entry<String, List<String>> entry : composantsMapPossible.entrySet()) {
            listeComposantPPossibles.put(entry.getKey(), entry.getValue().toArray(new String[]{}));
        }
    }

    private void persistCMP( Melange melange, ValeurCaracteristiqueMP vcmp,String commentaire) throws PersistenceException, BusinessException {
        final CompositionsCMP dbccmp = ccmpDAO.getByMixte(melange, vcmp);
        
        createOrUpdateCCMP(melange, vcmp, commentaire, dbccmp);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CompositionsCMP.NAME_ENTITY_JPA);
                int indexcode = tokenizerValues.currentTokenIndex();
                final String code = tokenizerValues.nextToken();
                final String codec = tokenizerValues.nextToken();
                int index = tokenizerValues.currentTokenIndex();
                final String type = tokenizerValues.nextToken();
                final String nomc = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();

                Melange dbmelange = melangeDAO.getByNKeys(code, codec);
                if (dbmelange == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_COMP),line,indexcode, code, codec));
                }

                ValeurCaracteristiqueMP dbvcmp = vcmpDAO.getCMPV(type, nomc, valeur);
                if (dbvcmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_TNV), line,index,nomc, type, valeur));
                }
                if (!errorsReport.hasErrors()) {
                    persistCMP(dbmelange, dbvcmp, commentaire);
                }
                values = parser.getLine();
            }
            afficheErreur(errorsReport);

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    public void setCcmpDAO(ICompositionCMPDAO ccmpDAO) {
        this.ccmpDAO = ccmpDAO;
    }

    public void setMelangeDAO(IMelangeDAO melangeDAO) {
        this.melangeDAO = melangeDAO;
    }

    public void setVcmpDAO(IValeurCaracteristiqueMPDAO vcmpDAO) {
        this.vcmpDAO = vcmpDAO;
    }

    private void updateBDCCMP(final Melange melange,ValeurCaracteristiqueMP vcmp,String commentaire, final CompositionsCMP dbcompositioncmp) throws PersistenceException {
        dbcompositioncmp.setMelange(melange);
        dbcompositioncmp.setValeurcaracteristiquemp(vcmp);
        dbcompositioncmp.setCommentaire(commentaire);
        vcmpDAO.saveOrUpdate(vcmp);
    }

}
