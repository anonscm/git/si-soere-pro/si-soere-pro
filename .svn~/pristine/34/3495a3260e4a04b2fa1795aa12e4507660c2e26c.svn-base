package org.inra.ecoinfo.pro.refdata.caracteristiqueprocess;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;
import java.util.Locale;
import java.util.Properties;


/**
 * 
 * @author adiankha
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<CarProcess> {

    protected ICaracteristiqueProcessDAO caractprocessDAO;
    private Properties nomCPEn;
    private Properties commentEn;

    private void createCaracteristiqueProcess(final CarProcess carprocess) throws PersistenceException {
        caractprocessDAO.saveOrUpdate(carprocess);
        caractprocessDAO.flush();
    }
    private void createOrUpdateCaracteristiqueProcess(final String code,String nom,String commentaire, final CarProcess dbcarprocess) throws PersistenceException {
        if (dbcarprocess == null) {
             final CarProcess carprocess = new CarProcess(code,nom, commentaire);
             carprocess.setCpro_code(code);
             carprocess.setCpro_nom(nom);
             carprocess.setCommentaire(commentaire);
            createCaracteristiqueProcess(carprocess);
        } else {
            updateBDCaracteristiqueProcess(code, nom, commentaire, dbcarprocess);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String libelle = tokenizerValues.nextToken();

                caractprocessDAO.remove(caractprocessDAO.SearchByCP(libelle));
                
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<CarProcess> getAllElements() throws PersistenceException {
        return caractprocessDAO.getAll();
    }

    public ICaracteristiqueProcessDAO getCaractprocessDAO() {
        return caractprocessDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CarProcess carprocess) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(carprocess == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        carprocess.getCpro_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(carprocess == null || carprocess.getCpro_nom()== null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        nomCPEn.getProperty(carprocess.getCpro_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(carprocess == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        carprocess.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
         
          lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(carprocess == null || carprocess.getCommentaire()== null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        commentEn.getProperty(carprocess.getCommentaire()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        return lineModelGridMetadata;
    }
    
      @Override
    protected ModelGridMetadata<CarProcess> initModelGridMetadata() {
        nomCPEn = localizationManager.newProperties(CarProcess.JPA_NAME_ENTITY, CarProcess.JPA_COLUMN_NAME, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(CarProcess.JPA_NAME_ENTITY, CarProcess.JPA_COLUMN_COMENT,Locale.ENGLISH);
        return super.initModelGridMetadata();

    }


    private void persistCaracteristiqueProcess(final String code, final String tp_nom,String commentaire) throws PersistenceException, BusinessException {
        final CarProcess dbcarprocess = caractprocessDAO.SearchByCP(tp_nom);
        createOrUpdateCaracteristiqueProcess(code, tp_nom, commentaire, dbcarprocess);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);

            // je parcours chaque ligne du fichier
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CarProcess.JPA_NAME_ENTITY);
                // je parcours chaque colonne d'une ligne
                final String tp_nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(tp_nom);
                 String commentaire = tokenizerValues.nextToken();
                persistCaracteristiqueProcess(code, tp_nom, commentaire);
                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    public void setCaractprocessDAO(ICaracteristiqueProcessDAO caractprocessDAO) {
        this.caractprocessDAO = caractprocessDAO;
    }

    /**
     * 
     * @param carprocess
     * @param dbcarprocess
     */
    private void updateBDCaracteristiqueProcess(final String code,String nom,String commentaire, final CarProcess dbcarprocess) throws PersistenceException {
        dbcarprocess.setCpro_code(code);
        dbcarprocess.setCpro_nom(nom);
        dbcarprocess.setCommentaire(commentaire);
        caractprocessDAO.saveOrUpdate(dbcarprocess);

    }
    
   

}
