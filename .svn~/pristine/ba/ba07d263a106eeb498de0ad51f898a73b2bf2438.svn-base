package org.inra.ecoinfo.pro.refdata.substratpedologique;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


public class Recorder extends AbstractCSVMetadataRecorder<Substratpedologique>{
    ISubstratPedologiqueDAO substratDAO;
    private Properties substratEN;
    
    private void createSubstrat(final Substratpedologique substrat) throws PersistenceException {
        substratDAO.saveOrUpdate(substrat);
        substratDAO.flush();
    }
    
    private void updateSubstrat(final String nom, final Substratpedologique dbsubstrat) throws PersistenceException {
        dbsubstrat.setNom(nom);
        substratDAO.saveOrUpdate(dbsubstrat);
    }

    private void createOrUpdateSubstrat(final String code,String nom, final Substratpedologique dbsubstrat) throws PersistenceException {
        if (dbsubstrat == null) {
            final Substratpedologique substrat = new Substratpedologique(nom);
        substrat.setCode(code);
        substrat.setNom(nom);
            createSubstrat(substrat);
        } else {
            updateSubstrat(nom, dbsubstrat);
        }
    }
    private void persistSubstrat( final String code, final String nom) throws PersistenceException, BusinessException {
        final Substratpedologique dbsubstrat = substratDAO.getByKey(code);
        createOrUpdateSubstrat(code,nom, dbsubstrat);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Substratpedologique substrat) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(substrat == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : substrat.getNom(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(substrat == null || substrat.getNom() == null ? 
                              AbstractCSVMetadataRecorder.EMPTY_STRING : substratEN.getProperty(substrat.getNom()),
                                   ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Substratpedologique.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
              final String code = Utils.createCodeFromString(nom);

                final Substratpedologique dbsubstrat = substratDAO.getByKey(code);
                if (dbsubstrat != null) {
                    substratDAO.remove(dbsubstrat);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }

    @Override
    protected List<Substratpedologique> getAllElements() throws PersistenceException {
       return substratDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Substratpedologique.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistSubstrat(code, nom);
                values = parser.getLine();
            }   

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    @Override
    protected ModelGridMetadata<Substratpedologique> initModelGridMetadata() {
            substratEN = localizationManager.newProperties(Substratpedologique.NAME_ENTITY_JPA, Substratpedologique.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    
    public ISubstratPedologiqueDAO getSubstratDAO() {
        return substratDAO;
    }

    
    public void setSubstratDAO(ISubstratPedologiqueDAO substratDAO) {
        this.substratDAO = substratDAO;
    }

    
    public Properties getSubstratEN() {
        return substratEN;
    }

    
    public void setSubstratEN(Properties substratEN) {
        this.substratEN = substratEN;
    }   

}
