/**
 *
 */
package org.inra.ecoinfo.pro.refdata.protocole;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


public class Recorder extends AbstractCSVMetadataRecorder<Protocole> {

    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IProtocoleDAO protocoleDAO;
    
    /**
     * @param nom
     * @param anneeDebut
     * @param anneeFin
     * @param utiliseGuide
     * @param dbProtocole
     * @param protocole
     * @throws PersistenceException
     */
    private void createOrUpdate(String nom, String anneeDebut, String anneeFin, Boolean utiliseGuide, Protocole dbProtocole, Protocole protocole) throws PersistenceException 
    {
        if (dbProtocole == null) 
        {
            protocoleDAO.saveOrUpdate(protocole);
        } 
        else
        {
            dbProtocole.setAnneeDebutVal(anneeDebut);
            dbProtocole.setAnneeFinVal(anneeFin);
            dbProtocole.setUtiliseGuideMethodologique(utiliseGuide);
            protocoleDAO.saveOrUpdate(dbProtocole);
        }
    }

    
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();

                protocoleDAO.remove(protocoleDAO.getByNom(nom));
                
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Protocole> getAllElements() throws PersistenceException {
        return protocoleDAO.getAll(Protocole.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Protocole protocole) throws PersistenceException 
    {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        //Nom
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocole == null ? Constantes.STRING_EMPTY : protocole.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        //Année début
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocole == null ? Constantes.STRING_EMPTY : protocole.getAnneeDebutVal(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        //Année fin
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocole == null ? Constantes.STRING_EMPTY : protocole.getAnneeFinVal(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        //Utilisation du guide méthodologique Réseau PRO (oui/non)
        String utiliseGuide = protocole == null ? new String("") : protocole.getUtiliseGuideMethodologique() ? RefDataConstantes.valBooleanObligatoire[0] : RefDataConstantes.valBooleanObligatoire[1];
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocole == null ? Constantes.STRING_EMPTY : utiliseGuide, RefDataConstantes.valBooleanObligatoire, null, false, false, true));

        return lineModelGridMetadata;
    }
    
    
    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }
    
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        ErrorsReport errorsReport = new ErrorsReport();
        long line =0;
        
        try 
        {
            skipHeader(parser);
            
            String[] values = parser.getLine();
            while (values != null) 
            {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Protocole.TABLE_NAME);
           
                String nom = tokenizerValues.nextToken();
                int indexand = tokenizerValues.currentTokenIndex();
                String anneeDebut = tokenizerValues.nextToken();
                int indexanf = tokenizerValues.currentTokenIndex();
                String anneeFin = tokenizerValues.nextToken();
                int indexUtiliseGuide = tokenizerValues.currentTokenIndex();
                String utiliseGuide = tokenizerValues.nextToken();

                verifieAnnees(anneeDebut, anneeFin, nom, line+1, indexand+1, indexanf+1, errorsReport);
                verifieMethodo(utiliseGuide, line+1, indexUtiliseGuide+1, errorsReport);
                
                Protocole dbProtocole = protocoleDAO.getByNom(nom);

                Boolean utilGuide = utiliseGuide.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[0]) ? new Boolean("true") : new Boolean("false");
                Protocole protocole = new Protocole(nom, anneeDebut, anneeFin, utilGuide);
                
                if(!errorsReport.hasErrors())
                {
                	createOrUpdate(nom, anneeDebut, anneeFin, utilGuide, dbProtocole, protocole);
                }

                values = parser.getLine();
            } // fin while
            
            gestionErreurs(errorsReport);
            
        } 
        catch (IOException e1) 
        {
            throw new BusinessException(e1.getMessage(), e1);
        } 
        catch (PersistenceException e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    /**
     * @param anneeDebut
     * @param anneeFin
     * @param nom
     * @param line
     * @param indexD
     * @param indexF
     * @param errorsReport
     */
    private void verifieAnnees(String anneeDebut, String anneeFin, String nom, long line, int indexD, int indexF, ErrorsReport errorsReport)
    {
    	verifieAnneeFormat(anneeFin, line, indexF, errorsReport);
    	verifieAnneeFormat(anneeDebut, line, indexD, errorsReport);
    	
    	if (anneeFin != null && new Integer(anneeFin) < new Integer(anneeDebut)) 
        {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ERROR_DATE"),line, indexF, anneeFin, nom, anneeDebut));
        }
    }
    
    /**
     * @param annee
     * @param line
     * @param index
     * @param errorsReport
     */
    public void verifieAnneeFormat(String annee, long line, int index, ErrorsReport errorsReport)
    {
    	if(annee != null)
        {
    		try 
    		{
    			DateUtil.getSimpleDateFormatDateyyyyLocale().parse(annee);
    			
    			if(annee.length() < 4)
    			{
    				errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
    			}
    		} 
    		catch (ParseException e) 
    		{
       	 		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
    		}
        }
    }
    
    /**
     * @param utiliseGuide
     * @param line
     * @param index
     * @param errorsReport
     */
    public void verifieMethodo(String utiliseGuide, long line, int index, ErrorsReport errorsReport)
    {
    	if (!utiliseGuide.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[0]) && !utiliseGuide.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[1])) 
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ERROR_BOOLEAN"), line, index, datasetDescriptor.getColumns().get(5).getName()));
    	}
    }

    /**
     * @param protocoleDAO
     *            the protocoleDAO to set
     */
    public void setProtocoleDAO(IProtocoleDAO protocoleDAO) {
        this.protocoleDAO = protocoleDAO;
    }

}
