package org.inra.ecoinfo.pro.refdata.protocoledispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.protocole.IProtocoleDAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


public class Recorder extends AbstractCSVMetadataRecorder<ProtocoleDispositif> {

    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IProtocoleDAO protocoleDAO;
    IDispositifDAO dispositifDAO;
    IProtocoleDispositifDAO protocoleDispositifDAO;

    /**
     * @return
     * @throws PersistenceException
     */
    private String[] getDispositifPossibles() throws PersistenceException 
    {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll(Dispositif.class);
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) 
        {
            dispositifPossibles[index++] = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
        }
        return dispositifPossibles;
    }

    /**
     * @return
     * @throws PersistenceException
     */
    private String[] getProtocolePossibles() throws PersistenceException 
    {
        List<Protocole> lstProtocoles = protocoleDAO.getAll(Protocole.class);
        String[] protocolePossibles = new String[lstProtocoles.size()];
        int index = 0;
        for (Protocole protocole : lstProtocoles) 
        {
            protocolePossibles[index++] = protocole.getNom();
        }
        return protocolePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomProtocole = tokenizerValues.nextToken();

                int index1 = codeDispositifLieu.indexOf('(');
                int index2 = codeDispositifLieu.indexOf(')');
                String codeDispositif = index1 != -1 ? codeDispositifLieu.substring(0, index1).trim() : codeDispositifLieu;
                String nomLieu = index1 != -1 && index2 != -1 ? codeDispositifLieu.substring(index1 + 1, index2).trim() : codeDispositifLieu;

                Lieu lieu = lieuDAO.getByNom(nomLieu);
                Dispositif dispositif = dispositifDAO.getByCodeLieu(codeDispositif, lieu);
                Protocole protocole = protocoleDAO.getByNom(nomProtocole);

                ProtocoleDispositif protocoleDispositif = protocoleDispositifDAO.getByProtocoleDispositif(protocole, dispositif);
                protocoleDispositifDAO.remove(protocoleDispositif);
                protocoleDispositifDAO.flush();
                
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ProtocoleDispositif> getAllElements() throws PersistenceException {
        return protocoleDispositifDAO.getAll(ProtocoleDispositif.class);
    }

 
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProtocoleDispositif protocoleDispositif) throws PersistenceException 
    {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        String codeDipositifLieu = protocoleDispositif == null ? new String("") : protocoleDispositif.getDispositif().getCode() + " (" + protocoleDispositif.getDispositif().getLieu().getNom() + ")";
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocoleDispositif == null ? Constantes.STRING_EMPTY : codeDipositifLieu, getDispositifPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocoleDispositif == null ? Constantes.STRING_EMPTY : protocoleDispositif.getProtocole().getNom(), getProtocolePossibles(), null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        ErrorsReport errorsReport = new ErrorsReport();
        long line=0;
        try 
        {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) 
            {
            	line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                int indexdispo = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                int indexpro = tokenizerValues.currentTokenIndex();
                String nomProtocole = tokenizerValues.nextToken();
                
                int index1 = codeDispositifLieu.indexOf('(');
                int index2 = codeDispositifLieu.indexOf(')');
                String codeDispositif = index1 != -1 ? codeDispositifLieu.substring(0, index1).trim() : codeDispositifLieu;
                String nomLieu = index1 != -1 && index2 != -1 ? codeDispositifLieu.substring(index1 + 1, index2).trim() : codeDispositifLieu;
                
                Lieu lieu = verifieLieu(nomLieu, line+1, indexdispo+1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line+1, indexdispo+1, errorsReport);
                Protocole protocole = verifieProtocole(nomProtocole, line+1, indexpro+1, errorsReport);
                
                ProtocoleDispositif protocoleDispositif = new ProtocoleDispositif(protocole, dispositif);
                ProtocoleDispositif dbProtocoleDispositif = protocoleDispositifDAO.getByProtocoleDispositif(protocole, dispositif);
                
                if(!errorsReport.hasErrors())
                {
                	if (dbProtocoleDispositif == null) 
                	{
                		protocoleDispositifDAO.saveOrUpdate(protocoleDispositif);
                	}
                } 
                
                values = parser.getLine();
            }
            
            RefDataUtil.gestionErreurs(errorsReport);
            
        } 
        catch (IOException e1) 
        {
            throw new BusinessException(e1.getMessage(), e1);
        } 
        catch (PersistenceException e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport)
    {
    	Lieu lieu = lieuDAO.getByNom(nomLieu);
        if (lieu == null) 
        {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }
         
         return lieu;
    }
    
    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport)
    {
    	Dispositif dispositif = dispositifDAO.getByCodeLieu(codeDispositif, lieu);
        if (dispositif == null) 
        {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }
         
         return dispositif;
    }
    

    /**
     * @param nomProtocole
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Protocole verifieProtocole(String nomProtocole, long line, int index, ErrorsReport errorsReport)
    {
    	Protocole protocole = protocoleDAO.getByNom(nomProtocole);
        if (protocole == null) 
        {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "PROTOCOLE_NONDEFINI"),line, index, nomProtocole));
        }
        
        return protocole;
    }
    
	/**
	 * @param lieuDAO the lieuDAO to set
	 */
	public void setLieuDAO(ILieuDAO lieuDAO) {
		this.lieuDAO = lieuDAO;
	}

	/**
	 * @param protocoleDAO the protocoleDAO to set
	 */
	public void setProtocoleDAO(IProtocoleDAO protocoleDAO) {
		this.protocoleDAO = protocoleDAO;
	}

	/**
	 * @param dispositifDAO the dispositifDAO to set
	 */
	public void setDispositifDAO(IDispositifDAO dispositifDAO) {
		this.dispositifDAO = dispositifDAO;
	}

	/**
	 * @param protocoleDispositifDAO the protocoleDispositifDAO to set
	 */
	public void setProtocoleDispositifDAO(
			IProtocoleDispositifDAO protocoleDispositifDAO) {
		this.protocoleDispositifDAO = protocoleDispositifDAO;
	}
      
}
