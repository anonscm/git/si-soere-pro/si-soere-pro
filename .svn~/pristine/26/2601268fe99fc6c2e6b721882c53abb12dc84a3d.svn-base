package org.inra.ecoinfo.pro.refdata.filiationechantillonsol;

import com.Ostermiller.util.CSVParser;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.inra.ecoinfo.pro.refdata.echantillonsol.IEchantillonSolDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author Vivianne
 */
public class Recorder extends AbstractCSVMetadataRecorder<FiliationEchantillonSol> {
	
	IFiliationEchantillonSolDAO filiationSolDAO;
	IEchantillonSolDAO echantillonSolDAO;
	private String[] listeCodeEchanPossible;
	
	private static final String PROPERTY_MSG_FILIATIONSOL_BAD_ECHANTILLON = "PROPERTY_MSG_FILIATIONSOL_BAD_ECHANTILLON";
	private static final String PROPERTY_MSG_FILIATIONSOL_BAD_NUMBER_ECHANTILLON = "PROPERTY_MSG_FILIATIONSOL_BAD_NUMBER_ECHANTILLON";
	private static final String PROPERTY_MSG_FILIATIONSOL_SAMPLE_EQUAL_PARENT="PROPERTY_MSG_FILIATIONSOL_SAMPLE_EQUAL_PARENT";
	private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
	
	private void createFiliationSol(FiliationEchantillonSol filiationsol)throws PersistenceException{
        filiationSolDAO.saveOrUpdate(filiationsol);
        filiationSolDAO.flush();
    }
	
	private void updateFiliationSol(long numeroechantillon, EchantillonsSol code,FiliationEchantillonSol dbF )throws PersistenceException{
		dbF.setNumeroechantillon(numeroechantillon);
		dbF.setEchantillonsSol(code);
		filiationSolDAO.saveOrUpdate(dbF);
		
	}
	
	
	private void createOrUpdateFiliationSol(long numeroechantillon, EchantillonsSol code,  FiliationEchantillonSol dbF) throws PersistenceException {
		if(dbF==null){
			FiliationEchantillonSol filiationechsol = new FiliationEchantillonSol(numeroechantillon, code);
			filiationechsol.setNumeroechantillon(numeroechantillon);
			filiationechsol.setEchantillonsSol(code);
			
			createFiliationSol(filiationechsol);

		}else{
			updateFiliationSol(numeroechantillon, code, dbF);

		}
	}
	
	
	private void persistFiliatiolSol(long numeroechantillon,EchantillonsSol code) throws PersistenceException, BusinessException{
		
		FiliationEchantillonSol dbFiliationSol = filiationSolDAO.getByNames(numeroechantillon, code);
		createOrUpdateFiliationSol(numeroechantillon, code, dbFiliationSol);

	}
	
	
	private void listeEchantillonsPossibles() throws PersistenceException {
        List<EchantillonsSol> groupescode = echantillonSolDAO.getAll(EchantillonsSol.class);
        String[] listecodePossibles = new String[groupescode.size() + 1];
        listecodePossibles[0] = "";
        int index = 1;
        for (EchantillonsSol codeechantillon : groupescode) {
            listecodePossibles[index++] = codeechantillon.getCodelabo();
        }
        this.listeCodeEchanPossible = listecodePossibles;
    }
	
	@Override
	public void processRecord(CSVParser parser, File file, String encoding)throws BusinessException {
		  
		final ErrorsReport errorsReport = new ErrorsReport();
		
	        try {
	            skipHeader(parser);
	            long line =0;
	            String[] values = parser.getLine();
	            while (values != null) {
	               line++;
	                final TokenizerValues tokenizerValues = new TokenizerValues(values, FiliationEchantillonSol.NAME_ENTITY_JPA);
	                
	                final String num_ech = tokenizerValues.nextToken();
	                long numeroechantillon = Long.parseLong(num_ech);
	                int indexech = tokenizerValues.currentTokenIndex();
	                String codeech = tokenizerValues.nextToken();
                        
	                EchantillonsSol dbnumech = echantillonSolDAO.numEchanExist(numeroechantillon);
	                if(dbnumech==null){	
	                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_FILIATIONSOL_BAD_NUMBER_ECHANTILLON),line, numeroechantillon));
                        }
	                EchantillonsSol dbechsol = echantillonSolDAO.getByKey(codeech);
	                
	                if(dbechsol==null){
	                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_FILIATIONSOL_BAD_ECHANTILLON),line,indexech, codeech));
	                }
	                if (dbechsol == dbnumech) {
	                	errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_FILIATIONSOL_SAMPLE_EQUAL_PARENT), numeroechantillon,codeech));
		   	      }
	                if (!errorsReport.hasErrors()) {	 
	                	 persistFiliatiolSol(numeroechantillon, dbechsol );
	                 }
				values = parser.getLine();	
	                 }
	            
	            gestionErreurs(errorsReport);

	        } catch (final IOException e) {
	            throw new BusinessException(e.getMessage(), e);
	        } catch (final PersistenceException e) {
	            throw new BusinessException(e.getMessage(), e);
	        }
	    }

	
	
	@Override
	public LineModelGridMetadata getNewLineModelGridMetadata(FiliationEchantillonSol filiation) throws PersistenceException {
		
		final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
	        
	        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
	                new ColumnModelGridMetadata(filiation == null || filiation.getNumeroechantillon()== 0 ? 
	                        AbstractCSVMetadataRecorder.EMPTY_STRING : filiation.getNumeroechantillon(), 
	                                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
	       
	        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
	                new ColumnModelGridMetadata(filiation == null || filiation.getEchantillonsSol().getCodelabo()== null ? 
	                        AbstractCSVMetadataRecorder.EMPTY_STRING : filiation.getEchantillonsSol().getCodelabo()!= null ?
	                                 filiation.getEchantillonsSol().getCodelabo(): "",
	                                 listeCodeEchanPossible, null, true, false, true));  
	        
	        return lineModelGridMetadata;
	}

	@Override
	public void deleteRecord(CSVParser parser, File file, String encoding)throws BusinessException {
		 try 
	        {
	            String[] values = parser.getLine();
	            while (values != null) 
	            {
	                TokenizerValues tokenizerValues = new TokenizerValues(values);
	                final String num_ech = tokenizerValues.nextToken();
	                long numeroechantillon = Long.parseLong(num_ech);
	                filiationSolDAO.remove(filiationSolDAO.getByKey(numeroechantillon));
	                
	                values = parser.getLine();
	            }
	        } 
	        catch (Exception e) 
	        {
	            throw new BusinessException(e.getMessage(), e);
	        }
		
	}

	
	 private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
	        if (errorsReport.hasErrors()) {
	            throw new BusinessException(errorsReport.getErrorsMessages());
	        }
	 }

	@Override
	protected List<FiliationEchantillonSol> getAllElements()throws PersistenceException {
		return filiationSolDAO.getAll();
	}
	
		
	 public void setFiliationSolDAO(IFiliationEchantillonSolDAO filiationSolDAO) {
		this.filiationSolDAO = filiationSolDAO;
	}

	public void setEchantillonSolDAO(IEchantillonSolDAO echantillonSolDAO) {
		this.echantillonSolDAO = echantillonSolDAO;
	}
	@Override
	protected ModelGridMetadata<FiliationEchantillonSol> initModelGridMetadata() {
	        try {
	        	listeEchantillonsPossibles();
	        } catch (PersistenceException e) {
	            e.printStackTrace();
	        }
	       
	        return super.initModelGridMetadata();
	    }

}
