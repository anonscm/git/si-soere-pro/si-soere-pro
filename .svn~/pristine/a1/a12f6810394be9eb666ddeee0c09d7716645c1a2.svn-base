package org.inra.ecoinfo.pro.refdata.typeclimat;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
public class Recorder extends AbstractCSVMetadataRecorder<Typeclimat>{
    ITypeClimatDAO typeclimatDAO;
    private Properties tclimatEN;
    
    private void createTypeclimat(final Typeclimat tclimat) throws PersistenceException {
        typeclimatDAO.saveOrUpdate(tclimat);
        typeclimatDAO.flush();
    }
    
    private void updateTypeclimat(final String nom,final Typeclimat dbtclimat) throws PersistenceException {
        dbtclimat.setNom(nom);
        typeclimatDAO.saveOrUpdate(dbtclimat);
    }

    private void createOrUpdateTypeclimat(final String code,String nom,final Typeclimat dbtclimat) throws PersistenceException {
        if (dbtclimat == null) {
             final Typeclimat typeclimat = new Typeclimat(code,nom);
        typeclimat.setCode(code);
        typeclimat.setNom(nom);
            createTypeclimat(typeclimat);
        } else {
            updateTypeclimat(nom, dbtclimat);
        }
    }
    private void persistTypeclimat( final String code, final String nom) throws PersistenceException, BusinessException {
        final Typeclimat dbtypeclimat = typeclimatDAO.getByName(nom);
        createOrUpdateTypeclimat(code,nom, dbtypeclimat);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Typeclimat typeclimat) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeclimat == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : typeclimat.getNom(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeclimat == null || typeclimat.getNom() == null ? 
                              AbstractCSVMetadataRecorder.EMPTY_STRING : tclimatEN.getProperty(typeclimat.getNom()),
                                   ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typeclimat.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final Typeclimat dbtype = typeclimatDAO.getByName(nom);
                if (dbtype != null) {
                    typeclimatDAO.remove(dbtype);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }

    @Override
    protected List<Typeclimat> getAllElements() throws PersistenceException {
        return typeclimatDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typeclimat.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistTypeclimat(code, nom);
                values = parser.getLine();
            }   

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    @Override
    protected ModelGridMetadata<Typeclimat> initModelGridMetadata() {
            tclimatEN = localizationManager.newProperties(Typeclimat.NAME_ENTITY_JPA, Typeclimat.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    
    public ITypeClimatDAO getTypeclimatDAO() {
        return typeclimatDAO;
    }

    
    public void setTypeclimatDAO(ITypeClimatDAO typeclimatDAO) {
        this.typeclimatDAO = typeclimatDAO;
    }

    
    public Properties getTclimatEN() {
        return tclimatEN;
    }

    
    public void setTclimatEN(Properties tclimatEN) {
        this.tclimatEN = tclimatEN;
    }

    
}
