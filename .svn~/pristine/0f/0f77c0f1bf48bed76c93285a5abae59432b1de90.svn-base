/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import static org.inra.ecoinfo.AbstractJPADAO.LOGGER;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.IMesureProEtudieDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie;
import org.inra.ecoinfo.pro.dataset.sol.JPA.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.sol.JPA.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordProEtudie extends AbstractProcessRecord {

    IMesureProEtudieDAO<MesureProEtudie> mesureProEtudieDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    IListeItineraireDAO listeItineraireDAO;
     IDescriptionTraitementDAO descriptionTraitementDAO;
    protected static final String BUNDLE_PATH_PROETUDIE = "org.inra.ecoinfo.pro.dataset.itk.messages";
    private static final String MSG_ERROR_PROETUDUE__DB = "MSG_ERROR_PROETUDUE__DB";
    private static final String MSG_ERROR_CULTURES_PE_DB = "MSG_ERROR_CULTURES_PE_DB";
    private static final String MSG_ERROR_LISTEITINERAIRE_NA_DB = "MSG_ERROR_LISTEITINERAIRE_NA_DB";
    private static final String MSG_ERROR_LISTEITINERAIRE_VV_DB = "MSG_ERROR_LISTEITINERAIRE_VV_DB";
    private static final String MSG_ERROR_LISTEITINERAIRE_PR_DB = "MSG_ERROR_LISTEITINERAIRE_PR_DB";
    private static final String MSG_ERROR_LISTEITINERAIRE_LP_DB = "MSG_ERROR_LISTEITINERAIRE_LP_DB";
    private static final String MSG_ERROR_LISTEITINERAIRE_CT_DB = "MSG_ERROR_LISTEITINERAIRE_CT_DB";
    private static final String MSG_ERROR_QUANTITE_NOT_FOUND_DVU_DB = "MSG_ERROR_QUANTITE_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_CA_NOT_FOUND_DVU_DB = "MSG_ERROR_CA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_CONDITIONT_NOT_FOUND_DVU_DB = "MSG_ERROR_CONDITIONT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_VENTV_NOT_FOUND_DVU_DB = "MSG_ERROR_VENTV_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_NIVEAU_NOT_FOUND_DVU_DB = "MSG_ERROR_NIVEAU_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_TRAITEMENT_PRO_NOT_FOUND_IN_DB="MSG_ERROR_TRAITEMENT_PRO_NOT_FOUND_IN_DB";

//    test des informations liées a lunicité
    private static final String MSG_ERROR_HUMIDITE_PE = "MSG_ERROR_HUMIDITE_PE";
    private static final String MSG_ERROR_CODE_TEMPRATURE_PE = "MSG_ERROR_CODE_TEMPRATURE_PE";
    private static final String MSG_ERROR_VITESSE_VENT_PE = "MSG_ERROR_VITESSE_VENT_PE";
    private static final String MSG_ERROR_NIVEAU_ATTEINT_PE = "MSG_ERROR_NIVEAU_ATTEINT_PE";
    private static final String MSG_ERROR_TYPE_OBSERVATION_PE = "MSG_ERROR_TYPE_OBSERVATION_PE";
    private static final String MSG_ERROR_NOM_OBSERVATION_PE = "MSG_ERROR_NOM_OBSERVATION_PE";
    private static final String MSG_ERROR_MATERIEL_PE = "MSG_ERROR_MATERIEL_PE";
     private static final String MSG_ERROR_CULTURE_PE = "MSG_ERROR_CULTURE_PE";

    public ProcessRecordProEtudie() {
        super();
    }

    private long readLines(final CSVParser parser, final Map<Date, List<ProduitEtudieLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException, ParseException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final Date datedebut = DateUtil.getSimpleDateFormatDateUTC().parse(cleanerValues.nextToken());
            final String codedispositif = cleanerValues.nextToken();
            final String codetraitement = cleanerValues.nextToken();
            final String nomparcelle = cleanerValues.nextToken();
            final String nomplacette = cleanerValues.nextToken();
            final String localisationprecide = cleanerValues.nextToken();
            final String typepro = cleanerValues.nextToken();
            final String codepro = cleanerValues.nextToken();
            final Date datefin = DateUtil.getSimpleDateFormatDateUTC().parse(cleanerValues.nextToken());
            final float quantiteapport = Float.parseFloat(cleanerValues.nextToken());
            final String unite = cleanerValues.nextToken();
            final String materielapport = cleanerValues.nextToken();
            final float profondeurapport = Float.parseFloat(cleanerValues.nextToken());
            final Date dateenfouissement = DateUtil.getSimpleDateFormatDateUTC().parse(cleanerValues.nextToken());
            final String culture = cleanerValues.nextToken();
            final String codebbch = cleanerValues.nextToken();
            final String precisionstade = cleanerValues.nextToken();
            final String conditionhumidite = cleanerValues.nextToken();
            final String conditiontemperature = cleanerValues.nextToken();
            final String vitessevent = cleanerValues.nextToken();
            final String observationqualite = cleanerValues.nextToken();
            final String nomobservation = cleanerValues.nextToken();
            final String niveauatteint = cleanerValues.nextToken();
            final String commentaire = cleanerValues.nextToken();

        

            final ProduitEtudieLineRecord line = new ProduitEtudieLineRecord(lineCount, datedebut, codedispositif, codetraitement, nomparcelle, nomplacette,
                    localisationprecide, typepro, codepro, datefin, quantiteapport, unite, materielapport, profondeurapport, dateenfouissement,
                    culture, codebbch, precisionstade, conditionhumidite, conditiontemperature, vitessevent, observationqualite, nomobservation, niveauatteint, commentaire);
            Date date;
            try {
                date = DateUtil.getSimpleDateFormatDateUTC().parse(
                        DateUtil.getSimpleDateFormatDateUTC().format(datedebut));
                if (!lines.containsKey(date)) {
                    lines.put(date, new LinkedList<>());
                }
                lines.get(date).add(line);
            } catch (ParseException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        datedebut, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void recordErrors(final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<Date, List<ProduitEtudieLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<ProduitEtudieLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, datasetDescriptorPRO, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | ParseException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            java.util.logging.Logger.getLogger(ProcessRecordProEtudie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<Date, List<ProduitEtudieLineRecord>> lines, DatasetDescriptorPRO datasetDescriptorPRO,
            final SortedSet<ProduitEtudieLineRecord> ligneEnErreur) throws PersistenceException,
            ParseException,
            InsertionDatabaseException {
        Iterator<Entry<Date, List<ProduitEtudieLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Date, List<ProduitEtudieLineRecord>> lineDateEntry = iterator.next();
            Date date = lineDateEntry.getKey();
            for (ProduitEtudieLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties, datasetDescriptorPRO);
            }

        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureProEtudieDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public void setMesureProEtudieDAO(IMesureProEtudieDAO<MesureProEtudie> mesureProEtudieDAO) {
        this.mesureProEtudieDAO = mesureProEtudieDAO;
    }

    private void buildMesure(ProduitEtudieLineRecord line, VersionFile versionFile, SortedSet<ProduitEtudieLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, ISessionPropertiesPRO sessionProperties, DatasetDescriptorPRO datasetDescriptorPRO)
            throws PersistenceException,
            InsertionDatabaseException, ParseException {
        final Date datedebut = line.getDatedebut();
        final String codedisp = line.getCodedispositif();
       // final String codeTrait = line.getCodetraitement();
         String keydisp = Utils.createCodeFromString(sessionProperties.getDispositif().getCode());
        final String codeTrait = Utils.createCodeFromString(line.getCodetraitement());
        String codeunique = keydisp + "_" + codeTrait;
         DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByNKey(codeunique);
              if(dbTraitement==null){
             errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_TRAITEMENT_PRO_NOT_FOUND_IN_DB), codeTrait,keydisp));
             }
        final String codeParcelle = line.getCodeparcelle();
        final String nomplacette = line.getNomplacette();
        final String localisationprecise = Utils.createCodeFromString(line.getLocalisationprecise());
        final String typepro = line.getTypepro();
        final String codepro = line.getCodepro();
        final Date datefin = line.getDatefin();
        final float quantiteapport = line.getQuantiteapport();
        final String unite = line.getUnite();
        final String materielapport = line.getMaterielapport();
        final float profondeurapport = line.getProfondeurapport();
        final Date dateenfouissement = line.getDateenfouillement();
        final String culture = line.getCulture();
        final String codebbch = line.getCodebbch();
        final String precisionstade = line.getPrecisionstade();
        final String conditionhumidite = Utils.createCodeFromString(line.getConditionhumidite());
        final String conditiontemperature = Utils.createCodeFromString(line.getConditiontemperature());
        final String vitessevent = Utils.createCodeFromString(line.getVitessevent());
        final String observationqualite = line.getTypeobservation();
        final String nomobservation = line.getNomobservation();
        final String niveauatteint = Utils.createCodeFromString(line.getNiveauatteinte());
        final String commentaire = line.getCommentaire();

        String datatype = versionFile.getDataset().getLeafNode().getDatatype().getCode();

        String cdatatype = Utils.createCodeFromString(datatype);
        String quantite = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(9));
        String proapport = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(12));
        String localisation = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(5));
        String conditionair = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(17));
        String conditiontemp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(18));
        String vitesse = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(19));
       // String niveau = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(22));
        
         MesureProEtudie mesureProEtudie = getOrCreate(datedebut, codedisp, codeTrait, codeParcelle, nomplacette, typepro, codepro, dbTraitement, datefin, codebbch, culture, observationqualite, nomobservation, precisionstade, materielapport, niveauatteint, dateenfouissement, versionFile, commentaire);

        ListeItineraire dbLP = listeItineraireDAO.getByNKeys(localisation, localisationprecise);
        if (dbLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_LISTEITINERAIRE_LP_DB), localisation, localisationprecise));
        }

        ListeItineraire dbCA = listeItineraireDAO.getByNKeys(conditionair, conditionhumidite);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_LISTEITINERAIRE_PR_DB), conditionair, conditionhumidite));
        }
        ListeItineraire dbCT = listeItineraireDAO.getByNKeys(conditiontemp, conditiontemperature);
        if (dbCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_LISTEITINERAIRE_CT_DB), conditiontemp, conditiontemperature));
        }

        ListeItineraire dbVV = listeItineraireDAO.getByNKeys(vitesse, vitessevent);
        if (dbVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_LISTEITINERAIRE_VV_DB), vitesse, vitessevent));
        }
      /*  ListeItineraire dbNA = listeItineraireDAO.getByNKeys(niveau, niveauatteint);
        if (dbNA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_ITK,
                    ProcessRecordProEtudie.MSG_ERROR_LISTEITINERAIRE_NA_DB), niveau, niveauatteint));
        }*/

        DatatypeVariableUnitePRO dbdvquantite = dataTypeVariableUnitePRODAO.getSemisKey(datatype, quantite);
        if (dbdvquantite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_QUANTITE_NOT_FOUND_DVU_DB), cdatatype, quantite));
        }

        DatatypeVariableUnitePRO dbdvapport = dataTypeVariableUnitePRODAO.getSemisKey(datatype, proapport);
        if (dbdvapport == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_APPORT_NOT_FOUND_DVU_DB), cdatatype, proapport));
        }

        DatatypeVariableUnitePRO dbdvuLP = dataTypeVariableUnitePRODAO.getSemisKey(datatype, localisation);
        if (dbdvuLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, localisation));
        }

        DatatypeVariableUnitePRO dbdvCA = dataTypeVariableUnitePRODAO.getSemisKey(datatype, conditionair);
        if (dbdvCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_CA_NOT_FOUND_DVU_DB), cdatatype, conditionair));

        }
        DatatypeVariableUnitePRO dbdvuCT = dataTypeVariableUnitePRODAO.getSemisKey(datatype, conditiontemp);
        if (dbdvuCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_CONDITIONT_NOT_FOUND_DVU_DB), cdatatype, conditiontemp));

        }
        DatatypeVariableUnitePRO dbdvuVV = dataTypeVariableUnitePRODAO.getSemisKey(datatype, vitesse);
        if (dbdvuVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_PROETUDIE,
                    ProcessRecordProEtudie.MSG_ERROR_VENTV_NOT_FOUND_DVU_DB), cdatatype, vitesse));

        }
      /*  DatatypeVariableUnitePRO dbdvumNA = dataTypeVariableUnitePRODAO.getSemisKey(datatype, niveau);
        if (dbdvumNA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordProEtudie.BUNDLE_PATH_ITK,
                    ProcessRecordProEtudie.MSG_ERROR_NIVEAU_NOT_FOUND_DVU_DB), cdatatype, niveau));

        }*/

        ValeurProEtudie valeurProEtudieqa = new ValeurProEtudie(mesureProEtudie, dbdvquantite, quantiteapport);
        valeurProEtudieqa.setDatatypeVariableUnitePRO(dbdvquantite);
        valeurProEtudieqa.setValeur(quantiteapport);
        valeurProEtudieqa.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudieqa);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }
        
        ValeurProEtudie valeurProEtudiepa = new ValeurProEtudie(mesureProEtudie, dbdvapport, profondeurapport);
        valeurProEtudiepa.setDatatypeVariableUnitePRO(dbdvapport);
        valeurProEtudiepa.setValeur(profondeurapport);
        valeurProEtudiepa.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudiepa);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }
        
        ValeurProEtudie valeurProEtudielp = new ValeurProEtudie(dbdvuLP, mesureProEtudie, dbLP);
        valeurProEtudielp.setDatatypeVariableUnitePRO(dbdvuLP);
        valeurProEtudielp.setListeItineraire(dbLP);
        valeurProEtudielp.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudielp);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }
        
        ValeurProEtudie valeurProEtudieca = new ValeurProEtudie(dbdvCA, mesureProEtudie, dbCA);
        valeurProEtudieca.setDatatypeVariableUnitePRO(dbdvCA);
        valeurProEtudieca.setListeItineraire(dbCA);
        valeurProEtudieca.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudieca);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }
        
        ValeurProEtudie valeurProEtudiect = new ValeurProEtudie(dbdvuCT, mesureProEtudie, dbCT);
        valeurProEtudiect.setDatatypeVariableUnitePRO(dbdvuCT);
        valeurProEtudiect.setListeItineraire(dbCT);
        valeurProEtudiect.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudiect);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }
        
        ValeurProEtudie valeurProEtudievv = new ValeurProEtudie(dbdvuVV, mesureProEtudie, dbVV);
        valeurProEtudievv.setDatatypeVariableUnitePRO(dbdvuVV);
        valeurProEtudievv.setListeItineraire(dbVV);
        valeurProEtudievv.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudievv);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }
       /* ValeurProEtudie valeurProEtudiena = new ValeurProEtudie(dbdvumNA, mesureProEtudie, dbNA);
        valeurProEtudiena.setDatatypeVariableUnitePRO(dbdvumNA);
        valeurProEtudiena.setListeItineraire(dbNA);
        valeurProEtudiena.setMesureinterventionproetudie(mesureProEtudie);
        mesureProEtudie.getValeurproetudie().add(valeurProEtudiena);
        if(!errorsReport.hasErrors()){
        mesureProEtudieDAO.saveOrUpdate(mesureProEtudie);
        }*/
    }

    private MesureProEtudie getOrCreate(final Date datedebut, final String codedisp, final String codeTrait, final String codeParcelle, final String nomplacette, final String typepro, final String codepro, DescriptionTraitement dbTraitement, final Date datefin, final String codebbch, final String culture, final String observationqualite, final String nomobservation, final String precisionstade, final String materielapport, final String niveauatteint, final Date dateenfouissement, VersionFile versionFile, final String commentaire) throws PersistenceException {
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");
        dateString = sdfr.format(datedebut);
        String key = dateString + "_" + codedisp+ "_" + codeTrait+ "_" +codeParcelle+ "_" + nomplacette+ "_" + typepro+ "_" +codepro;
        MesureProEtudie mesureProEtudie = mesureProEtudieDAO.getByKeys(key);
        if (mesureProEtudie == null) {
            
            mesureProEtudie = new MesureProEtudie(codedisp, dbTraitement, codeParcelle, nomplacette, typepro, codepro, datedebut, datefin, codebbch,
                    culture, observationqualite, nomobservation, precisionstade, materielapport,niveauatteint, dateenfouissement, versionFile, commentaire);
            mesureProEtudie.setCodedispositif(codedisp);
            mesureProEtudie.setDescriptionTraitement(dbTraitement);
            mesureProEtudie.setNomparcelle(codeParcelle);
            mesureProEtudie.setNomplacette(nomplacette);
            mesureProEtudie.setDatedebut(datedebut);
            mesureProEtudie.setDatefin(datefin);
            mesureProEtudie.setCodebbch(codebbch);
            mesureProEtudie.setNomculture(culture);
            mesureProEtudie.setCodeproduit(codepro);
            mesureProEtudie.setTypeproduit(typepro);
            mesureProEtudie.setDateenfouissement(dateenfouissement);
            mesureProEtudie.setMaterielapport(materielapport);
            mesureProEtudie.setPrecisionstade(precisionstade);
            mesureProEtudie.setCommentaire(commentaire);
            mesureProEtudie.setTypeobservation(observationqualite);
            mesureProEtudie.setNomobservation(nomobservation);
            mesureProEtudie.setNiveauatteint(niveauatteint);
            mesureProEtudie.setKeymesure(key);
        }
        return mesureProEtudie;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }
    
    

}
