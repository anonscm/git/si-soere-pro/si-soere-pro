/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typeculture;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


public class Recorder extends AbstractCSVMetadataRecorder<TypeCulture> {
	
	protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ITypeCultureDAO typeCultureDAO;
    
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String libelle = tokenizerValues.nextToken();

                typeCultureDAO.remove(typeCultureDAO.getByLibelle(libelle));
                
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<TypeCulture> getAllElements() throws PersistenceException {
        return typeCultureDAO.getAll(TypeCulture.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeCulture typeCulture) throws PersistenceException 
    {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesNameLibelle = localizationManager.newProperties(TypeCulture.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_TPC, Locale.ENGLISH);

        String localizedChampNameLibelle = "";

        if (typeCulture != null) 
        {
            localizedChampNameLibelle = propertiesNameLibelle.containsKey(typeCulture.getLibelle()) ? propertiesNameLibelle.getProperty(typeCulture.getLibelle()) : typeCulture.getLibelle();
        }
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeCulture == null ? Constantes.STRING_EMPTY : typeCulture.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeCulture == null ? Constantes.STRING_EMPTY : localizedChampNameLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
    	ErrorsReport errorsReport = new ErrorsReport();
    	long line = 0;
    	
        try 
        {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) 
            {
            	line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, TypeCulture.TABLE_NAME);
                
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                String code = Utils.createCodeFromString(libelle);
                
                if(libelle == null || libelle.isEmpty())
                {
                	errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"),line+1, indexLibelle-1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                TypeCulture typeCulture = new TypeCulture(libelle, code);
                TypeCulture dbTypeCulture = typeCultureDAO.getByCode(code);
                
                if(!errorsReport.hasErrors())
                {
                	if (dbTypeCulture == null) 
                	{
                		typeCultureDAO.saveOrUpdate(typeCulture);
                	} 
                	else 
                	{
                		dbTypeCulture.setLibelle(libelle);
                		dbTypeCulture.setCode(code);
                		typeCultureDAO.saveOrUpdate(dbTypeCulture);
                	}
                }
                
                values = parser.getLine();
            }
            
            RefDataUtil.gestionErreurs(errorsReport);
        } 
        catch (IOException e1) 
        {
            throw new BusinessException(e1.getMessage(), e1);
        } 
        catch (PersistenceException e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

	/**
	 * @param typeCultureDAO the typeCultureDAO to set
	 */
	public void setTypeCultureDAO(ITypeCultureDAO typeCultureDAO) {
		this.typeCultureDAO = typeCultureDAO;
	}
    
}
