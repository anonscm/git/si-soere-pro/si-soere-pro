/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dispositifthemedatatypeparcelle;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.dispositiftheme.DispositifTheme;
import org.inra.ecoinfo.pro.refdata.dispositiftheme.IDispositifThemeDAO;
import org.inra.ecoinfo.pro.refdata.dispthemedatatype.DispositifThemeDataType;
import org.inra.ecoinfo.pro.refdata.dispthemedatatype.IDispositifThemeDataTypeDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.node.ILeafTreeNode;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */

public class Recorder extends AbstractCSVMetadataRecorder<DispositifThemeDatatypeParcelle>{
    
    IDispositifThemeDAO dispositifThemeDAO;
    IDispositifDAO dispositifDAO;
    IThemeDAO themeDAO;
    IDatatypeDAO datatypeDAO;
    IDispositifThemeDataTypeDAO dtdtDAO;
    IDispositifThemeDatatypeParcelleDAO dispositifThemeDatatypeParcelleDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
  
   
    
    final Map<String, String[]>                   listeDispoPossibles                              = new ConcurrentHashMap();
    final Map<String, String[]>                   listeThemePossibles                              = new ConcurrentHashMap();
    final Map<String, String[]>                   listeDatatypePossibles                           = new ConcurrentHashMap();
    final Map<String, String[]>                   parcellesPossibles                               = new ConcurrentHashMap();
    
    

    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    static final String                           PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB    = "PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB";
    static final String                           PROPERTY_MSG_ERROR_CODE_DISP_NOT_FOUND_IN_DB     = "PROPERTY_MSG_ERROR_CODE_DISP_NOT_FOUND_IN_DB";
    static final String                           PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE        = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";
    private static final String                   PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB";
    static final String                           PROPERTY_MSG_BAD_DISPOSITIF_THEME_DATATYPE       = "PROPERTY_MSG_BAD_DISPOSITIF_THEME_DATATYPE";
    static final String                           PROPERTY_MSG_DISP_THEME_DATA_MISSING_IN_DATABASE = "PROPERTY_MSG_DISP_THEME_DATA_MISSING_IN_DATABASE";
    static final String                           BUNDLE_NAME_DATASET                              = "org.inra.ecoinfo.pro.dataset.messages";
    static final Logger                                        LOGGER                                           = Logger.getLogger(Recorder.class);

    
    @Override
    public void deleteRecord(CSVParser parser, File file, String string) throws BusinessException {
         try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeDispositif = tokenizerValues.nextToken();
                final String codeTheme = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codeDatatype = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String codePE = Utils.createCodeFromString(tokenizerValues.nextToken());
                this.dispositifThemeDatatypeParcelleDAO
                       .remove(this.dispositifThemeDatatypeParcelleDAO
                        .getDispThemeDatatypeParcelleCode(codeDispositif, codeTheme, codeDatatype, codePE));
                
           
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e);
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
 
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try 
        {
            this.skipHeader(parser);

            String[] values = null;
            while ((values =parser.getLine()) != null) 
            {
            	//line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                int indexDisp = tokenizerValues.currentTokenIndex();
                final String codeDispositif = tokenizerValues.nextToken();
                int indextheme= tokenizerValues.currentTokenIndex();
                final String codeTheme  = tokenizerValues.nextToken();
                 int indexdata = tokenizerValues.currentTokenIndex();
                final String codeDatatype = tokenizerValues.nextToken();
                final String codePE = tokenizerValues.nextToken();
                

                DispositifThemeDataType dispositifThemeDatatype;
                
                try{
                    
                    dispositifThemeDatatype= this.retrieveOrCreateDispositifThemeDataType( errorsReport,  codeDispositif,  codeTheme, codeDatatype);
                
                }catch (final PersistenceException e) {
                    continue;
                }
                
                this.persistDispositifThemeDatatypeParcelle(dispositifThemeDatatype, codePE);

            }
               
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e);
            throw new BusinessException(e.getMessage(), e);
        }
    }



    ParcelleElementaire verificationParcelle(final String codePE, Dispositif dispositif, ErrorsReport errorsReport) throws PersistenceException {
        ParcelleElementaire dbparcelleElementaire = parcelleElementaireDAO.getByCodeDispositif(codePE, dispositif);
        
        if(dbparcelleElementaire == null){
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB), codePE));
        }
        return dbparcelleElementaire;
    }

   DataType verificationDatatype(final String codeDatatype, ErrorsReport errorsReport) throws PersistenceException {
        DataType dbdatatype = datatypeDAO.getByCode(Utils.createCodeFromString(codeDatatype));
        if(dbdatatype==null){
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE), codeDatatype));
        }
        return dbdatatype;
    }

    Theme verificationTheme(final String codeTheme, ErrorsReport errorsReport) throws PersistenceException {
        Theme dbtheme = themeDAO.getByCode(Utils.createCodeFromString(codeTheme));
        if (dbtheme == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB),  codeTheme));
        }
        return dbtheme;
    }

    Dispositif verifcationDispositif(final String codeDispositif, ErrorsReport errorsReport) throws NoResultException, NonUniqueResultException {
        Dispositif dbdispositif = dispositifDAO.getByCode(codeDispositif);
        if (dbdispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_CODE_DISP_NOT_FOUND_IN_DB) ,codeDispositif));
        }
    return dbdispositif;
    }
    

    @Override
    protected List<DispositifThemeDatatypeParcelle> getAllElements() throws PersistenceException {
       List<ILeafTreeNode> nodes = this.dispositifThemeDatatypeParcelleDAO.getAll(DispositifThemeDatatypeParcelle.class);
       List<DispositifThemeDatatypeParcelle> dispositifThemeDatatypeParcelles = new LinkedList();
        for (ILeafTreeNode node : nodes) {
            dispositifThemeDatatypeParcelles.add((DispositifThemeDatatypeParcelle) node);
        }
       // Collections.sort(dispositifThemeDatatypeParcelles);
        return dispositifThemeDatatypeParcelles;
    }
 

       private void DispositifPossibles() throws PersistenceException {
        List<Dispositif> groupedispo = dispositifDAO.getAll(Dispositif.class);
        String[] ListeDispositif = new String[groupedispo.size() + 1];
        ListeDispositif[0] = "";
        int index = 1;
        for (Dispositif dispo : groupedispo) {
            ListeDispositif[index++] = dispo.getCode();
        }
        this.listeDispoPossibles.put(ColumnModelGridMetadata.NULL_KEY,ListeDispositif);
    }
      
     private void ThemePossibles() throws PersistenceException {
        List<Theme> groupetheme = themeDAO.getAll(Theme.class);
        String[] ListeTheme = new String[groupetheme.size() + 1];
        ListeTheme[0] = "";
        int index = 1;
        for (Theme theme : groupetheme) {
            ListeTheme[index++] = theme.getName();
        }
        this.listeThemePossibles.put(ColumnModelGridMetadata.NULL_KEY,ListeTheme);
    } 
    
    private void dataTypePossibles() throws PersistenceException {
        List<DataType> groupedt = datatypeDAO.getAll(DataType.class);
        String[] Listedatatype = new String[groupedt.size() + 1];
        Listedatatype[0] = "";
        int index = 1;
        for (DataType datatype : groupedt) {
            Listedatatype[index++] = datatype.getCode();
        }
        this.listeDatatypePossibles.put(ColumnModelGridMetadata.NULL_KEY,Listedatatype);
    }
    
    void updateNamesParcellesPossibles() throws PersistenceException {
        final List<Dispositif> localDisp = this.dispositifDAO.getAll(Dispositif.class);
        
        this.parcellesPossibles.clear();
        for (final Dispositif dispositif: localDisp) {
            final List<ParcelleElementaire> parcelles = this.parcelleElementaireDAO.getByDispositif(dispositif);
            final String[] namesParcellesPossibles = new String[parcelles.size() + 1];
            if (!parcelles.isEmpty()) {
                int index = 0;
                namesParcellesPossibles[index++] = org.apache.commons.lang.StringUtils.EMPTY;
                for (final ParcelleElementaire parcelle : parcelles) {
                    namesParcellesPossibles[index++] = parcelle.getNom();
                }
                this.parcellesPossibles.put(dispositif.getCode(), namesParcellesPossibles);
            }
            this.parcellesPossibles.put(dispositif.getCode(), namesParcellesPossibles);
        }
      
    }
    
   
    
    DispositifTheme retrieveOrCreateDispositifTheme(
            final ErrorsReport errorsReport, final String codeDispositif, final String codeTheme)
                    throws PersistenceException {
        try {
         

            Theme dbtheme = themeDAO.getByCode(Utils.createCodeFromString(codeTheme));
            if (dbtheme == null) {
                     errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB),  codeTheme));
            }
            
            Dispositif dbdispositif = dispositifDAO.getByCode(codeDispositif);
            if (dbdispositif == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_CODE_DISP_NOT_FOUND_IN_DB) ,codeDispositif));
            }
            
            DispositifTheme dispositifTheme = dispositifThemeDAO.getByNames(dbdispositif, dbtheme);
      
            if (dispositifTheme == null) {
                dispositifTheme = new DispositifTheme();
                dispositifTheme.setTheme(dbtheme);
                dispositifTheme.setDispositif(dbdispositif);
            }
            
            this.dispositifThemeDAO.saveOrUpdate(dispositifTheme);
            
            return dispositifTheme;
            
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB), codeTheme, codeDispositif));
            throw new PersistenceException();
        }
    }
    
    
    
     DispositifThemeDataType retrieveOrCreateDispositifThemeDataType(
            final ErrorsReport errorsReport, final String pathSite, final String themeCode,
            final String datatypeCode) throws PersistenceException {
        try {
            final DispositifTheme dispositifTheme = this.retrieveOrCreateDispositifTheme(errorsReport, pathSite, themeCode);
            
            final DataType datatype = this.verificationDatatype(datatypeCode, errorsReport);
       
            DispositifThemeDataType dispositifThemeDataType = dtdtDAO.getByName(dispositifTheme, datatype);
            if (dispositifThemeDataType == null) {
                dispositifThemeDataType = new DispositifThemeDataType();
                dispositifThemeDataType.setDispositiftheme(dispositifTheme);
                dispositifThemeDataType.setDatatype(datatype);
            }
            this.dispositifThemeDAO.saveOrUpdate(dispositifTheme);
            
            return dispositifThemeDataType;
            
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
             errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_DISP_THEME_DATA_MISSING_IN_DATABASE),  themeCode,datatypeCode));
          
            throw new PersistenceException();
        }
    }
    
    
     void persistDispositifThemeDatatypeParcelle(
            final DispositifThemeDataType dispositifThemeDataType,
            final String parcelleCode) throws PersistenceException {
        
        try {
            ParcelleElementaire parcelle = null;
            ErrorsReport errorsReport =null;
            
            if (!Strings.isNullOrEmpty(parcelleCode)) {
                parcelle = this.verificationParcelle( parcelleCode, dispositifThemeDataType.getDispositiftheme().getDispositif(), errorsReport);
                if (parcelle == null) {
                    throw new PersistenceException();
                }
            }
            DispositifThemeDatatypeParcelle dispositifThemeDatatypeParcelle = this.dispositifThemeDatatypeParcelleDAO
                    .getDispThemeDatatypeParcelleCode(dispositifThemeDataType.getDispositiftheme().getDispositif().getCode(),
                            dispositifThemeDataType.getDispositiftheme().getTheme().getCode(),
                            dispositifThemeDataType.getDatatype().getCode(),
                            parcelleCode);
            
            if (dispositifThemeDatatypeParcelle == null) {
                dispositifThemeDatatypeParcelle = new DispositifThemeDatatypeParcelle(
                        dispositifThemeDataType, parcelle);
            }
            dispositifThemeDataType.getDispositifThemeDatatypeParcelles().add(
                    dispositifThemeDatatypeParcelle);
            dispositifThemeDatatypeParcelle.setPath(dispositifThemeDatatypeParcelle.buildPath());
            dispositifThemeDataType.getPath();
            this.dispositifThemeDatatypeParcelleDAO.saveOrUpdate(dispositifThemeDatatypeParcelle);
            
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
           
            throw new PersistenceException();
        }
    }
    
    
    
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DispositifThemeDatatypeParcelle dispthemedatatypeparcelle) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        String valeurDispositif = dispthemedatatypeparcelle == null ? 
                AbstractCSVMetadataRecorder.EMPTY_STRING : 
                    dispthemedatatypeparcelle.getDispositifThemeDataType().getDispositiftheme().getDispositif().getCode() != null ?
                               dispthemedatatypeparcelle.getDispositifThemeDataType().getDispositiftheme().getDispositif().getCode():""; 
                                      ColumnModelGridMetadata columnDispositif= new ColumnModelGridMetadata(valeurDispositif,
                                              listeDispoPossibles, null, true, false, true);
       
        String valeurTheme = dispthemedatatypeparcelle == null ? 
                AbstractCSVMetadataRecorder.EMPTY_STRING : 
                    dispthemedatatypeparcelle.getDispositifThemeDataType().getDispositiftheme().getTheme().getName() != null ?
                               dispthemedatatypeparcelle.getDispositifThemeDataType().getDispositiftheme().getTheme().getName() :""; 
                                      ColumnModelGridMetadata columnTheme= new ColumnModelGridMetadata(valeurTheme,
                                              listeThemePossibles, null, true, false, true);
 
       
        String valeurDatatype = dispthemedatatypeparcelle == null ? 
                AbstractCSVMetadataRecorder.EMPTY_STRING : 
                    dispthemedatatypeparcelle.getDispositifThemeDataType().getDatatype().getName() != null ?
                               dispthemedatatypeparcelle.getDispositifThemeDataType().getDatatype().getName() :""; 
                                      ColumnModelGridMetadata columnDatatype= new ColumnModelGridMetadata(valeurDatatype,
                                              listeDatatypePossibles, null, true, false, true);
                                      
         final ColumnModelGridMetadata parcelleColumn = new ColumnModelGridMetadata(
                dispthemedatatypeparcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : dispthemedatatypeparcelle.getParcelleElementaire()== null ? org.apache.commons.lang.StringUtils.EMPTY
                                : dispthemedatatypeparcelle.getParcelleElementaire().getNom(),
                                this.parcellesPossibles, null, true, false);                              
                                              
       List<ColumnModelGridMetadata> refsDispositif = new LinkedList<ColumnModelGridMetadata>();
       refsDispositif.add(parcelleColumn);
       columnDispositif.setRefs(refsDispositif);
       parcelleColumn
        .setValue(dispthemedatatypeparcelle == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : dispthemedatatypeparcelle.getParcelleElementaire() == null ? org.apache.commons.lang.StringUtils.EMPTY
                        : dispthemedatatypeparcelle.getParcelleElementaire().getNom());
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispositif);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTheme);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDatatype);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(parcelleColumn);
       
        return lineModelGridMetadata;

    }
     
    
    @Override
    protected ModelGridMetadata<DispositifThemeDatatypeParcelle> initModelGridMetadata() {
        try {
        
        DispositifPossibles();
        ThemePossibles() ;
        dataTypePossibles();
        updateNamesParcellesPossibles();
            
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
        }
        return super.initModelGridMetadata();
    }
     
    
    
    public IDispositifThemeDAO getDispositifThemeDAO() {
        return dispositifThemeDAO;
    }

    public void setDispositifThemeDAO(IDispositifThemeDAO dispositifThemeDAO) {
        this.dispositifThemeDAO = dispositifThemeDAO;
    }

    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    public IThemeDAO getThemeDAO() {
        return themeDAO;
    }

    public void setThemeDAO(IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    public IDatatypeDAO getDatatypeDAO() {
        return datatypeDAO;
    }

    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    public IDispositifThemeDataTypeDAO getDtdtDAO() {
        return dtdtDAO;
    }

    public void setDtdtDAO(IDispositifThemeDataTypeDAO dtdtDAO) {
        this.dtdtDAO = dtdtDAO;
    }

     
    public IDispositifThemeDatatypeParcelleDAO getDispositifThemeDatatypeParcelleDAO() {
        return dispositifThemeDatatypeParcelleDAO;
    }

    public void setDispositifThemeDatatypeParcelleDAO(IDispositifThemeDatatypeParcelleDAO dispositifThemeDatatypeParcelleDAO) {
        this.dispositifThemeDatatypeParcelleDAO = dispositifThemeDatatypeParcelleDAO;
    }

    public IParcelleElementaireDAO getParcelleElementaireDAO() {
        return parcelleElementaireDAO;
    }

    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

         
}
