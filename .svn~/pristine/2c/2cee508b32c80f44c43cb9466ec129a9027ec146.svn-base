/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.recoltecoupe.impl;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.extraction.listeitineraires.impl.ITKExtractor;

import org.inra.ecoinfo.pro.extraction.listeitineraires.impl.ITKParameterVO;
import org.inra.ecoinfo.pro.extraction.recoltecoupe.interfaces.IRecolteCoupeDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.vo.DatesForm1ParamVO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class RecolteCoupeExtractor extends AbstractExtractor{
    
    
    public static final String    RECOLTE_COUPE                     = "recolte_coupe";

  
    protected static final String MAP_INDEX_RECOLTE_COUPE              = "recoltecoupe";

   
    protected static final String MAP_INDEX_0                       = "0";

    
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";
    
    IRecolteCoupeDAO recolteCoupeDAO;

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        final Map<String, List> filteredResultsDatasMap = this
                .filterExtractedDatas(resultsDatasMap);
        if (((ITKParameterVO) parameters).getResults().get(
                ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE) == null
                || ((ITKParameterVO) parameters).getResults()
                .get(ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE).isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(
                    ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE, filteredResultsDatasMap);
        } else {
            ((ITKParameterVO) parameters)
            .getResults()
            .get(ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE)
            .put(RecolteCoupeExtractor.RECOLTE_COUPE,
                    filteredResultsDatasMap.get(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE));
        }
        ((ITKParameterVO) parameters)
        .getResults()
        .get(ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE)
        .put(RecolteCoupeExtractor.MAP_INDEX_0,
                filteredResultsDatasMap.get(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE));
    }

    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
         final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final DatesForm1ParamVO datesForm1ParamVO = (DatesForm1ParamVO) requestMetadatasMap
                    .get(DatesForm1ParamVO.class.getSimpleName());
            final List<Dispositif> selectedDispositifs= (List<Dispositif>) requestMetadatasMap
                    .get(Dispositif.class.getSimpleName());
            final String dateStart = datesForm1ParamVO.getDateStart();
            final String dateEnd = datesForm1ParamVO.getDateEnd();
            final Date dateDebut = DateUtil.getSimpleDateFormatDateLocale().parse(dateStart);
            final Date dateFin = DateUtil.getSimpleDateFormatDateLocale().parse(dateEnd);
            final List<MesureRecolteCoupe> mesuresFluxChambre = this.recolteCoupeDAO
                    .extractRecolteCoupe(selectedDispositifs, dateDebut, dateFin);
            if (mesuresFluxChambre == null || mesuresFluxChambre.isEmpty()) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        RecolteCoupeExtractor.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
            extractedDatasMap.put(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE, mesuresFluxChambre);
        } catch (final ParseException | BusinessException | PersistenceException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    @Override
    protected Map<String, List> filterExtractedDatas(Map<String, List> resultsDatasMap) throws BusinessException {
        if (resultsDatasMap.get(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE) != null
                && !resultsDatasMap.get(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE).isEmpty()) {
            this.filterExtractedDatas(resultsDatasMap, RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE);
        }
        return resultsDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }
    
     void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort((List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(ITKParameterVO.RECOLTECOUPE)), new Comparator<VariablesPRO>() {

                    @Override
                    public int compare(final VariablesPRO o1, final VariablesPRO o2) {
                        return o1.getId().toString().compareTo(o2.getId().toString());
                    }
                });

    }
     
      @Override
    public long getExtractionSize(IParameter parameters) {
        try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final DatesForm1ParamVO datesForm1ParamVO = (DatesForm1ParamVO) requestMetadatasMap
                    .get(DatesForm1ParamVO.class.getSimpleName());
            final List<Dispositif> selectedDisp = (List<Dispositif>) requestMetadatasMap
                    .get(Dispositif.class.getSimpleName());
            final String dateStart = datesForm1ParamVO.getDateStart();
            final String dateEnd = datesForm1ParamVO.getDateEnd();
            final Date dateDebut = RecorderPRO.getSimpleDateTimeFormatTimeLocale().parse(
                    dateStart + "00:00:00");
            final Date dateFin = RecorderPRO.getSimpleDateTimeFormatTimeLocale().parse(
                    dateEnd + "23:30:00");
            return recolteCoupeDAO.sizeRecolteCoupe(selectedDisp, dateDebut, dateFin);
        } catch (ParseException | PersistenceException ex) {
            return -1l;
        } 
    }

    public void setRecolteCoupeDAO(IRecolteCoupeDAO recolteCoupeDAO) {
        this.recolteCoupeDAO = recolteCoupeDAO;
    }
    
    
    
}
