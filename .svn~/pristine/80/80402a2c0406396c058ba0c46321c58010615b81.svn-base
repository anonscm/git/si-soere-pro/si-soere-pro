/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typecaracteristiquemp;
import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.Recorder;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;
import org.inra.ecoinfo.pro.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author adiankha
 */
public class RecorderTCMPTest {
    
    public RecorderTCMPTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
     MockUtils m = MockUtils.getInstance();
    Recorder instance;
    String encoding = "UTF-8";

    @Mock
    Properties propertiesNom;
    @Mock
    TypeCaracteristiques tc1;
    @Mock
    TypeCaracteristiques tc2;
    
    @Before
    public void setUp() {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setTypecaracteristiqueDAO(m.typecaracteristiqueDAO);
        instance.setLocalizationManager(m.localizationManager);
        Mockito.when(m.localizationManager.newProperties(TypeCaracteristiques.JPA_ENTITY_NAME, TypeCaracteristiques.JPA_COLUMN_NAME, Locale.ENGLISH)).thenReturn(propertiesNom);
        Mockito.when(m.origines.getOrigine_nom()).thenReturn(MockUtils.TYPECARACTMP);
        Mockito.when(propertiesNom.getProperty(MockUtils.TYPECARACTMP)).thenReturn("TCNomEN");
    }
     @Test
    public void testGetTypeCatacteristiqueDAO() {
        System.out.println("setOrigineDAO");
        instance = Mockito.spy(new Recorder());
        Assert.assertNull("typecaracteristiqueDao not null", instance.getTypecaracteristiqueDAO());
        instance.setTypecaracteristiqueDAO(m.typecaracteristiqueDAO);
        Assert.assertEquals("typecaracteristiqueDao not setted", m.typecaracteristiqueDAO, instance.getTypecaracteristiqueDAO());
    }
     @Test
    public void testInitModelGridMetadata() throws PersistenceException {
        System.out.println("initModelGridMetadata");
        m.localizationManager = Mockito.spy(m.localizationManager);
        instance.setLocalizationManager(m.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.localizationManager).newProperties(TypeCaracteristiques.JPA_ENTITY_NAME, TypeCaracteristiques.JPA_COLUMN_NAME, Locale.ENGLISH);
        Assert.assertEquals("propertiesNom not initialized", propertiesNom, instance.getTCNomEN());
    }
     @Test
    public void testGetNewLineModelGridMetadata() throws Exception {
        System.out.println("getNewLineModelGridMetadata");
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.typecaracteristiques);
        Assert.assertTrue(2 == result.getColumnsModelGridMetadatas().size());
      //  Assert.assertEquals("typecaracteristiques", result.getColumnModelGridMetadataAt(0).getValue());
       // Assert.assertEquals("TCNomEN", result.getColumnModelGridMetadataAt(1).getValue());
    }
    
     @Test
    public void testGetAllElements() throws Exception {
        System.out.println("getAllElements");
        List<TypeCaracteristiques> tc = Arrays.asList(new TypeCaracteristiques[]{tc1, tc2});
        Mockito.when(m.typecaracteristiqueDAO.getAll()).thenReturn(tc);
        List<TypeCaracteristiques> tcDb = instance.getAllElements();
        Assert.assertEquals(tc, tcDb);
        Assert.assertEquals(tc.get(0), tc1);
        Assert.assertEquals(tc.get(1), tc2);
        
    }
     @Test
    public void testDeleteRecorder() throws Exception {
        System.out.println("deleteRecord");
        String text = "TypeCaracteristique;tc_nom_fr;tc_nom_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        Mockito.when(m.typecaracteristiqueDAO.FindbyName("typecaracteristique")).thenReturn(m.typecaracteristiques);
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.typecaracteristiqueDAO).remove(m.typecaracteristiques);

        // Persistence exception on getByCode
        text = "TypeCaracteristique;tc_nom_fr;tc_nom_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(m.typecaracteristiqueDAO.FindbyName("typecaracteristique")).thenThrow(new PersistenceException("error"));
        BusinessException error = null;
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error", error.getMessage());
        error = null;

        // Persistence exception on remove
        text = "TypeCaracteristique;tc_nom_fr;tc_nom_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doReturn(m.typecaracteristiques).when(m.typecaracteristiqueDAO).FindbyName("typecaracteristique");
        Mockito.when(m.typecaracteristiqueDAO.FindbyName("typecaracteristique")).thenReturn(m.typecaracteristiques);
        Mockito.doThrow(new PersistenceException("error2")).when(m.typecaracteristiqueDAO).remove(m.typecaracteristiques);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }
     @Test
    public void testProcessRecord() throws Exception {
        System.out.println("processRecord");
        TypeCaracteristiques typecaracteristiques = Mockito.mock(TypeCaracteristiques.class);
        Mockito.when(typecaracteristiques.getTcar_code()).thenReturn("typecaracteristiques");
        Mockito.when(typecaracteristiques.getTcar_nom()).thenReturn("TypeCaracteristiques");
        String text = "tc_nom_fr;ct_nom_en\n" + "TypeCaracteristiques;ct_nom_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        ArgumentCaptor<TypeCaracteristiques> ts = ArgumentCaptor.forClass(TypeCaracteristiques.class);

        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.typecaracteristiqueDAO).saveOrUpdate(ts.capture());
      //  Mockito.verify(m.typecaracteristiques).setTcar_nom("typecaracteristiques");

        // existing origine
        Mockito.when(m.typecaracteristiqueDAO.FindbyName("typecaracteristiques")).thenReturn(null);
        ts = ArgumentCaptor.forClass(TypeCaracteristiques.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.typecaracteristiqueDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
         Assert.assertNotEquals(m.typecaracteristiques,ts.getValue());
         Assert.assertEquals("typecaracteristiques", ts.getValue().getTcar_code());
          Assert.assertEquals("TypeCaracteristiques", ts.getValue().getTcar_nom());
          
    }

   
}
