/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.filiationechantillonproduit;

import com.Ostermiller.util.CSVParser;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.IEchantillonsProduitDAO;

import org.inra.ecoinfo.pro.refdata.prelevementproduit.IPrelevementProduitDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 * Vivianne
 */
public class Recorder extends AbstractCSVMetadataRecorder<FiliationEchantillonProduit>{
    
    IFiliationEchantillonProduitDAO filiationproduitDAO;
    IEchantillonsProduitDAO echantillonsproduitDAO;
    IPrelevementProduitDAO prelevementProduitDAO;
    
    private static final String PROPERTY_MSG_FEP_BAD_ECHANPRO = "PROPERTY_MSG_FEP_BAD_ECHANPRO ";
     private static final String PROPERTY_MSG_NON_PARENT = "PROPERTY_MSG_NON_PARENT";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_FILIATIONPRODUIT_BAD_NUMBER_ECHANTILLON="PROPERTY_MSG_FILIATIONPRODUIT_BAD_NUMBER_ECHANTILLON";
    
    //private String[] listeFiliationProduitPossibles;
	private String[] listeCodeEchanPossible;
    
    private void createFiliationProduit(FiliationEchantillonProduit fep) throws PersistenceException{
        filiationproduitDAO.saveOrUpdate(fep);
        filiationproduitDAO.flush();
    }
    
    private void updateFiliationProduit(long numeroechantillon,EchantillonsProduit echantillonProduit,FiliationEchantillonProduit dbfep) throws PersistenceException{
        dbfep.setNumeroechantillon(numeroechantillon);
        dbfep.setEchantillonsProduit(echantillonProduit);
        filiationproduitDAO.saveOrUpdate(dbfep);
    }
    
    private void saveOrUpdateFiliationProduit(long numeroechantillon,EchantillonsProduit echantillonProduit,FiliationEchantillonProduit dbfep) throws PersistenceException{
    	
        if(dbfep==null){
        	
        FiliationEchantillonProduit filiationEchantillonProduit = new FiliationEchantillonProduit(numeroechantillon, echantillonProduit);
        filiationEchantillonProduit.setNumeroechantillon(numeroechantillon);
        filiationEchantillonProduit.setEchantillonsProduit(echantillonProduit);
        
            createFiliationProduit(filiationEchantillonProduit);
            
        } else{
        	
            updateFiliationProduit(numeroechantillon, echantillonProduit, dbfep);
        }
    }
    
    
    private void persistFiliationEchantillonProduit(long numeroechantillon,EchantillonsProduit echantillonProduit) throws PersistenceException{
    	
        FiliationEchantillonProduit fep = filiationproduitDAO.getByNames(numeroechantillon, echantillonProduit);
        saveOrUpdateFiliationProduit(numeroechantillon, echantillonProduit, fep);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
         try 
         {
             String[] values = parser.getLine();
             while (values != null) 
             {
                 TokenizerValues tokenizerValues = new TokenizerValues(values);
                 String code_ech = tokenizerValues.nextToken();
                 long numero = Long.parseLong(code_ech);
                 filiationproduitDAO.remove(filiationproduitDAO.getByDelete(numero));
                  values = parser.getLine();
             }
         } 
         catch (Exception e) 
         {
             throw new BusinessException(e.getMessage(), e);
         }
    }

    private void listeEchantillonsPossibles() throws PersistenceException {
        List<EchantillonsProduit> groupescode = echantillonsproduitDAO.getAll(EchantillonsProduit.class);
        String[] listecodePossibles = new String[groupescode.size() + 1];
        listecodePossibles[0] = "";
        int index = 1;
        for (EchantillonsProduit codeechantillon : groupescode) {
            listecodePossibles[index++] = codeechantillon.getCode_unique();
        }
        this.listeCodeEchanPossible = listecodePossibles;
    }
	
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            long line =0;
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, FiliationEchantillonProduit.NAME_ENTITY_JPA);
                int indexcodeechpro = tokenizerValues.currentTokenIndex();
                String codeechan = tokenizerValues.nextToken();
                long numero = Long.parseLong(codeechan);
                final String code_unique = tokenizerValues.nextToken();
                EchantillonsProduit dbnumech = echantillonsproduitDAO.numEchanExist(numero);
                if(dbnumech==null){	
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_FILIATIONPRODUIT_BAD_NUMBER_ECHANTILLON),line, numero));
                }
                EchantillonsProduit dbechantillonproduit = echantillonsproduitDAO.getByECH(code_unique);
                if (dbechantillonproduit == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_FEP_BAD_ECHANPRO),line, indexcodeechpro , code_unique));
                }
                if(dbechantillonproduit == dbnumech){
                     errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NON_PARENT), numero , code_unique));
                }
                if (!errorsReport.hasErrors()) {	
                persistFiliationEchantillonProduit(numero,dbechantillonproduit);	 
                }
                values = parser.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(FiliationEchantillonProduit filiationpro) throws PersistenceException {
    	final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(filiationpro == null || filiationpro.getNumeroechantillon()== 0 ? 
                        AbstractCSVMetadataRecorder.EMPTY_STRING : filiationpro.getNumeroechantillon(), 
                                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
       lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(filiationpro == null || filiationpro.getEchantillonsProduit().getCode_unique()==null? 
                		AbstractCSVMetadataRecorder.EMPTY_STRING :filiationpro.getEchantillonsProduit().getCode_unique()!=null?
                				filiationpro.getEchantillonsProduit().getCode_unique():"",
                                 listeCodeEchanPossible, null, true, false, true));  
        
        return lineModelGridMetadata;
    }

    
    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }
    
    
    @Override
    protected List<FiliationEchantillonProduit> getAllElements() throws PersistenceException {
       return  filiationproduitDAO.getAll();
    }

   
	public void setFiliationproduitDAO(
			IFiliationEchantillonProduitDAO filiationproduitDAO) {
		this.filiationproduitDAO = filiationproduitDAO;
	}

	public void setEchantillonsproduitDAO(
			IEchantillonsProduitDAO echantillonsproduitDAO) {
		this.echantillonsproduitDAO = echantillonsproduitDAO;
	}
        @Override
	protected ModelGridMetadata<FiliationEchantillonProduit> initModelGridMetadata() {
        try {
        	listeEchantillonsPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
       
        return super.initModelGridMetadata();
    }

    
}
