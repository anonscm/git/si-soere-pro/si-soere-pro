package org.inra.ecoinfo.pro.refdata.typesolarvalis;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


public class Recorder extends AbstractCSVMetadataRecorder<Typesolarvalis>{
    ITypeSolArvalisDAO tsarvalisDAO;
    private Properties tsarvalisEN;
    
    private void createTypesolarvalis(final Typesolarvalis typesolarvalis) throws PersistenceException {
        tsarvalisDAO.saveOrUpdate(typesolarvalis);
        tsarvalisDAO.flush();
    }
    
    private void updateTypesolarvalis(final String nom,final Typesolarvalis dbtypesolarvalis) throws PersistenceException {
        dbtypesolarvalis.setNom(nom);
        tsarvalisDAO.saveOrUpdate(dbtypesolarvalis);
    }

    private void createOrUpdateTypesolarvalis(final String code,String nom,final Typesolarvalis dbtypesolarvalis) throws PersistenceException {
        if (dbtypesolarvalis == null) {
            final Typesolarvalis typesol = new Typesolarvalis(code,nom);
        typesol.setCode(code);
        typesol.setNom(nom);
            createTypesolarvalis(typesol);
        } else {
            updateTypesolarvalis(nom, dbtypesolarvalis);
        }
    }
    private void persistTypesolarvalis( final String code, final String nom) throws PersistenceException, BusinessException {
        final Typesolarvalis dbtypesol = tsarvalisDAO.getByName(nom);
        createOrUpdateTypesolarvalis(code,nom, dbtypesol);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Typesolarvalis typesolarvalis) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typesolarvalis == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : typesolarvalis.getNom(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typesolarvalis == null || typesolarvalis.getNom() == null ? 
                              AbstractCSVMetadataRecorder.EMPTY_STRING : tsarvalisEN.getProperty(typesolarvalis.getNom()),
                                   ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
         final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values,Typesolarvalis.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final Typesolarvalis dbfr = tsarvalisDAO.getByName(nom);
                if (dbfr != null) {
                    tsarvalisDAO.remove(dbfr);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }

    @Override
    protected List<Typesolarvalis> getAllElements() throws PersistenceException {
        return tsarvalisDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typesolarvalis.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistTypesolarvalis( code, nom);
                values = parser.getLine();
            }   

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    @Override
    protected ModelGridMetadata<Typesolarvalis> initModelGridMetadata() {
        tsarvalisEN = localizationManager.newProperties(Typesolarvalis.NAME_ENTITY_JPA, Typesolarvalis.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
    
    public ITypeSolArvalisDAO getTsarvalisDAO() {
        return tsarvalisDAO;
    }

    
    public void setTsarvalisDAO(ITypeSolArvalisDAO tsarvalisDAO) {
        this.tsarvalisDAO = tsarvalisDAO;
    }

    
    public Properties getTsarvalisEN() {
        return tsarvalisEN;
    }

    
    public void setTsarvalisEN(Properties tsarvalisEN) {
        this.tsarvalisEN = tsarvalisEN;
    }
    

}
