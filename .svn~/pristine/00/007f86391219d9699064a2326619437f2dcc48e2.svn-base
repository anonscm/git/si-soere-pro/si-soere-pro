/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.entity;

import java.io.Serializable;
import java.util.Date;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;

/**
 *
 * @author vjkoyao
 */

@Entity
@Table(name=ValeurIncubationSol.JPA_NAME_TABLE, uniqueConstraints = @UniqueConstraint(columnNames ={
                                                                                                         ValeurIncubationSol.COLUMN_JPA_VALUE,
                                                                                                         DatatypeVariableUnitePRO.JPA_ID,
                                                                                                         MesureIncubationSol.ID_JPA
} 
                                                                                                         ),
     indexes = {
                   @Index(name="valeur_incub_mesure_idx",columnList=MesureIncubationSol.ID_JPA),
                   @Index(name="valeur_incub_variable" ,columnList= DatatypeVariableUnitePRO.JPA_ID)
     }   
)
public class ValeurIncubationSol implements Serializable,ISecurityPathWithVariable{

    
    public static final String  JPA_NAME_TABLE         = "valeurincubationsol";
    public static final String  ID_JPA             = "vis_id";
    public static final String  COLUMN_JPA_VALUE = "valeur";
    public static final String  ATTRIBUTE_JPA_STATUT_VALEUR = "statutvaleur";
    
    public static String        RESOURCE_PATH_WITH_VARIABLE = "%s/%s";
    
    
    
    @Id
    @Column(name = ValeurIncubationSol.ID_JPA)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long vis_id;
    
    @Column(name = ValeurIncubationSol.COLUMN_JPA_VALUE, nullable = false)
    private Float valeur;
    
     @Column(name=ValeurIncubationSol.ATTRIBUTE_JPA_STATUT_VALEUR)
    private String statutvaleur;
    
    @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = MesureIncubationSol.class)
    @JoinColumn(name = MesureIncubationSol.ID_JPA, referencedColumnName = MesureIncubationSol.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private MesureIncubationSol  mesureIncubationSol;
   
   
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = DatatypeVariableUnitePRO.JPA_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private DatatypeVariableUnitePRO datatypeVariableUnitePRO;

    public ValeurIncubationSol() {
        super();
    }

    public ValeurIncubationSol(Float valeur, String statutvaleur, MesureIncubationSol mesureIncubationSol, DatatypeVariableUnitePRO datatypeVariableUnitePRO) {
        this.valeur = valeur;
        this.statutvaleur = statutvaleur;
        this.mesureIncubationSol = mesureIncubationSol;
        this.datatypeVariableUnitePRO = datatypeVariableUnitePRO;
    }
    
       
    
    @Override
    public Date getDatePrelevement() {
        return  this.mesureIncubationSol.getDatePrelevement();
    }

    @Override
    public String getSecurityPathWithVariable() {
        return String.format(ValeurIncubationSol.RESOURCE_PATH_WITH_VARIABLE,
               this.getMesureIncubationSol().getVersionfile().getDataset().getLeafNode().getDatatype().getCode(),
               this.getMesureIncubationSol().getEchantillonsSol().getPrelevementsol().getDispositif().getCode());
    }

    public Long getVis_id() {
        return vis_id;
    }

    public void setVis_id(Long vis_id) {
        this.vis_id = vis_id;
    }

    public Float getValeur() {
        return valeur;
    }

    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    public String getStatutvaleur() {
        return statutvaleur;
    }

    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    public MesureIncubationSol getMesureIncubationSol() {
        return mesureIncubationSol;
    }

    public void setMesureIncubationSol(MesureIncubationSol mesureIncubationSol) {
        this.mesureIncubationSol = mesureIncubationSol;
    }

    public DatatypeVariableUnitePRO getDatatypeVariableUnitePRO() {
        return datatypeVariableUnitePRO;
    }

    public void setDatatypeVariableUnitePRO(DatatypeVariableUnitePRO datatypeVariableUnitePRO) {
        this.datatypeVariableUnitePRO = datatypeVariableUnitePRO;
    }
    
    
    
}
