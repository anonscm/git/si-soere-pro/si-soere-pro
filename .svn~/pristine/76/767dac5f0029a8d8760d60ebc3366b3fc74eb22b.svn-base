/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.impl;

import java.io.File;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.vo.DatesForm1ParamVO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public abstract class AbstractIncubationOutputBuilder<M> extends AbstractOutputBuilder{

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, 
            Map<String, Object> requestMetadatasMap) throws BusinessException{

        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedIncubationSolVariables = (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(IncubationParameterVO.INCUBATIONSOL));
        final DatesForm1ParamVO selectedIntervalDate = (DatesForm1ParamVO) requestMetadatasMap.get(DatesForm1ParamVO.class.getSimpleName());
        final List<M> mesureMap = resultsDatasMap.get(getIndexResults());
        final SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<Date, M>>> mesuresMap = new TreeMap();
        
        try {
            this.buildmap(mesureMap, mesuresMap);
        } catch (ParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        final Set<String> datatypeNames = new HashSet();
        selectedDispositifs.stream().filter((dispositif) -> !(!mesuresMap.containsKey(dispositif.getId()))).forEach((dispositif) -> {
            datatypeNames.add(getDispositifDatatypeKey(dispositif));
        });
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames, AbstractOutputBuilder.SUFFIX_FILENAME_DEFAULT);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().stream().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        this.readMap(selectedDispositifs, selectedIncubationSolVariables, selectedIntervalDate, outputPrintStreamMap, mesuresMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    abstract protected String getIndexResults();
    
    protected String getDispositifDatatypeKey(Dispositif dispositif) {
        return String.format("%s_%s", getDatatypeName(), dispositif.getNom());
    }

    abstract protected String getDatatypeName();

    abstract protected void buildmap(List<M> mesureMap, SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<Date, M>>> mesuresMap)throws ParseException;

    abstract protected void readMap(final List<Dispositif> selectedDispositifs,
           final List<VariablesPRO> selectedIncubationVariables,
           final DatesForm1ParamVO selectedIntervalDate,
           final Map<String, PrintStream> outputPrintStreamMap,
           final SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<Date, M>>> mesuresMap);
}
