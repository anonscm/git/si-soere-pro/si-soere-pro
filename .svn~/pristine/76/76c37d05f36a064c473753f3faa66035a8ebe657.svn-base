/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.solbrut;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.pro.extraction.solmoyenne.interfaces.ISolMoyenneDAO;
import org.inra.ecoinfo.pro.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.pro.refdata.categorievariable.IGroupeVariableBuilder;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author adiankha
 */
public class SolMoyenneDatasetManager extends MO implements ISolMoyenneDatasetManager, BeanFactoryAware {

    ISolMoyenneDAO solmoyenneDAO;
    protected BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    private IGroupeVariableBuilder getGroupeVariableBuilder() {
        return (IGroupeVariableBuilder) beanFactory.getBean(IGroupeVariableBuilder.BEAN_ID);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Lieu> getDispositifAvailableLieuSolMoyenne() {
        final List<Lieu> lieuAvailablesSolBrut;
        lieuAvailablesSolBrut = this.solmoyenneDAO.getAvailableSolMoyenneLieu();
        return lieuAvailablesSolBrut;
    }

    @Override
    public List<GroupeVariableVO> retrieveGroupesVariablesSolMoyenne(List<Long> dispositifId) throws BusinessException {
        try {
            List<VariablesPRO> variables = solmoyenneDAO.retrieveSolBrutAvailablesVariables(dispositifId);
            List<GroupeVariableVO> groupesVariables = getGroupeVariableBuilder().build(variables);
            return groupesVariables;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public List<CategorieVariable> getCategorieAvailableVariablesSolMoyenne(Long id) {
        final List<CategorieVariable> categorieVariablesAvailables;
        categorieVariablesAvailables = this.solmoyenneDAO.getAvailableVariableSolBrut(id);
        return categorieVariablesAvailables;
    }

    @Override
    public List<Dispositif> retrievePhysicoChimieAvailablesDispositifsByIdSolMoyenne(Long id) {
        final List<Dispositif> availablesDispositif = new LinkedList();
        final List<Dispositif> dispositif = this.solmoyenneDAO.getAvailablesDispositifsByLieuIdSolMoyenne(id);
        for (final Dispositif disps : dispositif) {
            availablesDispositif.add(disps);
        }
        return availablesDispositif;
    }

    public ISolMoyenneDAO getSolmoyenneDAO() {
        return solmoyenneDAO;
    }

    public void setSolmoyenneDAO(ISolMoyenneDAO solmoyenneDAO) {
        this.solmoyenneDAO = solmoyenneDAO;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    @Override
    public Dispositif retrieveSolMoyennesLieuByDispositif(Long idLieu, Long idDispositif) throws BusinessException {
        try {
            return solmoyenneDAO.retrieveSolMoyenneLieuByDispositif(idLieu, idDispositif);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

}
