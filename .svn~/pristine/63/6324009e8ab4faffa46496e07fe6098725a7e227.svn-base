/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.famille;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.inra.ecoinfo.localization.ILocalizationManager;
import static org.inra.ecoinfo.pro.refdata.RefDataUtil.gestionErreurs;
import org.inra.ecoinfo.pro.refdata.categorie.CategorieITK;
import org.inra.ecoinfo.pro.refdata.categorie.ICategorieDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */

public class Recorder extends AbstractCSVMetadataRecorder<Famille> {
    private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_BAD_CATEGORIE_ITK = "PROPERTY_MSG_BAD_CATEGORIE_ITK";
    
    protected IFamilleDAO familleDAO;
    protected String[] ListeCategoriesPossibles;
    protected ICategorieDAO categorieDAO;
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
       try {
           
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                 final String categorie = tokenizerValues.nextToken();
                 CategorieITK dbCategorie = categorieDAO.getByName(categorie);
                final Famille dbFamille = familleDAO.getByDelete(intitule, dbCategorie);
                    familleDAO.remove(dbFamille);
                    values = parser.getLine();
                
        } }
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        
        final ErrorsReport errorsReport = new ErrorsReport();
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            long line = 0;
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Famille.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code=Utils.createCodeFromString(nom);
                final String categorie_nom = tokenizerValues.nextToken();
                int index = tokenizerValues.currentTokenIndex();

                CategorieITK cat = categorieDAO.getByName(categorie_nom);
                if (cat == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_CATEGORIE_ITK), line,index,  categorie_nom));
                }
               
                if (!errorsReport.hasErrors()) {
                    persistFamille(nom, code, cat);
                }
                values = parser.getLine();
            }

            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }

    @Override
    protected List<Famille> getAllElements() throws PersistenceException {
       return familleDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Famille famille) throws PersistenceException {
        
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(famille == null || famille.getFamille_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : famille.getFamille_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
         
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(famille == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : (famille.getCategorieITK() != null ? famille.getCategorieITK().getCategorie_nom() : ""), ListeCategoriesPossibles, 
                        null, true, false, true));
            
        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Famille> initModelGridMetadata() {
        try {
            CategoriesPossibles();
        } catch (PersistenceException ex) {
            Logger.getLogger(Recorder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return super.initModelGridMetadata();
       
    }

    private void CategoriesPossibles()throws PersistenceException {
        List<CategorieITK> groupecategorie = categorieDAO.getAll(CategorieITK.class);
        String[] listeCategoriesPossibles = new String[groupecategorie.size() + 1];
        listeCategoriesPossibles[0] = "";
        int index = 1;
        for (CategorieITK categorieITK: groupecategorie) {
            listeCategoriesPossibles[index++] = categorieITK.getCategorie_nom();
        }
        this.ListeCategoriesPossibles = listeCategoriesPossibles;
    }

    private void persistFamille(String famille_nom,String code, CategorieITK cat) throws PersistenceException, BusinessException {
        final Famille famille = familleDAO.getByName(famille_nom);
        createOrUpdateFamille(famille_nom, code, cat, famille);
    }

    private void createOrUpdateFamille(String nom,String code, CategorieITK cat, Famille famille)throws PersistenceException, BusinessException {
        if (famille == null) {
            final Famille familles = new Famille(nom, cat);
            familles.setFamille_nom(nom);
            familles.setFamille_code(code);
            familles.setCategorieITK(cat);           
            createFamille(familles);
            
        } else {
            updateFamille(nom, code, cat, famille);
        }
    }

    private void createFamille(Famille familles) throws PersistenceException {
        familleDAO.saveOrUpdate(familles);
        familleDAO.flush();
    }

    private void updateFamille(String famille_nom,String code,CategorieITK categorie,Famille famille)throws PersistenceException {
        famille.setFamille_nom(famille_nom);
        famille.setFamille_code(code);
        famille.setCategorieITK(categorie);
        familleDAO.saveOrUpdate(famille);
    }

    public IFamilleDAO getFamilleDAO() {
        return familleDAO;
    }

    public void setFamilleDAO(IFamilleDAO familleDAO) {
        this.familleDAO = familleDAO;
    }

    public String[] getListeCategoriesPossibles() {
        return ListeCategoriesPossibles;
    }

    public void setListeCategoriesPossibles(String[] ListeCategoriesPossibles) {
        this.ListeCategoriesPossibles = ListeCategoriesPossibles;
    }

    public ICategorieDAO getCategorieDAO() {
        return categorieDAO;
    }

    public void setCategorieDAO(ICategorieDAO categorieDAO) {
        this.categorieDAO = categorieDAO;
    }

    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    
}
