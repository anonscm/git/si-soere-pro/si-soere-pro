package org.inra.ecoinfo.pro.refdata.nomregionalsol;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;

import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.pro.refdata.region.IRegionDAO;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typepedologique.ITypePedologiqueDAO;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.ITypeSolArvalisDAO;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis;
public class Recorder extends AbstractCSVMetadataRecorder<Nomregionalsol>{
     private static final String PROPERTY_MSG_BAD_REGION = "PROPERTY_MSG_BAD_REGION";
     private static final String PROPERTY_MSG_BAD_TYPEPEDO = "PROPERTY_MSG_BAD_TYPEPEDO";
     private static final String PROPERTY_MSG_BAD_ARVALIS = "PROPERTY_MSG_BAD_ARVALIS";
     private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
     protected  INomRegionalSolDAO nomregionalDAO;
     protected IRegionDAO regionDAO;
     protected ITypeSolArvalisDAO tsarvalisDAO;
     protected ITypePedologiqueDAO typepedoDAO;
     private Properties nregionalEN;
     
     private String[] listeRegionPossibles;
     private String[] listeTypepedologiquePossibles; 
     private String[] listeArvalisPossibles;
    
    private void createNomregionalsol(final Nomregionalsol nomregionalsol) throws PersistenceException {
        nomregionalDAO.saveOrUpdate(nomregionalsol);
        nomregionalDAO.flush();
    }
    
    private void updateNomregionalsol(final String code ,String nom, Nomregionalsol dbnomregionalsol, Typepedologique typo,
                                              Typesolarvalis typearvalis,Region region) throws PersistenceException {
        dbnomregionalsol.setNom(nom);
        dbnomregionalsol.setCode(code);
        dbnomregionalsol.setRegion(region);
        dbnomregionalsol.setTypepedologique(typo);
        dbnomregionalsol.setTypesolarvalis(typearvalis);
        nomregionalDAO.saveOrUpdate(dbnomregionalsol);
        
    }

    private void createOrUpdateNomregionalsol(final String code ,String nom, Nomregionalsol dbnomregionalsol, Typepedologique typo,
                                              Typesolarvalis typearvalis,Region region) throws PersistenceException, BusinessException {
        if (dbnomregionalsol == null) {
             final Nomregionalsol nomregionalsol = new Nomregionalsol(nom,typo,typearvalis,region);
        nomregionalsol.setCode(code);
        nomregionalsol.setNom(nom);
        nomregionalsol.setRegion(region);
        nomregionalsol.setTypepedologique(typo);
        nomregionalsol.setTypesolarvalis(typearvalis);
         createNomregionalsol(nomregionalsol);
        } else {
            updateNomregionalsol(code, nom, dbnomregionalsol, typo, typearvalis, region);
        }
    }
    private void persistNomregionalsol( final String code, final String nom,Typepedologique typo,
                                             Typesolarvalis solarvalis,Region region) throws PersistenceException, BusinessException {
        final Nomregionalsol dbnomregionalsol = nomregionalDAO.getByName(nom,typo);
        createOrUpdateNomregionalsol(code, nom, dbnomregionalsol, typo,solarvalis,region);
    }
    
     private void listeNomRegion() throws PersistenceException {
        List<Region> lesregions = regionDAO.getAll(Region.class);
        String[] Listeregion = new String[lesregions.size() + 1];
        Listeregion[0] = "";
        int index = 1;
        for (Region region : lesregions) {
            Listeregion[index++] = region.getNom();
        }
        this.listeRegionPossibles = Listeregion;
    }
    
     
      private void listeNomArvalis() throws PersistenceException {
        List<Typesolarvalis> lesarvalis = tsarvalisDAO.getAll(Typesolarvalis.class);
        String[] Listearvalis = new String[lesarvalis.size() + 1];
        Listearvalis[0] = "";
        int index = 1;
        for (Typesolarvalis typesolarvalis : lesarvalis) {
            Listearvalis[index++] = typesolarvalis.getNom();
        }
        this.listeArvalisPossibles = Listearvalis;
    } 
      private void listeNomTypePedo() throws PersistenceException {
        List<Typepedologique> lestypepedo = typepedoDAO.getAll(Typepedologique.class);
        String[] Listetypepedo = new String[lestypepedo.size() + 1];
        Listetypepedo[0] = "";
        int index = 1;
        for (Typepedologique typepedologique : lestypepedo) {
            Listetypepedo[index++] = typepedologique.getNom();
        }
        this.listeTypepedologiquePossibles = Listetypepedo;
    } 

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Nomregionalsol nomregionalsol) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomregionalsol.getNom(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null || nomregionalsol.getNom() == null ? 
                              AbstractCSVMetadataRecorder.EMPTY_STRING : nregionalEN.getProperty(nomregionalsol.getNom()),
                                   ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        nomregionalsol.getRegion().getNom()!= null ?
                                        nomregionalsol.getRegion().getNom() : "", 
                                                    listeRegionPossibles, null, true, false, true));
        
         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        nomregionalsol.getTypesolarvalis().getNom()!= null ?
                                        nomregionalsol.getTypesolarvalis().getNom() : "", 
                                                    listeArvalisPossibles, null, true, false, true));
         
          lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        nomregionalsol.getTypepedologique()!= null ?
                                        nomregionalsol.getTypepedologique().getNom() : "", 
                                                    listeTypepedologiquePossibles, null, true, false, true));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nomregionalsol.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
               /* tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String pedo = tokenizerValues.nextToken();
                Typepedologique dbpedo = typepedoDAO.getByName(pedo);*/
                Nomregionalsol nas=  nomregionalDAO.getByNName(nom);
                nomregionalDAO.remove(nas);
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
     private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    protected List<Nomregionalsol> getAllElements() throws PersistenceException {
       return nomregionalDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line=0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nomregionalsol.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                int index=tokenizerValues.currentTokenIndex();
                final String region = tokenizerValues.nextToken();
                int indexarvalis=tokenizerValues.currentTokenIndex();
                final String arvalis = tokenizerValues.nextToken();
                int indexpedo=tokenizerValues.currentTokenIndex();
                final String typepedo = tokenizerValues.nextToken();
                Region dbregion = regionDAO.getByNom(region);
                if(dbregion==null){
                 errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH,Recorder.PROPERTY_MSG_BAD_REGION ),line,index, region));
                }
                
                Typepedologique dbtypepedo  = typepedoDAO.getByName(typepedo);
                if(dbtypepedo==null){
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_TYPEPEDO),line,indexpedo, typepedo)); 
                }
                
                Typesolarvalis dbtypearvalis = tsarvalisDAO.getByName(arvalis);
                if(dbtypearvalis==null){
                     errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_ARVALIS),line,indexarvalis, arvalis)); 
                }
                if (!errorsReport.hasErrors()) {
                    persistNomregionalsol(code, nom, dbtypepedo, dbtypearvalis, dbregion);
                }
               
                values = parser.getLine();
            }

            gestionErreurs(errorsReport);
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    @Override
    protected ModelGridMetadata<Nomregionalsol> initModelGridMetadata() {
         try {
              nregionalEN = localizationManager.newProperties(Nomregionalsol.NAME_ENTITY_JPA, Nomregionalsol.JPA_COLUMN_NAME, Locale.ENGLISH);
             listeNomArvalis();
             listeNomRegion();
              listeNomTypePedo();
         } catch (PersistenceException ex) {
             ex.printStackTrace();
         }
        return super.initModelGridMetadata();
    }
    
    public INomRegionalSolDAO getNomregionalDAO() {
        return nomregionalDAO;
    }

    
    public void setNomregionalDAO(INomRegionalSolDAO nomregionalDAO) {
        this.nomregionalDAO = nomregionalDAO;
    }

    
    public Properties getNregionalEN() {
        return nregionalEN;
    }

    
    public void setNregionalEN(Properties nregionalEN) {
        this.nregionalEN = nregionalEN;
    }

    public IRegionDAO getRegionDAO() {
        return regionDAO;
    }

    public ITypeSolArvalisDAO getTsarvalisDAO() {
        return tsarvalisDAO;
    }

    public ITypePedologiqueDAO getTypepedoDAO() {
        return typepedoDAO;
    }

    public void setRegionDAO(IRegionDAO regionDAO) {
        this.regionDAO = regionDAO;
    }

    public void setTsarvalisDAO(ITypeSolArvalisDAO tsarvalisDAO) {
        this.tsarvalisDAO = tsarvalisDAO;
    }

    public void setTypepedoDAO(ITypePedologiqueDAO typepedoDAO) {
        this.typepedoDAO = typepedoDAO;
    }
    

}
