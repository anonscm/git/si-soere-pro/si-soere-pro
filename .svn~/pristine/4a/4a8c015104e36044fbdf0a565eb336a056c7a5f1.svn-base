/*
 *
 */
package org.inra.ecoinfo.pro.refdata.nomenclature;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;
import org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO;
import org.inra.ecoinfo.pro.refdata.filiere.Filiere;
import org.inra.ecoinfo.pro.refdata.filiere.IFiliereDAO;
import org.inra.ecoinfo.pro.refdata.precisionfiliere.IPrecisionFiliereDAO;
import org.inra.ecoinfo.pro.refdata.precisionfiliere.Precisionfiliere;
import org.inra.ecoinfo.pro.refdata.precisionpro.IPrecisionProDAO;
import org.inra.ecoinfo.pro.refdata.precisionpro.Precisionpro;
import org.inra.ecoinfo.pro.refdata.typenomenclature.ITypeNomenclatureDAO;
import org.inra.ecoinfo.pro.refdata.typenomenclature.TypeNomenclature;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 * 
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Nomenclatures> {

    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_TYPENOMEN = "PROPERTY_MSG_NOMENCLATURE_BAD_TYPENOMEN";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_FILIERE = "PROPERTY_MSG_NOMENCLATURE_BAD_FILIERE";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONFILIERE = "PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONFILIERE";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONPRO = "PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONPRO";
    private static final String PROPERTY_MSG_FACTEUR_NONDEFINI = "FACTEUR_NONDEFINI";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    protected INomenclatureDAO nomenDAO;

    protected ITypeNomenclatureDAO typenomenclatureDAO;
    
    IFiliereDAO filiereDAO;
    IPrecisionFiliereDAO precisionfiliereDAO;
    IPrecisionProDAO precisionproDAO;
    
    IFacteurDAO facteurDAO;

    private String[] listeTypeNomenclaturePossibles;
    private String [] listeFilierePossibles;
    private String [] listePrecisionProPossibles;
    private String [] listePrecisionFilierePossibles;
    private String[] listeFacteurPossibles;

    private Properties nomenNomEN;

    private Properties commenEN;

    private void createNomenclatures(final Nomenclatures nomenclatures) throws PersistenceException {
        nomenDAO.saveOrUpdate(nomenclatures);
        nomenDAO.flush();
    }

    private void createOrUpdateNomenclatures(String code,String mycode,String nom,String comment,String etat,
                                       TypeNomenclature tnom,Filiere filiere, Precisionfiliere pfiliere, String ppro,
                                       final Nomenclatures dbnomenclatures, Facteur facteur,String provenance,
                                       String intitule_affichage) throws PersistenceException {
        if (dbnomenclatures == null) {
          Nomenclatures nomenclatures = new Nomenclatures(mycode, mycode, provenance, comment, etat, tnom, filiere, pfiliere, ppro, facteur, provenance, intitule_affichage);
          nomenclatures.setNomen_code(code);
          nomenclatures.setNomen_mycode(mycode);
          nomenclatures.setTypenomenclature(tnom);
          nomenclatures.setNomen_comment(comment);
          nomenclatures.setNomen_nom(nom);
          nomenclatures.setEtatcoproduit(etat);
          nomenclatures.setFiliere(filiere);
          nomenclatures.setPrecisionfiliere(pfiliere);
          nomenclatures.setPrecisionpro(ppro);
          nomenclatures.setFacteur(facteur);
          nomenclatures.setIntitule_affichage(intitule_affichage);
          nomenclatures.setProvenance(provenance);
            createNomenclatures(nomenclatures);
        } else {
            updateBDNomenclatures(code, mycode, nom, comment, etat, tnom, filiere, pfiliere, ppro, dbnomenclatures, facteur, provenance, intitule_affichage);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {
                TypeNomenclature tnomenclature = null;
                Nomenclatures nomenclature = null;    
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String libelle = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String typenoment = tokenizerValues.nextToken();
                tnomenclature = typenomenclatureDAO.getByName(typenoment);
                nomenclature = nomenDAO.getByNNames(libelle, tnomenclature.getTn_nom());
                nomenDAO.remove(nomenclature);
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    protected List<Nomenclatures> getAllElements() throws PersistenceException {
        return nomenDAO.getAll();
    }


    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Nomenclatures nomenclatures) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_code() == null ? 
                        AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getNomen_code(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false,
                        true));
        

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getNomen_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false,
                        true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_nom() == null ? 
                        AbstractCSVMetadataRecorder.EMPTY_STRING : nomenNomEN.getProperty(nomenclatures.getNomen_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        
         lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getIntitule_affichage()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getIntitule_affichage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false,
                        true));
          lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getProvenance()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getProvenance(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false,
                        false));
        
         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        nomenclatures.getTypenomenclature()!= null ? nomenclatures.getTypenomenclature().getTn_nom(): "",
                              listeTypeNomenclaturePossibles,
                        null, true, false, true));
       
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        nomenclatures.getFiliere().getFiliere_nom()!= null ? nomenclatures.getFiliere().getFiliere_nom() : "",
                              listeFilierePossibles,
                        null, false, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        nomenclatures.getPrecisionfiliere().getPfiliere_nom()!= null ? nomenclatures.getPrecisionfiliere().getPfiliere_nom() : "",
                              listePrecisionFilierePossibles,
                        null, false, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getPrecisionpro()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getPrecisionpro(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false,
                        false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        nomenclatures.getFacteur() != null ? nomenclatures.getFacteur().getLibelle() : "",
                              listeFacteurPossibles,
                        null, false, false, true));
         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getEtatcoproduit() == null ? AbstractCSVMetadataRecorder.
                        EMPTY_STRING : nomenclatures.getEtatcoproduit(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
         
         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_comment() == null ? 
                        AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getNomen_comment(), 
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_comment() == null ? AbstractCSVMetadataRecorder.
                        EMPTY_STRING : commenEN.getProperty(nomenclatures.getNomen_comment()),
                               ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

   
    public INomenclatureDAO getNomenDAO() {
        return nomenDAO;
    }

   

    @Override
    protected ModelGridMetadata<Nomenclatures> initModelGridMetadata() {
        try {
            nomenNomEN = localizationManager.newProperties(Nomenclatures._NAME_ENTITY_JPA, Nomenclatures.JPA_COLUMN_NAME, Locale.ENGLISH);
            commenEN = localizationManager.newProperties(Nomenclatures._NAME_ENTITY_JPA, Nomenclatures.JPA_COLUMN__COMMENT, Locale.ENGLISH);
            updateNamesTypeNomen();
            PrecisionProPossibles();
            PrecisionFilierePossibles();
            FilierePossibles();
            facteurPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();

    }

    private void persistNomenclature( final String code, final String mycode, final String nom, 
                              final String comment, String etat,TypeNomenclature tnomen,
                               Filiere filiere,Precisionfiliere pfiliere,String ppro, Facteur facteur,
                               String provenance,String affichage) throws PersistenceException, BusinessException {
        final Nomenclatures dbnomenclatures = nomenDAO.getByName(nom);
       
        createOrUpdateNomenclatures(code, mycode, nom, comment, etat, tnomen, filiere, pfiliere, ppro, dbnomenclatures, facteur, provenance, affichage);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line=0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nomenclatures._NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(nom);
                String affichage = tokenizerValues.nextToken();
                String provenance = tokenizerValues.nextToken();
                long indextn = tokenizerValues.currentTokenIndex();
                final String tnnom = tokenizerValues.nextToken();
                long indexf = tokenizerValues.currentTokenIndex();
                final String filiere = tokenizerValues.nextToken();
                long indexpf = tokenizerValues.currentTokenIndex();
                final String pfiliere = tokenizerValues.nextToken();
                long indexpp = tokenizerValues.currentTokenIndex();
                final String ppro = tokenizerValues.nextToken();
                long indexFacteur = tokenizerValues.currentTokenIndex();
                final String libelleFacteur = tokenizerValues.nextToken();   
                final String etat = tokenizerValues.nextToken();
                final String commen = tokenizerValues.nextToken();
               
                TypeNomenclature dbtypenomen = verifieTypeNomenclature(tnnom, errorsReport, line, indextn);
                Filiere dbfiliere = verifeFiliere(filiere, errorsReport, line, indexf);
                Precisionfiliere dbpfiliere = verifiePrecisionFiliere(pfiliere, dbfiliere, errorsReport, line, indexpf);
               
                Facteur dbFacteur = verifieFacteur(libelleFacteur, errorsReport, line, indexFacteur);
                
               
                if (!errorsReport.hasErrors()) {
                    persistNomenclature(code, mycode, nom, commen, etat, dbtypenomen, dbfiliere, dbpfiliere, ppro, dbFacteur, provenance, affichage);
                }
                values = parser.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    

   
    
    private Facteur verifieFacteur(final String libelleFacteur, final ErrorsReport errorsReport, long line, long index) throws PersistenceException {
    	Facteur dbFacteur = facteurDAO.getByLibelleFct(libelleFacteur);
        if (dbFacteur == null) 
        {
        	errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_FACTEUR_NONDEFINI),line, index, libelleFacteur));
        }
        return dbFacteur;
    }

    private Precisionfiliere verifiePrecisionFiliere(final String pfiliere, Filiere dbfiliere, final ErrorsReport errorsReport, long line, long indexpp) throws PersistenceException {
        Precisionfiliere dbpfiliere = precisionfiliereDAO.getByName(pfiliere);
        if(dbfiliere==null){
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONFILIERE),line,indexpp, pfiliere));
        }
        return dbpfiliere;
    }

    private Filiere verifeFiliere(final String filiere, final ErrorsReport errorsReport, long line, long indexpf) throws PersistenceException {
        Filiere dbfiliere = filiereDAO.getByName(filiere);
        if(dbfiliere==null){
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_FILIERE),line,indexpf, filiere));
        }
        return dbfiliere;
    }

    private TypeNomenclature verifieTypeNomenclature(final String tnnom, final ErrorsReport errorsReport, long line, long indexf) throws PersistenceException {
        TypeNomenclature dbtypenomen = typenomenclatureDAO.getByCode(Utils.createCodeFromString(tnnom));
        if (dbtypenomen == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_TYPENOMEN),line,indexf, tnnom));
        }
        return dbtypenomen;
    }
    public Properties getNomenNomEN() {
        return nomenNomEN;
    }

    
    public void setNomenNomEN(Properties nomenNomEN) {
        this.nomenNomEN = nomenNomEN;
    }

    
    public Properties getCommenEN() {
        return commenEN;
    }

    
    public void setCommenEN(Properties commenEN) {
        this.commenEN = commenEN;
    }

    public void setTypenomenclatureDAO(ITypeNomenclatureDAO typenomenclatureDAO) {
        this.typenomenclatureDAO = typenomenclatureDAO;
    }

   
    public void setNomenDAO(INomenclatureDAO nomenDAO) {
        this.nomenDAO = nomenDAO;
    }
    
    private void updateBDNomenclatures(String code,String mycode,String nom,String comment,String etat,
                                       TypeNomenclature tnom,Filiere filiere, Precisionfiliere pfiliere, String ppro,
                                       final Nomenclatures dbnomenclatures, Facteur facteur,
                                       String provenance,String affichage) throws PersistenceException {
        dbnomenclatures.setNomen_code(code);
        dbnomenclatures.setNomen_nom(nom);
        dbnomenclatures.setNomen_mycode(mycode);
        dbnomenclatures.setTypenomenclature(tnom);
        dbnomenclatures.setFiliere(filiere);
        dbnomenclatures.setEtatcoproduit(etat);
        dbnomenclatures.setNomen_comment(comment);
        dbnomenclatures.setPrecisionfiliere(pfiliere);
        dbnomenclatures.setPrecisionpro(ppro);
        dbnomenclatures.setFacteur(facteur);
        dbnomenclatures.setIntitule_affichage(affichage);
        dbnomenclatures.setProvenance(provenance);
        nomenDAO.saveOrUpdate(dbnomenclatures);
    }

    private void updateNamesTypeNomen() throws PersistenceException {
        List<TypeNomenclature> groupegtp = typenomenclatureDAO.getAll(TypeNomenclature.class);
        String[] listsTNPossibles = new String[groupegtp.size() + 1];
        listsTNPossibles[0] = "";
        int index = 1;
        for (TypeNomenclature typenome : groupegtp) {
            listsTNPossibles[index++] = typenome.getTn_nom();
        }
        this.listeTypeNomenclaturePossibles = listsTNPossibles;
    }
    private void FilierePossibles() throws PersistenceException {
        List<Filiere> lesfilieres = filiereDAO.getAll(Filiere.class);
        String[] listesFilierePossibles = new String[lesfilieres.size() + 1];
        listesFilierePossibles[0] = "";
        int index = 1;
        for (Filiere filiere : lesfilieres) {
            listesFilierePossibles[index++] = filiere.getFiliere_nom();
        }
        this.listeFilierePossibles = listesFilierePossibles;
    }
    private void PrecisionFilierePossibles() throws PersistenceException {
        List<Precisionfiliere> lespfilieres = precisionfiliereDAO.getAll(Precisionfiliere.class);
        String[] listePFilierePossibles = new String[lespfilieres.size() + 1];
        listePFilierePossibles[0] = "";
        int index = 1;
        for (Precisionfiliere pfiliere : lespfilieres) {
            listePFilierePossibles[index++] = pfiliere.getPfiliere_nom();
        }
        this.listePrecisionFilierePossibles = listePFilierePossibles;
    }
    private void PrecisionProPossibles() throws PersistenceException {
        List<Precisionpro> lesppros = precisionproDAO.getAll(Precisionpro.class);
        String[] listePProPossibles = new String[lesppros.size() + 1];
        listePProPossibles[0] = "";
        int index = 1;
        for (Precisionpro ppro : lesppros) {
            listePProPossibles[index++] = ppro.getP_nom();
        }
        this.listePrecisionProPossibles = listePProPossibles;
    }
    
    private void facteurPossibles() throws PersistenceException 
    {
        List<Facteur> lstFacteurs = facteurDAO.getLstFacteurByIsAssocNomenclature(true);
        String[] facteurPossibles = new String[lstFacteurs.size()+1];
        facteurPossibles[0] = (String) Constantes.STRING_EMPTY;
		int index = 1;
        for (Facteur facteur : lstFacteurs) 
        {
        	facteurPossibles[index++] = facteur.getLibelle();
        }
        this.listeFacteurPossibles = facteurPossibles;
    }

    public IFiliereDAO getFiliereDAO() {
        return filiereDAO;
    }

    public IPrecisionFiliereDAO getPrecisionfiliereDAO() {
        return precisionfiliereDAO;
    }

    public IPrecisionProDAO getPrecisionproDAO() {
        return precisionproDAO;
    }

    public void setFiliereDAO(IFiliereDAO filiereDAO) {
        this.filiereDAO = filiereDAO;
    }

    public void setPrecisionfiliereDAO(IPrecisionFiliereDAO precisionfiliereDAO) {
        this.precisionfiliereDAO = precisionfiliereDAO;
    }

    public void setPrecisionproDAO(IPrecisionProDAO precisionproDAO) {
        this.precisionproDAO = precisionproDAO;
    }

	public void setFacteurDAO(IFacteurDAO facteurDAO) {
		this.facteurDAO = facteurDAO;
	}
    
    

}
