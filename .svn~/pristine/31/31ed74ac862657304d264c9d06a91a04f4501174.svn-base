/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.pro.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.pro.extraction.jsf.HelperForm;
import org.inra.ecoinfo.pro.extraction.physicochimie.plante.IPlanteBrutDatasetManager;
import org.inra.ecoinfo.pro.extraction.plantebrute.impl.PlanteBrutParameters;
import org.inra.ecoinfo.pro.extraction.vo.DispositifVO;
import org.inra.ecoinfo.pro.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.pro.extraction.vo.VariableVO;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.vo.DatesForm1ParamVO;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.tree.TreeNodeItem;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.richfaces.component.UITree;
import org.richfaces.component.UITreeNode;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 *
 * @author adiankha
 */
@ManagedBean(name = "uiBeanPlanteBrut")
@ViewScoped
public class UIBeanPlanteBrutPhysicoChimie extends AbstractUIBeanForSteps implements Serializable {

    @ManagedProperty(value = "#{plantebrutDatasetManager}")
    IPlanteBrutDatasetManager plantebrutDatasetManager;

    @ManagedProperty(value = "#{notificationsManager}")
    INotificationsManager notificationsManager;

    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;

    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    @ManagedProperty(value = "#{securityContext}")
    ISecurityContext securityContext;

    @ManagedProperty(value = "#{localizationManager}")
    ILocalizationManager localizationManager;

    @Transient
    ParametersRequest parametersRequest = new ParametersRequest();

    @Transient
    HelperForm helperForm = new HelperForm();

    Long idVariableSelected;

    private Properties propertiesGroupeNames;

    String affichage = "1";

    final Map<Long, LieuJSF> lieuAvailablesPlanteBrut = new HashMap();
    @Transient
    final Map<Long, VariableJSF> variablesPhysicoChimiePlanteAvailables = new HashMap();

    Boolean frequenceRequired = false;

    Properties propertiesDispNames;

    Properties propertiesVariablesNames;

    Properties propertiesLieuNames;

    private UITreeNode treeNodeGroupeVariable;

    private UITree treeListVariables;

    private TreeNodeGroupeVariable treeNodeGroupeVariableSelected;

    @Transient
    Map<String, TreeNodeItem> availablesDispositifs = new TreeMap();

    private List<TreeNodeGroupeVariable> rootNodes = new ArrayList<TreeNodeGroupeVariable>();

    private VOVariableJSF variableSelected = new VOVariableJSF(null);

    List<VariableJSF> variablesSelected = new LinkedList<VariableJSF>();

    public UIBeanPlanteBrutPhysicoChimie() {
    }

    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setPlantebrutDatasetManager(IPlanteBrutDatasetManager plantebrutDatasetManager) {
        this.plantebrutDatasetManager = plantebrutDatasetManager;
    }

    public String navigate() {
        return "plantebrut";
    }

    public Map<Long, LieuJSF> getLieuAvailables() throws BusinessException {

        if (this.lieuAvailablesPlanteBrut.isEmpty()) {
            final List<Lieu> lieux = this.plantebrutDatasetManager.getDispositifAvailableLieuPlanteBrut();
            for (final Lieu lieu : lieux) {
                final List<Dispositif> dispositifs = this.plantebrutDatasetManager.
                        retrievePhysicoChimieAvailablesDispositifsByIdPlanteBrut(lieu.getId());
                final LieuJSF lieuJSF = new LieuJSF(lieu, dispositifs);
                this.lieuAvailablesPlanteBrut.put(lieu.getId(), lieuJSF);
            }
        }
        return this.lieuAvailablesPlanteBrut;
    }

    public final String addAllDepths() {
        return null;
    }

    private void expandAndSelectGroupeVariable(TreeNodeGroupeVariable treeNodeGroupeVariable) {
        treeNodeGroupeVariable.setExpanded(true);
        if (treeNodeGroupeVariable.getGroupesVariables() == null || treeNodeGroupeVariable.getGroupesVariables().isEmpty()) {
            for (VOVariableJSF variable : treeNodeGroupeVariable.getVariables()) {
                enableOrDisableLeaf(false, variable);
            }

        } else {
            for (TreeNodeGroupeVariable childTreeNodeGroupeVariable : treeNodeGroupeVariable.getGroupesVariables()) {
                expandAndSelectGroupeVariable(childTreeNodeGroupeVariable);
            }
        }
    }

    public String deployNodeGroupeVariable() {

        expandAndSelectGroupeVariable(treeNodeGroupeVariableSelected);

        return null;
    }

    public void selectVariable(VOVariableJSF variable) {
        Map<Long, VOVariableJSF> variablesSelecteds = getParametersRequest().getVariablesSelected();
        enableOrDisableLeaf(variablesSelecteds.containsKey(variable.getVariable().getId()), variable);
    }

    private void enableOrDisableLeaf(Boolean conditionDisabling, VOVariableJSF variable) {
        Map<Long, VOVariableJSF> variablesSelecteds = getParametersRequest().getVariablesSelected();

        if (conditionDisabling) {
            variablesSelecteds.remove(variable.getVariable().getId());
            variable.setSelected(false);
        } else {
            variablesSelecteds.put(variable.getVariable().getId(), variable);
            variable.setSelected(true);
        }
    }

    public String selectVariable() {
        selectVariable(variableSelected);
        return null;
    }

    /**
     *
     * @return
     */
    public String deSelectVariable() {
        for (TreeNodeGroupeVariable groupeVariable : rootNodes) {
            deSelectVariable(groupeVariable);
        }
        return null;
    }

    private String deSelectVariable(TreeNodeGroupeVariable groupeVariable) {
        if (groupeVariable.getGroupeVariable().getChildren().isEmpty()) {
            for (VOVariableJSF variable : groupeVariable.getVariables()) {
                if (variable.getVariable().getId().equals(idVariableSelected)) {
                    variableSelected = variable;
                    selectVariable(variableSelected);
                    return null;
                }
            }
        } else {
            for (TreeNodeGroupeVariable groupeVariables : groupeVariable.getGroupesVariables()) {

                deSelectVariable(groupeVariables);
            }
        }
        return null;
    }

    public String addAllVariables() {
        Map<Long, VOVariableJSF> variablesSelected;
        variablesSelected = getParametersRequest().getVariablesSelected();
        variablesSelected.clear();

        for (TreeNodeGroupeVariable groupeVariable : rootNodes) {
            addAllGroupVariables(groupeVariable);
        }
        return null;
    }

    private String addAllGroupVariables(TreeNodeGroupeVariable groupeVariable) {
        groupeVariable.setExpanded(true);
        if (groupeVariable.getGroupeVariable().getChildren().isEmpty()) {
            for (VOVariableJSF variable : groupeVariable.getVariables()) {
                getParametersRequest().getVariablesSelected().put(variable.getVariable().getId(), variable);
                variable.setSelected(true);

            }
        } else {
            for (TreeNodeGroupeVariable groupeVariables : groupeVariable.getGroupesVariables()) {

                addAllGroupVariables(groupeVariables);
            }
        }
        return null;
    }

    public String removeAllVariables() throws BusinessException, ParseException {
        createOrUpdateVariablesAvailables();
        return null;
    }

    public List<TreeNodeGroupeVariable> getRootNodes() throws BusinessException, ParseException {
        if (rootNodes == null) {
            createOrUpdateVariablesAvailables();
        }
        return rootNodes;
    }

    void createOrUpdateVariablesAvailables() throws BusinessException {

        getParametersRequest().getVariablesSelected().clear();
        rootNodes.clear();
        if (!this.getParametersRequest().getDispositifsSelected().isEmpty()) {
            List<GroupeVariableVO> groupeVariables;
            groupeVariables = plantebrutDatasetManager.retrieveGroupesVariablesPlanteBrut(new LinkedList<Long>(this.getParametersRequest()
                    .getDispositifsSelected().keySet()));
            for (GroupeVariableVO groupeVariable : groupeVariables) {
                TreeNodeGroupeVariable treeNodeGroupeVariable = new TreeNodeGroupeVariable(groupeVariable);
                rootNodes.add(treeNodeGroupeVariable);

            }
        }
    }

    public String extract() throws BusinessException {
          final Map<String, Object> metadatasMap = new HashMap();

        metadatasMap.put(Dispositif.class.getSimpleName(),
                this.parametersRequest.getListDispositifsSelected());

        List<VariablesPRO> variablesPRO = new LinkedList<>();
        for (VOVariableJSF variable : parametersRequest.getListVariablesSelected()) {
            variablesPRO.add(variable.getVariable().getVariablesPRO());
        }
        metadatasMap.put(VariablesPRO.class.getSimpleName().concat(PlanteBrutParameters.PLANTEBRUT), variablesPRO);

        metadatasMap.put(DatesForm1ParamVO.class.getSimpleName(), this.parametersRequest.getDatesForm1ParamVO());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, this.parametersRequest.getCommentExtraction());
        
        final PlanteBrutParameters parameters = new PlanteBrutParameters(metadatasMap);
        Integer aff = Integer.parseInt(affichage);
        this.extractionManager.extract(parameters, aff);
        return null;
    }

    DatesForm1ParamVO datesForm1ParamVO;

    public Boolean getDateStepIsValid() throws java.text.ParseException, BadExpectedValueException {
        if (this.datesForm1ParamVO == null) {
            return false;
        }
        this.datesForm1ParamVO.getIntervalDate();
        return true;
    }

    public void setIsStepValid(boolean b) {

    }

    @Override
    public boolean getIsStepValid() {
        if (this.getStep() == 1) {
            return this.getParametersRequest().getDispositifStepIsValid();
        } else if (this.getStep() == 2) {
            return this.getParametersRequest().getVariableStepIsValid();
        }/* else if (this.getStep() == 3) {
            return this.getParametersRequest().getDateStepIsValid();
        }*/
        return false;
    }

    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    public Map<Long, VariableJSF> getVariablesPhysicoChimiePlanteAvailables() {
        return this.variablesPhysicoChimiePlanteAvailables;
    }

    public Properties getPropertiesDispNames() {
        if (this.propertiesDispNames == null) {
            this.setPropertiesDispNames(this.localizationManager.newProperties(
                    Dispositif.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesDispNames;
    }

    public final void setIdVariableSelected(final Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }

    public Long getIdVariableSelected() {
        return this.idVariableSelected;
    }

    public final void setFrequenceRequired(final Boolean frequenceRequired) {
        this.frequenceRequired = frequenceRequired;
    }

    public void setPropertiesDispNames(Properties propertiesDispNames) {
        this.propertiesDispNames = propertiesDispNames;
    }

    public Map<String, TreeNodeItem> getAvailablesDispositifs() {
        return availablesDispositifs;
    }

    public void setAvailablesDispositifs(Map<String, TreeNodeItem> availablesDispositifs) {
        this.availablesDispositifs = availablesDispositifs;
    }

    public DatesForm1ParamVO getDatesForm1ParamVO() {
        return datesForm1ParamVO;
    }

    public void setDatesForm1ParamVO(DatesForm1ParamVO datesForm1ParamVO) {
        this.datesForm1ParamVO = datesForm1ParamVO;
    }

    @Override
    public String nextStep() {
        super.nextStep();
        switch (this.getStep()) {
            case 2:
                try {
                    this.createOrUpdateVariablesAvailables();
                } catch (BusinessException e) {
                    this.rootNodes.clear();
                }
                break;
            default:
                break;
        }
        return null;
    }

    public String selectDispositif(Long idLieuSelected, Long idDispSelected) throws BusinessException {
        final DispositifJSF dispositifSelected = this.lieuAvailablesPlanteBrut.get(idLieuSelected).
                getDispositifs().get(idDispSelected);
        if (dispositifSelected.getSelected()) {
            this.getParametersRequest().getDispositifsSelected().remove(idDispSelected);
            dispositifSelected.setSelected(false);
        } else {
            this.getParametersRequest().getDispositifsSelected().put(idDispSelected, dispositifSelected.getDispositif());
            dispositifSelected.setSelected(true);
        }
        return null;
    }

    public class TreeNodeGroupeVariable {

        private final GroupeVariableVO groupeVariable;
        private String localizedGroupeName;
        private final List<TreeNodeGroupeVariable> groupesVariables = Lists.newArrayList();
        private Boolean expanded = false;

        private final List<VOVariableJSF> variables = Lists.newArrayList();

        public GroupeVariableVO getGroupeVariable() {
            return groupeVariable;
        }

        public TreeNodeGroupeVariable(GroupeVariableVO groupeVariable) {
            super();
            this.groupeVariable = groupeVariable;
            if (groupeVariable != null) {
                this.localizedGroupeName = getPropertiesGroupeNames().getProperty(groupeVariable.getCode());
                if (Strings.isNullOrEmpty(this.localizedGroupeName)) {
                    this.localizedGroupeName = groupeVariable.getCode();
                }
            }

        }

        public List<TreeNodeGroupeVariable> getGroupesVariables() {
            if (groupesVariables.isEmpty()) {

                if (groupeVariable.getChildren() != null && !groupeVariable.getChildren().isEmpty()) {
                    for (GroupeVariableVO groupeVariableVO : groupeVariable.getChildren()) {
                        groupesVariables.add(new TreeNodeGroupeVariable(groupeVariableVO));
                    }
                }
            }
            return groupesVariables;
        }

        public List<VOVariableJSF> getVariables() {

            if (variables.isEmpty()) {
                if (groupeVariable.getVariables() != null && !groupeVariable.getVariables().isEmpty() && !parametersRequest.dispositifsSelected.isEmpty()) {
                    for (Dispositif dispositif : parametersRequest.dispositifsSelected.values()) {
                        for (VariableVO variableVO : groupeVariable.getVariables()) {
                            if (securityContext.isRoot()
                                    || securityContext.matchPrivilege(String.format("%s/*/plante_brute/%s", dispositif.getCode(), variableVO.getCode()), Role.ROLE_EXTRACTION,
                                            ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                                VOVariableJSF voVariableJSF = new VOVariableJSF(variableVO);

                                variables.add(voVariableJSF);
                            }
                        }
                    }
                }
            }
            return variables;
        }

        public Boolean getExpanded() {
            return expanded;
        }

        public void setExpanded(Boolean expanded) {
            this.expanded = expanded;
        }

        public String getLocalizedGroupeName() {
            return localizedGroupeName;
        }

        public void setLocalizedGroupeName(String localizedGroupeName) {
            this.localizedGroupeName = localizedGroupeName;
        }
    }

    public TreeNodeGroupeVariable getTreeNodeGroupeVariableSelected() {
        return treeNodeGroupeVariableSelected;
    }

    public void setTreeNodeGroupeVariableSelected(TreeNodeGroupeVariable treeNodeGroupeVariableSelected) {
        this.treeNodeGroupeVariableSelected = treeNodeGroupeVariableSelected;
    }

    public UITree getTreeListVariables() {
        return treeListVariables;
    }

    public void setTreeListVariables(UITree treeListVariables) {
        this.treeListVariables = treeListVariables;
    }

    public UITreeNode getTreeNodeGroupeVariable() {
        return treeNodeGroupeVariable;
    }

    public void setTreeNodeGroupeVariable(UITreeNode treeNodeGroupeVariable) {
        this.treeNodeGroupeVariable = treeNodeGroupeVariable;
    }

    public void setRootNodes(List<TreeNodeGroupeVariable> rootNodes) {
        this.rootNodes = rootNodes;
    }

    public List<VariableJSF> getVariablesSelected() {
        return variablesSelected;
    }

    public void setVariablesSelected(List<VariableJSF> variablesSelected) {
        this.variablesSelected = variablesSelected;
    }

    public class DispositifJSF {

        final Dispositif dispositif;

        private String code;
        private String nom;

        Boolean selected = false;

        public DispositifJSF(final Dispositif dispositif) {
            super();
            this.dispositif = dispositif;
            this.code = dispositif.getCode();
            this.nom = dispositif.getNom();
        }

        public Dispositif getDispositif() {
            return dispositif;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(final Boolean selected) {
            this.selected = selected;
        }

    }

    public class LieuJSF {

        LieuVO lieu = null;
        final Map<Long, DispositifJSF> dispositifs = new HashMap();

        public LieuJSF(final Lieu lieu, final List<Dispositif> dispositifs) {
            super();
            this.lieu = new LieuVO(lieu);
            for (final Dispositif disp : dispositifs) {
                this.dispositifs.put(disp.getId(), new DispositifJSF(disp));
            }
        }

        public LieuVO getLieu() {
            return this.lieu;
        }

        public Map<Long, DispositifJSF> getDispositifs() {
            return this.dispositifs;
        }

    }

    public class LieuVO {

        private String nom;
        private Lieu lieu;
        private String localizedName;

        public LieuVO(final Lieu lieu) {
            super();
            this.setNom(lieu.getNom());
        }

        public String getLocalizedName() {
            return this.localizedName;
        }

        public String getNom() {
            return this.nom;
        }

        final void setNom(final String nom) {
            this.nom = nom;
            if (nom != null) {
                localizedName = getPropertiesAgrosNames().getProperty(nom);
            }
            if (Strings.isNullOrEmpty(this.localizedName)) {
                this.localizedName = nom;
            }
        }
    }

    public Properties getPropertiesAgrosNames() {
        if (this.propertiesLieuNames == null) {
            this.setPropertiesLieuNames(this.localizationManager.newProperties(
                    Dispositif.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesLieuNames;
    }

    public void setPropertiesLieuNames(Properties propertiesLieuNames) {
        this.propertiesLieuNames = propertiesLieuNames;
    }

    public class VariableJSF {

        public static final String PRODUITBRUT = "ProduitBrute";

        public static final String PRODUITMOYENNE = "ProduitMoyenne";

        public static final String SOLBRUTE = "SolBrute";

        public static final String SOLMOYENNE = "SolMoyenne";

        final VariableVO variable;

        String type;

        Boolean selected = false;

        private String localizedVariableName;

        public VariableJSF(final VariablesPRO variable, final String type) {
            super();
            this.variable = new VariableVO();
            this.type = type;

            if (variable != null) {
                this.localizedVariableName = getPropertiesVariablesNames().getProperty(variable.getAffichage());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getAffichage();
                }
            }
        }

        public Boolean getSelected() {
            return this.selected;
        }

        public String getType() {
            return this.type;
        }

        public VariableVO getVariable() {
            return this.variable;
        }

        public final void setSelected(final Boolean selected) {
            this.selected = selected;
        }

        public final void setType(final String type) {
            this.type = type;
        }
    }

    public class VOVariableJSF {

        private VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        public VOVariableJSF(VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariablesNames().getProperty(
                        variable.getAffichage());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getAffichage();
                }
            }

        }

        public VariableVO getVariable() {
            return variable;
        }

        public void setVariable(VariableVO variable) {
            this.variable = variable;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
    }

    public class ParametersRequest {

        final Map<Long, Dispositif> dispositifsSelected = new HashMap();
        private Map<Long, VOVariableJSF> variablesSelected = new HashMap<>();

        DatesForm1ParamVO datesForm1ParamVO;

        String commentExtraction;

        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        public DatesForm1ParamVO getDatesForm1ParamVO() {
            if (this.datesForm1ParamVO == null) {
                this.initDatesForm1ParamVO();
            }
            return this.datesForm1ParamVO;
        }

        public Boolean getDateStepIsValid() {
            try {
                if (this.datesForm1ParamVO == null) {
                    return false;
                }
                this.datesForm1ParamVO.getIntervalDate();
                return true;
            } catch (final ParseException | BadExpectedValueException e) {
                return false;
            }
        }

        public boolean getFormIsValid() {
            return this.getDispositifStepIsValid() && this.getVariableStepIsValid()
                    && this.getDateStepIsValid();
        }

        public List<Dispositif> getListDispositifsSelected() {
            return new LinkedList<>(this.dispositifsSelected.values());
        }

        public Map<Long, Dispositif> getDispositifsSelected() {
            return this.dispositifsSelected;
        }

        public Boolean getDispositifStepIsValid() {
            return !this.dispositifsSelected.isEmpty();
        }

        public Map<Long, VOVariableJSF> getVariablesSelected() {
            return variablesSelected;
        }

        public List<VOVariableJSF> getListVariablesSelected() {
            return new LinkedList<VOVariableJSF>(variablesSelected.values());
        }

        public Boolean getVariableStepIsValid() {
            return (!this.variablesSelected
                    .isEmpty()) && this.getDispositifStepIsValid();
        }

        void initDatesForm1ParamVO() {
            this.datesForm1ParamVO = new DatesForm1ParamVO(localizationManager);
        }

        public final void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        public final void setDatesForm1ParamVO(final DatesForm1ParamVO datesForm1ParamVO) {
            this.datesForm1ParamVO = datesForm1ParamVO;
        }
    }

    public void setPhysicochimieDatasetManager(IPlanteBrutDatasetManager plantebrutDatasetManager) {
        this.plantebrutDatasetManager = plantebrutDatasetManager;
    }

    public String getAffichage() {
        return this.affichage;
    }

    public void setAffichage(String affichage) {
        this.affichage = affichage;
    }

   /* public class DispositifVO {

        private Dispositif dispositif;
        private String localizedDispositifName;
        private String localizedSiteName;
        private Boolean selected = false;
        private Boolean type = true;

        public DispositifVO(Dispositif dispositif) {
            super();
            this.dispositif = dispositif;
            if (dispositif != null) {
                this.localizedDispositifName = getPropertiesDispNames().getProperty(dispositif.getNom());
                if (Strings.isNullOrEmpty(this.localizedDispositifName)) {
                    this.localizedDispositifName = dispositif.getNom();
                }

            }
        }

        public Dispositif getDispositif() {
            return dispositif;
        }

        public void setDispositif(Dispositif dispositif) {
            this.dispositif = dispositif;
        }

        public String getLocalizedDispositifName() {
            return localizedDispositifName;
        }

        public void setLocalizedDispositifName(String localizedDispositifName) {
            this.localizedDispositifName = localizedDispositifName;
        }

        public String getLocalizedSiteName() {
            return localizedSiteName;
        }

        public void setLocalizedSiteName(String localizedSiteName) {
            this.localizedSiteName = localizedSiteName;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        public Boolean getType() {
            return type;
        }

        public void setType(Boolean type) {
            this.type = type;
        }

    }*/

    public Properties getPropertiesGroupeNames() {
        if (this.propertiesGroupeNames == null) {
            this.setPropertiesGroupeNames(localizationManager.newProperties(CategorieVariable.NAME_ENTITY_JPA, "cvariable_nom"));
        }
        return propertiesGroupeNames;
    }

    public void setPropertiesGroupeNames(Properties propertiesGroupeNames) {
        this.propertiesGroupeNames = propertiesGroupeNames;
    }

    public Properties getPropertiesVariablesNames() {
        if (this.propertiesVariablesNames == null) {
            this.setPropertiesVariablesNames(this.localizationManager.newProperties(
                    VariablesPRO.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesVariablesNames;
    }

    public void setPropertiesVariablesNames(final Properties propertiesVariablesNames) {
        this.propertiesVariablesNames = propertiesVariablesNames;
    }

    public void setVariableSelected(VOVariableJSF variableSelected) {
        this.variableSelected = variableSelected;
    }

    public VOVariableJSF getVariableSelected() {
        return variableSelected;
    }

}
