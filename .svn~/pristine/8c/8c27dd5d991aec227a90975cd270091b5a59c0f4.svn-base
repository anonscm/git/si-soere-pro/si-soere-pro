
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SMP.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;


import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SMP.IMesureSMPDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.MesureSMP;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.SequenceSMP;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.SousSequenceSMP;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.ValeurSMP;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.impl.MPSLineRecord;
import org.inra.ecoinfo.pro.dataset.sol.JPA.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.sol.JPA.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.JPAHumiditeExpression;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordSMP extends AbstractProcessRecord{

    
    
     static final Logger                                       LOGGER           = Logger.getLogger(ProcessRecordSMP.class);
     protected static final String BUNDLE_PATH_SMP = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
     private static final String MSG_ERROR_MSP_NOT_FOUND_DVU_DB = "MSG_ERROR_MPS_NOT_FOUND_DVU_DB";
     private static final String MSG_ERROR_MPS_NOT_FOUND_METHODE_DB = "MSG_ERROR_MPS_NOT_FOUND_METHODE_DB";
     private static final String MSG_ERROR_MPS_NOT_UNITEPRO_DB = "MSG_ERROR_MPS_NOT_UNITEPRO_DB";
     private static final String MSG_ERROR_MPS_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_MPS_NOT_FOUND_HUMIDITE_DB";
     private static final String MSG_ERROR_MPS_NOT_VARIABLEPRO_DB = "MSG_ERROR_MPS_NOT_VARIABLEPRO_DB";
    
    //protected IMesureSMPDAO<MesureFluxChambres> mesureFluxChambreDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IParcelleElementaireDAO parcelleElementaireDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    JPAHumiditeExpression humiditeDAO;
    IVariablesPRODAO variPRODAO;

    public ProcessRecordSMP() {
        super();
    }
   
    
    
    

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties, String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport = new org.inra.ecoinfo.pro.utils.ErrorsReport();
        try {
           // Création du fichier de dépôt en base
            String[] values = null;
          
            final Map<Date, List<MPSLineRecord>> lines = new HashMap();
            
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, lines, lineCount, errorsReport);
            final SortedSet<MPSLineRecord> ligneEnErreur = new TreeSet();
            
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, lines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
            
        } catch (final IOException | ParseException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }
    
    
    private long readLines(CSVParser parser, Map<Date, List<MPSLineRecord>> lines, long lineCount, org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws IOException, ParseException{
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            
            final Date date= DateUtil.getSimpleDateFormatDateUTC().parse(cleanerValues.nextToken());
            final Date time = DateUtil.getSimpleDateFormatTimeUTC().parse(cleanerValues.nextToken());
            final String codevariable = cleanerValues.nextToken();
            final float valeurvariable = Float.parseFloat(cleanerValues.nextToken());
            final String statut = cleanerValues.nextToken();
             final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();
            
          
            final MPSLineRecord line = new MPSLineRecord(date, time,  lineCount,  codevariable,  valeurvariable, statut,  codemethode,  codeunite, codehumidite);
            Date date_mesure;
            try {
                date_mesure = DateUtil.getSimpleDateFormatDateUTC().parse(
                        DateUtil.getSimpleDateFormatDateUTC().format(date));
                if (!lines.containsKey(date_mesure)) {
                    lines.put(date_mesure, new LinkedList<MPSLineRecord>());
                }
                lines.get(date_mesure).add(line);
            } catch (ParseException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        date, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }
    
    
     private void buildLines(VersionFile versionFile, ISessionPropertiesPRO sessionProperties, org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport, Map<Date, List<MPSLineRecord>> lines, SortedSet<MPSLineRecord> ligneEnErreur) {
         Iterator<Entry<Date, List<MPSLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Date, List<MPSLineRecord>> lineDateEntry = iterator.next();
            Date date = lineDateEntry.getKey();
            for (MPSLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    
    private void recordErrors(org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
       if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    private void buildMesure(MPSLineRecord line, VersionFile versionFile, SortedSet<MPSLineRecord> ligneEnErreur, org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport, ISessionPropertiesPRO sessionProperties) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
     private void buildSequence(Date datePrelevement,  ParcelleElementaire parcelleElementaire, List<MPSLineRecord> sequenceLines, VersionFile versionFile, ErrorsReport errorsReport, boolean debugMode) throws PersistenceException, InsertionDatabaseException {

        MPSLineRecord firstLine = sequenceLines.get(0);

        SequenceSMP sequenceSMP = new SequenceSMP();

      //  testConflictsSequenceChimie(datePrelevement, projetSite.getId(), versionFile, debugMode);
      
        sequenceSMP.setDate(datePrelevement);
        sequenceSMP.setParcelleElementaire(parcelleElementaire);
        sequenceSMP.setVersion(versionFile);

        Map<String, List<MPSLineRecord>> sousSequencesLinesMap = new HashMap<String, List<MPSLineRecord>>();

//        for (MPSLineRecord line : sequenceLines) {
//            fillLinesMap(sousSequencesLinesMap, line, line.getCode_parcelle().concat(line.getProfondeur()));
//        }
//
//        for (String parcelleCode : sousSequencesLinesMap.keySet()) {
//            try {
//                List<MPSLineRecord> mesureLines = sousSequencesLinesMap.get(parcelleCode);
//                buildSousSequence(mesureLines.get(0).getPlateformeCode(), mesureLines, mesureLines.get(0).getOutilPrelevementCode(), sequenceChimie, errorsReport);
//            } catch (InsertionDatabaseException e) {
//                errorsReport.addException(e);
//            }
//        }
//        if (!errorsReport.hasErrors()) {
//            try {
//                sequenceSMPDAO.saveOrUpdate(sequenceChimie);
//            } catch (ConstraintViolationException e) {
//                String message = String.format(getGLACPEMessage(PROPERTY_MSG_DUPLICATE_SEQUENCE), DateUtil.getSimpleDateFormatDateyyyyMMddUTC().format(datePrelevement));
//                errorsReport.addException(new BusinessException(message));
//            }
//        }
    }
     
      private void fillLinesMap(Map<String, List<MPSLineRecord>> linesMap, MPSLineRecord line, String keyLine) {
        List<MPSLineRecord> sousSequenceLines = null;
        if (keyLine != null && keyLine.trim().length() > 0) {
            if (linesMap.containsKey(keyLine)) {
                linesMap.get(keyLine).add(line);
            } else {
                sousSequenceLines = new LinkedList<MPSLineRecord>();
                sousSequenceLines.add(line);
                linesMap.put(keyLine, sousSequenceLines);
            }
        }
    }
       
      
    private void buildSousSequence( List<MPSLineRecord> lineRecordMPS,  SequenceSMP sequenceSMP, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

          SousSequenceSMP sousSequenceSMP = new SousSequenceSMP();
          MPSLineRecord mPSLineRecord = lineRecordMPS.get(0);
          
          sousSequenceSMP.setProfondeur(Integer.MIN_VALUE);
          sousSequenceSMP.setSequenceSMP(sequenceSMP);
          
          sequenceSMP.getSousSequences().add(sousSequenceSMP);
          Map<String, List<MPSLineRecord>> mesuresMap = new HashMap<String, List<MPSLineRecord>>();
      }
     
     void buildMesure(final MPSLineRecord mesureLines, SousSequenceSMP sousSequenceSMP, List<ValeurSMP> valeursSMP,final VersionFile versionFile, 
            final SortedSet<MPSLineRecord> ligneEnErreur, ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException, InsertionDatabaseException, ParseException {
        
       // je parcours le fichier et je recupere les differentes valeurs de chaque parametre
      
        //Time heure = mesureLines.getHeure();
        String variable = mesureLines.getCodevariable();
        float valeur = mesureLines.getValeurvariable();
        String statut = mesureLines.getStatutvaleur();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();

        String datatype = versionFile.getDataset().getLeafNode().getDatatype().getCode();
      
        String cdatatype = Utils.createCodeFromString(datatype);

        String cunite = Utils.createCodeFromString(unite);

        String cmethode = Utils.createCodeFromString(methode);

        String chumidite = Utils.createCodeFromString(humidite);

        String cvariable = Utils.createCodeFromString(variable);
        
      
        
        VariablesPRO dbvariable = variPRODAO.betByKey(cvariable);
         if(dbvariable==null){
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSMP.BUNDLE_PATH_SMP, 
                     ProcessRecordSMP.MSG_ERROR_MPS_NOT_VARIABLEPRO_DB),cvariable)); 
         }
         
         Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite);
         if(dbunitepro==null){
              errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSMP.BUNDLE_PATH_SMP, 
                     ProcessRecordSMP.MSG_ERROR_MPS_NOT_UNITEPRO_DB),cunite)); 
         }
         
         Methode dbmethode = methodeDAO.getByMyKey(cmethode);
         if(dbmethode==null){
              errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSMP.BUNDLE_PATH_SMP, 
                     ProcessRecordSMP.MSG_ERROR_MPS_NOT_FOUND_METHODE_DB),cmethode)); 
         }
         
         HumiditeExpression dbhumidite = humiditeDAO.getByKEY(chumidite);
         if(dbhumidite==null){
              errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSMP.BUNDLE_PATH_SMP, 
                     ProcessRecordSMP.MSG_ERROR_MPS_NOT_FOUND_HUMIDITE_DB),chumidite)); 
         }
        

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getDVKeys(cdatatype,cvariable);

        if(dbdvum==null){

             errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSMP.BUNDLE_PATH_SMP, 

                     ProcessRecordSMP.MSG_ERROR_MSP_NOT_FOUND_DVU_DB),cdatatype, cvariable, cunite, cmethode, chumidite ));
        }

        Long fichier = mesureLines.getOriginalLineNumber();
        
        MesureSMP mesureSMP = new MesureSMP();
        
       // mesureSMP.setHeure(heure);
        mesureSMP.setSousSequenceSMP(sousSequenceSMP);
        mesureSMP.setLineNumber(0);
        mesureSMP.setValeursSMP(valeursSMP);
       
        
        final ValeurSMP valeurSMP = new  ValeurSMP();
        
            valeurSMP.setDatatypeVariableUnitePRO(dbdvum);
            valeurSMP.setLigneFichierEchange(fichier);
            valeurSMP.setMesureSMP(mesureSMP);
            valeurSMP.setStatutvaleur(statut);
            valeurSMP.setValeur_smp(valeur);
            valeurSMP.setNumRepetition(0);
                       
            mesureSMP.getValeursSMP().add(valeurSMP);
            
   } 
    
    
    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
           DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureSMPDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public IParcelleElementaireDAO getParcelleElementaireDAO() {
        return parcelleElementaireDAO;
    }

    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public JPAHumiditeExpression getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(JPAHumiditeExpression humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

   
    
   

}
