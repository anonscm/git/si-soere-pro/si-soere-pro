/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.listeitineraires.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.pro.extraction.apport.interfaces.IApportDAO;
import org.inra.ecoinfo.pro.extraction.listeitineraires.interfaces.IITKDatasetManager;
import org.inra.ecoinfo.pro.extraction.proetudie.interfaces.IProEtudierDAO;
import org.inra.ecoinfo.pro.extraction.recoltecoupe.interfaces.IRecolteCoupeDAO;
import org.inra.ecoinfo.pro.extraction.semisplantation.interfaces.ISemisPlantationDAO;
import org.inra.ecoinfo.pro.extraction.travaildusol.interfaces.ITravailDuSolDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.security.entity.Role;

/**
 *
 * @author adiankha
 */
public class ITKDatasetManager extends DefaultDatasetManager implements IITKDatasetManager {
    
     static final String         VARIABLE_RECOLTE_COUPE_PRIVILEGE_PATTERN = "%s/%s/*/recolte_coupe/%s";
     static final String         VARIABLE_TRAVAIL_SOL_PRIVILEGE_PATTERN = "%s/%s/*/travail_sol/%s";
     static final String         VARIABLE_APPORT_PRIVILEGE_PATTERN = "%s/%s/*/apport/%s";
     static final String         VARIABLE_PRO_ETUDIE_PRIVILEGE_PATTERN = "%s/%s/*/pro_etudie/%s";
      static final String         VARIABLE_SEMIS_PLANTATION_PRIVILEGE_PATTERN = "%s/%s/*/semis_plantation/%s";

    ITravailDuSolDAO travailDuSolDAO;
    IProEtudierDAO proEtudieDAO;
    IRecolteCoupeDAO recolteCoupeDAO;
    IApportDAO apportDAO;
    ISemisPlantationDAO semisDAO;

    @Override
    public List<Lieu> getDispositifAvailableLieu() {
        final List<Lieu> lieuAvailables;
        lieuAvailables = this.travailDuSolDAO.getAvailableTravailSolLieu();
        lieuAvailables.addAll(this.proEtudieDAO.getAvailableProEtudieLieu());
        lieuAvailables.addAll(this.recolteCoupeDAO.getAvailableRecolteCoupeLieu());
        lieuAvailables.addAll(this.apportDAO.getAvailableApport());
         lieuAvailables.addAll(this.semisDAO.getAvailableSemisPlantation());
        return lieuAvailables;
    }

    @Override
    public List<Dispositif> getAvailableDispositifs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Dispositif> retrievePhysicoChimieAvailablesDispositifsById(Long id) {
        final List<Dispositif> availablesDispositif = new LinkedList();
        final List<Dispositif> dispositif = this.travailDuSolDAO.getAvailablesDispositifsByLieuIdTravailSol(id);
        dispositif.addAll(this.proEtudieDAO.getAvailablesDispositifsByLieuIdProEtudie(id));
        dispositif.addAll(this.recolteCoupeDAO.getAvailablesDispositifsByLieuIdRecolteCoupe(id));
        dispositif.addAll(this.apportDAO.getAvailablesDispositifsByLieuIdApport(id));
        dispositif.addAll(this.semisDAO.getAvailablesDispositifsByLieuIdSemisPlantation(id));
        dispositif.stream().forEach((disps) -> {
            availablesDispositif.add(disps);
        });
        return availablesDispositif;
    }

    public void setTravailDuSolDAO(ITravailDuSolDAO travailDuSolDAO) {
        this.travailDuSolDAO = travailDuSolDAO;
    }

    public void setProEtudieDAO(IProEtudierDAO proEtudieDAO) {
        this.proEtudieDAO = proEtudieDAO;
    }

    public void setRecolteCoupeDAO(IRecolteCoupeDAO recolteCoupeDAO) {
        this.recolteCoupeDAO = recolteCoupeDAO;
    }

    public void setApportDAO(IApportDAO apportDAO) {
        this.apportDAO = apportDAO;
    }

    public void setSemisDAO(ISemisPlantationDAO semisDAO) {
        this.semisDAO = semisDAO;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesRecolteCoupeForDispositifs(Map<Long, Dispositif> availableDisps) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> variables = this.recolteCoupeDAO
                .getAvailablesVariablesByDispositifRecolteCoupe(new LinkedList(availableDisps.values()));
        if (variables == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : variables) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDisps.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        ITKDatasetManager.VARIABLE_RECOLTE_COUPE_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
             
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesTravailSolForDispositifs(Map<Long, Dispositif> availableDisps) {
       final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> variables = this.travailDuSolDAO
                .getAvailablesVariablesByDispositifTravailSol(new LinkedList(availableDisps.values()));
        if (variables == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : variables) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDisps.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        ITKDatasetManager.VARIABLE_TRAVAIL_SOL_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
             
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesApportForDispositifs(Map<Long, Dispositif> availableDisps) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> variables = this.apportDAO
                .getAvailablesVariablesByDispositifTravailSol(new LinkedList(availableDisps.values()));
        if (variables == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : variables) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDisps.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        ITKDatasetManager.VARIABLE_APPORT_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
             
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesSemisPlantationForDispositifs(Map<Long, Dispositif> availableDisps) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> variables = this.semisDAO
                .getAvailablesVariablesByDispositifSemeisPlantation(new LinkedList(availableDisps.values()));
        if (variables == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : variables) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDisps.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        ITKDatasetManager.VARIABLE_SEMIS_PLANTATION_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
             
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesProEtudieForDispositifs(Map<Long, Dispositif> availableDisps) {
       final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> variables = this.proEtudieDAO
                .getAvailablesVariablesByDispositifProEtudie(new LinkedList(availableDisps.values()));
        if (variables == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : variables) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDisps.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        ITKDatasetManager.VARIABLE_PRO_ETUDIE_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
             
        }
        return availablesVariables;
    }
    
    

}

