/**
 *
 */
package org.inra.ecoinfo.pro.refdata.modalite;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;
import org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.ITypeFacteurDAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


public class Recorder extends AbstractCSVMetadataRecorder<Modalite> 
{
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ITypeFacteurDAO typeFacteurDAO;
    IFacteurDAO facteurDAO;
    IModaliteDAO modaliteDAO;
    /*ITypeNomenclatureDAO typenomenclatureDAO;
    protected INomenclatureDAO nomenDAO;

    Map<String, String[]> nomenPossibles = new TreeMap<String, String[]>();
    */
    Map<String, String[]> facteursPossibles = new TreeMap<String, String[]>();
    //Boolean nKeyNomen = false;
   
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {
                Facteur facteur = null;
                Modalite modalite = null;
                      
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String libelleFacteur = tokenizerValues.nextToken();
                String valeur = tokenizerValues.nextToken();
                facteur = facteurDAO.getByLibelleFct(libelleFacteur);
                modalite = modaliteDAO.getByValeurFacteur(valeur, facteur);
                modaliteDAO.remove(modalite);
                
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return
     * @throws PersistenceException
     */
    private String[] getLibelleTypeFacteurPossibles() throws PersistenceException 
    {
        List<TypeFacteur> lstTypeFacteurs = typeFacteurDAO.getAll(TypeFacteur.class);
        
        String[] typeFacteurPossibles = new String[lstTypeFacteurs.size()];
        int index = 0;
        for (TypeFacteur typeFacteur : lstTypeFacteurs) 
        {
            typeFacteurPossibles[index++] = typeFacteur.getLibelle();
        }
        return typeFacteurPossibles;
    }
    
    /**
     * @return
     * @throws PersistenceException
     */
    private Map<String, String[]> getLibelleFacteurPossibles() throws PersistenceException 
    {
        List<TypeFacteur> lstTypeFacteurs = typeFacteurDAO.getAll(TypeFacteur.class);

        for (TypeFacteur typeFacteur : lstTypeFacteurs) 
        {
            List<Facteur> lstFacteurs = facteurDAO.getLstFacteurByTPFacteur(typeFacteur);
            String[] tpFacteurPossibles = new String[lstFacteurs.size()];
            int index = 0;
            for (Facteur facteur : lstFacteurs)
            {
                tpFacteurPossibles[index++] = facteur.getLibelle();
            }

            facteursPossibles.put(typeFacteur.getLibelle(), tpFacteurPossibles);
        }

        return facteursPossibles;
    }
    
    /**
     * @return
     * @throws PersistenceException
     */
    /*private String[] getTypeNomenclaturePossibles() throws PersistenceException 
    {
        List<TypeNomenclature> lstTypeNomens = typenomenclatureDAO.getAll(TypeNomenclature.class);
        
        String[] lstTypeNomenPossibles = new String[lstTypeNomens.size() + 1];
        lstTypeNomenPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (TypeNomenclature typeNomenclature : lstTypeNomens) 
        {
        	lstTypeNomenPossibles[index++] = typeNomenclature.getTn_nom();
        }
        
        return lstTypeNomenPossibles;
    }
    */
    
    /**
     * @throws PersistenceException
     */
    /*private Map<String, String[]> getNomenclaturePossibles() throws PersistenceException 
    {
    	List<TypeNomenclature> lstTypeNomens = typenomenclatureDAO.getAll(TypeNomenclature.class);
    	
    	if(lstTypeNomens != null)
    	{
    		for (TypeNomenclature typeNomenclature : lstTypeNomens) 
    		{
    			List<Nomenclatures> lstNomens = nomenDAO.getByTypeNomenclature(typeNomenclature);
    			String[] lstNomenPossibles = new String[lstNomens.size() + 1];
    			lstNomenPossibles[0] = "";
    			int index = 1;
    			for (Nomenclatures nomenclatures : lstNomens) 
    			{
    				lstNomenPossibles[index++] = nomenclatures.getNomen_nom();
    			}
    			
    			nomenPossibles.put(typeNomenclature.getTn_nom(), lstNomenPossibles);
    		}
    	}
        
        return nomenPossibles;
    }
    */
    
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Modalite> getAllElements() throws PersistenceException 
    {
        return modaliteDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Modalite modalite) throws PersistenceException 
    {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        //type de facteur
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(modalite == null ? 
        		Constantes.STRING_EMPTY : modalite.getFacteur().getTypeFacteur().getLibelle(), getLibelleTypeFacteurPossibles(), null, true, false, true);
        
        //facteur
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(modalite == null ? 
        		Constantes.STRING_EMPTY : modalite.getFacteur().getLibelle(), getLibelleFacteurPossibles(), null, true, false, true);
        
        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();
        
        refsColonne1.add(colonne2);
        
        colonne1.setRefs(refsColonne1);
        colonne2.setValue(modalite == null ? new String("") : modalite.getFacteur() == null ? new String("") : modalite.getFacteur().getLibelle());
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);
        
        /*Boolean isAssocFacteur = facteurDAO.getByLibelleFct(colonne2.getValue()) == null ? false : facteurDAO.getByLibelleFct(colonne2.getValue()).getIsAssocNomenclature();
        nKeyNomen = isAssocFacteur ? true : false;
        //valeur de la modalité
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(modalite == null ? Constantes.STRING_EMPTY : modalite.getValeur(), 
        		ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, !nKeyNomen, false, false));
        */
        //valeur de la modalité
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(modalite == null ? Constantes.STRING_EMPTY : modalite.getValeur(), 
        		ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        
        //Type de nomenclature
        /*ColumnModelGridMetadata colonne3 = new ColumnModelGridMetadata(modalite == null ? Constantes.STRING_EMPTY : 
            		(modalite.getTypeNomenclatures() == null ? Constantes.STRING_EMPTY : 
            			(modalite.getTypeNomenclatures().getTn_nom() != null ? modalite.getTypeNomenclatures().getTn_nom() : Constantes.STRING_EMPTY)), 
            			getTypeNomenclaturePossibles(), null, false, false, false);
        
        //Nomenclature
        ColumnModelGridMetadata colonne4 = new ColumnModelGridMetadata(modalite == null ? Constantes.STRING_EMPTY : 
            		(modalite.getNomenclatures() == null ? Constantes.STRING_EMPTY : 
            			(modalite.getNomenclatures().getNomen_nom() != null ? modalite.getNomenclatures().getNomen_nom() : Constantes.STRING_EMPTY)), 
            			getNomenclaturePossibles(), null, nKeyNomen, false, false);
        
        List<ColumnModelGridMetadata> refsColonne2 = new LinkedList<ColumnModelGridMetadata>();
        
        refsColonne2.add(colonne4);

        colonne3.setRefs(refsColonne2);
        colonne4.setValue(modalite == null ? new String("") : 
    		(modalite.getNomenclatures() == null ? new String("") : 
    			(modalite.getNomenclatures().getNomen_nom() != null ? modalite.getNomenclatures().getNomen_nom() : new String(""))));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne3);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne4);
        */
        
        //Commentaire
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(modalite == null || modalite.getCommentaire() == null ? Constantes.STRING_EMPTY : modalite.getCommentaire(), 
                		ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }
 
    /**
     * @param errorsReport
     * @param facteur
     * @param valeur
     * @param nomen
     * @param commentaire
     * @throws PersistenceException
     * @throws BusinessException
     */
    private void createOrupdateModalite (ErrorsReport errorsReport, Facteur facteur, String valeur, String commentaire) throws PersistenceException, BusinessException 
    {
        Modalite modalite = new Modalite(facteur, valeur, commentaire); //new Modalite(facteur, typeNomenclature, nomen, valeur, commentaire);
        
        Modalite dbModalite = modaliteDAO.getByValeurFacteur(valeur, facteur);

        if (dbModalite != null)
        {
        	dbModalite.setFacteur(facteur);
        	dbModalite.setValeur(valeur);
        	/*dbModalite.setTypeNomenclatures(typeNomenclature);
        	dbModalite.setNomenclatures(nomen);
        	*/
        	dbModalite.setCommentaire(commentaire);
        	modaliteDAO.saveOrUpdate(dbModalite);
        }
        else
        	modaliteDAO.saveOrUpdate(modalite);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try 
        {
            skipHeader(parser);
            String[] values = null;
            values = parser.getLine();
            while (values != null) 
            {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Modalite.TABLE_NAME);
                
                int indexTypeFac = tokenizerValues.currentTokenIndex();
                String libelleTypeFacteur = tokenizerValues.nextToken();
                int indexFac = tokenizerValues.currentTokenIndex();
                String libelleFacteur = tokenizerValues.nextToken();
                //int indexValeur = tokenizerValues.currentTokenIndex();
                String valeur = tokenizerValues.nextToken();
                /*int indexTypeNomen = tokenizerValues.currentTokenIndex();
                String nomTypeNomen = tokenizerValues.nextToken();
                int indexNomen = tokenizerValues.currentTokenIndex();
                String nomNomen = tokenizerValues.nextToken();
                */
                String commentaire = tokenizerValues.nextToken();
                
                verifieTypeFacteur(libelleTypeFacteur, line+1, indexTypeFac+1, errorsReport);
                Facteur facteur = verifieFacteur(libelleFacteur, line+1, indexFac+1, errorsReport);
                /*verifieValeurEtNomNomenclature(facteur, valeur, nomTypeNomen, nomNomen, line+1, indexValeur+1, indexNomen+1, errorsReport);
                TypeNomenclature typeNomen = verifieTypeNomenclatures(nomTypeNomen, line+1, indexTypeNomen+1, errorsReport);
                Nomenclatures nomen = verifieNomenclatures(nomNomen, line+1, indexNomen+1, errorsReport);
                */
                
                String valeurModalite = (valeur == null) ? "" : valeur;
                
                if (!errorsReport.hasErrors()) 
                {
                	createOrupdateModalite(errorsReport, facteur, valeurModalite, commentaire);
                }
                
                values = parser.getLine();
            }

            if (errorsReport.hasErrors()) 
            {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } 
        catch (IOException e1) 
        {
            throw new BusinessException(e1.getMessage(), e1);
        } 
        catch (PersistenceException e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    /**
     * @param libelleTypeFacteur
     * @param line
     * @param index
     * @param errorsReport
     * @throws PersistenceException
     */
    private void verifieTypeFacteur(String libelleTypeFacteur, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
    	TypeFacteur typeFacteur = typeFacteurDAO.getByLibelle(libelleTypeFacteur);
    	if (typeFacteur == null) 
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TYPEFACTEUR_NONDEFINI"),line, index, libelleTypeFacteur));
    	}
    }
    
    /**
     * @param libelleFacteur
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws PersistenceException
     */
    private Facteur verifieFacteur(String libelleFacteur, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
    	Facteur facteur = facteurDAO.getByLibelleFct(libelleFacteur);
    	if (facteur == null) 
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "FACTEUR_NONDEFINI"),line, index, libelleFacteur));
    	}
    	
    	return facteur;
    }
    
    /**
     * @param valeur
     * @param nomenclature
     * @param line
     * @param index1
     * @param index2
     * @param errorsReport
     */
    /*private void verifieValeurEtNomNomenclature(Facteur facteur, String valeur, String typeNomenclature, String nomenclature, long line, int index1, int index2, ErrorsReport errorsReport)
    {
    	if (valeur == null && nomenclature == null || 
    			(valeur !=null && valeur.isEmpty() && nomenclature != null && nomenclature.isEmpty()))
    	{
    		typeNomenclature = null;
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(
    				BUNDLE_SOURCE_PATH, "VALEURNOMEN_NONDEFINI"),line, index1, index2, datasetDescriptor.getColumns().get(2).getName(), datasetDescriptor.getColumns().get(3).getName()));
    	}
    	
    	if (valeur != null && nomenclature != null && !valeur.isEmpty() && !nomenclature.isEmpty())
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(
    				BUNDLE_SOURCE_PATH, "VALEURNOMENCLATURE_FALSE"),line, index1, index2, datasetDescriptor.getColumns().get(2).getName(), datasetDescriptor.getColumns().get(3).getName()));
    	}
    	
    	if(facteur != null && nomenclature != null && !nomenclature.isEmpty() && !facteur.getIsAssocNomenclature())
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(
    				BUNDLE_SOURCE_PATH, "VALEURNOMENCLATURE_ERRONE"),line, index2, facteur.getLibelle()));
    	}
    		
    }
    */
    
    /**
     * @param nomTypeNomen
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws PersistenceException
     */
    /*private TypeNomenclature verifieTypeNomenclatures(String nomTypeNomen, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
    	TypeNomenclature dbTypeNomen = null;
        if(nomTypeNomen != null && !nomTypeNomen.isEmpty()) //inutile de chercher avec un nom vide
        {	
        	String code = Utils.createCodeFromString(nomTypeNomen);
        	dbTypeNomen = typenomenclatureDAO.getByCode(code);
        
        	if(dbTypeNomen == null)
        	{
        		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TYPENOMENCLATURE_NONDEFINI"), line, index, nomTypeNomen));
        	}
        }
        
        return dbTypeNomen;
    }
    */
    
    /**
     * @param nomnomen
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws PersistenceException
     */
    /*private Nomenclatures verifieNomenclatures(String nomnomen, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
    	Nomenclatures dbnomen = null;
        if(nomnomen != null && !nomnomen.isEmpty()) //inutile de chercher avec un nom vide
        {	
        	dbnomen = nomenDAO.getByName(nomnomen);
        
        	if(dbnomen == null)
        	{
        		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "NOMENCLATURE_NONDEFINI"), line, index, nomnomen));
        	}
        }
        
        return dbnomen;
    }
    */

	/**
	 * @param typeFacteurDAO the typeFacteurDAO to set
	 */
	public void setTypeFacteurDAO(ITypeFacteurDAO typeFacteurDAO) {
		this.typeFacteurDAO = typeFacteurDAO;
	}

	/**
	 * @param facteurDAO the facteurDAO to set
	 */
	public void setFacteurDAO(IFacteurDAO facteurDAO) {
		this.facteurDAO = facteurDAO;
	}

	/**
	 * @param modaliteDAO the modaliteDAO to set
	 */
	public void setModaliteDAO(IModaliteDAO modaliteDAO) {
		this.modaliteDAO = modaliteDAO;
	}
	
	/**
	 * @param typenomenclatureDAO the typenomenclatureDAO to set
	 */
	/*public void setTypenomenclatureDAO(ITypeNomenclatureDAO typenomenclatureDAO) {
		this.typenomenclatureDAO = typenomenclatureDAO;
	}
	*/

	/**
	 * @param nomenDAO the nomenDAO to set
	 */
	/*public void setNomenDAO(INomenclatureDAO nomenDAO) {
		this.nomenDAO = nomenDAO;
	}
	*/

}
