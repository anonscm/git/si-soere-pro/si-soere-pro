/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureProEtudie.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                                                MesureProEtudie.ATTRIBUTE_JPA_DISPOSITIF,
                                                                                                DescriptionTraitement.ID_JPA,
                                                                                                MesureProEtudie.ATTRIBUTE_JPA_PARCELLE,
                                                                                                MesureProEtudie.ATTRIBUTE_JPA_DATE_DEBUT,
                                                                                                 MesureProEtudie.ATTRIBUTE_JPA_PLACETTE,
                                                                                                 MesureProEtudie.ATTRIBUTE_TYPEPRO,
                                                                                                 MesureProEtudie.ATTRIBUTE_JPA_PRODUIT}))

public class MesureProEtudie implements Serializable, ISecurityPath {

    public static final String ATTRIBUTE_JPA_ID = "mesure_id";
    public static final String ATTRIBUTE_JPA_DISPOSITIF = "codedispositif";
   // public static final String ATTRIBUTE_JPA_TRAITEMENT = "codetraitement";
    public static final String ATTRIBUTE_JPA_PARCELLE = "nomparcelle";
    public static final String ATTRIBUTE_JPA_PLACETTE = "nomplacette";
    public static final String ATTRIBUTE_JPA_CULTURE = "nomculture";
    public static final String ATTRIBUTE_TYPEPRO = "typeproduit";
    public static final String ATTRIBUTE_JPA_PRODUIT = "codeproduit";
    public static final String ATTRIBUTE_JPA_DATE_DEBUT = "datedebut";
    public static final String ATTRIBUTE_JPA_DATE_FIN = "datefin";
    public static final String ATTRIBUTE_PRECISION_STADE = "precisionstade";
    public static final String ATTRIBUTE_JPA_BBCH = "codebbch";
    public static final String ATTRIBUTE_COMMENTAIRE = "commentaire";
    public static final String ATTRIBUTE_MATERIEL = "materielapport";
    public static final String ATTRIBUTE_JPA_DATE_ENFOUISSEMENT = "dateenfouissement";
    public static final String ATTRIBUTE_TYPEOBSERVATION = "typeobservation";
     public static final String ATTRIBUTE_NIVEAU = "niveauatteint";
    public static final String ATTRIBUTE_NOMOBSERVATION = "nomobservation";
     public static final String ATTRIBUTE_KEYMESURE = "keymesure";

    public static final String TABLE_NAME = "mesureinterventionproetudie";

    static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(MesureProEtudie.class.getName());


    @Id
    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long mesure_id;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_DISPOSITIF, nullable = false)
    private String codedispositif;

    @Column(name = MesureProEtudie.ATTRIBUTE_KEYMESURE, nullable = false,unique = true)
    private String keymesure;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_PARCELLE, nullable = false)
    private String nomparcelle;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_PLACETTE, nullable = false)
    private String nomplacette;
    
    @Column(name = MesureProEtudie.ATTRIBUTE_TYPEPRO, nullable = false)
    private String typeproduit;
    
    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_PRODUIT, nullable = false)
    private String codeproduit;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_DATE_DEBUT, nullable = false)
    @Temporal(TemporalType.DATE)
    Date datedebut;

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_DATE_FIN, nullable = true)
    @Temporal(TemporalType.DATE)
    Date datefin;
    
    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_BBCH, nullable = false)
    private String codebbch;
    
    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_CULTURE, nullable = false)
    private String nomculture;
    
    @Column(name = MesureProEtudie.ATTRIBUTE_TYPEOBSERVATION, nullable = false)
    private String typeobservation;
    
    @Column(name = MesureProEtudie.ATTRIBUTE_NOMOBSERVATION, nullable = false)
    private String nomobservation;

    @Column(name = MesureProEtudie.ATTRIBUTE_COMMENTAIRE, length = 300)
    private String commentaire = "";

    @Column(name = MesureProEtudie.ATTRIBUTE_MATERIEL, length = 300)
    private String materielapport = "";

    @Column(name = MesureProEtudie.ATTRIBUTE_JPA_DATE_ENFOUISSEMENT, nullable = true)
    @Temporal(TemporalType.DATE)
    Date dateenfouissement;

    @Column(name = MesureProEtudie.ATTRIBUTE_PRECISION_STADE, nullable = false)
    private String precisionstade = "";
    
     @Column(name = MesureProEtudie.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint = "";

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    VersionFile versionfile;
    
     @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = DescriptionTraitement.class)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, referencedColumnName = DescriptionTraitement.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
     DescriptionTraitement descriptionTraitement;

    @OneToMany(mappedBy = "mesureinterventionproetudie", cascade = ALL)
    List<ValeurProEtudie> valeurproetudie = new LinkedList();
    public MesureProEtudie() {
        super();
    }

    public MesureProEtudie(String codedispositif, DescriptionTraitement traitement, String nomparcelle, String nomplacette, String typeproduit, String codeproduit,
            Date datedebut, Date datefin, String codebbch, String nomculture, String typeobservation, String nomobservation, String precisionstade,String materiel,String niveau,
            Date dateenfouissement, VersionFile versionfile,String commentaire) {
        this.codedispositif = codedispositif;
        this.descriptionTraitement = traitement;
        this.nomparcelle = nomparcelle;
        this.nomplacette = nomplacette;
        this.typeproduit = typeproduit;
        this.codeproduit = codeproduit;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.codebbch = codebbch;
        this.nomculture = nomculture;
        this.typeobservation = typeobservation;
        this.nomobservation = nomobservation;
        this.niveauatteint = niveau;
        this.dateenfouissement = dateenfouissement;
        this.versionfile = versionfile;
        this.precisionstade = precisionstade;
        this.materielapport = materiel;
        this.commentaire = commentaire;
    }


    public List<ValeurProEtudie> getValeurproetudie() {
        return valeurproetudie;
    }

    public void setValeurproetudie(List<ValeurProEtudie> valeurproetudie) {
        this.valeurproetudie = valeurproetudie;
    }

   
    @Override
    public Date getDatePrelevement() {
        return this.datedebut;
    }

    @Override
    public String getSecurityPath() {
        return String.format(MesureProEtudie.RESOURCE_PATH,
                 this.getVersionfile().getDataset().getLeafNode().getDatatype().getCode());
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeurproetudie.clear();
        for (final ISecurityPathWithVariable child : children) {
            this.valeurproetudie.add((ValeurProEtudie) child);
        }
    }

    public Long getMesure_id() {
        return mesure_id;
    }

    public void setMesure_id(Long mesure_id) {
        this.mesure_id = mesure_id;
    }

    public String getCodedispositif() {
        return codedispositif;
    }

    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    public DescriptionTraitement getDescriptionTraitement() {
        return descriptionTraitement;
    }

    public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    public String getNiveauatteint() {
        return niveauatteint;
    }

    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }


    public String getNomparcelle() {
        return nomparcelle;
    }

    public void setNomparcelle(String nomparcelle) {
        this.nomparcelle = nomparcelle;
    }

    public String getNomplacette() {
        return nomplacette;
    }

    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    public String getTypeproduit() {
        return typeproduit;
    }

    public void setTypeproduit(String typeproduit) {
        this.typeproduit = typeproduit;
    }

    public String getCodeproduit() {
        return codeproduit;
    }

    public void setCodeproduit(String codeproduit) {
        this.codeproduit = codeproduit;
    }

    public Date getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(Date datedebut) {
        this.datedebut = datedebut;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public String getCodebbch() {
        return codebbch;
    }

    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    public String getNomculture() {
        return nomculture;
    }

    public void setNomculture(String nomculture) {
        this.nomculture = nomculture;
    }

    public String getTypeobservation() {
        return typeobservation;
    }

    public void setTypeobservation(String typeobservation) {
        this.typeobservation = typeobservation;
    }

    public String getNomobservation() {
        return nomobservation;
    }

    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getMaterielapport() {
        return materielapport;
    }

    public void setMaterielapport(String materielapport) {
        this.materielapport = materielapport;
    }

    public Date getDateenfouissement() {
        return dateenfouissement;
    }

    public void setDateenfouissement(Date dateenfouissement) {
        this.dateenfouissement = dateenfouissement;
    }

    public String getPrecisionstade() {
        return precisionstade;
    }

    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    public VersionFile getVersionfile() {
        return versionfile;
    }

    public void setVersionfile(VersionFile versionfile) {
        this.versionfile = versionfile;
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getKeymesure() {
        return keymesure;
    }

    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }
    
    

}
