
package org.inra.ecoinfo.pro.refdata.typesolarvalis;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author adiankha
 */
public class RecorderTypeSolArvalisTest {
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    @AfterClass
    public static void tearDownClass() {
    }
    
     MockUtils m = MockUtils.getInstance();
    Recorder instance;
    String encoding = "UTF-8";

    @Mock
    Properties propertiesNom;
    
    @Mock
    Typesolarvalis tsa1;
    @Mock
    Typesolarvalis tsa2;
    public RecorderTypeSolArvalisTest() {
    }
    
    @Before
    public void setUp() {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setTsarvalisDAO(m.tsarvalisDAO);
        instance.setLocalizationManager(m.localizationManager);
        Mockito.when(m.localizationManager.newProperties(Typesolarvalis.NAME_ENTITY_JPA, Typesolarvalis.JPA_COLUMN_NAME, Locale.ENGLISH)).thenReturn(propertiesNom);
        Mockito.when(m.typesolarvalis.getNom()).thenReturn(MockUtils.TYPESOLARVALIS);
        Mockito.when(propertiesNom.getProperty(MockUtils.TYPESOLARVALIS)).thenReturn("tsarvalisEN");
    }
    
     @Test
      public void testGetMPDAO() {
        System.out.println("setprocessDAO");
        instance = Mockito.spy(new Recorder());
        Assert.assertNull("heteroDAO not null", instance.getTsarvalisDAO());
        instance.setTsarvalisDAO(m.tsarvalisDAO);
        Assert.assertEquals("methodeprocessDAO not setted", m.tsarvalisDAO, instance.getTsarvalisDAO());
    }
    
     @Test
       public void testGetNewLineModelGridMetadata() throws Exception {
        System.out.println("getNewLineModelGridMetadata");
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.typesolarvalis);
        Assert.assertTrue(2 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("typesolarvalis", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("tsarvalisEN", result.getColumnModelGridMetadataAt(1).getValue());
    }

    @Test
      public void testInitModelGridMetadata() throws PersistenceException {
        System.out.println("initModelGridMetadata");
        m.localizationManager = Mockito.spy(m.localizationManager);
        instance.setLocalizationManager(m.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.localizationManager).newProperties(Typesolarvalis.NAME_ENTITY_JPA, Typesolarvalis.JPA_COLUMN_NAME, Locale.ENGLISH);
        Assert.assertEquals("propertiesNom not initialized", propertiesNom, instance.getTsarvalisEN());
    }
    
    @Test
    public void testGetAllElements() throws Exception {
        System.out.println("getAllElements");
        List<Typesolarvalis> arvalis = Arrays.asList(new Typesolarvalis[]{tsa1,tsa2});
        Mockito.when(m.tsarvalisDAO.getAll()).thenReturn(arvalis);
        List<Typesolarvalis> heteroDb = instance.getAllElements();
        Assert.assertEquals(arvalis, heteroDb);
        Assert.assertEquals(arvalis.get(0), tsa1);
        Assert.assertEquals(arvalis.get(1), tsa2);    
    }
    
     @Test
    public void testProcessRecord() throws Exception {
        System.out.println("processRecord");
        Typesolarvalis hetero = Mockito.mock(Typesolarvalis.class);
        Mockito.when(hetero.getCode()).thenReturn("typesolarvalis");
        Mockito.when(hetero.getNom()).thenReturn("typesolarvalis");
        String text = "nom_fr;nom_en\n" + "typesolarvalis;nom_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        ArgumentCaptor<Typesolarvalis> ts = ArgumentCaptor.forClass(Typesolarvalis.class);

        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.tsarvalisDAO).saveOrUpdate(ts.capture());
        Mockito.verify(m.typesolarvalis).setNom("typesolarvalis");

        // existing etape
        Mockito.when(m.tsarvalisDAO.getByName("typesolarvalis")).thenReturn(null);
        ts = ArgumentCaptor.forClass(Typesolarvalis.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.tsarvalisDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertNotEquals(m.typesolarvalis, ts.getValue());
        Assert.assertEquals("typesolarvalis", ts.getValue().getCode());
        Assert.assertEquals("typesolarvalis", ts.getValue().getNom());
    }
    
     @Test
    public void testDeleteRecorder() throws Exception {
        System.out.println("deleteRecord");
        String text = "Typesolarvalis;nom_fr;nom_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        Mockito.when(m.tsarvalisDAO.getByName("typesolarvalis")).thenReturn(m.typesolarvalis);
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.tsarvalisDAO).remove(m.typesolarvalis);

        // Persistence exception on getByCode
        text = "Typesolarvalis;nom_fr;nom_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(m.tsarvalisDAO.getByName("typesolarvalis")).thenThrow(new PersistenceException("error"));
        BusinessException error = null;
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error", error.getMessage());
        error = null;

        // Persistence exception on remove
        text = "Typesolarvalis;nom_fr;nom_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doReturn(m.typesolarvalis).when(m.tsarvalisDAO).getByName("typesolarvalis");
        Mockito.when(m.tsarvalisDAO.getByName("typesolarvalis")).thenReturn(m.typesolarvalis);
        Mockito.doThrow(new PersistenceException("error2")).when(m.tsarvalisDAO).remove(m.typesolarvalis);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }
    
    
}
