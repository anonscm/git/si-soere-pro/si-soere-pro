/*
 *
 */
package org.inra.ecoinfo.pro.refdata.etape;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;
public class Recorder extends AbstractCSVMetadataRecorder<Etapes> {
    protected IEtapeDAO etapeDAO;
    private Properties EtapenomEN;
    private Properties commentEn;
    
    private void createEtape(final Etapes etapes) throws PersistenceException {
        etapeDAO.saveOrUpdate(etapes);
        etapeDAO.flush();
    }
    private void createOrUpdateEtapes(final String code,String libelle,String commentaire, final Etapes dbetapes) throws PersistenceException {
        if (dbetapes == null) {
             final Etapes etapes = new Etapes(libelle,commentaire);
             etapes.setEtape_code(code);
             etapes.setEtape_intitule(libelle);
             etapes.setCommentaire(commentaire);
            createEtape(etapes);
        } else {
            updateBDEtapes(code, libelle, commentaire, dbetapes);
    }}
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final Etapes dbEtape = etapeDAO.getByIntitule(intitule);
                if (dbEtape != null) {
                    etapeDAO.remove(dbEtape);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Etapes> getAllElements() throws PersistenceException {
        return etapeDAO.getAll();
    }
    public IEtapeDAO getEtapeDAO() {
        return etapeDAO;
    }
    public Properties getEtapenomEN() {
        return EtapenomEN;
    }
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Etapes etapes) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(etapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        etapes.getEtape_intitule(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapes == null || etapes.getEtape_intitule() == null ? 
                        AbstractCSVMetadataRecorder.EMPTY_STRING : EtapenomEN.getProperty(etapes.getEtape_intitule()),
                                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(etapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        etapes.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapes == null || etapes.getCommentaire()== null ? 
                        AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(etapes.getCommentaire()),
                                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }
    @Override
    protected ModelGridMetadata<Etapes> initModelGridMetadata() {
        EtapenomEN = localizationManager.newProperties(Etapes.NAME_ENTITY_JPA, Etapes.JPA_COLUMN_INTITULE, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(Etapes.NAME_ENTITY_JPA, Etapes.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void persistEtapes( final String code, final String intitule,String commentaire) throws PersistenceException, BusinessException {
        final Etapes dbetapes = etapeDAO.getByIntitule(intitule);
        createOrUpdateEtapes(code, intitule, commentaire, dbetapes);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Etapes.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistEtapes(code, intitule, commentaire);
                }
                values = parser.getLine();
            }

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }
    public void setEtapeDAO(IEtapeDAO etapeDAO) {
        this.etapeDAO = etapeDAO;
    }
    public void setEtapenomEN(Properties etapenomEN) {
        EtapenomEN = etapenomEN;
    }
    private void updateBDEtapes(final String code,String libelle,String commentaire, final Etapes dbetapes) throws PersistenceException {
        dbetapes.setEtape_code(code);
        dbetapes.setEtape_intitule(libelle);
        dbetapes.setCommentaire(commentaire);
        etapeDAO.saveOrUpdate(dbetapes);

    }

}
