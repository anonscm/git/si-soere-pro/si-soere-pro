/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.dataset.security.ISecurityDatasetManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.IPhysicoChimieDatasetManager;
import org.inra.ecoinfo.pro.extraction.plantebrute.interfaces.IPlanteBrutDAO;
import org.inra.ecoinfo.pro.extraction.plantemoyenne.interfaces.IPlanteMoyenneDAO;
import org.inra.ecoinfo.pro.extraction.produitbrute.interfaces.IProduitBrutesDAO;

import org.inra.ecoinfo.pro.extraction.produitmoyenne.interfaces.IProduitMoyenneDAO;
import org.inra.ecoinfo.pro.extraction.solbrute.interfaces.ISolBrutDAO;
import org.inra.ecoinfo.pro.extraction.solmoyenne.interfaces.ISolMoyenneDAO;
import org.inra.ecoinfo.pro.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.pro.extraction.vo.VariableVO;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.pro.refdata.categorievariable.ICategorieVariableDAO;
import org.inra.ecoinfo.pro.refdata.categorievariable.IGroupeVariableBuilder;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.security.entity.Role;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.BeansException;

/**
 *
 * @author adiankha
 */
public class PhysicohimieDatasetManager extends DefaultDatasetManager implements IPhysicoChimieDatasetManager, BeanFactoryAware {

    static final String DISPOSITIF_PRODUIT_BRUTE_PRIVILEGE_PATTERN = "%s/*/produit_brute/*";

    static final String DISPOSITIF_PRODUIT_MOYENNE_PRIVILEGE_PATTERN = "%s/*/produit_moyennes/*";

    static final String DISPOSITIF_SOL_BRUT_PRIVILEGE_PATTERN = "%s/*/sol_brut/*";

    static final String DISPOSITIF_SOL_MOYENNE_PRIVILEGE_PATTERN = "%s/*/sol_moyennes/*";

    static final String VARIABLE_PRODUIT_BRUTE_PRIVILEGE_PATTERN = "%s/%s/*/produit_brute/%s";

    static final String DISPOSITIF_PLANTE_MOYENNE_PRIVILEGE_PATTERN = "%s/*/plante_moyennes/*";

    static final String VARIABLE_PLANTE_BRUTE_PRIVILEGE_PATTERN = "%s/*/plante_brute/%s";

    IProduitBrutesDAO produitBrutDAO;
    IProduitMoyenneDAO produitMoyenneDAO;
    ISolBrutDAO solBrutDAO;
    ISolMoyenneDAO solmoyenneDAO;
    IPlanteBrutDAO planteBrutDAO;
    ICategorieVariableDAO categorieVariableDAO;
    protected IVariableDAO variableDAO;
    protected BeanFactory beanFactory;
    IVariablesPRODAO variPRODAO;
    IPlanteMoyenneDAO planteMoyenneDAO;

    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;

    public PhysicohimieDatasetManager() {
        super();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Lieu> getDispositifAvailableLieu() {
        final List<Lieu> lieuAvailables;
        lieuAvailables = this.produitBrutDAO.getAvailableLieu();
        return lieuAvailables;
    }

    @Override
    public List<Dispositif> getAvailableDispositifs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Dispositif> retrievePhysicoChimieAvailablesDispositifsById(Long id) {
        final List<Dispositif> availablesDispositif = new LinkedList();
        final List<Dispositif> dispositif = this.produitBrutDAO.getAvailablesDispositifsByLieuId(id);
        for (final Dispositif disps : dispositif) {
            if (this.securityContext.isRoot()
                    || this.securityContext.matchPrivilege(String.format(
                                    PhysicohimieDatasetManager.DISPOSITIF_PRODUIT_BRUTE_PRIVILEGE_PATTERN, disps
                                    .getLieu().getNom(), disps.getCode()),
                            Role.ROLE_EXTRACTION, ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {

                availablesDispositif.add(disps);
            }
        }
        return availablesDispositif;
    }

    public void setProduitBrutDAO(IProduitBrutesDAO produitBrutDAO) {
        this.produitBrutDAO = produitBrutDAO;
    }

    public void setProduitMoyenneDAO(IProduitMoyenneDAO produitMoyenneDAO) {
        this.produitMoyenneDAO = produitMoyenneDAO;
    }

    public void setSolmoyenneDAO(ISolMoyenneDAO solmoyenneDAO) {
        this.solmoyenneDAO = solmoyenneDAO;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesProduitBruteForDispositifs(Map<Long, Dispositif> availableDispositif) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> datatypeVariablePRO = this.produitBrutDAO
                .getAvailablesVariablesByDispositifProduitBrut(
                        new LinkedList(availableDispositif.values()));
        if (datatypeVariablePRO == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : datatypeVariablePRO) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDispositif.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        PhysicohimieDatasetManager.VARIABLE_PRODUIT_BRUTE_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailblesVariablesProduitMoyenneForDispositfs(Map<Long, Dispositif> availableDispositif) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> datatypeVariablePRO = this.produitMoyenneDAO.getAvailablesVariablesByDispositifProduitMoyennes(
                new LinkedList(availableDispositif.values()));
        if (datatypeVariablePRO == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : datatypeVariablePRO) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDispositif.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        PhysicohimieDatasetManager.DISPOSITIF_PRODUIT_MOYENNE_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailblesVariablesSolBruteForDispositfs(Map<Long, Dispositif> availableDispositif) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> datatypeVariablePRO = this.solBrutDAO.getAvailablesVariablesByDispositifSolBrut(
                new LinkedList(availableDispositif.values()));
        if (datatypeVariablePRO == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : datatypeVariablePRO) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDispositif.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        PhysicohimieDatasetManager.DISPOSITIF_SOL_BRUT_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailblesVariablesSolMoyenneForDispositfs(Map<Long, Dispositif> availableDispositif) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> datatypeVariablePRO = this.solmoyenneDAO.getAvailablesVariablesByDispositifSolMoyenne(
                new LinkedList(availableDispositif.values()));
        if (datatypeVariablePRO == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : datatypeVariablePRO) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDispositif.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        PhysicohimieDatasetManager.DISPOSITIF_SOL_BRUT_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
        }
        return availablesVariables;
    }

    private IGroupeVariableBuilder getGroupeVariableBuilder() {
        return (IGroupeVariableBuilder) beanFactory.getBean(IGroupeVariableBuilder.BEAN_ID);

    }

    @Override
    public List<CategorieVariable> getCategorieVariableAvailbble() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<GroupeVariableVO> retrieveGroupesVariables(List<Long> dispositifId) throws BusinessException {
        try {
            List<VariablesPRO> variables = produitBrutDAO.retrieveProduitBrutAvailablesVariables(dispositifId);
            List<GroupeVariableVO> groupesVariables = getGroupeVariableBuilder().build(variables);
            return groupesVariables;
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    protected void print(List<GroupeVariableVO> groupesVariables, String tabs) {
        tabs = tabs + "   ";
        for (GroupeVariableVO groupeVariable : groupesVariables) {
            System.out.println(tabs + groupeVariable.getCode());

            if (groupeVariable.getChildren().size() > 0) {
                print(groupeVariable.getChildren(), tabs);
            } else {
                for (VariableVO variable : groupeVariable.getVariables()) {
                    System.out.println(tabs + "   " + "variable:" + variable.getCode());
                }
            }
        }
    }

    public ICategorieVariableDAO getCategorieVariableDAO() {
        return categorieVariableDAO;
    }

    public void setCategorieVariableDAO(ICategorieVariableDAO categorieVariableDAO) {
        this.categorieVariableDAO = categorieVariableDAO;
    }

    public IVariableDAO getVariableDAO() {
        return variableDAO;
    }

    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    @Override
    public List<CategorieVariable> getCategorieAvailableVariables(Long id) {
        final List<CategorieVariable> categorieVariablesAvailables;
        categorieVariablesAvailables = this.produitBrutDAO.getAvailableVariablePRO(id);
        return categorieVariablesAvailables;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;

    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    public void setSolBrutDAO(ISolBrutDAO solBrutDAO) {
        this.solBrutDAO = solBrutDAO;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailablesVariablesPlanteBruteForDispositifs(Map<Long, Dispositif> availableDispositif) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> datatypeVariablePRO = this.planteBrutDAO.getAvailablesVariablesByDispositifPlanteBrut(
                new LinkedList(availableDispositif.values()));
        if (datatypeVariablePRO == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : datatypeVariablePRO) {
            for (final Entry<Long, Dispositif> dispositifEntry : availableDispositif.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        PhysicohimieDatasetManager.VARIABLE_PLANTE_BRUTE_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
        }
        return availablesVariables;
    }

    @Override
    public List<DatatypeVariableUnitePRO> getAvailblesVariablesPlanteMoyenneForDispositfs(Map<Long, Dispositif> map) {
        final List<DatatypeVariableUnitePRO> availablesVariables = new LinkedList();
        final List<DatatypeVariableUnitePRO> datatypeVariablePRO = this.planteMoyenneDAO.getAvailablesVariablesByDispositifPlanteMoyenne(
                new LinkedList(map.values()));
        if (datatypeVariablePRO == null) {
            return availablesVariables;
        }
        for (final DatatypeVariableUnitePRO dvumpro : datatypeVariablePRO) {
            for (final Entry<Long, Dispositif> dispositifEntry : map.entrySet()) {
                if (this.securityContext.isRoot()
                        || this.securityContext.matchPrivilege(String.format(
                                        PhysicohimieDatasetManager.DISPOSITIF_PLANTE_MOYENNE_PRIVILEGE_PATTERN,
                                        dispositifEntry.getValue().getCode(), dispositifEntry
                                        .getValue().getCode(), dvumpro.getVariablespro().getCode()),
                                Role.ROLE_EXTRACTION,
                                ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET)) {
                    availablesVariables.add(dvumpro);
                    break;
                }
            }
        }
        return availablesVariables;
    }

    public void setPlanteBrutDAO(IPlanteBrutDAO planteBrutDAO) {
        this.planteBrutDAO = planteBrutDAO;
    }

    public void setPlanteMoyenneDAO(IPlanteMoyenneDAO planteMoyenneDAO) {
        this.planteMoyenneDAO = planteMoyenneDAO;
    }

}
