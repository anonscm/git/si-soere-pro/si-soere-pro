/**
 * 
 */
package org.inra.ecoinfo.pro.refdata.modalitefacteuretudie;

/**
 * @author sophie
 *
 */
/*public class Recorder extends AbstractCSVMetadataRecorder<ModaliteFacteurEtudieTrt>{
	
	protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
	
	ITypeFacteurDAO typeFacteurDAO;
	IFacteurDAO facteurDAO;
	IModaliteDAO modaliteDAO;
	ITypeNomenclatureDAO typenomenclatureDAO;
	INomenclatureDAO nomenDAO;
	ILieuDAO lieuDAO;
	IDispositifDAO dispositifDAO;
	IDescriptionTraitementDAO descriptionTraitementDAO;
	IModaliteFacteurEtudieTrtDAO modaliteFacteurEtudieTrtDAO;
	
	Map<String, String[]> codeTraitementByDispositifPossibles = new TreeMap<String, String[]>();
	Map<String, String[]> facteursPossibles = new TreeMap<String, String[]>();
	Map<String, String[]> modalitesPossibles = new TreeMap<String, String[]>();
	Map<String, String[]> nomenPossibles = new TreeMap<String, String[]>();
	
	int nbreFacteurEtudieMax = 4;

	 (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
	 
	@Override
	public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException
	{		
		ErrorsReport errorsReport = new ErrorsReport();
		long line = 0;
		try 
		{	
			 skipHeader(parser);
			 
			String[] values = null;
			values = parser.getLine();
			while (values != null) 
			{	
				line++;
				
				Map<Integer, Modalite> lstModaliteAEnregistrer = new TreeMap<Integer, Modalite>();
				Map<Integer, Nomenclatures> lstNomenclatureAEnregistrer = new TreeMap<Integer, Nomenclatures>();
				List<ModaliteFacteurEtudieTrt> lstModaliteFactEtudieParTrait = new ArrayList<ModaliteFacteurEtudieTrt>();
				List<Modalite> lstAssocModaliteAEnlever = new ArrayList<Modalite>();
				List<Nomenclatures> lstAssocNomenAEnlever = new ArrayList<Nomenclatures>();
				
				TokenizerValues tokenizerValues = new TokenizerValues(values);
				
				int indexDispLieu = tokenizerValues.currentTokenIndex();
				String codeDispositifLieu = tokenizerValues.nextToken();
				int indexCodeTraitement = tokenizerValues.currentTokenIndex();
				String codeTraitement = tokenizerValues.nextToken();
				
				int index1 = codeDispositifLieu.indexOf('(');
                int index2 = codeDispositifLieu.indexOf(')');
                String codeDispositif = index1 != -1 ? codeDispositifLieu.substring(0, index1).trim() : codeDispositifLieu;
                String nomLieu = (index1 != -1 && index2 != -1) ? codeDispositifLieu.substring(index1 + 1, index2).trim() : codeDispositifLieu;
                
                Lieu lieu = verifieLieu(nomLieu, line+1, indexDispLieu+1, errorsReport);
				Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line+1, indexDispLieu+1, errorsReport);
				DescriptionTraitement descriptionTraitement = verifieDescriptionTraitement(codeTraitement, dispositif, line+1, indexCodeTraitement+1, errorsReport);
				
				Facteur facteur = null;

				for(int i = 0; i < nbreFacteurEtudieMax; i++)
				{		
					int indexFacteur = tokenizerValues.currentTokenIndex();
					String libelleFacteur = tokenizerValues.nextToken();
					
					int indexValeur = 0;
					int indexTpNomencl = 0;
					int indexNomencl = 0;
					String valeur = "";
					String libelleTypeNomenclature = "";
					String libelleNomenclature = "";
					if(i != nbreFacteurEtudieMax-1)
					{
						indexValeur = tokenizerValues.currentTokenIndex();
						valeur = tokenizerValues.nextToken();
					}
					else
					{
						indexTpNomencl = tokenizerValues.currentTokenIndex();
						libelleTypeNomenclature = tokenizerValues.nextToken();
						indexNomencl = tokenizerValues.currentTokenIndex();
						libelleNomenclature = tokenizerValues.nextToken();
					}
					
					//il n'y a pas forcément 3 facteurs étudiés et leurs modalités
					Modalite modalite = null;
					Nomenclatures nomenclature = null;
					
					if(libelleFacteur != null && !libelleFacteur.isEmpty())
					{
						facteur = verifieFacteur(libelleFacteur, line+1, indexFacteur+1, errorsReport);
						
						if(!facteur.getIsAssocNomenclature())
						{   //facteur associé à une modalité
							if(valeur != null && !valeur.isEmpty())
							{
								modalite = verifieModalite(valeur, libelleFacteur, facteur, line+1, indexValeur+1, errorsReport);
								
								if(!errorsReport.hasErrors() && modalite != null)
									lstModaliteAEnregistrer.put(i, modalite);
							}
						}
						else
						{   //facteur associé à une nomenclature
							if(libelleTypeNomenclature != null && !libelleTypeNomenclature.isEmpty() 
									&& libelleNomenclature != null && !libelleNomenclature.isEmpty())
							{
								verifieTypeNomenclatures(libelleTypeNomenclature, line+1, indexTpNomencl+1, errorsReport);
								nomenclature = verifieNomenclatures(libelleNomenclature, line+1, indexNomencl+1, errorsReport);
								
								if(!errorsReport.hasErrors() && nomenclature != null)
									lstNomenclatureAEnregistrer.put(i, nomenclature);
							}
						}
					}
				} //fin for
				
				if(!errorsReport.hasErrors())
	            {
					//si modification ou suppression d'un ou plusieurs facteurs étudiés dans une ligne, 
					//supprimer les associations dans la table modalite_facteur_etudie_traitement
					
					//liste des modalités et des nomenclatures dans une ligne avant modification de la ligne dans l'interface
					lstModaliteFactEtudieParTrait = modaliteFacteurEtudieTrtDAO.getLstModaliteFactEtudieByDescriptionTrt(descriptionTraitement);
					for(ModaliteFacteurEtudieTrt mdFE : lstModaliteFactEtudieParTrait)
					{
						if(mdFE.getModalite() != null)
							lstAssocModaliteAEnlever.add(mdFE.getModalite());
						else if(mdFE.getNomenclature() != null)
							lstAssocNomenAEnlever.add(mdFE.getNomenclature());
					}
					
					verifieListeAEnregisterVide(lstModaliteAEnregistrer, lstNomenclatureAEnregistrer, descriptionTraitement, facteur, line+1, indexCodeTraitement+1, errorsReport);
					
					//création des objets ModaliteFacteurEtudieTrt à enregistrer dans la table modalite_facteur_etudie_traitement
					if(!lstModaliteAEnregistrer.isEmpty())	
						createModFactEtudieTrtAvecMod(lstModaliteAEnregistrer, lstAssocModaliteAEnlever, descriptionTraitement);
					if(!lstNomenclatureAEnregistrer.isEmpty())
						createModFactEtudieTrtAvecNomen(lstNomenclatureAEnregistrer, lstAssocNomenAEnlever, descriptionTraitement);
						
					supprimeAssoc(lstAssocModaliteAEnlever, lstAssocNomenAEnlever, descriptionTraitement);
	            }
				
				values = parser.getLine();
			}	 //fin while
			
			gestionErreurs(errorsReport);
		}
		catch (IOException e1) 
		{
			throw new BusinessException(e1.getMessage(), e1);
		} 
		catch (PersistenceException e) 
		{
			//throw new BusinessException(e.getMessage(), e);
			errorsReport.addErrorMessage(e.getMessage());
			throw new BusinessException(errorsReport.getErrorsMessages());
		}
	}
	
	*//**
	 * @param lstModaliteAEnregistrer
	 * @param lstAssocModaliteAEnlever
	 * @param descriptionTraitement
	 * @throws PersistenceException 
	 *//*
	private void createModFactEtudieTrtAvecMod(Map<Integer, Modalite> lstModaliteAEnregistrer, List<Modalite> lstAssocModaliteAEnlever, DescriptionTraitement descriptionTraitement) throws PersistenceException
	{
		for(Entry<Integer, Modalite> entry : lstModaliteAEnregistrer.entrySet())
		{
			Modalite modalite = entry.getValue();
			if(lstAssocModaliteAEnlever.contains(modalite))
			{
				lstAssocModaliteAEnlever.remove(modalite);
			}
			ModaliteFacteurEtudieTrt modaliteFacteurEtudieTrt = new ModaliteFacteurEtudieTrt(modalite, null, descriptionTraitement);
			//sera utilisé pour conserver l'ordre d'affichage des facteurs étudiés (chaque groupe de 4 colonnes) même après un update
			int rang = entry.getKey().intValue()+1;
			modaliteFacteurEtudieTrt.setRangMod(rang);
			ModaliteFacteurEtudieTrt dbModaliteFacteurEtudieTrt = modaliteFacteurEtudieTrtDAO.getByModaliteDescriptionTrt(modalite, descriptionTraitement);
			
			if(dbModaliteFacteurEtudieTrt == null)
			{
				modaliteFacteurEtudieTrtDAO.saveOrUpdate(modaliteFacteurEtudieTrt);
			}
		}
	}
	
	*//**
	 * @param lstNomenclatureAEnregistrer
	 * @param lstAssocNomenAEnlever
	 * @param descriptionTraitement
	 * @throws PersistenceException 
	 *//*
	private void createModFactEtudieTrtAvecNomen(Map<Integer, Nomenclatures> lstNomenclatureAEnregistrer, List<Nomenclatures> lstAssocNomenAEnlever, DescriptionTraitement descriptionTraitement) throws PersistenceException
	{
		for(Entry<Integer, Nomenclatures> entry : lstNomenclatureAEnregistrer.entrySet())
		{
			Nomenclatures nomenclature = entry.getValue();
			if(lstAssocNomenAEnlever.contains(nomenclature))
			{
				lstAssocNomenAEnlever.remove(nomenclature);
			}
			ModaliteFacteurEtudieTrt modaliteFacteurEtudieTrt = new ModaliteFacteurEtudieTrt(null, nomenclature, descriptionTraitement);
			//sera utilisé pour conserver l'ordre d'affichage des facteurs étudiés même après un update
			int rang = entry.getKey().intValue()+1;
			modaliteFacteurEtudieTrt.setRangNom(rang);
			ModaliteFacteurEtudieTrt dbModaliteFacteurEtudieTrt = modaliteFacteurEtudieTrtDAO.getByNomenDescriptionTrt(nomenclature, descriptionTraitement);
			
			if(dbModaliteFacteurEtudieTrt == null)
			{
				modaliteFacteurEtudieTrtDAO.saveOrUpdate(modaliteFacteurEtudieTrt);
			}
		}
	}
	
	*//**
	 * @param lstAssocModaliteAEnlever
	 * @param lstAssocNomenAEnlever
	 * @param descriptionTraitement
	 * @throws NoResultException
	 * @throws NonUniqueResultException
	 * @throws PersistenceException
	 *//*
	private void supprimeAssoc(List<Modalite> lstAssocModaliteAEnlever, List<Nomenclatures> lstAssocNomenAEnlever, DescriptionTraitement descriptionTraitement) throws NoResultException, NonUniqueResultException, PersistenceException
	{
		for(Modalite modFactEtud : lstAssocModaliteAEnlever)
		{
			ModaliteFacteurEtudieTrt mdFE1 = modaliteFacteurEtudieTrtDAO.getByModaliteDescriptionTrt(modFactEtud, descriptionTraitement);
			modaliteFacteurEtudieTrtDAO.remove(mdFE1);
			
		}
		
		for(Nomenclatures nomFactEtud : lstAssocNomenAEnlever)
		{
			ModaliteFacteurEtudieTrt mdFE2 = modaliteFacteurEtudieTrtDAO.getByNomenDescriptionTrt(nomFactEtud, descriptionTraitement);
			modaliteFacteurEtudieTrtDAO.remove(mdFE2);
			
		}
	}
	
	*//**
	 * @param nomLieu
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 *//*
	private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport)
    {
		Lieu lieu = lieuDAO.getByNom(nomLieu);
        if (lieu == null) 
        {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }
        
        return lieu;
    }
	
	*//**
	 * @param codeDispositif
	 * @param lieu
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 *//*
	private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport)
	{
		Dispositif dispositif = dispositifDAO.getByCodeLieu(codeDispositif, lieu);
		if (dispositif == null)
		{
			errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
		}
		
		return dispositif;
	}
	
	*//**
	 * @param codeTraitement
	 * @param dispositif
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 *//*
	private DescriptionTraitement verifieDescriptionTraitement(String codeTraitement, Dispositif dispositif, long line, int index, ErrorsReport errorsReport)
    {
		DescriptionTraitement descriptionTraitement = descriptionTraitementDAO.getByCodeTraitementDispositif(codeTraitement, dispositif);
		String identifiantDispositif = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
		if (descriptionTraitement == null)
		{
			errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DESCRIPTRT_NONDEFINI"), line, index, codeTraitement, identifiantDispositif));
		}
		
		return descriptionTraitement;
    }
	
	*//**
	 * @param libelleFacteur
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 * @throws PersistenceException
	 *//*
	private Facteur verifieFacteur(String libelleFacteur, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
		Facteur facteur = facteurDAO.getByLibelleFct(libelleFacteur);
		if (facteur == null) 
		{
			errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "FACTEUR_NONDEFINI"), line, index, libelleFacteur));
		}
		
		return facteur;
    }
	
	*//**
	 * @param libelleModalite
	 * @param libelleFacteur
	 * @param facteur
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 * @throws PersistenceException
	 *//*
	private Modalite verifieModalite(String libelleModalite, String libelleFacteur, Facteur facteur, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
		Modalite modalite = modaliteDAO.getByValeurFacteur(libelleModalite, facteur);
		if (modalite == null)
		{
			errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "MOD_NONDEFINI"), line, index, libelleModalite, libelleFacteur));
		}
		
		return modalite;
    }
	
	*//**
	 * @param nomTypeNomen
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 * @throws PersistenceException
	 *//*
	private TypeNomenclature verifieTypeNomenclatures(String nomTypeNomen, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
    	TypeNomenclature dbTypeNomen = null;
        
        String code = Utils.createCodeFromString(nomTypeNomen);
        dbTypeNomen = typenomenclatureDAO.getByCode(code);
        
        if(dbTypeNomen == null)
        {
        	errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TYPENOMENCLATURE_NONDEFINI"), line, index, nomTypeNomen));
        }
        
        return dbTypeNomen;
    }
	
	*//**
	 * @param nomnomen
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @return
	 * @throws PersistenceException
	 *//*
	private Nomenclatures verifieNomenclatures(String nomnomen, long line, int index, ErrorsReport errorsReport) throws PersistenceException
    {
    	Nomenclatures dbnomen = nomenDAO.getByName(nomnomen);
        
        if(dbnomen == null)
        {
        	errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "NOMENCLATURE_NONDEFINI"), line, index, nomnomen));
        }
        
        return dbnomen;
    }
	
	*//**
	 * @param lstModaliteAEnregistrer
	 * @param descriptionTraitement
	 * @param line
	 * @param index
	 * @param errorsReport
	 * @throws BusinessException
	 *//*
	private void verifieListeAEnregisterVide(Map<Integer, Modalite> lstModaliteAEnregistrer, Map<Integer, Nomenclatures> lstNomenclatureAEnregistrer, DescriptionTraitement descriptionTraitement, Facteur facteur, long line, int index, ErrorsReport errorsReport) throws BusinessException
	{
		if(lstModaliteAEnregistrer.isEmpty() && lstNomenclatureAEnregistrer.isEmpty())
		{
			errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TRAITEMENT_INCORRECT"), line, index, descriptionTraitement.getCode(), 
						descriptionTraitement.getDispositif().getCode() + " (" + descriptionTraitement.getDispositif().getLieu().getNom() + ")"));
			throw new BusinessException(errorsReport.getErrorsMessages());
		}
		
	}
	
	 (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.impl.CSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
	 
	@Override
	public void deleteRecord(CSVParser parser, File file, String encoding)
			throws BusinessException 
	{
		try 
		{
			String[] values = parser.getLine();
			while (values != null) 
			{

				TokenizerValues tokenizerValues = new TokenizerValues(values);
				
				List<Modalite> lstModaliteFacteurEtudie = new ArrayList<Modalite>();
				List<Nomenclatures> lstNomenclatureFacteurEtudie = new ArrayList<Nomenclatures>();
				
				String codeDispositifLieu = tokenizerValues.nextToken();
				String codeTraitement = tokenizerValues.nextToken();
				
				int index1 = codeDispositifLieu.indexOf('(');
                int index2 = codeDispositifLieu.indexOf(')');
                String codeDispositif = index1 != -1 ? codeDispositifLieu.substring(0, index1).trim() : codeDispositifLieu;
                String nomLieu = (index1 != -1 && index2 != -1) ? codeDispositifLieu.substring(index1 + 1, index2).trim() : codeDispositifLieu;
                
                Facteur facteur = null;
                //on parcourt les colonnes facteurs étudiés
				for(int i = 0; i < nbreFacteurEtudieMax; i++)
				{					
					String libelleFacteur = tokenizerValues.nextToken();
					
					String libelleModalite = "";
					String libelleNomenclature = "";
					if(i != nbreFacteurEtudieMax-1)
					{
						libelleModalite = tokenizerValues.nextToken();
					}
					else
					{
						tokenizerValues.nextToken();
						libelleNomenclature = tokenizerValues.nextToken();
					}
					
					//il n'y a pas forcément 3 facteurs étudiés (c'est-à-dire 3 groupes de 4 colonnes)
					Modalite modalite = null;
					Nomenclatures nomenclature = null;
					
					if(libelleFacteur != null && !libelleFacteur.isEmpty())
					{
						facteur = facteurDAO.getByLibelleFct(libelleFacteur);
						
						if(!facteur.getIsAssocNomenclature())
						{   //facteur associé à une modalité
							if(libelleModalite != null && !libelleModalite.isEmpty())
							{
								modalite = modaliteDAO.getByValeurFacteur(libelleModalite, facteur);
								lstModaliteFacteurEtudie.add(modalite);
							}
						}
						else
						{   //facteur associé à une nomenclature
							if(libelleNomenclature != null && !libelleNomenclature.isEmpty())
							{
								nomenclature = nomenDAO.getByName(libelleNomenclature);
								lstNomenclatureFacteurEtudie.add(nomenclature);
							}
						}
					}
				} //fin for
				
				Lieu lieu = lieuDAO.getByNom(nomLieu);
				Dispositif dispositif = dispositifDAO.getByCodeLieu(codeDispositif, lieu);
				DescriptionTraitement descriptionTraitement = descriptionTraitementDAO.getByCodeTraitementDispositif(codeTraitement, dispositif);
				
				for(Modalite modalite : lstModaliteFacteurEtudie)
				{
					ModaliteFacteurEtudieTrt modaliteFacteurEtudieTrt1 = modaliteFacteurEtudieTrtDAO.getByModaliteDescriptionTrt(modalite, descriptionTraitement);
					modaliteFacteurEtudieTrtDAO.remove(modaliteFacteurEtudieTrt1);
				}
				
				for(Nomenclatures nomen : lstNomenclatureFacteurEtudie)
				{
					ModaliteFacteurEtudieTrt modaliteFacteurEtudieTrt2 = modaliteFacteurEtudieTrtDAO.getByNomenDescriptionTrt(nomen, descriptionTraitement);
					modaliteFacteurEtudieTrtDAO.remove(modaliteFacteurEtudieTrt2);
				}
				
				values = parser.getLine();
			}
		} 
		catch (Exception e) 
		{
			throw new BusinessException(e.getMessage(),e);
		}
	}
	
	*//**
	 * @param errorsReport
	 * @throws BusinessException
	 *//*
	public void gestionErreurs(ErrorsReport errorsReport) throws BusinessException 
    {
        if (errorsReport.hasErrors()) 
        {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }
	
	*//**
	 * @return
	 * @throws PersistenceException
	 *//*
	private String[] getDispositifPossibles() throws PersistenceException
    {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll(Dispositif.class);
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) 
        {
            dispositifPossibles[index++] = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
        }
        return dispositifPossibles;
    }
	
	*//**
	 * @return
	 * @throws PersistenceException
	 *//*
	private Map<String, String[]> getCodeTraitementByCodeDispositifPossibles() throws PersistenceException 
    {
		List<Dispositif> lstDispositifs = dispositifDAO.getAll(Dispositif.class);
 		
 		for (Dispositif dispositif  : lstDispositifs) 
        {
 			List<DescriptionTraitement> lstDescriptionTraitements = descriptionTraitementDAO.getByDispositif(dispositif);
 			
 			if(lstDescriptionTraitements==null){
    			return new HashMap();
    		}
 			String[] codeTraitementPossibles = new String[lstDescriptionTraitements.size()];
 			int index = 0;
            for (DescriptionTraitement descriptionTraitement : lstDescriptionTraitements) 
            {
            	codeTraitementPossibles[index++] = descriptionTraitement.getCode();
            }
            
            String codeDispositif = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
            codeTraitementByDispositifPossibles.put(codeDispositif, codeTraitementPossibles);
        }
        
        return codeTraitementByDispositifPossibles;
    }
	
	*//**
     * @return
     * @throws PersistenceException
     *//*
    private String[] getLibelleFacteurModPossibles() throws PersistenceException 
    {
        List<Facteur> lstFacteurs = facteurDAO.getLstFacteurByIsInfosApportFert(false);
        for (Facteur facteur1 : lstFacteurs) 
        {
        	if(facteur1.getIsAssocNomenclature())
        		lstFacteurs.remove(facteur1);
        }
        String[] facteurPossibles = new String[lstFacteurs.size()+1];
        facteurPossibles[0] = (String) Constantes.STRING_EMPTY;
		int index = 1;
        for (Facteur facteur : lstFacteurs) 
        {
        	facteurPossibles[index++] = facteur.getLibelle();
        }
        return facteurPossibles;
    }
    
    *//**
     * @return
     * @throws PersistenceException
     *//*
    private String[] getLibelleFacteurNomenPossibles() throws PersistenceException 
    {
        List<Facteur> lstFacteurs = facteurDAO.getLstFacteurByIsAssocNomenclature(true);
        String[] facteurPossibles = new String[lstFacteurs.size()+1];
        facteurPossibles[0] = (String) Constantes.STRING_EMPTY;
		int index = 1;
        for (Facteur facteur : lstFacteurs) 
        {
        	facteurPossibles[index++] = facteur.getLibelle();
        }
        return facteurPossibles;
    }
   
	*//**
	 * @return
	 * @throws PersistenceException
	 *//*
	private Map<String, String[]> getValeurModalitePossibles() throws PersistenceException 
	{
		String[] valeurModalitePossibles = null;
		
		List<Facteur> lstFacteurs = facteurDAO.getAll(Facteur.class);
		
		for (Facteur facteur : lstFacteurs)
		{			
			List<Modalite> lstValeurModalites = modaliteDAO.getLstModByFacteur(facteur);
			
			valeurModalitePossibles = new String[lstValeurModalites.size()+1];
			
			valeurModalitePossibles[0] = (String) Constantes.STRING_EMPTY;
			
			int index = 1;
			
			for (Modalite modalite : lstValeurModalites)
			{
				valeurModalitePossibles[index++] = modalite.getValeur();
			}
			
			modalitesPossibles.put(facteur.getLibelle(), valeurModalitePossibles);			
		}

		return modalitesPossibles;
	}
	
	*//**
	 * @return
	 * @throws PersistenceException
	 *//*
	private String[] getTypeNomenclaturePossibles() throws PersistenceException 
    {
        List<TypeNomenclature> lstTypeNomens = typenomenclatureDAO.getAll(TypeNomenclature.class);
        
        String[] lstTypeNomenPossibles = new String[lstTypeNomens.size() + 1];
        lstTypeNomenPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (TypeNomenclature typeNomenclature : lstTypeNomens) 
        {
        	lstTypeNomenPossibles[index++] = typeNomenclature.getTn_nom();
        }
        
        return lstTypeNomenPossibles;
    }
	
	*//**
	 * @return
	 * @throws PersistenceException
	 *//*
	private Map<String, String[]> getNomenclaturePossibles() throws PersistenceException 
    {
    	List<TypeNomenclature> lstTypeNomens = typenomenclatureDAO.getAll(TypeNomenclature.class);
    	
    	if(lstTypeNomens != null)
    	{
    		for (TypeNomenclature typeNomenclature : lstTypeNomens) 
    		{
    			List<Nomenclatures> lstNomenAvecFacteur = new ArrayList<Nomenclatures>();
    			List<Nomenclatures> lstNomens = nomenDAO.getByTypeNomenclature(typeNomenclature);
    			for (Nomenclatures nomencl : lstNomens) 
    			{
    				if(nomencl.getFacteur() != null)
    					lstNomenAvecFacteur.add(nomencl);
    			}
    			String[] lstNomenPossibles = new String[lstNomenAvecFacteur.size() + 1];
    			lstNomenPossibles[0] = "";
    			int index = 1;
    			for (Nomenclatures nomenclatures : lstNomenAvecFacteur) 
    			{
    				lstNomenPossibles[index++] = nomenclatures.getNomen_nom();
    			}
    			
    			nomenPossibles.put(typeNomenclature.getTn_nom(), lstNomenPossibles);
    		}
    	}
        
        return nomenPossibles;
    }
	
	 (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
	 
	@Override
	public LineModelGridMetadata getNewLineModelGridMetadata(ModaliteFacteurEtudieTrt modaliteFacteurEtudieTrt) throws PersistenceException {
		LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
		
		// Code du dispositif (Nom du lieu)
		String codeDipositifLieu = modaliteFacteurEtudieTrt == null ? new String("") : 
						(modaliteFacteurEtudieTrt.getDescriptionTraitement() == null ? new String("") :
							(modaliteFacteurEtudieTrt.getDescriptionTraitement().getDispositif() == null ? new String("") : 
								(modaliteFacteurEtudieTrt.getDescriptionTraitement().getDispositif().getCode() + " (" + modaliteFacteurEtudieTrt.getDescriptionTraitement().getDispositif().getLieu().getNom() + ")")));
		ColumnModelGridMetadata colonne5 = 
	        	new ColumnModelGridMetadata(modaliteFacteurEtudieTrt == null ? Constantes.STRING_EMPTY : codeDipositifLieu, getDispositifPossibles(), null, true, false, true);
		
		//Code du traitement
		String codeDispTrt = "";
		if (modaliteFacteurEtudieTrt != null && modaliteFacteurEtudieTrt.getDescriptionTraitement() != null)
		{
			codeDispTrt = modaliteFacteurEtudieTrt.getDescriptionTraitement().getCode();
		}
		ColumnModelGridMetadata colonne6 = 
				new ColumnModelGridMetadata(codeDispTrt,  
						getCodeTraitementByCodeDispositifPossibles(), null, true, false, true);
		
		List<ColumnModelGridMetadata> refsColonne2 = new LinkedList<ColumnModelGridMetadata>();
	      
		refsColonne2.add(colonne6);
	      
		colonne5.setRefs(refsColonne2);
	      
		colonne6.setValue(codeDispTrt);
		
		lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne5);
		lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne6);
		
		//Important : les facteurs étudiés 
		//sont répétées, dans cet ordre plusieurs fois (ce nombre de fois est donné par "nbreFacteurEtudieMax" qui vaut actuellement 4)
		//(ces colonnes sont des lignes de la table modalite_facteur_etudie_traitement qui est la table d'association 
		//entre un traitement donné, les modalités et/ou les nomenclatures des facteurs étudiés).
		for(int i = 0; i < nbreFacteurEtudieMax; i++)
		{
			//Le 1er facteur étudié est obligatoire
			Boolean mandatory = i == 0 ? true : false;
			
			//Libellé du facteur étudié
			String libelleFact  = "";
			
			if(i!=nbreFacteurEtudieMax-1)
			{
				if (modaliteFacteurEtudieTrt != null && modaliteFacteurEtudieTrt.getLstModaliteParTraitement() != null 
						&& !modaliteFacteurEtudieTrt.getLstModaliteParTraitement().isEmpty()
						&& modaliteFacteurEtudieTrt.getLstModaliteParTraitement().entrySet() != null
						&& !modaliteFacteurEtudieTrt.getLstModaliteParTraitement().entrySet().isEmpty() 
						&& modaliteFacteurEtudieTrt.getLstModaliteParTraitement().get(i+1) != null)
				{
					if(modaliteFacteurEtudieTrt.getLstModaliteParTraitement().get(i+1).getFacteur() != null)
						libelleFact = modaliteFacteurEtudieTrt.getLstModaliteParTraitement().get(i+1).getFacteur().getLibelle();
				}
				
				ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(libelleFact, 
								getLibelleFacteurModPossibles(), null, false, false, mandatory);
				
				//valeur de la modalité du facteur étudié
				String valeurModaliteEtudiee = "";
				if (modaliteFacteurEtudieTrt != null && modaliteFacteurEtudieTrt.getLstModaliteParTraitement() != null 
						&& modaliteFacteurEtudieTrt.getLstModaliteParTraitement().entrySet() != null
						&& !modaliteFacteurEtudieTrt.getLstModaliteParTraitement().entrySet().isEmpty() 
						&& modaliteFacteurEtudieTrt.getLstModaliteParTraitement().get(i+1) != null)
				{
						valeurModaliteEtudiee = modaliteFacteurEtudieTrt.getLstModaliteParTraitement().get(i+1).getValeur();
				}
						
						ColumnModelGridMetadata colonne2 = 
								new ColumnModelGridMetadata(valeurModaliteEtudiee, 
										getValeurModalitePossibles(), null, false, false, false);
						
						List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();
						
						refsColonne1.add(colonne2);
						
						colonne1.setRefs(refsColonne1);
						
						colonne2.setValue(valeurModaliteEtudiee);
						
						lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
						lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);
			}
			else
			{
				if(modaliteFacteurEtudieTrt != null && modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement() != null
						&& modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().entrySet() != null
						&& !modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().entrySet().isEmpty() 
						&& modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1) != null)
				{
					if(modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1).getFacteur() != null)
						libelleFact = modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1).getFacteur().getLibelle();
				}
				
				lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(libelleFact, 
						getLibelleFacteurNomenPossibles(), null, false, false, mandatory));
				
				//Type de nomenclature
				String libelleTypeNomen = "";
				if(modaliteFacteurEtudieTrt != null && modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement() != null
						&& modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().entrySet() != null
						&& !modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().entrySet().isEmpty() 
						&& modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1) != null)
				{
					libelleTypeNomen = modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1).getTypenomenclature() == null ? 
							new String("") : modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1).getTypenomenclature().getTn_nom();
				}
				ColumnModelGridMetadata colonne3 = new ColumnModelGridMetadata(libelleTypeNomen, getTypeNomenclaturePossibles(), null, false, false, false);
				
				//Nomenclature
				String libelleNomen = "";
				if(modaliteFacteurEtudieTrt != null && modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement() != null
						&& modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().entrySet() != null
						&& !modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().entrySet().isEmpty() 
						&& modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1) != null)
				{
					libelleNomen = modaliteFacteurEtudieTrt.getLstNomenclatureParTraitement().get(i+1).getNomen_nom();
				}
				ColumnModelGridMetadata colonne4 = new ColumnModelGridMetadata(libelleNomen, getNomenclaturePossibles(), null, false, false, false);
				
				List<ColumnModelGridMetadata> refsColonne3 = new LinkedList<ColumnModelGridMetadata>();
				
				refsColonne3.add(colonne4);
				
				colonne3.setRefs(refsColonne3);
				colonne4.setValue(libelleNomen);
				
				lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne3);
				lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne4);
			}
	    } //fin for
		
		return lineModelGridMetadata;
	}
	
	 Important
	 * getAllElements() est redéfinie car, dans le noyau le fonctionnement est le suivant : 
	 * dans la méthode buildModelGrid() qui appelle getAllElements() pour chaque élément renvoyé par la méthode getAllElements(), 
     * une ligne est créée dans le ModelGridMetadata. 
	 * Le fonctionnement attendu spécifiquement ici est le suivant :  pour plusieurs éléments renvoyés par getAllElements(), il 
	 * n'y aura qu'une seule ligne (avec plusieurs colonnes) créée dans ModelGridMetadata.
	 
	@Override
	protected List<ModaliteFacteurEtudieTrt> getAllElements() throws PersistenceException 
	{ 
		List<ModaliteFacteurEtudieTrt> lstModaliteFacteurEtudieTraitement = new ArrayList<ModaliteFacteurEtudieTrt>();
		
		List<ModaliteFacteurEtudieTrt> lstModfactEtudieTrt = modaliteFacteurEtudieTrtDAO.getAll();
		
		//trie la liste des modaliteFacteurEtudieTrt par l'identifiant du traitement
		Collections.sort(lstModfactEtudieTrt,new CompareV());
		
		Map<Integer, Modalite> lstModalite = new TreeMap<Integer, Modalite>();
		Map<Integer, Nomenclatures> lstNomenclature = new TreeMap<Integer, Nomenclatures>();
		
		for(int i = 0; i < lstModfactEtudieTrt.size(); i++)
		{
			ModaliteFacteurEtudieTrt mdFETrt = lstModfactEtudieTrt.get(i);
			
			DescriptionTraitement descriptionTraitement = mdFETrt.getDescriptionTraitement();
			Modalite modalite = mdFETrt.getModalite();
			Nomenclatures nomenclature = mdFETrt.getNomenclature();
			if(modalite != null)
				lstModalite.put(new Integer(mdFETrt.getRangMod()),modalite);
			if(nomenclature != null)
				lstNomenclature.put(new Integer(mdFETrt.getRangNom()), nomenclature);
					
			if(i+1 < lstModfactEtudieTrt.size())
			{
				DescriptionTraitement descriptionTraitementSuiv = lstModfactEtudieTrt.get(i+1).getDescriptionTraitement();
				if(!descriptionTraitement.equals(descriptionTraitementSuiv))
				{
					ModaliteFacteurEtudieTrt modaliteFacteurEtudieTraitement = new ModaliteFacteurEtudieTrt(descriptionTraitement);
					modaliteFacteurEtudieTraitement.setLstModaliteParTraitement(lstModalite);
					modaliteFacteurEtudieTraitement.setLstNomenclatureParTraitement(lstNomenclature);
					lstModalite = new TreeMap<Integer, Modalite>();
					lstNomenclature = new TreeMap<Integer, Nomenclatures>();
					//liste des modaliteFacteurEtudieTraitement utilisée par buildModelGrid() qui appelle getAllElements() 
					lstModaliteFacteurEtudieTraitement.add(modaliteFacteurEtudieTraitement);
				}
			}
			else
			{
				ModaliteFacteurEtudieTrt modaliteFacteurEtudieTraitement = new ModaliteFacteurEtudieTrt(descriptionTraitement);
				modaliteFacteurEtudieTraitement.setLstModaliteParTraitement(lstModalite);
				modaliteFacteurEtudieTraitement.setLstNomenclatureParTraitement(lstNomenclature);
				//liste des modaliteFacteurEtudieTraitement utilisée par buildModelGrid() qui appelle getAllElements() 
				lstModaliteFacteurEtudieTraitement.add(modaliteFacteurEtudieTraitement);
			}
		}
		
		for(int i = 0; i < lstModaliteFacteurEtudieTraitement.size(); i++)
		{
			if(lstModaliteFacteurEtudieTraitement.get(i).getDescriptionTraitement() != null 
					&& lstModaliteFacteurEtudieTraitement.get(i).getDescriptionTraitement().getDispositif() != null 
					&& lstModaliteFacteurEtudieTraitement.get(i).getDescriptionTraitement().getDispositif().getCode() != null)
			{
				System.out.println("ModaliteFacteurEtudieTrt " + lstModaliteFacteurEtudieTraitement.get(i).getDescriptionTraitement().getDispositif().getCode());
			}
		}
		
		
		return lstModaliteFacteurEtudieTraitement;
	}
	
	//inner class
	class CompareV implements Comparator<Object> {
		
		public int compare(Object o1, Object o2) 
		{
			ModaliteFacteurEtudieTrt mdFE1 = (ModaliteFacteurEtudieTrt) o1;
			ModaliteFacteurEtudieTrt mdFE2 = (ModaliteFacteurEtudieTrt) o2;
			
			long idTrt1 = mdFE1.getDescriptionTraitement().getId();
			long idTrt2 = mdFE2.getDescriptionTraitement().getId();
		    
			if (idTrt1 > idTrt2)
				return 1;
			else if (idTrt1 < idTrt2)
				return -1;
			else
				return 0;
		}
	}

	*//**
	 * @param typeFacteurDAO the typeFacteurDAO to set
	 *//*
	public void setTypeFacteurDAO(ITypeFacteurDAO typeFacteurDAO) {
		this.typeFacteurDAO = typeFacteurDAO;
	}

	*//**
	 * @param facteurDAO the facteurDAO to set
	 *//*
	public void setFacteurDAO(IFacteurDAO facteurDAO) {
		this.facteurDAO = facteurDAO;
	}

	*//**
	 * @param modaliteDAO the modaliteDAO to set
	 *//*
	public void setModaliteDAO(IModaliteDAO modaliteDAO) {
		this.modaliteDAO = modaliteDAO;
	}
	
	*//**
	 * @param typenomenclatureDAO the typenomenclatureDAO to set
	 *//*
	public void setTypenomenclatureDAO(ITypeNomenclatureDAO typenomenclatureDAO) {
		this.typenomenclatureDAO = typenomenclatureDAO;
	}

	*//**
	 * @param nomenclatureDAO the nomenclatureDAO to set
	 *//*
	public void setNomenDAO(INomenclatureDAO nomenDAO) {
		this.nomenDAO = nomenDAO;
	}

	*//**
	 * @param lieuDAO the lieuDAO to set
	 *//*
	public void setLieuDAO(ILieuDAO lieuDAO) {
		this.lieuDAO = lieuDAO;
	}

	*//**
	 * @param dispositifDAO the dispositifDAO to set
	 *//*
	public void setDispositifDAO(IDispositifDAO dispositifDAO) {
		this.dispositifDAO = dispositifDAO;
	}

	*//**
	 * @param descriptionTraitementDAO the descriptionTraitementDAO to set
	 *//*
	public void setDescriptionTraitementDAO(
			IDescriptionTraitementDAO descriptionTraitementDAO) {
		this.descriptionTraitementDAO = descriptionTraitementDAO;
	}

	*//**
	 * @param modaliteFacteurEtudieTrtDAO the modaliteFacteurEtudieTrtDAO to set
	 *//*
	public void setModaliteFacteurEtudieTrtDAO(
			IModaliteFacteurEtudieTrtDAO modaliteFacteurEtudieTrtDAO) {
		this.modaliteFacteurEtudieTrtDAO = modaliteFacteurEtudieTrtDAO;
	}
	
	
}
*/