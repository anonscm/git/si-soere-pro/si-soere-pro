/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.security.entity.ISecurityPath;
import org.inra.ecoinfo.security.entity.ISecurityPathWithVariable;

/**
 *
 * @author vjkoyao
 */
@Entity
@Table(name = MesureSemisPlantation.TABLE_NAME, uniqueConstraints = @UniqueConstraint(columnNames = {
                                                                                                       MesureSemisPlantation.ATTRIBUTE_CODE_DISPOSITIF,
                                                                                                       DescriptionTraitement.ID_JPA, 
                                                                                                       MesureSemisPlantation.ATTRIBUTE_JPA_DATE_DEBUT,
                                                                                                       MesureSemisPlantation.ATTRIBUTE_NOM_PARCELLE,
                                                                                                       MesureSemisPlantation.ATTRIBUTE_NOM_PLACETTE,
                                                                                                        MesureSemisPlantation.ATTRIBUTE_NOM_CULTURE}))

public class MesureSemisPlantation implements Serializable, ISecurityPath {

    public static final String ATTRIBUTE_JPA_ID = "id_mesure_semis";
    public static final String TABLE_NAME = "mesureinterventionsemisplantation";
    public static final String ATTRIBUTE_JPA_DATE_DEBUT = "datedebutmesure";
    public static final String ATTRIBUTE_CODE_DISPOSITIF = "codedispositif";
    public static final String ATTRIBUTE_KEYMESURE = "keymesure";
    public static final String ATTRIBUTE_NOM_PARCELLE = "nomparcelle";
    public static final String ATTRIBUTE_NOM_PLACETTE = "nomplacette";
    public static final String ATTRIBUTE_VARIETE = "varietecepage";
    public static final String ATTRIBUTE_NOM_CULTURE = "nomculture";
    public static final String ATTRIBUTE_MATERIEL_1 = "materiel1";
    public static final String ATTRIBUTE_MATERIEL_2 = "materiel2";
    public static final String ATTRIBUTE_MATERIEL_3 = "materiel3";
    public static final String ATTRIBUTE_TYPE_OBSERVATION = "typeobservation";
    public static final String ATTRIBUTE_NOM_OBSERVATION = "nomobservation";
    public static final String ATTRIBUTE_COMMENTAIRE_OBS_QUA = "commentaire";
     public static final String ATTRIBUTE_NIVEAU = "niveauatteint";

    static final long serialVersionUID = 1L;

    
    

    @Id
    @Column(name = MesureSemisPlantation.ATTRIBUTE_JPA_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id_mesure_semis;

    @Column(name = MesureSemisPlantation.ATTRIBUTE_JPA_DATE_DEBUT, nullable = false)
    @Temporal(TemporalType.DATE)
    Date datedebutmesure;
    
    @Column(name = MesureSemisPlantation.ATTRIBUTE_CODE_DISPOSITIF, nullable = false)
    private String codedispositif = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_KEYMESURE, nullable = false,unique = true)
    private String keymesure = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_NOM_PARCELLE, nullable = false)
    private String nomparcelle = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_NOM_PLACETTE, nullable = false)
    private String nomplacette = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_VARIETE, nullable = false)
    private String varietecepage = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_NOM_CULTURE, nullable = false)
    private String nomculture = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_MATERIEL_1, nullable = false)
    private String materiel1 = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_MATERIEL_2, nullable = true)
    private String materiel2 = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_MATERIEL_3, nullable = true)
    private String materiel3 = "";
    @Column(name = MesureSemisPlantation.ATTRIBUTE_TYPE_OBSERVATION, nullable = true)
    private String typeobservation;
    @Column(name = MesureSemisPlantation.ATTRIBUTE_NOM_OBSERVATION, nullable = true)
    private String nomobservation;
    @Column(name = MesureSemisPlantation.ATTRIBUTE_COMMENTAIRE_OBS_QUA, nullable = false)
    private String commentaire = "";
     @Column(name = MesureSemisPlantation.ATTRIBUTE_NIVEAU, nullable = true)
    private String niveauatteint = "";
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = DescriptionTraitement.class)
    @JoinColumn(name = DescriptionTraitement.ID_JPA, referencedColumnName = DescriptionTraitement.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
     DescriptionTraitement descriptionTraitement;  
     
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    VersionFile versionfile;


    @OneToMany(mappedBy = "mesureinterventionsemis", cascade = ALL)
    List<ValeurSemisPlantation> valeursemis = new LinkedList();
    public MesureSemisPlantation() {
        super();
    }
    public MesureSemisPlantation(Date datedebutmesure, String codedispositif, DescriptionTraitement codetraitement,String nomparcelle,String nomplacette,String varietecepage,
            String nomculture,String materiel1, String materiel2, String materiel3,String niveau,
            VersionFile versionfile,String typeobservation,String nomobservation,String commentaire
    ) {
        this.datedebutmesure = datedebutmesure;
        this.varietecepage = varietecepage;
        this.commentaire = commentaire;
        this.materiel1 = materiel1;
        this.materiel2 = materiel2;
        this.materiel3 = materiel3;
        this.nomplacette= nomplacette;
        this.versionfile = versionfile;
        this.codedispositif= codedispositif;
        this.nomculture = nomculture;
        this.niveauatteint = niveau;
        this.descriptionTraitement = codetraitement;
        this.typeobservation = typeobservation;
        this.nomparcelle=nomparcelle;
        this.nomobservation = nomobservation;
    }

    @Override
    public List<ISecurityPathWithVariable> getChildren() {
        final List<ISecurityPathWithVariable> children = new LinkedList();
        for (final ValeurSemisPlantation valeur : this.valeursemis) {
            children.add(valeur);
        }
        return children;
    }

    @Override
    public Date getDatePrelevement() {
        return this.getDatePrelevement();
    }

    @Override
    public String getSecurityPath() {
        return String.format(MesureSemisPlantation.RESOURCE_PATH,
                this.getVersionfile().getDataset().getLeafNode().getDatatype().getCode());
    }

    @Override
    public void setChildren(List<ISecurityPathWithVariable> children) {
        this.valeursemis.clear();
        for (final ISecurityPathWithVariable child : children) {
            this.valeursemis.add((ValeurSemisPlantation) child);
        }
    }

    public Long getId_mesure_semis() {
        return id_mesure_semis;
    }

    public void setId_mesure_semis(Long id_mesure_semis) {
        this.id_mesure_semis = id_mesure_semis;
    }

    public Date getDatedebutmesure() {
        return datedebutmesure;
    }

    public void setDatedebutmesure(Date datedebutmesure) {
        this.datedebutmesure = datedebutmesure;
    }

    public String getCodedispositif() {
        return codedispositif;
    }

    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    public DescriptionTraitement getDescriptionTraitement() {
        return descriptionTraitement;
    }

    public void setDescriptionTraitement(DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    public String getNiveauatteint() {
        return niveauatteint;
    }

    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    public String getKeymesure() {
        return keymesure;
    }

    public void setKeymesure(String keymesure) {
        this.keymesure = keymesure;
    }

   

    public String getNomparcelle() {
        return nomparcelle;
    }

    public void setNomparcelle(String nomparcelle) {
        this.nomparcelle = nomparcelle;
    }

    public String getNomplacette() {
        return nomplacette;
    }

    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    public String getVarietecepage() {
        return varietecepage;
    }

    public void setVarietecepage(String varietecepage) {
        this.varietecepage = varietecepage;
    }

    public String getNomculture() {
        return nomculture;
    }

    public void setNomculture(String nomculture) {
        this.nomculture = nomculture;
    }

    public String getMateriel1() {
        return materiel1;
    }

    public void setMateriel1(String materiel1) {
        this.materiel1 = materiel1;
    }

    public String getMateriel2() {
        return materiel2;
    }

    public void setMateriel2(String materiel2) {
        this.materiel2 = materiel2;
    }

    public String getMateriel3() {
        return materiel3;
    }

    public void setMateriel3(String materiel3) {
        this.materiel3 = materiel3;
    }

   

    public String getTypeobservation() {
        return typeobservation;
    }

    public void setTypeobservation(String typeobservation) {
        this.typeobservation = typeobservation;
    }

    public String getNomobservation() {
        return nomobservation;
    }

    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public VersionFile getVersionfile() {
        return versionfile;
    }

    public void setVersionfile(VersionFile versionfile) {
        this.versionfile = versionfile;
    }

    public List<ValeurSemisPlantation> getValeursemis() {
        return valeursemis;
    }

    public void setValeursemis(List<ValeurSemisPlantation> valeursemis) {
        this.valeursemis = valeursemis;
    }

   
  

}
