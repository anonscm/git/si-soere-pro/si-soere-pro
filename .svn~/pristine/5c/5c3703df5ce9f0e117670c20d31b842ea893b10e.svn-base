package org.inra.ecoinfo.pro;

import java.io.IOException;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.associate.IAssociateDAO;
import org.inra.ecoinfo.filecomp.associate.IAssociateFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.springframework.test.context.TestContext;
import org.springframework.transaction.annotation.Transactional;


public class ProTransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {
    private static String testName="";

    public static IMetadataManager metadataManager;
    public static IDatasetManager datasetManager;
    public static IDatasetDAO datasetDAO;
    public static IVersionFileDAO versionFileDAO;
    public static IVariableDAO variableDAO;
    public static ILocalizationManager localizationManager;
    public static IExtractionManager extractionManager;
    public static INotificationsManager notificationsManager;
    public static IFileCompManager fileCompManager;
    public static IAssociateFileCompManager associateFileCompManager;
    public static IFileCompConfiguration fileCompConfiguration;
    public static IAssociateDAO associateDAO;

    /**
     * After super class.
     * 
     * @param testContext
     * @link(TestContext) the test context
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     * @throws DataSetException
     *             the data set exception
     * @throws DatabaseUnitException
     *             the database unit exception
     * @throws SQLException
     *             the sQL exception
     * @throws Exception
     *             the exception
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        super.afterTestClass(testContext);
    }

    /**
     * Before test class.
     * 
     * @param testContext
     * @link(TestContext) the test context
     * @throws Exception
     *             the exception
     * @see org.springframework.test.context.support.AbstractTestExecutionListener #beforeTestClass(org.springframework.test.context.TestContext)
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        ProTransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        ProTransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        ProTransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        ProTransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        ProTransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        ProTransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        ProTransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        ProTransactionalTestFixtureExecutionListener.fileCompManager = (IFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompManager");
        ProTransactionalTestFixtureExecutionListener.associateFileCompManager = (IAssociateFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateFileCompManager");
        ProTransactionalTestFixtureExecutionListener.fileCompConfiguration = (IFileCompConfiguration) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompConfiguration");
        ProTransactionalTestFixtureExecutionListener.associateDAO = (IAssociateDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateDAO");
    
        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
        System.setProperty("concordion.output.dir",testName);
    }

    /**
     * Clean tables.
     * 
     * @throws SQLException
     *             the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion().getConnection().prepareStatement("delete from utilisateur;").execute();
    }
}
