/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre;

import com.google.common.base.Strings;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.pro.extraction.fluxchambre.impl.FluxChambreParameterVO;
import org.inra.ecoinfo.pro.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.pro.extraction.jsf.HelperForm;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.vo.AbstractDatesFormParam;
import org.inra.ecoinfo.pro.utils.vo.DatesForm1ParamVO;
import org.inra.ecoinfo.pro.utils.vo.DatesRequestParamVO;
import org.inra.ecoinfo.security.ISecurityContext;
import org.inra.ecoinfo.tree.TreeNodeItem;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 *
 * @author vkoyao
 */


@ManagedBean(name = "uiBeanFluxChambre")
@ViewScoped
public class UIBeanFluxChambre extends AbstractUIBeanForSteps implements Serializable {

    @ManagedProperty(value = "#{fluxchambreDatasetManager}")
    IFluxChambreDatasetManager fluxchambreDatasetManager;
    
    @ManagedProperty(value = "#{fluxChambreDatatype}")
    public String fluxChambreDatatype;
    
    @ManagedProperty(value = "#{transactionManager}")
    JpaTransactionManager transactionManager;

    @ManagedProperty(value = "#{extractionManager}")
    @Transient
    IExtractionManager extractionManager;
    
    @ManagedProperty(value = "#{localizationManager}")
    @Transient
    ILocalizationManager localizationManager;

    @ManagedProperty(value = "#{notificationsManager}")
    @Transient
    INotificationsManager notificationsManager;
    
    @ManagedProperty(value = "#{securityContext}")
    @Transient
    ISecurityContext securityContext;


    @Transient
    ParametersRequest parametersRequest = new ParametersRequest();

    Long idVariableSelected;
    Long idFluxChambreVariableSelected;
    
    String affichage = "1";
    Boolean frequenceRequired = false;

   
    @Transient
    Map<String, TreeNodeItem> availablesDispositif = new TreeMap();
 
    @Transient
    Map<String, List<VariableJSF>> availablesVariables = new HashMap();
    
//    @Transient
//    final Map<Long, List<VariableJSF>> variablesFluxChambreAvailables = new HashMap();
    
    @Transient
    Map<Long, VariableJSF> variablesFluxChambreAvailables = new HashMap();
    
    @Transient
    final Map<Long, LieuJSF> lieuAvailables = new HashMap();
    
    private VOVariableJSF variableSelected = new VOVariableJSF(null);

    List<VOVariableJSF> variablesSelected = new LinkedList<>();
   
    @Transient
    HelperForm helperForm = new HelperForm();
   
    Properties propertiesNamesSite;
    Properties propertiesDispNames;
    Properties propertiesLieuNames;
    Properties propertiesVariablesNames;
    public UIBeanFluxChambre() {
        super();
    }

    public Map<String, List<VariableJSF>> getAvailablesVariables() {
        return availablesVariables;
    }

    
     public void setIdVariableSelected(Long idVariableSelected) {
        this.idVariableSelected = idVariableSelected;
    }
    public void setAvailablesVariables(Map<String, List<VariableJSF>> availablesVariables) {
        this.availablesVariables = availablesVariables;
    }

    
    

    
    public String navigate() {
        return "fluxchambre";
    }
   

      @Override
    public String nextStep() {
        super.nextStep();
        switch (this.getStep()) {
            case 2:
                try {
                    this.createOrUpdateVariablesAvailables();
                } catch (final BusinessException e) {
                    
                    this.variablesFluxChambreAvailables.clear();
                }
                break;
            default:
                break;
        }
        return null;
    }


    void createOrUpdateVariablesAvailables() throws BusinessException {
        //getParametersRequest().getVariablesSelected().clear();
        if (!this.getParametersRequest().getDispositifsSelected().isEmpty()) {
            this.variablesFluxChambreAvailables.clear();
            for (final DatatypeVariableUnitePRO variable : this.fluxchambreDatasetManager
                    .getAvailablesVariablesFluxChambreForDispositifs(this.getParametersRequest().getDispositifsSelected())) {
                this.variablesFluxChambreAvailables.put(variable.getDvu_id(), new VariableJSF(variable, FluxChambreParameterVO.FLUXCHAMBRE));
            }
           
        }
    }

    public Map<Long, LieuJSF> getLieuAvailables() throws BusinessException {
        if (this.lieuAvailables.isEmpty()) {
            final List<Lieu> lieux = this.fluxchambreDatasetManager.getDispositifAvailableLieu();
            for (final Lieu lieu : lieux) {
                final List<Dispositif> dispositifs = this.fluxchambreDatasetManager.retrieveFluxChambreAvailablesDispositifsById(lieu.getId());
                final LieuJSF lieuJSF = new LieuJSF(lieu, dispositifs);
                this.lieuAvailables.put(lieu.getId(), lieuJSF);
            }
        }
        return this.lieuAvailables;
    }

    public String addAllDepths() {
        return null;
    }

    public Long getIdVariableSelected() {
        return this.idVariableSelected;
    }

    public String selectDispositif(Long idLieuSelected, Long idDispSelected) throws BusinessException {
        final DispositifJSF dispositifSelected = this.lieuAvailables.get(idLieuSelected).
                getDispositifs().get(idDispSelected);
        if (dispositifSelected.getSelected()) {
            this.getParametersRequest().getDispositifsSelected().remove(idDispSelected);
            dispositifSelected.setSelected(false);
        } else {
            this.getParametersRequest().getDispositifsSelected().put(idDispSelected, dispositifSelected.getDispositif());
            dispositifSelected.setSelected(true);
        }
        return null;
    }

    public void selectVariable() {
        Map<Long, VariableVO> variablesSelected = null;
        VariableJSF variable = null;
        if(this.variablesFluxChambreAvailables.containsKey(this.idVariableSelected)){
            variablesSelected = this.parametersRequest.variablesFluxChambreSelected;
            variable = this.variablesFluxChambreAvailables.get(this.idVariableSelected);
        }
        if (variable != null) {
            if (variable.selected) {
                variablesSelected.remove(this.idVariableSelected);
                variable.setSelected(Boolean.FALSE);
            } else {
                variablesSelected.put(this.idVariableSelected, variable.getVariable());
                variable.setSelected(Boolean.TRUE);
            }
        }
    }

    public ParametersRequest getParametersRequest() {
        return this.parametersRequest;
    }

    void initProperties() {
        this.propertiesDispNames = this.localizationManager.newProperties(Dispositif.NAME_ENTITY_JPA,
                "nom");
        this.propertiesVariablesNames = this.localizationManager.newProperties(
                VariablesPRO.NAME_ENTITY_JPA, "nom");
    }

   

    
    protected Map<String, String> buildNewMapPeriod() {
        final Map<String, String> firstMap = new HashMap();
        firstMap.put(AbstractDatesFormParam.START_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        firstMap.put(AbstractDatesFormParam.END_INDEX, org.apache.commons.lang.StringUtils.EMPTY);
        return firstMap;
    }

    @SuppressWarnings("static-access")
    public String extract() throws BusinessException {
        final Map<String, Object> metadatasMap = new HashMap();
        metadatasMap.put(Dispositif.class.getSimpleName(),
                this.parametersRequest.getListDispositifsSelected());

       final List<VariablesPRO> availablesVariables = new LinkedList<>();
        parametersRequest.getListVariablesFluxChambreSelected().stream().forEach((variable) -> {
            availablesVariables.add(variable.getVariable().getVariablespro());
        });
        metadatasMap.put(VariablesPRO.class.getSimpleName().concat(FluxChambreParameterVO.FLUXCHAMBRE), availablesVariables);
        
        metadatasMap.put(DatesForm1ParamVO.class.getSimpleName(), this.parametersRequest.getDatesForm1ParamVO());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, this.parametersRequest.getCommentExtraction());

        final FluxChambreParameterVO parameters = new FluxChambreParameterVO(metadatasMap);
        Integer aff = Integer.parseInt(affichage);
        this.extractionManager.extract(parameters, aff);
        return null;
    }

    public void setIsStepValid(boolean b) {

    }

    @Override
    public boolean getIsStepValid() {
        if (this.getStep() == 1) {
            return this.getParametersRequest().getDispositifStepIsValid();
        } else if (this.getStep() == 2) {
            return this.getParametersRequest().getVariableStepIsValid();
        }

        return false;
    }

    public String addAllVariables() {
        this.addAllVariablesFluxChambre();
        return null;
    }
    
    public String addAllVariablesFluxChambre(){
    final Map<Long, VariableVO> variablesSelected = this.getParametersRequest()
                .getVariablesFluxChambreSelected();
        variablesSelected.clear();

        this.variablesFluxChambreAvailables
                .entrySet().stream().map((variableEntry) -> {
                    variablesSelected.put(variableEntry.getKey(), variableEntry.getValue().getVariable());
                    return variableEntry;
                }).forEach((variableEntry) -> {
                    variableEntry.getValue().setSelected(Boolean.TRUE);
                });
        return null;
    }

    public String removeAllVariables() throws BusinessException {
       this.parametersRequest.getVariablesFluxChambreSelected().clear();
        this.variablesFluxChambreAvailables.entrySet().stream().forEach((variableEntry) -> {
                    variableEntry.getValue().setSelected(Boolean.FALSE);
                });
    
        return null;
 
    }

 

    public Properties getPropertiesVariablesNames() {
        if (this.propertiesVariablesNames == null) {
            this.setPropertiesVariablesNames(this.localizationManager.newProperties(
                    VariablesPRO.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesVariablesNames;
    }

    public void setPropertiesVariablesNames(final Properties propertiesVariablesNames) {
        this.propertiesVariablesNames = propertiesVariablesNames;
    }


    public Properties getPropertiesAgrosNames() {
        if (this.propertiesLieuNames == null) {
            this.setPropertiesLieuNames(this.localizationManager.newProperties(
                    Dispositif.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesLieuNames;
    }

    public void setAvailablesDispositif(Map<String, TreeNodeItem> availablesDispositif) {
        this.availablesDispositif = availablesDispositif;
    }


    public void setPropertiesLieuNames(Properties propertiesLieuNames) {
        this.propertiesLieuNames = propertiesLieuNames;
    }


    public Properties getPropertiesDispNames() {
        if (this.propertiesDispNames == null) {
            this.setPropertiesDispNames(this.localizationManager.newProperties(
                    Dispositif.NAME_ENTITY_JPA, "nom"));
        }
        return this.propertiesDispNames;
    }

    public void setPropertiesDispNames(Properties propertiesDispNames) {
        this.propertiesDispNames = propertiesDispNames;
    }

    
    
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setFluxchambreDatasetManager(IFluxChambreDatasetManager fluxchambreDatasetManager) {
        this.fluxchambreDatasetManager = fluxchambreDatasetManager;
    }


    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    public void setSecurityContext(ISecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    public JpaTransactionManager getTransactionManager() {
        return transactionManager;
    }

    public void setTransactionManager(JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public void setParametersRequest(ParametersRequest parametersRequest) {
        this.parametersRequest = parametersRequest;
    }
      
    public IFluxChambreDatasetManager getFluxchambreDatasetManager() {
        return fluxchambreDatasetManager;
    }

    public ISecurityContext getSecurityContext() {
        return securityContext;
    }
 

    public Boolean getFrequenceRequired() {
        return frequenceRequired;
    }

    public void setFrequenceRequired(Boolean frequenceRequired) {
        this.frequenceRequired = frequenceRequired;
    }

    public IExtractionManager getExtractionManager() {
        return extractionManager;
    }

    public Map<String, TreeNodeItem> getAvailablesDispositif() {
        return availablesDispositif;
    }



    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public INotificationsManager getNotificationsManager() {
        return notificationsManager;
    }

    public Properties getPropertiesLieuNames() {
        return propertiesLieuNames;
    }

    public Properties getPropertiesNamesSite() {
        return propertiesNamesSite;
    }

    public void setPropertiesNamesSite(Properties propertiesNamesSite) {
        this.propertiesNamesSite = propertiesNamesSite;
    }

    public Long getIdFluxChambreVariableSelected() {
        return idFluxChambreVariableSelected;
    }

    public void setIdFluxChambreVariableSelected(Long idFluxChambreVariableSelected) {
        this.idFluxChambreVariableSelected = idFluxChambreVariableSelected;
    }

    public VOVariableJSF getVariableSelected() {
        return variableSelected;
    }

    public void setVariableSelected(VOVariableJSF variableSelected) {
        this.variableSelected = variableSelected;
    }

    public List<VOVariableJSF> getVariablesSelected() {
        return variablesSelected;
    }

    public void setVariablesSelected(List<VOVariableJSF> variablesSelected) {
        this.variablesSelected = variablesSelected;
    }

    public Map<Long, VariableJSF> getVariablesFluxChambreAvailables() {
        return variablesFluxChambreAvailables;
    }

    public void setVariablesFluxChambreAvailables(Map<Long, VariableJSF> variablesFluxChambreAvailables) {
        this.variablesFluxChambreAvailables = variablesFluxChambreAvailables;
    }

    public String getFluxChambreDatatype() {
        return fluxChambreDatatype;
    }

    public void setFluxChambreDatatype(String fluxChambreDatatype) {
        this.fluxChambreDatatype = fluxChambreDatatype;
    }
    public class VOVariableJSF {
        
        private org.inra.ecoinfo.pro.extraction.vo.VariableVO variable;
        private String localizedVariableName;
        private Boolean selected = false;

        public VOVariableJSF(org.inra.ecoinfo.pro.extraction.vo.VariableVO variable) {
            super();
            this.variable = variable;
            if (variable != null) {
                this.localizedVariableName = getPropertiesVariablesNames().getProperty(
                        variable.getAffichage());
                if (Strings.isNullOrEmpty(this.localizedVariableName)) {
                    this.localizedVariableName = variable.getAffichage();
                }
            }

        }

        public org.inra.ecoinfo.pro.extraction.vo.VariableVO getVariable() {
            return variable;
        }

        public void setVariable(org.inra.ecoinfo.pro.extraction.vo.VariableVO variable) {
            this.variable = variable;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

        public String getLocalizedVariableName() {
            return localizedVariableName;
        }

        public void setLocalizedVariableName(String localizedVariableName) {
            this.localizedVariableName = localizedVariableName;
        }
        
    }

    public class ParametersRequest {

        final Map<Long, Dispositif> dispositifsSelected = new HashMap();
        //private Map<Long, VariableVO> variablesSelected = new HashMap<>();
        final Map<Long, VariableVO> variablesFluxChambreSelected = new HashMap();
        private DatesRequestParamVO datesRequestParam;
        DatesForm1ParamVO datesForm1ParamVO;
        String commentExtraction;

        public String getCommentExtraction() {
            return this.commentExtraction;
        }

        public DatesForm1ParamVO getDatesForm1ParamVO() {
            if (this.datesForm1ParamVO == null) {
                this.initDatesForm1ParamVO();
            }
            return this.datesForm1ParamVO;
        }

        public Boolean getDateStepIsValid() {
            try {
                if (this.datesForm1ParamVO == null) {
                    return false;
                }
                this.datesForm1ParamVO.getIntervalDate();
                return true;
            } catch (final ParseException | BadExpectedValueException e) {
                return false;
            }
        }

        public boolean getFormIsValid() {
            return this.getDispositifStepIsValid() && this.getVariableStepIsValid()
                    && this.getDateStepIsValid();
        }

        public List<Dispositif> getListDispositifsSelected() {
            return new LinkedList<>(this.dispositifsSelected.values());
        }

        public List<VariableVO> getListVariablesFluxChambreSelected() {
            return new LinkedList<>(this.variablesFluxChambreSelected.values());
        }

        public Map<Long, Dispositif> getDispositifsSelected() {
            return this.dispositifsSelected;
        }

        public Boolean getDispositifStepIsValid() {
            return !this.dispositifsSelected.isEmpty();
        }

        public Boolean getVariableStepIsValid() {
            return (!this.variablesFluxChambreSelected.isEmpty()
                    && this.getDispositifStepIsValid());
        }

        void initDatesForm1ParamVO() {
            this.datesForm1ParamVO = new DatesForm1ParamVO(localizationManager);
        }

        public void setCommentExtraction(final String commentExtraction) {
            this.commentExtraction = commentExtraction;
        }

        public void setDatesForm1ParamVO(final DatesForm1ParamVO datesForm1ParamVO) {
            this.datesForm1ParamVO = datesForm1ParamVO;
        }

        public DatesRequestParamVO getDatesRequestParam() {
            return datesRequestParam;
        }

        public Map<Long, VariableVO> getVariablesFluxChambreSelected() {
            return this.variablesFluxChambreSelected;
        }
    }

    public class VariableJSF {

        boolean selected = false;
        final VariableVO variable;
        String datatype;
      
        public VariableJSF( final DatatypeVariableUnitePRO variable, String datatype) {
            super();
            this.variable = new VariableVO(variable);
            this.datatype = datatype;
        }

        public String getDatatype() {
            return this.datatype;
        }

        public String getLocalizedName() {
            final String localizedName = propertiesVariablesNames.getProperty(variable.variable.getVariablespro().getNom());
            return Strings.isNullOrEmpty(localizedName) ? this.variable.variable.getVariablespro().getNom() : localizedName;
        }

        public boolean getSelected() {
            return this.selected;
        }

        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

        public void setSelected(final boolean selected) {
            this.selected = selected;
        }

        public VariableVO getVariable() {
            return this.variable;
        }
    }

    public class VariableVO {

        String localizedName;
        final DatatypeVariableUnitePRO variable;

        public VariableVO(final DatatypeVariableUnitePRO variable) {
            super();
            this.variable = variable;
            this.setLocalizedName(variable.getVariablespro().getNom());
        }

        public String getAffichage() {
            return this.variable.getVariablespro().getAffichage();
        }

        public String getCode() {
            return this.variable.getVariablespro().getCode();
        }

        public Long getDvu_id() {
            return this.variable.getDvu_id();
        }

        public Long getId() {
            return this.variable.getVariablespro().getId();
        }

        public String getLocalizedName() {
            return this.localizedName;
        }

        public String getNom() {
            return this.variable.getVariablespro().getNom();
        }

        public DatatypeVariableUnitePRO getVariable() {
            return this.variable;
        }

        void setLocalizedName(final String nom) {
            if (nom != null) {
                localizedName = getPropertiesVariablesNames().getProperty(nom);
            }
            if (Strings.isNullOrEmpty(this.localizedName)) {
                this.localizedName = nom;
            }
        }
    }
    public class LieuJSF {

        LieuVO lieu = null;
        final Map<Long, DispositifJSF> dispositifs = new HashMap();

        public LieuJSF(final Lieu lieu, final List<Dispositif> dispositifs) {
            super();
            this.lieu = new LieuVO(lieu);
            for (final Dispositif disp : dispositifs) {
                this.dispositifs.put(disp.getId(), new DispositifJSF(disp));
            }
        }

        public LieuVO getLieu() {
            return this.lieu;
        }

        public Map<Long, DispositifJSF> getDispositifs() {
            return this.dispositifs;
        }

    }
    public class DispositifJSF {

        final Dispositif dispositif;

        private String code;
        private String nom;

        Boolean selected = false;

        public DispositifJSF(final Dispositif dispositif) {
            super();
            this.dispositif = dispositif;
            this.code = dispositif.getCode();
            this.nom = dispositif.getNom();
        }

        public Dispositif getDispositif() {
            return dispositif;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(final Boolean selected) {
            this.selected = selected;
        }

    }

    public class LieuVO {

        private String nom;
        private Lieu lieu;
        private String localizedName;

        public LieuVO(final Lieu lieu) {
            super();
            this.setNom(lieu.getNom());
        }

        public String getLocalizedName() {
            return this.localizedName;
        }

        public String getNom() {
            return this.nom;
        }

        void setNom(final String nom) {
            this.nom = nom;
            if (nom != null) {
                localizedName = getPropertiesAgrosNames().getProperty(nom);
            }
            if (Strings.isNullOrEmpty(this.localizedName)) {
                this.localizedName = nom;
            }
        }
    }
    public class DispositifVO {

        private Dispositif dispositif;
        private String localizedDispositifName;
        private String localizedSiteName;
        private Boolean selected = false;
        private Boolean type = true;

        public DispositifVO(Dispositif dispositif) {
            super();
            this.dispositif = dispositif;
            if (dispositif != null) {
                this.localizedDispositifName = getPropertiesDispNames().getProperty(dispositif.getNom());
                if (Strings.isNullOrEmpty(this.localizedDispositifName)) {
                    this.localizedDispositifName = dispositif.getNom();
                }

            }
        }
    }

   

  
  

}
