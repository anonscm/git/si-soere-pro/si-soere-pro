package org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.ICaracteristiqueValeurDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.ITypeCaracteristiqueDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

import com.Ostermiller.util.CSVParser;


public class Recorder extends AbstractCSVMetadataRecorder<CaracteristiqueMatierePremieres> {

    private static final String PROPERTY_MSG_CARACTMP_BAD_NOMTYPECMP = "PROPERTY_MSG_CARACTMP_BAD_NOMTYPECMP";

    protected ICaracteristiqueMatierePremiereDAO cmpDAO;

    protected ITypeCaracteristiqueDAO typecaracteristiqueDAO;

    protected ICaracteristiqueValeurDAO cvDAO;

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    private String[] listeTcarPossibles;


    private void createCMP(final CaracteristiqueMatierePremieres cmp) throws PersistenceException {
        cmpDAO.saveOrUpdate(cmp);
        cmpDAO.flush();
    }

    private void createOrUpdateCMP(final String code,String nom, TypeCaracteristiques typec,final CaracteristiqueMatierePremieres dbcmp) throws PersistenceException {
        if (dbcmp == null) {
             final CaracteristiqueMatierePremieres cmp = cmpDAO.getByNameCMP(nom, typec);
             cmp.setCmp_code(code);
             cmp.setCmp_nom(nom);
             cmp.setTypecaracteristique(typec);
            createCMP(cmp);
        } else {
            updateBDCMP(code, nom, typec, dbcmp);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String type = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                TypeCaracteristiques dbtcar = typecaracteristiqueDAO.FindbyName(type);
                final CaracteristiqueMatierePremieres dbcmp = cmpDAO.getByNameCMP(nom, dbtcar);
                cmpDAO.remove(dbcmp);
               values = parser.getLine();
               
             }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<CaracteristiqueMatierePremieres> getAllElements() throws PersistenceException {
        
        return cmpDAO.getAll() ;
    }

    public ICaracteristiqueMatierePremiereDAO getCmpDAO() {
        return cmpDAO;
    }

    public ICaracteristiqueValeurDAO getCvDAO() {
        return cvDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CaracteristiqueMatierePremieres cmp) throws PersistenceException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(cmp == null ? 
                AbstractCSVMetadataRecorder.EMPTY_STRING : cmp.getTypecaracteristique().getTcar_nom()!= null ? cmp.getTypecaracteristique().getTcar_nom(): "",
                listeTcarPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(cmp == null ? 
                AbstractCSVMetadataRecorder.EMPTY_STRING : cmp.getCmp_nom(), 
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;

    }

    public ITypeCaracteristiqueDAO getTypecaracteristiqueDAO() {
        return typecaracteristiqueDAO;
    }

    private void getTypeCarPossibles() throws PersistenceException {
        List<TypeCaracteristiques> groupetcar = typecaracteristiqueDAO.getAll(TypeCaracteristiques.class);
        String[] Listetcar = new String[groupetcar.size() + 1];
        Listetcar[0] = "";
        int index = 1;
        for (TypeCaracteristiques tcar : groupetcar) {
            Listetcar[index++] = tcar.getTcar_nom();
        }
        this.listeTcarPossibles = Listetcar;
    }

    @Override
    protected ModelGridMetadata<CaracteristiqueMatierePremieres> initModelGridMetadata() {
        try {
            getTypeCarPossibles();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return super.initModelGridMetadata();

    }
    private void persistCMP(final String cmp_code, final String cmp_nom, TypeCaracteristiques tc) throws PersistenceException, BusinessException {
        final CaracteristiqueMatierePremieres cmp = new CaracteristiqueMatierePremieres(cmp_code, cmp_nom, tc);
        createOrUpdateCMP(cmp_code, cmp_nom, tc, cmp);
        
        
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
      long line=0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CaracteristiqueMatierePremieres.NAME_ENTITY_JPA);
                int index = tokenizerValues.currentTokenIndex();
                final String tcar = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);

                TypeCaracteristiques dbtcar = typecaracteristiqueDAO.FindbyName(tcar);
                if (dbtcar == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CARACTMP_BAD_NOMTYPECMP),line,index, tcar));
                }
                if (!errorsReport.hasErrors()) {
                    persistCMP(code, nom, dbtcar);
                }

                values = parser.getLine();
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    public void setCmpDAO(ICaracteristiqueMatierePremiereDAO cmpDAO) {
        this.cmpDAO = cmpDAO;
    }

    public void setCvDAO(ICaracteristiqueValeurDAO cvDAO) {
        this.cvDAO = cvDAO;
    }

    public void setTypecaracteristiqueDAO(ITypeCaracteristiqueDAO typecaracteristiqueDAO) {
        this.typecaracteristiqueDAO = typecaracteristiqueDAO;
    }
    private void updateBDCMP(final String code,String nom, TypeCaracteristiques typec,final CaracteristiqueMatierePremieres dbcmp) throws PersistenceException {
        dbcmp.setCmp_code(code);
        dbcmp.setCmp_nom(nom);
        dbcmp.setTypecaracteristique(typec);
        cmpDAO.saveOrUpdate(dbcmp);
    }

}
