
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.IMesureSemisPlantationDAO;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import static org.inra.ecoinfo.AbstractJPADAO.LOGGER;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.dataset.sol.JPA.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.sol.JPA.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.refdata.applicationtraitementprclt.IApplicationTraitementParcelleEltDAO;
import org.inra.ecoinfo.pro.refdata.cultures.ICulturesDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.materiel.IMaterielDAO;
import org.inra.ecoinfo.pro.refdata.observationqualitative.IObservationQualitativeDAO;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordSemisPlantation extends AbstractProcessRecord {

    protected static final String BUNDLE_PATH_ITK = "org.inra.ecoinfo.pro.dataset.itk.messages";

    private static final String MSG_ERROR_OBJET_NOT_DB = "MSG_ERROR_OBJET_NOT_DB";
    private static final String MSG_ERROR_FILIERE_NOT_DB = "MSG_ERROR_FILIERE_NOT_DB";
    private static final String MSG_ERROR_AIR_NOT__DB = "MSG_ERROR_AIR_NOT__DB";
    private static final String MSG_ERROR_TEMPERATURE_NOT_DB = "MSG_ERROR_TEMPERATURE_NOT_DB";
    private static final String MSG_ERROR_VITESSE_NOT_DB = "MSG_ERROR_VITESSE_NOT_DB";
    private static final String MSG_ERROR_NIVEAU_NOT__DB = "MSG_ERROR_NIVEAU_NOT__DB";
    private static final String MSG_ERROR_SEMIS_NOT_VARIABLEPRO_DB = "MSG_ERROR_SEMIS_NOT_VARIABLEPRO_DB";
    
     private static final String MSG_ERROR_RATIO_NOT_FOUND_DVU_DB = "MSG_ERROR_RATIO_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_OBJET_NOT_FOUND_DVU_DB = "MSG_ERROR_OBJET_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_FD_NOT_FOUND_DVU_DB = "MSG_ERROR_FD_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_CA_NOT_FOUND_DVU_DB = "MSG_ERROR_CA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_CT_NOT_FOUND_DVU_DB = "MSG_ERROR_CT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_VV_NOT_FOUND_DVU_DB = "MSG_ERROR_VV_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_NA_NOT_FOUND_DVU_DB = "MSG_ERROR_NA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_TRAITEMENT_SEMIS_NOT_FOUND_IN_DB="MSG_ERROR_TRAITEMENT_SEMIS_NOT_FOUND_IN_DB";
    IMesureSemisPlantationDAO<MesureSemisPlantation> mesureSemisPlantationDAO;
    IApplicationTraitementParcelleEltDAO applicationTraitementParcelleEltDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    IUniteproDAO uniteproDAO;
    IPlacetteDAO placetteDAO;
    ICulturesDAO culturesDAO;
    IMaterielDAO materielDAO;
    IObservationQualitativeDAO observationQualitativeDAO;
    IListeItineraireDAO listeItineraireDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    public ProcessRecordSemisPlantation() {
        super();
    }

    private long readLines(final CSVParser parser, final Map<Date, List<SemisPlantationLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException, ParseException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final Date datesemis = DateUtil.getSimpleDateFormatDateUTC().parse(cleanerValues.nextToken());
            final String codedispositif = cleanerValues.nextToken();
            final String codetraitement = cleanerValues.nextToken();
            final String nomparcelle = cleanerValues.nextToken();
            final String nomplacette = cleanerValues.nextToken();
            final String culture = cleanerValues.nextToken();
            final String objetculture = cleanerValues.nextToken();
            final String filieredestination = cleanerValues.nextToken();
            final String varietecepage = cleanerValues.nextToken();
            final float ratio = Float.parseFloat(cleanerValues.nextToken());
            final float quantiteapport = Float.parseFloat(cleanerValues.nextToken());
            final String unite = cleanerValues.nextToken();
            final String materiel1 = cleanerValues.nextToken();
            final String materiel2 = cleanerValues.nextToken();
            final String materiel3 = cleanerValues.nextToken();
            final int largeurtravail = Integer.parseInt(cleanerValues.nextToken());
            final String conditionhumidite = cleanerValues.nextToken();
            final String conditiontemperature = cleanerValues.nextToken();
            final String vitessevent = cleanerValues.nextToken();
            final String observationqualite = cleanerValues.nextToken();
            final String nomobservation = cleanerValues.nextToken();
            final String niveauatteint = cleanerValues.nextToken();
            final String commentaire = cleanerValues.nextToken();

            final SemisPlantationLineRecord line = new SemisPlantationLineRecord(lineCount, codedispositif, codetraitement, nomparcelle, nomplacette, culture, objetculture, filieredestination, datesemis, varietecepage, ratio, quantiteapport, unite, materiel1, materiel2, materiel3, largeurtravail, conditionhumidite, conditiontemperature, vitessevent, observationqualite, nomobservation, niveauatteint, commentaire);
            Date date;
            try {
                date = DateUtil.getSimpleDateFormatDateUTC().parse(
                        DateUtil.getSimpleDateFormatDateUTC().format(datesemis));
                if (!lines.containsKey(date)) {
                    lines.put(date, new LinkedList<>());
                }
                lines.get(date).add(line);
            } catch (ParseException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        datesemis, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void recordErrors(final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<Date, List<SemisPlantationLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<SemisPlantationLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, datasetDescriptorPRO, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | ParseException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            java.util.logging.Logger.getLogger(ProcessRecordSemisPlantation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buildLines(final VersionFile versionFile, DatasetDescriptorPRO datasetDescriptorPRO,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<Date, List<SemisPlantationLineRecord>> lines,
            final SortedSet<SemisPlantationLineRecord> ligneEnErreur) throws PersistenceException,
            ParseException,
            InsertionDatabaseException {
        Iterator<Entry<Date, List<SemisPlantationLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Date, List<SemisPlantationLineRecord>> lineDateEntry = iterator.next();
            Date date = lineDateEntry.getKey();
            for (SemisPlantationLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, datasetDescriptorPRO, sessionProperties);
            }

        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureSemisPlantationDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public void setMesureSemisPlantationDAO(IMesureSemisPlantationDAO<MesureSemisPlantation> mesureSemisPlantationDAO) {
        this.mesureSemisPlantationDAO = mesureSemisPlantationDAO;
    }

    public void setApplicationTraitementParcelleEltDAO(IApplicationTraitementParcelleEltDAO applicationTraitementParcelleEltDAO) {
        this.applicationTraitementParcelleEltDAO = applicationTraitementParcelleEltDAO;
    }

    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    public void setCulturesDAO(ICulturesDAO culturesDAO) {
        this.culturesDAO = culturesDAO;
    }

    public void setMaterielDAO(IMaterielDAO materielDAO) {
        this.materielDAO = materielDAO;
    }

    public void setObservationQualitativeDAO(IObservationQualitativeDAO observationQualitativeDAO) {
        this.observationQualitativeDAO = observationQualitativeDAO;
    }

    private void buildMesure(SemisPlantationLineRecord line, VersionFile versionFile,
            SortedSet<SemisPlantationLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, DatasetDescriptorPRO datasetDescriptorPRO,
            ISessionPropertiesPRO sessionProperties) throws PersistenceException,
            InsertionDatabaseException, ParseException {
        Date datesemis = line.getDatesemisouplantation();
        String codedisp = line.getCodedispositif();
         String keydisp = Utils.createCodeFromString(sessionProperties.getDispositif().getCode());
        final String codeTrait = Utils.createCodeFromString(line.getCodetraitement());
        String codeunique = keydisp + "_" + codeTrait;
        DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByNKey(codeunique);
        if(dbTraitement==null){
             errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_TRAITEMENT_SEMIS_NOT_FOUND_IN_DB), codeTrait,keydisp));
        }
      //  String codeTrait = line.getCodetraitement();
        String nomParcelle = line.getCodeparcelle();
        String nomPlacette = line.getNomplacette();
        String nomCulture = line.getCulture();
        String objetculture = Utils.createCodeFromString(line.getObjectculture());
        String filieredes = Utils.createCodeFromString(line.getFilieredestination());
        String varietécepage = line.getVarieteousepage();
        float ratio = line.getRatio();
        float qtapport = line.getQuantiteapport();
        String unite = line.getUnite();
        String materiel1 = line.getMateriel1();
        String materiel2 = line.getMateriel2();
        String materiel3 = line.getMateriel3();
        float largeurtravail = line.getLargeurtravail();
        String conditionhumidite = Utils.createCodeFromString(line.getConditionhumidite());
        String conditiontemperature = Utils.createCodeFromString(line.getConditiontemperature());
        String vitessevent = Utils.createCodeFromString(line.getVitessevent());
        String observationqualite = Utils.createCodeFromString(line.getObservationqualite());
        String nomobservation = Utils.createCodeFromString(line.getNomobservation());
        String niveauatteint = Utils.createCodeFromString(line.getNiveauatteint());
        String commentaire = line.getCommentaire();
        String datatype = versionFile.getDataset().getLeafNode().getDatatype().getCode();

        String cdatatype = Utils.createCodeFromString(datatype);
        String cratio = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(9));
        String apport = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(10));
        String objet = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(6));
        String filiere = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(7));
        String conditionair = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(16));
        String conditiontemp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(17));
        String vitesse = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(18));
        String largeur = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(15));

       

        ListeItineraire dbObjet = listeItineraireDAO.getByNKeys(objet, objetculture);
        if (dbObjet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_OBJET_NOT_DB), objet, objetculture));
        }

        ListeItineraire dbFiliere = listeItineraireDAO.getByNKeys(filiere, filieredes);
        if (dbFiliere == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_FILIERE_NOT_DB), filiere, filieredes));
        }

        ListeItineraire dbCA = listeItineraireDAO.getByNKeys(conditionair, conditionhumidite);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_AIR_NOT__DB), conditionair, filieredes));
        }

        ListeItineraire dbCT = listeItineraireDAO.getByNKeys( conditiontemp,conditiontemperature);
        if (dbCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_TEMPERATURE_NOT_DB), conditiontemperature, conditiontemp));
        }

        ListeItineraire dbVV = listeItineraireDAO.getByNKeys(vitesse, vitessevent);
        if (dbVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_VITESSE_NOT_DB), vitesse, vitessevent));
        }


        
         MesureSemisPlantation mesureSemisPlantation = getorCreate(datesemis, codedisp, codeTrait, nomParcelle, nomPlacette, nomCulture, dbTraitement, varietécepage, materiel1, materiel2, materiel3, unite, versionFile, nomobservation, commentaire, observationqualite, keydisp, niveauatteint);

        DatatypeVariableUnitePRO dbdvration = dataTypeVariableUnitePRODAO.getSemisKey(datatype, cratio);
        if (dbdvration == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_RATIO_NOT_FOUND_DVU_DB), cdatatype, cratio));
        }

        DatatypeVariableUnitePRO dbdvapport = dataTypeVariableUnitePRODAO.getSemisKey(datatype, apport);
        if (dbdvapport == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_OBJET_NOT_FOUND_DVU_DB), cdatatype, apport));
        }
        
        DatatypeVariableUnitePRO dbdvumobjet = dataTypeVariableUnitePRODAO.getSemisKey(datatype, objet);
        if (dbdvumobjet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_OBJET_NOT_FOUND_DVU_DB), cdatatype, objet));
        }

        DatatypeVariableUnitePRO dbdvuFiliere = dataTypeVariableUnitePRODAO.getSemisKey(datatype, filiere);
        if (dbdvuFiliere == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_FD_NOT_FOUND_DVU_DB), cdatatype, filiere));

        }
        DatatypeVariableUnitePRO dbdvCA = dataTypeVariableUnitePRODAO.getSemisKey(datatype, conditionair);
        if (dbdvCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_CA_NOT_FOUND_DVU_DB), cdatatype, conditionair));

        }
        DatatypeVariableUnitePRO dbdvuCT = dataTypeVariableUnitePRODAO.getSemisKey(datatype, conditiontemp);
        if (dbdvuCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_CT_NOT_FOUND_DVU_DB), cdatatype, conditiontemp));

        }
        DatatypeVariableUnitePRO dbdvuVV = dataTypeVariableUnitePRODAO.getSemisKey(datatype, vitesse);
        if (dbdvuVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_VV_NOT_FOUND_DVU_DB), cdatatype, vitesse));

        }
        DatatypeVariableUnitePRO dbdvumLT = dataTypeVariableUnitePRODAO.getSemisKey(datatype, largeur);
        if (dbdvumLT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_NA_NOT_FOUND_DVU_DB), cdatatype, largeur));

        }

        ValeurSemisPlantation valeurSemisPlantation = new ValeurSemisPlantation(ratio, mesureSemisPlantation, dbdvration);
        valeurSemisPlantation.setValeur(ratio);
        valeurSemisPlantation.setDatatypeVariableUnitePRO(dbdvration);
        valeurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(valeurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation qavaleurSemisPlantation = new ValeurSemisPlantation(qtapport, mesureSemisPlantation, dbdvapport);
        qavaleurSemisPlantation.setValeur(qtapport);
        qavaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvapport);
        qavaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(qavaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation ocvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbObjet, dbdvumobjet);
        ocvaleurSemisPlantation.setListeItineraires(dbObjet);
        ocvaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvumobjet);
        ocvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(ocvaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation fdvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbFiliere, dbdvuFiliere);
        fdvaleurSemisPlantation.setListeItineraires(dbFiliere);
        fdvaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvuFiliere);
        fdvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(fdvaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation cavaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbCA, dbdvCA);
        cavaleurSemisPlantation.setListeItineraires(dbCA);
        cavaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvCA);
        cavaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(cavaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation ctvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbCT, dbdvuCT);
        ctvaleurSemisPlantation.setListeItineraires(dbCT);
        ctvaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvuCT);
        ctvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(ctvaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation vvvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbVV, dbdvuVV);
        vvvaleurSemisPlantation.setListeItineraires(dbVV);
        vvvaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvuVV);
        vvvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(vvvaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
        
        ValeurSemisPlantation navaleurSemisPlantation = new ValeurSemisPlantation(largeurtravail, mesureSemisPlantation, dbdvumLT);
        valeurSemisPlantation.setValeur(largeurtravail);
        navaleurSemisPlantation.setDatatypeVariableUnitePRO(dbdvumLT);
        navaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(navaleurSemisPlantation);
        if(!errorsReport.hasErrors()){
        mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
    }

    private MesureSemisPlantation getorCreate(Date datesemis, String codedisp, final String codeTrait, String nomParcelle, String nomPlacette, String nomCulture, DescriptionTraitement dbTraitement, String varietécepage, String materiel1, String materiel2, String materiel3, String unite, VersionFile versionFile, String nomobservation, String commentaire, String observationqualite, String keydisp, String niveauatteint) throws PersistenceException {
        
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");
        dateString = sdfr.format(datesemis);
        String key = dateString + "_" + codedisp+ "_" + codeTrait+ "_" +nomParcelle+ "_" + nomPlacette+ "_" + nomCulture;
        MesureSemisPlantation mesureSemisPlantation = mesureSemisPlantationDAO.getByKeys(key);
        if (mesureSemisPlantation == null) {
            
            mesureSemisPlantation = new MesureSemisPlantation(datesemis, codedisp, dbTraitement, nomParcelle, nomPlacette, varietécepage, nomCulture, materiel1, materiel2, materiel3, unite, versionFile, nomobservation, nomobservation, commentaire);
            mesureSemisPlantation.setDatedebutmesure(datesemis);
            mesureSemisPlantation.setCodedispositif(codedisp);
            mesureSemisPlantation.setDescriptionTraitement(dbTraitement);
            mesureSemisPlantation.setNomparcelle(nomParcelle);
            mesureSemisPlantation.setNomplacette(nomPlacette);
            mesureSemisPlantation.setNomculture(nomCulture);
            mesureSemisPlantation.setMateriel1(materiel1);
            mesureSemisPlantation.setMateriel2(materiel2);
            mesureSemisPlantation.setMateriel3(materiel3);
            mesureSemisPlantation.setVarietecepage(varietécepage);
            mesureSemisPlantation.setCommentaire(commentaire);
            mesureSemisPlantation.setTypeobservation(observationqualite);
            mesureSemisPlantation.setNomobservation(nomobservation);
            mesureSemisPlantation.setKeymesure(key);
            mesureSemisPlantation.setNiveauatteint(niveauatteint);
        }
        return mesureSemisPlantation;
    }

    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

     @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }
    

}
