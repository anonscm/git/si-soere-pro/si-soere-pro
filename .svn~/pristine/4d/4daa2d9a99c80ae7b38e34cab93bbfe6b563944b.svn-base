/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant;

import java.io.Serializable;
import java.util.Objects;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;

/**
 *
 * @author adiankha
 */
@Entity
@Table(name=DatatypeVariableUnitePRO.NAME_ENTITY_JPA,uniqueConstraints = @UniqueConstraint(columnNames = {DataType.ID_JPA, 
                                                                                                         VariablesPRO.ID_JPA,
                                                                                                         Unitepro.ID_JPA,
                                                                                                         Methode.JPA_ID,
                                                                                                         HumiditeExpression.JPA_ID
                                                                                                            
                                                                                                            
  }))
@DiscriminatorValue("datatypeVariableUnite_pro")
@PrimaryKeyJoinColumn(name = DatatypeVariableUnitePRO.JPA_ID)
public class DatatypeVariableUnitePRO  implements Comparable<DatatypeVariableUnitePRO> ,Serializable{
    private static final long serialVersionUID = 1L;
    public static final String NAME_ENTITY_JPA = "datatypevariableunitepro";
    public static final String JPA_ID = "dvu_id";
    
     @Id
    @Column(name = DatatypeVariableUnitePRO.JPA_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long dvu_id;
    
   @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Methode.JPA_ID, nullable = false)
    private Methode methode;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = HumiditeExpression.JPA_ID, nullable = false)
    private HumiditeExpression humiditeexpression;
    
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = Unitepro.ID_JPA, nullable = false)
    private Unitepro unitepro;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = VariablesPRO.ID_JPA, nullable = false)
    private VariablesPRO variablespro;
    
    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = DataType.ID_JPA, nullable = false)
    private DataType datatype;
    
    @ManyToOne(cascade = { MERGE, PERSIST, REFRESH }, optional = false)
    @JoinColumn(name = DatatypeVariableUnite.ID_JPA, nullable = false)
    DatatypeVariableUnite datatypeVariableUnite;
    
    public DatatypeVariableUnitePRO(){
        super();
    }
    
    public DatatypeVariableUnitePRO(DataType datatype,Unitepro unitepro,VariablesPRO variablespro,Methode methode,HumiditeExpression humidite,
            DatatypeVariableUnite datatypeVariableUnite){
       this.unitepro = unitepro;
       this.variablespro = variablespro;
       this.methode = methode;
       this.humiditeexpression = humidite;
       this.datatypeVariableUnite = datatypeVariableUnite;
    }

    public Methode getMethode() {
        return methode;
    }

    public void setMethode(Methode methode) {
        this.methode = methode;
    }

    public HumiditeExpression getHumiditeexpression() {
        return humiditeexpression;
    }

    public void setHumiditeexpression(HumiditeExpression humiditeexpression) {
        this.humiditeexpression = humiditeexpression;
    }

        

    @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DatatypeVariableUnitePRO other = (DatatypeVariableUnitePRO) obj;
        return !(!Objects.equals(this.getDatatype(), other.getDatatype())
                && !Objects.equals(this.getVariablespro(), other.getVariablespro())
                && !Objects.equals(this.getUnitepro().getCode(), other.getUnitepro().getCode())
                && !Objects.equals(this.getMethode().getMethode_code(), other.getMethode().getMethode_code())
                && !Objects.equals(this.getHumiditeexpression().getCode(), other.getHumiditeexpression().getCode()));
    }

    @Override
    public int hashCode() {
         int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.getDatatype().getCode());
        hash = 311 * hash + Objects.hashCode(this.getVariablespro().getCode());
        hash = 3_111 * hash + Objects.hashCode(this.getUnitepro().getCode());
        hash = 3_1111 * hash + Objects.hashCode(this.getMethode().getMethode_code());
        hash = 3_1111 * hash + Objects.hashCode(this.getHumiditeexpression().getCode());
        
        return hash;
    }
    
     @Override
    public int compareTo(DatatypeVariableUnitePRO o) {
        if (o == null) {
            return -1;
        }
        if (this.getDatatype().compareTo(o.getDatatype()) != 0) {
            return this.getDatatype().compareTo(o.getDatatype());
        }
        if (((Comparable<VariablesPRO>) this.getVariablespro()).compareTo((VariablesPRO) o
                .getVariablespro()) != 0) {
            return ((Comparable<VariablesPRO>) this.getVariablespro()).compareTo((VariablesPRO) o
                    .getVariablespro());
        }
        return this.getUnitepro().getCode().compareTo(o.getUnitepro().getCode());
    }

    public Unitepro getUnitepro() {
        return unitepro;
    }

    public void setUnitepro(Unitepro unitepro) {
        this.unitepro = unitepro;
    }

    public VariablesPRO getVariablespro() {
        return variablespro;
    }

    public void setVariablespro(VariablesPRO variablespro) {
        this.variablespro = variablespro;
    }

    public long getDvu_id() {
        return dvu_id;
    }

    public void setDvu_id(long dvu_id) {
        this.dvu_id = dvu_id;
    }

   

    public DataType getDatatype() {
        return datatype;
    }

    public void setDatatype(DataType datatype) {
        this.datatype = datatype;
    }

    public DatatypeVariableUnite getDatatypeVariableUnite() {
        return datatypeVariableUnite;
    }

    public void setDatatypeVariableUnite(DatatypeVariableUnite datatypeVariableUnite) {
        this.datatypeVariableUnite = datatypeVariableUnite;
    }



  
}
