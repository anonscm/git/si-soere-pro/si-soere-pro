package org.inra.ecoinfo.dataset.physicochimie;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.dataset.UnLoadDatasetFixture;
import org.inra.ecoinfo.pro.ProTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author vkoyao
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional(rollbackFor = Exception.class, readOnly = false)
@TestExecutionListeners(listeners = {ProTransactionalTestFixtureExecutionListener.class})
public class UnLoadProElementaireFixture extends UnLoadDatasetFixture {

    

    /**
     *
     */
    public UnLoadProElementaireFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }
}
