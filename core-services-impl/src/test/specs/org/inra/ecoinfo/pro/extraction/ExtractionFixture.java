package org.inra.ecoinfo.extraction;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.pro.ProTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional(rollbackFor = Exception.class, readOnly = false)
@TestExecutionListeners(listeners = {ProTransactionalTestFixtureExecutionListener.class})
public class ExtractionFixture  {

    

    /**
     *
     */
    public ExtractionFixture() {
        super();
    }
}
