package org.inra.ecoinfo.pro.dataset;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author vkoyao
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
public class LoadDatasetFixture extends org.inra.ecoinfo.dataset.LoadDatasetFixture {

    /**
     *
     */
    public LoadDatasetFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }
}
