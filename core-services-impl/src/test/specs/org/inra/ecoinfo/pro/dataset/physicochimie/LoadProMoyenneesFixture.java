package org.inra.ecoinfo.pro.dataset.physicochimie;
import org.concordion.api.ExpectedToPass;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.dataset.LoadDatasetFixture;
import org.inra.ecoinfo.pro.ProTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vjkoyao
 */

@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ProTransactionalTestFixtureExecutionListener.class})
@ExpectedToPass

/**
 *
 * @author vjkoyao
 */
public class LoadProMoyenneesFixture extends LoadDatasetFixture{
    public LoadProMoyenneesFixture(){
        super();
        MockitoAnnotations.initMocks(this);
    }
}
