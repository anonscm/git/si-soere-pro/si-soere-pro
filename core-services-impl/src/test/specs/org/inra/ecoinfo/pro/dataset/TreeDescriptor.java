package org.inra.ecoinfo.pro.dataset;

/**
 *
 * @author ptcherniati
 */
public class TreeDescriptor {

    

    
    String codeDispositif;
    String themeCode;
    String datatypeCode;
    String codePE;

    /**
     *
     * 
     * @param path
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @param parcelleCode
     */
    public TreeDescriptor(String path) {
        super();
        String[] splitPath = path.split(",");
        this.codeDispositif = splitPath.length>0?splitPath[0]:"";
        this.themeCode = splitPath.length>1?splitPath[1]:"";
        this.datatypeCode = splitPath.length>2?splitPath[2]:"";
        this.codePE = splitPath.length>3?splitPath[3]:"";
    }

    /**
     *
     * 
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     * @param codePE
     * @param parcelleCode
     */
    public TreeDescriptor(String codeDispositif, String codeTheme, String datatypeCode, String codePE) {
        super();
        
        this.codeDispositif = codeDispositif;
        this.themeCode = themeCode;
        this.datatypeCode = datatypeCode;
        this.codePE = codePE;
    }

    /**
     *
     * @return
     */
    public String getCodeDispositif() {
        return codeDispositif;
    }

    /**
     *
     * @return
     */
    public String getThemeCode() {
        return themeCode;
    }

    /**
     *
     * @return
     */
    public String getDatatypeCode() {
        return datatypeCode;
    }

    /**
     *
     * @return
     */
    public String getCodePE() {
        return codePE;
    }
}
