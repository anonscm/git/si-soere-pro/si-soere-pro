/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.activiteindustrielle;
import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.pro.test.utils.MockUtils;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author adiankha
 */
public class RecoderAITest {
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    MockUtils m = MockUtils.getInstance();
    Recorder instance;
    String encoding = "UTF-8";

    @Mock
    Properties propertiesNom;
    
    @Mock
    Activiteindustrielle ai1;
    @Mock
    Activiteindustrielle ai2;

    /**
     *
     */
    public RecoderAITest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        m = MockUtils.getInstance();
        instance = new Recorder();
        MockitoAnnotations.initMocks(this);
        instance.setActiviteDAO(m.activiteDAO);
        instance.setLocalizationManager(m.localizationManager);
        Mockito.when(m.localizationManager.newProperties(Activiteindustrielle.NAME_ENTITY_JPA, Activiteindustrielle.JPA_COLUMN_NAME, Locale.ENGLISH)).thenReturn(propertiesNom);
        Mockito.when(m.activiteindustrielle.getNom()).thenReturn(MockUtils.ACTIVITEINDUSTRIELLE);
        Mockito.when(propertiesNom.getProperty(MockUtils.ACTIVITEINDUSTRIELLE)).thenReturn("activiteEN");
    }
    
    /**
     *
     */
    @Test
      public void testGetActiviteIDAO() {
        instance = Mockito.spy(new Recorder());
        Assert.assertNull("activiteDAO not null", instance.getActiviteDAO());
        instance.setActiviteDAO(m.activiteDAO);
        Assert.assertEquals("activiteDAO not setted", m.activiteDAO, instance.getActiviteDAO());
    }
    
    /**
     *
     * @throws Exception
     */
    @Test
       public void testGetNewLineModelGridMetadata() throws Exception {
        instance.initModelGridMetadata();
        LineModelGridMetadata result = instance.getNewLineModelGridMetadata(m.activiteindustrielle);
        Assert.assertTrue(2 == result.getColumnsModelGridMetadatas().size());
        Assert.assertEquals("activiteindustrielle", result.getColumnModelGridMetadataAt(0).getValue());
        Assert.assertEquals("activiteEN", result.getColumnModelGridMetadataAt(1).getValue());
    }

    /**
     *
     * @throws PersistenceException
     */
    @Test
      public void testInitModelGridMetadata() throws PersistenceException {
        m.localizationManager = Mockito.spy(m.localizationManager);
        instance.setLocalizationManager(m.localizationManager);
        instance.initModelGridMetadata();
        Mockito.verify(m.localizationManager).newProperties(Activiteindustrielle.NAME_ENTITY_JPA, Activiteindustrielle.JPA_COLUMN_NAME, Locale.ENGLISH);
        Assert.assertEquals("propertiesNom not initialized", propertiesNom, instance.getActiviteEN());
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testGetAllElements() throws Exception {
        List<Activiteindustrielle> activite = Arrays.asList(new Activiteindustrielle[]{ai1,ai2});
        Mockito.when(m.activiteDAO.getAll()).thenReturn(activite);
        List<Activiteindustrielle> activiteDb = instance.getAllElements();
        Assert.assertEquals(activite, activiteDb);
        Assert.assertEquals(activite.get(0), ai1);
        Assert.assertEquals(activite.get(1), ai2);    
    }
    
    /**
     *
     * @throws Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        Activiteindustrielle activite = Mockito.mock(Activiteindustrielle.class);
        Mockito.when(activite.getCode()).thenReturn("activiteindustrielle");
        Mockito.when(activite.getNom()).thenReturn("activiteindustrielle");
        String text = "nom_fr;nom_en\n" + "activiteindustrielle;nom_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        ArgumentCaptor<Activiteindustrielle> ts = ArgumentCaptor.forClass(Activiteindustrielle.class);

        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.activiteDAO).saveOrUpdate(ts.capture());
        Mockito.verify(m.activiteindustrielle).setNom("activiteindustrielle");

        // existing etape
        Mockito.when(m.activiteDAO.getByNKey("activiteindustrielle")).thenReturn(Optional.empty());
        ts = ArgumentCaptor.forClass(Activiteindustrielle.class);
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        instance.processRecord(parser, file, encoding);
        Mockito.verify(m.activiteDAO, Mockito.times(2)).saveOrUpdate(ts.capture());
        Assert.assertNotEquals(m.activiteindustrielle, ts.getValue());
        Assert.assertEquals("activiteindustrielle", ts.getValue().getCode());
        Assert.assertEquals("activiteindustrielle", ts.getValue().getNom());
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testDeleteRecorder() throws Exception {
        String text = "Activiteindustrielle;nom_fr;nom_en";
        CSVParser parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        File file = null;
        // nominal case
        Mockito.when(m.activiteDAO.getByNKey("activiteindustrielle")).thenReturn(Optional.ofNullable(m.activiteindustrielle));
        instance.deleteRecord(parser, file, encoding);
        Mockito.verify(m.activiteDAO).remove(m.activiteindustrielle);

        // Persistence exception on getByCode
        text = "Activiteindustrielle;nom_fr;nom_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.when(m.activiteDAO.getByNKey("activiteindustrielle")).thenThrow(new PersistenceException("error"));
        BusinessException error = null;
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error", error.getMessage());
        error = null;

        // Persistence exception on remove
        text = "Activiteindustrielle;nom_fr;nom_en";
        parser = new CSVParser(new ByteArrayInputStream(text.getBytes()), ';');
        Mockito.doReturn(Optional.ofNullable(m.activiteindustrielle)).when(m.activiteDAO).getByNKey("activiteindustrielle");
        Mockito.when(m.activiteDAO.getByNKey("activiteindustrielle")).thenReturn(Optional.ofNullable(m.activiteindustrielle));
        Mockito.doThrow(new PersistenceException("error2")).when(m.activiteDAO).remove(m.activiteindustrielle);
        try {
            instance.deleteRecord(parser, file, encoding);
        } catch (BusinessException e) {
            error = e;
        }
        Assert.assertNotNull("une exception doit être lancée", error);
        Assert.assertEquals("error2", error.getMessage());
    }
    
    
    
}
