package org.inra.ecoinfo.pro.dataset.impl;

import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.mockito.Mock;

/**
 *
 * @author ptcherniati
 */
public class RecorderPROTest {

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {}

   

    @Mock
    DatasetDescriptor datasetDescriptor;

    @Mock
    ILocalizationManager localizationManager;

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {}

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {}

}
