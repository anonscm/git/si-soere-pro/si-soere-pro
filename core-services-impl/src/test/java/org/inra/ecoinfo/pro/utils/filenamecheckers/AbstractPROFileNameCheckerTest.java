
package org.inra.ecoinfo.pro.utils.filenamecheckers;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.site.ISiteDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author ptcherniati
 */
public class AbstractPROFileNameCheckerTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    protected AbstractPROFileNameChecker instance;

    /**
     *
     */
    @Mock
    protected IDatasetConfiguration datasetConfiguration;

    /**
     *
     */
    @Mock
    protected ICoreConfiguration configuration;

    /**
     *
     */
    @Mock
    protected IDataTypeVariableQualifiantDAO dataTypeVariableQualifiantDAO;

    /**
     *
     */
    @Mock
    protected IDispositifDAO dispositifDAO;

    /**
     *
     */
    @Mock
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    @Mock
    protected ISiteDAO siteDAO;

    /**
     *
     */
    @Mock
    protected VersionFile version;

    /**
     *
     */
    @Mock
    protected Dataset dataset;

    /**
     *
     */
    @Mock
    protected RealNode realNode;

    /**
     *
     */
    @Mock
    protected RealNode realNodeDispositif;

    /**
     *
     */
    @Mock
    protected RealNode realNodeDatatype;

    /**
     *
     */
    @Mock
    protected RealNode realNodeParcelle;

    /**
     *
     */
    protected String fileName = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_10-09-2002_10-10-2002.csv";

    /**
     *
     */
    protected String codeDispositifForFile = "QA-feucherolles";

    /**
     *
     */
    protected String codeDispositifLieu = "QA(feucherolles)";

    /**
     *
     */
    protected String codeDispositif = "QA";

    /**
     *
     */
    protected String codeLieu = "feucherolles";

    /**
     *
     */
    protected String codeDatatype = "sols_physico-chimie_donnees_elementaires";

    /**
     *
     */
    protected LocalDateTime dateDeDebut = LocalDateTime.of(2002, Month.SEPTEMBER, 10, 0, 0);

    /**
     *
     */
    protected LocalDateTime dateDeFin = LocalDateTime.of(2002, Month.OCTOBER, 10, 0, 0);

    /**
     *
     */
    @Mock
    protected Variable variable;

    /**
     *
     */
    @Mock
    protected Dispositif dispositif;

    /**
     *
     */
    @Mock
    protected DataType dataType;

    /**
     *
     */
    @Mock
    protected ParcelleElementaire parcelleElementaire;

    /**
     *
     */
    protected String qAfeucherolles_sols_physicochimie_donnees = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_dd-MM-yyyy_dd-MM-yyyy.csv";

    /**
     *
     */
    public AbstractPROFileNameCheckerTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new AbstractPROFileNameCheckerImpl();
        instance = spy(instance);
        MockitoAnnotations.initMocks(this);
        initInstance();
        doReturn("QA-feucherolles").when(dispositif).getCodeforFileName();
        doReturn("sols_physico-chimie_donnees_elementaires").when(dataType).getCode();
        doReturn("1").when(parcelleElementaire).getName();
        doReturn(fileName).when(instance).getFilePath(version);
        doReturn(dataset).when(version).getDataset();
        doReturn(realNode).when(dataset).getRealNode();
        doReturn(realNodeDispositif).when(realNode).getNodeByNodeableTypeResource(Dispositif.class);
        doReturn(dispositif).when(realNodeDispositif).getNodeable();
        doReturn("codeDispo").when(dispositif).getCode();
        doReturn("QA(feucherolles)").when(dispositif).getCode();
        doReturn(realNodeDatatype).when(realNode).getNodeByNodeableTypeResource(DataType.class);
        doReturn(dataType).when(realNodeDatatype).getNodeable();
        doReturn(realNodeParcelle).when(realNode).getNodeByNodeableTypeResource(ParcelleElementaire.class);
        doReturn(parcelleElementaire).when(realNodeParcelle).getNodeable();

        Localization.addLanguage("fr");
        Localization.setDefaultLocalisation("fr");
        IntervalDate.setLocalizationManager(new SpringLocalizationManager());

    }

    /**
     *
     */
    public void initInstance() {
        instance.setDatasetConfiguration(datasetConfiguration);
        instance.setConfiguration(configuration);
        instance.setDataTypeVariableUnitePRODAO(dataTypeVariableQualifiantDAO);
        instance.setDispositifDAO(dispositifDAO);
        instance.setSiteDAO(siteDAO);
        instance.setDatatypeDAO(datatypeDAO);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of isValidFileName method, of class AbstractPROFileNameChecker.
     * @throws java.lang.Exception
     */
    @Test
    public void testIsValidFileName() throws Exception {
        boolean result = instance.isValidFileName(fileName, version);
        assertTrue(result);
    }

    /**
     * Test of testDatatype method, of class AbstractPROFileNameChecker.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestDatatype() throws Exception {
        instance.testDatatype(codeDispositifForFile, codeDatatype, codeDatatype);
        try {
            instance.testDatatype(codeDispositifForFile, codeDatatype, "badDatatype");
            fail();
        } catch (InvalidFileNameException e) {
            assertTrue(qAfeucherolles_sols_physicochimie_donnees.equals(e.getMessage()));
        }

    }

    /**
     * Test of testDispositif method, of class AbstractPROFileNameChecker.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestDispositif() throws Exception {
        instance.testDispositif(version, codeDispositifForFile, codeDatatype, codeDispositifForFile);
        try {
            instance.testDispositif(version, codeDispositifForFile, codeDatatype, "badDispositif");
            fail();
        } catch (InvalidFileNameException e) {
            assertTrue(qAfeucherolles_sols_physicochimie_donnees.equals(e.getMessage()));
        }
    }

    /**
     * Test of testPath method, of class AbstractPROFileNameChecker.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestPath() throws Exception {
        Matcher splitFilename = Pattern.compile(
                String.format("^(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$", codeDispositifForFile, codeDatatype))
                .matcher(fileName);
        instance.testPath(codeDispositifForFile, codeDatatype, splitFilename);
        try {
            instance.testPath(codeDispositifForFile, codeDatatype, splitFilename);
            fail();
        } catch (InvalidFileNameException e) {
            assertTrue(qAfeucherolles_sols_physicochimie_donnees.equals(e.getMessage()));
        }
    }

    /**
     * Test of getDatatypeFromVersion method, of class
     * AbstractPROFileNameChecker.
     */
    @Test
    public void testGetDatatypeFromVersion() {
        DataType result = RecorderPRO.getDatatypeFromVersion(version);
        assertEquals(dataType, result);
    }

    /**
     * Test of getDispositifFromVersion method, of class
     * AbstractPROFileNameChecker.
     */
    @Test
    public void testGetDispositifFromVersion() {
        Dispositif result = RecorderPRO.getDispositifFromVersion(version);
        assertEquals(dispositif, result);
    }

    /**
     *
     */
    public class AbstractPROFileNameCheckerImpl extends AbstractPROFileNameChecker {

        /**
         *
         */
        protected AbstractPROFileNameCheckerImpl() {

        }

        @Override
        protected String getDatePattern() {
            return DateUtil.DD_MM_YYYY_FILE;
        }

        @Override
        protected void testDates(VersionFile version, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {

        }

        /**
         *
         * @param dataset
         * @return
         */
        @Override
        public String getFilePath(Dataset dataset) {
            return "";
        }
    }
}
