
package org.inra.ecoinfo.pro.utils.filenamecheckers;

import org.inra.ecoinfo.utils.DateUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckerddmmyyyyTest extends AbstractPROFileNameCheckerTest {
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public FileNameCheckerddmmyyyyTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        super.setUp();
        instance = new FileNameCheckerddmmyyyy();
        super.initInstance();
        doReturn(codeDispositifLieu).when(dispositif).getCodeDispo();
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDatePattern method, of class FileNameCheckerddmmyyyy.
     */
    @Test
    public void testGetDatePattern() {
        String result = ((FileNameCheckerddmmyyyy) instance).getDatePattern();
        assertEquals(DateUtil.DD_MM_YYYY_FILE, result);
    }
    
}
