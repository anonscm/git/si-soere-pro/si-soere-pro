/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils.filenamecheckers;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.utils.DateUtil;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckerParcelleddmmyyyyTest extends AbstractPROFileNameCheckerTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    protected String fileName = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_10-09-2002_10-10-2002.csv";

    /**
     *
     */
    protected String codeParcelle = "1";

    /**
     *
     */
    public FileNameCheckerParcelleddmmyyyyTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        super.setUp();
        instance = new FileNameCheckerParcelleddmmyyyy();
        super.initInstance();
        doReturn(codeDispositifLieu).when(dispositif).getCodeDispo();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDatePattern method, of class FileNameCheckerParcelleddmmyyyy.
     */
    @Test
    public void testGetDatePattern() {
        String result = ((FileNameCheckerParcelleddmmyyyy) instance).getDatePattern();
        assertEquals(DateUtil.DD_MM_YYYY_FILE, result);
    }

    /**
     * Test of testDates method, of class FileNameCheckerParcelleddmmyyyy.
     * @throws java.lang.Exception
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testTestDates_4args() throws Exception {
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        final Matcher splitFilename = Pattern.compile(
                String.format(FileNameCheckerParcelleddmmyyyy.PATTERN, codeDispositifForFile,
                        codeDatatype, codeParcelle)).matcher(fileName);
        splitFilename.find();
        instance.testDates(version, codeDispositifForFile, codeDatatype, splitFilename);
    }

    /**
     * Test of testDates method, of class FileNameCheckerParcelleddmmyyyy.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestDates_5args() throws Exception {
        initializeTestdates();
        ArgumentCaptor<LocalDateTime> dd = ArgumentCaptor.forClass(LocalDateTime.class);
        ArgumentCaptor<LocalDateTime> df = ArgumentCaptor.forClass(LocalDateTime.class);
        verify(dataset).setDateDebutPeriode(dd.capture());
        assertEquals(dateDeDebut, dd.getValue());
        verify(dataset).setDateFinPeriode(df.capture());
        assertEquals(dateDeFin, df.getValue());
        //bad interval date
        fileName = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_10-09-2004_10-10-2002.csv";
        try {
            initializeTestdates();
            fail();
        } catch (InvalidFileNameException e) {
            assertEquals("QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_dd-MM-yyyy_dd-MM-yyyy.csv", e.getMessage());
        }

    }

    void initializeTestdates() throws InvalidFileNameException {
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        final Matcher splitFilename = Pattern.compile(
                String.format(FileNameCheckerParcelleddmmyyyy.PATTERN, codeDispositifForFile,
                        codeDatatype, codeParcelle)).matcher(fileName);
        assertTrue(splitFilename.find());
        instance.testDates(version, codeDispositifForFile, codeDatatype, codeParcelle, splitFilename);
    }

    /**
     * Test of testParcelle method, of class FileNameCheckerParcelleddmmyyyy.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestParcelle() throws Exception {
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        instance.testParcelle(version, codeDispositifForFile, codeDatatype, codeParcelle, codeParcelle);
        try {
            instance.testParcelle(version, codeDispositifForFile, codeDatatype, codeParcelle, "bad parcelle");
            fail();
        } catch (InvalidFileNameException e) {
            assertEquals("QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_dd-MM-yyyy_dd-MM-yyyy.csv", e.getMessage());
        }
    }

    /**
     * Test of testPath method, of class FileNameCheckerParcelleddmmyyyy.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestPath() throws Exception {
        initTestPath();
        //bad file name
        fileName = "QAfeucherllessolsphysico-chimiedonneeselementaires10-09-2004_10-10-2002.csv";
        try {
            initTestPath();
            fail();
        } catch (InvalidFileNameException e) {
            assertEquals("QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_dd-MM-yyyy_dd-MM-yyyy.csv", e.getMessage());
        }
    }

    void initTestPath() throws InvalidFileNameException {
        doReturn(Optional.ofNullable(dispositif)).when(dispositifDAO).getByNKey(codeDispositif, codeLieu);
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        final Matcher splitFilename = Pattern.compile(
                String.format(FileNameCheckerParcelleddmmyyyy.PATTERN, codeDispositifForFile,
                        codeDatatype, codeParcelle)).matcher(fileName);
        instance.testPath(codeDispositifForFile, codeDatatype, codeParcelle, splitFilename);
    }

    /**
     * Test of getFilePath method, of class FileNameCheckerParcelleddmmyyyy.
     */
    @Test
    public void testGetFilePath() {
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        doReturn(dateDeDebut).when(dataset).getDateDebutPeriode();
        doReturn(dateDeFin).when(dataset).getDateFinPeriode();
        doReturn(5L).when(version).getVersionNumber();
        String result = instance.getFilePath(version);
        assertEquals("QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_10-09-2002_10-10-2002#V5#.csv", result);
    }

    /**
     * Test of isValidFileName method, of class FileNameCheckerParcelleddmmyyyy.
     * @throws java.lang.Exception
     */
    @Test
    public void testIsValidFileName() throws Exception {
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        instance = Mockito.spy(instance);
        
        doReturn(Optional.ofNullable(dispositif)).when(dispositifDAO).getByNKey(codeDispositif, codeLieu);
        doNothing().when(instance).testDisp(version, codeDispositifForFile, codeDatatype, codeParcelle, codeDispositifForFile);
        doNothing().when(instance).testDatatype(codeDispositifForFile, codeDatatype, codeParcelle, codeDatatype);
        doNothing().when(instance).testParcelle(version, codeDispositifForFile, codeDatatype, codeParcelle, fileName);
        doNothing().when(instance).testDates(eq(version), eq(codeDispositifForFile), eq(codeDatatype), eq(codeParcelle), any(Matcher.class));
        ArgumentCaptor<Matcher> matcher = ArgumentCaptor.forClass(Matcher.class);
        boolean result = instance.isValidFileName(fileName, version);
        verify(instance).testDisp(version, codeDispositifForFile, codeDatatype, codeParcelle, codeDispositifForFile);
        verify(instance).testDatatype(codeDispositifForFile, codeDatatype, codeParcelle, codeDatatype);
        verify(instance).testParcelle(version, codeDispositifForFile, codeDatatype, codeParcelle, codeParcelle);
        verify(instance).testDates(eq(version), eq(codeDispositifForFile), eq(codeDatatype), eq(codeParcelle), matcher.capture());
        assertEquals("^(QA-feucherolles|.*?)_(sols_physico-chimie_donnees_elementaires|.*?)_(1|.*?)_(.*)_(.*)\\.csv$", matcher.getValue().pattern().toString());
        assertEquals(true, result);
    }

    /**
     * Test of testDatatype method, of class FileNameCheckerParcelleddmmyyyy.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestDatatype() throws Exception {
        FileNameCheckerParcelleddmmyyyy instance = (FileNameCheckerParcelleddmmyyyy) this.instance;
        doReturn(Optional.ofNullable(dataType)).when(datatypeDAO).getByCode(codeDatatype);
        doReturn(Optional.ofNullable(codeDatatype)).when(dataTypeVariableQualifiantDAO).getNameVariableForDatatypeName(codeDatatype);
        instance.testDatatype(codeDispositifForFile, codeDatatype, codeParcelle, codeDatatype);
        try {
            instance.testDatatype(codeDispositifForFile, codeDatatype, codeParcelle, "bad datatype");
            fail();
        } catch (InvalidFileNameException e) {
            assertEquals("QA-feucherolles_sols_physico-chimie_donnees_elementaires_1_dd-MM-yyyy_dd-MM-yyyy.csv", e.getMessage());
        }
    }

}
