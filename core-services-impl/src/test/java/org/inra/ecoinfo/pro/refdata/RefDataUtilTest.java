/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata;

import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * 
 * @author ptcherniati
 */
public class RefDataUtilTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {}

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {}

    /**
     *
     */
    public RefDataUtilTest() {}

    /**
     *
     */
    @Before
    public void setUp() {}

    /**
     *
     */
    @After
    public void tearDown() {}

    /**
     * Test of verifieEmail method, of class RefDataUtil.
     * @throws java.lang.Exception
     */
    @Test
    public void testVerifieEmail() throws Exception {
        String eMail = "";
        AbstractCSVMetadataRecorder.ErrorsReport errorsReport = null;
        ILocalizationManager localizationManager = null;
        RefDataUtil.verifieEmail(eMail, errorsReport, localizationManager);
    }

    /**
     * Test of verifieNotelNoFax method, of class RefDataUtil.
     * @throws java.lang.Exception
     */
    @Test
    public void testVerifieNotelNoFax() throws Exception {
        String noTel = "";
        String noFax = "";
        AbstractCSVMetadataRecorder.ErrorsReport errorsReport = null;
        ILocalizationManager localizationManager = null;
        RefDataUtil.verifieNotelNoFax(noTel, noFax, errorsReport, localizationManager);
    }

}
