package org.inra.ecoinfo.pro.test.utils;



import java.text.ParseException;
import java.util.Arrays;
import java.util.Optional;
import org.inra.ecoinfo.localization.ILocalizationDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.localization.entity.UserLocale;
import org.inra.ecoinfo.localization.impl.SpringLocalizationManager;
import org.inra.ecoinfo.pro.refdata.FormePhysique.FormePhysiques;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits;
import org.inra.ecoinfo.pro.refdata.activiteindustrielle.Activiteindustrielle;
import org.inra.ecoinfo.pro.refdata.activiteindustrielle.IActiviteIndustrielleDAO;
import org.inra.ecoinfo.pro.refdata.amenagementavantplantation.AmenagementAvantPlantation;
import org.inra.ecoinfo.pro.refdata.amenagementavantplantation.IAmenagementAvantPlantationDAO;
import org.inra.ecoinfo.pro.refdata.annees.Annees;
import org.inra.ecoinfo.pro.refdata.annees.IAnneesDAO;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.CaracteristiqueEtapes;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.ICaracteristiqueEtapeDAO;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.CaracteristiqueValeur;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.ICaracteristiqueValeurDAO;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.etape.IEtapeDAO;
import org.inra.ecoinfo.pro.refdata.faitremarquable.Faitremarquable;
import org.inra.ecoinfo.pro.refdata.faitremarquable.IFaitRemarquableDAO;
import org.inra.ecoinfo.pro.refdata.formephysique.IFormePhysiqueDAO;
import org.inra.ecoinfo.pro.refdata.grandtypeproduit.IGrandTypeProduitDAO;
import org.inra.ecoinfo.pro.refdata.heterogeinite.Heterogeinite;
import org.inra.ecoinfo.pro.refdata.heterogeinite.IHeterogeiniteDAO;
import org.inra.ecoinfo.pro.refdata.methodeetape.IMethodeEtapeDAO;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.methodeprocess.IMethodeProcessDAO;
import org.inra.ecoinfo.pro.refdata.methodeprocess.MethodeProcess;
import org.inra.ecoinfo.pro.refdata.origine.IOrigineDAO;
import org.inra.ecoinfo.pro.refdata.origine.Origines;
import org.inra.ecoinfo.pro.refdata.pays.IPaysDAO;
import org.inra.ecoinfo.pro.refdata.pays.Pays;
import org.inra.ecoinfo.pro.refdata.pente.IPenteDAO;
import org.inra.ecoinfo.pro.refdata.pente.Pente;
import org.inra.ecoinfo.pro.refdata.process.IProcessDAO;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.statut.IStatutDAO;
import org.inra.ecoinfo.pro.refdata.statut.Statut;
import org.inra.ecoinfo.pro.refdata.substratpedologique.ISubstratPedologiqueDAO;
import org.inra.ecoinfo.pro.refdata.substratpedologique.Substratpedologique;
import org.inra.ecoinfo.pro.refdata.systemeprojection.ISystemeProjectionDAO;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.ITeneurCalcaireInitialDAO;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.Teneurcalcaireinitial;
import org.inra.ecoinfo.pro.refdata.texturesol.ITextureSolDAO;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.ITypeCaracteristiqueDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;
import org.inra.ecoinfo.pro.refdata.typeclimat.ITypeClimatDAO;
import org.inra.ecoinfo.pro.refdata.typeclimat.Typeclimat;
import org.inra.ecoinfo.pro.refdata.typedocument.ITypeDocumentDAO;
import org.inra.ecoinfo.pro.refdata.typedocument.TypeDocument;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typepedologique.ITypePedologiqueDAO;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.ITypeSolArvalisDAO;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis;
import org.inra.ecoinfo.pro.refdata.unites.IUniteDAO;
import org.inra.ecoinfo.pro.refdata.unites.Unites;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class MockUtils {
    
    /**
     *
     */
    public static final String ORIGINE = "origine" ;

    /**
     *
     */
    public static String ORIGINE_CODE = "origine" ;

    /**
     *
     */
    public static String ORIGINE_NOM = "origine" ;

    /**
     *
     */
    public static String ORIGINE_COMMENT = "origine" ;

    /**
     *
     */
    public static final String ETAPE = "etape" ;

    /**
     *
     */
    public static String ETAPE_CODE="etape" ;

    /**
     *
     */
    public static String ETAPE_NOM = "etape" ;

    /**
     *
     */
    public static final String PROCESS = "proces" ;

    /**
     *
     */
    public static String PROCESS_CODE = "proces" ;

    /**
     *
     */
    public static String PROCESS_NOM = "proces" ;

    /**
     *
     */
    public static final String METHODEPROCESS = "methodeprocess" ;

    /**
     *
     */
    public static String MPROCESS_CODE = "methodeprocess" ;

    /**
     *
     */
    public static String MPROCESS_NOM = "methodeprocess" ;

    /**
     *
     */
    public static final String ANNEE ="annee" ;

    /**
     *
     */
    public static String ANNEE_VALEUR = "annee" ;

    /**
     *
     */
    public static final String CARACTERISITIQUEETAPE="caracteristiqueetape" ;

    /**
     *
     */
    public static String CETAPE_CODE = "caracteristiqueetape" ;

    /**
     *
     */
    public static String CETAPE_INTITULE = "caracteristiqueetape" ;

    /**
     *
     */
    public static final String STATUT ="statut" ;

    /**
     *
     */
    public static String STATUT_CODE = "statut" ;

    /**
     *
     */
    public static String STATUT_NOM = "statut" ;

    /**
     *
     */
    public static final String FORMEPHYSIQUE ="formephysique" ;

    /**
     *
     */
    public static String FORMEPHYSIQUE_CODE = "formephysique" ;

    /**
     *
     */
    public static String FORMEPHYSIQUE_NOM = "formephysique" ;

    /**
     *
     */
    public static final String UNITE = "unite" ;

    /**
     *
     */
    public static String UNITE_CODE = "unite" ;

    /**
     *
     */
    public static String UNITE_NOM = "unite" ;

    /**
     *
     */
    public static final String CARACTERISTIQUEVALEUR = "caracteristiquevaleur" ;

    /**
     *
     */
    public static String CV_CODE = "caracteristiquevaleur" ;

    /**
     *
     */
    public static String CV_NOM = "caracteristiquevaleur" ;

    /**
     *
     */
    public static final String TYPECARACTMP="typecaracteristique" ;

    /**
     *
     */
    public static String TC_NOM ="typecaracteristique" ;

    /**
     *
     */
    public static String TC_CODE ="typecaracteristique" ;

    /**
     *
     */
    public static final String METHODEETAPE ="methodeetape" ;

    /**
     *
     */
    public static String ME_CODE = "methodeetape" ;

    /**
     *
     */
    public static String ME_NOM = "methodeetape" ;

    /**
     *
     */
    public static final String PENTE = "pente" ;

    /**
     *
     */
    public static String CODE = "pente" ;

    /**
     *
     */
    public static String NOM = "pente" ;

    /**
     *
     */
    public static final String HETEROGEINITE = "heterogeinite" ;

    /**
     *
     */
    public static String HE_CODE = "heterogeinite" ;

    /**
     *
     */
    public static String HE_NOM = "heterogeinite" ;

    /**
     *
     */
    public static final String FAITREMARQUABLE="faitremarquable" ;

    /**
     *
     */
    public static String FR_CODE = "faitremarquable" ;

    /**
     *
     */
    public static String FR_NOM = "faitremarquable" ;

    /**
     *
     */
    public static final String TYPESOLARVALIS="typesolarvalis" ;

    /**
     *
     */
    public static String TSA_CODE = "typesolarvalis" ;

    /**
     *
     */
    public static String TSA_NOM = "typesolarvalis" ;

    /**
     *
     */
    public static final String TYPEPEDOLOGIQUE ="typepedologique" ;

    /**
     *
     */
    public static String TP_CODE ="typepedologique" ;

    /**
     *
     */
    public static String TP_NOM ="typepedologique" ;

    /**
     *
     */
    public static final String SUBSTRATPEDOLOGIQUE = "substratpedologique" ;

    /**
     *
     */
    public static String SP_CODE ="substratpedologique" ;

    /**
     *
     */
    public static String SP_NOM = "substratpedologique" ;

    /**
     *
     */
    public static final String ACTIVITEINDUSTRIELLE = "activiteindustrielle" ;

    /**
     *
     */
    public static String AI_CODE="activiteindustrielle" ;

    /**
     *
     */
    public static String AI_NOM="activiteindustrielle" ;

    /**
     *
     */
    public static final String TYPECLIMAT ="typeclimat" ;

    /**
     *
     */
    public static String TCL_CODE ="typeclimat" ;

    /**
     *
     */
    public static String TCL_NOM ="typeclimat" ;

    /**
     *
     */
    public static final String TENEURCALCAIREINITIAL ="teneurcalcaireinitial" ;
    public static final String TEXTURESOL ="texturesol" ;

    /**
     *
     */
    public static String TCI_CODE = "teneurcalcaireinitial" ;

    /**
     *
     */
    public static String TCI_NOM ="teneurcalcaireinitial" ;

    /**
     *
     */
    public static String PAYS_NOM = "Pays" ;

    /**
     *
     */
    public static final String SYSTEMEPROJECTION = "systemeprojection" ;

    /**
     *
     */
    public static String SYST_NOM = "systemeprojection" ;

    /**
     *
     */
    public static String SYST_DEFINITION = "systemeprojection" ;

    /**
     *
     */
    public static String AMENAGEMENT_NOM = "AmenagementAvantPlantation" ;

    /**
     *
     */
    public static String AMENAGEMENT_CODE = "AmenagementAvantPlantation" ;

    /**
     *
     */
    public static String TYPEDOCUMENT_LIBELLE = "TypeDocument" ;

    /**
     *
     * @return
     */
    public  static MockUtils getInstance() {

        MockUtils instance = new MockUtils();
        MockitoAnnotations.initMocks(instance);

        try {
            instance.initMocks();
        } catch (ParseException e) {
        }
        instance.initLocalization();
        ((SpringLocalizationManager) instance.localizationManager).setLocalizationDAO(instance.localizationDAO);
        return instance;
    }

    /**
     *
     */
    public ILocalizationManager localizationManager ;

    /**
     *
     */
    @Mock
public IOrigineDAO origineDAO ;

    /**
     *
     */
    @Mock
public Origines origines ;

    /**
     *
     */
    @Mock
public ILocalizationDAO localizationDAO ;

    /**
     *
     */
    @Mock
public IEtapeDAO etapeDAO ;

    /**
     *
     */
    @Mock
public Etapes etapes ;

    /**
     *
     */
    @Mock
public IProcessDAO processDAO ;

    /**
     *
     */
    @Mock
public Process process ;

    /**
     *
     */
    @Mock
public IMethodeProcessDAO methodeprocessDAO ;

    /**
     *
     */
    @Mock
public MethodeProcess methodeprocess ;

    /**
     *
     */
    @Mock
public IAnneesDAO anneeDAO ;

    /**
     *
     */
    @Mock
public Annees annees ;

    /**
     *
     */
    @Mock
public ICaracteristiqueEtapeDAO caracetapeDAO ;

    /**
     *
     */
    @Mock
public CaracteristiqueEtapes caracteristiqueetapes ;

    /**
     *
     */
    @Mock
public IStatutDAO statutDAO ;
 
    /**
     *
     */
    @Mock 
public Statut statut ;

    /**
     *
     */
    @Mock
public  IFormePhysiqueDAO formeDAO ;

    /**
     *
     */
    @Mock
public FormePhysiques formephysiques ;

    /**
     *
     */
    @Mock
public IUniteDAO uniteDAO ;

    /**
     *
     */
    @Mock
public Unites unites ;

    /**
     *
     */
    @Mock
public CaracteristiqueValeur caracteristiquevaleur ;

    /**
     *
     */
    @Mock
public ICaracteristiqueValeurDAO cvDAO ;

    /**
     *
     */
    @Mock
public ITypeCaracteristiqueDAO typecaracteristiqueDAO ;

    /**
     *
     */
    @Mock
public TypeCaracteristiques typecaracteristiques ;

    /**
     *
     */
    @Mock
public MethodeEtapes methodeetapes ;

    /**
     *
     */
    @Mock
public IMethodeEtapeDAO methodeetapeDAO ;

    /**
     *
     */
    @Mock
public Pente pente ;

    /**
     *
     */
    @Mock
public IPenteDAO penteDAO ;

    /**
     *
     */
    @Mock
public Heterogeinite heterogeinite ;

    /**
     *
     */
    @Mock
public IHeterogeiniteDAO heteroDAO ;

    /**
     *
     */
    @Mock
public Faitremarquable faitremarquable ;

    /**
     *
     */
    @Mock
public IFaitRemarquableDAO faitremarquableDAO ;

    /**
     *
     */
    @Mock
public Typesolarvalis typesolarvalis ;
 
    /**
     *
     */
    @Mock 
public ITypeSolArvalisDAO tsarvalisDAO ;

    /**
     *
     */
    @Mock
public GrandTypeProduits grandtypeproduits ;
 
    /**
     *
     */
    @Mock 
public IGrandTypeProduitDAO gtpDAO ;

    /**
     *
     */
    @Mock
public Substratpedologique substratpedologique ;

    /**
     *
     */
    @Mock
public ISubstratPedologiqueDAO substratDAO ;

    /**
     *
     */
    @Mock
public SystemeProjection systemeProjection ;

    /**
     *
     */
    @Mock
public ISystemeProjectionDAO systemeProjectionDAO ;

    /**
     *
     */
    @Mock
public Typepedologique typepedologique ;

    /**
     *
     */
    @Mock
public ITypePedologiqueDAO typepedoDAO; ;

    /**
     *
     */
    @Mock
public Activiteindustrielle activiteindustrielle ;

    /**
     *
     */
    @Mock
public  IActiviteIndustrielleDAO activiteDAO ;

    /**
     *
     */
    @Mock
public Typeclimat typeclimat ;

    /**
     *
     */
    @Mock
public  ITypeClimatDAO typeclimatDAO ;

    /**
     *
     */
    @Mock
public Teneurcalcaireinitial teneurcalcaireinitial ;
    @Mock
public Texturesol texturesol ;
    @Mock
public  ITextureSolDAO textureSolDAO ;

    /**
     *
     */
    @Mock
public  ITeneurCalcaireInitialDAO tcinitialDAO ;

    /**
     *
     */
    @Mock
public Pays pays ;

    /**
     *
     */
    @Mock
public IPaysDAO paysDAO ;

    /**
     *
     */
    @Mock
public AmenagementAvantPlantation amenagementAvantPlantation ;

    /**
     *
     */
    @Mock
public IAmenagementAvantPlantationDAO avpDAO ;

    /**
     *
     */
    @Mock
public TypeDocument typeDocument ;

    /**
     *
     */
    @Mock
public  ITypeDocumentDAO typeDocumentDAO ;
private void initLocalization() {
    Localization.setDefaultLocalisation("fr");
    Localization.setLocalisations(Arrays.asList(new String[]{"fr", "en"}));
    localizationManager = new SpringLocalizationManager();
    ((SpringLocalizationManager) localizationManager).setUserLocale(new UserLocale());
}
private void initMocks() throws ParseException{
    try {
        initDAOs();
        initEntities();
        initLocalization();
    } catch (PersistenceException ex) {
        LoggerFactory.getLogger(MockUtils.class.getName()).error(ex.getMessage(), ex);
    }
}


private void initDAOs() throws PersistenceException {
    Mockito.when(origineDAO.getByNKey(MockUtils.ORIGINE)).thenReturn(Optional.ofNullable(origines));
    Mockito.when(etapeDAO.getByNKey(MockUtils.ETAPE)).thenReturn(Optional.ofNullable(etapes));
    Mockito.when(processDAO.getByNKey(MockUtils.PROCESS)).thenReturn(Optional.ofNullable(process));
    Mockito.when(methodeprocessDAO.getByNKey(MockUtils.METHODEPROCESS)).thenReturn(Optional.ofNullable(methodeprocess));
    Mockito.when(anneeDAO.getByNKey(MockUtils.ANNEE)).thenReturn(Optional.ofNullable(annees));
    Mockito.when(caracetapeDAO.getByNKey(MockUtils.CARACTERISITIQUEETAPE)).thenReturn(Optional.ofNullable(caracteristiqueetapes));
    Mockito.when(statutDAO.getByNKey(MockUtils.STATUT)).thenReturn(Optional.ofNullable(statut));
    Mockito.when(formeDAO.getByNKey(MockUtils.FORMEPHYSIQUE)).thenReturn(Optional.ofNullable(formephysiques));
    Mockito.when(uniteDAO.getByNKey(MockUtils.UNITE)).thenReturn(Optional.ofNullable(unites));
    Mockito.when(cvDAO.getByNKey(MockUtils.CARACTERISTIQUEVALEUR)).thenReturn(Optional.ofNullable(caracteristiquevaleur));
    Mockito.when(typecaracteristiqueDAO.getByNKey(MockUtils.TYPECARACTMP)).thenReturn(Optional.ofNullable(typecaracteristiques));
    Mockito.when(methodeetapeDAO.getByNKey(MockUtils.METHODEETAPE)).thenReturn(Optional.ofNullable(methodeetapes));
    Mockito.when(penteDAO.getByNKey(MockUtils.PENTE)).thenReturn(Optional.ofNullable(pente));
    Mockito.when(heteroDAO.getByNKey(MockUtils.HETEROGEINITE)).thenReturn(Optional.ofNullable(heterogeinite));
    Mockito.when(faitremarquableDAO.getByNKey(MockUtils.FAITREMARQUABLE)).thenReturn(Optional.ofNullable(faitremarquable));
    Mockito.when(tsarvalisDAO.getByNKey(MockUtils.TYPESOLARVALIS)).thenReturn(Optional.ofNullable(typesolarvalis));
    Mockito.when(activiteDAO.getByNKey(MockUtils.ACTIVITEINDUSTRIELLE)).thenReturn(Optional.ofNullable(activiteindustrielle));
    Mockito.when(typepedoDAO.getByNKey(MockUtils.TYPEPEDOLOGIQUE)).thenReturn(Optional.ofNullable(typepedologique));
    Mockito.when(substratDAO.getByNKey(MockUtils.SUBSTRATPEDOLOGIQUE)).thenReturn(Optional.ofNullable(substratpedologique));
    Mockito.when(typeclimatDAO.getByNKey(MockUtils.TYPECLIMAT)).thenReturn(Optional.ofNullable(typeclimat));
    Mockito.when(textureSolDAO.getByNKey(MockUtils.TEXTURESOL)).thenReturn(Optional.ofNullable(texturesol));
    Mockito.when(tcinitialDAO.getByNKey(MockUtils.TENEURCALCAIREINITIAL, texturesol)).thenReturn(Optional.ofNullable(teneurcalcaireinitial));
    Mockito.when(paysDAO.getByNKey(MockUtils.PAYS_NOM)).thenReturn(Optional.ofNullable(pays));
    Mockito.when(systemeProjectionDAO.getByNKey(MockUtils.SYSTEMEPROJECTION)).thenReturn(Optional.ofNullable(systemeProjection));
    Mockito.when(avpDAO.getByNKey(MockUtils.AMENAGEMENT_NOM)).thenReturn(Optional.ofNullable(amenagementAvantPlantation));
    Mockito.when(typeDocumentDAO.getByNKey(MockUtils.TYPEDOCUMENT_LIBELLE)).thenReturn(Optional.ofNullable(typeDocument));
}

@SuppressWarnings("unused")
private void initEntities() {
    Mockito.when(origines.getOrigine_code()).thenReturn(MockUtils.ORIGINE_CODE);
    Mockito.when(origines.getOrigine_nom()).thenReturn(MockUtils.ORIGINE_NOM);
    Mockito.when(origines.getOrigine_comment()).thenReturn(MockUtils.ORIGINE_COMMENT);
    
    Mockito.when(etapes.getEtape_code()).thenReturn(MockUtils.ETAPE_CODE);
    Mockito.when(etapes.getEtape_intitule()).thenReturn(MockUtils.ETAPE_NOM);
    
    Mockito.when(process.getProcess_code()).thenReturn(MockUtils.PROCESS_CODE);
    Mockito.when(process.getProcess_intitule()).thenReturn(MockUtils.PROCESS_NOM);
    
    Mockito.when(methodeprocess.getMethodep_code()).thenReturn(MockUtils.MPROCESS_CODE);
    Mockito.when(methodeprocess.getMethodep_nom()).thenReturn(MockUtils.MPROCESS_NOM);
    
    Mockito.when(annees.getAnnee_valeur()).thenReturn(MockUtils.ANNEE_VALEUR);
    
    Mockito.when(caracteristiqueetapes.getCetape_code()).thenReturn(MockUtils.CETAPE_CODE);
    Mockito.when(caracteristiqueetapes.getCetape_intitule()).thenReturn(MockUtils.CETAPE_INTITULE);
    
    Mockito.when(statut.getStatut_code()).thenReturn(MockUtils.STATUT_CODE);
    Mockito.when(statut.getStatut_nom()).thenReturn(MockUtils.STATUT_NOM);
    
    Mockito.when(formephysiques.getFp_code()).thenReturn(MockUtils.FORMEPHYSIQUE_CODE);
    Mockito.when(formephysiques.getFp_nom()).thenReturn(MockUtils.FORMEPHYSIQUE_NOM);
    
    Mockito.when(unites.getCode()).thenReturn(MockUtils.UNITE_CODE);
    Mockito.when(unites.getUnite()).thenReturn(MockUtils.UNITE_NOM);
    
    Mockito.when(caracteristiquevaleur.getCv_code()).thenReturn(MockUtils.CV_CODE);
    Mockito.when(caracteristiquevaleur.getCv_nom()).thenReturn(MockUtils.CV_NOM);
    
    Mockito.when(typecaracteristiques.getTcar_code()).thenReturn(MockUtils.TC_CODE);
    Mockito.when(typecaracteristiques.getTcar_nom()).thenReturn(MockUtils.TC_NOM);
    
    Mockito.when(methodeetapes.getMe_code()).thenReturn(MockUtils.ME_CODE);
    Mockito.when(methodeetapes.getMe_nom()).thenReturn(MockUtils.ME_NOM);
    
    Mockito.when(pente.getCode()).thenReturn(MockUtils.CODE);
    Mockito.when(pente.getNom()).thenReturn(MockUtils.NOM);
    
    Mockito.when(heterogeinite.getCode()).thenReturn(MockUtils.HE_CODE);
    Mockito.when(heterogeinite.getNom()).thenReturn(MockUtils.HE_NOM);
    
    Mockito.when(faitremarquable.getCode()).thenReturn(MockUtils.FR_CODE);
    Mockito.when(faitremarquable.getNom()).thenReturn(MockUtils.FR_NOM);
    
    Mockito.when(typesolarvalis.getCode()).thenReturn(MockUtils.TSA_CODE);
    Mockito.when(typesolarvalis.getNom()).thenReturn(MockUtils.TSA_NOM);
    
    
    Mockito.when(activiteindustrielle.getCode()).thenReturn(MockUtils.AI_CODE);
    Mockito.when(activiteindustrielle.getNom()).thenReturn(MockUtils.AI_NOM);
    
    Mockito.when(typepedologique.getCode()).thenReturn(MockUtils.TP_CODE);
    Mockito.when(typepedologique.getNom()).thenReturn(MockUtils.TP_NOM);
    
    Mockito.when(substratpedologique.getCode()).thenReturn(MockUtils.SP_CODE);
    Mockito.when(substratpedologique.getNom()).thenReturn(MockUtils.SP_NOM);
    
    Mockito.when(typeclimat.getCode()).thenReturn(MockUtils.TCL_CODE);
    Mockito.when(typeclimat.getNom()).thenReturn(MockUtils.TCL_NOM);
    
    Mockito.when(teneurcalcaireinitial.getCode()).thenReturn(MockUtils.TCI_CODE);
    Mockito.when(teneurcalcaireinitial.getNom()).thenReturn(MockUtils.TCI_NOM);

    Mockito.when(pays.getNom()).thenReturn(MockUtils.PAYS_NOM);
    
    Mockito.when(amenagementAvantPlantation.getAap_libelle()).thenReturn(MockUtils.AMENAGEMENT_NOM);
    Mockito.when(typeDocument.getLibelle()).thenReturn(MockUtils.TYPEDOCUMENT_LIBELLE);
 
    
    Mockito.when(systemeProjection.getNom()).thenReturn(MockUtils.SYST_NOM);
    Mockito.when(systemeProjection.getDefinition()).thenReturn(MockUtils.SYST_DEFINITION);
}

}
