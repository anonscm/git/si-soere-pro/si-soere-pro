
package org.inra.ecoinfo.pro.utils.filenamecheckers;

import org.inra.ecoinfo.utils.DateUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckeryyyyTest extends AbstractPROFileNameCheckerTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    public FileNameCheckeryyyyTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        qAfeucherolles_sols_physicochimie_donnees = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_yyyy_yyyy.csv";
        fileName = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_2002_2004.csv";
        super.setUp();
        instance = new FileNameCheckeryyyy();
        super.initInstance();
        doReturn(codeDispositifLieu).when(dispositif).getCodeDispo();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDatePattern method, of class FileNameCheckeryyyy.
     */
    @Test
    public void testGetDatePattern() {
        String result = ((FileNameCheckeryyyy) instance).getDatePattern();
        assertEquals(DateUtil.YYYY, result);
    }

}
