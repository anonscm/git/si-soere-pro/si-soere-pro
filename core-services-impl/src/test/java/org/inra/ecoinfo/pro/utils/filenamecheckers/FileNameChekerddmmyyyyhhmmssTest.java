
package org.inra.ecoinfo.pro.utils.filenamecheckers;

import java.time.LocalDateTime;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;

/**
 *
 * @author ptcherniati
 */
public class FileNameChekerddmmyyyyhhmmssTest extends AbstractPROFileNameCheckerTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    protected LocalDateTime dateDeDebut = LocalDateTime.of(2002, Month.SEPTEMBER, 10, 01, 23, 45);

    /**
     *
     */
    protected LocalDateTime dateDeFin = LocalDateTime.of(2002, Month.OCTOBER, 10, 12, 34, 54);

    /**
     *
     */
    public FileNameChekerddmmyyyyhhmmssTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        qAfeucherolles_sols_physicochimie_donnees = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_dd-MM-yyyy-HHmmss_dd-MM-yyyy-HHmmss.csv";
        fileName = "QA-feucherolles_sols_physico-chimie_donnees_elementaires_10-09-2002-012345_10-10-2002-123456.csv";
        super.setUp();
        instance = new FileNameChekerddmmyyyyhhmmss();
        super.initInstance();
        doReturn(codeDispositifLieu).when(dispositif).getCodeDispo();
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getDatePattern method, of class FileNameChekerddmmyyyyhhmmss.
     */
    @Test
    public void testGetDatePattern() {
        System.out.println("getDatePattern");
        String result = ((FileNameChekerddmmyyyyhhmmss) instance).getDatePattern();
        assertEquals(FileNameChekerddmmyyyyhhmmss.DATE_PATTERN, result);
    }

}
