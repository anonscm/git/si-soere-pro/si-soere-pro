/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.famille;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author vjkoyao
 */
public class JPAFamilleDAO extends AbstractJPADAO<Famille> implements IFamilleDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Famille> getAll() {
        return getAllBy(Famille.class, Famille::getFamille_code);
    }

    /**
     *
     * @param famille_nom
     * @return
     */
    @Override
    public Optional<Famille> getByNKey(String famille_nom) {
        CriteriaQuery<Famille> query = builder.createQuery(Famille.class);
        Root<Famille> famille = query.from(Famille.class);
        query
                .select(famille)
                .where(
                        builder.equal(famille.get(Famille_.famille_nom), famille_nom)
                );
        return getOptional(query);
    }
}
