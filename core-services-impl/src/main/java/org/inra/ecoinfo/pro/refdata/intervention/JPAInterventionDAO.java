/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.intervention;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author vjkoyao
 */
public class JPAInterventionDAO extends AbstractJPADAO<Intervention> implements IInterventionDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Intervention> getAll() {
        return getAll(Intervention.class);
    }

    /**
     *
     * @param intervention_nom
     * @return
     */
    @Override
    public Optional<Intervention> getByNKey(String intervention_nom) {
        CriteriaQuery<Intervention> query = builder.createQuery(Intervention.class);
        Root<Intervention> intervention = query.from(Intervention.class);
        query
                .select(intervention)
                .where(
                        builder.equal(intervention.get(Intervention_.intervention_nom), intervention_nom)
                );
        return getOptional(query);
    }

}
