/**
 *
 */
package org.inra.ecoinfo.pro.refdata.observatoire;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IObservatoireDAO extends IDAO<Observatoire> {

    /**
     *
     * @return
     */
    List<Observatoire> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<Observatoire> getByNKey(String code);

}
