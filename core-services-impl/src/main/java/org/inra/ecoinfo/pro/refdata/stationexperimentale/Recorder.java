/**
 *
 */
package org.inra.ecoinfo.pro.refdata.stationexperimentale;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<StationExperimentale> {

    IStationExperimentaleDAO stationExperimentaleDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com .Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();

                stationExperimentaleDAO.remove(stationExperimentaleDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException(" can't get stationExperimentale")));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<StationExperimentale> getAllElements() throws BusinessException {
        return stationExperimentaleDAO.getAll(StationExperimentale.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata (java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StationExperimentale stationExperimentale) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        // Nom
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(stationExperimentale == null ? Constantes.STRING_EMPTY : stationExperimentale.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com .Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nom = tokenizerValues.nextToken();

                StationExperimentale stationExperimentale = new StationExperimentale(nom);

                StationExperimentale dbStationExperimentale = stationExperimentaleDAO.getByNKey(nom).orElse(null);

                if (dbStationExperimentale == null) {
                    stationExperimentaleDAO.saveOrUpdate(stationExperimentale);
                } else {
                    dbStationExperimentale.setNom(nom);
                    stationExperimentaleDAO.saveOrUpdate(dbStationExperimentale);
                }

                values = parser.getLine();
            } // fin while
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param stationExperimentaleDAO the stationExperimentaleDAO to set
     */
    public void setStationExperimentaleDAO(IStationExperimentaleDAO stationExperimentaleDAO) {
        this.stationExperimentaleDAO = stationExperimentaleDAO;
    }

}
