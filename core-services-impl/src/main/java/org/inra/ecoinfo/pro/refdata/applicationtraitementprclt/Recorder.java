package org.inra.ecoinfo.pro.refdata.applicationtraitementprclt;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.ICodeParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;
import org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.modalite.IModaliteDAO;
import org.inra.ecoinfo.pro.refdata.modalite.Modalite;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 * @author adiankha
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<ApplicationTraitementParcelleElt> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;
    ICodeParcelleElementaireDAO codeParcelleElementaireDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IApplicationTraitementParcelleEltDAO applicationTraitementParcelleEltDAO;
    IModaliteDAO modaliteDAO;
    IFacteurDAO facteurDAO;

    private Map<String, String[]> listeTraitementsPossibles;

    private Map<String, String[]> listeParcellesPossibles;
    private String[] listeDispositifPossibles;
    private String[] listeModalitesPossibles;
    private String[] listeFacteurPossibles;
    private Map<String, String[]> listeModalitePossibles;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);

            // On parcourt chaque ligne du fichier
            String[] values = parser.getLine();
            while (values != null) {
                line++;

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexDispLieu = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                String codePE = tokenizerValues.nextToken();
                int indexCodeTrt = tokenizerValues.currentTokenIndex();
                String codeTraitement = tokenizerValues.nextToken();
                int indexNumeroRepetition = tokenizerValues.currentTokenIndex();
                String numeroRepetition = tokenizerValues.nextToken();
                int indexfacteur = tokenizerValues.currentTokenIndex();
                String facteur = tokenizerValues.nextToken();
                int indexmodalite = tokenizerValues.currentTokenIndex();
                String modalite = tokenizerValues.nextToken();
                int indexAnneeDebut = tokenizerValues.currentTokenIndex();
                String anneeDebut = tokenizerValues.nextToken();
                int indexAnneeFin = tokenizerValues.currentTokenIndex();
                String anneeFin = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                Lieu lieu = verifieLieu(nomLieu, line + 1, indexDispLieu + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line + 1, indexDispLieu + 1, errorsReport);
                DescriptionTraitement descriptionTraitement = verifieDescriptionTraitement(codeTraitement, dispositif, line + 1, indexCodeTrt + 1, errorsReport);
                CodeParcelleElementaire codeParcelleElementaire = verifieCodeParcelleElementaire(codePE, line + 1, indexCodeTrt + 1, errorsReport);
                ParcelleElementaire parcelleElementaire = verifieParcelleElementaire(codePE, codeDispositif, codeParcelleElementaire, dispositif, line + 1, indexCodeTrt + 1, errorsReport);
                verifieAnnees(codeDispositifLieu, codeTraitement, anneeDebut, anneeFin, line + 1, indexAnneeDebut + 1, indexAnneeFin + 1, errorsReport);
                Integer numRepetition = verifieNombreRepetition(numeroRepetition, line + 1, indexNumeroRepetition + 1, errorsReport);

                Facteur fact = facteurDAO.getByNKey(facteur).orElse(null);
                if (fact == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "FACTEUR_LIBELLE_NONDEFINI"), line, indexfacteur, facteur));
                }

                Modalite dbmodalite = modaliteDAO.getByValeurFacteur(modalite, fact).orElse(null);
                if (dbmodalite == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "MODALITE_FACTEUR_NONDEFINI"), line, indexmodalite, String.format("%s_%s", modalite, facteur)));
                }

                if (!errorsReport.hasErrors()) {
                    ApplicationTraitementParcelleElt applicationTraitementParcellElt = new ApplicationTraitementParcelleElt(descriptionTraitement, parcelleElementaire, dbmodalite, anneeDebut, anneeFin, numRepetition);

                    ApplicationTraitementParcelleElt dbApplicationTraitementParcelleElt = applicationTraitementParcelleEltDAO.getByNKey(descriptionTraitement, parcelleElementaire, dbmodalite).orElse(null);

                    createOrUpdate(anneeDebut, anneeFin, numRepetition, dbApplicationTraitementParcelleElt, applicationTraitementParcellElt);

                }

                //createOrUpdate(anneeDebut, anneeFin, numRepetition,  codePE, codeTraitement);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }

        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }

        return dispositif;
    }

    /**
     * @param codeTraitement
     * @param dispositif
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws PersistenceException
     */
    private DescriptionTraitement verifieDescriptionTraitement(String codeTraitement, Dispositif dispositif, long line, int index, ErrorsReport errorsReport) {
        DescriptionTraitement descriptionTraitement = descriptionTraitementDAO.getByNKey(codeTraitement, dispositif).orElse(null);
        String identifiantDispositif = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
        if (descriptionTraitement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DESCRIPTRT_NONDEFINI"), line, index, codeTraitement, identifiantDispositif));
        }

        return descriptionTraitement;
    }

    /**
     * @param codePE
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private CodeParcelleElementaire verifieCodeParcelleElementaire(String codePE, long line, int index, ErrorsReport errorsReport) {
        CodeParcelleElementaire codeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(codePE).orElse(null);
        if (codeParcelleElementaire == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPARCELLEELT_NONDEFINI"), line, index, codePE));
        }

        return codeParcelleElementaire;
    }

    /**
     * @param codePE
     * @param codeDispositif
     * @param codeParcelleElementaire
     * @param dispositif
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private ParcelleElementaire verifieParcelleElementaire(String codePE, String codeDispositif, CodeParcelleElementaire codeParcelleElementaire, Dispositif dispositif, long line, int index, ErrorsReport errorsReport) {
        ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByNKey(codeParcelleElementaire, dispositif).orElse(null);
        if (parcelleElementaire == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PRCELT_NONDEFINI"), line, index, codePE, codeDispositif));
        }

        return parcelleElementaire;
    }

    /**
     * @param annee
     * @param line
     * @param index
     * @param errorsReport
     */
    public void verifieAnneeFormat(String annee, long line, int index, ErrorsReport errorsReport) {
        if (annee != null) {
            try {
                DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, String.format("01/01/%s", annee));
            } catch (DateTimeParseException e) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
            }
        }
    }

    /**
     * @param anneeDebut
     * @param anneeFin
     * @param line
     * @param indexD
     * @param indexF
     * @param errorsReport
     */
    private void verifieAnnees(String codeDispositifLieu, String codeTraitement, String anneeDebut, String anneeFin, long line, int indexD, int indexF, ErrorsReport errorsReport) {
        verifieAnneeFormat(anneeFin, line, indexF, errorsReport);
        verifieAnneeFormat(anneeDebut, line, indexD, errorsReport);

        if (anneeFin != null && new Integer(anneeFin) < new Integer(anneeDebut)) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_APPLTRTPRCLT"), line, indexF, anneeFin, codeTraitement, codeDispositifLieu, anneeDebut));
        }
    }

    /**
     * @param numeroRepetition
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Integer verifieNombreRepetition(String numeroRepetition, long line, int index, ErrorsReport errorsReport) {
        Integer numRepetition = null;
        try {
            numRepetition = numeroRepetition != null ? Integer.parseInt(numeroRepetition) : new Integer(0);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "NUMEROREPETION_BADFORMAT"), line, index, numeroRepetition));
        }

        return numRepetition;
    }

    /**
     * @param anneeDebut
     * @param anneeFin
     * @param numRepetition
     * @param dbApplicationTraitementParcelleElt
     * @param applicationTraitementParcellElt
     * @throws PersistenceException
     */
    private void createOrUpdate(String anneeDebut, String anneeFin,
            Integer numRepetition, ApplicationTraitementParcelleElt dbApplicationTraitementParcelleElt, ApplicationTraitementParcelleElt applicationTraitementParcellElt) throws BusinessException {
        try {
            if (dbApplicationTraitementParcelleElt == null) {
                applicationTraitementParcelleEltDAO.saveOrUpdate(applicationTraitementParcellElt);
            } else {
                dbApplicationTraitementParcelleElt.setAnneeDebut(anneeDebut);
                dbApplicationTraitementParcelleElt.setAnneeFin(anneeFin);
                dbApplicationTraitementParcelleElt.setNumeroRepetition(numRepetition);

                applicationTraitementParcelleEltDAO.saveOrUpdate(dbApplicationTraitementParcelleElt);
            }
        } catch (PersistenceException pe) {
            throw new BusinessException("Can't create or save dbApplicationTraitementParcelleElt");
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String codeDispositifLieu = tokenizerValues.nextToken();
                String codePE = tokenizerValues.nextToken();
                String codeTraitement = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String facteur = tokenizerValues.nextToken();
                String modalite = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                DescriptionTraitement descriptionTraitement = descriptionTraitementDAO.getByNKey(codeTraitement, dispositif).orElse(null);
                CodeParcelleElementaire codeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(codePE).orElse(null);
                ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByNKey(codeParcelleElementaire, dispositif).orElse(null);
                Facteur dbfacteur = facteurDAO.getByNKey(facteur).orElse(null);
                Modalite dbmodalite = modaliteDAO.getByValeurFacteur(modalite, dbfacteur).orElse(null);

                applicationTraitementParcelleEltDAO.remove(
                        applicationTraitementParcelleEltDAO.getByNKey(descriptionTraitement, parcelleElementaire, dbmodalite)
                                .orElseThrow(() -> new PersistenceException("can't get applicationTraitementParcelleElt"))
                );

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* private void getModaliteFacteurPossibles() throws PersistenceException {
        List<Modalite> lstModalites = modaliteDAO.getAll();
        String[] listemodalitePossibles = new String[lstModalites.size() + 1];
        listemodalitePossibles[0] = "";
        int index = 1;
        for (Modalite modalite : lstModalites) {
            listemodalitePossibles[index++] = modalite.getFacteur().getLibelle()+ " (" + modalite.getValeur() + ")";
        }
        this.listeModalitesPossibles = listemodalitePossibles;
    }*/
    private void initDispositifTraitementPossibles() {
        Map<String, String[]> dispositifTraitement = new HashMap<>();
        Map<String, Set<String>> dispositifTraitementList = new HashMap<>();
        List<DescriptionTraitement> groupestraitement = descriptionTraitementDAO.getAll();
        groupestraitement.forEach((traitement) -> {
            String codedisp = traitement.getDispositif().getNomDispositif_nomLieu();
            String codetrait = traitement.getCode();
            if (!dispositifTraitementList.containsKey(codedisp)) {
                dispositifTraitementList.put(codedisp, new TreeSet());
            }
            dispositifTraitementList.get(codedisp).add(codetrait);
        });
        dispositifTraitementList.entrySet().forEach((entryProduit) -> {
            dispositifTraitement.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeTraitementsPossibles = dispositifTraitement;
        listeDispositifPossibles = dispositifTraitement.keySet().toArray(new String[]{});
    }

    /**
     * @return @throws PersistenceException
     */
    private void initDispositifParcellePossibles() {

        Map<String, String[]> dispositifParcelle = new HashMap<>();
        Map<String, Set<String>> dispositifParcelleList = new HashMap<>();
        List<ParcelleElementaire> lstParcelleElementaires = parcelleElementaireDAO.getAll();
        lstParcelleElementaires.forEach((parcelle) -> {
            String codedisp = parcelle.getDispositif().getNomDispositif_nomLieu();
            String codeParc = parcelle.getCodeParcelleElementaire().getCode();
            if (!dispositifParcelleList.containsKey(codedisp)) {
                dispositifParcelleList.put(codedisp, new TreeSet());
            }
            dispositifParcelleList.get(codedisp).add(codeParc);
        });
        dispositifParcelleList.entrySet().forEach((entryDispositif) -> {
            dispositifParcelle.put(entryDispositif.getKey(), entryDispositif.getValue().toArray(new String[]{}));
        });
        this.listeParcellesPossibles = dispositifParcelle;
    }

    private void initFacteurModalitePossibles() {
        Map<String, String[]> facteurmodalite = new HashMap<>();
        Map<String, Set<String>> facteurmodalitetList = new HashMap<>();
        List<Modalite> groupesmodalite = modaliteDAO.getAll();
        groupesmodalite.forEach((modalite) -> {
            String codefacteur = modalite.getFacteur().getLibelle();
            String codemodalite = modalite.getValeur();
            if (!facteurmodalitetList.containsKey(codefacteur)) {
                facteurmodalitetList.put(codefacteur, new TreeSet());
            }
            facteurmodalitetList.get(codefacteur).add(codemodalite);
        });
        facteurmodalitetList.entrySet().forEach((entryProduit) -> {
            facteurmodalite.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeModalitePossibles = facteurmodalite;
        listeFacteurPossibles = facteurmodalite.keySet().toArray(new String[]{});
    }


    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ApplicationTraitementParcelleElt applicationTraitementParcelleElt) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String valeurDisp = applicationTraitementParcelleElt == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : applicationTraitementParcelleElt.getDescriptionTraitement().getDispositif() != null
                ? applicationTraitementParcelleElt.getDescriptionTraitement().getDispositif().getNomDispositif_nomLieu() : "";
        ColumnModelGridMetadata columnDispositif = new ColumnModelGridMetadata(valeurDisp, listeDispositifPossibles, null, true, false, true);

        String valeurTraitement = applicationTraitementParcelleElt == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : applicationTraitementParcelleElt.getDescriptionTraitement().getCode() != null
                ? applicationTraitementParcelleElt.getDescriptionTraitement().getCode() : "";
        ColumnModelGridMetadata columnTraitement = new ColumnModelGridMetadata(valeurTraitement, listeTraitementsPossibles, null, true, false, true);

        String codePE = applicationTraitementParcelleElt == null ? "" : applicationTraitementParcelleElt.getParcelleElementaire().getCodeParcelleElementaire().getCode();
        ColumnModelGridMetadata columnParcelle
                = new ColumnModelGridMetadata(applicationTraitementParcelleElt == null ? Constantes.STRING_EMPTY : codePE, listeParcellesPossibles,
                        null, true, false, true);
        final List<ColumnModelGridMetadata> refs = new LinkedList();
        refs.add(columnParcelle);
        refs.add(columnTraitement);
        setColumnRef(columnDispositif, refs);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispositif);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnParcelle);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTraitement);

        // Numéro de répétition
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(applicationTraitementParcelleElt == null ? Constantes.STRING_EMPTY : applicationTraitementParcelleElt.getNumeroRepetition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        String valeurFacteur = applicationTraitementParcelleElt == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : applicationTraitementParcelleElt.getModalite() != null ? applicationTraitementParcelleElt.getModalite().getFacteur().getLibelle() : "";
        ColumnModelGridMetadata columnFac = new ColumnModelGridMetadata(valeurFacteur, listeFacteurPossibles, null, false, false, true);
        String valeurProcede = applicationTraitementParcelleElt == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : applicationTraitementParcelleElt.getModalite() != null ? applicationTraitementParcelleElt.getModalite().getValeur() : "";
        ColumnModelGridMetadata columnModa = new ColumnModelGridMetadata(valeurProcede, listeModalitePossibles, null, false, false, true);
        List<ColumnModelGridMetadata> refsModalite = new LinkedList<ColumnModelGridMetadata>();
        refsModalite.add(columnModa);
        columnModa.setValue(valeurProcede);
        columnFac.setRefs(refsModalite);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnFac);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnModa);

        // Année de début
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(applicationTraitementParcelleElt == null ? Constantes.STRING_EMPTY : applicationTraitementParcelleElt.getAnneeDebut(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        // Année de fin
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(applicationTraitementParcelleElt == null ? Constantes.STRING_EMPTY : applicationTraitementParcelleElt.getAnneeFin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ApplicationTraitementParcelleElt> getAllElements() throws BusinessException {
        return applicationTraitementParcelleEltDAO.getAll();
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param descriptionTraitementDAO the descriptionTraitementDAO to set
     */
    public void setDescriptionTraitementDAO(
            IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    /**
     * @param modaliteFacteurEtudieTrtDAO the modaliteFacteurEtudieTrtDAO to set
     */
    /*public void setModaliteFacteurEtudieTrtDAO(
     IModaliteFacteurEtudieTrtDAO modaliteFacteurEtudieTrtDAO) {
     this.modaliteFacteurEtudieTrtDAO = modaliteFacteurEtudieTrtDAO;
     }
     */
    /**
     * @param codeParcelleElementaireDAO the codeParcelleElementaireDAO to set
     */
    public void setCodeParcelleElementaireDAO(
            ICodeParcelleElementaireDAO codeParcelleElementaireDAO) {
        this.codeParcelleElementaireDAO = codeParcelleElementaireDAO;
    }

    /**
     * @param parcelleElementaireDAO the parcelleElementaireDAO to set
     */
    public void setParcelleElementaireDAO(
            IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     * @param applicationTraitementParcelleEltDAO the
     * applicationTraitementParcelleEltDAO to set
     */
    public void setApplicationTraitementParcelleEltDAO(
            IApplicationTraitementParcelleEltDAO applicationTraitementParcelleEltDAO) {
        this.applicationTraitementParcelleEltDAO = applicationTraitementParcelleEltDAO;
    }

    /**
     *
     * @param modaliteDAO
     */
    public void setModaliteDAO(IModaliteDAO modaliteDAO) {
        this.modaliteDAO = modaliteDAO;
    }

    /**
     *
     * @param facteurDAO
     */
    public void setFacteurDAO(IFacteurDAO facteurDAO) {
        this.facteurDAO = facteurDAO;
    }

    @Override
    protected ModelGridMetadata<ApplicationTraitementParcelleElt> initModelGridMetadata() {

        initDispositifTraitementPossibles();
        initDispositifParcellePossibles();
        initFacteurModalitePossibles();

        return super.initModelGridMetadata();

    }
}
