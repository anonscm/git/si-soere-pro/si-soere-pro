/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeechantillon;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeEchantillon> {
    
    ITypeEchantillon typeechantillonDAO;
    
    Properties commentaireEn;
    
    /**
     *
     * @param typeechantillon
     * @throws BusinessException
     */
    public void createTEchantillon(TypeEchantillon typeechantillon) throws BusinessException {
        try {
            typeechantillonDAO.saveOrUpdate(typeechantillon);
            typeechantillonDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create typeechantillon");
        }
    }
    
    private void updatecreateTEchantillon(final String nom, String code, String coment, final TypeEchantillon dbtypeechantillon) throws BusinessException {
        try {
            dbtypeechantillon.setTe_code(code);
            dbtypeechantillon.setTe_nom(nom);
            dbtypeechantillon.setTe_comment(coment);
            typeechantillonDAO.saveOrUpdate(dbtypeechantillon);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update typeechantillon");
        }
    }
    
    private void createOrUpdatecreateTEchantillon(final String nom, String code, String coment, final TypeEchantillon dbtypeechantillon) throws BusinessException {
        if (dbtypeechantillon == null) {
            final TypeEchantillon techantillon = new TypeEchantillon(nom, coment);
            techantillon.setTe_code(code);
            techantillon.setTe_nom(nom);
            techantillon.setTe_comment(coment);
            createTEchantillon(techantillon);
        } else {
            updatecreateTEchantillon(nom, code, coment, dbtypeechantillon);
        }
    }
    
    private void persistTEchantillon(String nom, String code, String coment) throws BusinessException, BusinessException {
        final TypeEchantillon dbtechantillon = typeechantillonDAO.getByNkey(nom).orElse(null);
        createOrUpdatecreateTEchantillon(nom, code, coment, dbtechantillon);
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String nom = tokenizerValues.nextToken();
                
                typeechantillonDAO.remove(typeechantillonDAO.getByNkey(nom)
                        .orElseThrow(() -> new BusinessException("can't get type echantillon")));
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, TypeEchantillon.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistTEchantillon(intitule, code, commentaire);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<TypeEchantillon> getAllElements() throws BusinessException {
        return typeechantillonDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeEchantillon techantillon) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(techantillon == null || techantillon.getTe_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : techantillon.getTe_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(techantillon == null || techantillon.getTe_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : techantillon.getTe_comment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(techantillon == null || techantillon.getTe_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(techantillon.getTe_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @param typeechantillonDAO
     */
    public void setTypeechantillonDAO(ITypeEchantillon typeechantillonDAO) {
        this.typeechantillonDAO = typeechantillonDAO;
    }
    
    @Override
    protected ModelGridMetadata<TypeEchantillon> initModelGridMetadata() {
        commentaireEn = localizationManager.newProperties(TypeEchantillon.NAME_ENTITY_JPA, TypeEchantillon.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
}
