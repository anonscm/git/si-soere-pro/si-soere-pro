/**
 *
 */
package org.inra.ecoinfo.pro.refdata.geolocalisation;

import java.util.List;
import org.inra.ecoinfo.IDAO;


/**
 * @author sophie
 * 
 */
public interface IGeolocalisationDAO extends IDAO<Geolocalisation> {

    /**
     *
     * @param systemeProjection
     * @param latitude
     * @param longitude
     * @return
     */
    List<Geolocalisation> getBySystProjectLatitudeLongitude(String systemeProjection, String latitude, String longitude);

}
