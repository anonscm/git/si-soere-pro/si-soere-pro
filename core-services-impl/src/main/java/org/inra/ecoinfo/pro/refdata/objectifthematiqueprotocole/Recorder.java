package org.inra.ecoinfo.pro.refdata.objectifthematiqueprotocole;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.protocole.IProtocoleDAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.thematiqueetudiee.IThematiqueEtudieeDAO;
import org.inra.ecoinfo.pro.refdata.thematiqueetudiee.ThematiqueEtudiee;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<ObjectifThematiqueProtocole> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IProtocoleDAO protocoleDAO;
    IThematiqueEtudieeDAO thematiqueEtudieeDAO;
    IObjectifThematiqueProtocoleDAO objectifThematiqueProtocoleDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, ObjectifThematiqueProtocole.TABLE_NAME);

                String nomProtocole = tokenizerValues.nextToken();
                int indexpro = tokenizerValues.currentTokenIndex();

                String libelleThematiqueEtudiee = tokenizerValues.nextToken();
                int indexlib = tokenizerValues.currentTokenIndex();

                String objectif = tokenizerValues.nextToken();
                int indexoject = tokenizerValues.currentTokenIndex();

                Protocole protocole = protocoleDAO.getByNKey(nomProtocole).orElse(null);
                if (protocole == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "PROTOCOLE_NONDEFINI"), line + 1, indexpro, nomProtocole));
                }

                ThematiqueEtudiee thematiqueEtudiee = thematiqueEtudieeDAO.getByNKey(libelleThematiqueEtudiee).orElse(null);
                if (thematiqueEtudiee == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "THEMATIQUE_NONDEFINI"), line + 1, indexlib, libelleThematiqueEtudiee));
                }

                if (objectif == null || objectif.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexoject - 1, datasetDescriptor.getColumns().get(2).getName()));
                }

                ObjectifThematiqueProtocole dbObjectifThematiqueProtocole = objectifThematiqueProtocoleDAO.getByNKey(protocole, thematiqueEtudiee, objectif).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbObjectifThematiqueProtocole == null) {
                        ObjectifThematiqueProtocole objectifThematiqueProtocole = new ObjectifThematiqueProtocole(protocole, thematiqueEtudiee, objectif);
                        objectifThematiqueProtocoleDAO.saveOrUpdate(objectifThematiqueProtocole);
                    } else {
                        dbObjectifThematiqueProtocole.setObjectif(objectif);
                        objectifThematiqueProtocoleDAO.saveOrUpdate(dbObjectifThematiqueProtocole);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nomProtocole = tokenizerValues.nextToken();
                String libelleThematiqueEtudiee = tokenizerValues.nextToken();
                String objectif = tokenizerValues.nextToken();

                Protocole protocole = protocoleDAO.getByNKey(nomProtocole).orElse(null);
                ThematiqueEtudiee thematiqueEtudiee = thematiqueEtudieeDAO.getByNKey(libelleThematiqueEtudiee).orElse(null);

                objectifThematiqueProtocoleDAO.remove(objectifThematiqueProtocoleDAO.getByNKey(protocole, thematiqueEtudiee, objectif)
                        .orElseThrow(() -> new BusinessException("can't get objectifThematiqueProtocole")));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getProtocolePossibles() {
        List<Protocole> lstProtocoles = protocoleDAO.getAll(Protocole.class);
        String[] protocolePossibles = new String[lstProtocoles.size()];
        int index = 0;
        for (Protocole protocole : lstProtocoles) {
            protocolePossibles[index++] = protocole.getNom();
        }
        return protocolePossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getThematiquePossibles() {
        List<ThematiqueEtudiee> lstThematiques = thematiqueEtudieeDAO.getAll(ThematiqueEtudiee.class);
        String[] thematiquePossibles = new String[lstThematiques.size()];
        int index = 0;
        for (ThematiqueEtudiee thematiqueEtudiee : lstThematiques) {
            thematiquePossibles[index++] = thematiqueEtudiee.getLibelle();
        }
        return thematiquePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ObjectifThematiqueProtocole> getAllElements() throws BusinessException {
        return objectifThematiqueProtocoleDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ObjectifThematiqueProtocole objectThemProtocole) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesObjectif = localizationManager.newProperties(ObjectifThematiqueProtocole.TABLE_NAME, RefDataConstantes.COLUMN_OBJECTIF_OBJTHEMPROTOCOLE, Locale.ENGLISH);

        String localizedChampObjectif = "";

        if (objectThemProtocole != null) {
            localizedChampObjectif = propertiesObjectif.containsKey(objectThemProtocole.getObjectif()) ? propertiesObjectif.getProperty(objectThemProtocole.getObjectif()) : objectThemProtocole.getObjectif();
        }

        //Nom du protocole
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(objectThemProtocole == null ? Constantes.STRING_EMPTY : objectThemProtocole.getProtocole().getNom(),
                        getProtocolePossibles(), null, true, false, true));

        //Libellé de la thématique étudiée
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(objectThemProtocole == null ? Constantes.STRING_EMPTY : objectThemProtocole.getThematiqueEtudiee().getLibelle(),
                        getThematiquePossibles(), null, true, false, true));

        //Objectif de le thématique_fr
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(objectThemProtocole == null ? Constantes.STRING_EMPTY : objectThemProtocole.getObjectif(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, true, true));
        //Objectif de la thématique_en
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(objectThemProtocole == null ? Constantes.STRING_EMPTY : localizedChampObjectif,
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /**
     * @param objectifThematiqueProtocoleDAO the objectifThematiqueProtocoleDAO
     * to set
     */
    public void setObjectifThematiqueProtocoleDAO(IObjectifThematiqueProtocoleDAO objectifThematiqueProtocoleDAO) {
        this.objectifThematiqueProtocoleDAO = objectifThematiqueProtocoleDAO;
    }

    /**
     * @param protocoleDAO the protocoleDAO to set
     */
    public void setProtocoleDAO(IProtocoleDAO protocoleDAO) {
        this.protocoleDAO = protocoleDAO;
    }

    /**
     * @param thematiqueEtudieeDAO the thematiqueEtudieeDAO to set
     */
    public void setThematiqueEtudieeDAO(IThematiqueEtudieeDAO thematiqueEtudieeDAO) {
        this.thematiqueEtudieeDAO = thematiqueEtudieeDAO;
    }

}
