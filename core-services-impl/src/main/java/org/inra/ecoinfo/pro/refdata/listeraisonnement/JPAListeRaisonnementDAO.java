/**
 *
 */
package org.inra.ecoinfo.pro.refdata.listeraisonnement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAListeRaisonnementDAO extends AbstractJPADAO<ListeRaisonnement> implements IListeRaisonnementDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.listeraisonnement.IListeRaisonnementDAO#getAll()
     */
    @Override
    public List<ListeRaisonnement> getAll() {
        return getAll(ListeRaisonnement.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.listeraisonnement.IListeRaisonnementDAO# getByLibelle(java.lang.String)
     */
    @Override
    public Optional<ListeRaisonnement> getByNKey(String libelle) {
        CriteriaQuery<ListeRaisonnement> query = builder.createQuery(ListeRaisonnement.class);
        Root<ListeRaisonnement> listeRaisonnement = query.from(ListeRaisonnement.class);
        query
                .select(listeRaisonnement)
                .where(
                        builder.equal(listeRaisonnement.get(ListeRaisonnement_.libelle), libelle)
                );
        return getOptional(query);
    }

}
