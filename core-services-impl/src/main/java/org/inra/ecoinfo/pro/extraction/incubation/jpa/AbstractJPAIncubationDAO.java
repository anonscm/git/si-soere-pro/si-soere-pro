/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.jpa;

import org.inra.ecoinfo.pro.extraction.incubation.IIncubationDAO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubation;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubation_;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubation;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubation_;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO_;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO_;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractJPAIncubationDAO<M extends MesureIncubation, V extends ValeurIncubation> extends AbstractJPADAO<M> implements IIncubationDAO<M>{
    /**
     * Extract flux tours.
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user the value of user
     * @link(List<SiteACBB>)
     * @link(Date)
     * @link(Date)
     * @return the
     * java.util.List<org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours>
     */
    @SuppressWarnings("unchecked")
    public List<M> extractMesureIncubation(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user) {
        CriteriaQuery<M> criteria = buildQuery(selectedDispositif, selectedVariables, interval, user, false);
        return getResultList(criteria);

    }

    /**
     *
     * @param selectedDispositif
     * @param user
     * @param interval
     * @param selectedVariables
     * @return the java.lang.Long
     */
    public Long getExtractionSize(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user) {
        CriteriaQuery<Long> criteria = buildQuery(selectedDispositif, selectedVariables, interval, user, true);
        return getOptional(criteria).orElse(-1L);
    }

    /**
     *
     * @param selectedDispositif
     * @param selectedVariable
     * @param intervalDate
     * @param user
     * @param isCount
     * @return
     */
    public CriteriaQuery buildQuery(final List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariable, IntervalDate intervalDate, IUser user, boolean isCount) {

        CriteriaQuery criteria;
        if (isCount) {
            criteria = builder.createQuery(Long.class);
        } else {
            criteria = builder.createQuery(getMesureIncubationClass());
        }
        Root<V> v = criteria.from(getValeurIncubationClass());
        Join<V, M> m = v.join(getMesureAttribute());
        Join<V, RealNode> rnVariable = v.join(ValeurIncubation_.realNode);
        Join<RealNode, RealNode> rnDatatype = rnVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> rnTheme = rnDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> rnDispositif = rnTheme.join(RealNode_.parent);
        Join<RealNode, Nodeable> ndbDispositif = rnDispositif.join(RealNode_.nodeable);
        Root<DatatypeVariableUnitePRO> dvu = criteria.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, VariablesPRO> variable = dvu.join(DatatypeVariableUnitePRO_.variablespro);
        final Path<Nodeable> ndbDVU = rnVariable.get(RealNode_.nodeable);
        Root<NodeDataSet> vns = criteria.from(NodeDataSet.class);
        ArrayList<Predicate> predicatesAnd = new ArrayList();
        predicatesAnd.add(builder.equal(rnVariable, vns.get(NodeDataSet_.realNode)));
        Join<RealNode, Nodeable> ndbDvu = rnVariable.join(RealNode_.nodeable);
        predicatesAnd.add(builder.equal(dvu, ndbDVU));
        predicatesAnd.add(ndbDispositif.in(selectedDispositif));
        predicatesAnd.add(variable.in(selectedVariable));
        predicatesAnd.add(builder.between(m.<LocalDate>get(MesureIncubation_.LocalDate_prel_sol), intervalDate.getBeginDate().toLocalDate(), intervalDate.getEndDate().toLocalDate()));
        final Path<LocalDate> dateMesure = m.<LocalDate>get(MesureIncubation_.LocalDate_prel_sol);
        if (!isCount) {
            addRestrictiveRequestOnRoles(user, criteria, predicatesAnd, builder, vns, dateMesure);
        }
        Predicate where = builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()]));
        criteria.where(where);
        if (isCount) {
            criteria.select(builder.countDistinct(m));
        } else {
            criteria.select(m);
            List<Order> orders = new ArrayList();
            orders.add(builder.asc(ndbDispositif));
            orders.add(builder.asc(m.get(MesureIncubation_.LocalDate_prel_sol)));
            orders.add(builder.asc(ndbDVU));
            criteria.orderBy(orders);
        }
        return criteria;
    }

    /**
     *
     * @return
     */
    abstract protected Class<V> getValeurIncubationClass();

    /**
     *
     * @return
     */
    abstract protected Class<M> getMesureIncubationClass();

    /**
     *
     * @return
     */
    abstract protected Class<? extends GenericSynthesisValue> getSynthesisValueClass();

    /**
     *
     * @return
     */
    abstract protected SingularAttribute<V, M> getMesureAttribute();
    
    /**
     *
     * @param user
     * @param criteria
     * @param predicatesAnd
     * @param builder
     * @param vns
     * @param dateMesure
     */
    @SuppressWarnings("unchecked")
    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd, CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.login), user.getLogin()));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

    abstract protected String getDatatypeCode();
    @Override
    public List<Dispositif> getAvailablesDispositifs(IUser user) {
        ArrayList<Predicate> and = new ArrayList<>();
        CriteriaQuery<Dispositif> query = builder.createQuery(Dispositif.class);
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rnDisp = rnt.join(RealNode_.parent);
        and.add(builder.equal(ndsv, sv.get(GenericSynthesisValue_.idNode)));
        Path<Dispositif> disp = builder.treat(rnDisp.get(RealNode_.nodeable), Dispositif.class);
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, sv.get(GenericSynthesisValue_.date));
        }
        query
                .select(disp)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    @Override
    public List<NodeDataSet> getAvailablesVariablesByDispositif(List<Dispositif> dispositifs, IUser user) {
        ArrayList<Predicate> and = new ArrayList<>();
        CriteriaQuery<NodeDataSet> query = builder.createQuery(NodeDataSet.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Root<? extends GenericSynthesisValue> sv = query.from(getSynthesisValueClass());
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rnDisp = rnt.join(RealNode_.parent);
        and.add(builder.equal(ndsv, sv.get(GenericSynthesisValue_.idNode)));
        Path<Dispositif> disp = builder.treat(rnDisp.get(RealNode_.nodeable), Dispositif.class);
        and.add(disp.in(dispositifs));
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, sv.get(GenericSynthesisValue_.date));
        }
        query
                .select(ndsv)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    @Override
    public List<CategorieVariable> getAvailableCategorieVariable(Long idVariable) {
        CriteriaQuery<CategorieVariable> query = builder.createQuery(CategorieVariable.class);
        Root<DatatypeVariableUnitePRO> dvu = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, VariablesPRO> variable = dvu.join(DatatypeVariableUnitePRO_.variablespro);
        Join<VariablesPRO, CategorieVariable> categorieVariable = variable.join(VariablesPRO_.categorievariable);
        query
                .select(categorieVariable)
                .where(
                        builder.equal(variable.get(VariablesPRO_.id), idVariable)
                );
        return getResultList(query);
    }
    
}
