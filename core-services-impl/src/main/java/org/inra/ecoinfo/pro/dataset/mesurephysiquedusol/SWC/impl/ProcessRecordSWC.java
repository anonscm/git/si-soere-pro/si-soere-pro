package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC.impl;

import java.io.IOException;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC.IMesureSWCDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.MesureSWC;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.SequenceSWC;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.SousSequenceSWC;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.ValeurSWC;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.impl.MPSLineRecord;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordSWC extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordSWC.class);

    protected static final String BUNDLE_PATH_SWC = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
    private static final String MSG_ERROR_MSP_NOT_FOUND_DVU_DB = "MSG_ERROR_MPS_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_MPS_NOT_FOUND_METHODE_DB = "MSG_ERROR_MPS_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_MPS_NOT_UNITEPRO_DB = "MSG_ERROR_MPS_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_MPS_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_MPS_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_MPS_NOT_VARIABLEPRO_DB = "MSG_ERROR_MPS_NOT_VARIABLEPRO_DB";
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IParcelleElementaireDAO parcelleElementaireDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;

    private void buildSousSequence(List<MPSLineRecord> lineRecordMPS, SequenceSWC sequenceSWC, ErrorsReport errorsReport) throws PersistenceException, InsertionDatabaseException {

        SousSequenceSWC sousSequenceSWC = new SousSequenceSWC();
        MPSLineRecord mPSLineRecord = lineRecordMPS.get(0);

        sousSequenceSWC.setProfondeur(Integer.MIN_VALUE);
        sousSequenceSWC.setSequenceSWC(sequenceSWC);

        sequenceSWC.getSousSequences().add(sousSequenceSWC);
        Map<String, List<MPSLineRecord>> mesuresMap = new HashMap<String, List<MPSLineRecord>>();
    }

    void buildMesure(final MPSLineRecord mesureLines, SousSequenceSWC sousSequenceSWC, List<ValeurSWC> valeursSWC, final VersionFile versionFile,
            final SortedSet<MPSLineRecord> ligneEnErreur, ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException, InsertionDatabaseException {

        // je parcours le fichier et je recupere les differentes valeurs de chaque parametre
        LocalTime heure = mesureLines.getHeure();
        String variable = mesureLines.getCodevariable();
        float valeur = mesureLines.getValeurvariable();
        String statut = mesureLines.getStatutvaleur();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());

        String cunite = Utils.createCodeFromString(unite);

        String cmethode = Utils.createCodeFromString(methode);

        String chumidite = Utils.createCodeFromString(humidite);

        String cvariable = Utils.createCodeFromString(variable);

        VariablesPRO dbvariable = variPRODAO.betByNKey(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSWC.BUNDLE_PATH_SWC,
                    ProcessRecordSWC.MSG_ERROR_MPS_NOT_VARIABLEPRO_DB), cvariable));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSWC.BUNDLE_PATH_SWC,
                    ProcessRecordSWC.MSG_ERROR_MPS_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSWC.BUNDLE_PATH_SWC,
                    ProcessRecordSWC.MSG_ERROR_MPS_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumidite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSWC.BUNDLE_PATH_SWC,
                    ProcessRecordSWC.MSG_ERROR_MPS_NOT_FOUND_HUMIDITE_DB), chumidite));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(datatype, dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);
        if (dbdvum == null) {

            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSWC.BUNDLE_PATH_SWC,
                    ProcessRecordSWC.MSG_ERROR_MSP_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumidite));
        }

        Long fichier = mesureLines.getOriginalLineNumber();

        MesureSWC mesureSWC = new MesureSWC();

        mesureSWC.setHeure(heure);
        mesureSWC.setSousSequenceSWC(sousSequenceSWC);
        mesureSWC.setLineNumber(0);
        mesureSWC.setValeursSWC(valeursSWC);

        final ValeurSWC valeurSWC = new ValeurSWC();

        valeurSWC.setRealNode(dbdvumRealNode);
        valeurSWC.setLigneFichierEchange(fichier);
        valeurSWC.setMesureSWC(mesureSWC);
        valeurSWC.setStatutvaleur(statut);
        valeurSWC.setValeur(valeur);
        valeurSWC.setNumRepetition(0);

        mesureSWC.getValeursSWC().add(valeurSWC);

    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureSWCDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public IParcelleElementaireDAO getParcelleElementaireDAO() {
        return parcelleElementaireDAO;
    }

    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

}
