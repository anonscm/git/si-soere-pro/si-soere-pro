/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy_;
/**
 *
 * @author vjkoyao
 */
public class JPAPublicationPhysicoChimieMoyDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurPhysicoChimieSolsMoy> deleteValeurs = builder.createCriteriaDelete(ValeurPhysicoChimieSolsMoy.class);
        Root<ValeurPhysicoChimieSolsMoy> valeur = deleteValeurs.from(ValeurPhysicoChimieSolsMoy.class);
        Subquery<MesurePhysicoChimieSolsMoy> subquery = deleteValeurs.subquery(MesurePhysicoChimieSolsMoy.class);
        Root<MesurePhysicoChimieSolsMoy> m = subquery.from(MesurePhysicoChimieSolsMoy.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesurePhysicoChimieSolsMoy_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurPhysicoChimieSolsMoy_.mesurePhysicoChimieSolsMoy).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesurePhysicoChimieSolsMoy> deleteSequence = builder.createCriteriaDelete(MesurePhysicoChimieSolsMoy.class);
        Root<MesurePhysicoChimieSolsMoy> sequence = deleteSequence.from(MesurePhysicoChimieSolsMoy.class);
        deleteSequence.where(builder.equal(sequence.get(MesurePhysicoChimieSolsMoy_.versionfile), version));
        delete(deleteSequence);
    }

}
