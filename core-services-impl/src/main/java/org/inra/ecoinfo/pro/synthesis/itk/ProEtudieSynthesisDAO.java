/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.itk;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie_;
import org.inra.ecoinfo.pro.synthesis.proetudie.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.proetudie.SynthesisValue;
/**
 *
 * @author ptcherniati
 */
public class ProEtudieSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurProEtudie, MesureProEtudie> {

    @Override
    Class<ValeurProEtudie> getValueClass() {
        return ValeurProEtudie.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurProEtudie, MesureProEtudie> getMesureAttribute() {
        return ValeurProEtudie_.mesureinterventionproetudie;
    }

    @Override
    Boolean isValue() {
        return Boolean.TRUE;
    }

}
