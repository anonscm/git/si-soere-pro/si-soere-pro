/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.amenagementavantplantation;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IAmenagementAvantPlantationDAO extends IDAO<AmenagementAvantPlantation>{

    /**
     *
     * @return
     */
    List<AmenagementAvantPlantation> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<AmenagementAvantPlantation> getByNKey(String libelle);
    
}
