package org.inra.ecoinfo.pro.refdata.pente;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IPenteDAO extends IDAO<Pente> {

    /**
     *
     * @return
     */
    List<Pente> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Pente> getByNKey(String nom);

}
