package org.inra.ecoinfo.pro.dataset.flux_chambre.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.flux_chambre.IMesureFluxChambreDAO;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordFluxChambre extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordFluxChambre.class);
    protected static final String BUNDLE_PATH_FLUX = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
    private static final String MSG_ERROR_FLUX_NOT_FOUND_DVU_DB = "MSG_ERROR_FLUX_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_FLUX_NOT_FOUND_METHODE_DB = "MSG_ERROR_FLUX_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_FLUX_NOT_UNITEPRO_DB = "MSG_ERROR_FLUX_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_FLUX_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_FLUX_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_FLUX_NOT_VARIABLEPRO_DB = "MSG_ERROR_FLUX_NOT_VARIABLEPRO_DB";

    protected IMesureFluxChambreDAO<MesureFluxChambres> mesureFluxChambreDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IParcelleElementaireDAO parcelleElementaireDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;

    public ProcessRecordFluxChambre() {
        super();
    }

    void buildMesure(final FluxChambreLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<FluxChambreLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException {

        // je parcours le fichier et je recupere les differentes valeurs de chaque parametre
        LocalDate date = mesureLines.getDatemesure();
        int nbr_chambres = mesureLines.getNbr_chambres();
        int nbr_cycles = mesureLines.getNbr_cycles();
        String variable = mesureLines.getCodevariable();
        float valeur = mesureLines.getValeurvariable();
        String statut = mesureLines.getStatutvaleur();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());

        String cunite = Utils.createCodeFromString(unite);

        String cmethode = Utils.createCodeFromString(methode);

        String chumidite = Utils.createCodeFromString(humidite);

        String cvariable = Utils.createCodeFromString(variable);

        VariablesPRO dbvariable = variPRODAO.betByNKey(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordFluxChambre.BUNDLE_PATH_FLUX,
                    ProcessRecordFluxChambre.MSG_ERROR_FLUX_NOT_VARIABLEPRO_DB), cvariable));
        }
        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordFluxChambre.BUNDLE_PATH_FLUX,
                    ProcessRecordFluxChambre.MSG_ERROR_FLUX_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumidite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordFluxChambre.BUNDLE_PATH_FLUX,
                    ProcessRecordFluxChambre.MSG_ERROR_FLUX_NOT_FOUND_HUMIDITE_DB), chumidite));
        }

        Unitepro dbunitepro = uniteproDAO.getByNKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordFluxChambre.BUNDLE_PATH_FLUX,
                    ProcessRecordFluxChambre.MSG_ERROR_FLUX_NOT_UNITEPRO_DB), cunite));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(datatype, dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {

            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordFluxChambre.BUNDLE_PATH_FLUX,
                    ProcessRecordFluxChambre.MSG_ERROR_FLUX_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumidite));
        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        Long fichier = mesureLines.getOriginalLineNumber();

        final ParcelleElementaire parcelleElementaire = sessionPropertiesPRO.getParcelleElementaire();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);

        MesureFluxChambres mesureFluxChambres = new MesureFluxChambres(date, versionFile, parcelleElementaire, fichier, nbr_chambres, nbr_cycles);

        mesureFluxChambres.setLocalDate(date);
        mesureFluxChambres.setParcelleElementaire(parcelleElementaire);
        mesureFluxChambres.setLigneFichierEchange(fichier);
        mesureFluxChambres.setNbrChambre(nbr_chambres);
        mesureFluxChambres.setNbrCycle(nbr_cycles);
        mesureFluxChambres.setVersionFile(versionFileDB);

        final ValeurFluxChambres valeurFluxChambre = new ValeurFluxChambres();
        valeurFluxChambre.setRealNode(dbdvumRealNode);
        valeurFluxChambre.setValeur(valeur);
        valeurFluxChambre.setMesureFluxChambres(mesureFluxChambres);
        valeurFluxChambre.setStatutvaleur(statut);
        mesureFluxChambres.getValeurs().add(valeurFluxChambre);
        mesureFluxChambreDAO.saveOrUpdate(mesureFluxChambres);

    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<FluxChambreLineRecord>> lines,
            final SortedSet<FluxChambreLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<FluxChambreLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<FluxChambreLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (FluxChambreLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    private long readLines(final CSVParser parser, final Map<LocalDate, List<FluxChambreLineRecord>> lines,
            long lineCount, ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;

            final LocalDate date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final int nbr_chambres = Integer.parseInt(cleanerValues.nextToken());
            final int nbr_cycles = Integer.parseInt(cleanerValues.nextToken());
            final String codevariable = cleanerValues.nextToken();
            final float valeurflux = Float.parseFloat(cleanerValues.nextToken());
            final String statut = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();

            final FluxChambreLineRecord line = new FluxChambreLineRecord(date, nbr_chambres, nbr_cycles, lineCount, codevariable, valeurflux, statut, codemethode, codeunite, codehumidite);
            try {
                if (!lines.containsKey(date)) {
                    lines.put(date, new LinkedList<FluxChambreLineRecord>());
                }
                lines.get(date).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        date, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String fileEncoding,
            DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();

        try {

            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);
            final Map<LocalDate, List<FluxChambreLineRecord>> mesuresMapLines = new HashMap();
            //           final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<FluxChambreLineRecord> ligneEnErreur = new TreeSet();

            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }

            this.recordErrors(errorsReport);

        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordFluxChambre.class.getName()).error(ex.getMessage(), ex);
        }
    }

    private void recordErrors(ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureFluxChambreDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IMesureFluxChambreDAO<MesureFluxChambres> getMesureFluxChambreDAO() {
        return mesureFluxChambreDAO;
    }

    public void setMesureFluxChambreDAO(IMesureFluxChambreDAO<MesureFluxChambres> mesureFluxChambreDAO) {
        this.mesureFluxChambreDAO = mesureFluxChambreDAO;
    }

    public IParcelleElementaireDAO getParcelleElementaireDAO() {
        return parcelleElementaireDAO;
    }

    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public IVersionFileDAO getVersionFileDAO() {
        return versionFileDAO;
    }

    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

}
