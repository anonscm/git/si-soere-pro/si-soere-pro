/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.constantspc;

/**
 *
 * @author adiankha
 */
public class ConstantsPhysicoChimie {
    
    /**
     *
     */
    public static final String KEY_DATATYPE_PHYSICO_CHIMIE = "physico_chimie" ;
     
    /**
     *
     */
    public static final String KEY_VARIABLE_PHYSICO_CHIMIE_PRO_BRUT = "PRO_Physico-chimie_donnees elementaires" ;
    
}
