package org.inra.ecoinfo.pro.refdata.compositioncmp;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.ValeurCaracteristiqueMP;

/**
 *
 * @author ptcherniati
 */
public class CompositionCMPDAOImpl extends AbstractJPADAO<CompositionsCMP> implements ICompositionCMPDAO {

    /**
     *
     * @return
     */
    @Override
    public List<CompositionsCMP> getAll() {
        return getAllBy(CompositionsCMP.class, CompositionsCMP_.melange);
    }

    /**
     *
     * @param melange
     * @param vcmp
     * @return
     */
    @Override
    public Optional<CompositionsCMP> getByNKey(Melange melange, ValeurCaracteristiqueMP vcmp) {
        CriteriaQuery<CompositionsCMP> query = builder.createQuery(CompositionsCMP.class);
        Root<CompositionsCMP> compositionsCMP = query.from(CompositionsCMP.class);
        Join<CompositionsCMP, Melange> mel = compositionsCMP.join(CompositionsCMP_.melange);
        Join<CompositionsCMP, ValeurCaracteristiqueMP> valeurCar = compositionsCMP.join(CompositionsCMP_.valeurcaracteristiquemp);
        query
                .select(compositionsCMP)
                .where(
                        builder.equal(mel, melange),
                        builder.equal(valeurCar, vcmp)
                );
        return getOptional(query);

    }

}
