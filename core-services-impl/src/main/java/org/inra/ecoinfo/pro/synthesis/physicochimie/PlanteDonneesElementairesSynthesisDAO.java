/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire_;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantebrut.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantebrut.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class PlanteDonneesElementairesSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurPlanteElementaire, MesurePlanteElementaire> {

    @Override
    Class<ValeurPlanteElementaire> getValueClass() {
        return ValeurPlanteElementaire.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurPlanteElementaire, MesurePlanteElementaire> getMesureAttribute() {
        return ValeurPlanteElementaire_.mesurePlanteElementaire;
    }
}
