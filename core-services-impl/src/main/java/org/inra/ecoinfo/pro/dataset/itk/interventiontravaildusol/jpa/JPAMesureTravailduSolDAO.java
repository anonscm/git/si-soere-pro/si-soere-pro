/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.IMesureTravailduSolDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol_;

/**
 *
 * @author adiankha
 */
public class JPAMesureTravailduSolDAO extends AbstractJPADAO<MesureTravailDuSol> implements IMesureTravailduSolDAO<MesureTravailDuSol> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesureTravailDuSol> getByKeys(String keymesure) {
        CriteriaQuery<MesureTravailDuSol> query = builder.createQuery(MesureTravailDuSol.class);
        Root<MesureTravailDuSol> m = query.from(MesureTravailDuSol.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureTravailDuSol_.keymesure), keymesure)
                );
        return getOptional(query);
    }
}
