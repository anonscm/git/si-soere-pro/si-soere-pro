/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.stadedeveloppement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IStadeDAO extends IDAO<StadeDeveloppement> {

    /**
     *
     * @return
     */
    List<StadeDeveloppement> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<StadeDeveloppement> getByName(String nom);

    /**
     *
     * @param code
     * @return
     */
    Optional<StadeDeveloppement> getByNKey(String code);

}
