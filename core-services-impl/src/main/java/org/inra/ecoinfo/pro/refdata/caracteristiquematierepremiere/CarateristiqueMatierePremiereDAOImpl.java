package org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques_;

public class CarateristiqueMatierePremiereDAOImpl extends AbstractJPADAO<CaracteristiqueMatierePremieres> implements ICaracteristiqueMatierePremiereDAO {

    @Override
    public List<CaracteristiqueMatierePremieres> getAll() {
        return getAllBy(CaracteristiqueMatierePremieres.class, CaracteristiqueMatierePremieres::getCmp_code);
    }

    @Override
    public Optional<CaracteristiqueMatierePremieres> getByNomAndType(String cmp_nom, String cmp_type) {
        CriteriaQuery<CaracteristiqueMatierePremieres> query = builder.createQuery(CaracteristiqueMatierePremieres.class);
        Root<CaracteristiqueMatierePremieres> cmp = query.from(CaracteristiqueMatierePremieres.class);
        Join<CaracteristiqueMatierePremieres, TypeCaracteristiques> type = cmp.join(CaracteristiqueMatierePremieres_.typecaracteristique);
        query
                .select(cmp)
                .where(
                        builder.equal(cmp.get(CaracteristiqueMatierePremieres_.cmp_nom), cmp_nom),
                        builder.equal(type.get(TypeCaracteristiques_.tcar_nom), cmp_type)
                );
        return getOptional(query);
    }

    @Override
    public Optional<CaracteristiqueMatierePremieres> getByNKey(String cmp_nom, TypeCaracteristiques tc) {
        CriteriaQuery<CaracteristiqueMatierePremieres> query = builder.createQuery(CaracteristiqueMatierePremieres.class);
        Root<CaracteristiqueMatierePremieres> cmp = query.from(CaracteristiqueMatierePremieres.class);
        query
                .select(cmp)
                .where(
                        builder.equal(cmp.get(CaracteristiqueMatierePremieres_.cmp_nom), cmp_nom),
                        builder.equal(cmp.get(CaracteristiqueMatierePremieres_.typecaracteristique), tc)
                );
        return getOptional(query);
    }
}
