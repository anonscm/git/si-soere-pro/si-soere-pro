/*
 *
 */
package org.inra.ecoinfo.pro.refdata.matierepremiere;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

// TODO: Auto-generated Javadoc
/**
 * The Interface IMatierePremiereDAO.
 */
public interface IMatierePremiereDAO extends IDAO<MatieresPremieres> {

    /**
     *
     * @return
     */
    List<MatieresPremieres> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<MatieresPremieres> getByNKey(String nom);

}
