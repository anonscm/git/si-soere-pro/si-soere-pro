/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.IMesureIncubationSolProMoyDAO;

/**
 *
 * @author vjkoyao
 */

public class JPAMesureIncubationSolProMoyDAO extends AbstractJPADAO<MesureIncubationSolProMoy> implements IMesureIncubationSolProMoyDAO<MesureIncubationSolProMoy>{

    /**
     *
     * @param key
     * @return
     */
    @Override
    public Optional<MesureIncubationSolProMoy> getByKeys(String key){
        CriteriaQuery<MesureIncubationSolProMoy> query = builder.createQuery(MesureIncubationSolProMoy.class);
        Root<MesureIncubationSolProMoy> m = query.from(MesureIncubationSolProMoy.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureIncubationSolProMoy_.keymesure), key)
                );
        return getOptional(query);
    }
    
}
