/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeobservationqualitative;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author vjkoyao
 */
public interface ITypeObservationQualitativeDAO extends IDAO<TypeObservationQualitative> {

    /**
     *
     * @return
     */
    List<TypeObservationQualitative> getAll();

    /**
     *
     * @param type_observation_nom
     * @return
     */
    Optional<TypeObservationQualitative> getByNKey(String type_observation_nom);

}
