package org.inra.ecoinfo.pro.refdata.texturesol;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.Teneurcalcaireinitial;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

public class Recorder extends AbstractCSVMetadataRecorder<Texturesol> {
    
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_TENEUR = "PROPERTY_MSG_TENEUR";
    ITextureSolDAO textureDAO;
    private Properties textureSolEN;
    
    private void createTexture(final Texturesol texturesol) throws BusinessException {
        try {
            textureDAO.saveOrUpdate(texturesol);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create texturesol");
        }
        textureDAO.flush();
    }
    
    private void updateTexture(final String code, String nom, final Texturesol dbtexturesol) throws BusinessException {
        try {
            dbtexturesol.setCode(code);
            dbtexturesol.setNom(nom);
            textureDAO.saveOrUpdate(dbtexturesol);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update texturesol");
        }
    }
    
    private void createOrUpdateTCInitial(final String code, String nom, final Texturesol dbtexturesol) throws BusinessException {
        if (dbtexturesol == null) {
            final Texturesol texturesol = new Texturesol(code, nom);
            texturesol.setCode(code);
            texturesol.setNom(nom);
            createTexture(texturesol);
        } else {
            updateTexture(code, nom, dbtexturesol);
        }
    }
    
    private void persistTexture(final String code, final String nom) throws BusinessException, BusinessException {
        final Texturesol dbtexture = textureDAO.getByNKey(nom).orElse(null);
        createOrUpdateTCInitial(code, nom, dbtexture);
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Texturesol texturesol) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(texturesol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : texturesol.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(texturesol == null || texturesol.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : textureSolEN.getProperty(texturesol.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Teneurcalcaireinitial.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                Texturesol dbtexturesol = textureDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't find textureSol"));
                textureDAO.remove(dbtexturesol);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected List<Texturesol> getAllElements() throws BusinessException {
        return textureDAO.getAll();
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Texturesol.NAME_ENTITY_JPA);
                String texture = tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(texture);
                
                if (!errorsReport.hasErrors()) {
                    persistTexture(code, texture);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    public ITextureSolDAO getTextureDAO() {
        return textureDAO;
    }
    
    public void setTextureDAO(ITextureSolDAO textureDAO) {
        this.textureDAO = textureDAO;
    }
    
    @Override
    protected ModelGridMetadata<Texturesol> initModelGridMetadata() {
        
        textureSolEN = localizationManager.newProperties(Texturesol.NAME_ENTITY_JPA, Texturesol.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
