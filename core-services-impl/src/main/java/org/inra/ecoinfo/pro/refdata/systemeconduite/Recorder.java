/**
 *
 */
package org.inra.ecoinfo.pro.refdata.systemeconduite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<SystemeConduiteEssai> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ISystemeConduiteEssaiDAO systemeConduiteEssaiDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                systemeConduiteEssaiDAO.remove(systemeConduiteEssaiDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get systeme de conduite")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<SystemeConduiteEssai> getAllElements() throws BusinessException {
        return systemeConduiteEssaiDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(SystemeConduiteEssai systemeConduiteEssai) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesLibelle = localizationManager.newProperties(SystemeConduiteEssai.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_STC, Locale.ENGLISH);
        
        String localizedChampLibelle = "";
        
        if (systemeConduiteEssai != null) {
            localizedChampLibelle = propertiesLibelle.containsKey(systemeConduiteEssai.getLibelle()) ? propertiesLibelle.getProperty(systemeConduiteEssai.getLibelle()) : systemeConduiteEssai.getLibelle();
        }
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(systemeConduiteEssai == null ? Constantes.STRING_EMPTY : systemeConduiteEssai.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(systemeConduiteEssai == null ? Constantes.STRING_EMPTY : localizedChampLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, SystemeConduiteEssai.TABLE_NAME);
                
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                if (libelle == null || libelle.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                SystemeConduiteEssai systemeConduiteEssai = new SystemeConduiteEssai(libelle);
                SystemeConduiteEssai dbSystemeConduiteEssai = systemeConduiteEssaiDAO.getByNKey(libelle).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbSystemeConduiteEssai == null) {
                        systemeConduiteEssaiDAO.saveOrUpdate(systemeConduiteEssai);
                    } else {
                        dbSystemeConduiteEssai.setLibelle(libelle);
                        systemeConduiteEssaiDAO.saveOrUpdate(dbSystemeConduiteEssai);
                    }
                }
                
                values = parser.getLine();
            } 
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param systemeConduiteEssaiDAO
     */
    public void setSystemeConduiteEssaiDAO(ISystemeConduiteEssaiDAO systemeConduiteEssaiDAO) {
        this.systemeConduiteEssaiDAO = systemeConduiteEssaiDAO;
    }
    
}
