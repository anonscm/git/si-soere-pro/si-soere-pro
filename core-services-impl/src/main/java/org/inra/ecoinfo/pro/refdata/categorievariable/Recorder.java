/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.categorievariable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<CategorieVariable> {

    ICategorieVariableDAO categorievariableDAO;
    Properties cvnomEn;
    Properties cvdescripEn;
    Properties codeEn;

    /**
     *
     * @param cvariable
     * @throws BusinessException
     */
    public void createCategorieVariable(CategorieVariable cvariable) throws BusinessException {
        try {
            categorievariableDAO.saveOrUpdate(cvariable);
            categorievariableDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create CategorieVariable");
        }
    }

    private void updateCategorieVariable(final String nom, String code, String mycode, String description, final CategorieVariable dbcvariable) throws BusinessException {
        try {
            dbcvariable.setCvariable_mycode(mycode);
            dbcvariable.setCvariable_code(code);
            dbcvariable.setCvariable_nom(nom);
            dbcvariable.setCvariable_definition(description);
            categorievariableDAO.saveOrUpdate(dbcvariable);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update CategorieVariable");
        }
    }

    private void createOrUpdateCategorieVariable(String nom, String code, String mycode, String description, final CategorieVariable dbcvariable) throws BusinessException {
        if (dbcvariable == null) {
            final CategorieVariable categorieVariable = new CategorieVariable(code, nom, description);
            categorieVariable.setCvariable_mycode(mycode);
            categorieVariable.setCvariable_code(code);
            categorieVariable.setCvariable_nom(nom);
            categorieVariable.setCvariable_definition(description);
            createCategorieVariable(categorieVariable);
        } else {
            updateCategorieVariable(nom, code, mycode, description, dbcvariable);
        }
    }

    private void persistCategorieVariable(String code, String nom, String mycode, String description) throws BusinessException, BusinessException {
        final CategorieVariable cvariable = categorievariableDAO.getByName((nom)).orElse(null);
        createOrUpdateCategorieVariable(nom, code, mycode, description, cvariable);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nom = tokenizerValues.nextToken();
                categorievariableDAO.remove(categorievariableDAO.getByNKey(nom).orElseThrow(PersistenceException::new));
                values = csvp.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<CategorieVariable> getAllElements() throws BusinessException {
        return categorievariableDAO.gettAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CategorieVariable.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String libelle = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(code);
                final String desc = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistCategorieVariable(code, libelle, mycode, desc);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CategorieVariable categorieVariable) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(categorieVariable == null || categorieVariable.getCvariable_code() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : categorieVariable.getCvariable_code(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(categorieVariable == null || categorieVariable.getCvariable_code() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : codeEn.getProperty(categorieVariable.getCvariable_code()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(categorieVariable == null || categorieVariable.getCvariable_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : categorieVariable.getCvariable_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(categorieVariable == null || categorieVariable.getCvariable_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cvnomEn.getProperty(categorieVariable.getCvariable_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(categorieVariable == null || categorieVariable.getCvariable_definition() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : categorieVariable.getCvariable_definition(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(categorieVariable == null || categorieVariable.getCvariable_definition() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cvdescripEn.getProperty(categorieVariable.getCvariable_definition()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<CategorieVariable> initModelGridMetadata() {
        cvnomEn = localizationManager.newProperties(CategorieVariable.NAME_ENTITY_JPA, CategorieVariable.JPA_COLUMN_NAME, Locale.ENGLISH);
        cvdescripEn = localizationManager.newProperties(CategorieVariable.NAME_ENTITY_JPA, CategorieVariable.JPA_COLUMN_DEFINITION, Locale.ENGLISH);
        codeEn = localizationManager.newProperties(CategorieVariable.NAME_ENTITY_JPA, CategorieVariable.JPA_COLUMN_KEY, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param categorievariableDAO
     */
    public void setCategorievariableDAO(ICategorieVariableDAO categorievariableDAO) {
        this.categorievariableDAO = categorievariableDAO;
    }

}
