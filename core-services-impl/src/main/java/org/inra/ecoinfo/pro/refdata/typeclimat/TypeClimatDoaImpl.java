package org.inra.ecoinfo.pro.refdata.typeclimat;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class TypeClimatDoaImpl extends AbstractJPADAO<Typeclimat> implements ITypeClimatDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Typeclimat> getAll() {
        return getAllBy(Typeclimat.class, Typeclimat::getCode);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Typeclimat> getByNKey(String nom) {
        CriteriaQuery<Typeclimat> query = builder.createQuery(Typeclimat.class);
        Root<Typeclimat> typeclimat = query.from(Typeclimat.class);
        query
                .select(typeclimat)
                .where(
                        builder.equal(typeclimat.get(Typeclimat_.code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }

}
