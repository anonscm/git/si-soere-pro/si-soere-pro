/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.IMesurePlanteBrutDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.IValeurPlanteBrutDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.echantillonplante.EchantillonPlante;
import org.inra.ecoinfo.pro.refdata.echantillonplante.IEchantillonPlanteDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordPlanteBrut extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPlanteBrut.class);
    private static final String BUNDLE_PATH_PLANTE_BRUT = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.messages";
    private static final String MSG_ERROR_PLANTE_NOT_FOUND_DVU_DB = "MSG_ERROR_PLANTE_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_PLANTE_NOT_VARIABLEPRO_DB = "MSG_ERROR_PLANTE_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_PLANTE_NOT_FOUND_METHODE_DB = "MSG_ERROR_PLANTE_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_PLANTE_NOT_UNITEPRO_DB = "MSG_ERROR_PLANTE_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_PLANTE_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_PLANTE_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_PLANTE_ECHAN_BD = "MSG_ERROR_PLANTE_ECHAN_BD";

    protected IMesurePlanteBrutDAO<MesurePlanteElementaire> mesurePlanteBrutDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IEchantillonPlanteDAO echantillonPlanteDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IValeurPlanteBrutDAO valeurPlanteBrutDAO;

    public ProcessRecordPlanteBrut() {
        super();
    }

    void buildMesure(final PlanteLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<PlanteLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException {

        LocalDate date = mesureLines.getDateprelevement();
        String codeech = mesureLines.getCodeechantillon();
        final EchantillonPlante echantillonsPlante = echantillonPlanteDAO.getByNKey(Utils.createCodeFromString(codeech)).orElse(null);
        if (echantillonsPlante == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    ProcessRecordPlanteBrut.MSG_ERROR_PLANTE_ECHAN_BD), codeech));
        }
        String nomlabo = mesureLines.getNomlabo();
        String numerolabo = mesureLines.getReference();
        int numerorepet = mesureLines.getNumerorepet();
        Long fichier = mesureLines.getOriginalLineNumber();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);
        String variable = mesureLines.getCodevariable();
        float valeur = mesureLines.getValeurvariable();
        String statut = mesureLines.getStatutvaleur();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();
        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cvariable = Utils.createCodeFromString(variable);
        String cunite = Utils.createCodeFromString(unite);
        String cmethode = Utils.createCodeFromString(methode);
        String chumitite = Utils.createCodeFromString(humidite);

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    ProcessRecordPlanteBrut.MSG_ERROR_PLANTE_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    ProcessRecordPlanteBrut.MSG_ERROR_PLANTE_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    ProcessRecordPlanteBrut.MSG_ERROR_PLANTE_NOT_FOUND_HUMIDITE_DB), chumitite));
        }

        VariablesPRO dbvariable = variPRODAO.getByCodeUser(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    ProcessRecordPlanteBrut.MSG_ERROR_PLANTE_NOT_VARIABLEPRO_DB), cvariable));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(RecorderPRO.getDatatypeFromVersion(versionFile), dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    ProcessRecordPlanteBrut.MSG_ERROR_PLANTE_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumitite));

        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesurePlanteElementaire mesurePlanteB = getOrCreate(date, numerorepet, codeech, nomlabo, versionFile, echantillonsPlante, numerolabo, fichier, versionFileDB);
            persitValeurPlante(valeur, statut, dbdvumRealNode, mesurePlanteB);
        }

    }

    private MesurePlanteElementaire getOrCreate(LocalDate date, int numerorepet, String codeech, String nomlabo, final VersionFile versionFile, final EchantillonPlante echantillonsPlante, String codelabo, Long fichier, final VersionFile versionFileDB) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(date, format);
        String intString = null;
        intString = Integer.toString(numerorepet);
        String key = dateString + "_" + codeech + "_" + nomlabo + "_" + intString;

        MesurePlanteElementaire mesurePlanteBrut = mesurePlanteBrutDAO.getByKeys(key).orElse(null);
        if (mesurePlanteBrut == null) {
            mesurePlanteBrut = new MesurePlanteElementaire(date, echantillonsPlante, nomlabo, codelabo, numerorepet, fichier, versionFile);
            mesurePlanteBrut.setDatePrelevement(date);
            mesurePlanteBrut.setEchantillonPlante(echantillonsPlante);
            mesurePlanteBrut.setLigneFichierEchange(fichier);
            mesurePlanteBrut.setNomlabo(nomlabo);
            mesurePlanteBrut.setCodelabo(codelabo);
            mesurePlanteBrut.setNumero_repetition(numerorepet);
            mesurePlanteBrut.setVersionfile(versionFileDB);
            mesurePlanteBrut.setKeymesure(key);
            mesurePlanteBrutDAO.saveOrUpdate(mesurePlanteBrut);
        }
        return mesurePlanteBrut;
    }

    private void persitValeurPlante(float valeur, String statut, RealNode dbdvumRealNode, MesurePlanteElementaire mesurePlanteBrut) throws PersistenceException {
        final ValeurPlanteElementaire valeurSol = valeurPlanteBrutDAO.getByNKeys(dbdvumRealNode, mesurePlanteBrut, valeur, statut).orElse(null);
        createOrUpdateValeurSol(dbdvumRealNode, mesurePlanteBrut, valeur, statut, valeurSol);
    }

    private void CreateDTVQ(final ValeurPlanteElementaire valeurPlanteBrut) throws PersistenceException {
        valeurPlanteBrutDAO.saveOrUpdate(valeurPlanteBrut);
    }

    private void updateValeurSol(RealNode dvumRealNode, MesurePlanteElementaire mesurePlanteBrut, float valeur, String statutvaleur, ValeurPlanteElementaire valeurPlanteElementaire) throws PersistenceException {
        valeurPlanteElementaire.setRealNode(dvumRealNode);
        valeurPlanteElementaire.setMesurePlanteElementaire(mesurePlanteBrut);
        valeurPlanteElementaire.setValeur(valeur);
        valeurPlanteElementaire.setStatutvaleur(statutvaleur);
        valeurPlanteBrutDAO.saveOrUpdate(valeurPlanteElementaire);
    }

    void createOrUpdateValeurSol(RealNode dvumRealNode, MesurePlanteElementaire mesurePlanteBrut, float valeur, String statutvaleur, ValeurPlanteElementaire valeurPlanteElementaire) throws PersistenceException {
        if (valeurPlanteElementaire == null) {
            ValeurPlanteElementaire valeurplante = new ValeurPlanteElementaire(valeur, statutvaleur, mesurePlanteBrut, dvumRealNode);
            valeurplante.setRealNode(dvumRealNode);
            valeurplante.setMesurePlanteElementaire(mesurePlanteBrut);
            valeurplante.setValeur(valeur);
            valeurplante.setStatutvaleur(statutvaleur);
            CreateDTVQ(valeurplante);
        } else {
            updateValeurSol(dvumRealNode, mesurePlanteBrut, valeur, statutvaleur, valeurPlanteElementaire);
        }
    }

    private long readLines(final CSVParser parser, final Map<LocalDate, List<PlanteLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate dateprelevement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeech = cleanerValues.nextToken();
            final String labonom = cleanerValues.nextToken();
            final String numeroechan = cleanerValues.nextToken();
            final int numerorepe = Integer.parseInt(cleanerValues.nextToken());
            final String codevariable = cleanerValues.nextToken();
            final float valeur = Float.parseFloat(cleanerValues.nextToken());
            final String statut = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();
            final PlanteLineRecord line = new PlanteLineRecord(dateprelevement, codeech, labonom, numeroechan, numerorepe, codevariable, valeur, statut, codemethode, codeunite, codehumidite, lineCount);
            try {
                if (!lines.containsKey(dateprelevement)) {
                    lines.put(dateprelevement, new LinkedList<PlanteLineRecord>());
                }
                lines.get(dateprelevement).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        dateprelevement, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void recordErrors(final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<PlanteLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<PlanteLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LOGGER.error(null, ex);
            throw new BusinessException(ex);
        }
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<PlanteLineRecord>> lines,
            final SortedSet<PlanteLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<PlanteLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<PlanteLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (PlanteLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesurePlanteBrutDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IMesurePlanteBrutDAO<MesurePlanteElementaire> getMesurePlanteBrutDAO() {
        return mesurePlanteBrutDAO;
    }

    public void setMesurePlanteBrutDAO(IMesurePlanteBrutDAO<MesurePlanteElementaire> mesurePlanteBrutDAO) {
        this.mesurePlanteBrutDAO = mesurePlanteBrutDAO;
    }

    public IEchantillonPlanteDAO getEchantillonPlanteDAO() {
        return echantillonPlanteDAO;
    }

    public void setEchantillonPlanteDAO(IEchantillonPlanteDAO echantillonPlanteDAO) {
        this.echantillonPlanteDAO = echantillonPlanteDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setValeurPlanteBrutDAO(IValeurPlanteBrutDAO valeurPlanteBrutDAO) {
        this.valeurPlanteBrutDAO = valeurPlanteBrutDAO;
    }

}
