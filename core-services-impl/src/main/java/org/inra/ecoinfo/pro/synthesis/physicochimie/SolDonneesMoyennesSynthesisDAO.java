/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy_;
import org.inra.ecoinfo.pro.synthesis.physicochimiesolmoy.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.physicochimiesolmoy.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class SolDonneesMoyennesSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurPhysicoChimieSolsMoy, MesurePhysicoChimieSolsMoy> {

    @Override
    Class<ValeurPhysicoChimieSolsMoy> getValueClass() {
        return ValeurPhysicoChimieSolsMoy.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurPhysicoChimieSolsMoy, MesurePhysicoChimieSolsMoy> getMesureAttribute() {
        return ValeurPhysicoChimieSolsMoy_.mesurePhysicoChimieSolsMoy;
    }
}
