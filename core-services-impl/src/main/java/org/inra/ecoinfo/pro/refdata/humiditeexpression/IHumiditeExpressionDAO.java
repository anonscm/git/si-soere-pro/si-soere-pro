/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.humiditeexpression;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IHumiditeExpressionDAO extends IDAO<HumiditeExpression> {

    /**
     *
     * @return
     */
    List<HumiditeExpression> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<HumiditeExpression> getByCode(String code);

    /**
     *
     * @param mycode
     * @return
     */
    Optional<HumiditeExpression> getByNKey(String mycode);

}
