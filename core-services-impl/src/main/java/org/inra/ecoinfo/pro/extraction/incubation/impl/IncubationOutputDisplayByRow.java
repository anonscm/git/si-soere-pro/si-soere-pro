/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
public class IncubationOutputDisplayByRow extends IncubationOutputsBuildersResolver {
    
    IOutputBuilder incubationSolBuildOutputByRow ;
    IOutputBuilder incubationSolProBuildOutputByRow ;
    IOutputBuilder incubationSolMoyBuildOutputByRow ;
    IOutputBuilder incubationSolProMoyBuildOutputByRow ;

    public IncubationOutputDisplayByRow() {
    }

    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) {
        try {
            super.setIncubationSolOutputBuilder(this.incubationSolBuildOutputByRow);
            super.setIncubationSolProOutputBuilder(this.incubationSolProBuildOutputByRow);
            super.setIncubationSolMoyOutputBuilder(this.incubationSolMoyBuildOutputByRow);
            super.setIncubationSolProMoyOutputBuilder(this.incubationSolProMoyBuildOutputByRow);
            
            return super.buildOutput(parameters);
        } catch (BusinessException e) {
            LoggerFactory.getLogger(getClass()).error("error while building output", e);
            return null;
        }
    }
    
    
    public void setIncubationSolBuildOutputByRow(IOutputBuilder incubationSolBuildOutputByRow) {
        this.incubationSolBuildOutputByRow = incubationSolBuildOutputByRow;
    }

    public void setIncubationSolProBuildOutputByRow(IOutputBuilder incubationSolProBuildOutputByRow) {
        this.incubationSolProBuildOutputByRow = incubationSolProBuildOutputByRow;
    }

    public void setIncubationSolMoyBuildOutputByRow(IOutputBuilder incubationSolMoyBuildOutputByRow) {
        this.incubationSolMoyBuildOutputByRow = incubationSolMoyBuildOutputByRow;
    }

    public void setIncubationSolProMoyBuildOutputByRow(IOutputBuilder incubationSolProMoyBuildOutputByRow) {
        this.incubationSolProMoyBuildOutputByRow = incubationSolProMoyBuildOutputByRow;
    }
    
    
}
