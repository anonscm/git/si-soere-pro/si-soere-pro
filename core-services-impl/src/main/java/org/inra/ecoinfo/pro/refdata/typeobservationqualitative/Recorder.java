/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeobservationqualitative;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeObservationQualitative> {

    /**
     *
     */
    protected ITypeObservationQualitativeDAO typeObservationQualitativeDAO;
    Properties libelleEn;

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String type_observation_nom = tokenizerValues.nextToken();

                typeObservationQualitativeDAO.remove(typeObservationQualitativeDAO.getByNKey(type_observation_nom)
                        .orElseThrow(()
                                -> new BusinessException("can't get type observation qualitative")
                        ));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, TypeObservationQualitative.NAME_ENTITY_JPA);
                final String type_observation_nom = tokenizerValues.nextToken();
                final String type_observation_code = Utils.createCodeFromString(type_observation_nom);
                persistTypeObservation(type_observation_nom, type_observation_code);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<TypeObservationQualitative> getAllElements() throws BusinessException {
        return typeObservationQualitativeDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeObservationQualitative typeObservation) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeObservation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typeObservation.getType_observation_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeObservation == null || typeObservation.getType_observation_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(typeObservation.getType_observation_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    private void persistTypeObservation(String type_observation_nom, String type_observation_code) throws BusinessException {
        final TypeObservationQualitative dbtypeobs = typeObservationQualitativeDAO.getByNKey(type_observation_nom).orElse(null);
        createOrUpdateTypeObservation(type_observation_nom, type_observation_code, dbtypeobs);
    }

    private void createOrUpdateTypeObservation(String type_observation_nom, String type_observation_code, TypeObservationQualitative dbtypeobs) throws BusinessException {
        if (dbtypeobs == null) {
            TypeObservationQualitative typeObservationQualitative = new TypeObservationQualitative(type_observation_nom, type_observation_code);

            typeObservationQualitative.setType_observation_nom(type_observation_nom);
            typeObservationQualitative.setType_observation_code(type_observation_code);

            createTypeObservation(typeObservationQualitative);

        } else {
            updateTypeObservation(type_observation_code, dbtypeobs);
        }
    }

    private void createTypeObservation(TypeObservationQualitative typeObservationQualitative) throws BusinessException {
        try {
            typeObservationQualitativeDAO.saveOrUpdate(typeObservationQualitative);
            typeObservationQualitativeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create typeObservationQualitative");
        }
    }

    private void updateTypeObservation(String type_observation_code, TypeObservationQualitative dbtypeobs) throws BusinessException {
        try {
            dbtypeobs.setType_observation_code(type_observation_code);
            typeObservationQualitativeDAO.saveOrUpdate(dbtypeobs);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update typeObservationQualitative");
        }
    }

    /**
     *
     * @return
     */
    public ITypeObservationQualitativeDAO getTypeObservationQualitativeDAO() {
        return typeObservationQualitativeDAO;
    }

    /**
     *
     * @param typeObservationQualitativeDAO
     */
    public void setTypeObservationQualitativeDAO(ITypeObservationQualitativeDAO typeObservationQualitativeDAO) {
        this.typeObservationQualitativeDAO = typeObservationQualitativeDAO;
    }

    @Override
    protected ModelGridMetadata<TypeObservationQualitative> initModelGridMetadata() {
        libelleEn = localizationManager.newProperties(TypeObservationQualitative.NAME_ENTITY_JPA, TypeObservationQualitative.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }
}
