package org.inra.ecoinfo.pro.refdata.procedemethodeprodmp;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.component.IComposantDAO;
import org.inra.ecoinfo.pro.refdata.melange.IMelangeDAO;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.process.IProcessDAO;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.unites.IUniteDAO;
import org.inra.ecoinfo.pro.refdata.unites.Unites;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

public class Recorder extends AbstractGenericRecorder<ProcedeMethodeProdMp> {;

    private static final String CODECOMPOSANT_N_EXISTE_PAS = "CODECOMPOSANT_N_EXISTE_PAS";

    private static final String NOM_PROCESS_N_EXISTE_PAS = "NOM_PROCESS_N_EXISTE_PAS";

    private static final String COMPOSITION_N_EXISTE_PAS = "COMPOSITION_N_EXISTE_PAS";

    private static final String DATE_EST_INVALIDE = "DATE_EST_INVALIDE";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MESSAGE_BAD_DATE_FORMAT = "PROPERTY_MESSAGE_BAD_DATE_FORMAT";

    protected IProMPProcedeDAO prompprocedeDAO;

    protected IMelangeDAO melangeDAO;

    protected IProcessDAO processDAO;

    protected IUniteDAO unitesDAO;
    
    protected IComposantDAO composantDAO;

    private String[] listeUnitesPossibles;

    private String[] listeProcedePossibles;
    Properties comentEn;

    private void createOrUpdateProPMP(Melange melange, Process process, int ordre, String commentaire, String duree, String unite, ProcedeMethodeProdMp dbprompp) throws BusinessException {
        if (dbprompp == null) {
            ProcedeMethodeProdMp procedemethodempp = new ProcedeMethodeProdMp(melange, process, ordre, commentaire, duree, unite);
            procedemethodempp.setMelange(melange);
            procedemethodempp.setProcess(process);
            procedemethodempp.setOdreprocede(ordre);
            procedemethodempp.setCommentaire(commentaire);
            procedemethodempp.setDuree(duree);
            procedemethodempp.setUnite(unite);
            createProPMP(procedemethodempp);
        } else {
            UpdateProPMP(melange, process, ordre, commentaire, duree, unite, dbprompp);
        }
    }

    private void createProPMP(final ProcedeMethodeProdMp prompp) throws BusinessException {
        try {
            prompprocedeDAO.saveOrUpdate(prompp);
            prompprocedeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prompp");
        }

    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeProduits = tokenizerValues.nextToken();
                final String codeComposant = tokenizerValues.nextToken();
                final Double pourcentage = Double.parseDouble(tokenizerValues.nextToken());
                final String procede = tokenizerValues.nextToken();
                final String ordre = tokenizerValues.nextToken();
                int order = Integer.parseInt(ordre);
                Melange melange = melangeDAO.getByNKey(codeProduits, codeComposant, pourcentage).orElse(null);
                Process process = processDAO.getByNKey(procede).orElse(null);
                final ProcedeMethodeProdMp dbprompp = prompprocedeDAO.getByNKey(melange, process, order)
                        .orElseThrow(() -> new BusinessException("can't get prompprocede"));
                prompprocedeDAO.remove(dbprompp);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    private LocalDate checkDateFormat(String madate, final ErrorsReport errorsReport, long line) throws NumberFormatException {
        try {
            return DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, madate);
        } catch (DateTimeParseException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(PRO_TYPO_SOURCE_PATH, PROPERTY_MESSAGE_BAD_DATE_FORMAT), line, madate, DateUtil.DD_MM_YYYY));
            return null;
        }
    }

    @Override
    protected List<ProcedeMethodeProdMp> getAllElements() throws BusinessException {
        return prompprocedeDAO.getAll();
    }

    public IMelangeDAO getMelangeDAO() {
        return melangeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProcedeMethodeProdMp prompp) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        org.inra.ecoinfo.pro.refdata.melange.Recorder.initNewLine(lineModelGridMetadata, prompp == null ? null : prompp.getMelange());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prompp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prompp.getProcess().getProcess_intitule() != null ? prompp.getProcess().getProcess_intitule() : "", listeProcedePossibles, null, true, false,
                        true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(prompp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prompp.getOdreprocede(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(prompp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prompp.getDuree(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prompp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prompp.getUnite() != null ? prompp.getUnite() : "", listeUnitesPossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(prompp == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : prompp.getCommentaire(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prompp == null || prompp.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comentEn.getProperty(prompp.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    public IProcessDAO getProcessDAO() {
        return processDAO;
    }

    public IProMPProcedeDAO getPrompprocedeDAO() {
        return prompprocedeDAO;
    }

    public IUniteDAO getUnitesDAO() {
        return unitesDAO;
    }

    @Override
    protected ModelGridMetadata<ProcedeMethodeProdMp> initModelGridMetadata() {
        listeDesProcedesPossibles();
        unitesNomPossibles();
        org.inra.ecoinfo.pro.refdata.melange.Recorder.initMelangesPossibles(melangeDAO);
        comentEn = localizationManager.newProperties(ProcedeMethodeProdMp.NAME_ENTITY_JPA, ProcedeMethodeProdMp.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void listeDesProcedesPossibles() {
        List<Process> groupenomp = processDAO.getAll(Process.class);
        String[] ListeNomPPossibles = new String[groupenomp.size() + 1];
        ListeNomPPossibles[0] = "";
        int index = 1;
        for (Process process : groupenomp) {
            ListeNomPPossibles[index++] = process.getProcess_intitule();
        }
        this.listeProcedePossibles = ListeNomPPossibles;
    }

    private void persistProMPP(Melange melange, Process process, int ordre, String commentaire, String duree, String unite) throws BusinessException, BusinessException {
        final ProcedeMethodeProdMp dbprompp = prompprocedeDAO.getByNKey(melange, process, ordre).orElse(null);

        createOrUpdateProPMP(melange, process, ordre, commentaire, duree, unite, dbprompp);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProcedeMethodeProdMp.NAME_ENTITY_JPA);
                int indexcomp = tokenizerValues.currentTokenIndex();
                final String codeProduit = tokenizerValues.nextToken();
                final String nomMatierePremiere = tokenizerValues.nextToken();
                final Double pourcentage = verifieDouble(tokenizerValues, line, true, errorsReport);
                int indexprocede = tokenizerValues.currentTokenIndex();
                final String IntituleProcede = tokenizerValues.nextToken();
                int order = verifieInt(tokenizerValues, line, true, errorsReport);
                final String dure = tokenizerValues.nextToken();
                final String unite = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                Melange dbmelange = melangeDAO.getByNKey(codeProduit, nomMatierePremiere, pourcentage).orElse(null);
                if (dbmelange == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.COMPOSITION_N_EXISTE_PAS), codeProduit, nomMatierePremiere, pourcentage));
                }
                Process dbprocess = processDAO.getByNKey(IntituleProcede).orElse(null);
                if (dbprocess == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.NOM_PROCESS_N_EXISTE_PAS), line, indexprocede, IntituleProcede));
                }
                if (!errorsReport.hasErrors()) {
                    persistProMPP(dbmelange, dbprocess, order, commentaire, dure, unite);
                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);

        }
    }

    public void setComposantDAO(IComposantDAO composantDAO) {
        this.composantDAO = composantDAO;
    }

    public void setMelangeDAO(IMelangeDAO melangeDAO) {
        this.melangeDAO = melangeDAO;
    }

    public void setProcessDAO(IProcessDAO processDAO) {
        this.processDAO = processDAO;
    }

    public void setPrompprocedeDAO(IProMPProcedeDAO prompprocedeDAO) {
        this.prompprocedeDAO = prompprocedeDAO;
    }

    public void setUnitesDAO(IUniteDAO unitesDAO) {
        this.unitesDAO = unitesDAO;
    }

    private void unitesNomPossibles() {
        List<Unites> groupeunite = unitesDAO.getAll(Unites.class);
        String[] Listeunite = new String[groupeunite.size() + 1];
        Listeunite[0] = "";
        int index = 1;
        for (Unites unites : groupeunite) {
            Listeunite[index++] = unites.getUnite();
        }
        this.listeUnitesPossibles = Listeunite;
    }

    private void UpdateProPMP(Melange melange, Process process, int ordre, String commentaire, String duree, String unite, ProcedeMethodeProdMp dbprompp) throws BusinessException {
        try {
            dbprompp.setMelange(melange);
            dbprompp.setProcess(process);
            dbprompp.setOdreprocede(ordre);
            dbprompp.setCommentaire(commentaire);
            dbprompp.setDuree(duree);
            dbprompp.setUnite(unite);
            prompprocedeDAO.saveOrUpdate(dbprompp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't UpdateProPMP");
        }
    }

}
