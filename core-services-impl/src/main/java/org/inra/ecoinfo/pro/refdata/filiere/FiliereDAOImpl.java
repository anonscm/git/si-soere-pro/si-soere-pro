/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.filiere;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class FiliereDAOImpl extends AbstractJPADAO<Filiere> implements IFiliereDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Filiere> getAll() {
        return getAll(Filiere.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Filiere> getByNKey(String nom) {
        CriteriaQuery<Filiere> query = builder.createQuery(Filiere.class);
        Root<Filiere> filiere = query.from(Filiere.class);
        query
                .select(filiere)
                .where(
                        builder.equal(filiere.get(Filiere_.filiere_code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }

}
