/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe_;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationRecolteCoupeDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurRecolteCoupe> deleteValeurs = builder.createCriteriaDelete(ValeurRecolteCoupe.class);
        Root<ValeurRecolteCoupe> valeur = deleteValeurs.from(ValeurRecolteCoupe.class);
        Subquery<MesureRecolteCoupe> subquery = deleteValeurs.subquery(MesureRecolteCoupe.class);
        Root<MesureRecolteCoupe> m = subquery.from(MesureRecolteCoupe.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureRecolteCoupe_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurRecolteCoupe_.mesureinterventionrecolte).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureRecolteCoupe> deleteSequence = builder.createCriteriaDelete(MesureRecolteCoupe.class);
        Root<MesureRecolteCoupe> sequence = deleteSequence.from(MesureRecolteCoupe.class);
        deleteSequence.where(builder.equal(sequence.get(MesureRecolteCoupe_.versionfile), version));
        delete(deleteSequence);
    }

}
