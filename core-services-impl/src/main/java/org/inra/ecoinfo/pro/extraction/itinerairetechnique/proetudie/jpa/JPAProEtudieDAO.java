/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.proetudie.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie_;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.jpa.JPAITKDAO;
import org.inra.ecoinfo.pro.synthesis.proetudie.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPAProEtudieDAO extends JPAITKDAO<MesureProEtudie, ValeurProEtudie> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurProEtudie> getValeurITKClass() {
        return ValeurProEtudie.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureProEtudie> getMesureITKClass() {
        return MesureProEtudie.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurProEtudie, MesureProEtudie> getMesureAttribute() {
        return ValeurProEtudie_.mesureinterventionproetudie;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }
}
