package org.inra.ecoinfo.pro.logging.impl;

import java.time.LocalDateTime;
import org.aspectj.lang.JoinPoint.StaticPart;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.LoggerObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class DefaultIdentificationLogging {

    /**
     *
     * @param staticPart
     * @param result
     */
    public void logCheckPasswordExit(StaticPart staticPart, Object result) {
        Logger logger = LoggerFactory.getLogger("logging");
        logger.info(new LoggerObject(((IUser) result).getLogin().trim(), LocalDateTime.now(), "s'est connecté").toString());
    }
}