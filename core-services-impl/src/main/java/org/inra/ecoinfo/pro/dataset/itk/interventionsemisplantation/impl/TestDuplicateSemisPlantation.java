/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.impl;

import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class TestDuplicateSemisPlantation extends AbstractTestDuplicate {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicateSemisPlantation.class.getName());

    static final long serialVersionUID = 1L;

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";

    static final String PROPERTY_MSG_DUPLICATE_LINE_SEMISPLANTATION = "PROPERTY_MSG_DUPLICATE_LINE_SEMISPLANTATION";

    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    public TestDuplicateSemisPlantation() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    protected void addLine(final String date, String codedisp, String codeTrait, final String parcelle, String placette, String culture,
            final String quantiteapport, String objetculture, final long lineNumber) {
        final String key = this.getKey(date, codedisp, codeTrait, parcelle, placette, culture, quantiteapport, objetculture);
        final String keySol = this.getKey(date, codedisp, codeTrait, parcelle, placette, culture, quantiteapport, objetculture);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, quantiteapport);
            } else if (!this.lineCouvert.get(keySol).equals(objetculture)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicateSemisPlantation.BUNDLE_SOURCE_PATH,
                        TestDuplicateSemisPlantation.PROPERTY_MSG_DUPLICATE_LINE_SEMISPLANTATION), lineNumber,
                        date, codedisp, codeTrait, parcelle, culture, quantiteapport, placette, this.lineCouvert.get(keySol), objetculture));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicateSemisPlantation.BUNDLE_SOURCE_PATH,
                    TestDuplicateSemisPlantation.PROPERTY_MSG_DUPLICATE_LINE_SEMISPLANTATION), lineNumber, date, codedisp, codeTrait, parcelle, culture, placette, quantiteapport,
                    objetculture, this.line.get(key)));

        }
    }

    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[10], lineNumber);
    }

}
