package org.inra.ecoinfo.pro.refdata.grandtypeproduit;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.origine.IOrigineDAO;
import org.inra.ecoinfo.pro.refdata.origine.Origines;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<GrandTypeProduits> {

    private static final String PROPERTY_MSG_GTP_BAD_ORIGINE = "PROPERTY_MSG_GTP_BAD_ORIGINE";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected IGrandTypeProduitDAO gtpDAO;

    /**
     *
     */
    protected IOrigineDAO origineDAO;

    /**
     *
     */
    protected String[] ListeOriginesPossibles;

    private Properties GTPNomEN;

    private Properties GTPCommenEn;

    private void createGrandTypeProduit(final GrandTypeProduits gtp) throws BusinessException {
        try {
            gtpDAO.saveOrUpdate(gtp);
            gtpDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create grand type de produits");
        }
    }

    private void createOrUpdateGrandTypeProduit(final String code, String bycode, String nom, String comment, Origines origines, final GrandTypeProduits dbgtp) throws BusinessException, BusinessException {
        if (dbgtp == null) {
            final GrandTypeProduits grandtypeproduits = new GrandTypeProduits(code, bycode, nom, comment, origines);
            grandtypeproduits.setGtp_code(code);
            grandtypeproduits.setGtp_bycode(bycode);
            grandtypeproduits.setGtp_nom(nom);
            grandtypeproduits.setGtp_comment(comment);
            grandtypeproduits.setOrigines(origines);
            createGrandTypeProduit(grandtypeproduits);
        } else {
            updateBDGTP(code, nom, comment, dbgtp);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                Origines origine = null;
                GrandTypeProduits gtp = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values, GrandTypeProduits.NAME_ENTITY_JPA);
                tokenizerValues.nextToken();
                String libelle = tokenizerValues.nextToken();
                gtp = gtpDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get grand type de produit"));
                gtpDAO.remove(gtp);
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<GrandTypeProduits> getAllElements() throws BusinessException {
        return gtpDAO.getAll();
    }

    /**
     *
     * @return
     */
    public Properties getGTPCommenEn() {
        return GTPCommenEn;
    }

    /**
     *
     * @return
     */
    public IGrandTypeProduitDAO getGtpDAO() {
        return gtpDAO;
    }

    /**
     *
     * @return
     */
    public Properties getGTPNomEN() {
        return GTPNomEN;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(GrandTypeProduits grandtypeproduits) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(grandtypeproduits == null || grandtypeproduits.getGtp_bycode() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : grandtypeproduits.getGtp_bycode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(grandtypeproduits == null || grandtypeproduits.getGtp_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : grandtypeproduits.getGtp_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(grandtypeproduits == null || grandtypeproduits.getGtp_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : GTPNomEN.getProperty(grandtypeproduits.getGtp_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(grandtypeproduits == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : (grandtypeproduits.getOrigines() != null ? grandtypeproduits.getOrigines().getOrigine_nom() : ""), ListeOriginesPossibles,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(grandtypeproduits == null || grandtypeproduits.getGtp_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : grandtypeproduits.getGtp_comment(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(grandtypeproduits == null || grandtypeproduits.getGtp_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : GTPCommenEn.getProperty(grandtypeproduits.getGtp_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;

    }

    /**
     *
     * @return
     */
    public IOrigineDAO getOrigineDAO() {
        return origineDAO;
    }

    @Override
    protected ModelGridMetadata<GrandTypeProduits> initModelGridMetadata() {
        GTPNomEN = localizationManager.newProperties(GrandTypeProduits.NAME_ENTITY_JPA, GrandTypeProduits.JPA_COLUMN_NAME, Locale.ENGLISH);
        GTPCommenEn = localizationManager.newProperties(GrandTypeProduits.NAME_ENTITY_JPA, GrandTypeProduits.JPA_COLUMN_COMENT, Locale.ENGLISH);
        updateNamesGroupesOriginesPossibles();
        return super.initModelGridMetadata();
    }

    private void persistGTP(final String code, final String bycode, final String nom, Origines origines, final String comment) throws BusinessException {
        final GrandTypeProduits dbgtp = gtpDAO.getByNKey(nom).orElse(null);
        createOrUpdateGrandTypeProduit(bycode, code, nom, comment, origines, dbgtp);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();

        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            long line = 0;
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, GrandTypeProduits.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom_fr = tokenizerValues.nextToken();
                final String bycode = Utils.createCodeFromString(nom_fr);
                final String origine_nom = tokenizerValues.nextToken();
                int index = tokenizerValues.currentTokenIndex();

                Origines orig = origineDAO.getByNKey(origine_nom).orElse(null);
                if (orig == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_GTP_BAD_ORIGINE), line, index, origine_nom));
                }
                final String comm_fr = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistGTP(code, bycode, nom_fr, orig, comm_fr);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param gTPCommenEn
     */
    public void setGTPCommenEn(Properties gTPCommenEn) {
        GTPCommenEn = gTPCommenEn;
    }

    /**
     *
     * @param gtpDAO
     */
    public void setGtpDAO(IGrandTypeProduitDAO gtpDAO) {
        this.gtpDAO = gtpDAO;
    }

    /**
     *
     * @param gTPNomEN
     */
    public void setGTPNomEN(Properties gTPNomEN) {
        GTPNomEN = gTPNomEN;
    }

    /**
     *
     * @param origineDAO
     */
    public void setOrigineDAO(IOrigineDAO origineDAO) {
        this.origineDAO = origineDAO;
    }

    private void updateBDGTP(final String code, String nom, String coment, final GrandTypeProduits dbgtp) throws BusinessException {
        try {
            dbgtp.setGtp_code(code);
            dbgtp.setGtp_nom(nom);
            dbgtp.setGtp_comment(coment);
            gtpDAO.saveOrUpdate(dbgtp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update grand type de produits");
        }

    }

    private void updateNamesGroupesOriginesPossibles() {
        List<Origines> groupeorigines = origineDAO.getAll(Origines.class);
        String[] listeOriginesPossibles = new String[groupeorigines.size() + 1];
        listeOriginesPossibles[0] = "";
        int index = 1;
        for (Origines origines : groupeorigines) {
            listeOriginesPossibles[index++] = origines.getOrigine_nom();
        }
        this.ListeOriginesPossibles = listeOriginesPossibles;
        //Collections.sort(this.ListeOriginesPossibles, new OriginesDaoImpl(Origines.JPA_COLUMN_NAME));

    }

}
