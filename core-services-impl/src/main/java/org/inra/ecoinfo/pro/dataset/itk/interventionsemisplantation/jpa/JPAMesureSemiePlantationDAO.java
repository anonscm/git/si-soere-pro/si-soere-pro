/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.IMesureSemisPlantationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation_;

/**
 *
 * @author adiankha
 */
public class JPAMesureSemiePlantationDAO extends AbstractJPADAO<MesureSemisPlantation> implements IMesureSemisPlantationDAO<MesureSemisPlantation> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesureSemisPlantation> getByKeys(String keymesure) {
        CriteriaQuery<MesureSemisPlantation> query = builder.createQuery(MesureSemisPlantation.class);
        Root<MesureSemisPlantation> m = query.from(MesureSemisPlantation.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureSemisPlantation_.keymesure), keymesure)
                );
        return getOptional(query);
    }
}
