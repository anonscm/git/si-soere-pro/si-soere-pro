
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.impl;

import java.time.LocalDate;
import java.time.LocalTime;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author vjkoyao
 */
public class MPSLineRecord implements Comparable<MPSLineRecord>{

    LocalDate datemesure;
    float profondeur;
    LocalTime heure;
    int num_repetition;
    Long originalLineNumber;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
    int code_parcelle;
    long lineCount;
    
    /**
     *
     */
    public MPSLineRecord() {
        super();
    }

    /**
     *
     * @param date
     * @param time
     * @param lineCount
     * @param codevariable
     * @param valeurvariable
     * @param statut
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     */
    public MPSLineRecord(LocalDate date, LocalTime time, long lineCount, String codevariable, float valeurvariable, String statut, String codemethode, String codeunite, String codehumidite) {
        this.datemesure = date;
        this.heure = time;
        this.lineCount = lineCount;
        this.codevariable = codevariable;
        this.valeurvariable = valeurvariable;
        this.statutvaleur = statut;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;
      
        
    }

    /**
     *
     * @param datemesure
     * @param profondeur
     * @param heure
     * @param num_repetition
     * @param originalLineNumber
     * @param codevariable
     * @param valeurvariable
     * @param statutvaleur
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     * @param code_parcelle
     */
    public MPSLineRecord(LocalDate datemesure, float profondeur, LocalTime heure, int num_repetition, Long originalLineNumber, String codevariable, float valeurvariable, String statutvaleur, String codemethode, String codeunite, String codehumidite, int code_parcelle) {
        this.datemesure = datemesure;
        this.profondeur = profondeur;
        this.heure = heure;
        this.num_repetition = num_repetition;
        this.originalLineNumber = originalLineNumber;
        this.codevariable = codevariable;
        this.valeurvariable = valeurvariable;
        this.statutvaleur = statutvaleur;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;
        this.code_parcelle = code_parcelle;
    }

    /**
     *
     * @param line
     */
    public void copy(final MPSLineRecord line){
        this.datemesure = line.getDatemesure();
        this.profondeur = line.getProfondeur();
        this.heure = line.getHeure();
        this.num_repetition = line.getNum_repetition();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.codevariable = line.getCodevariable();
        this.valeurvariable = line.getValeurvariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codemethode = line.getCodemethode();
        this.codeunite = line.getCodeunite();
        this.codehumidite = line.getCodehumidite();
        this.code_parcelle= line.getCode_parcelle();
    }
    
    @Override
    public int compareTo(MPSLineRecord o) {  
        int returnValue = -1 ;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    
    @Override
    public boolean equals(Object obj) {
         return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public LocalDate getDatemesure() {
        return datemesure;
    }

    /**
     *
     * @param datemesure
     */
    public void setDatemesure(LocalDate datemesure) {
        this.datemesure = datemesure;
    }

    /**
     *
     * @return
     */
    public float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(float profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public LocalTime getHeure() {
        return heure;
    }

    /**
     *
     * @param heure
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

    /**
     *
     * @return
     */
    public int getNum_repetition() {
        return num_repetition;
    }

    /**
     *
     * @param num_repetition
     */
    public void setNum_repetition(int num_repetition) {
        this.num_repetition = num_repetition;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeurvariable() {
        return valeurvariable;
    }

    /**
     *
     * @param valeurvariable
     */
    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public int getCode_parcelle() {
        return code_parcelle;
    }

    /**
     *
     * @param code_parcelle
     */
    public void setCode_parcelle(int code_parcelle) {
        this.code_parcelle = code_parcelle;
    }

    /**
     *
     * @return
     */
    public long getLineCount() {
        return lineCount;
    }

    /**
     *
     * @param lineCount
     */
    public void setLineCount(long lineCount) {
        this.lineCount = lineCount;
    }

   
    
    
    
    
}
