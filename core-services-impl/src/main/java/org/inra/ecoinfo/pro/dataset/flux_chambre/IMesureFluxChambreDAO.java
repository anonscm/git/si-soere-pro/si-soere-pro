/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre;

;
import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureFluxChambreDAO<T> extends IDAO<MesureFluxChambres> {

    /**
     *
     * @param date_messure
     * @param nbChambre
     * @param parcelle
     * @return
     */
    Optional<MesureFluxChambres> getByNKeys(LocalDate date_messure, int nbChambre, ParcelleElementaire parcelle);
}
