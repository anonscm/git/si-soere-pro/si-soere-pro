/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsol;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureIncubationSolDAO<T> extends IDAO<MesureIncubationSol> {

//    Object[] getLinePublicationNameDoublon(Date date_mesure)throws PersistenceException;

    /**
     *
     * @param key
     * @return
     */
    public Optional<MesureIncubationSol> getByKey(String key);
}
