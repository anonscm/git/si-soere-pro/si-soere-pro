/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.semisplantation.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDatatypeManager;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.ITKExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class SemisPlantationBuildOutputDisplayByRow extends AbstractITKOutputBuilder<MesureSemisPlantation> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_SEMIS_PLANTATION";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.itk.itk-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    static final String PATTERB_CSV_23_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    static final String LIST_COLUMN_VARIABLE_SEMIS = "LIST_COLUMN_VARIABLE_SEMIS";

    final ComparatorVariable comparator = new ComparatorVariable();

    IVariablesPRODAO variPRODAO;

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                SemisPlantationBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        SemisPlantationBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        SemisPlantationBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(ITKExtractor.CST_RESULT_EXTRACTION_SEMISPLANTATION_CODE)
                .get(SemisPlantationExtractor.MAP_INDEX_SEMIS_PLANTATION) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(ITKExtractor.CST_RESULT_EXTRACTION_SEMISPLANTATION_CODE)
                        .get(SemisPlantationExtractor.MAP_INDEX_SEMIS_PLANTATION).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ITKExtractor.CST_RESULT_EXTRACTION_SEMISPLANTATION_CODE));
        return null;
    }

    /**
     *
     * @param mesuresSemie
     * @param mesuresSemis
     */
    @Override
    protected void buildmap(List<MesureSemisPlantation> mesuresSemie, SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureSemisPlantation>>> mesuresSemis) {
        java.util.Iterator<MesureSemisPlantation> itMesure = mesuresSemie
                .iterator();
        while (itMesure.hasNext()) {
            MesureSemisPlantation mesurePlantation = itMesure
                    .next();
            DescriptionTraitement echan = mesurePlantation.getDescriptionTraitement();
            Long siteId = echan.getDispositif().getId();
            if (mesuresSemis.get(siteId) == null) {
                mesuresSemis.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresSemis.get(siteId).get(echan) == null) {
                mesuresSemis.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresSemis.get(siteId).get(echan).put(mesurePlantation.getDatedebut(), mesurePlantation);
            itMesure.remove();
        }

    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedRecolteCoupeVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresSemisPlantationsMap
     */
    @Override
    protected void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedRecolteCoupeVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureSemisPlantation>>> mesuresSemisPlantationsMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresSemisPlantationsMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureSemisPlantation>>> mesuresSemisPlantationsMap, IntervalDate selectedIntervalDate) {
        try {
            String currentDispositif;
            String currentTraitement;
            String curentplacette;
            String currentParcelle;
            String culture;
            String variete;
            String materiel1;
            String materiel2;
            String materiel3;
            String niveau;
            String typeobs;
            String observation;
            String commentaire;
            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    SemisPlantationBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    SemisPlantationBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_SEMIS).split(";"));
            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureSemisPlantation>> mesureSemissMapByDisp = mesuresSemisPlantationsMap
                        .get(dispositif.getId());
                if (mesureSemissMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesureSemisPlantation>>> itEchan = mesureSemissMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesureSemisPlantation>> echanEntry = itEchan
                            .next();
                    DescriptionTraitement echantillon = echanEntry.getKey();
                    SortedMap<LocalDate, MesureSemisPlantation> mesureSemisMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                    for (Entry<LocalDate, MesureSemisPlantation> entrySet : mesureSemisMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureSemisPlantation mesureSemisPlantation = entrySet.getValue();
                        currentDispositif = mesureSemisPlantation.getCodedispositif();
                        currentTraitement = mesureSemisPlantation.getDescriptionTraitement().getCode();
                        currentParcelle = mesureSemisPlantation.getNomparcelle();
                        curentplacette = mesureSemisPlantation.getNomparcelle();
                        culture = mesureSemisPlantation.getNomculture();
                        variete = mesureSemisPlantation.getVarietecepage();
                        materiel1 = mesureSemisPlantation.getMateriel1();
                        materiel2 = mesureSemisPlantation.getMateriel2();
                        materiel3 = mesureSemisPlantation.getMateriel3();
                        niveau = mesureSemisPlantation.getNiveauatteint();
                        typeobs = mesureSemisPlantation.getTypeobservation();
                        observation = mesureSemisPlantation.getNomobservation();
                        commentaire = mesureSemisPlantation.getCommentaire();
                        String genericPattern = LineDataFixe(date, currentDispositif, currentTraitement, currentParcelle, curentplacette,
                                culture, variete, materiel1, materiel2, materiel3, typeobs, observation, niveau, commentaire);
                        out.print(genericPattern);
                        LineDataVariable(mesureSemisPlantation, out, listVariable);
                    }
                    itEchan.remove();
                }
            }
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentTraitement, String currentParcelle,
            String courentPlacette, String culture, String variete,
            String materiel1,
            String materiel2,
            String materiel3,
            String typeobs, String observation, String niveau,
            String commentaire) {
        String genericPattern = String.format(PATTERB_CSV_23_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentTraitement,
                currentParcelle,
                courentPlacette,
                culture, variete,
                materiel1, materiel2, materiel3, typeobs, observation, niveau, commentaire);
        return genericPattern;
    }

    private void LineDataVariable(MesureSemisPlantation mesureSemisPlantation, PrintStream out, List<String> listVariable) {

        final List<ValeurSemisPlantation> valeurtravailsol = mesureSemisPlantation.getValeursemis();
        listVariable.forEach((variableColumnName) -> {
            for (Iterator<ValeurSemisPlantation> ValeurIterator = valeurtravailsol.iterator(); ValeurIterator.hasNext();) {
                ValeurSemisPlantation valeur = ValeurIterator.next();
                if (!((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
                    continue;
                }
                String line;
                line = String.format(";%s", getValeurToString(valeur));
                out.print(line);
                ValeurIterator.remove();
            }
        });
        out.println();
    }

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return SemisPlantationExtractor.MAP_INDEX_SEMIS_PLANTATION;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IITKDatatypeManager.CODE_DATATYPE_SEMIS_PLANTATION;
    }

    private String getValeurToString(ValeurSemisPlantation valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else if (valeur.getListeItineraire() != null) {
            return valeur.getListeItineraire().getListe_valeur();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(SemisPlantationBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
