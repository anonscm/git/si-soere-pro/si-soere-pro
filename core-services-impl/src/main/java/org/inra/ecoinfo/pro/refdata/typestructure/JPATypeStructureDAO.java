/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typestructure;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeStructureDAO extends AbstractJPADAO<TypeStructure> implements ITypeStructureDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typestructure.ITypeStructureDAO#getByLibelle (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<TypeStructure> getByNKey(String libelle) {
        CriteriaQuery<TypeStructure> query = builder.createQuery(TypeStructure.class);
        Root<TypeStructure> typeStructure = query.from(TypeStructure.class);
        query
                .select(typeStructure)
                .where(
                        builder.equal(typeStructure.get(TypeStructure_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.typestructure.ITypeStructureDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @Override
    public Optional<TypeStructure> getByCode(String code) {
        CriteriaQuery<TypeStructure> query = builder.createQuery(TypeStructure.class);
        Root<TypeStructure> typeStructure = query.from(TypeStructure.class);
        query
                .select(typeStructure)
                .where(
                        builder.equal(typeStructure.get(TypeStructure_.code), code)
                );
        return getOptional(query);
    }

}
