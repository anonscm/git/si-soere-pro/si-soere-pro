/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureIncubationSolProDAO<T> extends IDAO<MesureIncubationSolPro> {

    /**
     *
     * @param key
     * @return
     */
    Optional<MesureIncubationSolPro> getByKeys(String key);

}
