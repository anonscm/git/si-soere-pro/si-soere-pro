/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cepagevariete;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<CepageVariete> {

    ICepageVarieteDAO cepagevarieteDAO;
    private Properties cepageVarieteEn;

    /**
     *
     * @param cepagevariete
     * @throws BusinessException
     */
    public void createCepageVariete(CepageVariete cepagevariete) throws BusinessException {
        try {
            cepagevarieteDAO.saveOrUpdate(cepagevariete);
            cepagevarieteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create cepage");
        }
    }

    private void updateCepageVariete(final String libelle, final CepageVariete dbcepagevariete) throws BusinessException {
        try {
            dbcepagevariete.setCv_libelle(libelle);
            cepagevarieteDAO.saveOrUpdate(dbcepagevariete);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update cepage");
        }
    }

    private void createOrUpdateCepageVariete(final String code, final String libelle, final CepageVariete dbcepageveriete) throws BusinessException {
        if (dbcepageveriete == null) {
            final CepageVariete cepagevariete = new CepageVariete(libelle);
            cepagevariete.setCv_code(code);
            cepagevariete.setCv_libelle(libelle);
            createCepageVariete(cepagevariete);
        } else {
            updateCepageVariete(libelle, dbcepageveriete);
        }
    }

    private void persistTypeCulture(final String code, final String nom) throws BusinessException, BusinessException {
        final CepageVariete dbcepage = cepagevarieteDAO.getByNKey(nom).orElse(null);
        createOrUpdateCepageVariete(code, nom, dbcepage);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();
                cepagevarieteDAO.remove(cepagevarieteDAO.getByNKey(nom).orElseThrow(PersistenceException::new));
                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<CepageVariete> getAllElements() throws BusinessException {
        return cepagevarieteDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CepageVariete.NAME_ENTITY_JPA);
                final String libelle = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(libelle);
                if (!errorsReport.hasErrors()) {
                    persistTypeCulture(code, libelle);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CepageVariete cepage) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cepage == null || cepage.getCv_libelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cepage.getCv_libelle(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cepage == null || cepage.getCv_libelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cepageVarieteEn.getProperty(cepage.getCv_libelle()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<CepageVariete> initModelGridMetadata() {
        cepageVarieteEn = localizationManager.newProperties(CepageVariete.NAME_ENTITY_JPA,
                CepageVariete.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param cepagevarieteDAO
     */
    public void setCepagevarieteDAO(ICepageVarieteDAO cepagevarieteDAO) {
        this.cepagevarieteDAO = cepagevarieteDAO;
    }

}
