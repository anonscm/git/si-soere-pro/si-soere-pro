package org.inra.ecoinfo.pro.refdata.melange;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
public interface IMelangeDAO extends IDAO<Melange> {

    /**
     *
     * @return
     */
    List<Melange> getAll();

    /**
     *
     * @param produits
     * @param composant
     * @param pourcentage
     * @return
     */
    Optional<Melange> getByNKey(Produits produits, Composant composant, double pourcentage);

    /**
     *
     * @param codeProduits
     * @param codeComposant
     * @param pourcentage
     * @return
     */
    Optional<Melange> getByNKey(String codeProduits, String codeComposant, double pourcentage);

}
