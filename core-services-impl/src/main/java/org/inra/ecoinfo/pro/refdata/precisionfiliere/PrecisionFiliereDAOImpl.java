/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionfiliere;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class PrecisionFiliereDAOImpl extends AbstractJPADAO<Precisionfiliere> implements IPrecisionFiliereDAO {

    private static final String QUERY_PRECISIONFILIERE_NAME = "from Precisionfiliere pf where  pf.pfiliere_nom = :nom";

    /**
     *
     * @return
     */
    @Override
    public List<Precisionfiliere> getAll() {
        return getAll(Precisionfiliere.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Precisionfiliere> getByNKey(String nom) {
        CriteriaQuery<Precisionfiliere> query = builder.createQuery(Precisionfiliere.class);
        Root<Precisionfiliere> precisionfiliere = query.from(Precisionfiliere.class);
        query
                .select(precisionfiliere)
                .where(
                        builder.equal(precisionfiliere.get(Precisionfiliere_.pfiliere_code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }

}
