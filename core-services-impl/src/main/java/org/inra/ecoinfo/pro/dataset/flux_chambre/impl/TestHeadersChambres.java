
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.fluxchambres.IFluxChambresDatatypeManager;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class TestHeadersChambres extends GenericTestHeader {

    static final long serialVersionUID = 1L;

    /**
     *
     */
    public TestHeadersChambres() {
        super();
    }

    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return
     * @throws BusinessException
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile, final ISessionPropertiesPRO sessionProperties,
            final String encoding, final BadsFormatsReport badsFormatsReport, final DatasetDescriptorPRO datasetDescriptor)
            throws BusinessException {
        super.testHeaders(parser, versionFile, sessionProperties, encoding, badsFormatsReport, datasetDescriptor);
        final ISessionPropertiesFluxChambre sessionPropertiesFluxChambre = (ISessionPropertiesFluxChambre) sessionProperties;
        sessionPropertiesFluxChambre.initDate();
        long lineNumber = 0;

        try {
            lineNumber = this.readDispAndParcelle(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    IFluxChambresDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRES_PRO);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, sessionProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 1);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, sessionProperties);
            // lineNumber = this.jumpLines(parser, lineNumber, 3);
        } catch (final IOException e) {
            LOGGER.debug("can't read parser", e);
            badsFormatsReport.addException(e);
        }
        return lineNumber;
    }

}
