/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;

/**
 *
 * @author adiankha
 */
public class VariableValueMoy {
    
    VariablesPRO variable;
    
    String       value;

    
    Boolean      flag = false;

    
   float      ecarttype;

    /**
     *
     * @param variable
     * @param value
     */
    public VariableValueMoy(VariablesPRO variable, String value) {
        this.variable = variable;
        this.value = value;
    }

    /**
     *
     * @return
     */
    public VariablesPRO getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariablesPRO variable) {
        this.variable = variable;
    }

    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     *
     * @param value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public Boolean getFlag() {
        return flag;
    }

    /**
     *
     * @param flag
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

   
   
   
    
}
