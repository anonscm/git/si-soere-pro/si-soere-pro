/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeintervention;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author vjkoyao
 */
public interface ITypeInterventionDAO extends IDAO<TypeIntervention> {

    /**
     *
     * @param type_intervention_nom
     * @return
     */
    Optional<TypeIntervention> getByNKey(String type_intervention_nom);

    /**
     *
     * @return
     */
    List<TypeIntervention> getAll();
}
