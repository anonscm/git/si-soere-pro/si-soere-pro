package org.inra.ecoinfo.pro.refdata.codebloc;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ICodeBlocDAO extends IDAO<CodeBloc> {

    /**
     *
     * @return
     */
    List<CodeBloc> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<CodeBloc> getByNKey(String code);
}
