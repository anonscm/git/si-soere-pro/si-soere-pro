package org.inra.ecoinfo.pro.refdata.activiteindustrielle;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class ActiviteIndustrielleDaoImpl extends AbstractJPADAO<Activiteindustrielle> implements IActiviteIndustrielleDAO {

    private static final String QUERY_ACTIVITEINDUSTRIELLE_NAME = "from Activiteindustrielle t where t.nom =:nom";

    /**
     *
     * @return
     */
    @Override
    public List<Activiteindustrielle> getAll() {
        return getAllBy(Activiteindustrielle.class, Activiteindustrielle::getCode);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Activiteindustrielle> getByNKey(String nom) {
        CriteriaQuery<Activiteindustrielle> query = builder.createQuery(Activiteindustrielle.class);
        Root<Activiteindustrielle> ai = query.from(Activiteindustrielle.class);
        query
                .select(ai)
                .where(builder.equal(ai.get(Activiteindustrielle_.nom), nom));
        return getOptional(query);
    }

}
