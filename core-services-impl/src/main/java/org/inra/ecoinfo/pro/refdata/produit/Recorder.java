package org.inra.ecoinfo.pro.refdata.produit;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.FormePhysique.FormePhysiques;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.annees.Annees;
import org.inra.ecoinfo.pro.refdata.annees.IAnneesDAO;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.departement.Departement;
import org.inra.ecoinfo.pro.refdata.departement.IDepartementDAO;
import org.inra.ecoinfo.pro.refdata.formephysique.IFormePhysiqueDAO;
import org.inra.ecoinfo.pro.refdata.nomenclature.INomenclatureDAO;
import org.inra.ecoinfo.pro.refdata.nomenclature.Nomenclatures;
import org.inra.ecoinfo.pro.refdata.personneressource.IPersonneRessourceDAO;
import org.inra.ecoinfo.pro.refdata.statut.IStatutDAO;
import org.inra.ecoinfo.pro.refdata.statut.Statut;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<Produits> {

    private static final String PROPERTY_MSG_PRODUIT_BAD_ANNEE = "PROPERTY_MSG_PRODUIT_BAD_ANNEE";
    private static final String PROPERTY_MSG_PRODUIT_BAD_NOMEN = "PROPERTY_MSG_PRODUIT_BAD_NOMEN";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected IProduitDAO produitDAO;

    /**
     *
     */
    protected INomenclatureDAO nomenDAO;

    /**
     *
     */
    protected IStatutDAO statutDAO;

    /**
     *
     */
    protected IAnneesDAO anneeDAO;

    /**
     *
     */
    protected IFormePhysiqueDAO formeDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;

    IDepartementDAO departementDAO;

    /**
     *
     */
    protected int id = 0;

    /**
     *
     */
    protected IPersonneRessourceDAO personneRessourceDAO;

    private String[] listeStatutsPossibles;
    private String[] listeAnneesPossibles;
    private String[] listeFormesPossibles;
    private Map<String, String[]> listNomenclaturePossibles;
    private String[] listeTypeNomenPossibles;
    private String[] listeBooleansPossible;
    private String[] listeDepartementPossible;

    private Properties cmProduitEN;
    @SuppressWarnings("rawtypes")
    List code = new LinkedList();

    private void createOrUpdateProduit(String codeComposant, String codeuser, Nomenclatures nomenclatures, final String statut, final int annee, final String lieu, final String forme, final String nomcial, final String npk,
            final String etudie, final String comment, String epandu, String detenteur, final Produits dbproduits) throws BusinessException {
        if (dbproduits == null) {
            final Produits produits = new Produits(codeComposant, codeuser, nomenclatures, statut, annee,
                    lieu, forme, nomcial, npk, etudie, comment, epandu, detenteur);
            createProduit(produits);
        } else {
            updateBDProduit(nomenclatures, statut, forme, nomcial, npk, etudie, comment, epandu, dbproduits);

        }
    }

    private void createProduit(final Produits produits) throws BusinessException {
        try {
            produitDAO.saveOrUpdate(produits);
            produitDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create produits");
        }
    }

    /**
     *
     * @param produits
     * @param dbproduits
     */
    private void updateBDProduit(Nomenclatures nomenclatures, final String statut, final String forme, final String nomcial, final String npk,
            final String etudie, final String comment, String epandu, final Produits dbproduits) throws BusinessException {
        try {
            dbproduits.setNomenclatures(nomenclatures);
            dbproduits.setProd_statut(statut);
            dbproduits.setProd_formephysique(forme);
            dbproduits.setProd_nomcial(nomcial);
            dbproduits.setProd_teneurnpk(npk);
            dbproduits.setPro_etudie(etudie);
            dbproduits.setProd_comment(comment);
            dbproduits.setProd_epandu(epandu);

            produitDAO.saveOrUpdate(dbproduits);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update produits");
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nom = tokenizerValues.nextToken();
                Produits produit = produitDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get produit"));
                produitDAO.remove(produit);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Produits> getAllElements() throws BusinessException {
        return produitDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IAnneesDAO getAnneeDAO() {
        return anneeDAO;
    }

    /**
     *
     * @return
     */
    public IFormePhysiqueDAO getFormeDAO() {
        return formeDAO;
    }

    private void iniTypeNomenclaturePossibles() {
        SortedMap<String, String[]> nomenclatures = new TreeMap();
        SortedMap<String, SortedSet<String>> nomentypenomenList = new TreeMap<>();
        List<Nomenclatures> groupetnomenclature = nomenDAO.getAllBy(Nomenclatures.class, Nomenclatures::getNomen_nom);
        groupetnomenclature.forEach((nomen) -> {
            String tnnom = nomen.getTypenomenclature().getTn_nom();
            String nomenNom = nomen.getNomen_nom();
            if (!nomentypenomenList.containsKey(tnnom)) {
                nomentypenomenList.put(tnnom, new TreeSet());
            }
            nomentypenomenList.get(tnnom).add(nomenNom);
        });
        nomentypenomenList.entrySet().forEach((entryProduit) -> {
            nomenclatures.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listNomenclaturePossibles = nomenclatures;
        listeTypeNomenPossibles = nomenclatures.keySet().toArray(new String[]{});
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Produits produits) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getCodecomposant()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getCodecomposant(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        String valeurTNnom = produits == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : produits.getNomenclatures().getTypenomenclature().getTn_nom() != null
                ? produits.getNomenclatures().getTypenomenclature().getTn_nom() : "";
        ColumnModelGridMetadata columnTNnom = new ColumnModelGridMetadata(valeurTNnom,
                listeTypeNomenPossibles, null, true, false, true);
        String valeurNom = produits == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : produits.getNomenclatures().getNomen_nom() != null
                ? produits.getNomenclatures().getNomen_nom() : "";
        ColumnModelGridMetadata columnNomen = new ColumnModelGridMetadata(valeurNom, listNomenclaturePossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnNomen);
        columnNomen.setValue(valeurNom);
        columnTNnom.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTNnom);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnNomen);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getStc_Detenteur() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getStc_Detenteur(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_annee() == 0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_annee() != 0 ? produits.getProd_annee()
                        : "", listeAnneesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_lieu() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_lieu() != null ? produits.getProd_lieu()
                        : "", listeDepartementPossible, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_epandu() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_epandu() != null
                        ? produits.getProd_epandu() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getPro_etudie() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getPro_etudie() != null
                        ? produits.getPro_etudie() : "",
                        listeBooleansPossible, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_statut() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_statut() != null
                        ? produits.getProd_statut() : "", listeStatutsPossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_formephysique() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_formephysique() != null
                        ? produits.getProd_formephysique() : "",
                        listeFormesPossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_nomcial() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_nomcial(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_teneurnpk() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_teneurnpk(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : produits.getProd_comment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produits == null || produits.getProd_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cmProduitEN.getProperty(produits.getProd_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public INomenclatureDAO getNomenDAO() {
        return nomenDAO;
    }

    /**
     *
     * @return
     */
    public IPersonneRessourceDAO getPersonneRessourceDAO() {
        return personneRessourceDAO;
    }

    /**
     *
     * @return
     */
    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    /**
     *
     * @return
     */
    public IStatutDAO getStatutDAO() {
        return statutDAO;
    }

    @Override
    protected ModelGridMetadata<Produits> initModelGridMetadata() {
        cmProduitEN = localizationManager.newProperties(Produits.NAME_ENTITY_JPA, Produits.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        listeGroupesStatutPossibles();
        listeGroupesAnneesPossibles();
        listeGroupesFormesPossibles();
        iniTypeNomenclaturePossibles();
        listeBooleanPossibles();
        listeDepartPossibles();
        return super.initModelGridMetadata();
    }

    private void listeDepartPossibles() {
        List<Departement> groupeprem = departementDAO.getAllBy(Departement.class, Departement::getNoDepartement);
        String[] listedepartementPossibles = new String[groupeprem.size() + 1];
        listedepartementPossibles[0] = "";
        int index = 1;
        for (Departement deparement : groupeprem) {
            listedepartementPossibles[index++] = deparement.getNoDepartement();
        }
        this.listeDepartementPossible = listedepartementPossibles;
    }

    private void listeGroupesAnneesPossibles() {
        List<Annees> groupesAnnees = anneeDAO.getAll();
        String[] listedesAnneesPossibles = new String[groupesAnnees.size() + 1];
        listedesAnneesPossibles[0] = "";
        int index = 1;
        for (Annees annees : groupesAnnees) {
            listedesAnneesPossibles[index++] = annees.getAnnee_valeur();
        }
        this.listeAnneesPossibles = listedesAnneesPossibles;
    }

    private void listeGroupesFormesPossibles() {
        List<FormePhysiques> groupesFormes = formeDAO.getAll(FormePhysiques.class);
        String[] listedesFormesPossibles = new String[groupesFormes.size() + 1];
        listedesFormesPossibles[0] = "";
        int index = 1;
        for (FormePhysiques formes : groupesFormes) {
            listedesFormesPossibles[index++] = formes.getFp_nom();
        }
        this.listeFormesPossibles = listedesFormesPossibles;
    }

    private void listeGroupesStatutPossibles() {
        List<Statut> groupestatut = statutDAO.getAll(Statut.class);
        String[] listedesstatutsPossibles = new String[groupestatut.size() + 1];
        listedesstatutsPossibles[0] = "";
        int index = 1;
        for (Statut statut : groupestatut) {
            listedesstatutsPossibles[index++] = statut.getStatut_nom();
        }
        this.listeStatutsPossibles = listedesstatutsPossibles;
    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    private void persistProduit(String codeComposant, String codeuser, Nomenclatures nomenclatures, final String statut, final int annee,
            final String lieu, final String forme, final String nomcial, final String npk,
            final String etudie, final String comment, String epandu,
            String detenteur) throws BusinessException, BusinessException {
        final Produits dbproduits = produitDAO.getByNKey(codeComposant).orElse(null);

        createOrUpdateProduit(codeComposant, codeuser, nomenclatures, statut, annee, lieu, forme, nomcial,
                npk, etudie, comment, epandu, detenteur, dbproduits);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Produits.NAME_ENTITY_JPA);
                final String codeuser = tokenizerValues.nextToken();
                int indexnomen = tokenizerValues.currentTokenIndex();
                final String tnnom = tokenizerValues.nextToken();
                final String nomen = tokenizerValues.nextToken();
                final String detenteur = tokenizerValues.nextToken();

                final String annee = tokenizerValues.nextToken();
                int indexannee = tokenizerValues.currentTokenIndex();
                final String noDepartement = tokenizerValues.nextToken();
                final String epandu = tokenizerValues.nextToken();
                final String etudie = tokenizerValues.nextToken();
                int an = validiteAnnee(errorsReport, line, indexannee, annee);
                final String statut = tokenizerValues.nextToken();
                final String forme = tokenizerValues.nextToken();
                final String nomcial = tokenizerValues.nextToken();
                final String teneurNPK = tokenizerValues.nextToken();

                final String comment = tokenizerValues.nextToken();

                Nomenclatures dbnomen = verifieNomenclature(nomen, tnnom, errorsReport, line, indexnomen);
                Produits dbpro = produitDAO.getByNKey(codeuser, noDepartement, an, detenteur).orElse(null);
                String codeProduit = Produits.createCodeProduits(codeuser, detenteur, an, noDepartement);
                String codeComposant = Produits.createCodeComposant(codeuser, detenteur, an, noDepartement);

                createProduit(errorsReport, codeComposant, codeProduit, dbnomen, statut, an, noDepartement, forme, nomcial, 
                        teneurNPK, etudie, comment, epandu, detenteur);

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    private void createProduit(final ErrorsReport errorsReport, String codeComposant, final String codeuser, Nomenclatures dbnomen,
            final String statut, int annee, String lieu, final String forme, final String nomcial, final String npk, String etudie,
            final String comment, String epandu, final String detenteur)
            throws BusinessException, PersistenceException {
        if (!errorsReport.hasErrors()) {
            persistProduit(codeComposant, codeuser, dbnomen, statut, annee, lieu, forme, nomcial, npk,
                    etudie, comment, epandu, detenteur);
        }
    }

    private Nomenclatures verifieNomenclature(final String nomen, final String tnnom, final ErrorsReport errorsReport, long line, int indexnomen) throws BusinessException {
        Nomenclatures dbnomen = nomenDAO.getByNKey(nomen, tnnom).orElse(null);
        if (nomen == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRODUIT_BAD_NOMEN), line, indexnomen, tnnom, nomen));
        }
        return dbnomen;
    }

    private int validiteAnnee(final ErrorsReport errorsReport, long line, int indexannee, final String annee) {
        int an = 0;
        an = Integer.parseInt(annee);
        String stringyear = DateUtil.getUTCDateTextFromLocalDateTime(LocalDate.now(), DateUtil.YYYY);
        long year = 0;
        year = Integer.parseInt(stringyear);
        if (an > year) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, PROPERTY_MSG_PRODUIT_BAD_ANNEE), line, indexannee, an));
        }
        return an;
    }

    /**
     *
     * @param anneeDAO
     */
    public void setAnneeDAO(IAnneesDAO anneeDAO) {
        this.anneeDAO = anneeDAO;
    }

    /**
     *
     * @return
     */
    public Properties getCmProduitEN() {
        return cmProduitEN;
    }

    /**
     *
     * @param cmProduitEN
     */
    public void setCmProduitEN(Properties cmProduitEN) {
        this.cmProduitEN = cmProduitEN;
    }

    /**
     *
     * @param formeDAO
     */
    public void setFormeDAO(IFormePhysiqueDAO formeDAO) {
        this.formeDAO = formeDAO;
    }

    /**
     *
     * @param nomenDAO
     */
    public void setNomenDAO(INomenclatureDAO nomenDAO) {
        this.nomenDAO = nomenDAO;
    }

    /**
     *
     * @param personneRessourceDAO
     */
    public void setPersonneRessourceDAO(IPersonneRessourceDAO personneRessourceDAO) {
        this.personneRessourceDAO = personneRessourceDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param statutDAO
     */
    public void setStatutDAO(IStatutDAO statutDAO) {
        this.statutDAO = statutDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param departementDAO
     */
    public void setDepartementDAO(IDepartementDAO departementDAO) {
        this.departementDAO = departementDAO;
    }

}
