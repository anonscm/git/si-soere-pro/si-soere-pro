package org.inra.ecoinfo.pro.refdata.rolepersonneressource;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IRolePersonneRessourceDAO extends IDAO<RolePersonneRessource> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<RolePersonneRessource> getByNKey(String libelle);

}
