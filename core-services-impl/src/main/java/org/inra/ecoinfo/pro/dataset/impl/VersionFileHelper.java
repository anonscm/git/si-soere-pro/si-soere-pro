/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.dataset.DefaultVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author adiankha
 */
public class VersionFileHelper extends DefaultVersionFileHelper {

    /**
     *
     * @param versionFile
     * @return
     */
    public static final Dispositif getDispositifFromVersion(VersionFile versionFile) {
        return (Dispositif) versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(Dispositif.class).getNodeable();
    }

    /**
     *
     * @param versionFile
     * @return
     */
    public static final DataType getDatatypeFromVersion(VersionFile versionFile) {
        return (DataType) versionFile.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getNodeable();
    }

    private List<String> scheduleDatatypes = new LinkedList<>();

    /**
     *
     * @param scheduleDatatypes
     */
    public void setScheduleDatatypes(List<String> scheduleDatatypes) {
        this.scheduleDatatypes = scheduleDatatypes;
    }

    @Override
    public String buildDownloadFilename(VersionFile versionFile) {

        final StringBuffer nomFichier = new StringBuffer();
        try {
            //final DispositifThemeDataType dispositifThemeDataType = (DispositifThemeDataType) versionFile
            Dispositif dispositif = getDispositifFromVersion(versionFile);
            DataType dataType = getDatatypeFromVersion(versionFile);
            nomFichier
                    .append(dispositif.getCodeDispo()
                            .replaceAll(
                                    IVersionFileHelper.FILE_SEPARATOR,
                                    PatternConfigurator.ANCESTOR_SEPARATOR))
                    .append(DefaultVersionFileHelper.UNDERSCORE);
            nomFichier
                    .append(dataType.getCode())
                    .append(DefaultVersionFileHelper.UNDERSCORE);
//            if (dispositifThemeDataType.getDispositiftheme()!= null) {
//                nomFichier.append(
//                        ((DispositifThemeDataType) versionFile.getDataset()
//                                .getLeafNode()).getDispositiftheme().getDispositif().getCode()).append(
//                                        DefaultVersionFileHelper.UNDERSCORE);
//            }
            final String format;
            if (scheduleDatatypes.contains(dataType.getCode())) {
                format = RecorderPRO.DD_MM_YYYY_HHMMSS_FILE;
            } else {
                format = DateUtil.DD_MM_YYYY_FILE;
            }
            nomFichier.append(DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateDebutPeriode(), format)).append(
                    DefaultVersionFileHelper.UNDERSCORE);
            nomFichier.append(DateUtil.getUTCDateTextFromLocalDateTime(versionFile.getDataset().getDateFinPeriode(), format)).append(
                    IVersionFileHelper.EXTENSION_FILE_CSV);
        } catch (final Exception e) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        return nomFichier.toString();
    }


}
