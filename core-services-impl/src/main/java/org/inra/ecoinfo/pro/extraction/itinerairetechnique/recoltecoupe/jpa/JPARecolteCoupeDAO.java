/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.recoltecoupe.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe_;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.jpa.JPAITKDAO;
import org.inra.ecoinfo.pro.synthesis.recoltecoupe.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPARecolteCoupeDAO extends JPAITKDAO<MesureRecolteCoupe, ValeurRecolteCoupe> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurRecolteCoupe> getValeurITKClass() {
        return ValeurRecolteCoupe.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureRecolteCoupe> getMesureITKClass() {
        return MesureRecolteCoupe.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurRecolteCoupe, MesureRecolteCoupe> getMesureAttribute() {
        return ValeurRecolteCoupe_.mesureinterventionrecolte;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }
}
