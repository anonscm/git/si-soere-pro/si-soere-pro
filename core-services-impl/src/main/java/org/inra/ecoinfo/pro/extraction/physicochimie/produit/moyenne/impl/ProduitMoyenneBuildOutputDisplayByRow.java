/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.moyenne.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePROMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieOutputDisplayByRow;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class ProduitMoyenneBuildOutputDisplayByRow extends PhysicoChimieOutputDisplayByRow {

    static final String PATTERN_WORD = "%s";
    static final String PATTERB_CSV_12_FIELD = "%s;%s;%s;%s;%s;%s;%%s;%%s;%%s;%%s;%%s;%%s";
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA_PRODUITMOYENNE = "PROPERTY_HEADER_RAW_DATA_PRODUITMOYENNE";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.produit.messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedPlanteMoyenneVariables = getVariablesSelected(requestMetadatasMap, IPhysicoChimiePROMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO_MOY);
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<MesurePhysicoChimiePROMoy> mesuresProduitMoyenne = getMesures(resultsDatasMap, ProduitMoyenneExtractor.MAP_INDEX_PRODUITMOYENNE);
        final Set<String> dispositifsNames = buildListOfDispositifsForDatatype(selectedDispositifs, getDatatype());
        final Map<String, File> filesMap = this.buildOutputsFiles(dispositifsNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>>> mesuresProduitMoyennesMap = new TreeMap();

        try {
            this.buildmap(mesuresProduitMoyenne, mesuresProduitMoyennesMap);
        } catch (DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedDispositifs, selectedPlanteMoyenneVariables, selectedIntervalDate,
                outputPrintStreamMap, mesuresProduitMoyennesMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void buildmap(
            final List<MesurePhysicoChimiePROMoy> mesuresProduitMoyennes,
            final SortedMap<Long, SortedMap<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>>> mesuresProduitMoyenneMap) {
        java.util.Iterator<MesurePhysicoChimiePROMoy> itMesure = mesuresProduitMoyennes
                .iterator();
        while (itMesure.hasNext()) {
            MesurePhysicoChimiePROMoy mesureProduitMoyenne = itMesure
                    .next();
            ProduitDispositif prodisp = mesureProduitMoyenne.getDisppro();
            Long siteId = prodisp.getDispositif().getId();
            if (!mesuresProduitMoyenneMap.containsKey(siteId)) {
                mesuresProduitMoyenneMap.put(siteId,
                        new TreeMap<>());
            }
            if (!mesuresProduitMoyenneMap.get(siteId).containsKey(prodisp)) {
                mesuresProduitMoyenneMap.get(siteId).put(prodisp,
                        new TreeMap<>());
            }
            mesuresProduitMoyenneMap.get(siteId).get(prodisp).put(mesureProduitMoyenne.getDatePrelevement(), mesureProduitMoyenne);
            itMesure.remove();
        }
    }

    private void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitMoyenneVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>>> mesuresPlanteMoyennesMap) {
        try {
            BuildDataLine(selectedDispositifs, selectedProduitMoyenneVariables, outputPrintStreamMap, mesuresPlanteMoyennesMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void BuildDataLine(final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitMoyenneVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>>> mesuresPlanteMoyennesMap,
            final IntervalDate selectedIntervalDate) {
        PrintStream out;
        String currentDispositif;
        String currentLieu;
        String currentPro;
        int currentRep;
        String currentlabo;
        for (final Dispositif dispositif : selectedDispositifs) {
            out = outputPrintStreamMap.get(getDispositifDatatype(dispositif, getDatatype()));
            final SortedMap<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>> mesurePlanteMoyennesMapByDisp = mesuresPlanteMoyennesMap
                    .get(dispositif.getId());
            if (mesurePlanteMoyennesMapByDisp == null) {
                continue;
            }
            Iterator<Entry<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>>> itEchan = mesurePlanteMoyennesMapByDisp
                    .entrySet().iterator();
            while (itEchan.hasNext()) {
                java.util.Map.Entry<ProduitDispositif, SortedMap<LocalDate, MesurePhysicoChimiePROMoy>> echanEntry = itEchan
                        .next();
                ProduitDispositif prodisp = echanEntry.getKey();
                SortedMap<LocalDate, MesurePhysicoChimiePROMoy> mesurePlanteMoyennesMap = echanEntry.getValue()
                        .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                for (Entry<LocalDate, MesurePhysicoChimiePROMoy> entrySet : mesurePlanteMoyennesMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesurePhysicoChimiePROMoy mesurePlanteMoyenne = entrySet.getValue();
                    currentPro = prodisp.getProduits().getProd_key();
                    currentDispositif = prodisp.getDispositif().getName();
                    currentLieu = prodisp.getDispositif().getLieu().getNom() != null ? prodisp.getDispositif().getLieu().getNom()
                            : org.apache.commons.lang.StringUtils.EMPTY;
                    currentRep = mesurePlanteMoyenne.getNbre_repetition();
                    currentlabo = String.format(
                            ProduitMoyenneBuildOutputDisplayByRow.PATTERN_WORD,
                            mesurePlanteMoyenne.getNom_laboratoire());

                    String genericPattern = LineDataFixe(date, currentPro, currentDispositif, currentLieu, currentRep, currentlabo);
                    LineDataVariable(mesurePlanteMoyenne, genericPattern, out, selectedProduitMoyenneVariables);
                }
                itEchan.remove();
            }
        }
    }

    private void LineDataVariable(MesurePhysicoChimiePROMoy mesureProduitMoyenne, String genericPattern, PrintStream out, final List<VariablesPRO> selectedProduitMoyenneVariables) {
        selectedProduitMoyenneVariables.forEach((variablesPRO) -> {
            for (Iterator<ValeurPhysicoChimiePROMoy> ValeurIterator = mesureProduitMoyenne.getValeurPhysicoChimiePROMoy().iterator(); ValeurIterator.hasNext();) {
                ValeurPhysicoChimiePROMoy valeur = ValeurIterator.next();
                if (((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getId().equals(variablesPRO.getId())) {
                    String line = String.format(genericPattern,
                            valeur.getValeur(),
                            valeur.getEcarttype(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());
                    out.println(line);
                    ValeurIterator.remove();
                }
            }
        });
    }

    private String LineDataFixe(LocalDate date, String currentPro, String currentDispositif, String currentLieu,
            int currentRep,
            String currentlabo) {
        String genericPattern = String.format(PATTERB_CSV_12_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentPro, currentDispositif,
                currentLieu,
                currentRep,
                currentlabo);
        return genericPattern;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                ProduitMoyenneBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        ProduitMoyenneBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        ProduitMoyenneBuildOutputDisplayByRow.HEADER_RAW_DATA_PRODUITMOYENNE));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (CollectionUtils.isEmpty(((DefaultParameter) parameters).getResults()
                .get(ProduitMoyenneExtractor.CST_RESULT_EXTRACTION_PRODUITMOY_CODE)
                .getOrDefault(ProduitMoyenneExtractor.MAP_INDEX_PRODUITMOYENNE,
                        ((DefaultParameter) parameters).getResults()
                .get(ProduitMoyenneExtractor.CST_RESULT_EXTRACTION_PRODUITMOY_CODE)
                .get(ProduitMoyenneExtractor.MAP_INDEX_0)))) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ProduitMoyenneExtractor.CST_RESULT_EXTRACTION_PRODUITMOY_CODE));
        return null;
    }

    private String getDatatype() {
        return "pro_physico-chimie_moyennes";
    }
}
