package org.inra.ecoinfo.pro.refdata.typepedologique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique_;

/**
 *
 * @author ptcherniati
 */
public class TypePedologiqueDaoImpl extends AbstractJPADAO<Typepedologique> implements ITypePedologiqueDAO {

    private static final String QUERY_TYPEPEDOLOGIQUE_NAME = "from Typepedologique t where t.nom =:nom";

    /**
     *
     * @return
     */
    @Override
    public List<Typepedologique> getAll() {
        return getAll(Typepedologique.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Typepedologique> getByNKey(String nom) {
        CriteriaQuery<Typepedologique> query = builder.createQuery(Typepedologique.class);
        Root<Typepedologique> typepedologique = query.from(Typepedologique.class);
        query
                .select(typepedologique)
                .where(
                        builder.equal(typepedologique.get(Typepedologique_.nom), nom)
                );
        return getOptional(query);
    }

}
