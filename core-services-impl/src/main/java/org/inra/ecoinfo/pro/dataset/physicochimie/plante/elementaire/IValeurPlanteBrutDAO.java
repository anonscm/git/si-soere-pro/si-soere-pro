/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;

/**
 *
 * @author adiankha
 */
public interface IValeurPlanteBrutDAO extends IDAO<ValeurPlanteElementaire> {

    /**
     *
     * @param realNode
     * @param mesurePlanteBrut
     * @param valeur
     * @param statut
     * @return
     */
    Optional<ValeurPlanteElementaire> getByNKeys(RealNode realNode, MesurePlanteElementaire mesurePlanteBrut, Float valeur, String statut);
}
