/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.component;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IComposantDAO extends IDAO<Composant> {
    List<? extends Composant> getAll();
    /**
     *
     * @param composant_key
     * @param idProduit
     * @return
     */
    Optional<?extends Composant> getByNKey(String composant_key, boolean idProduit);
    Optional<?extends Composant> getByCodeComposant(String composant_key);
    
}
