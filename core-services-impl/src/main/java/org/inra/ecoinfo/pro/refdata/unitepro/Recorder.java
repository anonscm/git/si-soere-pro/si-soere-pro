/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.unitepro;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Unitepro> {
    
    IUniteproDAO uniteproDAO;
    
    Properties codeEn;
    Properties nomEn;
    
    private void createUnipro(Unitepro unitepro) throws BusinessException {
        try {
            uniteproDAO.saveOrUpdate(unitepro);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prelevement");
        }
        
    }
    
    private void updateUnitepro(String code, String mycode, String nom, Unitepro dbUnitepro) throws BusinessException {
        try {
            dbUnitepro.setCode(code);
            dbUnitepro.setMycode(mycode);
            dbUnitepro.setName(nom);
            uniteproDAO.saveOrUpdate(dbUnitepro);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prelevement");
        }
        
    }
    
    private void createOrUpdate(String code, String mycode, String nom) throws BusinessException {
        Unitepro dbUnitePro = uniteproDAO.getByNKey(code).orElse(null);
        if (dbUnitePro == null) {
            Unitepro unitepro = new Unitepro(code, nom);
            unitepro.setCode(code);
            unitepro.setMycode(mycode);
            unitepro.setName(nom);
            createUnipro(unitepro);
        } else {
            updateUnitepro(code, mycode, nom, dbUnitePro);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Unitepro.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                Unitepro dbtexturesol = uniteproDAO.getByNKey(code)
                        .orElseThrow(() -> new BusinessException("can't get unit pro"));
                uniteproDAO.remove(dbtexturesol);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Unitepro.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(code);
                createOrUpdate(code, mycode, nom);
                values = csvp.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<Unitepro> getAllElements() throws BusinessException {
        return uniteproDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Unitepro unitepro) {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : unitepro.getCode(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unitepro == null || unitepro.getName()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : codeEn.getProperty(unitepro.getCode()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : unitepro.getName(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unitepro == null || unitepro.getName()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomEn.getProperty(unitepro.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @param uniteproDAO
     */
    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }
    
    @Override
    protected ModelGridMetadata<Unitepro> initModelGridMetadata() {
        codeEn = localizationManager.newProperties(Unitepro.NAME_ENTITY_JPA, Unitepro.JPA_COLUMN_CODE, Locale.ENGLISH);
        nomEn = localizationManager.newProperties(Unitepro.NAME_ENTITY_JPA, Unitepro.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
    
}
