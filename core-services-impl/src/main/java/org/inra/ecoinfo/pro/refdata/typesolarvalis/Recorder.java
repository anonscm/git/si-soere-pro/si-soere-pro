package org.inra.ecoinfo.pro.refdata.typesolarvalis;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Typesolarvalis> {

    ITypeSolArvalisDAO tsarvalisDAO;
    private Properties tsarvalisEN;

    private void createTypesolarvalis(final Typesolarvalis typesolarvalis) throws BusinessException {
        try {
            tsarvalisDAO.saveOrUpdate(typesolarvalis);
            tsarvalisDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create typesolarvalis");
        }
    }

    private void updateTypesolarvalis(final String nom, final Typesolarvalis dbtypesolarvalis) throws BusinessException {
        try {
            dbtypesolarvalis.setNom(nom);
            tsarvalisDAO.saveOrUpdate(dbtypesolarvalis);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update typesolarvalis");
        }
    }

    private void createOrUpdateTypesolarvalis(final String code, String nom, final Typesolarvalis dbtypesolarvalis) throws BusinessException {
        if (dbtypesolarvalis == null) {
            final Typesolarvalis typesol = new Typesolarvalis(code, nom);
            typesol.setCode(code);
            typesol.setNom(nom);
            createTypesolarvalis(typesol);
        } else {
            updateTypesolarvalis(nom, dbtypesolarvalis);
        }
    }

    private void persistTypesolarvalis(final String code, final String nom) throws BusinessException, BusinessException {
        final Typesolarvalis dbtypesol = tsarvalisDAO.getByNKey(nom).orElse(null);
        createOrUpdateTypesolarvalis(code, nom, dbtypesol);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Typesolarvalis typesolarvalis) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typesolarvalis == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : typesolarvalis.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typesolarvalis == null || typesolarvalis.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : tsarvalisEN.getProperty(typesolarvalis.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typesolarvalis.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final Typesolarvalis dbfr = tsarvalisDAO.getByNKey(nom).orElseThrow(() -> new BusinessException("can 't get type arvalis"));
                tsarvalisDAO.remove(dbfr);

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Typesolarvalis> getAllElements() throws BusinessException {
        return tsarvalisDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typesolarvalis.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistTypesolarvalis(code, nom);
                values = parser.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected ModelGridMetadata<Typesolarvalis> initModelGridMetadata() {
        tsarvalisEN = localizationManager.newProperties(Typesolarvalis.NAME_ENTITY_JPA, Typesolarvalis.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public ITypeSolArvalisDAO getTsarvalisDAO() {
        return tsarvalisDAO;
    }

    /**
     *
     * @param tsarvalisDAO
     */
    public void setTsarvalisDAO(ITypeSolArvalisDAO tsarvalisDAO) {
        this.tsarvalisDAO = tsarvalisDAO;
    }

    /**
     *
     * @return
     */
    public Properties getTsarvalisEN() {
        return tsarvalisEN;
    }

    /**
     *
     * @param tsarvalisEN
     */
    public void setTsarvalisEN(Properties tsarvalisEN) {
        this.tsarvalisEN = tsarvalisEN;
    }

}
