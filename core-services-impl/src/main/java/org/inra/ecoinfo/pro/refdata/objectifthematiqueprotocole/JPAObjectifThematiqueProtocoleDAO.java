package org.inra.ecoinfo.pro.refdata.objectifthematiqueprotocole;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.thematiqueetudiee.ThematiqueEtudiee;

/**
 *
 * @author ptcherniati
 */
public class JPAObjectifThematiqueProtocoleDAO extends AbstractJPADAO<ObjectifThematiqueProtocole> implements IObjectifThematiqueProtocoleDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.objectifthematiqueprotocole.IObjectifThematiqueProtocoleDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<ObjectifThematiqueProtocole> getAll() {
        return getAll(ObjectifThematiqueProtocole.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.objectifthematiqueprotocole. IObjectifThematiqueProtocoleDAO #getByProtocoleThematiqueObjectif(org.inra. ecoinfo.pro.refdata.protocole.Protocole,
     * org.inra.ecoinfo.pro.refdata.thematiqueetudiee.ThematiqueEtudiee, java.lang.String)
     */

    /**
     *
     * @param protocole
     * @param thematiqueEtudiee
     * @param objectif
     * @return
     */

    @Override
    public Optional<ObjectifThematiqueProtocole> getByNKey(Protocole protocole, ThematiqueEtudiee thematiqueEtudiee, String objectif) {
        CriteriaQuery<ObjectifThematiqueProtocole> query = builder.createQuery(ObjectifThematiqueProtocole.class);
        Root<ObjectifThematiqueProtocole> objectifThematiqueProtocole = query.from(ObjectifThematiqueProtocole.class);
        Join<ObjectifThematiqueProtocole, Protocole> prot = objectifThematiqueProtocole.join(ObjectifThematiqueProtocole_.protocole);
        Join<ObjectifThematiqueProtocole, ThematiqueEtudiee> them = objectifThematiqueProtocole.join(ObjectifThematiqueProtocole_.thematiqueEtudiee);
        query
                .select(objectifThematiqueProtocole)
                .where(
                        builder.equal(prot, protocole),
                        builder.equal(them, thematiqueEtudiee),
                        builder.equal(objectifThematiqueProtocole.get(ObjectifThematiqueProtocole_.objectif), objectif)
                );
        return getOptional(query);
    }

}
