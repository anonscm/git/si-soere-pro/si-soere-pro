/**
 *
 */
package org.inra.ecoinfo.pro.refdata.commune;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.departement.Departement;
import org.inra.ecoinfo.pro.refdata.departement.IDepartementDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Commune> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IDepartementDAO departementDAO;
    ICommuneDAO communeDAO;
    private String[] departementPossibles;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexDepartement = tokenizerValues.currentTokenIndex();
                String nomDepartement = tokenizerValues.nextToken();
                int indexCodePostal = tokenizerValues.currentTokenIndex();
                String codePostal = tokenizerValues.nextToken();
                String nomCommune = tokenizerValues.nextToken();

                Departement departement = verifieDepartement(nomDepartement, line + 1, indexDepartement + 1, errorsReport);
                String nomPays = departement == null ? "" : departement.getRegion().getPays().getNom();

                //code postal des départements "01" à "09" si écrit "xxxx" ou lieu de "0xxxx" dan le fichier
                if (nomPays.equals(Constantes.FRANCE) && codePostal != null && codePostal.length() == 4) {
                    codePostal = "0" + codePostal;
                }

                verifieCodePostalF(nomPays, codePostal, line + 1, indexCodePostal + 1, errorsReport);

                if (departement != null) {
                    // vérification de cohérence entre le numéro de département et le code postal saisi dans le cas de la France
                    // pas de vérification pour les autres pays
                    if (nomPays.equals(Constantes.FRANCE) && departement.getNoDepartement() != null) {
                        verifieCoherenceNoDepCodePost(departement.getNoDepartement(), codePostal, line + 1, indexCodePostal + 1, errorsReport);
                    }
                }

                Commune dbCommune = communeDAO.getByNKey(nomCommune, codePostal).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbCommune == null) {
                        Commune commune = new Commune(codePostal, nomCommune, departement);
                        communeDAO.saveOrUpdate(commune);
                    } else {
                        dbCommune.setCodePostal(codePostal);
                        dbCommune.setNomCommune(nomCommune);
                        dbCommune.setDepartement(departement);
                        communeDAO.saveOrUpdate(dbCommune);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException | PersistenceException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param nomDepartement
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Departement verifieDepartement(String nomDepartement, long line, int index, ErrorsReport errorsReport) {
        Departement departement = departementDAO.getByNom(nomDepartement).orElse(null);
        if (departement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DEPARTEMENT_NONDEFINI"), line, index, nomDepartement));
        }

        return departement;
    }

    /**
     * @param nomPays
     * @param codePostal
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieCodePostalF(String nomPays, String codePostal, long line, int index, ErrorsReport errorsReport) {
        try {
            Integer.parseInt(codePostal);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPOST_INCORRECT"), line, index));
        }

        // vérifie la longueur du code postal (en France uniquement)
        if (nomPays.equals(Constantes.FRANCE) && codePostal != null && codePostal.length() != 5) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LONGUEUR_CODEPOSTAL"), line, index, codePostal));
        }
    }

    /**
     * @param noDepartement
     * @param codePostal
     * @param line
     * @param index
     * @param errorsReport
     * @throws BusinessException
     */
    private void verifieCoherenceNoDepCodePost(String noDepartement, String codePostal, long line, int index, ErrorsReport errorsReport) throws BusinessException {
        if (!noDepartement.equals("2A") && !noDepartement.equals("2B")) {
            // cas des départements de la métropole (hors Corse)
            if (new Integer(noDepartement) <= 95 && !codePostal.substring(0, 2).equals(noDepartement)) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "COHERENCE_NODEP_CODEPOST"), line, index, codePostal, noDepartement));
            } else if (noDepartement != null && new Integer(noDepartement) > 95 && !codePostal.substring(0, 3).equals(noDepartement)) {
                // cas des départements d'outre-mer
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "COHERENCE_NODEP_CODEPOST"), line, index, codePostal, noDepartement));
            }
        } else { // cas de la corse
            if (!codePostal.substring(0, 2).equals("20")) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "COHERENCE_NODEP_CODEPOST"), line, index, codePostal, noDepartement));
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                tokenizerValues.nextToken(); //nom departement
                String codePostal = tokenizerValues.nextToken();
                String nomCommune = tokenizerValues.nextToken();

                Commune commune = communeDAO.getByNKey(nomCommune, codePostal).orElseThrow(PersistenceException::new);

                communeDAO.remove(commune);

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getDepartementPossibles() {
        List<Departement> lstDepartements = departementDAO.getAll();
        String[] departementPossibles = new String[lstDepartements.size()];
        int index = 0;
        for (Departement departement : lstDepartements) {
            departementPossibles[index++] = departement.getNomDepartement();
        }
        //    listeCodeTriePossibles = Arrays.sort(departementPossibles);
        // listeCodeTriePossibles = Collections.sort(departementPossibles);
        return departementPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Commune commune) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        // Nom du département
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(commune == null ? Constantes.STRING_EMPTY
                        : (commune.getDepartement() == null ? Constantes.STRING_EMPTY : commune.getDepartement().getNomDepartement()),
                        departementPossibles, null, true, false, true));

        // Code postal
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(commune == null ? Constantes.STRING_EMPTY : commune.getCodePostal(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // Nom de la commune
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(commune == null ? Constantes.STRING_EMPTY : commune.getNomCommune(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Commune> getAllElements() throws BusinessException {
        return communeDAO.getAll();
    }

    /**
     * @param departementDAO the departementDAO to set
     */
    public void setDepartementDAO(IDepartementDAO departementDAO) {
        this.departementDAO = departementDAO;
    }

    /**
     * @param communeDAO the communeDAO to set
     */
    public void setCommuneDAO(ICommuneDAO communeDAO) {
        this.communeDAO = communeDAO;
    }

    @Override
    protected ModelGridMetadata<Commune> initModelGridMetadata() {
        departementPossibles =getDepartementPossibles();
        return super.initModelGridMetadata(); 
    }

}
