/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.impl;

import java.time.LocalDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author adiankha
 */
public class ProLineRecord implements Comparable<ProLineRecord>{
    
    LocalDate dateprelevement;
    Long originalLineNumber;
    String nomlabo;
    int numerorepet ;
    int numerolabo;
    String codeechantillon;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
    
    
    public ProLineRecord(){
        super();
    }

    public ProLineRecord(LocalDate dateprelevement, Long originalLineNumber, String nomlabo, int numerorepet, int numerolabo, String codeechantillon, 
           String codevariable,
    float valeurvariable, String statutvaleur, String codemethode,String codeunite, String codehumidite) {
        this.dateprelevement = dateprelevement;
        this.originalLineNumber = originalLineNumber;
        this.nomlabo = nomlabo;
        this.numerorepet = numerorepet;
        this.numerolabo = numerolabo;
        this.codeechantillon = codeechantillon;
       this.codehumidite = codehumidite;
       this.codemethode = codemethode;
       this.codevariable = codevariable;
       this.statutvaleur = statutvaleur;
       this.codeunite = codeunite;
       this.valeurvariable = valeurvariable;
    }
    
    
    public void copy(final ProLineRecord line) {
        this.dateprelevement = line.getDateprelevement();
        this.codehumidite = line.codehumidite;
        this.originalLineNumber = line.getOriginalLineNumber();
        this.nomlabo = line.getNomlabo();
        this.numerolabo = line.getNumerolabo();
        this.numerorepet = line.getNumerorepet();
        this.codeechantillon = line.getCodeechantillon();
        this.codehumidite = line.getCodehumidite();
        this.codemethode = line.getCodemethode();
        this.codevariable = line.getCodevariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codeunite = line.getCodeunite();
        this.valeurvariable = line.getValeurvariable();
    }

    @Override
    public boolean equals(Object obj) {
         return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public int compareTo(ProLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    public String getNomlabo() {
        return nomlabo;
    }

    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    public int getNumerorepet() {
        return numerorepet;
    }

    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    public int getNumerolabo() {
        return numerolabo;
    }

    public void setNumerolabo(int numerolabo) {
        this.numerolabo = numerolabo;
    }

    public String getCodeechantillon() {
        return codeechantillon;
    }

    public void setCodeechantillon(String codeechantillon) {
        this.codeechantillon = codeechantillon;
    }

    public String getCodevariable() {
        return codevariable;
    }

    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    public float getValeurvariable() {
        return valeurvariable;
    }

    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    public String getStatutvaleur() {
        return statutvaleur;
    }

    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    public String getCodemethode() {
        return codemethode;
    }

    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    public String getCodeunite() {
        return codeunite;
    }

    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    public String getCodehumidite() {
        return codehumidite;
    }

    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    

    
    
    
    
}
