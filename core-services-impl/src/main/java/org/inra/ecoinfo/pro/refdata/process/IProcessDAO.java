/*
 *
 */
package org.inra.ecoinfo.pro.refdata.process;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

// TODO: Auto-generated Javadoc
/**
 * The Interface IProcessDAO.
 */
public interface IProcessDAO extends IDAO<Process> {

    /**
     * All process.
     *
     * @return the list< process>
     */
    List<Process> getAll();

    /**
     * Findby id.
     *
     * @param process_intitule
     * @param intitule the intitule
     * @return the process
     * @throws PersistenceException
     */
    Optional<Process> getByNKey(String process_intitule);

}
