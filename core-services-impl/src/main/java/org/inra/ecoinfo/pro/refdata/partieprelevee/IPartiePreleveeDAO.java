/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.partieprelevee;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IPartiePreleveeDAO extends IDAO<PartiePrelevee> {

    /**
     *
     * @return
     */
    List<PartiePrelevee> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<PartiePrelevee> getByNKey(String code);

}
