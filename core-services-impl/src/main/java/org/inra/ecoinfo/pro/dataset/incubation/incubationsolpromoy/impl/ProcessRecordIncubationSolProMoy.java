/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.IMesureIncubationSolProMoyDAO;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.IValeurIncubationSolProMoyDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsol.IEchantillonSolDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.IEchantillonsProduitDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordIncubationSolProMoy extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordIncubationSolProMoy.class);
    protected static final String BUNDLE_PATH_INCUBATION = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB = "MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB = "MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_INCUBATION_ECHAN_BD = "MSG_ERROR_INCUBATION_ECHAN_BD";
    private static final String MSG_ERROR_INCUBATION_ECHAN_PRO_BD = "MSG_ERROR_INCUBATION_ECHAN_PRO_BD";

    protected IMesureIncubationSolProMoyDAO<MesureIncubationSolProMoy> mesureIncubationSolProMoyDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IEchantillonSolDAO echantillonSolDAO;
    protected IMethodeDAO methodeDAO;
    protected IEchantillonsProduitDAO echantillonsproduitDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IValeurIncubationSolProMoyDAO valeurIncubationSolProMoyDAO;

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String fileEncoding,
            DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO); //To change body of generated methods, choose Tools | Templates.
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser, datasetDescriptorPRO);

            final Map<LocalDate, List<IncubationSolProMoyLineRecord>> mesuresMapLines = new HashMap();

            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<IncubationSolProMoyLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        }

    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureIncubationSolProMoyDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    private void readLines(CSVParser parser, Map<LocalDate, List<IncubationSolProMoyLineRecord>> lines,
            long lineCount, ErrorsReport errorsReport) throws IOException {

        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;

            final LocalDate date_prel_sol = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeech = cleanerValues.nextToken();
            final int ordre_manip = Integer.parseInt(cleanerValues.nextToken());
            final float masse_de_sol = Float.parseFloat(cleanerValues.nextToken());
            final LocalDate date_prel_pro = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeechpro = cleanerValues.nextToken();
            final float masse_de_pro = Float.parseFloat(cleanerValues.nextToken());
            final String condition_incub = cleanerValues.nextToken();
            final String n_mineral = cleanerValues.nextToken();
            final LocalDate date_debut_incub = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final int num_rep_analyse = Integer.parseInt(cleanerValues.nextToken());
            final String codevariable = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();
            final int jour_incub = Integer.parseInt(cleanerValues.nextToken());
            final float valeur_moyenne = Float.parseFloat(cleanerValues.nextToken());
            final float valeur_ecart_type = Float.parseFloat(cleanerValues.nextToken());

            final IncubationSolProMoyLineRecord line = new IncubationSolProMoyLineRecord(date_prel_sol, codeech, codeechpro,
                    ordre_manip, date_prel_pro, date_debut_incub, jour_incub, masse_de_sol, condition_incub, num_rep_analyse, masse_de_pro,
                    n_mineral, codevariable, valeur_moyenne, valeur_ecart_type, codemethode, codeunite, codehumidite);

            try {
                if (!lines.containsKey(date_prel_sol)) {
                    lines.put(date_prel_sol, new LinkedList<IncubationSolProMoyLineRecord>());
                }

                lines.get(date_prel_sol).add(line);

            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        date_prel_sol, DateUtil.DD_MM_YYYY));
            }
        }
    }

    private void buildLines(VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            ErrorsReport errorsReport, Map<LocalDate, List<IncubationSolProMoyLineRecord>> lines,
            SortedSet<IncubationSolProMoyLineRecord> ligneEnErreur) throws PersistenceException {

        Iterator<Entry<LocalDate, List<IncubationSolProMoyLineRecord>>> iterator = lines.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry<LocalDate, List<IncubationSolProMoyLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (IncubationSolProMoyLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    private void recordErrors(ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    private void buildMesure(IncubationSolProMoyLineRecord mesuresLines, VersionFile versionFile,
            SortedSet<IncubationSolProMoyLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, ISessionPropertiesPRO sessionProperties) throws PersistenceException {

        LocalDate date_prel_sol = mesuresLines.getDate_prel_sol();
        String codeech = mesuresLines.getCodeech();

        EchantillonsSol echantillonsSol = echantillonSolDAO.getByNKey(Utils.createCodeFromString(codeech)).orElse(null);
        if (echantillonsSol == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_ECHAN_BD), codeech));
        }

        int ordre_manip = mesuresLines.getOrdre_manip();
        float masse_de_sol = mesuresLines.getMasse_de_sol();
        LocalDate date_prel_pro = mesuresLines.getDate_prel_pro();
        String codeechpro = mesuresLines.getCodeechpro();

        final EchantillonsProduit echantillonsProduit = echantillonsproduitDAO.getByECH(Utils.createCodeFromString(codeechpro)).orElse(null);
        if (echantillonsProduit == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_ECHAN_PRO_BD), codeechpro));
        }

        float masse_de_pro = mesuresLines.getMasse_de_pro();
        LocalDate date_debut_incub = mesuresLines.getDate_debut_incub();
        int jour_incub = mesuresLines.getJour_incub();
        String condition_incub = mesuresLines.getCondition_incub();
        int numero_rep_analyse = mesuresLines.getNumero_rep_analyse();
        String n_mineral = mesuresLines.getN_mineral();

        String codevariable = mesuresLines.getCodevariable();
        float valeur_moyenne = mesuresLines.getValeur_moyenne();
        float valeur_ecart_type = mesuresLines.getValeur_ecart_type();
        String codemethode = mesuresLines.getCodemethode();
        String codeunite = mesuresLines.getCodeunite();
        String codehumidite = mesuresLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);
        Long fichier = mesuresLines.getOriginalLineNumber();
        final VersionFile versionfile = this.versionFileDAO.getById(versionFile.getId()).orElse(null);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cvariable = Utils.createCodeFromString(codevariable);
        String cunite = Utils.createCodeFromString(codeunite);
        String cmethode = Utils.createCodeFromString(codemethode);
        String chumitite = Utils.createCodeFromString(codehumidite);

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB), chumitite));
        }

        VariablesPRO dbvariable = variPRODAO.betByNKey(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB), cvariable));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(datatype, dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolProMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolProMoy.MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumitite));

        }
        RealNode realNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesureIncubationSolProMoy mesureIncubationSolProMoy = getOrCreate(date_prel_sol, codeech, ordre_manip, masse_de_sol, date_prel_pro, codeechpro, masse_de_pro, condition_incub, date_debut_incub, jour_incub, numero_rep_analyse, fichier, versionfile, echantillonsSol, echantillonsProduit);
            persistValeurIncubationSolProMoy(valeur_moyenne, valeur_ecart_type, realNode, mesureIncubationSolProMoy);
        }

    }

    private MesureIncubationSolProMoy getOrCreate(LocalDate date_prel_sol, String codeech, int ordre_manip, float masse_de_sol, LocalDate date_prel_pro, String codeechpro, float masse_de_pro, String condition_incub, LocalDate date_debut_incub, int jour_incub, int numero_rep_analyse, Long fichier, VersionFile versionfile, EchantillonsSol echantillonsSol, EchantillonsProduit echantillonsProduit) throws PersistenceException {

        String datePrelSol = null;
        String datePrelPro = null;
        String intStringNumRep = null;
        String intStringJourIncub = null;

        String format = "dd/MM/yyyy";

        datePrelSol = DateUtil.getUTCDateTextFromLocalDateTime(date_prel_sol, format);
        datePrelPro = DateUtil.getUTCDateTextFromLocalDateTime(date_prel_pro, format);

        intStringNumRep = Integer.toString(numero_rep_analyse);
        intStringJourIncub = Integer.toString(jour_incub);

        String key = datePrelSol + "_" + codeech + "_" + ordre_manip + "_" + datePrelPro + "_" + codeechpro + "_" + intStringNumRep + "_" + intStringJourIncub;

        MesureIncubationSolProMoy mesureIncubationSolProMoy = mesureIncubationSolProMoyDAO.getByKeys(key).orElse(null);

        if (mesureIncubationSolProMoy == null) {

            mesureIncubationSolProMoy = new MesureIncubationSolProMoy(date_prel_sol, echantillonsSol, ordre_manip, masse_de_sol, date_prel_pro, echantillonsProduit, masse_de_pro, condition_incub, date_debut_incub, jour_incub, numero_rep_analyse, fichier, versionfile);

            mesureIncubationSolProMoy.setLocalDate_prel_sol(date_prel_sol);
            mesureIncubationSolProMoy.setEchantillonsSol(echantillonsSol);
            mesureIncubationSolProMoy.setOrdre_manip(ordre_manip);
            mesureIncubationSolProMoy.setMasse_de_sol(masse_de_sol);
            mesureIncubationSolProMoy.setLocalDate_prel_pro(date_prel_pro);
            mesureIncubationSolProMoy.setEchantillonsProduit(echantillonsProduit);
            mesureIncubationSolProMoy.setMasse_de_pro(masse_de_pro);
            mesureIncubationSolProMoy.setCondition_incub(condition_incub);
            mesureIncubationSolProMoy.setLocalDate_debut_incub(date_debut_incub);
            mesureIncubationSolProMoy.setNumero_rep_analyse(numero_rep_analyse);
            mesureIncubationSolProMoy.setLigneFichierEchange(fichier);
            mesureIncubationSolProMoy.setJour_incub(jour_incub);
            mesureIncubationSolProMoy.setVersionfile(versionfile);
            mesureIncubationSolProMoy.setKeymesure(key);

            mesureIncubationSolProMoyDAO.saveOrUpdate(mesureIncubationSolProMoy);
        }

        return mesureIncubationSolProMoy;
    }

    private void persistValeurIncubationSolProMoy(float valeur_moyenne, float valeur_ecart_type, RealNode realNode, MesureIncubationSolProMoy mesureIncubationSolProMoy) throws PersistenceException {
        final ValeurIncubationSolProMoy valeurIncubationSolProMoy = valeurIncubationSolProMoyDAO.getByNkeys(realNode, valeur_ecart_type, mesureIncubationSolProMoy).orElse(null);
        createOrUpdateValeurIncubSPM(realNode, mesureIncubationSolProMoy, valeur_moyenne, valeur_ecart_type, valeurIncubationSolProMoy);
    }

    private void createOrUpdateValeurIncubSPM(RealNode realNode, MesureIncubationSolProMoy mesureIncubationSolProMoy, float valeur_moyenne, float valeur_ecart_type, ValeurIncubationSolProMoy valeurIncubationSolProMoy) throws PersistenceException {

        if (valeurIncubationSolProMoy == null) {
            ValeurIncubationSolProMoy valeurIncubationSPM = new ValeurIncubationSolProMoy(valeur_moyenne, valeur_ecart_type, mesureIncubationSolProMoy, realNode);
            valeurIncubationSPM.setRealNode(realNode);
            valeurIncubationSPM.setValeur(valeur_moyenne);
            valeurIncubationSPM.setEcart_type(valeur_ecart_type);
            valeurIncubationSPM.setMesureIncubationSolProMoy(mesureIncubationSolProMoy);

            createVISPM(valeurIncubationSPM);

        } else {

            updateVISPM(realNode, mesureIncubationSolProMoy, valeur_moyenne, valeur_ecart_type, valeurIncubationSolProMoy);
        }
    }

    private void createVISPM(ValeurIncubationSolProMoy valeurIncubationSPM) throws PersistenceException {
        valeurIncubationSolProMoyDAO.saveOrUpdate(valeurIncubationSPM);
    }

    private void updateVISPM(RealNode realNode, MesureIncubationSolProMoy mesureIncubationSolProMoy, float valeur_moyenne, float valeur_ecart_type, ValeurIncubationSolProMoy valeurIncubationSolProMoy) throws PersistenceException {
        valeurIncubationSolProMoy.setRealNode(realNode);
        valeurIncubationSolProMoy.setMesureIncubationSolProMoy(mesureIncubationSolProMoy);
        valeurIncubationSolProMoy.setValeur(valeur_moyenne);
        valeurIncubationSolProMoy.setEcart_type(valeur_ecart_type);
        valeurIncubationSolProMoyDAO.saveOrUpdate(valeurIncubationSolProMoy);
    }

    public IMesureIncubationSolProMoyDAO<MesureIncubationSolProMoy> getMesureIncubationSolProMoyDAO() {
        return mesureIncubationSolProMoyDAO;
    }

    public void setMesureIncubationSolProMoyDAO(IMesureIncubationSolProMoyDAO<MesureIncubationSolProMoy> mesureIncubationSolProMoyDAO) {
        this.mesureIncubationSolProMoyDAO = mesureIncubationSolProMoyDAO;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public IEchantillonSolDAO getEchantillonSolDAO() {
        return echantillonSolDAO;
    }

    public void setEchantillonSolDAO(IEchantillonSolDAO echantillonSolDAO) {
        this.echantillonSolDAO = echantillonSolDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IEchantillonsProduitDAO getEchantillonsproduitDAO() {
        return echantillonsproduitDAO;
    }

    public void setEchantillonsproduitDAO(IEchantillonsProduitDAO echantillonsproduitDAO) {
        this.echantillonsproduitDAO = echantillonsproduitDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    public IValeurIncubationSolProMoyDAO getValeurIncubationSolProMoyDAO() {
        return valeurIncubationSolProMoyDAO;
    }

    public void setValeurIncubationSolProMoyDAO(IValeurIncubationSolProMoyDAO valeurIncubationSolProMoyDAO) {
        this.valeurIncubationSolProMoyDAO = valeurIncubationSolProMoyDAO;
    }

}
