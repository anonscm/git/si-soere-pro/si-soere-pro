/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cepagevariete;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface ICepageVarieteDAO extends IDAO<CepageVariete>{

    /**
     *
     * @param libelle
     * @return
     */
    Optional<CepageVariete> getByNKey(String libelle);

    /**
     *
     * @return
     */
    List<CepageVariete> getAll();
    
}
