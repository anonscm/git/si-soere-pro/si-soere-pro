/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.itk;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation_;
import org.inra.ecoinfo.pro.synthesis.semisplantation.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.semisplantation.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class SemisSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurSemisPlantation, MesureSemisPlantation> {

    @Override
    Class<ValeurSemisPlantation> getValueClass() {
        return ValeurSemisPlantation.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurSemisPlantation, MesureSemisPlantation> getMesureAttribute() {
        return ValeurSemisPlantation_.mesureinterventionsemis;
    }

    @Override
    Boolean isValue() {
        return Boolean.TRUE;
    }

}
