/**
 *
 */
package org.inra.ecoinfo.pro.refdata.pays;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 * @author sophie
 *
 */
public class JPAPaysDAO extends AbstractJPADAO<Pays> implements IPaysDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.pays.IPaysDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<Pays> getAll() {
        return getAll(Pays.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.pays.IPaysDAO#getByNom(java.lang.String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @Override
    public Optional<Pays> getByNKey(String nom) {
        CriteriaQuery<Pays> query = builder.createQuery(Pays.class);
        Root<Pays> pays = query.from(Pays.class);
        query
                .select(pays)
                .where(
                        builder.equal(pays.get(Pays_.code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);

    }
}
