/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.portegreffe;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAPorteGreffeDAO extends AbstractJPADAO<PorteGreffe> implements IPorteGreffeDAO {

    /**
     *
     * @param libelle
     * @return
     */
    @Override
    public Optional<PorteGreffe> getByNKey(String libelle) {
        CriteriaQuery<PorteGreffe> query = builder.createQuery(PorteGreffe.class);
        Root<PorteGreffe> porteGreffe = query.from(PorteGreffe.class);
        query
                .select(porteGreffe)
                .where(
                        builder.equal(porteGreffe.get(PorteGreffe_.pg_libelle), libelle)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<PorteGreffe> getAll() {
        return getAllBy(PorteGreffe.class, PorteGreffe::getPg_code);
    }

}
