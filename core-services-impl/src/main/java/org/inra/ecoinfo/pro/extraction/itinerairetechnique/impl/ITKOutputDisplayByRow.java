
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import org.slf4j.LoggerFactory;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;

/**
 *
 * @author adiankha
 */
public class ITKOutputDisplayByRow extends ITKOutputsBuildersResolver {

    /**
     *
     */
    protected static final Logger LOGGER = LoggerFactory.getLogger(ITKOutputDisplayByRow.class);
    
    /**
     *
     */
    public ITKOutputDisplayByRow() {
    }
    
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) {
        try {
            super.setRecolteCoupeOutputBuilder(this.recolteCoupeOutputBuilder);
            super.setTravailSolOutputBuilder(this.travailSolOutputBuilder);
            super.setApportOutputBuilder(this.apportOutputBuilder);
            super.setSemisPlantationOutputBuilder(this.semisPlantationOutputBuilder);
            super.setProEtudieOutputBuilder(this.proEtudieOutputBuilder);
            return super.buildOutput(parameters);
        } catch (BusinessException e) {
            LOGGER.error("error while building output", e);
            return null;
        }
    }
    
}
