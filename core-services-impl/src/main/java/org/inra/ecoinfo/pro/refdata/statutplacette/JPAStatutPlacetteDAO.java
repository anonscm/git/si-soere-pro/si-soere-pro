/**
 *
 */
package org.inra.ecoinfo.pro.refdata.statutplacette;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAStatutPlacetteDAO extends AbstractJPADAO<StatutPlacette> implements IStatutPlacetteDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.statutplacette.IStatutPlacetteDAO#getByLibelle (String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<StatutPlacette> getByNKey(String libelle) {
        CriteriaQuery<StatutPlacette> query = builder.createQuery(StatutPlacette.class);
        Root<StatutPlacette> statutPlacette = query.from(StatutPlacette.class);
        query
                .select(statutPlacette)
                .where(
                        builder.equal(statutPlacette.get(StatutPlacette_.libelle), libelle)
                );
        return getOptional(query);
    }

}
