/*
 *
 */
package org.inra.ecoinfo.pro.refdata.profillocalisation;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author Vivianne
 */
public class Recorder extends AbstractGenericRecorder<ProfilLocalisation> {

    /**
     *
     */
    protected IProfilLocalisationDAO profillocalisationDAO;

    /**
     *
     */
    protected Properties propertiesNom;

    private void createProfilLocalisation(final ProfilLocalisation profillocalisation) throws BusinessException {
        try {
            profillocalisationDAO.saveOrUpdate(profillocalisation);
            profillocalisationDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create profillocalisation");
        }
    }

    private void updateProfilLocalisation(String profil_localisation_code, String profil_localisation_nom, int code_profil_donesol, int x, int y, int geom, final ProfilLocalisation dbProfilLocalisation) throws BusinessException {
        try {
            dbProfilLocalisation.setProfil_localisation_nom(profil_localisation_nom);
            dbProfilLocalisation.setProfil_localisation_code(profil_localisation_code);
            dbProfilLocalisation.setCode_profil_donesol(code_profil_donesol);
            dbProfilLocalisation.setX(x);
            dbProfilLocalisation.setY(y);
            dbProfilLocalisation.setGeom(geom);
            profillocalisationDAO.saveOrUpdate(dbProfilLocalisation);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update profillocalisation");
        }
    }

    private void createOrUpdateProfilLocalisation(String profil_localisation_code, String profil_localisation_nom, int code_profil_donesol, int x, int y, int geom, final ProfilLocalisation dbProfilLocalisation) throws BusinessException {

        if (dbProfilLocalisation == null) {

            final ProfilLocalisation profillocalisation = new ProfilLocalisation(profil_localisation_code, profil_localisation_nom, code_profil_donesol, x, y, geom);
            profillocalisation.setProfil_localisation_code(profil_localisation_code);
            profillocalisation.setProfil_localisation_nom(profil_localisation_nom);
            profillocalisation.setCode_profil_donesol(code_profil_donesol);
            profillocalisation.setX(x);
            profillocalisation.setY(y);
            profillocalisation.setGeom(geom);
            createProfilLocalisation(profillocalisation);

        } else {

            updateProfilLocalisation(profil_localisation_code, profil_localisation_nom, code_profil_donesol, x, y, geom, dbProfilLocalisation);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProfilLocalisation.NAME_ENTITY_JPA);
                final String profil_localisation_nom = tokenizerValues.nextToken();
                profillocalisationDAO.remove(profillocalisationDAO.getByNkey(profil_localisation_nom)
                        .orElseThrow(() -> new BusinessException("can't get profillocalisation")));
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<ProfilLocalisation> getAllElements() throws BusinessException {
        return profillocalisationDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProfilLocalisation profillocalisation) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(profillocalisation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : profillocalisation.getProfil_localisation_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(profillocalisation == null || profillocalisation.getProfil_localisation_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : propertiesNom.getProperty(profillocalisation.getProfil_localisation_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(profillocalisation == null || profillocalisation.getCode_profil_donesol() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : profillocalisation.getCode_profil_donesol(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(profillocalisation == null || profillocalisation.getX() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : profillocalisation.getX(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(profillocalisation == null || profillocalisation.getY() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : profillocalisation.getY(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(profillocalisation == null || profillocalisation.getGeom() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : profillocalisation.getGeom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProfilLocalisationDAO getprofillocalisationDAO() {
        return profillocalisationDAO;
    }

    @Override
    protected ModelGridMetadata<ProfilLocalisation> initModelGridMetadata() {
        propertiesNom = localizationManager.newProperties(ProfilLocalisation.NAME_ENTITY_JPA, ProfilLocalisation.JPA_COLUMN_NOM, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void persistPropfilLocalisation(String profil_localisation_code, String profil_localisation_nom, int code_profil_donesol, int x, int y, int geom) throws BusinessException {
        final ProfilLocalisation dbprofillocalisation = profillocalisationDAO.getByNkey(profil_localisation_nom).orElse(null);
        createOrUpdateProfilLocalisation(profil_localisation_code, profil_localisation_nom, code_profil_donesol, x, y, geom, dbprofillocalisation);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();

        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            int line =0;
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProfilLocalisation.NAME_ENTITY_JPA);
                final String profil_localisation_nom = tokenizerValues.nextToken();
                final String profil_localisation_code = Utils.createCodeFromString(profil_localisation_nom);
                int X = verifieInt(tokenizerValues, line, false, errorsReport);
                int Y = verifieInt(tokenizerValues, line, false, errorsReport);
                int Geom = verifieInt(tokenizerValues, line, false, errorsReport);
                int code_donesol = verifieInt(tokenizerValues, line, true, errorsReport);

                persistPropfilLocalisation(profil_localisation_code, profil_localisation_nom, code_donesol, X, Y, Geom);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @return
     */
    public IProfilLocalisationDAO getProfillocalisationDAO() {
        return profillocalisationDAO;
    }

    /**
     *
     * @param profillocalisationDAO
     */
    public void setProfillocalisationDAO(
            IProfilLocalisationDAO profillocalisationDAO) {
        this.profillocalisationDAO = profillocalisationDAO;
    }

    /**
     *
     * @return
     */
    public Properties getPropertiesNom() {
        return propertiesNom;
    }

    /**
     *
     * @param propertiesNom
     */
    public void setPropertiesNom(Properties propertiesNom) {
        this.propertiesNom = propertiesNom;
    }

}
