package org.inra.ecoinfo.pro.extraction.incubation.incubationsolmoyennees.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationOutputBuilder;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolMoyBuildOutputDisplayByRow extends AbstractIncubationOutputBuilder<MesureIncubationSolMoy> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_INCUBATIONSOLMOY";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.incubation.incubation";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";

    static final String PATTERN_CSV_N_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String LIST_COLUMN_VARIABLE_INCUBATION_SOL_MOY = "LIST_COLUMN_VARIABLE_INCUBATION_SOL_MOY";

    final ComparatorVariable comparator = new ComparatorVariable();
    IVariablesPRODAO variPRODAO;

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return IncubationSolMoyExtractor.MAP_INDEX_INCUBATION_SOL_MOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IIncubationSolMoyDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_MOY;
    }

    /**
     *
     * @param mesure
     * @param mesuresMap
     */
    @Override
    protected void buildmap(List<MesureIncubationSolMoy> mesure, SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolMoy>>> mesuresMap) {
        java.util.Iterator<MesureIncubationSolMoy> itMesure = mesure
                .iterator();
        while (itMesure.hasNext()) {
            MesureIncubationSolMoy mesureIncubationSolMoy = itMesure
                    .next();
            EchantillonsSol echan = mesureIncubationSolMoy.getEchantillonsSol();
            Long siteId = echan.getPrelevementsol().getDispositif().getId();
            if (mesuresMap.get(siteId) == null) {
                mesuresMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresMap.get(siteId).get(echan) == null) {
                mesuresMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresMap.get(siteId).get(echan).put(mesureIncubationSolMoy.getLocalDate_debut_incub(), mesureIncubationSolMoy);
            itMesure.remove();
        }
    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedIncubationVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresMap
     */
    @Override
    protected void readMap(
            List<Dispositif> selectedDispositifs,
            List<VariablesPRO> selectedIncubationVariables,
            IntervalDate selectedIntervalDate,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolMoy>>> mesuresMap) {

        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                IncubationSolMoyBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        IncubationSolMoyBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        IncubationSolMoyBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {

        if (((DefaultParameter) parameters).getResults().get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_MOY_CODE)
                .get(IncubationSolMoyExtractor.MAP_INDEX_INCUBATION_SOL_MOY) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_MOY_CODE)
                        .get(IncubationSolMoyExtractor.MAP_INDEX_INCUBATION_SOL_MOY).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_MOY_CODE));
        return null;

    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolMoy>>> mesuresMap, IntervalDate selectedIntervalDate) {

        try {

            String currentDispositif;
            String currentEchantillon;
            int ordre_manip;
            float Masse_de_sol_sec;
            //String condition_incubation;
            String humidite_incubation;
            float temperature_incubation;
            float N_mineral_par_solution;
            LocalDate date_debut_incub;
            String labo_analyse;
            String code_interne_labo;
            int numero_rep_analyse;
            int jour_incubation;

            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    IncubationSolMoyBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    IncubationSolMoyBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_INCUBATION_SOL_MOY).split(";"));

            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolMoy>> mesureIncubSolMoyByDisp = mesuresMap
                        .get(dispositif.getId());

                if (mesureIncubSolMoyByDisp == null) {
                    continue;
                }

                Iterator<Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolMoy>>> itEchan = mesureIncubSolMoyByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolMoy>> echanEntry = itEchan.next();
                    EchantillonsSol echantillonsSol = echanEntry.getKey();
                    SortedMap<LocalDate, MesureIncubationSolMoy> mesureEchantMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());

                    for (Entry<LocalDate, MesureIncubationSolMoy> entrySet : mesureEchantMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureIncubationSolMoy mesureIncubationSolMoy = entrySet.getValue();
                        currentDispositif = mesureIncubationSolMoy.getEchantillonsSol().getPrelevementsol().getDispositif().getNomDispo();
                        currentEchantillon = mesureIncubationSolMoy.getEchantillonsSol().getCodeechsol();
                        ordre_manip = mesureIncubationSolMoy.getOrdre_manip();
                        Masse_de_sol_sec = mesureIncubationSolMoy.getMasse_de_sol();
                        humidite_incubation = mesureIncubationSolMoy.getHumidite_incub();
                        temperature_incubation = mesureIncubationSolMoy.getTemperature_incub();
                        N_mineral_par_solution = mesureIncubationSolMoy.getN_mineral_apporte();
                        date_debut_incub = mesureIncubationSolMoy.getLocalDate_debut_incub();
                        labo_analyse = mesureIncubationSolMoy.getLabo_analyse();
                        code_interne_labo = mesureIncubationSolMoy.getCode_interne_labo();
                        numero_rep_analyse = mesureIncubationSolMoy.getNumero_rep_analyse();
                        jour_incubation = mesureIncubationSolMoy.getJour_incub();
                        String genericPattern = LineDataFixe(date, currentDispositif, currentEchantillon, ordre_manip, Masse_de_sol_sec,
                                humidite_incubation, temperature_incubation, N_mineral_par_solution, date_debut_incub, labo_analyse,
                                code_interne_labo, numero_rep_analyse, jour_incubation);
                        out.print(genericPattern);
                        LineDataVariable(mesureIncubationSolMoy, out, listVariable);
                    }
                    itEchan.remove();
                }
            }

        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentEchantillon, int ordre_manip, float Masse_de_sol_sec, String humidite_incubation, float temperature_incubation, float N_mineral_par_solution, LocalDate date_debut_incub, String labo_analyse, String code_interne_labo, int numero_rep_analyse, int jour_incubation)  {

        String genericPattern = String.format(PATTERN_CSV_N_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentEchantillon,
                ordre_manip,
                Masse_de_sol_sec,
                humidite_incubation,
                temperature_incubation,
                N_mineral_par_solution,
                date_debut_incub,
                labo_analyse,
                code_interne_labo,
                numero_rep_analyse,
                jour_incubation);

        return genericPattern;

    }

    private void LineDataVariable(MesureIncubationSolMoy mesureIncubationSolMoy, PrintStream out, List<String> listVariable) {
        final List<ValeurIncubationSolMoy> valeurIncubationSolMoys = mesureIncubationSolMoy.getValeurIncubationSolMoy();
        listVariable.forEach((_item) -> {
            for (Iterator<ValeurIncubationSolMoy> ValeurIterator = valeurIncubationSolMoys.iterator(); ValeurIterator.hasNext();) {
                ValeurIncubationSolMoy valeur = ValeurIterator.next();
//                if (!((DatatypeVariableUnitePRO)valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
//                    continue;
//                }
String line;
line = String.format(";%s;%s;%s;%s;%s;%s",
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
        getValeurToString(valeur),
        valeur.getEcart_type(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());

out.print(line);
ValeurIterator.remove();
            }
        });
        out.println();
    }

    private String getValeurToString(ValeurIncubationSolMoy valeurmoy) {
        if (valeurmoy.getValeur() != null) {
            return valeurmoy.getValeur().toString();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public ComparatorVariable getComparator() {
        return comparator;
    }

    /**
     *
     * @return
     */
    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(IncubationSolMoyBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
