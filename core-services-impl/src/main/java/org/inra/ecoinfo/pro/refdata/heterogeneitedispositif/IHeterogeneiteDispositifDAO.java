/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.heterogeneitedispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.heterogeinite.Heterogeinite;

/**
 *
 * @author adiankha
 */
public interface IHeterogeneiteDispositifDAO extends IDAO<HeterogeineteDispositif> {

    /**
     *
     * @return
     */
    List<HeterogeineteDispositif> getAll();

    /**
     *
     * @param dispositif
     * @param hetero
     * @return
     */
    Optional<HeterogeineteDispositif> getByNKey(Dispositif dispositif, Heterogeinite hetero);

}
