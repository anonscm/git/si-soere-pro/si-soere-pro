/**
 *
 */
package org.inra.ecoinfo.pro.refdata.personneressource;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IPersonneRessourceDAO extends IDAO<PersonneRessource> {

    /**
     *
     * @param eMail
     * @return
     */
    Optional<PersonneRessource> getByEMail(String eMail);

    /**
     *
     * @param nom
     * @return
     */
    List<PersonneRessource> getByNom(String nom);

    /**
     *
     * @param nom
     * @param prenom
     * @return
     */
    List<PersonneRessource> getByNomPrenom(String nom, String prenom);

    /**
     *
     * @param nom
     * @param prenom
     * @param eMail
     * @return
     */
    Optional<PersonneRessource> getByNKey(String nom, String prenom, String eMail);
}
