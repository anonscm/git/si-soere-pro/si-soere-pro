/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.amenagementavantplantation;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.typeculture.ITypeCultureDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<AmenagementAvantPlantation> {

    IAmenagementAvantPlantationDAO avpDAO;
    ITypeCultureDAO typeCultureDAO;
    private Properties aapEn;

    /**
     *
     * @param avp
     * @throws BusinessException
     */
    public void createAVP(AmenagementAvantPlantation avp) throws BusinessException {
        try {
            avpDAO.saveOrUpdate(avp);
            avpDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't save AmenagementAvantPlantation", ex);
        }
    }

    private void updateAVP(final String libelle, final AmenagementAvantPlantation dbavp) throws BusinessException {
        try {
            dbavp.setAap_libelle(libelle);
            avpDAO.saveOrUpdate(dbavp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update AmenagementAvantPlantation", ex);

        }
    }

    private void createOrUpdateAVP(final String code, final String libelle, final AmenagementAvantPlantation dbavp) throws BusinessException {
        if (dbavp == null) {
            final AmenagementAvantPlantation avp = new AmenagementAvantPlantation(libelle);
            avp.setAap_code(code);
            avp.setAap_libelle(libelle);
            createAVP(avp);
        } else {
            updateAVP(libelle, dbavp);
        }
    }

    private void persistAVP(final String code, final String nom) throws BusinessException {
        final AmenagementAvantPlantation dbporte = avpDAO.getByNKey(nom).orElse(null);
        createOrUpdateAVP(code, nom, dbporte);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();
                avpDAO.remove(avpDAO.getByNKey(nom).orElseThrow(PersistenceException::new));
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<AmenagementAvantPlantation> getAllElements() throws BusinessException {
        return avpDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, AmenagementAvantPlantation.NAME_ENTITY_JPA);
                final String libelle = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(libelle);
                persistAVP(code, libelle);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(AmenagementAvantPlantation avp) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(avp == null || avp.getAap_libelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : avp.getAap_libelle(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(avp == null || avp.getAap_libelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : aapEn.getProperty(avp.getAap_libelle()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param avpDAO
     */
    public void setAvpDAO(IAmenagementAvantPlantationDAO avpDAO) {
        this.avpDAO = avpDAO;
    }

    @Override
    protected ModelGridMetadata<AmenagementAvantPlantation> initModelGridMetadata() {
        aapEn = localizationManager.newProperties(AmenagementAvantPlantation.NAME_ENTITY_JPA, AmenagementAvantPlantation.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}
