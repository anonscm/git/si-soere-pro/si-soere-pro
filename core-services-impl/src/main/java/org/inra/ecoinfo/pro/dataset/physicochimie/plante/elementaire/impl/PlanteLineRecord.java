/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl;

import java.time.LocalDate;

/**
 *
 * @author adiankha
 */
public class PlanteLineRecord implements Comparable<PlanteLineRecord> {

    LocalDate dateprelevement;
    String codeechantillon;
    String nomlabo;
    String reference;
    int numerorepet;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
    Long originalLineNumber;

    /**
     *
     */
    public PlanteLineRecord() {
    }

    /**
     *
     * @param dateprelevement
     * @param codeechantillon
     * @param nomlabo
     * @param reference
     * @param numerorepet
     * @param codevariable
     * @param valeurvariable
     * @param statutvaleur
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     * @param originalLineNumber
     */
    public PlanteLineRecord(LocalDate dateprelevement, String codeechantillon, String nomlabo, String reference, int numerorepet, String codevariable,
            float valeurvariable, String statutvaleur, String codemethode, String codeunite, String codehumidite, Long originalLineNumber) {
        this.dateprelevement = dateprelevement;
        this.codeechantillon = codeechantillon;
        this.nomlabo = nomlabo;
        this.reference = reference;
        this.numerorepet = numerorepet;
        this.codevariable = codevariable;
        this.valeurvariable = valeurvariable;
        this.statutvaleur = statutvaleur;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;
        this.originalLineNumber = originalLineNumber;
    }

    @Override
    public int compareTo(PlanteLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @param line
     */
    public void copy(final PlanteLineRecord line) {
        this.dateprelevement = line.getDateprelevement();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.nomlabo = line.getNomlabo();
        this.reference = line.getReference();
        this.numerorepet = line.getNumerorepet();
        this.codeechantillon = line.getCodeechantillon();
        this.codehumidite = line.getCodehumidite();
        this.codemethode = line.getCodemethode();
        this.codevariable = line.getCodevariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codeunite = line.getCodeunite();
        this.valeurvariable = line.getValeurvariable();
    }

    /**
     *
     * @return
     */
    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    /**
     *
     * @param dateprelevement
     */
    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    /**
     *
     * @return
     */
    public String getCodeechantillon() {
        return codeechantillon;
    }

    /**
     *
     * @param codeechantillon
     */
    public void setCodeechantillon(String codeechantillon) {
        this.codeechantillon = codeechantillon;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public String getReference() {
        return reference;
    }

    /**
     *
     * @param reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     *
     * @return
     */
    public int getNumerorepet() {
        return numerorepet;
    }

    /**
     *
     * @param numerorepet
     */
    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeurvariable() {
        return valeurvariable;
    }

    /**
     *
     * @param valeurvariable
     */
    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

}
