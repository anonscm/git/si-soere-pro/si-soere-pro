package org.inra.ecoinfo.pro.refdata.commune;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.departement.Departement;

/**
 * @author sophie
 *
 */
public interface ICommuneDAO extends IDAO<Commune> {

    List<Commune> getAll();

    List<Commune> getByCodePostal(String codePostal);

    Optional<Commune> getByNKey(String nomCommune, String codePostal);

    List<Commune> getByDepartement(Departement departement);

}
