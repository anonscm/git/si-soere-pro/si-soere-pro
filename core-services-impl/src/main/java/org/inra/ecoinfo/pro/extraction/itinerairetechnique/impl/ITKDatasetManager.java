/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDAO;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDatatypeManager;
import org.inra.ecoinfo.pro.extraction.jsf.IDispositifManager;
import org.inra.ecoinfo.pro.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author adiankha
 */
public class ITKDatasetManager extends MO implements IDispositifManager, IVariableManager<Dispositif> {

    static final String VARIABLE_RECOLTE_COUPE_PRIVILEGE_PATTERN = "%s/%s/*/recolte_coupe/%s";
    static final String VARIABLE_TRAVAIL_SOL_PRIVILEGE_PATTERN = "%s/%s/*/travail_sol/%s";
    static final String VARIABLE_APPORT_PRIVILEGE_PATTERN = "%s/%s/*/apport/%s";
    static final String VARIABLE_PRO_ETUDIE_PRIVILEGE_PATTERN = "%s/%s/*/pro_etudie/%s";
    static final String VARIABLE_SEMIS_PLANTATION_PRIVILEGE_PATTERN = "%s/%s/*/semis_plantation/%s";

    Map<String, IITKDAO> extractionsDAO = new HashMap();
    IITKDAO travailDuSolDAO;
    IITKDAO proEtudieDAO;
    IITKDAO recolteCoupeDAO;
    IITKDAO apportDAO;
    IITKDAO semisDAO;

    /**
     *
     * @return
     */
    @Override
    public List<Dispositif> getAvailablesDispositifs() {
        final List<Dispositif> availablesDispositif = new LinkedList();
        final List<Dispositif> dispositif = this.travailDuSolDAO.getAvailablesDispositifs(policyManager.getCurrentUser());
        dispositif.addAll(this.proEtudieDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        dispositif.addAll(this.recolteCoupeDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        dispositif.addAll(this.apportDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        dispositif.addAll(this.semisDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        dispositif.stream().forEach(disps -> availablesDispositif.add(disps));
        return availablesDispositif;
    }

    /**
     *
     * @param travailDuSolDAO
     */
    public void setTravailDuSolDAO(IITKDAO travailDuSolDAO) {
        this.travailDuSolDAO = travailDuSolDAO;
        this.extractionsDAO.put(IITKDatatypeManager.CODE_DATATYPE_TRAVAIL_DU_SOL, travailDuSolDAO);
    }

    /**
     *
     * @param proEtudieDAO
     */
    public void setProEtudieDAO(IITKDAO proEtudieDAO) {
        this.proEtudieDAO = proEtudieDAO;
        this.extractionsDAO.put(IITKDatatypeManager.CODE_DATATYPE_PRO_ETUDIE, proEtudieDAO);
    }

    /**
     *
     * @param recolteCoupeDAO
     */
    public void setRecolteCoupeDAO(IITKDAO recolteCoupeDAO) {
        this.recolteCoupeDAO = recolteCoupeDAO;
        this.extractionsDAO.put(IITKDatatypeManager.CODE_DATATYPE_RECOLTE_COUPE, recolteCoupeDAO);
    }

    /**
     *
     * @param apportDAO
     */
    public void setApportDAO(IITKDAO apportDAO) {
        this.apportDAO = apportDAO;
        this.extractionsDAO.put(IITKDatatypeManager.CODE_DATATYPE_APPORT, apportDAO);
    }

    /**
     *
     * @param semisDAO
     */
    public void setSemisDAO(IITKDAO semisDAO) {
        this.semisDAO = semisDAO;
        this.extractionsDAO.put(IITKDatatypeManager.CODE_DATATYPE_SEMIS_PLANTATION, semisDAO);
    }

    /**
     *
     * @param linkedList
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByDispositif(List<Dispositif> linkedList) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        extractionsDAO.entrySet().forEach((entry) -> {
            String datatype = entry.getKey();
            IITKDAO itkDAO = entry.getValue();
            variablesByDatatype.put(datatype, itkDAO.getAvailablesVariablesByDispositif(linkedList, policyManager.getCurrentUser()));
        });
        return variablesByDatatype;
    }

}
