/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structure;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.adresse.Adresse;

/**
 * @author sophie
 *
 */
public class JPAStructureDAO extends AbstractJPADAO<Structure> implements IStructureDAO {

    @Override
    public List<Structure> getAll() {
        return getAllBy(Structure.class, Structure::getNom);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.structure.IStructureDAO#getByAdr(org.inra .ecoinfo.pro.refdata.adresse.Adresse)
     */

    /**
     *
     * @param adresse
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Structure> getByAdr(Adresse adresse) {
        CriteriaQuery<Structure> query = builder.createQuery(Structure.class);
        Root<Structure> structure = query.from(Structure.class);
        Join<Structure, Adresse> adrr = structure.join(Structure_.adresse);
        query
                .select(structure)
                .where(
                        builder.equal(adrr, adresse)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.structure.IStructureDAO#getByNom(java.lang .String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Structure> getByNom(String nom) {
        CriteriaQuery<Structure> query = builder.createQuery(Structure.class);
        Root<Structure> structure = query.from(Structure.class);
        query
                .select(structure)
                .where(
                        builder.equal(structure.get(Structure_.nom), nom)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.structure.IStructureDAO#getByNomPrec(java .lang.String, java.lang.String)
     */

    /**
     *
     * @param nom
     * @param precision
     * @return
     */

    @Override
    public Optional<Structure> getByNKey(String nom, String precision) {
        CriteriaQuery<Structure> query = builder.createQuery(Structure.class);
        Root<Structure> structure = query.from(Structure.class);
        query
                .select(structure)
                .where(
                        builder.equal(structure.get(Structure_.nom), nom),
                        builder.equal(structure.get(Structure_.precision), precision)
                );
        return getOptional(query);
    }

}
