/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.proetudie.impl;

import java.io.PrintStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDatatypeManager;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.ITKExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class ProEtudieBuildOutputDisplayByRow extends AbstractITKOutputBuilder<MesureProEtudie> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_PROETUDIE";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.itk.itk-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    static final String PATTERB_CSV_23_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String LIST_COLUMN_VARIABLE_PRO = "LIST_COLUMN_VARIABLE_PRO";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    final ComparatorVariable comparator = new ComparatorVariable();

    IVariablesPRODAO variPRODAO;

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                ProEtudieBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        ProEtudieBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        ProEtudieBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(ITKExtractor.CST_RESULT_EXTRACTION_PROETUDIE_CODE)
                .get(ProEtudieExtractor.MAP_INDEX_PRO_ETUDIE) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(ITKExtractor.CST_RESULT_EXTRACTION_PROETUDIE_CODE)
                        .get(ProEtudieExtractor.MAP_INDEX_PRO_ETUDIE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ITKExtractor.CST_RESULT_EXTRACTION_PROETUDIE_CODE));
        return null;
    }

    /**
     *
     * @param mesuresProEtudie
     * @param mesuresProEtudieMap
     * @throws ParseException
     */
    @Override
    protected void buildmap(List<MesureProEtudie> mesuresProEtudie, SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureProEtudie>>> mesuresProEtudieMap) throws ParseException {
        java.util.Iterator<MesureProEtudie> itMesure = mesuresProEtudie
                .iterator();
        while (itMesure.hasNext()) {
            MesureProEtudie mesureProEtudies = itMesure
                    .next();
            DescriptionTraitement echan = mesureProEtudies.getDescriptionTraitement();
            Long siteId = echan.getDispositif().getId();
            if (mesuresProEtudieMap.get(siteId) == null) {
                mesuresProEtudieMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresProEtudieMap.get(siteId).get(echan) == null) {
                mesuresProEtudieMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresProEtudieMap.get(siteId).get(echan).put(mesureProEtudies.getDatedebut(), mesureProEtudies);
            itMesure.remove();
        }

    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedRecolteCoupeVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresProEtudiesMap
     */
    @Override
    protected void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedRecolteCoupeVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureProEtudie>>> mesuresProEtudiesMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresProEtudiesMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureProEtudie>>> mesuresProEtudiesMap, IntervalDate selectedIntervalDate) {
        try {

            String currentDispositif;
            String currentTraitement;
            String currentParcelle;
            String curentplacette;
            String typepro;
            String codepro;
            LocalDate datefin;
            String materielapport;
            LocalDate enfouil;
            String culture;
            String bbch;
            String stade;
            String typeobs;
            String observation;
            String niveauatteint;
            String commentaire;
            String unite;
            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    ProEtudieBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    ProEtudieBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_PRO).split(";"));
            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureProEtudie>> mesureProduitBrutsMapByDisp = mesuresProEtudiesMap
                        .get(dispositif.getId());
                if (mesureProduitBrutsMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesureProEtudie>>> itEchan = mesureProduitBrutsMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesureProEtudie>> echanEntry = itEchan
                            .next();
                    DescriptionTraitement echantillon = echanEntry.getKey();
                    SortedMap<LocalDate, MesureProEtudie> mesureProduitBrutsMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                    for (Entry<LocalDate, MesureProEtudie> entrySet : mesureProduitBrutsMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureProEtudie mesureProEtudie = entrySet.getValue();
                        currentDispositif = mesureProEtudie.getCodedispositif();
                        currentTraitement = mesureProEtudie.getDescriptionTraitement().getCode();
                        currentParcelle = mesureProEtudie.getNomparcelle();
                        curentplacette = mesureProEtudie.getNomparcelle();
                        typepro = mesureProEtudie.getTypeproduit();
                        codepro = mesureProEtudie.getCodeproduit();
                        datefin = mesureProEtudie.getDatefin();
                        materielapport = mesureProEtudie.getMaterielapport();
                        enfouil = mesureProEtudie.getLocalDateenfouissement();
                        culture = mesureProEtudie.getNomculture();
                        bbch = mesureProEtudie.getCodebbch();
                        stade = mesureProEtudie.getPrecisionstade();
                        typeobs = mesureProEtudie.getTypeobservation();
                        observation = mesureProEtudie.getNomobservation();
                        niveauatteint = mesureProEtudie.getNiveauatteint();
                        commentaire = mesureProEtudie.getCommentaire();
                        String genericPattern = LineDataFixe(date, currentDispositif, currentTraitement, currentParcelle, curentplacette, typepro,
                                codepro, datefin, materielapport, enfouil, culture, bbch, stade, typeobs, observation, niveauatteint, commentaire);
                        out.print(genericPattern);
                        LineDataVariable(mesureProEtudie, out, listVariable);
                    }
                    itEchan.remove();
                }
            }
        } catch (final ParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentTraitement, String currentParcelle,
            String courentPlacette, String typepro,
            String codepro, LocalDate datefin, String materielapport,
            LocalDate enfouil, String culture, String bbch, String stade,
            String typeobs, String observation, String niveauatteint,
            String commentaire) throws ParseException {
        String genericPattern = String.format(PATTERB_CSV_23_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentTraitement,
                currentParcelle,
                courentPlacette, typepro, codepro,
                datefin, materielapport, enfouil,
                culture,
                bbch, stade, typeobs, observation, niveauatteint, commentaire);
        return genericPattern;
    }

    private void LineDataVariable(MesureProEtudie mesureProEtudie, PrintStream out, List<String> listVariable) {
        final List<ValeurProEtudie> valeurtravailsol = mesureProEtudie.getValeurproetudie();
        listVariable.forEach((variableColumnName) -> {
            for (Iterator<ValeurProEtudie> ValeurIterator = valeurtravailsol.iterator(); ValeurIterator.hasNext();) {
                ValeurProEtudie valeur = ValeurIterator.next();
                if (!((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
                    continue;
                }
                String line;
                line = String.format(";%s", getValeurToString(valeur));
                out.print(line);
                ValeurIterator.remove();
            }
        });
        out.println();
    }

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return ProEtudieExtractor.MAP_INDEX_PRO_ETUDIE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IITKDatatypeManager.CODE_DATATYPE_PRO_ETUDIE;
    }

    private String getValeurToString(ValeurProEtudie valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else if (valeur.getListeItineraire() != null) {
            return valeur.getListeItineraire().getListe_valeur();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(ProEtudieBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
