package org.inra.ecoinfo.pro.refdata.faitremarquable;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class FaitRemarquableDaoImpl extends AbstractJPADAO<Faitremarquable> implements IFaitRemarquableDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Faitremarquable> getAll() {
        return getAllBy(Faitremarquable.class, Faitremarquable::getCode);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Faitremarquable> getByNKey(String nom) {
        CriteriaQuery<Faitremarquable> query = builder.createQuery(Faitremarquable.class);
        Root<Faitremarquable> faitremarquable = query.from(Faitremarquable.class);
        query
                .select(faitremarquable)
                .where(
                        builder.equal(faitremarquable.get(Faitremarquable_.nom), nom)
                );
        return getOptional(query);
    }
}
