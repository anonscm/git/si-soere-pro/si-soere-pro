package org.inra.ecoinfo.pro.refdata.booleans;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class BooleanDAOImpl extends AbstractJPADAO<Booleans> implements IBooleanDAO {

    private static final String QUERY_BOOLEAN_KEY = "from  Booleans b where b.libelle = :libelle";

    /**
     *
     * @param libelle
     * @return
     */
    @Override
    public Optional<Booleans> getByNKey(String libelle) {
        CriteriaQuery<Booleans> query = builder.createQuery(Booleans.class);
        Root<Booleans> bool = query.from(Booleans.class);
        query
                .select(bool)
                .where(
                        builder.equal(bool.get(Booleans_.libelle), libelle)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<Booleans> getAll() {
        return getAllBy(Booleans.class, Booleans::getLibelle);
    }

}
