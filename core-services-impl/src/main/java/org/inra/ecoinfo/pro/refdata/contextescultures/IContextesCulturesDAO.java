/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contextescultures;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.typeculture.TypeCulture;

/**
 *
 * @author adiankha
 */
public interface IContextesCulturesDAO extends IDAO<ContextesCultures> {

    /**
     *
     * @return
     */
    List<ContextesCultures> getAll();

    /**
     *
     * @param dispositif
     * @param typeculture
     * @param densite
     * @param anneed
     * @return
     */
    Optional<ContextesCultures> getByNKey(Dispositif dispositif, TypeCulture typeculture, Integer densite, Integer anneed);

}
