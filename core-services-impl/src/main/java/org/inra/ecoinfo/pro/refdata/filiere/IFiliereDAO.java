/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.filiere;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IFiliereDAO extends IDAO<Filiere> {

    /**
     *
     * @return
     */
    List<Filiere> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Filiere> getByNKey(String nom);

}
