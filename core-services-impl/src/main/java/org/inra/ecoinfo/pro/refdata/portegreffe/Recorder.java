/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.portegreffe;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<PorteGreffe> {
    
    IPorteGreffeDAO portegreffeDAO;
    private Properties portageGreffeEn;
    
    /**
     *
     * @param portegreffe
     * @throws BusinessException
     */
    public void createPorteGreffe(PorteGreffe portegreffe) throws BusinessException {
        try {
            portegreffeDAO.saveOrUpdate(portegreffe);
            portegreffeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create portegreffe");
        }
    }
    
    private void updatePorteGreffe(final String libelle, final PorteGreffe dbportegreffe) throws BusinessException {
        try {
            dbportegreffe.setPg_libelle(libelle);
            portegreffeDAO.saveOrUpdate(dbportegreffe);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update portegreffe");
        }
    }
    
    private void createOrUpdatePorteGreffe(final String code, final String libelle, final PorteGreffe dbportegreffe) throws BusinessException {
        if (dbportegreffe == null) {
            final PorteGreffe portegreffe = new PorteGreffe(libelle);
            portegreffe.setPg_code(code);
            portegreffe.setPg_libelle(libelle);
            createPorteGreffe(portegreffe);
        } else {
            updatePorteGreffe(libelle, dbportegreffe);
        }
    }
    
    private void persistPorteGreffe(final String code, final String libelle) throws BusinessException, BusinessException {
        final PorteGreffe dbporte = portegreffeDAO.getByNKey(libelle).orElse(null);
        createOrUpdatePorteGreffe(code, libelle, dbporte);
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String nom = tokenizerValues.nextToken();
                PorteGreffe dbporte = portegreffeDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get portegreffe"));
                portegreffeDAO.remove(dbporte);
                values = parser.getLine();
            }
            
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<PorteGreffe> getAllElements() throws BusinessException {
        return portegreffeDAO.getAll();
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, PorteGreffe.NAME_ENTITY_JPA);
                final String libelle = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(libelle);
                persistPorteGreffe(code, libelle);
                values = parser.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PorteGreffe greffe) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(greffe == null || greffe.getPg_libelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : greffe.getPg_libelle(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(greffe == null || greffe.getPg_libelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : portageGreffeEn.getProperty(greffe.getPg_libelle()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<PorteGreffe> initModelGridMetadata() {
        portageGreffeEn = localizationManager.newProperties(PorteGreffe.NAME_ENTITY_JPA, PorteGreffe.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    /**
     *
     * @param portegreffeDAO
     */
    public void setPortegreffeDAO(IPorteGreffeDAO portegreffeDAO) {
        this.portegreffeDAO = portegreffeDAO;
    }
    
}
