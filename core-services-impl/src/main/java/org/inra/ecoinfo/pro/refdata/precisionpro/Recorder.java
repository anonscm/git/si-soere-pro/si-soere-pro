/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpro;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Precisionpro> {

    IPrecisionProDAO precisionproDAO;
    Properties NonprecisionEN;

    /**
     *
     * @param precisionpro
     * @throws BusinessException
     */
    public void createPrecisionPro(Precisionpro precisionpro) throws BusinessException {
        try {
            precisionproDAO.saveOrUpdate(precisionpro);
            precisionproDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create precisionpro");
        }
    }

    private void updatePrecisionpro(final String nom, final Precisionpro dbprecisionpro) throws BusinessException {
        try {
            dbprecisionpro.setP_nom(nom);
            precisionproDAO.saveOrUpdate(dbprecisionpro);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update precisionpro");
        }

    }

    private void createOrUpdatePrecisionpro(final String code, String nom, final Precisionpro dbprecisionpro) throws BusinessException {
        if (dbprecisionpro == null) {
            final Precisionpro precisionpro = new Precisionpro(nom);
            precisionpro.setP_nom(nom);
            precisionpro.setP_code(code);

            createPrecisionPro(precisionpro);
        } else {
            updatePrecisionpro(nom, dbprecisionpro);
        }
    }

    private void persistPrecisionpro(final String code, final String nom) throws BusinessException, BusinessException {
        final Precisionpro dbprecisionpro = precisionproDAO.getByNKey(nom).orElse(null);
        createOrUpdatePrecisionpro(code, nom, dbprecisionpro);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();
                final Precisionpro dbprecisionpro = precisionproDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get precisionpro"));
                if (dbprecisionpro != null) {
                    precisionproDAO.remove(dbprecisionpro);
                    values = csvp.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Precisionpro> getAllElements() throws BusinessException {
        return precisionproDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Precisionpro.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistPrecisionpro(code, nom);
                values = csvp.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Precisionpro precisionpro) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(precisionpro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : precisionpro.getP_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(precisionpro == null || precisionpro.getP_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : NonprecisionEN.getProperty(precisionpro.getP_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IPrecisionProDAO getPrecisionproDAO() {
        return precisionproDAO;
    }

    /**
     *
     * @param precisionproDAO
     */
    public void setPrecisionproDAO(IPrecisionProDAO precisionproDAO) {
        this.precisionproDAO = precisionproDAO;
    }

    @Override
    protected ModelGridMetadata<Precisionpro> initModelGridMetadata() {
        NonprecisionEN = localizationManager.newProperties(Precisionpro.NAME_ENTITY_JPA, Precisionpro.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}
