/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.IMesurePhysicoChimieDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols_;

/**
 *
 * @author adiankha
 */
public class JPAMesurePhysicoChimieDAO extends AbstractJPADAO<MesurePhysicoChimieSols> implements IMesurePhysicoChimieDAO<MesurePhysicoChimieSols> {

    @Override
    public Optional<MesurePhysicoChimieSols> getByNKeys(String nomlabo, LocalDate dateprelevement) {
        CriteriaQuery<MesurePhysicoChimieSols> query = builder.createQuery(MesurePhysicoChimieSols.class);
        Root<MesurePhysicoChimieSols> m = query.from(MesurePhysicoChimieSols.class);
        query
                .where(builder.equal(m.get(MesurePhysicoChimieSols_.localDatePrelevement), dateprelevement),
                        builder.equal(m.get(MesurePhysicoChimieSols_.nom_laboratoire), nomlabo)
                );
        return getOptional(query);
    }

    @Override
    public Optional<MesurePhysicoChimieSols> getByKeys(String keymesure) {
        CriteriaQuery<MesurePhysicoChimieSols> query = builder.createQuery(MesurePhysicoChimieSols.class);
        Root<MesurePhysicoChimieSols> m = query.from(MesurePhysicoChimieSols.class);
        query
                .where(builder.equal(m.get(MesurePhysicoChimieSols_.keymesure), keymesure)
                );
        return getOptional(query);
    }
}
