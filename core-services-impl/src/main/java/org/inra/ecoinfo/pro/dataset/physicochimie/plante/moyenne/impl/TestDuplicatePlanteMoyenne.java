/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;

/**
 *
 * @author adiankha
 */
public class TestDuplicatePlanteMoyenne extends AbstractTestDuplicate {

    private static final String BUNDLE_PATH_PLANTE_MOY = "org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.messages";
    static final String PROPERTY_MSG_DUPLICATE_LINE_PLANTEMOY = "PROPERTY_MSG_DUPLICATE_LINE_PLANTEMOY";

    SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine = null;
    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    /**
     *
     */
    public TestDuplicatePlanteMoyenne() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    /**
     *
     * @param date
     * @param codeTrait
     * @param culture
     * @param partie
     * @param codevariable
     * @param methode
     * @param lineNumber
     */
    protected void addLine(final String date, String codeTrait, final String culture, String partie,
            final String codevariable, String methode, final long lineNumber) {
        final String key = this.getKey(date, codeTrait, culture, codevariable, methode);
        final String keySol = this.getKey(date, codeTrait, codevariable, methode);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, partie);
            } else if (!this.lineCouvert.get(keySol).equals(partie)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicatePlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                        TestDuplicatePlanteMoyenne.PROPERTY_MSG_DUPLICATE_LINE_PLANTEMOY), lineNumber,
                        date, codeTrait, codevariable, methode, this.lineCouvert.get(keySol), codeTrait));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicatePlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    TestDuplicatePlanteMoyenne.PROPERTY_MSG_DUPLICATE_LINE_PLANTEMOY), lineNumber, date, codeTrait, culture, partie, codevariable,
                    methode, this.line.get(key)));

        }
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], values[3], values[7], values[10], lineNumber);
    }
}
