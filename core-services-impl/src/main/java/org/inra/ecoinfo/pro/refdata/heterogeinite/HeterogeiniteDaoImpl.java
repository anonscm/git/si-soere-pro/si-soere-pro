package org.inra.ecoinfo.pro.refdata.heterogeinite;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class HeterogeiniteDaoImpl extends AbstractJPADAO<Heterogeinite> implements IHeterogeiniteDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Heterogeinite> getAll() {
        return getAll(Heterogeinite.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Heterogeinite> getByNKey(String nom) {
        CriteriaQuery<Heterogeinite> query = builder.createQuery(Heterogeinite.class);
        Root<Heterogeinite> heterogeinite = query.from(Heterogeinite.class);
        query
                .select(heterogeinite)
                .where(
                        builder.equal(heterogeinite.get(Heterogeinite_.code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }
}
