/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.moyenne.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePROMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.jpa.JPAPhysicoChimieDAO;
import org.inra.ecoinfo.pro.synthesis.physicochimiepromoy.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPAProduitMoyenneDAO extends JPAPhysicoChimieDAO<MesurePhysicoChimiePROMoy, ValeurPhysicoChimiePROMoy> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurPhysicoChimiePROMoy> getValeurPhysicoChimieClass() {
        return ValeurPhysicoChimiePROMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesurePhysicoChimiePROMoy> getMesurePhysicoChimieClass() {
        return MesurePhysicoChimiePROMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurPhysicoChimiePROMoy, MesurePhysicoChimiePROMoy> getMesureAttribute() {
        return ValeurPhysicoChimiePROMoy_.mesurePhysicoChimiePROMoy;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IPhysicoChimiePROMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO_MOY;
    }

}
