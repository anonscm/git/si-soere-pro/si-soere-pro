package org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere.CaracteristiqueMatierePremieres;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.CaracteristiqueValeur;

/**
 *
 * @author ptcherniati
 */
public interface IValeurCaracteristiqueMPDAO extends IDAO<ValeurCaracteristiqueMP> {

    /**
     *
     * @return
     */
    List<ValeurCaracteristiqueMP> getAll();

    /**
     *
     * @param cmp
     * @param cv
     * @return
     */
    Optional<ValeurCaracteristiqueMP> getByNKey(CaracteristiqueMatierePremieres cmp, CaracteristiqueValeur cv);

    /**
     *
     * @param type
     * @param nom
     * @param valeur
     * @return
     */
    Optional<ValeurCaracteristiqueMP> getByNKey(String type, String nom, String valeur);

}
