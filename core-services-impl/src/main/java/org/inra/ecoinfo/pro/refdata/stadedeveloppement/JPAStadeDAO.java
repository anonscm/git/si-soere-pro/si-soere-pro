/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.stadedeveloppement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAStadeDAO extends AbstractJPADAO<StadeDeveloppement> implements IStadeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<StadeDeveloppement> getAll() {
        return getAll(StadeDeveloppement.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<StadeDeveloppement> getByName(String nom) {
        CriteriaQuery<StadeDeveloppement> query = builder.createQuery(StadeDeveloppement.class);
        Root<StadeDeveloppement> stadeDeveloppement = query.from(StadeDeveloppement.class);
        query
                .select(stadeDeveloppement)
                .where(
                        builder.equal(stadeDeveloppement.get(StadeDeveloppement_.mycode), Utils.createCodeFromString(nom))
                );
        return getOptional(query);

    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<StadeDeveloppement> getByNKey(String codebbch) {
        CriteriaQuery<StadeDeveloppement> query = builder.createQuery(StadeDeveloppement.class);
        Root<StadeDeveloppement> stadeDeveloppement = query.from(StadeDeveloppement.class);
        query
                .select(stadeDeveloppement)
                .where(
                        builder.equal(stadeDeveloppement.get(StadeDeveloppement_.codebbch), Utils.createCodeFromString(codebbch))
                );
        return getOptional(query);
    }

}
