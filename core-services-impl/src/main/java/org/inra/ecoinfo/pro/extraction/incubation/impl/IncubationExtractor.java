/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.impl;

import java.util.Collection;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */

public class IncubationExtractor extends MO implements IExtractor{

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults" ;

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_INCUBATION_SOL_CODE = "extractionResultIncubationSol";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_CODE = "extractionResultIncubationSolPro" ;

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_INCUBATION_SOL_MOY_CODE = "extractionIncubationSolMoy" ;

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_MOY_CODE = "extractionResultIncubationSolProMoy" ;

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.incubation.incubation-messages";

    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";

    IExtractor incubationSolExtractor;
    IExtractor incubationSolProExtractor;
    IExtractor incubationSolMoyExtractor;
    IExtractor incubationSolProMoyExtractor;

    
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        int extractionResult = 4;
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(IncubationParameterVO.INCUBATIONSOL)))
                    .isEmpty()) {
                this.incubationSolExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(IncubationParameterVO.INCUBATIONSOLPRO)))
                    .isEmpty()) {
                this.incubationSolProExtractor.extract(parameters);
            } else {
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
       
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(IncubationParameterVO.INCUBATIONSOLMOY)))
                    .isEmpty()) {
                this.incubationSolMoyExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(IncubationParameterVO.INCUBATIONSOLPROMOY)))
                    .isEmpty()) {
                this.incubationSolProMoyExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
       
        if (extractionResult == 0) {
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    IncubationExtractor.BUNDLE_SOURCE_PATH, this.localizationManager.getMessage(
                            IncubationExtractor.BUNDLE_SOURCE_PATH,
                            IncubationExtractor.MSG_EXTRACTION_ABORTED))), Notification.ERROR,
                    IncubationExtractor.PROPERTY_MSG_BADS_RIGHTS, (Utilisateur) this.policyManager
                    .getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    IncubationExtractor.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
    }

    @Override
    public void setExtraction(Extraction extraction) {
       
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = -1L;
        size = +incubationSolExtractor.getExtractionSize(parameters);
        size = +incubationSolProExtractor.getExtractionSize(parameters);
        size = +incubationSolMoyExtractor.getExtractionSize(parameters);
        size = +incubationSolProMoyExtractor.getExtractionSize(parameters);
        return size;}

    /**
     *
     * @param incubationSolExtractor
     */
    public void setIncubationSolExtractor(IExtractor incubationSolExtractor) {
        this.incubationSolExtractor = incubationSolExtractor;
    }

    /**
     *
     * @param incubationSolProExtractor
     */
    public void setIncubationSolProExtractor(IExtractor incubationSolProExtractor) {
        this.incubationSolProExtractor = incubationSolProExtractor;
    }

    /**
     *
     * @param incubationSolMoyExtractor
     */
    public void setIncubationSolMoyExtractor(IExtractor incubationSolMoyExtractor) {
        this.incubationSolMoyExtractor = incubationSolMoyExtractor;
    }

    /**
     *
     * @param incubationSolProMoyExtractor
     */
    public void setIncubationSolProMoyExtractor(IExtractor incubationSolProMoyExtractor) {
        this.incubationSolProMoyExtractor = incubationSolProMoyExtractor;
    }

    /**
     *
     * @return
     */
    public IExtractor getIncubationSolExtractor() {
        return incubationSolExtractor;
    }

    /**
     *
     * @return
     */
    public IExtractor getIncubationSolProExtractor() {
        return incubationSolProExtractor;
    }

    /**
     *
     * @return
     */
    public IExtractor getIncubationSolMoyExtractor() {
        return incubationSolMoyExtractor;
    }

    /**
     *
     * @return
     */
    public IExtractor getIncubationSolProMoyExtractor() {
        return incubationSolProMoyExtractor;
    }
    
}
