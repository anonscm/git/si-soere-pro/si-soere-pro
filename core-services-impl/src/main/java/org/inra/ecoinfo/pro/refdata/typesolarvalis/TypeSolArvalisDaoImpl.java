package org.inra.ecoinfo.pro.refdata.typesolarvalis;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class TypeSolArvalisDaoImpl extends AbstractJPADAO<Typesolarvalis> implements ITypeSolArvalisDAO{

    /**
     *
     * @return
     */
    @Override
    public List<Typesolarvalis> getAll()  {
        return getAll(Typesolarvalis.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Typesolarvalis> getByNKey(String nom) {
        CriteriaQuery<Typesolarvalis> query = builder.createQuery(Typesolarvalis.class);
        Root<Typesolarvalis> typesolarvalis = query.from(Typesolarvalis.class);
        query
                .select(typesolarvalis)
                .where(
                        builder.equal(typesolarvalis.get(Typesolarvalis_.nom), nom)
                );
        return getOptional(query);
    }

}
