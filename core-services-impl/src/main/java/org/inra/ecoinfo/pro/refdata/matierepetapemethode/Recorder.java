package org.inra.ecoinfo.pro.refdata.matierepetapemethode;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.etape.IEtapeDAO;
import org.inra.ecoinfo.pro.refdata.melange.IMelangeDAO;
import org.inra.ecoinfo.pro.refdata.methodeetape.IMethodeEtapeDAO;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.procedemethodeprodmp.IProMPProcedeDAO;
import org.inra.ecoinfo.pro.refdata.procedemethodeprodmp.ProcedeMethodeProdMp;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<ProcedeMethodeEtapeMP> {

    private static final String PROPERTY_MSG_PMPP_BAD_METAPE = "PROPERTY_MSG_PMPP_BAD_METAPE";

    private static final String PROPERTY_MSG_PMPP_BAD_ETAPE = "PROPERTY_MSG_PMPP_BAD_ETAPE";

    private static final String PROPERTY_MSG_PMPP_BAD_PROD_MP_PROCESS = "PROPERTY_MSG_PMPP_BAD_PROD_MP_PROCESS";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected IProMPProcedeDAO prompprocedeDAO;

    /**
     *
     */
    protected IEtapeDAO etapeDAO;

    /**
     *
     */
    protected IMethodeEtapeDAO methodeetapeDAO;

    /**
     *
     */
    protected IMPProcedeMethodeEtapeDAO mpeDAO;
    IMelangeDAO melangeDAO;

    private String[] listeProduitsPossibles;
    private ConcurrentMap<String, String[]> listeComposantPossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listePourcentagePossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeProcedePossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeOrdreProcedesPossibles = new ConcurrentHashMap<>();

    private String[] listeEtapesPossibles;

    private String[] listeMEtapesPossibles;

    Properties sammaryEn;

    private void createMPEM(final ProcedeMethodeEtapeMP mpe) throws BusinessException {
        try {
            mpeDAO.saveOrUpdate(mpe);
            mpeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create ProcedeMethodeEtapeMP");
        }
    }

    private void createOrUpdatempem(final ProcedeMethodeProdMp pmp, MethodeEtapes me, Etapes etapes, int ordre, String sammary, final ProcedeMethodeEtapeMP dbmpe) throws BusinessException {
        if (dbmpe == null) {
            ProcedeMethodeEtapeMP mpe = new ProcedeMethodeEtapeMP(pmp, etapes, me, ordre, sammary);
            mpe.setProcedemethodepropm(pmp);
            mpe.setMethodeetapes(me);
            mpe.setEtapes(etapes);
            mpe.setOrdre(ordre);
            mpe.setCommentaire(sammary);
            createMPEM(mpe);
        } else {
            UpdateMPE(pmp, me, etapes, ordre, sammary, dbmpe);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final String codeProduit = values[0];
                final String nomMatierePremiere = values[1];
                final double pourcentage = Double.parseDouble(values[2]);
                final String intituleProcede = values[3];
                final double ordreProcede = Integer.parseInt(values[4]);
                final String intituleetape = values[5];
                final String methodeetape = values[6];
                int ordre = Integer.parseInt(values[7]);
                ProcedeMethodeProdMp dbmpp = prompprocedeDAO.getByNKey(codeProduit, nomMatierePremiere, pourcentage, intituleProcede, ordreProcede)
                        .orElseThrow(() -> new BusinessException("can't find ProcedeMethodeEtapeMP"));

                Etapes etapes = etapeDAO.getByNKey(intituleetape)
                        .orElseThrow(() -> new BusinessException("can't find etapes"));
                MethodeEtapes methodeetapes = methodeetapeDAO.getByNKey(methodeetape)
                        .orElseThrow(() -> new BusinessException("can't find methodeEtape"));
                ProcedeMethodeEtapeMP mpe = mpeDAO.getByNKey(dbmpp, etapes, methodeetapes, ordre)
                        .orElseThrow(() -> new BusinessException("can't find ProcedeMethodeProdMp"));
                mpeDAO.remove(mpe);
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    private void etapesPossibles() {
        List<Etapes> groupeetapes = etapeDAO.getAll(Etapes.class);
        String[] Listeetapes = new String[groupeetapes.size() + 1];
        Listeetapes[0] = "";
        int index = 1;
        for (Etapes etapes : groupeetapes) {
            Listeetapes[index++] = etapes.getEtape_intitule();
        }
        this.listeEtapesPossibles = Listeetapes;
    }

    @Override
    protected List<ProcedeMethodeEtapeMP> getAllElements() throws BusinessException {

        return mpeDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IEtapeDAO getEtapeDAO() {
        return etapeDAO;
    }

    /**
     *
     * @return
     */
    public IMethodeEtapeDAO getMethodeetapeDAO() {
        return methodeetapeDAO;
    }

    /**
     *
     * @return
     */
    public IMPProcedeMethodeEtapeDAO getMpeDAO() {
        return mpeDAO;
    }

    public void initNewLine(LineModelGridMetadata lineModelGridMetadata, ProcedeMethodeEtapeMP procedeMethodeEtapeMP) {
        String codeProduit = Optional.ofNullable(procedeMethodeEtapeMP)
                .map(pmeMp -> pmeMp.getProcedemethodepropm())
                .map(pmpM -> pmpM.getMelange())
                .map(mel -> mel.getProduits())
                .map(pro -> pro.getCodecomposant())
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata columnProduit = new ColumnModelGridMetadata(codeProduit, listeProduitsPossibles, null, true, false, true);

        String codeComposant = Optional.ofNullable(procedeMethodeEtapeMP)
                .map(pmeMp -> pmeMp.getProcedemethodepropm())
                .map(pmpM -> pmpM.getMelange())
                .map(mel -> mel.getComposant())
                .map(pro -> pro.getCodecomposant())
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata columnComposant = new ColumnModelGridMetadata(codeComposant, listeComposantPossibles, null, true, false, true);

        String percent = Optional.ofNullable(procedeMethodeEtapeMP)
                .map(pmeMp -> pmeMp.getProcedemethodepropm())
                .map(pmpM -> pmpM.getMelange())
                .map(mel -> mel.getPourcentage())
                .map(per -> Double.toString(per))
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata columnPercent = new ColumnModelGridMetadata(percent, listePourcentagePossibles, null, true, false, true);

        String procede = Optional.ofNullable(procedeMethodeEtapeMP)
                .map(pmeMp -> pmeMp.getProcedemethodepropm())
                .map(pmpM -> pmpM.getProcess())
                .map(proc -> proc.getProcess_intitule())
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata columnProcessCode = new ColumnModelGridMetadata(procede, listeProcedePossibles, null, true, false, true);

        String ordre = Optional.ofNullable(procedeMethodeEtapeMP)
                .map(pmeMp -> pmeMp.getProcedemethodepropm())
                .map(pmpM -> pmpM.getOdreprocede())
                .map(ord -> Integer.toString(ord))
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata columnOrdreProcess = new ColumnModelGridMetadata(ordre, listeOrdreProcedesPossibles, null, true, false, true);

        Deque<ColumnModelGridMetadata> columnsToBeLinked = new LinkedList();
        columnsToBeLinked.add(columnProduit);
        columnsToBeLinked.add(columnComposant);
        columnsToBeLinked.add(columnPercent);
        columnsToBeLinked.add(columnProcessCode);
        columnsToBeLinked.add(columnOrdreProcess);
        setColumnRefRecursive(columnsToBeLinked);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnProduit);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnComposant);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPercent);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnProcessCode);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnOrdreProcess);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProcedeMethodeEtapeMP mpem) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        initNewLine(lineModelGridMetadata, mpem);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mpem == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : mpem.getEtapes() != null ? mpem.getEtapes().getEtape_intitule() : "", listeEtapesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mpem == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : mpem.getMethodeetapes() != null
                        ? mpem.getMethodeetapes().getMe_nom() : "", listeMEtapesPossibles, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mpem == null || mpem.getOrdre() == 0 ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : mpem.getOrdre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mpem == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : mpem.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mpem == null || mpem.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : sammaryEn.getProperty(mpem.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProMPProcedeDAO getPrompprocedeDAO() {
        return prompprocedeDAO;
    }

    @Override
    protected ModelGridMetadata<ProcedeMethodeEtapeMP> initModelGridMetadata() {
        initMPCARPROCESSPossibles();
        etapesPossibles();
        methodeEtapesPossibles();
        sammaryEn = localizationManager.newProperties(ProcedeMethodeEtapeMP.NAME_ENTITY_JPA, ProcedeMethodeEtapeMP.JPA_COLUMN_SAMMERY, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void initMPCARPROCESSPossibles() {
        Map<String, Map<String, Map<String, Map<String, List<String>>>>> valeursMap = new HashMap();
        prompprocedeDAO.getAll(ProcedeMethodeProdMp.class)
                .stream()
                .forEach(pmpMp
                        -> valeursMap
                        .computeIfAbsent(pmpMp.getMelange().getProduits().getCodecomposant(), k -> new HashMap<>())
                        .computeIfAbsent(pmpMp.getMelange().getComposant().getCodecomposant(), k -> new HashMap<>())
                        .computeIfAbsent(Double.toString(pmpMp.getMelange().getPourcentage()), k -> new HashMap<>())
                        .computeIfAbsent(pmpMp.getProcess().getProcess_intitule(), k -> new LinkedList<>())
                        .add(Integer.toString(pmpMp.getOdreprocede()))
                );
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeComposantPossibles);
        listOfMapOfValuesPossibles.add(listePourcentagePossibles);
        listOfMapOfValuesPossibles.add(listeProcedePossibles);
        listOfMapOfValuesPossibles.add(listeOrdreProcedesPossibles);
        this.listeProduitsPossibles = readMapOfValuesPossibles(valeursMap, listOfMapOfValuesPossibles, new LinkedList<String>());
    }

    private void methodeEtapesPossibles() {
        List<MethodeEtapes> groupemetapes = methodeetapeDAO.getAll(MethodeEtapes.class);
        String[] Listemetapes = new String[groupemetapes.size() + 1];
        Listemetapes[0] = "";
        int index = 1;
        for (MethodeEtapes metapes : groupemetapes) {
            Listemetapes[index++] = metapes.getMe_nom();
        }
        this.listeMEtapesPossibles = Listemetapes;
    }

    private void persistMPE(ProcedeMethodeProdMp mpp, Etapes etapes, MethodeEtapes me, int ordre, String sammary) throws BusinessException, BusinessException {
        final ProcedeMethodeEtapeMP dbmpe = mpeDAO.getByNKey(mpp, etapes, me, ordre).orElse(null);
        createOrUpdatempem(mpp, me, etapes, ordre, sammary, dbmpe);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        long line = 0;
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProcedeMethodeEtapeMP.NAME_ENTITY_JPA);
                int indexcode = tokenizerValues.currentTokenIndex();
                final String codeProduit = tokenizerValues.nextToken();
                int indexnom = tokenizerValues.currentTokenIndex();
                final String nomMatierePremiere = tokenizerValues.nextToken();
                final double pourcentage = verifieDouble(tokenizerValues, line, true, errorsReport);
                int indexprocess = tokenizerValues.currentTokenIndex();
                final String intituleProcede = tokenizerValues.nextToken();
                final double ordreProcede = verifieInt(tokenizerValues, line, true, errorsReport);
                int indexetape = tokenizerValues.currentTokenIndex();
                final String intituleetape = tokenizerValues.nextToken();
                int indexme = tokenizerValues.currentTokenIndex();
                final String methodeEtape = tokenizerValues.nextToken();
                int ordreEtape = verifieInt(tokenizerValues, line, true, errorsReport);
                String commentaire = tokenizerValues.nextToken();

                ProcedeMethodeProdMp dbmpp = prompprocedeDAO.getByNKey(codeProduit, nomMatierePremiere, pourcentage, intituleProcede, ordreProcede).orElse(null);
                if (dbmpp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PMPP_BAD_PROD_MP_PROCESS), line,
                            codeProduit, nomMatierePremiere, pourcentage, intituleProcede, ordreProcede));

                }
                Etapes etapes = etapeDAO.getByNKey(intituleetape).orElse(null);
                if (etapes == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PMPP_BAD_ETAPE), line, indexetape, intituleetape));

                }
                MethodeEtapes methodeetapes = methodeetapeDAO.getByNKey(methodeEtape).orElse(null);
                if (methodeetapes == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PMPP_BAD_METAPE), line, indexme, methodeEtape));

                }
                if (!errorsReport.hasErrors()) {
                    persistMPE(dbmpp, etapes, methodeetapes, ordreEtape, commentaire);
                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param etapeDAO
     */
    public void setEtapeDAO(IEtapeDAO etapeDAO) {
        this.etapeDAO = etapeDAO;
    }

    /**
     *
     * @param methodeetapeDAO
     */
    public void setMethodeetapeDAO(IMethodeEtapeDAO methodeetapeDAO) {
        this.methodeetapeDAO = methodeetapeDAO;
    }

    /**
     *
     * @param mpeDAO
     */
    public void setMpeDAO(IMPProcedeMethodeEtapeDAO mpeDAO) {
        this.mpeDAO = mpeDAO;
    }

    /**
     *
     * @param prompprocedeDAO
     */
    public void setPrompprocedeDAO(IProMPProcedeDAO prompprocedeDAO) {
        this.prompprocedeDAO = prompprocedeDAO;
    }

    private void UpdateMPE(final ProcedeMethodeProdMp pmp, MethodeEtapes me, Etapes etapes, int ordre, String sammary, final ProcedeMethodeEtapeMP dbmpe) throws BusinessException {
        try {
            dbmpe.setProcedemethodepropm(pmp);
            dbmpe.setEtapes(etapes);
            dbmpe.setMethodeetapes(me);
            dbmpe.setOrdre(ordre);
            dbmpe.setCommentaire(sammary);
            mpeDAO.saveOrUpdate(dbmpe);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update ProcedeMethodeEtapeMP");
        }
    }

}
