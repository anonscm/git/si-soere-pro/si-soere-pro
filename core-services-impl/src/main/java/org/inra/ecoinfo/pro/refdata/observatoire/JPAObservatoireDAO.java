/**
 *
 */
package org.inra.ecoinfo.pro.refdata.observatoire;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 * @author sophie
 *
 */
public class JPAObservatoireDAO extends AbstractJPADAO<Observatoire> implements IObservatoireDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.observatoire.IObservatoireDAO#getAll()
     */
    /**
     *
     * @return
     */
    @Override
    public List<Observatoire> getAll() {
        return getAllBy(Observatoire.class, Observatoire::getCode);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.observatoire.IObservatoireDAO#getByCode(java.lang.String)
     */
    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<Observatoire> getByNKey(String code) {
        CriteriaQuery<Observatoire> query = builder.createQuery(Observatoire.class);
        Root<Observatoire> observatoire = query.from(Observatoire.class);
        query
                .select(observatoire)
                .where(
                        builder.equal(observatoire.get(Observatoire_.code), Utils.createCodeFromString(code))
                );
        return getOptional(query);
    }

}
