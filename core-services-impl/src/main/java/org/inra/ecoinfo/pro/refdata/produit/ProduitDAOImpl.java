/*
 *
 */
package org.inra.ecoinfo.pro.refdata.produit;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

// TODO: Auto-generated Javadoc
/**
 * The Class ProduitDAOImpl.
 */
public class ProduitDAOImpl extends AbstractJPADAO<Produits> implements IProduitDAO {

    @Override
    public List<Produits> getAll() {
        return getAllBy(Produits.class, Produits::getCodecomposant);
    }

    @Override
    public Optional<Produits> getByNKey(String codeComposant) {
        CriteriaQuery<Produits> query = builder.createQuery(Produits.class);
        Root<Produits> produits = query.from(Produits.class);
        query
                .select(produits)
                .where(
                        builder.equal(produits.get(Produits_.codecomposant), codeComposant)
                );
        return getOptional(query);
    }

    @Override
    public Optional<Produits> getByNKey(String codeuser, String noDepartement, int an, String detenteur) {
        CriteriaQuery<Produits> query = builder.createQuery(Produits.class);
        Root<Produits> produits = query.from(Produits.class);
        query
                .select(produits)
                .where(
                        builder.equal(produits.get(Produits_.prod_codeuser), codeuser),
                        builder.equal(produits.get(Produits_.prod_lieu), noDepartement),
                        builder.equal(produits.get(Produits_.prod_annee), an),
                        builder.equal(produits.get(Produits_.stc_Detenteur), detenteur)
                );
        return getOptional(query);
    }

}
