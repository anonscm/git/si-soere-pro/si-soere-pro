/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.cst;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class ConstantPROExtraction {

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String BUNDLE_NAME = "org.inra.ecoinfo.pro.extraction.cst";

    /**
     *
     */
    public static final Logger LOGGER_FOR_EXTRACTION = LoggerFactory.getLogger(ConstantPROExtraction.class);

    /**
     * The Constant RESOURCE_BUNDLE @link(ResourceBundle).
     */
    static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(ConstantPROExtraction.BUNDLE_NAME);

    /**
     * Gets the string.
     *
     * @param key the key
     * @return the string
     */
    public static String getString(final String key) {
        try {
            return ConstantPROExtraction.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    /**
     * Instantiates a new constant acbb extraction.
     */
    ConstantPROExtraction() {
    }

}
