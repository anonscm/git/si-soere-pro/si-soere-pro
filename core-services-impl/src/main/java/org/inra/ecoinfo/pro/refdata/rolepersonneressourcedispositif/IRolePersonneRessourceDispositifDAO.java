package org.inra.ecoinfo.pro.refdata.rolepersonneressourcedispositif;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.personneressource.PersonneRessource;
import org.inra.ecoinfo.pro.refdata.rolepersonneressource.RolePersonneRessource;

/**
 * @author sophie
 *
 */
public interface IRolePersonneRessourceDispositifDAO extends IDAO<RolePersonneRessourceDispositif> {

    /**
     *
     * @param personneRessource
     * @param dispositif
     * @param role
     * @return
     */
    public Optional<RolePersonneRessourceDispositif> getByPersonneRessourceDispositifRole(PersonneRessource personneRessource, Dispositif dispositif,
            RolePersonneRessource role);

}
