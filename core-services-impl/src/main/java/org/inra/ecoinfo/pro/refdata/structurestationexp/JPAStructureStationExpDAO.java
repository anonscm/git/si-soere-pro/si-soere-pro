/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structurestationexp;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.StationExperimentale;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * @author sophie
 *
 */
public class JPAStructureStationExpDAO extends AbstractJPADAO<StructureStationExperimentale> implements IStructureStationExpDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.structuresationexp.IStructureStationExpDAO #getByStructureStationExp(Structure, StationExperimentale)
     */

    /**
     *
     * @param structure
     * @param stationExperimentale
     * @return
     */

    @Override
    public Optional<StructureStationExperimentale> getByNKey(Structure structure, StationExperimentale stationExperimentale) {
        CriteriaQuery<StructureStationExperimentale> query = builder.createQuery(StructureStationExperimentale.class);
        Root<StructureStationExperimentale> structureStationExperimentale = query.from(StructureStationExperimentale.class);
        Join<StructureStationExperimentale, StationExperimentale> se = structureStationExperimentale.join(StructureStationExperimentale_.stationExperimentale);
        Join<StructureStationExperimentale, Structure> str = structureStationExperimentale.join(StructureStationExperimentale_.structure);
        query
                .select(structureStationExperimentale)
                .where(
                        builder.equal(str, structure),
                        builder.equal(se, stationExperimentale)
                );
        return getOptional(query);
    }

}
