package org.inra.ecoinfo.pro.refdata.environnement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IEnvironnementDAO extends IDAO<Environnement> {

    /**
     *
     * @return
     */
    List<Environnement> getAll();

    /**
     *
     * @param axe
     * @param agglo
     * @param vent
     * @return
     */
    Optional<Environnement> getByNKey(double axe, double agglo, String vent);

}
