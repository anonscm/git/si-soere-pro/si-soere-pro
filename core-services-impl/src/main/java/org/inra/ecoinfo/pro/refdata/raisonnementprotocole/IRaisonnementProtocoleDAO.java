package org.inra.ecoinfo.pro.refdata.raisonnementprotocole;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.raisonnement.Raisonnement;


/**
 * @author sophie
 * 
 */
public interface IRaisonnementProtocoleDAO extends IDAO<RaisonnementProtocole> {

    /**
     *
     * @param raisonnement
     * @param protocole
     * @return
     */
    public Optional<RaisonnementProtocole> getByNKey(Raisonnement raisonnement, Protocole protocole);

}
