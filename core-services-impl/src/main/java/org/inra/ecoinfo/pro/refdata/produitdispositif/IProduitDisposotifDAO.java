package org.inra.ecoinfo.pro.refdata.produitdispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author adiankha
 *
 */
public interface IProduitDisposotifDAO extends IDAO<ProduitDispositif> {

    /**
     *
     * @return
     */
    List<ProduitDispositif> getAll();

    /**
     *
     * @param produits
     * @param dispositif
     * @return
     */
    Optional<ProduitDispositif> getByNKey(Produits produits, Dispositif dispositif);

    /**
     *
     * @param nkey
     * @return
     */
    Optional<ProduitDispositif> getByNKey(String nkey);

}
