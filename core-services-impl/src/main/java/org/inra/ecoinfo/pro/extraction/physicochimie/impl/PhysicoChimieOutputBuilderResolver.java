/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.config.IExtractionConfiguration;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.extraction.physicochimie.plante.elementaire.impl.PlanteBrutExtractor;
import org.inra.ecoinfo.pro.extraction.physicochimie.plante.moyenne.impl.PlanteMoyenneExtractor;
import org.inra.ecoinfo.pro.extraction.physicochimie.produit.elementaire.impl.ProduitBrutExtractor;
import org.inra.ecoinfo.pro.extraction.physicochimie.produit.moyenne.impl.ProduitMoyenneExtractor;
import org.inra.ecoinfo.pro.extraction.physicochimie.sol.elementaire.impl.SolBrutExtractor;
import org.inra.ecoinfo.pro.extraction.physicochimie.sol.moyenne.impl.SolMoyenneExtractor;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class PhysicoChimieOutputBuilderResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver{

    IOutputBuilder produitBrutOutputBuilder;

    IOutputBuilder produitMoyenneOutputBuilder;

    IOutputBuilder solBrutOutputBuilder;

    IOutputBuilder solMoyenneOutputBuilder;

    IOutputBuilder planteBrutOutputBuilder;

    IOutputBuilder planteMoyenneOutputBuilder;

    IOutputBuilder requestReminderOutputBuilder;

    /**
     *
     * @param condition
     * @param outputBuilder
     * @param outputsBuilders
     */
    protected void addOutputBuilderByCondition(final Boolean condition,
            final IOutputBuilder outputBuilder, final List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }

    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        final List<IOutputBuilder> outputsBuilders = new LinkedList();
        final Map<String, Map<String, List>> results = (Map<String, Map<String, List>>) metadatasMap
                .get(PhysicoChimieExtractor.CST_RESULTS);
        this.addOutputBuilderByCondition(
                results.get(ProduitBrutExtractor.CST_RESULT_EXTRACTION_PRODUITBRUT_CODE) != null,
                this.produitBrutOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(ProduitMoyenneExtractor.CST_RESULT_EXTRACTION_PRODUITMOY_CODE) != null,
                this.produitMoyenneOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(SolBrutExtractor.CST_RESULT_EXTRACTION_SOLBRUT_CODE) != null,
                this.solBrutOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(SolMoyenneExtractor.CST_RESULT_EXTRACTION_SOLMOYENNE_CODE) != null,
                this.solMoyenneOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(PlanteBrutExtractor.CST_RESULT_EXTRACTION_PLANTEBRUT_CODE) != null,
                this.planteBrutOutputBuilder, outputsBuilders);
        this.addOutputBuilderByCondition(
                results.get(PlanteMoyenneExtractor.CST_RESULT_EXTRACTION_PLANTEMOYENNE_CODE) != null,
                this.planteMoyenneOutputBuilder, outputsBuilders);
        outputsBuilders.add(this.requestReminderOutputBuilder);
        return outputsBuilders;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream = super.buildOutput(parameters);
        try {
            parameters.getParameters().put(PhysicoChimieExtractor.CST_RESULTS, parameters.getResults());
            final List<IOutputBuilder> roBuildZipOutputStream = this.resolveOutputsBuilders(parameters.getParameters());
            NoExtractionResultException noDataToExtract;
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                noDataToExtract = null;
                for (final IOutputBuilder iOutputBuilder : roBuildZipOutputStream) {
                    try {
                        iOutputBuilder.buildOutput(parameters);
                    } catch (final NoExtractionResultException e) {
                        noDataToExtract = e;
                    }
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters).getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
        return rObuildZipOutputStream;
    }

    /**
     *
     * @param produitBrutOutputBuilder
     */
    public void setProduitBrutOutputBuilder(IOutputBuilder produitBrutOutputBuilder) {
        this.produitBrutOutputBuilder = produitBrutOutputBuilder;
    }

    /**
     *
     * @param produitMoyenneOutputBuilder
     */
    public void setProduitMoyenneOutputBuilder(IOutputBuilder produitMoyenneOutputBuilder) {
        this.produitMoyenneOutputBuilder = produitMoyenneOutputBuilder;
    }

    /**
     *
     * @param solBrutOutputBuilder
     */
    public void setSolBrutOutputBuilder(IOutputBuilder solBrutOutputBuilder) {
        this.solBrutOutputBuilder = solBrutOutputBuilder;
    }

    /**
     *
     * @param solMoyenneOutputBuilder
     */
    public void setSolMoyenneOutputBuilder(IOutputBuilder solMoyenneOutputBuilder) {
        this.solMoyenneOutputBuilder = solMoyenneOutputBuilder;
    }

    /**
     *
     * @param planteBrutOutputBuilder
     */
    public void setPlanteBrutOutputBuilder(IOutputBuilder planteBrutOutputBuilder) {
        this.planteBrutOutputBuilder = planteBrutOutputBuilder;
    }

    /**
     *
     * @param planteMoyenneOutputBuilder
     */
    public void setPlanteMoyenneOutputBuilder(IOutputBuilder planteMoyenneOutputBuilder) {
        this.planteMoyenneOutputBuilder = planteMoyenneOutputBuilder;
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param extractionConfiguration
     */
    public void setExtractionConfiguration(IExtractionConfiguration extractionConfiguration) {
        this.extractionConfiguration = extractionConfiguration;
    }

}
