/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy_;
import static org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy_.mesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.IValeurIncubationSolMoyDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAValeurIncubationSolMoyDAO extends AbstractJPADAO<ValeurIncubationSolMoy> implements IValeurIncubationSolMoyDAO {

    /**
     *
     * @param realNode
     * @param mesureIncubationSolMoy
     * @param valeur_ecart_type
     * @return
     */
    @Override
    public Optional<ValeurIncubationSolMoy> getByKeys(RealNode realNode, MesureIncubationSolMoy mesureIncubationSolMoy, float valeur_ecart_type) {
        CriteriaQuery<ValeurIncubationSolMoy> query = builder.createQuery(ValeurIncubationSolMoy.class);
        Root<ValeurIncubationSolMoy> v = query.from(ValeurIncubationSolMoy.class);
        Join<ValeurIncubationSolMoy, RealNode> rnVariable = v.join(ValeurIncubationSolMoy_.realNode);
        Join<ValeurIncubationSolMoy, MesureIncubationSolMoy> m = v.join(ValeurIncubationSolMoy_.mesureIncubationSolMoy);
        query
                .select(v)
                .where(
                        builder.equal(rnVariable, realNode),
                        builder.equal(m, mesureIncubationSolProMoy),
                        builder.equal(v.get(ValeurIncubationSolMoy_.ecart_type), valeur_ecart_type)
                );
        return getOptional(query);
    }
}
