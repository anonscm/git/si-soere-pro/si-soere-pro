/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeintervention;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeIntervention> {
    
    /**
     *
     */
    protected ITypeInterventionDAO typeInterventionDAO;
    Properties comentEn;
    Properties libelleEn;
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String type_intervention_nom = tokenizerValues.nextToken();
                
                typeInterventionDAO.remove(typeInterventionDAO.getByNKey(type_intervention_nom)
                        .orElseThrow(() -> new BusinessException("can't get type intervention")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, TypeIntervention.NAME_ENTITY_JPA);
                final String type_intervention_nom = tokenizerValues.nextToken();
                final String type_intervention_code = Utils.createCodeFromString(type_intervention_nom);
                final String type_intervention_objectif = tokenizerValues.nextToken();
                final String type_intervention_source = tokenizerValues.nextToken();
                final String type_intervention_commentaire = tokenizerValues.nextToken();
                persistTypeIntervention(type_intervention_nom, type_intervention_code, type_intervention_objectif, type_intervention_source, type_intervention_commentaire);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<TypeIntervention> getAllElements() throws BusinessException {
        return typeInterventionDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeIntervention typeIntervention) throws BusinessException {
        
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeIntervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typeIntervention.getType_intervention_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeIntervention == null || typeIntervention.getType_intervention_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(typeIntervention.getType_intervention_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeIntervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typeIntervention.getType_intervention_objectif(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeIntervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typeIntervention.getType_intervention_source(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeIntervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typeIntervention.getType_intervention_commentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typeIntervention == null || typeIntervention.getType_intervention_commentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comentEn.getProperty(typeIntervention.getType_intervention_commentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        
        return lineModelGridMetadata;
    }
    
    private void persistTypeIntervention(String type_intervention_nom, String type_intervention_code, String type_intervention_objectif, String type_intervention_source, String type_intervention_commentaire) throws BusinessException {
        final TypeIntervention dbtypeInt = typeInterventionDAO.getByNKey(type_intervention_nom).orElse(null);
        createOrUpdateTypeIntervention(type_intervention_nom, type_intervention_code, type_intervention_objectif, type_intervention_source, type_intervention_commentaire, dbtypeInt);
    }
    
    private void createOrUpdateTypeIntervention(String type_intervention_nom, String type_intervention_code, String type_intervention_objectif, String type_intervention_source, String type_intervention_commentaire, TypeIntervention dbtypeInt) throws BusinessException {
        if (dbtypeInt == null) {
            TypeIntervention typeIntervention = new TypeIntervention(type_intervention_nom, type_intervention_code, type_intervention_objectif, type_intervention_source, type_intervention_commentaire);
            
            typeIntervention.setType_intervention_nom(type_intervention_nom);
            typeIntervention.setType_intervention_objectif(type_intervention_objectif);
            typeIntervention.setType_intervention_source(type_intervention_source);
            typeIntervention.getType_intervention_commentaire();
            createTypeIntervention(typeIntervention);
            
        } else {
            updateTypeIntervention(type_intervention_nom, type_intervention_objectif, type_intervention_source, type_intervention_commentaire, dbtypeInt);
        }
    }
    
    private void createTypeIntervention(TypeIntervention typeIntervention) throws BusinessException {
        try {
            typeInterventionDAO.saveOrUpdate(typeIntervention);
            typeInterventionDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create typeIntervention");
        }
    }
    
    private void updateTypeIntervention(String type_intervention_nom, String type_intervention_objectif, String type_intervention_source, String type_intervention_commentaire, TypeIntervention dbtypeInt) throws BusinessException {
        try {
            dbtypeInt.setType_intervention_code(type_intervention_nom);
            dbtypeInt.setType_intervention_objectif(type_intervention_objectif);
            dbtypeInt.setType_intervention_source(type_intervention_source);
            dbtypeInt.setType_intervention_commentaire(type_intervention_commentaire);
            typeInterventionDAO.saveOrUpdate(dbtypeInt);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update typeIntervention");
        }
    }
    
    /**
     *
     * @return
     */
    public ITypeInterventionDAO getTypeInterventionDAO() {
        return typeInterventionDAO;
    }
    
    /**
     *
     * @param typeInterventionDAO
     */
    public void setTypeInterventionDAO(ITypeInterventionDAO typeInterventionDAO) {
        this.typeInterventionDAO = typeInterventionDAO;
    }
    
    @Override
    protected ModelGridMetadata<TypeIntervention> initModelGridMetadata() {
        comentEn = localizationManager.newProperties(TypeIntervention.NAME_ENTITY_JPA, TypeIntervention.JPA_COLUMN_COMMENTAIRE, Locale.ENGLISH);
        libelleEn = localizationManager.newProperties(TypeIntervention.NAME_ENTITY_JPA, TypeIntervention.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
}
