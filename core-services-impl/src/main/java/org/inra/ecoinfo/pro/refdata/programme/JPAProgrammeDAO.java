/**
 *
 */
package org.inra.ecoinfo.pro.refdata.programme;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAProgrammeDAO extends AbstractJPADAO<Programme> implements IProgrammeDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.programme.IProgrammeDAO#getByNom(java.lang .String)
     */

    /**
     *
     * @param acronyme
     * @param nom
     * @return
     */

    @Override
    public Optional<Programme> getByNKey(String acronyme, String nom) {
        CriteriaQuery<Programme> query = builder.createQuery(Programme.class);
        Root<Programme> programme = query.from(Programme.class);
        query
                .select(programme)
                .where(
                        builder.equal(programme.get(Programme_.acronyme), acronyme),
                        builder.equal(programme.get(Programme_.nom), nom)
                );
        return getOptional(query);
    }

}
