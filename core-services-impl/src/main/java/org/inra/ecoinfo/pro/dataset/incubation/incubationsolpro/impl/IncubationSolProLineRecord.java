/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.impl;

import java.time.LocalDate;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.pro.dataset.impl.VariableValue;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolProLineRecord implements Comparable<IncubationSolProLineRecord> {

    LocalDate date_prel_sol;
    String codeech;
    LocalDate date_debut_incub;
    int jour_incub;
    float masse_de_sol;
    LocalDate date_prel_pro;
    String condition_incub;
    int numero_rep_analyse;
    float masse_de_pro;
    String codeechpro;
//    String n_mineral;
    Long originalLineNumber;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
    int ordre_manip;
    List<VariableValue> variablesValues;

    /**
     *
     */
    public IncubationSolProLineRecord() {
        super();
    }

    /**
     *
     * @param date_prel_sol
     * @param codeech
     * @param codeechpro
     * @param ordre_manip
     * @param date_debut_pro
     * @param date_debut_incub
     * @param jour_incub
     * @param masse_de_sol
     * @param condition_incub
     * @param numero_rep_analyse
     * @param masse_de_pro
     * @param codevariable
     * @param valeurvariable
     * @param statutvaleur
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     */
    public IncubationSolProLineRecord(LocalDate date_prel_sol, String codeech, String codeechpro,
            int ordre_manip, LocalDate date_debut_pro, LocalDate date_debut_incub,
            int jour_incub, float masse_de_sol, String condition_incub, int numero_rep_analyse,
            float masse_de_pro, String codevariable,
            float valeurvariable, String statutvaleur, String codemethode, String codeunite, String codehumidite) {

        this.date_prel_sol = date_prel_sol;
        this.codeech = codeech;
        this.ordre_manip = ordre_manip;
        this.masse_de_sol = masse_de_sol;
        this.date_prel_pro = date_debut_pro;
        this.codeechpro = codeechpro;
        this.masse_de_pro = masse_de_pro;
        this.condition_incub = condition_incub;
        this.date_debut_incub = date_debut_incub;
        this.numero_rep_analyse = numero_rep_analyse;
        this.jour_incub = jour_incub;
        // this.n_mineral = n_mineral;
        //this.originalLineNumber = originalLineNumber;
        this.codevariable = codevariable;
        this.valeurvariable = valeurvariable;
        this.statutvaleur = statutvaleur;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;

    }

    /**
     *
     * @param line
     */
    public void copy(final IncubationSolProLineRecord line) {
        this.date_prel_sol = line.getDate_prel_sol();
        this.codeech = line.getCodeech();
        this.ordre_manip = line.getOrdre_manip();
        this.masse_de_sol = line.getMasse_de_sol();
        this.date_prel_pro = line.getDate_prel_pro();
        this.codeechpro = line.getCodeechpro();
        this.masse_de_pro = line.getMasse_de_pro();
        this.condition_incub = line.getCondition_incub();
        this.date_debut_incub = line.getDate_debut_incub();
        this.numero_rep_analyse = line.getNumero_rep_analyse();
        this.jour_incub = line.getJour_incub();
        //this.n_mineral = line.getN_mineral();
        // this.originalLineNumber = line.getOriginalLineNumber();
        this.codevariable = line.getCodevariable();
        this.valeurvariable = line.getValeurvariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codemethode = line.getCodemethode();
        this.codeunite = line.getCodeunite();
        this.codehumidite = line.getCodehumidite();
    }

    @Override
    public int compareTo(IncubationSolProLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_prel_sol() {
        return date_prel_sol;
    }

    /**
     *
     * @param date_prel_sol
     */
    public void setDate_prel_sol(LocalDate date_prel_sol) {
        this.date_prel_sol = date_prel_sol;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_debut_incub() {
        return date_debut_incub;
    }

    /**
     *
     * @param date_debut_incub
     */
    public void setDate_debut_incub(LocalDate date_debut_incub) {
        this.date_debut_incub = date_debut_incub;
    }

    /**
     *
     * @return
     */
    public int getJour_incub() {
        return jour_incub;
    }

    /**
     *
     * @param jour_incub
     */
    public void setJour_incub(int jour_incub) {
        this.jour_incub = jour_incub;
    }

    /**
     *
     * @return
     */
    public int getOrdre_manip() {
        return ordre_manip;
    }

    /**
     *
     * @param ordre_manip
     */
    public void setOrdre_manip(int ordre_manip) {
        this.ordre_manip = ordre_manip;
    }

    /**
     *
     * @return
     */
    public float getMasse_de_sol() {
        return masse_de_sol;
    }

    /**
     *
     * @param masse_de_sol
     */
    public void setMasse_de_sol(float masse_de_sol) {
        this.masse_de_sol = masse_de_sol;
    }

    /**
     *
     * @return
     */
    public String getCondition_incub() {
        return condition_incub;
    }

    /**
     *
     * @param condition_incub
     */
    public void setCondition_incub(String condition_incub) {
        this.condition_incub = condition_incub;
    }

    /**
     *
     * @return
     */
    public int getNumero_rep_analyse() {
        return numero_rep_analyse;
    }

    /**
     *
     * @param numero_rep_analyse
     */
    public void setNumero_rep_analyse(int numero_rep_analyse) {
        this.numero_rep_analyse = numero_rep_analyse;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_prel_pro() {
        return date_prel_pro;
    }

    /**
     *
     * @param date_prel_pro
     */
    public void setDate_prel_pro(LocalDate date_prel_pro) {
        this.date_prel_pro = date_prel_pro;
    }

    /**
     *
     * @return
     */
    public String getCodeechpro() {
        return codeechpro;
    }

    /**
     *
     * @param codeechpro
     */
    public void setCodeechpro(String codeechpro) {
        this.codeechpro = codeechpro;
    }

    /**
     *
     * @return
     */
    public float getMasse_de_pro() {
        return masse_de_pro;
    }

    /**
     *
     * @param masse_de_pro
     */
    public void setMasse_de_pro(float masse_de_pro) {
        this.masse_de_pro = masse_de_pro;
    }

//    public String getN_mineral() {
//        return n_mineral;
//    }
//
//    public void setN_mineral(String n_mineral) {
//        this.n_mineral = n_mineral;
//    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeurvariable() {
        return valeurvariable;
    }

    /**
     *
     * @param valeurvariable
     */
    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public String getCodeech() {
        return codeech;
    }

    /**
     *
     * @param codeech
     */
    public void setCodeech(String codeech) {
        this.codeech = codeech;
    }

}
