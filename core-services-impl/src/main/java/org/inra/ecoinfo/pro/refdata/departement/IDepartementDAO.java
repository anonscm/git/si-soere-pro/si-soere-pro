package org.inra.ecoinfo.pro.refdata.departement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.region.Region;

/**
 * @author sophie
 *
 */
public interface IDepartementDAO extends IDAO<Departement> {

    List<Departement> getAll();

    Optional<Departement> getByNom(String nomDepartement);

    Optional<Departement> getByNKey(String code);

    List<String> getAllNoDepartement();

    List<Departement> getByRegion(Region region);

}
