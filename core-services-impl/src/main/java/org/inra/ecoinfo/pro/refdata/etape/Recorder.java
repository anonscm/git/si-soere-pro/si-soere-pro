/*
 *
 */
package org.inra.ecoinfo.pro.refdata.etape;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Etapes> {

    /**
     *
     */
    protected IEtapeDAO etapeDAO;
    private Properties EtapenomEN;
    private Properties commentEn;

    private void createEtape(final Etapes etapes) throws BusinessException {
        try {
            etapeDAO.saveOrUpdate(etapes);
            etapeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create etape");
        }
    }

    private void createOrUpdateEtapes(final String code, String libelle, String commentaire, final Etapes dbetapes) throws BusinessException {
        if (dbetapes == null) {
            final Etapes etapes = new Etapes(libelle, commentaire);
            etapes.setEtape_code(code);
            etapes.setEtape_intitule(libelle);
            etapes.setCommentaire(commentaire);
            createEtape(etapes);
        } else {
            updateBDEtapes(code, libelle, commentaire, dbetapes);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final Etapes dbEtape = etapeDAO.getByNKey(intitule).orElse(null);
                if (dbEtape != null) {
                    etapeDAO.remove(dbEtape);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Etapes> getAllElements() throws BusinessException {
        return etapeDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IEtapeDAO getEtapeDAO() {
        return etapeDAO;
    }

    /**
     *
     * @return
     */
    public Properties getEtapenomEN() {
        return EtapenomEN;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Etapes etapes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(etapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : etapes.getEtape_intitule(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapes == null || etapes.getEtape_intitule() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : EtapenomEN.getProperty(etapes.getEtape_intitule()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(etapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : etapes.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapes == null || etapes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(etapes.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Etapes> initModelGridMetadata() {
        EtapenomEN = localizationManager.newProperties(Etapes.NAME_ENTITY_JPA, Etapes.JPA_COLUMN_INTITULE, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(Etapes.NAME_ENTITY_JPA, Etapes.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void persistEtapes(final String code, final String intitule, String commentaire) throws BusinessException, BusinessException {
        final Etapes dbetapes = etapeDAO.getByNKey(intitule).orElse(null);
        createOrUpdateEtapes(code, intitule, commentaire, dbetapes);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Etapes.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistEtapes(code, intitule, commentaire);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param etapeDAO
     */
    public void setEtapeDAO(IEtapeDAO etapeDAO) {
        this.etapeDAO = etapeDAO;
    }

    /**
     *
     * @param etapenomEN
     */
    public void setEtapenomEN(Properties etapenomEN) {
        EtapenomEN = etapenomEN;
    }

    private void updateBDEtapes(final String code, String libelle, String commentaire, final Etapes dbetapes) throws BusinessException {
        try {
            dbetapes.setEtape_code(code);
            dbetapes.setEtape_intitule(libelle);
            dbetapes.setCommentaire(commentaire);
            etapeDAO.saveOrUpdate(dbetapes);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update  etape");
        }

    }

}
