package org.inra.ecoinfo.pro.refdata.activiteindustrielle;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Activiteindustrielle> {

    IActiviteIndustrielleDAO activiteDAO;
    private Properties activiteEN;

    private void createActiviteIndustrielle(final Activiteindustrielle activite) throws BusinessException {
        try {
            activiteDAO.saveOrUpdate(activite);
            activiteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't save activite", ex);
        }
    }

    private void updateActiviteIndustrielle(final String nom, final Activiteindustrielle dbactivite) throws BusinessException {
        try {
            dbactivite.setNom(nom);
            activiteDAO.saveOrUpdate(dbactivite);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update activite", ex);
        }
    }

    private void createOrUpdateActivite(final String code, String nom, final Activiteindustrielle dbactivite) throws BusinessException {
        if (dbactivite == null) {
            final Activiteindustrielle activite = new Activiteindustrielle(code, nom);
            activite.setCode(code);
            activite.setNom(nom);
            createActiviteIndustrielle(activite);
        } else {
            updateActiviteIndustrielle(nom, dbactivite);
        }
    }

    private void persistActivite(ErrorsReport errorsReport, final String code, final String nom) throws BusinessException {
        final Activiteindustrielle dbactivite = activiteDAO.getByNKey(nom).orElse(null);
        createOrUpdateActivite(code, nom, dbactivite);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Activiteindustrielle activite) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(activite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : activite.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(activite == null || activite.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : activiteEN.getProperty(activite.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Activiteindustrielle.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                activiteDAO.remove(activiteDAO.getByNKey(nom).orElseThrow(PersistenceException::new));
                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Activiteindustrielle> getAllElements() throws BusinessException {
        return activiteDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Activiteindustrielle.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistActivite(errorsReport, code, nom);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected ModelGridMetadata<Activiteindustrielle> initModelGridMetadata() {
        activiteEN = localizationManager.newProperties(Activiteindustrielle.NAME_ENTITY_JPA, Activiteindustrielle.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public IActiviteIndustrielleDAO getActiviteDAO() {
        return activiteDAO;
    }

    /**
     *
     * @param activiteDAO
     */
    public void setActiviteDAO(IActiviteIndustrielleDAO activiteDAO) {
        this.activiteDAO = activiteDAO;
    }

    /**
     *
     * @return
     */
    public Properties getActiviteEN() {
        return activiteEN;
    }

    /**
     *
     * @param activiteEN
     */
    public void setActiviteEN(Properties activiteEN) {
        this.activiteEN = activiteEN;
    }

}
