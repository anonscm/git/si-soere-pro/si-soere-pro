
package org.inra.ecoinfo.pro.extraction.fluxchambre.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres;
import org.inra.ecoinfo.pro.extraction.fluxchambre.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class FluxChambreBuildOutputDisplayByRow extends AbstractFluxOutputbuilder<MesureFluxChambres>{

    
      private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_FLUXCHAMBRE";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.fluxchambre.fluxchambre-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    static final String PATTERB_CSV_23_FIELD = "%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";
    
    static final String LIST_COLUMN_VARIABLE_FLUX_CHAMBRE = "LIST_COLUMN_VARIABLE_FLUX_CHAMBRE";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";
    
     final ComparatorVariable comparator = new ComparatorVariable();

    IVariablesPRODAO variPRODAO;
    
    /**
     *
     * @param mesure
     * @param mesuresMap
     */
    @Override
    protected void buildmap(
            List<MesureFluxChambres> mesure, 
            SortedMap<Long, SortedMap<ParcelleElementaire, 
            SortedMap<LocalDate, MesureFluxChambres>>> mesuresMap) {
        java.util.Iterator<MesureFluxChambres> itMesure = mesure
                .iterator();
         while (itMesure.hasNext()) {
            MesureFluxChambres mesureFluxChambres = itMesure
                    .next();
            ParcelleElementaire echan = mesureFluxChambres.getParcelleElementaire();
            Long siteId = echan.getDispositif().getId();
            if (mesuresMap.get(siteId) == null) {
                mesuresMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresMap.get(siteId).get(echan) == null) {
                mesuresMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresMap.get(siteId).get(echan).put(mesureFluxChambres.getLocalDate(), mesureFluxChambres);
            itMesure.remove();
        }
    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedFluxChambreVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresMap
     */
    protected void readMap(
            final List<Dispositif> selectedDispositifs, 
            final List<VariablesPRO> selectedFluxChambreVariables, 
            final IntervalDate selectedIntervalDate, 
            final Map<String, PrintStream> outputPrintStreamMap, 
            final SortedMap<Long, SortedMap<ParcelleElementaire, SortedMap<LocalDate, MesureFluxChambres>>> mesuresMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
         return ChambreExtractor.MAP_INDEX_FLUX_CHAMBRE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
       return  IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRE;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
             return String.format(
                FluxChambreBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        FluxChambreBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        FluxChambreBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE)
				.get(ChambreExtractor.MAP_INDEX_FLUX_CHAMBRE) == null
				|| ((DefaultParameter) parameters).getResults()
						.get(FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE)
						.get(ChambreExtractor.MAP_INDEX_FLUX_CHAMBRE).isEmpty()) {
			return null;
		}
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE));
        return null;
    }

   
    
    private String getValeurToString(ValeurFluxChambres valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        }  else {
            return "NA";
        }
    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs, 
            Map<String, PrintStream> outputPrintStreamMap, 
            SortedMap<Long, SortedMap<ParcelleElementaire, SortedMap<LocalDate, MesureFluxChambres>>> mesuresMap, 
            IntervalDate selectedIntervalDate) {
        try {
            String currentDispositif;
            String currentParcelle;
            //Date date;
            int nbrchambre;
            int nbrcycle;
            
            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    FluxChambreBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    FluxChambreBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_FLUX_CHAMBRE).split(";"));
            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<ParcelleElementaire, SortedMap<LocalDate, MesureFluxChambres>> mesureFluxMapByDisp = mesuresMap
                        .get(dispositif.getId());
                if (mesureFluxMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<ParcelleElementaire, SortedMap<LocalDate, MesureFluxChambres>>> itEchan = mesureFluxMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<ParcelleElementaire, SortedMap<LocalDate, MesureFluxChambres>> echanEntry = itEchan
                            .next();
                    ParcelleElementaire parc = echanEntry.getKey();
                    SortedMap<LocalDate, MesureFluxChambres> mesureFluxMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());

                    for (Entry<LocalDate, MesureFluxChambres> entrySet : mesureFluxMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureFluxChambres mesureFluxChambres = entrySet.getValue();
                        currentDispositif = mesureFluxChambres.getParcelleElementaire().getDispositif().getNomDispo();
                        currentParcelle = mesureFluxChambres.getParcelleElementaire().getName();
                        nbrchambre= mesureFluxChambres.getNbrChambre();
                        nbrcycle= mesureFluxChambres.getNbrCycle();
                        String genericPattern = LineDataFixe(date, currentDispositif,currentParcelle, nbrchambre,nbrcycle);
                          out.print(genericPattern);
                        LineDataVariable(mesureFluxChambres, out,listVariable);
                    }
                    itEchan.remove();
                }
            }
            
            
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentParcelle, int nbrchambre, int nbrcycle){
         String genericPattern = String.format(PATTERB_CSV_23_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentParcelle,
                nbrchambre,
                nbrcycle);
        return genericPattern;
    }

    private void LineDataVariable(MesureFluxChambres mesureFluxChambres, PrintStream out, List<String> listVariable) {
         final List<ValeurFluxChambres> valeurFluxChambres= mesureFluxChambres.getValeurs();
         listVariable.forEach((_item) -> {
             for (Iterator<ValeurFluxChambres> ValeurIterator = valeurFluxChambres.iterator(); ValeurIterator.hasNext();) {
                 ValeurFluxChambres valeur = ValeurIterator.next();
                 
                 String line;
                 line = String.format(";%s;%s;%s",((DatatypeVariableUnitePRO)valeur.getRealNode().getNodeable()).getVariablespro().getCode(), getValeurToString(valeur),valeur.getStatutvaleur());
                 out.print(line);
                 ValeurIterator.remove();
             }
        });
        out.println();
    }
    
    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }
    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }
    static class ErrorsReport {
        
        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;
        
        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(FluxChambreBuildOutputDisplayByRow.CST_NEW_LINE);
        }
        
        public String getErrorsMessages() {
            return this.errorsMessages;
        }
        
        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {
        
        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }
}
