package org.inra.ecoinfo.pro.extraction.physicochimie.sol.moyenne.impl;

import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieExtractor;

/**
 *
 * @author adiankha
 */
public class SolMoyenneExtractor extends AbstractPhysicoChimieExtractor<MesurePhysicoChimieSolsMoy> {

    private static final String CST_EXTRACT = "EXTRACT_";

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     *
     */
    public static String MAP_INDEX_SOLMOYENNE = "solMoyenne";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_SOLMOYENNE_CODE = "extractionResultSolMoyenne";
    IExtractor solMoyenneExtractor;

    @Override
    public void setExtraction(Extraction extraction) {

    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return CST_RESULT_EXTRACTION_SOLMOYENNE_CODE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IPhysicoChimieSolMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_MOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_SOLMOYENNE;
    }

}
