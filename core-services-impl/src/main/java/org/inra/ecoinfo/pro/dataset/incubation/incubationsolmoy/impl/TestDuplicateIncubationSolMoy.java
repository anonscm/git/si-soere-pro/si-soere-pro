/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.impl;

import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;

/**
 *
 * @author vjkoyao
 */
public class TestDuplicateIncubationSolMoy extends AbstractTestDuplicate{
    
    static final String        BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
    static final String  PROPERTY_MSG_DUPLICATE_LINE_INCUBATION_SOL_MOY ="PROPERTY_MSG_DUPLICATE_LINE_INCUBATION_SOL_MOY";
    
    final SortedMap<String, Long>   line;
    final SortedMap<String, String> lineCouvert;

    /**
     *
     */
    public TestDuplicateIncubationSolMoy() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }
    
    /**
     *
     * @param date_prel_sol
     * @param code_echantillon_sol
     * @param ordre
     * @param num_rep_analyse
     * @param code_variable
     * @param code_unite
     * @param code_methode
     * @param code_humidite
     * @param jours_incub
     * @param lineNumber
     */
    protected void addLine (final String date_prel_sol, final String code_echantillon_sol,final String ordre, final String num_rep_analyse,final String code_variable,final String code_unite, final String code_methode,final String code_humidite, final String jours_incub,final long lineNumber){
        final String key = this.getKey(date_prel_sol, code_echantillon_sol, ordre, num_rep_analyse, code_variable,code_unite, code_methode, code_humidite, jours_incub);
        
        if (!this.line.containsKey(key)) {
                 this.line.put(key, lineNumber);
        }else {
                 this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                         TestDuplicateIncubationSolMoy.BUNDLE_SOURCE_PATH,
                         TestDuplicateIncubationSolMoy.PROPERTY_MSG_DUPLICATE_LINE_INCUBATION_SOL_MOY),lineNumber,date_prel_sol, code_echantillon_sol,ordre,num_rep_analyse,code_variable,code_unite,code_methode,code_humidite,
                         this.line.get(key)));
        }
    }
    
    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0],values[1],values[2],values[10],values[11],values[12],values[13],values[14],values[15],lineNumber);
    }
    
}
