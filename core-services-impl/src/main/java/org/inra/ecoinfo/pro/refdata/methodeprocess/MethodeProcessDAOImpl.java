/*
 *
 */
package org.inra.ecoinfo.pro.refdata.methodeprocess;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class MethodeProcessDAOImpl extends AbstractJPADAO<MethodeProcess> implements IMethodeProcessDAO {

    /**
     *
     * @return
     */
    @Override
    public List<MethodeProcess> getAll() {
        return getAll(MethodeProcess.class);
    }

    /**
     *
     * @param methodep_nom
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<MethodeProcess> getByNKey(String methodep_nom) {
        CriteriaQuery<MethodeProcess> query = builder.createQuery(MethodeProcess.class);
        Root<MethodeProcess> methodeProcess = query.from(MethodeProcess.class);
        query
                .select(methodeProcess)
                .where(
                        builder.equal(methodeProcess.get(MethodeProcess_.methodep_nom), methodep_nom)
                );
        return getOptional(query);
    }
}
