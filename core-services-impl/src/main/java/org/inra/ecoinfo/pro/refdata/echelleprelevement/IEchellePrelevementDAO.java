/**
 *
 */
package org.inra.ecoinfo.pro.refdata.echelleprelevement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IEchellePrelevementDAO extends IDAO<EchellePrelevement> {

    /**
     *
     * @return
     */
    List<EchellePrelevement> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<EchellePrelevement> getByNKey(String nom);

}
