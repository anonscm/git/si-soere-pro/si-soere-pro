/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.historiquedispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.faitremarquable.Faitremarquable;

/**
 *
 * @author adiankha
 */
public interface IHistoriqueDispositifDAO extends IDAO<HistoriqueDispositif> {

    /**
     *
     * @return
     */
    List<HistoriqueDispositif> getAll();

    /**
     *
     * @param faitremarquable
     * @param dispositif
     * @return
     */
    Optional<HistoriqueDispositif> getByNKey(Faitremarquable faitremarquable, Dispositif dispositif);

}
