/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author adiankha
 */
public class ITKParameterVO extends DefaultParameter implements IParameter{
    
    /**
     *
     */
    public static final String  RECOLTECOUPE                    = "Recoltecoupe" ;

    /**
     *
     */
    public static final String  APPORT                       = "Apport" ;

    /**
     *
     */
    public static final String  SEMISPLANTATION                           = "Semisplantation" ;
    
    /**
     *
     */
    public static final String  TRAVAILSOL                       = "Travailsol" ;

    /**
     *
     */
    public static final String  PROETUDIE                           = "Proetudie" ;
   
    static final String         ITK_EXTRACTION_TYPE_CODE = "itineraires_techniques";
    
     List<Dispositif>              selectedDispositif                  = new LinkedList();

  
    String                      commentaires;

    
    IntervalDate           intervalDate;

   
    int                         affichage;
    
    /**
     *
     */
    public ITKParameterVO() {
    }
    
    /**
     *
     * @param metadatasMap
     */
    public ITKParameterVO(final Map<String, Object> metadatasMap) {
        this.setParameters(metadatasMap);
        this.setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }
    

    @Override
    public String getExtractionTypeCode() {
       return ITKParameterVO.ITK_EXTRACTION_TYPE_CODE;
    }

    /**
     *
     * @return
     */
    public List<Dispositif> getSelectedDispositif() {
        return selectedDispositif;
    }

    /**
     *
     * @param selectedDispositif
     */
    public void setSelectedDispositif(List<Dispositif> selectedDispositif) {
        this.selectedDispositif = selectedDispositif;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     *
     * @param commentaires
     */
    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     *
     * @return
     */
    public IntervalDate getIntervalDate() {
        return intervalDate;
    }

    /**
     *
     * @param intervalDate
     */
    public void setIntervalDate(IntervalDate intervalDate) {
        this.intervalDate = intervalDate;
    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }
    
    
    
}
