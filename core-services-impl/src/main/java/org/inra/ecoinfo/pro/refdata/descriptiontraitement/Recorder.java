/**
 *
 */
package org.inra.ecoinfo.pro.refdata.descriptiontraitement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.typetraitement.ITypeTraitementDAO;
import org.inra.ecoinfo.pro.refdata.typetraitement.TypeTraitement;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<DescriptionTraitement> {
    
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    ITypeTraitementDAO typeTraitementDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;
    
    Map<String, String[]> codeTraitementPossibles = new TreeMap<String, String[]>();
    private String[] dispositifPossibles;
    private String[] typeTraitementPossibles;
    private Properties propertiesCommentaire;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        int lineCount = 0;
        try {
            skipHeader(parser);
            
            String[] values = null;
            values = parser.getLine();
            while (values != null) {
                lineCount++;
                
                TokenizerValues tokenizerValues = new TokenizerValues(values, DescriptionTraitement.TABLE_NAME);
                
                int indexDispLieu = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomTraitement = tokenizerValues.nextToken();
                String codeTraitement = tokenizerValues.nextToken();
                int indexNbreRepetition = tokenizerValues.currentTokenIndex();
                String nbreRepetition = tokenizerValues.nextToken();
                int indexlibelleTypeTrait = tokenizerValues.currentTokenIndex();
                String libelleTypeTraitement = tokenizerValues.nextToken();
                int indexAnneeDebutValidite = tokenizerValues.currentTokenIndex();
                String anneeDebutValidite = tokenizerValues.nextToken();
                int indexAnneeFinValidite = tokenizerValues.currentTokenIndex();
                String anneeFinValidite = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                
                Lieu lieu = verifieLieu(nomLieu, lineCount + 1, indexDispLieu + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, lineCount + 1, indexDispLieu + 1, errorsReport);
                Integer nombreRepetition = verifieNombreRepetition(nbreRepetition, lineCount + 1, indexNbreRepetition + 1, errorsReport);
                TypeTraitement typeTraitement = verifieTypeTraitement(libelleTypeTraitement, lineCount + 1, indexlibelleTypeTrait + 1, errorsReport);
                verifieAnneeValiditeTraitement(codeDispositifLieu, codeTraitement, anneeDebutValidite, anneeFinValidite, lineCount + 1, indexAnneeDebutValidite + 1, indexAnneeFinValidite + 1, errorsReport);
                
                if (!errorsReport.hasErrors()) {
                    String codeunique = Utils.createCodeFromString(codeDispositif + "_" + codeTraitement);
                    persitTraitement(dispositif, typeTraitement, nomTraitement, codeTraitement, nombreRepetition, anneeDebutValidite, anneeFinValidite, codeunique);
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }
        
        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }
        
        return dispositif;
    }

    /**
     * @param libelleTypeTraitement
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws PersistenceException
     */
    private TypeTraitement verifieTypeTraitement(String libelleTypeTraitement, long line, int index, ErrorsReport errorsReport) throws BusinessException {
        TypeTraitement typeTraitement = typeTraitementDAO.getByNKey(libelleTypeTraitement).orElse(null);
        if (typeTraitement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TYPETRAITEMENT_NONDEFINI"), line, index, libelleTypeTraitement));
        }
        
        return typeTraitement;
    }

    /**
     * @param annee
     * @param line
     * @param index
     * @param errorsReport
     */
    public void verifieAnneeFormat(String annee, long line, int index, ErrorsReport errorsReport) {
        if (annee != null) {
            try {
                DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, String.format("01/01/%s", annee));
            } catch (DateTimeParseException e) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
            }
        }
    }

    /**
     * @param anneeDebutValidite
     * @param anneeFinValidite
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieAnneeValiditeTraitement(String codeDispositifLieu, String codeTraitement, String anneeDebutValidite, String anneeFinValidite, long line, int indexD, int indexF, ErrorsReport errorsReport) {
        verifieAnneeFormat(anneeFinValidite, line, indexF, errorsReport);
        verifieAnneeFormat(anneeDebutValidite, line, indexD, errorsReport);
        
        if (anneeFinValidite != null && new Integer(anneeFinValidite) < new Integer(anneeDebutValidite)) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_DESCRIPTTRAIT"), line, indexF, anneeFinValidite, codeTraitement, codeDispositifLieu, anneeDebutValidite));
        }
    }

    /**
     * @param codeTraitement
     * @param codeTraitOrigine
     * @param dispositif
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private DescriptionTraitement verifieDescriptionTraitementOrigine(String codeTraitement, String codeTraitOrigine, Dispositif dispositif, long line, int index, ErrorsReport errorsReport) {
        //Tous les traitements n'ont pas de traitement père
        DescriptionTraitement descriptionTraitementOrig = null;
        
        if (codeTraitOrigine != null && !codeTraitOrigine.isEmpty()) {
            //vérifie que le code du traitement est différent du code de son traitement d'origine
            if (codeTraitement.equals(codeTraitOrigine)) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DESCRIPTIONTRAITORIG_IDENTIQUE"), line, index, codeTraitement, codeTraitOrigine));
            }
            
            descriptionTraitementOrig = descriptionTraitementDAO.getByNKey(codeTraitOrigine, dispositif).orElse(null);
            
            String designeDispositif = dispositif.getNomDispositif_nomLieu();
            //s'il n'existe pas encore dans la base, erreur
            if (descriptionTraitementOrig == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DESCRIPTIONTRAITORIG_NONDEFINI"), line, index, codeTraitOrigine, codeTraitement, designeDispositif));
            }
        }
        
        return descriptionTraitementOrig;
    }

    /**
     * @param nbreRepetition
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Integer verifieNombreRepetition(String nbreRepetition, long line, int index, ErrorsReport errorsReport) {
        Integer nombreRepetition = null;
        try {
            nombreRepetition = Integer.parseInt(nbreRepetition);
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "NUMEROREPETION_BADFORMAT"), line, index, nbreRepetition));
        }
        
        return nombreRepetition;
    }

    /**
     * @param dispositif
     * @param typeTraitement
     * @param descriptionTraitementOrig
     * @param nomTraitement
     * @param codeTraitement
     * @param nbreRepetition
     * @param anneeDebutValidite
     * @param anneeFinValidite
     * @param commentaire
     * @param anneesModifMineure
     * @param dbDescriptionTraitement
     * @param descriptionTraitement
     * @throws PersistenceException
     */
    private void createTraitement(DescriptionTraitement traitement) throws PersistenceException {
        descriptionTraitementDAO.saveOrUpdate(traitement);
        descriptionTraitementDAO.flush();
    }
    
    private void updateTraitement(Dispositif dispositif, TypeTraitement typeTraitement,
            String nomTraitement, String codeTraitement, Integer nbreRepetition, String anneeDebutValidite, String anneeFinValidite,
            DescriptionTraitement dbDescriptionTraitement, String codeunique) throws PersistenceException {
        dbDescriptionTraitement.setNom(nomTraitement);
        dbDescriptionTraitement.setNbreRepetition(nbreRepetition);
        dbDescriptionTraitement.setTypeTraitement(typeTraitement);
        dbDescriptionTraitement.setAnneeDebutValidite(anneeDebutValidite);
        dbDescriptionTraitement.setAnneeFinValidite(anneeFinValidite);
        dbDescriptionTraitement.setCode(codeTraitement);
        dbDescriptionTraitement.setDispositif(dispositif);
        dbDescriptionTraitement.setCodeunique(codeunique);
        descriptionTraitementDAO.saveOrUpdate(dbDescriptionTraitement);
    }
    
    private void createOrUpdate(Dispositif dispositif, TypeTraitement typeTraitement,
            String nomTraitement, String codeTraitement, Integer nbreRepetition, String anneeDebutValidite, String anneeFinValidite,
            DescriptionTraitement dbDescriptionTraitement, String unique) throws BusinessException {
        try {
            if (dbDescriptionTraitement == null) {
                DescriptionTraitement traitement = new DescriptionTraitement(dispositif, typeTraitement, dbDescriptionTraitement, DOT, DOT, nbreRepetition, anneeDebutValidite, anneeFinValidite, unique, anneeFinValidite);
                traitement.setDispositif(dispositif);
                traitement.setNom(nomTraitement);
                traitement.setCode(codeTraitement);
                traitement.setTypeTraitement(typeTraitement);
                traitement.setAnneeDebutValidite(anneeDebutValidite);
                traitement.setAnneeFinValidite(anneeFinValidite);
                traitement.setNbreRepetition(nbreRepetition);
                traitement.setCodeunique(unique);
                createTraitement(traitement);
            } else {
                updateTraitement(dispositif, typeTraitement, nomTraitement, codeTraitement, nbreRepetition, anneeDebutValidite, anneeFinValidite, dbDescriptionTraitement, unique);
            }
            
        } catch (PersistenceException ex) {
            
            throw new BusinessException("Can't persist DescriptionTraitement");
        }
    }
    
    private void persitTraitement(Dispositif dispositif, TypeTraitement typeTraitement,
            String nomTraitement, String codeTraitement, Integer nbreRepetition, String anneeDebutValidite, String anneeFinValidite, String unique)
            throws BusinessException {
        DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByNKey(codeTraitement, dispositif).orElse(null);
        createOrUpdate(dispositif, typeTraitement, nomTraitement, codeTraitement, nbreRepetition, anneeDebutValidite, anneeFinValidite, dbTraitement, unique);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String codeDispositifLieu = tokenizerValues.nextToken();
                tokenizerValues.nextToken(); //nomTraitement
                String codeTraitement = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                DescriptionTraitement descriptionTraitement = descriptionTraitementDAO.getByNKey(codeTraitement, dispositif)
                        .orElseThrow(() -> new BusinessException("no descriptionTraitement"));
                
                descriptionTraitementDAO.remove(descriptionTraitement);
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getCodeDispositifPossibles() {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll();
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) {
            dispositifPossibles[index++] = dispositif.getNomDispositif_nomLieu();
        }
        return dispositifPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getTypeTraitementPossibles() {
        List<TypeTraitement> lstTypeTraitements = typeTraitementDAO.getAll();
        String[] typeTraitementPossibles = new String[lstTypeTraitements.size()];
        int index = 0;
        for (TypeTraitement typeTraitement : lstTypeTraitements) {
            typeTraitementPossibles[index++] = typeTraitement.getLibelle();
        }
        return typeTraitementPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private Map<String, String[]> getCodeTraitementOrigPossibles() {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll();

        //(unicité du code d'un traitement pour un dispositif donné mais un même code peut-être utilisé pour plusieurs dispositifs)
        for (Dispositif dispositif : lstDispositifs) {
            //recherche les traitements pour un dispositif donné (affichage du code de ce traitement dans la liste des traitements disponibles
            //pour être éventuellement un traitement d'origine pour un autre traitement d'un dispositif donné)
            List<DescriptionTraitement> lstDescriptionTraitements = descriptionTraitementDAO.getByDispositif(dispositif);
            if (lstDescriptionTraitements == null) {
                return new HashMap();
            }
            String[] descriptionTraitementPossibles = new String[lstDescriptionTraitements.size() + 1];
            descriptionTraitementPossibles[0] = (String) Constantes.STRING_EMPTY;
            int index = 1;
            for (DescriptionTraitement descriptionTraitement : lstDescriptionTraitements) {
                descriptionTraitementPossibles[index++] = descriptionTraitement.getCode();
            }
            
            codeTraitementPossibles.put(dispositif.getNomDispositif_nomLieu(), descriptionTraitementPossibles);
            
        }
        return codeTraitementPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DescriptionTraitement descriptionTraitement) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        initNewLine(lineModelGridMetadata, descriptionTraitement);
        // nombre répétitions
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(descriptionTraitement == null ? Constantes.STRING_EMPTY : descriptionTraitement.getNbreRepetition(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        // type de traitement
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(descriptionTraitement == null ? Constantes.STRING_EMPTY : descriptionTraitement.getTypeTraitement().getLibelle(),
                        typeTraitementPossibles , null, false, false, true));

        // année début validité
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(descriptionTraitement == null ? Constantes.STRING_EMPTY : descriptionTraitement.getAnneeDebutValidite(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        // année fin validité
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(descriptionTraitement == null ? Constantes.STRING_EMPTY : descriptionTraitement.getAnneeFinValidite(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    public void initNewLine(LineModelGridMetadata lineModelGridMetadata, DescriptionTraitement descriptionTraitement) {
        final String nomDispositifLieu = descriptionTraitement == null || descriptionTraitement.getDispositif()==null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                :descriptionTraitement.getDispositif().getNomDispositif_nomLieu();
        ColumnModelGridMetadata dispositifColumn = new ColumnModelGridMetadata(nomDispositifLieu, dispositifPossibles, null, true, false, true);

        final String codeComposant = descriptionTraitement == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : descriptionTraitement.getCode();
        ColumnModelGridMetadata codeTreatmentColonne = new ColumnModelGridMetadata(codeComposant, codeTraitementPossibles, null, true, false, true);

        ColumnModelGridMetadata nomTreatmentColumn = new ColumnModelGridMetadata(descriptionTraitement == null ? Constantes.STRING_EMPTY : descriptionTraitement.getNom(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true);
        Deque<ColumnModelGridMetadata> columnsToBeLinked= new LinkedList();
        columnsToBeLinked.add(dispositifColumn);
        columnsToBeLinked.add(codeTreatmentColonne);
        setColumnRefRecursive(columnsToBeLinked);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(dispositifColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(nomTreatmentColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(codeTreatmentColonne);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<DescriptionTraitement> getAllElements() throws BusinessException {
        return descriptionTraitementDAO.getAll();
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param typeTraitementDAO the typeTraitementDAO to set
     */
    public void setTypeTraitementDAO(ITypeTraitementDAO typeTraitementDAO) {
        this.typeTraitementDAO = typeTraitementDAO;
    }

    /**
     * @param descriptionTraitementDAO the descriptionTraitementDAO to set
     */
    public void setDescriptionTraitementDAO(
            IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    @Override
    protected ModelGridMetadata<DescriptionTraitement> initModelGridMetadata() {
        dispositifPossibles = getCodeDispositifPossibles();
        codeTraitementPossibles = getCodeTraitementOrigPossibles();
        typeTraitementPossibles = getTypeTraitementPossibles();
        propertiesCommentaire
                = localizationManager.newProperties(DescriptionTraitement.TABLE_NAME, RefDataConstantes.COLUMN_COMMENT_DESCRPTTRT, Locale.ENGLISH);
        
        return super.initModelGridMetadata(); 
    }
    
}
