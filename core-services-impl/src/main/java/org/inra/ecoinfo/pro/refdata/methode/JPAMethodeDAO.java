/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methode;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAMethodeDAO extends AbstractJPADAO<Methode> implements IMethodeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Methode> getAll() {
        return getAllBy(Methode.class, Methode::getMethode_code);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Methode> getByName(String nom) {
        CriteriaQuery<Methode> query = builder.createQuery(Methode.class);
        Root<Methode> methode = query.from(Methode.class);
        query
                .select(methode)
                .where(
                        builder.equal(methode.get(Methode_.methode_nom), nom)
                );
        return getOptional(query);
    }

    /**
     *
     * @param methode_mycode
     * @return
     */
    @Override
    public Optional<Methode> getByNKey(String methode_mycode) {
        CriteriaQuery<Methode> query = builder.createQuery(Methode.class);
        Root<Methode> methode = query.from(Methode.class);
        query
                .select(methode)
                .where(
                        builder.equal(methode.get(Methode_.methode_mycode), Utils.createCodeFromString(methode_mycode))
                );
        return getOptional(query);
    }
}
