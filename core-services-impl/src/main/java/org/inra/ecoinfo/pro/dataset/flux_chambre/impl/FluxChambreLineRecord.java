/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre.impl;

import java.time.LocalDate;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.pro.dataset.impl.VariableValue;

/**
 *
 * @author vjkoyao
 */
public class FluxChambreLineRecord implements Comparable<FluxChambreLineRecord> {

    LocalDate datemesure;
    int nbr_chambres;
    int nbr_cycles;
    Long originalLineNumber;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
    //String nomParcelle;

    List<VariableValue> variablesValues;

    /**
     *
     */
    public FluxChambreLineRecord() {
        super();
    }

    /**
     *
     * @param datemesure
     * @param nbr_chambres
     * @param nbr_cycles
     * @param originalLineNumber
     * @param codevariable
     * @param valeurvariable
     * @param statutvaleur
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     */
    public FluxChambreLineRecord(LocalDate datemesure, int nbr_chambres, int nbr_cycles, Long originalLineNumber, String codevariable, float valeurvariable, String statutvaleur, String codemethode, String codeunite, String codehumidite) {
        this.datemesure = datemesure;
        this.nbr_chambres = nbr_chambres;
        this.nbr_cycles = nbr_cycles;
        this.originalLineNumber = originalLineNumber;
        this.codevariable = codevariable;
        this.valeurvariable = valeurvariable;
        this.statutvaleur = statutvaleur;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;
        //this.nomParcelle = nomParcelle;
    }

    /**
     *
     * @param line
     */
    public void copy(final FluxChambreLineRecord line) {
        this.datemesure = line.getDatemesure();
        this.nbr_chambres = line.getNbr_chambres();
        this.nbr_cycles = line.getNbr_cycles();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.codevariable = line.getCodevariable();
        this.valeurvariable = line.getValeurvariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codemethode = line.getCodemethode();
        this.codeunite = line.getCodeunite();
        this.codehumidite = line.getCodehumidite();
        // this.nomParcelle = line.getNomParcelle();
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /*public FluxChambreLineRecord(Date datemesure, int nbr_chambres, int nbr_cycles, Long originalLineNumber, List<VariableValue> variablesValues) {
        this.datemesure = datemesure;
        this.nbr_chambres = nbr_chambres;
        this.nbr_cycles = nbr_cycles;
        this.originalLineNumber = originalLineNumber;
        this.variablesValues = variablesValues;
    }
    
     public void copy(final FluxChambreLineRecord line) {
        this.datemesure = line.getDatemesure();
        this.nbr_chambres = line.getNbr_chambres();
        this.nbr_cycles = line.getNbr_cycles();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.variablesValues = line.getVariablesValues();
         
     }*/
    @Override
    public int compareTo(FluxChambreLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatemesure() {
        return datemesure;
    }

    /**
     *
     * @param datemesure
     */
    public void setDatemesure(LocalDate datemesure) {
        this.datemesure = datemesure;
    }

    /**
     *
     * @return
     */
    public int getNbr_chambres() {
        return nbr_chambres;
    }

    /**
     *
     * @param nbr_chambres
     */
    public void setNbr_chambres(int nbr_chambres) {
        this.nbr_chambres = nbr_chambres;
    }

    /**
     *
     * @return
     */
    public int getNbr_cycles() {
        return nbr_cycles;
    }

    /**
     *
     * @param nbr_cycles
     */
    public void setNbr_cycles(int nbr_cycles) {
        this.nbr_cycles = nbr_cycles;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @param variablesValues
     */
    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeurvariable() {
        return valeurvariable;
    }

    /**
     *
     * @param valeurvariable
     */
    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

//    public String getNomParcelle() {
//        return nomParcelle;
//    }
//
//    public void setNomParcelle(String nomParcelle) {
//        this.nomParcelle = nomParcelle;
//    }
//    
}
