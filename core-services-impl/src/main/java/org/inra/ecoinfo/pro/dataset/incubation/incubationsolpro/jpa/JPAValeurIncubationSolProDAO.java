/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;
import static org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy_.mesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.IValeurIncubationSolProDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAValeurIncubationSolProDAO extends AbstractJPADAO<ValeurIncubationSolPro> implements IValeurIncubationSolProDAO{

    /**
     *
     * @param realNode
     * @param statutvaleur
     * @param mesureIncubationSolPro
     * @return
     */
    @Override
    public Optional<ValeurIncubationSolPro> getByNKeys(RealNode realNode, String statutvaleur, MesureIncubationSolPro mesureIncubationSolPro){
         CriteriaQuery<ValeurIncubationSolPro> query = builder.createQuery(ValeurIncubationSolPro.class);
        Root<ValeurIncubationSolPro> v = query.from(ValeurIncubationSolPro.class);
        Join<ValeurIncubationSolPro, RealNode> rnVariable = v.join(ValeurIncubationSolPro_.realNode);
        Join<ValeurIncubationSolPro, MesureIncubationSolPro> m = v.join(ValeurIncubationSolPro_.mesureIncubationSolPro);
        query
                .select(v)
                .where(
                        builder.equal(rnVariable, realNode),
                        builder.equal(m, mesureIncubationSolProMoy),
                        builder.equal(v.get(ValeurIncubationSolPro_.statutvaleur), statutvaleur)
                );
        return getOptional(query);
    }
    
}
