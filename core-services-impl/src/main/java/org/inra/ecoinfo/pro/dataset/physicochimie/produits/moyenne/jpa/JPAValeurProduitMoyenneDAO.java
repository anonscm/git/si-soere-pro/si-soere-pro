/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.IValeurProduitMoyenneDAO;

/**
 *
 * @author adiankha
 */
public class JPAValeurProduitMoyenneDAO extends AbstractJPADAO<ValeurPhysicoChimiePROMoy> implements IValeurProduitMoyenneDAO{

    /**
     *
     * @param dvumRealNode
     * @param mesuremoyenne
     * @param ecartype
     * @return
     */
    @Override
    public Optional<ValeurPhysicoChimiePROMoy> getByNKeys(RealNode dvumRealNode, MesurePhysicoChimiePROMoy mesuremoyenne, Float ecartype) {
         CriteriaQuery<ValeurPhysicoChimiePROMoy> query = builder.createQuery(ValeurPhysicoChimiePROMoy.class);
         Root<ValeurPhysicoChimiePROMoy> v = query.from(ValeurPhysicoChimiePROMoy.class);
         Join<ValeurPhysicoChimiePROMoy, RealNode> rnVariable = v.join(ValeurPhysicoChimiePROMoy_.realNode);
         Join<ValeurPhysicoChimiePROMoy, MesurePhysicoChimiePROMoy> m = v.join(ValeurPhysicoChimiePROMoy_.mesurePhysicoChimiePROMoy);
         query
                 .select(v)
                 .where(
                         builder.equal(m, mesuremoyenne),
                         builder.equal(rnVariable, dvumRealNode),
                         builder.equal(v.get(ValeurPhysicoChimiePROMoy_.ecarttype), ecartype)
                 );
         return getOptional(query);
    }
    
}
