/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import org.inra.ecoinfo.dataset.IRecorder;

/**
 *
 * @author adiankha
 */
public interface IRecorderPRO extends IRecorder{
    
    /**
     *
     * @param deleteRecord
     */
    void setDeleteRecord(IDeleteRecord deleteRecord);

    /**
     *
     * @param processRecord
     */
    void setProcessRecord(IProcessRecord processRecord);

    /**
     *
     * @param sessionPropertiesName
     */
    void setSessionPropertiesName(String sessionPropertiesName);

    /**
     *
     * @param testFormat
     */
    void setTestFormat(ITestFormat testFormat);
    
}
