/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol;

import org.inra.ecoinfo.pro.dataset.impl.AbstractSessionPropertiesPRO;

/**
 *
 * @author adiankha
 */
public abstract class AbstractSolSessionProporties extends AbstractSessionPropertiesPRO implements ISessionPropertiesSol {

    /**
     *
     */
    public AbstractSolSessionProporties() {
        super();
    }

}
