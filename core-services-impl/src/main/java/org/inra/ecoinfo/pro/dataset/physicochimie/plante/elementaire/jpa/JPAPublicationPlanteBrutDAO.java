/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire_;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationPlanteBrutDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurPlanteElementaire> deleteValeurs = builder.createCriteriaDelete(ValeurPlanteElementaire.class);
        Root<ValeurPlanteElementaire> valeur = deleteValeurs.from(ValeurPlanteElementaire.class);
        Subquery<MesurePlanteElementaire> subquery = deleteValeurs.subquery(MesurePlanteElementaire.class);
        Root<MesurePlanteElementaire> m = subquery.from(MesurePlanteElementaire.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesurePlanteElementaire_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurPlanteElementaire_.mesurePlanteElementaire).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesurePlanteElementaire> deleteSequence = builder.createCriteriaDelete(MesurePlanteElementaire.class);
        Root<MesurePlanteElementaire> sequence = deleteSequence.from(MesurePlanteElementaire.class);
        deleteSequence.where(builder.equal(sequence.get(MesurePlanteElementaire_.versionfile), version));
        delete(deleteSequence);
    }

}
