package org.inra.ecoinfo.pro.refdata.dispositifobservatoire;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;

/**
 * @author sophie
 *
 */
public interface IDispositifObservatoireDAO extends IDAO<DispositifObservatoire> {

    /**
     *
     * @return
     */
    List<DispositifObservatoire> getAll();

    /**
     *
     * @param dispositif
     * @param observatoire
     * @return
     */
    Optional<DispositifObservatoire> getByNKey(Dispositif dispositif, Observatoire observatoire);

}
