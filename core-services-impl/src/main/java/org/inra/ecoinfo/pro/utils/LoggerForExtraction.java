/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class LoggerForExtraction {
    
     private static final Logger LOGGER_FOR_EXTRACTION = LoggerFactory.getLogger(LoggerForExtraction.class);

    /**
     *
     * @param utilisateur
     * @param file
     */
    public static void logRequest(Utilisateur utilisateur, File file) {
        final String dateComplete = DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY_HH_MM_SS);
        final String dateString = DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY);
        final String timeString = DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.HH_MM_SS);
        final String message = String.format("%s : extraction de %s", dateComplete, utilisateur.getLogin());
        printLine(file, dateString, timeString, utilisateur);
    }

    private static void printLine(File file, final String dateString, final String timeString, Utilisateur utilisateur) {
        LOGGER_FOR_EXTRACTION.info(String.format("%s;%s;%s;%s;%s;\"%s\"", dateString, timeString, utilisateur.getLogin(), utilisateur.getNom(), utilisateur.getEmail(), readFile(file)));
    }

    private static String readFile(File file) {
        if(file==null || !file.exists()){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        String ls = System.getProperty("line.separator");
        String line = org.apache.commons.lang.StringUtils.EMPTY;
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "ISO-8859-1")) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (IOException ex) {
            LOGGER_FOR_EXTRACTION.error(String.format("cant' load file %s", file.getAbsolutePath()), ex);
            return "file not found";
        }
    }
    
}
