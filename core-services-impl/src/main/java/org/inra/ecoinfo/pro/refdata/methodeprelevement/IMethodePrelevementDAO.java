/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methodeprelevement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IMethodePrelevementDAO extends IDAO<MethodePrelevement> {

    /**
     *
     * @return
     */
    List<MethodePrelevement> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<MethodePrelevement> getByNKey(String nom);

}
