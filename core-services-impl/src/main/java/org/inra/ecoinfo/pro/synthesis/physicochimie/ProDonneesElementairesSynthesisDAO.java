/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO_;
import org.inra.ecoinfo.pro.synthesis.physicochimiepro.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.physicochimiepro.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class ProDonneesElementairesSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurPhysicoChimiePRO, MesurePhysicoChimiePRO> {

    @Override
    Class<ValeurPhysicoChimiePRO> getValueClass() {
        return ValeurPhysicoChimiePRO.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurPhysicoChimiePRO, MesurePhysicoChimiePRO> getMesureAttribute() {
        return ValeurPhysicoChimiePRO_.mesurePhysicoChimiePRO;
    }
}
