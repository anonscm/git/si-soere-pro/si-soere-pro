package org.inra.ecoinfo.pro.refdata.formephysique;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.FormePhysique.FormePhysiques;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<FormePhysiques> {
    
    /**
     *
     */
    protected IFormePhysiqueDAO formeDAO;

    /**
     *
     */
    protected Properties NomFormeEn;

    /**
     *
     */
    protected Properties ComFormeEn;

    /**
     *
     * @param formes
     * @throws PersistenceException
     */
    private void createForme(final FormePhysiques formes) throws BusinessException {
        try {
            formeDAO.saveOrUpdate(formes);
            formeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create forme");
        }
    }

    /**
     *
     * @param formes
     * @param dbformes
     * @throws PersistenceException
     */
    private void createOrUpdateForme(final String code, String nom, String comment, final FormePhysiques dbformes) throws BusinessException {
        if (dbformes == null) {
            final FormePhysiques formes = new FormePhysiques(nom, comment);
            formes.setFp_code(code);
            formes.setFp_nom(nom);
            formes.setFp_comment(comment);
            createForme(formes);
        } else {
            updateBDForme(nom, comment, dbformes);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, FormePhysiques.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final FormePhysiques dbformes = formeDAO.getByNKey(code)
                        .orElseThrow(() -> new BusinessException("can't get forme"));
                formeDAO.remove(dbformes);
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected List<FormePhysiques> getAllElements() throws BusinessException {
        return formeDAO.getAll();
    }
    
    /**
     *
     * @return
     */
    public IFormePhysiqueDAO getFormeDAO() {
        return formeDAO;
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(FormePhysiques formes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(formes == null || formes.getFp_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : formes.getFp_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(formes == null || formes.getFp_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : NomFormeEn.getProperty(formes.getFp_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(formes == null || formes.getFp_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : formes.getFp_comment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(formes == null || formes.getFp_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ComFormeEn.getProperty(formes.getFp_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<FormePhysiques> initModelGridMetadata() {
        NomFormeEn = localizationManager.newProperties(FormePhysiques.NAME_ENTITY_JPA, FormePhysiques.JPA_COLUMN_NAME, Locale.ENGLISH);
        ComFormeEn = localizationManager.newProperties(FormePhysiques.NAME_ENTITY_JPA, FormePhysiques.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param errorsreport
     * @param code
     * @param nom
     * @throws PersistenceException
     */
    private void persistForme(final String code, String nom, String comment) throws BusinessException {
        final FormePhysiques dbformes = formeDAO.getByNKey(code).orElse(null);
        createOrUpdateForme(code, nom, comment, dbformes);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, FormePhysiques.NAME_ENTITY_JPA);
                final String forme_nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(forme_nom);
                String comment = tokenizerValues.nextToken();
                persistForme(code, forme_nom, comment);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    /**
     *
     * @param formeDAO
     */
    public void setFormeDAO(IFormePhysiqueDAO formeDAO) {
        this.formeDAO = formeDAO;
    }

    /**
     *
     * @param formes
     * @param dbformes
     */
    private void updateBDForme(final String nom, String comment, final FormePhysiques dbformes) throws BusinessException {
        try {
            dbformes.setFp_nom(nom);
            dbformes.setFp_comment(comment);
            formeDAO.saveOrUpdate(dbformes);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }
    
    /**
     *
     * @param NomFormeEn
     */
    public void setNomFormeEn(Properties NomFormeEn) {
        this.NomFormeEn = NomFormeEn;
    }
    
    /**
     *
     * @return
     */
    public Properties getNomFormeEn() {
        return NomFormeEn;
    }
    
}
