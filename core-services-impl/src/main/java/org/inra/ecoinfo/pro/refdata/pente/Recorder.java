package org.inra.ecoinfo.pro.refdata.pente;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Pente> {

    /**
     *
     */
    protected IPenteDAO penteDAO;
    private Properties penteEN;

    private void createPente(final Pente pente) throws BusinessException {

        try {
            penteDAO.saveOrUpdate(pente);
            penteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create pente");
        }
    }

    private void updatePente(final String nom, final Pente dbpente) throws BusinessException {
        try {
            dbpente.setNom(nom);
            penteDAO.saveOrUpdate(dbpente);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update pente");
        }
    }

    private void createOrUpdatePente(final String code, String nom, final Pente dbpente) throws BusinessException {
        if (dbpente == null) {
            final Pente pente = new Pente(code, nom);
            pente.setCode(code);
            pente.setNom(nom);
            createPente(pente);
        } else {
            updatePente(nom, dbpente);
        }
    }

    private void persistPente(final String code, final String nom) throws BusinessException, BusinessException {
        final Pente dbpente = penteDAO.getByNKey(nom).orElse(null);
        createOrUpdatePente(code, nom, dbpente);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Pente pente) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pente == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : pente.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pente == null || pente.getNom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : penteEN.getProperty(pente.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();
                final Pente dbpente = penteDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get pente"));
                penteDAO.remove(dbpente);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Pente> getAllElements() throws BusinessException {
        return penteDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Pente.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistPente(code, nom);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected ModelGridMetadata<Pente> initModelGridMetadata() {
        penteEN = localizationManager.newProperties(Pente.NAME_ENTITY_JPA, Pente.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param penteDAO
     */
    public void setPenteDAO(IPenteDAO penteDAO) {
        this.penteDAO = penteDAO;
    }

    /**
     *
     * @param penteEN
     */
    public void setPenteEN(Properties penteEN) {
        this.penteEN = penteEN;
    }

    /**
     *
     * @return
     */
    public IPenteDAO getPenteDAO() {
        return penteDAO;
    }

    /**
     *
     * @return
     */
    public Properties getPenteEN() {
        return penteEN;
    }

}
