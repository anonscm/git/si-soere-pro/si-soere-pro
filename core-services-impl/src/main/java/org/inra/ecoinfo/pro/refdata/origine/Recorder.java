/*
 *
 */
package org.inra.ecoinfo.pro.refdata.origine;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Origines> {

    /**
     *
     */
    protected IOrigineDAO origineDAO;

    /**
     *
     */
    protected Properties propertiesNom;

    /**
     *
     */
    protected Properties OrigineComment;

    private void createOrigines(final Origines origines) throws BusinessException {
        try {
            origineDAO.saveOrUpdate(origines);
            origineDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create origines");
        }
    }

    private void updateBDOrigines(final String nom, String comment, final Origines dborigines) throws BusinessException {
        try {
            dborigines.setOrigine_nom(nom);
            dborigines.setOrigine_comment(comment);
            origineDAO.saveOrUpdate(dborigines);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update origines");
        }

    }

    private void createOrUpdateOrigine(final String code, String nom, String comment, final Origines dbOrigines) throws BusinessException {
        if (dbOrigines == null) {
            final Origines origines = new Origines(nom, comment);
            origines.setOrigine_code(code);
            origines.setOrigine_nom(nom);
            origines.setOrigine_comment(comment);
            createOrigines(origines);
        } else {
            updateBDOrigines(nom, comment, dbOrigines);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Origines.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                origineDAO.remove(origineDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get origines")));
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Origines> getAllElements() throws BusinessException {
        return origineDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Origines origines) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(origines == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : origines.getOrigine_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(origines == null || origines.getOrigine_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : propertiesNom.getProperty(origines.getOrigine_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(origines == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : origines.getOrigine_comment(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(origines == null || origines.getOrigine_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : OrigineComment.getProperty(origines.getOrigine_comment()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public Properties getONomEN() {
        return propertiesNom;
    }

    /**
     *
     * @return
     */
    public IOrigineDAO getOrigineDAO() {
        return origineDAO;
    }

    @Override
    protected ModelGridMetadata<Origines> initModelGridMetadata() {
        propertiesNom = localizationManager.newProperties(Origines.NAME_ENTITY_JPA, Origines.JPA_COLUMN_NAME, Locale.ENGLISH);
        OrigineComment = localizationManager.newProperties(Origines.NAME_ENTITY_JPA, Origines.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void persistOrigines(final String code, final String nom, String comment) throws BusinessException {
        final Origines dbOrigines = origineDAO.getByNKey(nom).orElse(null);
        createOrUpdateOrigine(code, nom, comment, dbOrigines);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Origines.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String comment = tokenizerValues.nextToken();
                persistOrigines(code, nom, comment);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param oNomEN
     */
    public void setONomEN(Properties oNomEN) {
        propertiesNom = oNomEN;
    }

    /**
     *
     * @param origineDAO
     */
    public void setOrigineDAO(IOrigineDAO origineDAO) {
        this.origineDAO = origineDAO;
    }

}
