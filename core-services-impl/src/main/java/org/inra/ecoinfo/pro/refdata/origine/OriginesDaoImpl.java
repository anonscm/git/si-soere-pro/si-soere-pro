/*
 *
 */
package org.inra.ecoinfo.pro.refdata.origine;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * The Class OriginesDaoImpl.
 */
public class OriginesDaoImpl extends AbstractJPADAO<Origines> implements IOrigineDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.typo.refdata.Dao.IOrigineDAO#ALLOrigines()
     */

    /**
     *
     * @return
     */

    @Override
    public List<Origines> getAll() {
        return getAll(Origines.class);
    }

    /**
     *
     * @param origine_nom
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<Origines> getByNKey(String origine_nom) {
        CriteriaQuery<Origines> query = builder.createQuery(Origines.class);
        Root<Origines> origines = query.from(Origines.class);
        query
                .select(origines)
                .where(
                        builder.equal(origines.get(Origines_.origine_nom), origine_nom)
                );
        return getOptional(query);
    }

}
