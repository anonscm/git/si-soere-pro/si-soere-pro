/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique;

import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
public interface IITKDAO<M extends MesureITK, V extends ValeurITK> extends IDAO<M> {

    /**
     *
     * @param dispositifs
     * @param intervals
     * @param user
     * @return
     */
    List<NodeDataSet> getAvailablesVariablesByDispositif(List<Dispositif> dispositifs, IUser user);

    /**
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user
     * @return
     */
    List<M> extractMesureITK(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user);

    /**
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user
     * @return
     */
    Long getExtractionSize(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user);

    /**
     *
     * @param currentUser
     * @return
     */
    List<Dispositif> getAvailablesDispositifs(IUser currentUser);

}
