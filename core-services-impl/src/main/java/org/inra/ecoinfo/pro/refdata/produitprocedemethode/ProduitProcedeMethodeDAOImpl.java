package org.inra.ecoinfo.pro.refdata.produitprocedemethode;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.methodeprocess.MethodeProcess;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

public class ProduitProcedeMethodeDAOImpl extends AbstractJPADAO<ProduitProcedeMethode> implements IProduitProcedeMethodeDAO {

    @Override
    public List<ProduitProcedeMethode> getAll() {
        return getAll(ProduitProcedeMethode.class);
    }

    @Override
    public Optional<ProduitProcedeMethode> getByNKey(Produits prod, MethodeProcess mprocess, int duree) {
        CriteriaQuery<ProduitProcedeMethode> query = builder.createQuery(ProduitProcedeMethode.class);
        Root<ProduitProcedeMethode> produitProcedeMethode = query.from(ProduitProcedeMethode.class);
        Join<ProduitProcedeMethode, Produits> produit = produitProcedeMethode.join(ProduitProcedeMethode_.produits);
        Join<ProduitProcedeMethode, MethodeProcess> methodeProcess = produitProcedeMethode.join(ProduitProcedeMethode_.methodeprocess);
        query
                .select(produitProcedeMethode)
                .where(
                        builder.equal(methodeProcess, mprocess),
                        builder.equal(produit, prod),
                        builder.equal(produitProcedeMethode.get(ProduitProcedeMethode_.duree), duree)
                );
        return getOptional(query);
    }

}
