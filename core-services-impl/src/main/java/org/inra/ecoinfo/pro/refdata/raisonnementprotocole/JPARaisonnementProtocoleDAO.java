package org.inra.ecoinfo.pro.refdata.raisonnementprotocole;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.raisonnement.Raisonnement;

/**
 * @author
 *
 */
public class JPARaisonnementProtocoleDAO extends AbstractJPADAO<RaisonnementProtocole> implements IRaisonnementProtocoleDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.raisonnementprotocole.IRaisonnementProtocoleDAO #getByRaisonnementProtocole(org.inra.ecoinfo.pro.refdata.raisonnement. Raisonnement, org.inra.ecoinfo.pro.refdata.protocole.Protocole)
     */

    /**
     *
     * @param raisonnement
     * @param protocole
     * @return
     */

    @Override
    public Optional<RaisonnementProtocole> getByNKey(Raisonnement raisonnement, Protocole protocole) {
        CriteriaQuery<RaisonnementProtocole> query = builder.createQuery(RaisonnementProtocole.class);
        Root<RaisonnementProtocole> raisonnementProtocole = query.from(RaisonnementProtocole.class);
        Join<RaisonnementProtocole, Protocole> prot = raisonnementProtocole.join(RaisonnementProtocole_.protocole);
        Join<RaisonnementProtocole, Raisonnement> rais = raisonnementProtocole.join(RaisonnementProtocole_.raisonnement);
        query
                .select(raisonnementProtocole)
                .where(
                        builder.equal(rais, raisonnement),
                        builder.equal(prot, protocole)
                );
        return getOptional(query);
    }

}
