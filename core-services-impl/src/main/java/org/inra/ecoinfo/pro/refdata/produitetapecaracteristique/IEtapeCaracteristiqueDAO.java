package org.inra.ecoinfo.pro.refdata.produitetapecaracteristique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.CaracteristiqueEtapes;
import org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape.MethodeProcedeEtapes;

/**
 *
 * @author adiankha
 *
 */
public interface IEtapeCaracteristiqueDAO extends IDAO<EtapeCaracterisitique> {

    /**
     *
     * @return
     */
    List<EtapeCaracterisitique> getAll();

    /**
     *
     * @param mpe
     * @param caracteristiqueetapes
     * @param valeur
     * @param unite
     * @return
     */
    Optional<EtapeCaracterisitique> getByNKey(MethodeProcedeEtapes mpe, CaracteristiqueEtapes caracteristiqueetapes, int valeur, String unite);

}
