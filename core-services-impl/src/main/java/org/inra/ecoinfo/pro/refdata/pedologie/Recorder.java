/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.pedologie;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.nomregionalsol.INomRegionalSolDAO;
import org.inra.ecoinfo.pro.refdata.nomregionalsol.Nomregionalsol;
import org.inra.ecoinfo.pro.refdata.pente.IPenteDAO;
import org.inra.ecoinfo.pro.refdata.pente.Pente;
import org.inra.ecoinfo.pro.refdata.substratpedologique.ISubstratPedologiqueDAO;
import org.inra.ecoinfo.pro.refdata.substratpedologique.Substratpedologique;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<Pedologie> {

    private static final String PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE = "PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE";
<<<<<<< HEAD
=======
    private static final String PROPERTY_MSG_CONTEXTE_DISPO_BAD_CODEDISPO = "PROPERTY_MSG_CONTEXTE_DISPO_BAD_CODEDISPO";
>>>>>>> 82586c19... import oublié pour nouvelle property
    private static final String PROPERTY_MSG_BAD_PENTE = "PROPERTY_MSG_BAD_PENTE";
    private static final String PROPERTY_MSG_BAD_SP = " PROPERTY_MSG_BAD_SP ";
    private static final String PROPERTY_MSG_BAD_NOMSOLREGIONAL = "PROPERTY_MSG_BAD_NOMSOLREGIONAL";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    IPedologieDAO pedologieDAO;
    Properties CommentEn;
    IPenteDAO penteDAO;
    ISubstratPedologiqueDAO substratDAO;
    INomRegionalSolDAO nomregionalDAO;
    IDispositifDAO dispositifDAO;

    private String[] listePentesPossibles;
    private String[] listeNomRegionalPossibles;
    private String[] listeSubstratPedologiquePossibles;
    private String[] listeDispositifsPossibles;
    private Map<String, String[]> listeLieuPossibles;

    private void createPedelogie(final Pedologie pedologie) throws BusinessException {
        try {
            pedologieDAO.saveOrUpdate(pedologie);
            pedologieDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create pedologie");
        }
    }

    private void updatePedologie(double reserve, String orientation, double profondeur, String comment, Pente pente, Pedologie dbpedologie,
            Dispositif dispositif, Nomregionalsol regionalsol) throws BusinessException {
        try {
            dbpedologie.setReserve_Utile_Initiale(reserve);
            dbpedologie.setOrientation_Pente(orientation);
            dbpedologie.setProfondeur_Sol(profondeur);
            dbpedologie.setComment(comment);
            dbpedologie.setPente(pente);
            dbpedologie.setDispositif(dispositif);
            dbpedologie.setNomregionalsol(regionalsol);
            pedologieDAO.saveOrUpdate(dbpedologie);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update pedologie");
        }
    }

    private void createOrUpdate(double reserve, String orientation, double profondeur, String comment, Pente pente, Pedologie dbpedologie,
            Dispositif dispositif, Nomregionalsol regionalsol, Substratpedologique subspedo) throws BusinessException {
        if (dbpedologie == null) {
            Pedologie pedologie = new Pedologie(reserve, orientation, profondeur, comment, pente, dispositif, regionalsol, subspedo);
            pedologie.setComment(comment);
            pedologie.setOrientation_Pente(orientation);
            pedologie.setProfondeur_Sol(profondeur);
            pedologie.setReserve_Utile_Initiale(reserve);
            pedologie.setPente(pente);
            pedologie.setDispositif(dispositif);
            pedologie.setNomregionalsol(regionalsol);
            pedologie.setSubstratpedologique(subspedo);
            createPedelogie(pedologie);
        } else {
            updatePedologie(reserve, orientation, profondeur, comment, pente, dbpedologie, dispositif, regionalsol);
        }
    }

    private void persistPedologie(double reserve, String orientation, double profondeur, String comment, Pente pente,
            Dispositif dispositif, Nomregionalsol regionalsol, Substratpedologique subspedo) throws BusinessException {
        Pedologie dbpedologie = pedologieDAO.getByNKey(dispositif, regionalsol, subspedo).orElse(null);
        createOrUpdate(reserve, orientation, profondeur, comment, pente, dbpedologie, dispositif, regionalsol, subspedo);
    }

    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    private void listeNomPente() {
        List<Pente> lespentes = penteDAO.getAll(Pente.class);
        String[] Listepente = new String[lespentes.size() + 1];
        Listepente[0] = "";
        int index = 1;
        for (Pente pente : lespentes) {
            Listepente[index++] = pente.getNom();
        }
        this.listePentesPossibles = Listepente;
    }

    private void listeNomSubstratPedologique() {
        List<Substratpedologique> lessubstrat = substratDAO.getAll(Substratpedologique.class);
        String[] Listesubstrat = new String[lessubstrat.size() + 1];
        Listesubstrat[0] = "";
        int index = 1;
        for (Substratpedologique substrat : lessubstrat) {
            Listesubstrat[index++] = substrat.getNom();
        }
        this.listeSubstratPedologiquePossibles = Listesubstrat;
    }

    private void initDispositifPossibles() {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCodeDispo();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifsPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void listeNomRegionalSol() {
        List<Nomregionalsol> lessols = nomregionalDAO.getAll(Nomregionalsol.class);
        String[] ListeRegionalSol = new String[lessols.size() + 1];
        ListeRegionalSol[0] = "";
        int index = 1;
        for (Nomregionalsol regional : lessols) {
            ListeRegionalSol[index++] = regional.getNom();
        }
        this.listeNomRegionalPossibles = ListeRegionalSol;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, Pedologie.NAME_ENTITY_JPA);
                final String codeDispositif = tokenizerValues.nextToken();
                final String lieu = tokenizerValues.nextToken();
                final String nomRegionalSol = tokenizerValues.nextToken();
                final String pente = tokenizerValues.nextToken();
                final String nomSubstratPedologique = tokenizerValues.nextToken();
                Dispositif dbdispo = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                Nomregionalsol dnregionsol = nomregionalDAO.getByNKey(nomRegionalSol).orElse(null);
                Substratpedologique dbsubspedo = substratDAO.getByNKey(nomSubstratPedologique).orElse(null);
                Pedologie dbpedologie = pedologieDAO.getByNKey(dbdispo, dnregionsol, dbsubspedo)
                        .orElseThrow(() -> new BusinessException("can't get pedologie"));
                pedologieDAO.remove(dbpedologie);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Pedologie> getAllElements() throws BusinessException {
        return pedologieDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
               final TokenizerValues tokenizerValues = new TokenizerValues(values, Pedologie.NAME_ENTITY_JPA);
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String codeDispo = tokenizerValues.nextToken();
                int indexlieu = tokenizerValues.currentTokenIndex();
                final String lieu = tokenizerValues.nextToken();
                int indexsol = tokenizerValues.currentTokenIndex();
                final String nomRegionalSol = tokenizerValues.nextToken();
                int indexpente = tokenizerValues.currentTokenIndex();
                final String pente = tokenizerValues.nextToken();
                int indexsubs = tokenizerValues.currentTokenIndex();
                final String subs = tokenizerValues.nextToken();
                double profond = verifieDouble(tokenizerValues, line, true, errorsReport);
                double reservec = verifieDouble(tokenizerValues, line, true, errorsReport);
                final String orient = tokenizerValues.nextToken();
                final String comment = tokenizerValues.nextToken();
                Pente dbpente = penteDAO.getByNKey(pente).orElse(null);
                if (dbpente == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_PENTE), line, indexpente, pente));
                }

                Dispositif dbdispo = dispositifDAO.getByNKey(codeDispo, lieu).orElse(null);
                if (dbdispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE), line, indexdispo, codeDispo, indexlieu, lieu));
                }

                Nomregionalsol dnregionsol = nomregionalDAO.getByNKey(nomRegionalSol).orElse(null);
                if (dnregionsol == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_NOMSOLREGIONAL), line, indexsol, nomRegionalSol));
                }
                Substratpedologique dbsubspedo = substratDAO.getByNKey(subs).orElse(null);
                if (dbsubspedo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_SP), line, indexsubs, subs));
                }

                if (!errorsReport.hasErrors()) {
                    persistPedologie(reservec, orient, profond, comment, dbpente, dbdispo, dnregionsol, dbsubspedo);
                }

                values = csvp.getLine();
            }

            gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @return
     */
    public IPedologieDAO getPedologieDAO() {
        return pedologieDAO;
    }

    /**
     *
     * @param pedologieDAO
     */
    public void setPedologieDAO(IPedologieDAO pedologieDAO) {
        this.pedologieDAO = pedologieDAO;
    }

    /**
     *
     * @return
     */
    public IPenteDAO getPenteDAO() {
        return penteDAO;
    }

    /**
     *
     * @param penteDAO
     */
    public void setPenteDAO(IPenteDAO penteDAO) {
        this.penteDAO = penteDAO;
    }

    /**
     *
     * @return
     */
    public INomRegionalSolDAO getNomregionalDAO() {
        return nomregionalDAO;
    }

    /**
     *
     * @param nomregionalDAO
     */
    public void setNomregionalDAO(INomRegionalSolDAO nomregionalDAO) {
        this.nomregionalDAO = nomregionalDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Pedologie pedologie) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String valeurProduit = pedologie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : pedologie.getDispositif() != null ? pedologie.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifsPossibles, null, true, false, true);
        String valeurProcede = pedologie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : pedologie.getDispositif() != null ? pedologie.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : pedologie.getNomregionalsol().getNom() != null
                        ? pedologie.getNomregionalsol().getNom() : "",
                        listeNomRegionalPossibles, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : pedologie.getPente().getNom() != null
                        ? pedologie.getPente().getNom() : "",
                        listePentesPossibles, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : pedologie.getSubstratpedologique().getNom() != null
                        ? pedologie.getSubstratpedologique().getNom() : "",
                        listeSubstratPedologiquePossibles, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null || pedologie.getProfondeur_Sol() == 0.0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : pedologie.getProfondeur_Sol(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null || pedologie.getReserve_Utile_Initiale() == 0.0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : pedologie.getReserve_Utile_Initiale(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null || pedologie.getOrientation_Pente() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : pedologie.getOrientation_Pente(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null || pedologie.getComment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : pedologie.getComment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pedologie == null || pedologie.getComment() == null 
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : CommentEn.getProperty(pedologie.getComment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        return lineModelGridMetadata;

    }

    @Override
    protected ModelGridMetadata<Pedologie> initModelGridMetadata() {
        listeNomPente();
        listeNomSubstratPedologique();
        listeNomRegionalSol();
        initDispositifPossibles();
        CommentEn = localizationManager.newProperties(Pedologie.NAME_ENTITY_JPA, Pedologie.JPA_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public ISubstratPedologiqueDAO getSubstratDAO() {
        return substratDAO;
    }

    /**
     *
     * @param substratDAO
     */
    public void setSubstratDAO(ISubstratPedologiqueDAO substratDAO) {
        this.substratDAO = substratDAO;
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

}
