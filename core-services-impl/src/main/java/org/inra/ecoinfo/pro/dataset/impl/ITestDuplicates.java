/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.utils.IErrorsReport;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 *
 * @author adiankha
 */
public interface ITestDuplicates  extends Serializable{
    
    /**
     *
     */
    String PROPERTY_MSG_DOUBLON_LINE         = "PROPERTY_MSG_DOUBLON_LINE";

    /**
     *
     */
    String PROPERTY_MSG_DOUBLON_LINE_IN_FILE = "PROPERTY_MSG_DOUBLON_LINE_IN_FILE";

    /**
     *
     * @param badsFormatsReport
     */
    void addErrors(BadsFormatsReport badsFormatsReport);

    /**
     *
     * @param values
     * @param lineNumber
     * @param LocalDates
     * @param versionFile
     */
    void addLine(String[] values, long lineNumber, String[] LocalDates, VersionFile versionFile);

    /**
     *
     * @return
     */
    boolean hasError();

    /**
     *
     * @param errorsReport
     */
    void setErrorsReport(IErrorsReport errorsReport);
    
}
