/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonplante;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.prelevementplante.PrelevementPlante;

/**
 *
 * @author adiankha
 */
public class JPAEchantillonPlantesDAO extends AbstractJPADAO<EchantillonPlante> implements IEchantillonPlanteDAO {

    /**
     *
     * @return
     */
    @Override
    public List<EchantillonPlante> getAll() {
        return getAllBy(EchantillonPlante.class, EchantillonPlante::getCodeeplante);
    }

    /**
     *
     * @param prelevement
     * @param ordre
     * @return
     */
    @Override
    public Optional<EchantillonPlante> getByNKey(PrelevementPlante prelevement, long ordre) {
        CriteriaQuery<EchantillonPlante> query = builder.createQuery(EchantillonPlante.class);
        Root<EchantillonPlante> echantillonPlante = query.from(EchantillonPlante.class);
        Join<EchantillonPlante, PrelevementPlante> prel = echantillonPlante.join(EchantillonPlante_.prelevementplante);
        query
                .select(echantillonPlante)
                .where(
                        builder.equal(echantillonPlante.get(EchantillonPlante_.numeroechantillon), ordre),
                        builder.equal(prel, prelevement)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codePlante
     * @return
     */
    @Override
    public Optional<EchantillonPlante> getByNKey(String codePlante) {
        CriteriaQuery<EchantillonPlante> query = builder.createQuery(EchantillonPlante.class);
        Root<EchantillonPlante> echantillonPlante = query.from(EchantillonPlante.class);
        query
                .select(echantillonPlante)
                .where(
                        builder.equal(echantillonPlante.get(EchantillonPlante_.codeeplante), codePlante)
                );
        return getOptional(query);
    }
}
