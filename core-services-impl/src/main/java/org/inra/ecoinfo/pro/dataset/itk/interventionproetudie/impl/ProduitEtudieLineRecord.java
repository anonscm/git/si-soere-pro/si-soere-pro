/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.impl;

import java.time.LocalDate;

/**
 *
 * @author adiankha
 */
public class ProduitEtudieLineRecord implements Comparable<ProduitEtudieLineRecord> {

    Long originalLineNumber;
    LocalDate datedebut;
    String codedispositif;
    String codetraitement;
    String codeparcelle;
    String nomplacette;
    String localisationprecise;
    String typepro;
    String codepro;
    LocalDate datefin;
    Float quantiteapport;
    String unite;
    String materielapport;
    float profondeurapport;
    LocalDate dateenfouillement;
    String culture;
    String codebbch;
    String precisionstade;
    String conditionhumidite;
    String conditiontemperature;
    String vitessevent;
    String typeobservation;
    String nomobservation;
    String niveauatteinte;
    String commentaire;

    /**
     *
     */
    public ProduitEtudieLineRecord() {
    }

    /**
     *
     * @param originalLineNumber
     * @param datedebut
     * @param codedispositif
     * @param codetraitement
     * @param codeparcelle
     * @param nomplacette
     * @param localisationprecise
     * @param typepro
     * @param codepro
     * @param datefin
     * @param quantiteapport
     * @param unite
     * @param materielapport
     * @param profondeurapport
     * @param dateenfouillement
     * @param culture
     * @param codebbch
     * @param precisionstade
     * @param conditionhumidite
     * @param conditiontemperature
     * @param vitessevent
     * @param typeobservation
     * @param nomobservation
     * @param niveauatteinte
     * @param commentaire
     */
    public ProduitEtudieLineRecord(Long originalLineNumber, LocalDate datedebut, String codedispositif, String codetraitement, String codeparcelle,
            String nomplacette, String localisationprecise, String typepro, String codepro, LocalDate datefin, float quantiteapport, String unite,
            String materielapport, float profondeurapport, LocalDate dateenfouillement, String culture, String codebbch, String precisionstade,
            String conditionhumidite, String conditiontemperature, String vitessevent, String typeobservation, String nomobservation, String niveauatteinte, String commentaire) {
        this.originalLineNumber = originalLineNumber;
        this.datedebut = datedebut;
        this.codedispositif = codedispositif;
        this.codetraitement = codetraitement;
        this.codeparcelle = codeparcelle;
        this.nomplacette = nomplacette;
        this.localisationprecise = localisationprecise;
        this.typepro = typepro;
        this.codepro = codepro;
        this.datefin = datefin;
        this.quantiteapport = quantiteapport;
        this.unite = unite;
        this.materielapport = materielapport;
        this.profondeurapport = profondeurapport;
        this.dateenfouillement = dateenfouillement;
        this.culture = culture;
        this.codebbch = codebbch;
        this.precisionstade = precisionstade;
        this.conditionhumidite = conditionhumidite;
        this.conditiontemperature = conditiontemperature;
        this.vitessevent = vitessevent;
        this.typeobservation = typeobservation;
        this.nomobservation = nomobservation;
        this.niveauatteinte = niveauatteinte;
        this.commentaire = commentaire;
    }

    @Override
    public int compareTo(ProduitEtudieLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatedebut() {
        return datedebut;
    }

    /**
     *
     * @param datedebut
     */
    public void setDatedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
    }

    /**
     *
     * @return
     */
    public String getCodedispositif() {
        return codedispositif;
    }

    /**
     *
     * @param codedispositif
     */
    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    /**
     *
     * @return
     */
    public String getCodetraitement() {
        return codetraitement;
    }

    /**
     *
     * @param codetraitement
     */
    public void setCodetraitement(String codetraitement) {
        this.codetraitement = codetraitement;
    }

    /**
     *
     * @return
     */
    public String getCodeparcelle() {
        return codeparcelle;
    }

    /**
     *
     * @param codeparcelle
     */
    public void setCodeparcelle(String codeparcelle) {
        this.codeparcelle = codeparcelle;
    }

    /**
     *
     * @return
     */
    public String getNomplacette() {
        return nomplacette;
    }

    /**
     *
     * @param nomplacette
     */
    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    /**
     *
     * @return
     */
    public String getLocalisationprecise() {
        return localisationprecise;
    }

    /**
     *
     * @param localisationprecise
     */
    public void setLocalisationprecise(String localisationprecise) {
        this.localisationprecise = localisationprecise;
    }

    /**
     *
     * @return
     */
    public String getTypepro() {
        return typepro;
    }

    /**
     *
     * @param typepro
     */
    public void setTypepro(String typepro) {
        this.typepro = typepro;
    }

    /**
     *
     * @return
     */
    public String getCodepro() {
        return codepro;
    }

    /**
     *
     * @param codepro
     */
    public void setCodepro(String codepro) {
        this.codepro = codepro;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatefin() {
        return datefin;
    }

    /**
     *
     * @param datefin
     */
    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    /**
     *
     * @return
     */
    public float getQuantiteapport() {
        return quantiteapport;
    }

    /**
     *
     * @param quantiteapport
     */
    public void setQuantiteapport(float quantiteapport) {
        this.quantiteapport = quantiteapport;
    }

    /**
     *
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

    /**
     *
     * @return
     */
    public String getMaterielapport() {
        return materielapport;
    }

    /**
     *
     * @param materielapport
     */
    public void setMaterielapport(String materielapport) {
        this.materielapport = materielapport;
    }

    /**
     *
     * @return
     */
    public float getProfondeurapport() {
        return profondeurapport;
    }

    /**
     *
     * @param profondeurapport
     */
    public void setProfondeurapport(float profondeurapport) {
        this.profondeurapport = profondeurapport;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateenfouillement() {
        return dateenfouillement;
    }

    /**
     *
     * @param dateenfouillement
     */
    public void setDateenfouillement(LocalDate dateenfouillement) {
        this.dateenfouillement = dateenfouillement;
    }

    /**
     *
     * @return
     */
    public String getCulture() {
        return culture;
    }

    /**
     *
     * @param culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getPrecisionstade() {
        return precisionstade;
    }

    /**
     *
     * @param precisionstade
     */
    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    /**
     *
     * @return
     */
    public String getConditionhumidite() {
        return conditionhumidite;
    }

    /**
     *
     * @param conditionhumidite
     */
    public void setConditionhumidite(String conditionhumidite) {
        this.conditionhumidite = conditionhumidite;
    }

    /**
     *
     * @return
     */
    public String getConditiontemperature() {
        return conditiontemperature;
    }

    /**
     *
     * @param conditiontemperature
     */
    public void setConditiontemperature(String conditiontemperature) {
        this.conditiontemperature = conditiontemperature;
    }

    /**
     *
     * @return
     */
    public String getVitessevent() {
        return vitessevent;
    }

    /**
     *
     * @param vitessevent
     */
    public void setVitessevent(String vitessevent) {
        this.vitessevent = vitessevent;
    }

    /**
     *
     * @return
     */
    public String getTypeobservation() {
        return typeobservation;
    }

    /**
     *
     * @param typeobservation
     */
    public void setTypeobservation(String typeobservation) {
        this.typeobservation = typeobservation;
    }

    /**
     *
     * @return
     */
    public String getNomobservation() {
        return nomobservation;
    }

    /**
     *
     * @param nomobservation
     */
    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteinte() {
        return niveauatteinte;
    }

    /**
     *
     * @param niveauatteinte
     */
    public void setNiveauatteinte(String niveauatteinte) {
        this.niveauatteinte = niveauatteinte;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
