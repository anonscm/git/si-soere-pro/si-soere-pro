package org.inra.ecoinfo.pro.refdata.typepedologique;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Typepedologique> {
    
    /**
     *
     */
    protected ITypePedologiqueDAO typepedoDAO;
    private Properties typepedoEN;
    
    private void createTypepedo(final Typepedologique typepedologique) throws BusinessException {
        try {
            typepedoDAO.saveOrUpdate(typepedologique);
            typepedoDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create typepedologique");
        }
    }
    
    private void updateTypepedo(final String nom, final Typepedologique dbtypepedologique) throws BusinessException {
        try {
            dbtypepedologique.setNom(nom);
            typepedoDAO.saveOrUpdate(dbtypepedologique);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update typepedologique");
        }
    }
    
    private void createOrUpdateTypepedo(final String code, String nom, final Typepedologique dbtypepedologique) throws BusinessException {
        if (dbtypepedologique == null) {
            final Typepedologique typepedo = new Typepedologique(code, nom);
            typepedo.setCode(code);
            typepedo.setNom(nom);
            createTypepedo(typepedo);
        } else {
            updateTypepedo(nom, dbtypepedologique);
        }
    }
    
    private void persistTypepedo(final String code, final String nom) throws BusinessException, BusinessException {
        final Typepedologique dbtypepedo = typepedoDAO.getByNKey(nom).orElse(null);
        createOrUpdateTypepedo(code, nom, dbtypepedo);
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Typepedologique typepedo) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typepedo == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : typepedo.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typepedo == null || typepedo.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : typepedoEN.getProperty(typepedo.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typepedologique.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final Typepedologique dbtypo = typepedoDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get type pedologique"));
                typepedoDAO.remove(dbtypo);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    public List<Typepedologique> getAllElements() {
        return typepedoDAO.getAll();
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Typepedologique.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistTypepedo(code, nom);
                values = parser.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    public ModelGridMetadata<Typepedologique> initModelGridMetadata() {
        typepedoEN = localizationManager.newProperties(Typepedologique.NAME_ENTITY_JPA, Typepedologique.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
    
    /**
     *
     * @return
     */
    public ITypePedologiqueDAO getTypepedoDAO() {
        return typepedoDAO;
    }
    
    /**
     *
     * @param typepedoDAO
     */
    public void setTypepedoDAO(ITypePedologiqueDAO typepedoDAO) {
        this.typepedoDAO = typepedoDAO;
    }
    
    /**
     *
     * @return
     */
    public Properties getTypepedoEN() {
        return typepedoEN;
    }
    
    /**
     *
     * @param typepedoEN
     */
    public void setTypepedoEN(Properties typepedoEN) {
        this.typepedoEN = typepedoEN;
    }
    
}
