package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import static org.inra.ecoinfo.AbstractJPADAO.LOGGER;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.IMesureSemisPlantationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.refdata.applicationtraitementprclt.IApplicationTraitementParcelleEltDAO;
import org.inra.ecoinfo.pro.refdata.cultures.ICulturesDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.materiel.IMaterielDAO;
import org.inra.ecoinfo.pro.refdata.observationqualitative.IObservationQualitativeDAO;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordSemisPlantation extends AbstractProcessRecord {

    protected static final String BUNDLE_PATH_ITK = "org.inra.ecoinfo.pro.dataset.itk.messages";

    private static final String MSG_ERROR_OBJET_NOT_DB = "MSG_ERROR_OBJET_NOT_DB";
    private static final String MSG_ERROR_FILIERE_NOT_DB = "MSG_ERROR_FILIERE_NOT_DB";
    private static final String MSG_ERROR_AIR_NOT__DB = "MSG_ERROR_AIR_NOT__DB";
    private static final String MSG_ERROR_TEMPERATURE_NOT_DB = "MSG_ERROR_TEMPERATURE_NOT_DB";
    private static final String MSG_ERROR_VITESSE_NOT_DB = "MSG_ERROR_VITESSE_NOT_DB";
    private static final String MSG_ERROR_NIVEAU_NOT__DB = "MSG_ERROR_NIVEAU_NOT__DB";
    private static final String MSG_ERROR_SEMIS_NOT_VARIABLEPRO_DB = "MSG_ERROR_SEMIS_NOT_VARIABLEPRO_DB";

    private static final String MSG_ERROR_RATIO_NOT_FOUND_DVU_DB = "MSG_ERROR_RATIO_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_OBJET_NOT_FOUND_DVU_DB = "MSG_ERROR_OBJET_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_FD_NOT_FOUND_DVU_DB = "MSG_ERROR_FD_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_CA_NOT_FOUND_DVU_DB = "MSG_ERROR_CA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_CT_NOT_FOUND_DVU_DB = "MSG_ERROR_CT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_VV_NOT_FOUND_DVU_DB = "MSG_ERROR_VV_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_NA_NOT_FOUND_DVU_DB = "MSG_ERROR_NA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_TRAITEMENT_SEMIS_NOT_FOUND_IN_DB = "MSG_ERROR_TRAITEMENT_SEMIS_NOT_FOUND_IN_DB";
    IMesureSemisPlantationDAO<MesureSemisPlantation> mesureSemisPlantationDAO;
    IApplicationTraitementParcelleEltDAO applicationTraitementParcelleEltDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    IUniteproDAO uniteproDAO;
    IPlacetteDAO placetteDAO;
    ICulturesDAO culturesDAO;
    IMaterielDAO materielDAO;
    IObservationQualitativeDAO observationQualitativeDAO;
    IListeItineraireDAO listeItineraireDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    public ProcessRecordSemisPlantation() {
        super();
    }

    private long readLines(final CSVParser parser, final Map<LocalDate, List<SemisPlantationLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate datesemis = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codedispositif = cleanerValues.nextToken();
            final String codetraitement = cleanerValues.nextToken();
            final String nomparcelle = cleanerValues.nextToken();
            final String nomplacette = cleanerValues.nextToken();
            final String culture = cleanerValues.nextToken();
            final String objetculture = cleanerValues.nextToken();
            final String filieredestination = cleanerValues.nextToken();
            final String varietecepage = cleanerValues.nextToken();
            final float ratio = Float.parseFloat(cleanerValues.nextToken());
            final float quantiteapport = Float.parseFloat(cleanerValues.nextToken());
            final String unite = cleanerValues.nextToken();
            final String materiel1 = cleanerValues.nextToken();
            final String materiel2 = cleanerValues.nextToken();
            final String materiel3 = cleanerValues.nextToken();
            final int largeurtravail = Integer.parseInt(cleanerValues.nextToken());
            final String conditionhumidite = cleanerValues.nextToken();
            final String conditiontemperature = cleanerValues.nextToken();
            final String vitessevent = cleanerValues.nextToken();
            final String observationqualite = cleanerValues.nextToken();
            final String nomobservation = cleanerValues.nextToken();
            final String niveauatteint = cleanerValues.nextToken();
            final String commentaire = cleanerValues.nextToken();

            final SemisPlantationLineRecord line = new SemisPlantationLineRecord(lineCount, codedispositif, codetraitement, nomparcelle, nomplacette, culture, objetculture, filieredestination, datesemis, varietecepage, ratio, quantiteapport, unite, materiel1, materiel2, materiel3, largeurtravail, conditionhumidite, conditiontemperature, vitessevent, observationqualite, nomobservation, niveauatteint, commentaire);
            try {
                if (!lines.containsKey(datesemis)) {
                    lines.put(datesemis, new LinkedList<>());
                }
                lines.get(datesemis).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        datesemis, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void recordErrors(final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<SemisPlantationLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<SemisPlantationLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, datasetDescriptorPRO, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordSemisPlantation.class).error(ex.getMessage(), ex);
        }
    }

    private void buildLines(final VersionFile versionFile, DatasetDescriptorPRO datasetDescriptorPRO,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<SemisPlantationLineRecord>> lines,
            final SortedSet<SemisPlantationLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<SemisPlantationLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<SemisPlantationLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (SemisPlantationLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, datasetDescriptorPRO, sessionProperties);
            }

        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureSemisPlantationDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public void setMesureSemisPlantationDAO(IMesureSemisPlantationDAO<MesureSemisPlantation> mesureSemisPlantationDAO) {
        this.mesureSemisPlantationDAO = mesureSemisPlantationDAO;
    }

    public void setApplicationTraitementParcelleEltDAO(IApplicationTraitementParcelleEltDAO applicationTraitementParcelleEltDAO) {
        this.applicationTraitementParcelleEltDAO = applicationTraitementParcelleEltDAO;
    }

    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    public void setCulturesDAO(ICulturesDAO culturesDAO) {
        this.culturesDAO = culturesDAO;
    }

    public void setMaterielDAO(IMaterielDAO materielDAO) {
        this.materielDAO = materielDAO;
    }

    public void setObservationQualitativeDAO(IObservationQualitativeDAO observationQualitativeDAO) {
        this.observationQualitativeDAO = observationQualitativeDAO;
    }

    private void buildMesure(SemisPlantationLineRecord line, VersionFile versionFile,
            SortedSet<SemisPlantationLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, DatasetDescriptorPRO datasetDescriptorPRO,
            ISessionPropertiesPRO sessionProperties) throws PersistenceException,
            InsertionDatabaseException {
        LocalDate datesemis = line.getDatesemisouplantation();
        String codedisp = line.getCodedispositif();
        String keydisp = Utils.createCodeFromString(sessionProperties.getDispositif().getCode());
        final String codeTrait = Utils.createCodeFromString(line.getCodetraitement());
        String codeunique = keydisp + "_" + codeTrait;
        DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByCodeUnique(codeunique).orElse(null);
        if (dbTraitement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_TRAITEMENT_SEMIS_NOT_FOUND_IN_DB), codeTrait, keydisp));
        }
        //  String codeTrait = line.getCodetraitement();
        String nomParcelle = line.getCodeparcelle();
        String nomPlacette = line.getNomplacette();
        String nomCulture = line.getCulture();
        String objetculture = Utils.createCodeFromString(line.getObjectculture());
        String filieredes = Utils.createCodeFromString(line.getFilieredestination());
        String varietécepage = line.getVarieteousepage();
        float ratio = line.getRatio();
        float qtapport = line.getQuantiteapport();
        String unite = line.getUnite();
        String materiel1 = line.getMateriel1();
        String materiel2 = line.getMateriel2();
        String materiel3 = line.getMateriel3();
        float largeurtravail = line.getLargeurtravail();
        String conditionhumidite = Utils.createCodeFromString(line.getConditionhumidite());
        String conditiontemperature = Utils.createCodeFromString(line.getConditiontemperature());
        String vitessevent = Utils.createCodeFromString(line.getVitessevent());
        String observationqualite = Utils.createCodeFromString(line.getObservationqualite());
        String nomobservation = Utils.createCodeFromString(line.getNomobservation());
        String niveauatteint = Utils.createCodeFromString(line.getNiveauatteint());
        String commentaire = line.getCommentaire();
        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cratio = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(9));
        String apport = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(10));
        String objet = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(6));
        String filiere = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(7));
        String conditionair = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(16));
        String conditiontemp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(17));
        String vitesse = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(18));
        String largeur = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(15));

        ListeItineraire dbObjet = listeItineraireDAO.getByKKey(objet, objetculture).orElse(null);
        if (dbObjet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_OBJET_NOT_DB), objet, objetculture));
        }

        ListeItineraire dbFiliere = listeItineraireDAO.getByKKey(filiere, filieredes).orElse(null);
        if (dbFiliere == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_FILIERE_NOT_DB), filiere, filieredes));
        }

        ListeItineraire dbCA = listeItineraireDAO.getByKKey(conditionair, conditionhumidite).orElse(null);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_AIR_NOT__DB), conditionair, filieredes));
        }

        ListeItineraire dbCT = listeItineraireDAO.getByKKey(conditiontemp, conditiontemperature).orElse(null);
        if (dbCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_TEMPERATURE_NOT_DB), conditiontemperature, conditiontemp));
        }

        ListeItineraire dbVV = listeItineraireDAO.getByKKey(vitesse, vitessevent).orElse(null);
        if (dbVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_VITESSE_NOT_DB), vitesse, vitessevent));
        }

        MesureSemisPlantation mesureSemisPlantation = getorCreate(datesemis, codedisp, codeTrait, nomParcelle, nomPlacette, nomCulture, dbTraitement, varietécepage, materiel1, materiel2, materiel3, unite, versionFile, nomobservation, commentaire, observationqualite, keydisp, niveauatteint);

        DatatypeVariableUnitePRO dbdvration = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, cratio).orElse(null);
        if (dbdvration == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_RATIO_NOT_FOUND_DVU_DB), cdatatype, cratio));
        }

        RealNode dbdvrationRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvration.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvapport = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, apport).orElse(null);
        if (dbdvapport == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_OBJET_NOT_FOUND_DVU_DB), cdatatype, apport));
        }

        RealNode dbdvapportRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvapport.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvumobjet = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, objet).orElse(null);
        if (dbdvumobjet == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_OBJET_NOT_FOUND_DVU_DB), cdatatype, objet));
        }

        RealNode dbdvumobjetRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvumobjet.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuFiliere = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, filiere).orElse(null);
        if (dbdvuFiliere == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_FD_NOT_FOUND_DVU_DB), cdatatype, filiere));

        }
        RealNode dbdvuFiliereRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuFiliere.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvCA = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditionair).orElse(null);
        if (dbdvCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_CA_NOT_FOUND_DVU_DB), cdatatype, conditionair));

        }
        RealNode dbdvCARealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvCA.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuCT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditiontemp).orElse(null);
        if (dbdvuCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_CT_NOT_FOUND_DVU_DB), cdatatype, conditiontemp));

        }
        RealNode dbdvuCTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuCT.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuVV = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, vitesse).orElse(null);
        if (dbdvuVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_VV_NOT_FOUND_DVU_DB), cdatatype, vitesse));

        }
        RealNode dbdvuVVRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuVV.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvumLT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, largeur).orElse(null);
        if (dbdvumLT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordSemisPlantation.BUNDLE_PATH_ITK,
                    ProcessRecordSemisPlantation.MSG_ERROR_NA_NOT_FOUND_DVU_DB), cdatatype, largeur));

        }

        RealNode dbdvumLTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvumLT.getCode())).orElse(null);

        ValeurSemisPlantation valeurSemisPlantation = new ValeurSemisPlantation(ratio, mesureSemisPlantation, dbdvrationRealNode);
        valeurSemisPlantation.setValeur(ratio);
        valeurSemisPlantation.setRealNode(dbdvrationRealNode);
        valeurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(valeurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation qavaleurSemisPlantation = new ValeurSemisPlantation(qtapport, mesureSemisPlantation, dbdvapportRealNode);
        qavaleurSemisPlantation.setValeur(qtapport);
        qavaleurSemisPlantation.setRealNode(dbdvapportRealNode);
        qavaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(qavaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation ocvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbObjet, dbdvumobjetRealNode);
        ocvaleurSemisPlantation.setListeItineraire(dbObjet);
        ocvaleurSemisPlantation.setRealNode(dbdvumobjetRealNode);
        ocvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(ocvaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation fdvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbFiliere, dbdvuFiliereRealNode);
        fdvaleurSemisPlantation.setListeItineraire(dbFiliere);
        fdvaleurSemisPlantation.setRealNode(dbdvuFiliereRealNode);
        fdvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(fdvaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation cavaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbCA, dbdvCARealNode);
        cavaleurSemisPlantation.setListeItineraire(dbCA);
        cavaleurSemisPlantation.setRealNode(dbdvCARealNode);
        cavaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(cavaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation ctvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbCT, dbdvuCTRealNode);
        ctvaleurSemisPlantation.setListeItineraire(dbCT);
        ctvaleurSemisPlantation.setRealNode(dbdvuCTRealNode);
        ctvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(ctvaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation vvvaleurSemisPlantation = new ValeurSemisPlantation(mesureSemisPlantation, dbVV, dbdvuVVRealNode);
        vvvaleurSemisPlantation.setListeItineraire(dbVV);
        vvvaleurSemisPlantation.setRealNode(dbdvuVVRealNode);
        vvvaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(vvvaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }

        ValeurSemisPlantation navaleurSemisPlantation = new ValeurSemisPlantation(largeurtravail, mesureSemisPlantation, dbdvumLTRealNode);
        valeurSemisPlantation.setValeur(largeurtravail);
        navaleurSemisPlantation.setRealNode(dbdvumLTRealNode);
        navaleurSemisPlantation.setMesureinterventionsemis(mesureSemisPlantation);
        mesureSemisPlantation.getValeursemis().add(navaleurSemisPlantation);
        if (!errorsReport.hasErrors()) {
            mesureSemisPlantationDAO.saveOrUpdate(mesureSemisPlantation);
        }
    }

    private MesureSemisPlantation getorCreate(LocalDate datesemis, String codedisp, final String codeTrait, String nomParcelle, String nomPlacette, String nomCulture, DescriptionTraitement dbTraitement, String varietécepage, String materiel1, String materiel2, String materiel3, String unite, VersionFile versionFile, String nomobservation, String commentaire, String observationqualite, String keydisp, String niveauatteint) throws PersistenceException {

        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(datesemis, format);
        String key = dateString + "_" + codedisp + "_" + codeTrait + "_" + nomParcelle + "_" + nomPlacette + "_" + nomCulture;
        MesureSemisPlantation mesureSemisPlantation = mesureSemisPlantationDAO.getByKeys(key).orElse(null);
        if (mesureSemisPlantation == null) {

            mesureSemisPlantation = new MesureSemisPlantation(datesemis, codedisp, dbTraitement, nomParcelle, nomPlacette, varietécepage, nomCulture, materiel1, materiel2, materiel3, unite, versionFile, nomobservation, nomobservation, commentaire);
            mesureSemisPlantation.setDatedebut(datesemis);
            mesureSemisPlantation.setCodedispositif(codedisp);
            mesureSemisPlantation.setDescriptionTraitement(dbTraitement);
            mesureSemisPlantation.setNomparcelle(nomParcelle);
            mesureSemisPlantation.setNomplacette(nomPlacette);
            mesureSemisPlantation.setNomculture(nomCulture);
            mesureSemisPlantation.setMateriel1(materiel1);
            mesureSemisPlantation.setMateriel2(materiel2);
            mesureSemisPlantation.setMateriel3(materiel3);
            mesureSemisPlantation.setVarietecepage(varietécepage);
            mesureSemisPlantation.setCommentaire(commentaire);
            mesureSemisPlantation.setTypeobservation(observationqualite);
            mesureSemisPlantation.setNomobservation(nomobservation);
            mesureSemisPlantation.setKeymesure(key);
            mesureSemisPlantation.setNiveauatteint(niveauatteint);
        }
        return mesureSemisPlantation;
    }

    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

}
