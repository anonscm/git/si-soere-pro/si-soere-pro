/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author adiankha
 */
public class PhysicoChimieParameterVO extends DefaultParameter implements IParameter {

    /**
     *
     */
    public static final String PRODUITBRUTE = "ProduitBrut";

    /**
     *
     */
    public static final String PRODUITMOYENNE = "ProduitMoyennes";

    /**
     *
     */
    public static final String SOLBRUTE = "SolBrute" ;

    /**
     *
     */
    public static final String SOLMOYENNE = "SolMoyennes" ;

    static final String PHYSICO_CHIMIE_EXTRACTION_TYPE_CODE = "physico_chimie";

    List<Dispositif> selectedDispsitifs = new LinkedList();

    String commentaires;

    IntervalDate intervalDate;

    int affichage;

    /**
     *
     */
    public PhysicoChimieParameterVO() {
        super();
    }

    /**
     *
     * @param metadatasMap
     */
    public PhysicoChimieParameterVO(final Map<String, Object> metadatasMap) {
        this.setParameters(metadatasMap);
        this.setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    @Override
    public String getExtractionTypeCode() {
        return PhysicoChimieParameterVO.PHYSICO_CHIMIE_EXTRACTION_TYPE_CODE;
    }

    /**
     *
     * @return
     */
    public List<Dispositif> getSelectedDispositifs() {
        return this.selectedDispsitifs;
    }

    /**
     *
     * @param selectedDispsitifs
     */
    public void setSelectedDispositifs(List<Dispositif> selectedDispsitifs) {
        this.selectedDispsitifs = selectedDispsitifs == null ? new LinkedList<Dispositif>() : selectedDispsitifs;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     *
     * @param commentaires
     */
    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     *
     * @return
     */
    public IntervalDate getIntervalDate() {
        return intervalDate;
    }

    /**
     *
     * @param intervalDate
     */
    public void setIntervalDate(IntervalDate intervalDate) {
        this.intervalDate = intervalDate;
    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

}
