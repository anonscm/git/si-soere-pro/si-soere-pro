/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cepagevariete;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPACepageVarieteDAO extends AbstractJPADAO<CepageVariete> implements ICepageVarieteDAO {

    /**
     *
     * @param libelle
     * @return
     */
    @Override
    public Optional<CepageVariete> getByNKey(String libelle) {
        CriteriaQuery<CepageVariete> query = builder.createQuery(CepageVariete.class);
        Root<CepageVariete> cepageVariete = query.from(CepageVariete.class);
        query
                .select(cepageVariete)
                .where(
                        builder.equal(cepageVariete.get(CepageVariete_.cv_libelle), libelle)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<CepageVariete> getAll() {
        return getAllBy(CepageVariete.class, CepageVariete::getCv_libelle);
    }

}
