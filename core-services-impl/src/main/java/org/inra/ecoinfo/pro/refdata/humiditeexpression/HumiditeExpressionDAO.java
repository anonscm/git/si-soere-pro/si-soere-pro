/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.humiditeexpression;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class HumiditeExpressionDAO extends AbstractJPADAO<HumiditeExpression> implements IHumiditeExpressionDAO {

    /**
     *
     * @return
     */
    @Override
    public List<HumiditeExpression> getAll() {
        return getAllBy(HumiditeExpression.class, HumiditeExpression::getCode);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<HumiditeExpression> getByCode(String code) {
        CriteriaQuery<HumiditeExpression> query = builder.createQuery(HumiditeExpression.class);
        Root<HumiditeExpression> humiditeExpression = query.from(HumiditeExpression.class);
        query
                .select(humiditeExpression)
                .where(
                        builder.equal(humiditeExpression.get(HumiditeExpression_.code), code)
                );
        return getOptional(query);
    }

    /**
     *
     * @param mycode
     * @return
     */
    @Override
    public Optional<HumiditeExpression> getByNKey(String mycode) {
        CriteriaQuery<HumiditeExpression> query = builder.createQuery(HumiditeExpression.class);
        Root<HumiditeExpression> humiditeExpression = query.from(HumiditeExpression.class);
        query
                .select(humiditeExpression)
                .where(
                        builder.equal(humiditeExpression.get(HumiditeExpression_.mycode), mycode)
                );
        return getOptional(query);
    }

}
