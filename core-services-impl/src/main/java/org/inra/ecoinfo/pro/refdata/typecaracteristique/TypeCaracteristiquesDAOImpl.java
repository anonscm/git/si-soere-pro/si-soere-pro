package org.inra.ecoinfo.pro.refdata.typecaracteristique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class TypeCaracteristiquesDAOImpl extends AbstractJPADAO<TypeCaracteristiques> implements ITypeCaracteristiqueDAO {

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeCaracteristiques> getByNKey(String tcar_nom) {
        CriteriaQuery<TypeCaracteristiques> query = builder.createQuery(TypeCaracteristiques.class);
        Root<TypeCaracteristiques> typeCaracteristiques = query.from(TypeCaracteristiques.class);
        query
                .select(typeCaracteristiques)
                .where(
                        builder.equal(typeCaracteristiques.get(TypeCaracteristiques_.tcar_nom), tcar_nom)
                );
        return getOptional(query);
    }

    @Override
    public List<TypeCaracteristiques> getAll() {
        return getAllBy(TypeCaracteristiques.class, TypeCaracteristiques::getTcar_code);
    }
}
