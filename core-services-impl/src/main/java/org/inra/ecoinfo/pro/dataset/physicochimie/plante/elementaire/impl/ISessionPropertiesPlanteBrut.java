/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl;

import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;

/**
 *
 * @author adiankha
 */
public interface ISessionPropertiesPlanteBrut extends ISessionPropertiesPRO{
     String dateToString(String date, String time);
    
}
