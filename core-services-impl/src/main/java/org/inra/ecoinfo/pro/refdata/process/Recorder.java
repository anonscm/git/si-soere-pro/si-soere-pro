/*
 *
 */
package org.inra.ecoinfo.pro.refdata.process;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

// TODO: Auto-generated Javadoc
/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<Process> {

    /**
     * The process dao.
     */
    protected IProcessDAO processDAO;

    /**
     * The Process name en.
     */
    private Properties ProcessNameEN;
    private Properties CommentEn;

    private void createOrigines(final Process process) throws BusinessException {
        try {
            processDAO.saveOrUpdate(process);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create process");
        }
    }

    private void createOrUpdateProcess(final String code, String intitule, String commentaire, final Process dbprocess) throws BusinessException {
        if (dbprocess == null) {
            final Process process = new Process(code, intitule, commentaire);
            process.setProcess_code(code);
            process.setProcess_intitule(intitule);
            process.setCommentaire(commentaire);
            createOrigines(process);
        } else {
            updateBDProcess(code, intitule, commentaire, dbprocess);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Process.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final Process dbProcess = processDAO.getByNKey(intitule)
                        .orElseThrow(() -> new BusinessException("can't get process"));
                processDAO.remove(dbProcess);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    public List<Process> getAllElements() {
        return processDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Process process) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(process == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : process.getProcess_intitule(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(process == null || process.getProcess_intitule() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ProcessNameEN.getProperty(process.getProcess_intitule()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(process == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : process.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(process == null || process.getProcess_intitule() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : CommentEn.getProperty(process.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, true, false));

        return lineModelGridMetadata;

    }

    /**
     *
     * @return
     */
    public IProcessDAO getProcessDAO() {
        return processDAO;
    }

    /**
     *
     * @return
     */
    public Properties getProcessNameEN() {
        return ProcessNameEN;
    }

    @Override
    public ModelGridMetadata<Process> initModelGridMetadata() {
        ProcessNameEN = localizationManager.newProperties(Process.NAME_ENTITY_JPA, Process.JPA_COLUMN_INTITULE, Locale.ENGLISH);
        CommentEn = localizationManager.newProperties(Process.NAME_ENTITY_JPA, Process.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void persistProcess(final String code, final String intitule, String commentaire) throws BusinessException {
        final Process dbprocess = processDAO.getByNKey(intitule).orElse(null);
        createOrUpdateProcess(code, intitule, commentaire, dbprocess);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Process.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final String commentaire = tokenizerValues.nextToken();
                persistProcess(code, intitule, commentaire);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param processDAO
     */
    public void setProcessDAO(IProcessDAO processDAO) {
        this.processDAO = processDAO;
    }

    /**
     *
     * @param processNameEN
     */
    public void setProcessNameEN(Properties processNameEN) {
        ProcessNameEN = processNameEN;
    }

    private void updateBDProcess(final String code, String intitule, String commentaire, final Process dbprocess) throws BusinessException {
        try {
            dbprocess.setProcess_intitule(intitule);
            dbprocess.setProcess_code(code);
            dbprocess.setCommentaire(commentaire);
            processDAO.saveOrUpdate(dbprocess);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update process");
        }

    }

}
