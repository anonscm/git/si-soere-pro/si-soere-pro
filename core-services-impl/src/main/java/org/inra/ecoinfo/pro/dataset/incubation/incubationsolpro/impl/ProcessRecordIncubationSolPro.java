/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.IMesureIncubationSolProDAO;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.IValeurIncubationSolProDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl.ProcessRecordPlanteBrut;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsol.IEchantillonSolDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.IEchantillonsProduitDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordIncubationSolPro extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPlanteBrut.class);
    protected static final String BUNDLE_PATH_INCUBATION = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB = "MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB = "MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_INCUBATION_ECHAN_BD = "MSG_ERROR_INCUBATION_ECHAN_BD";
    private static final String MSG_ERROR_INCUBATION_ECHAN_PRO_BD = "MSG_ERROR_INCUBATION_ECHAN_PRO_BD";

    protected IMesureIncubationSolProDAO<MesureIncubationSolPro> mesureIncubationSolProDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IEchantillonSolDAO echantillonSolDAO;
    protected IMethodeDAO methodeDAO;
    protected IEchantillonsProduitDAO echantillonsproduitDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IValeurIncubationSolProDAO valeurIncubationSolProDAO;

    public ProcessRecordIncubationSolPro() {
        super();
    }

    private void buildMesure(IncubationSolProLineRecord mesuresLines, VersionFile versionFile,
            SortedSet<IncubationSolProLineRecord> ligneEnErreur, ErrorsReport errorsReport,
            ISessionPropertiesPRO sessionProperties) throws PersistenceException {

        LocalDate date_prel_sol = mesuresLines.getDate_prel_sol();
        String codeech = mesuresLines.getCodeech();

        final EchantillonsSol echantillonsSol = echantillonSolDAO.getByNKey(Utils.createCodeFromString(codeech)).orElse(null);
        if (echantillonsSol == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_ECHAN_BD), codeech));
        }
        int ordre_manip = mesuresLines.getOrdre_manip();
        float masse_de_sol = mesuresLines.getMasse_de_sol();
        LocalDate date_prel_pro = mesuresLines.getDate_prel_pro();
        String codeechpro = mesuresLines.getCodeechpro();

        final EchantillonsProduit echantillonsProduit = echantillonsproduitDAO.getByECH(Utils.createCodeFromString(codeechpro)).orElse(null);
        if (echantillonsProduit == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_ECHAN_PRO_BD), codeechpro));
        }

        float masse_de_pro = mesuresLines.getMasse_de_pro();
        LocalDate date_debut_incub = mesuresLines.getDate_debut_incub();
        int jour_incub = mesuresLines.getJour_incub();
        String condition_incub = mesuresLines.getCondition_incub();
        int numero_rep_analyse = mesuresLines.getNumero_rep_analyse();
        //String n_mineral= mesuresLines.getN_mineral();

        String codevariable = mesuresLines.getCodevariable();
        float valeurvariable = mesuresLines.getValeurvariable();
        String statutvaleur = mesuresLines.getStatutvaleur();
        String codemethode = mesuresLines.getCodemethode();
        String codeunite = mesuresLines.getCodeunite();
        String codehumidite = mesuresLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);
        Long fichier = mesuresLines.getOriginalLineNumber();
        final VersionFile versionfile = this.versionFileDAO.getById(versionFile.getId()).orElse(null);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cvariable = Utils.createCodeFromString(codevariable);
        String cunite = Utils.createCodeFromString(codeunite);
        String cmethode = Utils.createCodeFromString(codemethode);
        String chumitite = Utils.createCodeFromString(codehumidite);

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB), chumitite));
        }

        VariablesPRO dbvariable = variPRODAO.betByNKey(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB), cvariable));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(datatype, dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolPro.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolPro.MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumitite));

        }
        RealNode realNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesureIncubationSolPro mesureIncubationSolPro = getOrCreate(date_prel_sol, codeech, ordre_manip, masse_de_sol, date_prel_pro, codeechpro, masse_de_pro, condition_incub, date_debut_incub, jour_incub, numero_rep_analyse, fichier, versionfile, echantillonsSol, echantillonsProduit);
            persistValeurIncubationSolPro(valeurvariable, statutvaleur, realNode, mesureIncubationSolPro);
        }

    }

    private MesureIncubationSolPro getOrCreate(LocalDate date_prel_sol, String codeech, int ordre_manip, float masse_de_sol, LocalDate date_prel_pro, String codeechpro, float masse_de_pro, String condition_incub, LocalDate date_debut_incub, int jour_incub, int numero_rep_analyse, Long fichier, VersionFile versionfile, EchantillonsSol echantillonsSol, EchantillonsProduit echantillonsProduit) throws PersistenceException {

        String datePrelSol = null;
        String datePrelPro = null;
        String intStringNumRep = null;
        String intStringJourIncub = null;

        String format = "dd/MMM/yyyy";

        datePrelSol = DateUtil.getUTCDateTextFromLocalDateTime(date_prel_sol, format);
        datePrelPro = DateUtil.getUTCDateTextFromLocalDateTime(date_prel_pro, format);

        intStringNumRep = Integer.toString(numero_rep_analyse);
        intStringJourIncub = Integer.toString(jour_incub);

        String key = datePrelSol + "_" + codeech + "_" + ordre_manip + "_" + datePrelPro + "_" + codeechpro + "_" + intStringNumRep + "_" + intStringJourIncub;

        MesureIncubationSolPro mesureIncubationSolPro = mesureIncubationSolProDAO.getByKeys(key).orElse(null);

        if (mesureIncubationSolPro == null) {

            mesureIncubationSolPro = new MesureIncubationSolPro(date_prel_sol, echantillonsSol, ordre_manip, masse_de_sol, date_prel_pro, echantillonsProduit, masse_de_pro, condition_incub, date_debut_incub, numero_rep_analyse, jour_incub, fichier, versionfile);
            mesureIncubationSolPro.setLocalDate_prel_sol(date_prel_sol);
            mesureIncubationSolPro.setEchantillonsSol(echantillonsSol);
            mesureIncubationSolPro.setOrdre_manip(ordre_manip);
            mesureIncubationSolPro.setMasse_de_sol(masse_de_sol);
            mesureIncubationSolPro.setLocalDate_prel_pro(date_prel_pro);
            mesureIncubationSolPro.setEchantillonsProduit(echantillonsProduit);
            mesureIncubationSolPro.setMasse_de_pro(masse_de_pro);
            mesureIncubationSolPro.setCondition_incub(condition_incub);
            mesureIncubationSolPro.setLocalDate_debut_incub(date_debut_incub);
            mesureIncubationSolPro.setNumero_rep_analyse(numero_rep_analyse);
            mesureIncubationSolPro.setLigneFichierEchange(fichier);
            mesureIncubationSolPro.setJour_incub(jour_incub);
            mesureIncubationSolPro.setVersionfile(versionfile);
            mesureIncubationSolPro.setKeymesure(key);
            mesureIncubationSolProDAO.saveOrUpdate(mesureIncubationSolPro);

        }

        return mesureIncubationSolPro;
    }

    private void persistValeurIncubationSolPro(float valeurvariable, String statutvaleur, RealNode realNode, MesureIncubationSolPro mesureIncubationSolPro) throws PersistenceException {
        ValeurIncubationSolPro valeurIncubationSolPro = valeurIncubationSolProDAO.getByNKeys(realNode, statutvaleur, mesureIncubationSolPro).orElse(null);
        createOrUpdateValeurIncubSP(realNode, valeurvariable, statutvaleur, mesureIncubationSolPro, valeurIncubationSolPro);
    }

    private void createOrUpdateValeurIncubSP(RealNode realNode, float valeurvariable, String statutvaleur, MesureIncubationSolPro mesureIncubationSolPro, ValeurIncubationSolPro valeurIncubationSolPro) throws PersistenceException {

        if (valeurIncubationSolPro == null) {

            ValeurIncubationSolPro valeurIncubationSP = new ValeurIncubationSolPro(valeurvariable, statutvaleur, mesureIncubationSolPro, realNode);
            valeurIncubationSP.setRealNode(realNode);
            valeurIncubationSP.setValeur(valeurvariable);
            valeurIncubationSP.setStatutvaleur(statutvaleur);
            valeurIncubationSP.setMesureIncubationSolPro(mesureIncubationSolPro);
            createVISP(valeurIncubationSP);
        } else {
            updateVISP(realNode, valeurvariable, statutvaleur, mesureIncubationSolPro, valeurIncubationSolPro);
        }
    }

    private void createVISP(ValeurIncubationSolPro valeurIncubationSP) throws PersistenceException {
        valeurIncubationSolProDAO.saveOrUpdate(valeurIncubationSP);
    }

    private void updateVISP(RealNode realNode, float valeurvariable, String statutvaleur, MesureIncubationSolPro mesureIncubationSolPro, ValeurIncubationSolPro valeurIncubationSolPro) throws PersistenceException {
        valeurIncubationSolPro.setRealNode(realNode);
        valeurIncubationSolPro.setMesureIncubationSolPro(mesureIncubationSolPro);
        valeurIncubationSolPro.setValeur(valeurvariable);
        valeurIncubationSolPro.setStatutvaleur(statutvaleur);
        valeurIncubationSolProDAO.saveOrUpdate(valeurIncubationSolPro);
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureIncubationSolProDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String fileEncoding,
            DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser, datasetDescriptorPRO);

            final Map<LocalDate, List<IncubationSolProLineRecord>> mesuresMapLines = new HashMap();

            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<IncubationSolProLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    private void readLines(CSVParser parser, Map<LocalDate, List<IncubationSolProLineRecord>> lines,
            long lineCount, ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;

            final LocalDate date_prel_sol = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeech = cleanerValues.nextToken();
            final int ordre_manip = Integer.parseInt(cleanerValues.nextToken());
            final float masse_de_sol = Float.parseFloat(cleanerValues.nextToken());
            final LocalDate date_prel_pro = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeechpro = cleanerValues.nextToken();
            final float masse_de_pro = Float.parseFloat(cleanerValues.nextToken());
            final String condition_incub = cleanerValues.nextToken();
            //final String n_mineral = cleanerValues.nextToken();
            final LocalDate date_debut_incub = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final int num_rep_analyse = Integer.parseInt(cleanerValues.nextToken());
            final String codevariable = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();
            final int jour_incub = Integer.parseInt(cleanerValues.nextToken());
            final float valeurvariable = Float.parseFloat(cleanerValues.nextToken());
            final String statut = cleanerValues.nextToken();

            final IncubationSolProLineRecord line = new IncubationSolProLineRecord(date_prel_sol, codeech, codeechpro,
                    ordre_manip, date_prel_pro, date_debut_incub, jour_incub, masse_de_sol, condition_incub, num_rep_analyse, masse_de_pro,
                    codevariable, valeurvariable, statut, codemethode, codeunite, codehumidite);

            try {

                if (!lines.containsKey(date_prel_sol)) {
                    lines.put(date_prel_sol, new LinkedList<IncubationSolProLineRecord>());
                }
                lines.get(date_prel_sol).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        date_prel_sol, DateUtil.DD_MM_YYYY));
            }
        }

    }

    private void buildLines(VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            ErrorsReport errorsReport, Map<LocalDate, List<IncubationSolProLineRecord>> lines,
            SortedSet<IncubationSolProLineRecord> ligneEnErreur) throws PersistenceException {

        Iterator<Entry<LocalDate, List<IncubationSolProLineRecord>>> iterator = lines.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry<LocalDate, List<IncubationSolProLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (IncubationSolProLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    private void recordErrors(ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    public IMesureIncubationSolProDAO<MesureIncubationSolPro> getMesureIncubationSolProDAO() {
        return mesureIncubationSolProDAO;
    }

    public void setMesureIncubationSolProDAO(IMesureIncubationSolProDAO<MesureIncubationSolPro> mesureIncubationSolProDAO) {
        this.mesureIncubationSolProDAO = mesureIncubationSolProDAO;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public IEchantillonSolDAO getEchantillonSolDAO() {
        return echantillonSolDAO;
    }

    public void setEchantillonSolDAO(IEchantillonSolDAO echantillonSolDAO) {
        this.echantillonSolDAO = echantillonSolDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IEchantillonsProduitDAO getEchantillonsproduitDAO() {
        return echantillonsproduitDAO;
    }

    public void setEchantillonsproduitDAO(IEchantillonsProduitDAO echantillonsproduitDAO) {
        this.echantillonsproduitDAO = echantillonsproduitDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    public IVersionFileDAO getVersionFileDAO() {
        return versionFileDAO;
    }

    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    public IValeurIncubationSolProDAO getValeurIncubationSolProDAO() {
        return valeurIncubationSolProDAO;
    }

    public void setValeurIncubationSolProDAO(IValeurIncubationSolProDAO valeurIncubationSolProDAO) {
        this.valeurIncubationSolProDAO = valeurIncubationSolProDAO;
    }

}
