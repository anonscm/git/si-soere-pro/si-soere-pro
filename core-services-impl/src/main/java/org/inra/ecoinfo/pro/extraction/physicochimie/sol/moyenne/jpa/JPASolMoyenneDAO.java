/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.sol.moyenne.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.jpa.JPAPhysicoChimieDAO;
import org.inra.ecoinfo.pro.synthesis.physicochimiesolmoy.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPASolMoyenneDAO extends JPAPhysicoChimieDAO<MesurePhysicoChimieSolsMoy, ValeurPhysicoChimieSolsMoy> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurPhysicoChimieSolsMoy> getValeurPhysicoChimieClass() {
        return ValeurPhysicoChimieSolsMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesurePhysicoChimieSolsMoy> getMesurePhysicoChimieClass() {
        return MesurePhysicoChimieSolsMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurPhysicoChimieSolsMoy, MesurePhysicoChimieSolsMoy> getMesureAttribute() {
        return ValeurPhysicoChimieSolsMoy_.mesurePhysicoChimieSolsMoy;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IPhysicoChimieSolMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_MOY;
    }
}
