/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.IMesurePhysicoChimiePROMoyDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAMesurePhysicoChimiePROMoyDAO extends AbstractJPADAO<MesurePhysicoChimiePROMoy> implements IMesurePhysicoChimiePROMoyDAO<MesurePhysicoChimiePROMoy> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesurePhysicoChimiePROMoy> getByKeys(String keymesure) {
        CriteriaQuery<MesurePhysicoChimiePROMoy> query = builder.createQuery(MesurePhysicoChimiePROMoy.class);
        Root<MesurePhysicoChimiePROMoy> m = query.from(MesurePhysicoChimiePROMoy.class);
        query
                .select(m)
                .where(builder.equal(m.get(MesurePhysicoChimiePROMoy_.keymesure), keymesure));
        return getOptional(query);
    }

}
