
package org.inra.ecoinfo.pro.utils.filenamecheckers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.PersistenceException;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 *
 * @author vjkoyao
 */
public class FileNameCheckerParcelleddmmyyyy extends AbstractPROFileNameChecker {

    private static final Logger LOGGER = Logger.getLogger(FileNameCheckerParcelleddmmyyyy.class
            .getName());
    /**
     * The Constant DATE_PATTERN @link(String).
     */
    static final String DATE_PATTERN = DateUtil.DD_MM_YYYY_FILE;
    /**
     * The Constant PATTERN @link(String).
     */
    protected static final String PATTERN = "^(%s|.*?)_(%s|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";

    @Override
    protected String getDatePattern() {
        return FileNameCheckerParcelleddmmyyyy.DATE_PATTERN;
    }

    @Override
    protected void testDates(VersionFile version, String currentSite, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected void testDates(final VersionFile version, final String currentSite,
            final String currentDatatype, String currentParcelle, final Matcher splitFilename)
            throws InvalidFileNameException {
        IntervalDate intervalDate;
        try {
            intervalDate = IntervalDate.getIntervalDateddMMyyyy(splitFilename.group(4),
                    splitFilename.group(5));
        } catch (final BadExpectedValueException e1) {
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, FileNameCheckerParcelleddmmyyyy.DATE_PATTERN,
                    FileNameCheckerParcelleddmmyyyy.DATE_PATTERN));
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }

    /**
     *
     * @param version
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param parcelleName
     * @throws InvalidFileNameException
     */
    protected void testParcelle(final VersionFile version, final String currentSite,
            final String currentDatatype, final String currentParcelle, String parcelleName)
            throws InvalidFileNameException {
        try {
            if (parcelleName == null || !parcelleName.equals(currentParcelle)) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                        currentDatatype, currentParcelle, this.getDatePattern(),
                        this.getDatePattern()));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }

    /**
     *
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param splitFilename
     * @throws InvalidFileNameException
     */
    protected void testPath(final String currentSite, final String currentDatatype,
            String currentParcelle, final Matcher splitFilename) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }

    @Override
    public String getFilePath(VersionFile version) {
        final String currentDispositif = RecorderPRO.getDispositifFromVersion(version).getCodeforFileName();
        final String currentDatatype = RecorderPRO.getDatatypeFromVersion(version).getCode();
        final String currentParcelle = RecorderPRO.getParcelleElementaireFromVersion(version).getName();
        return String.format(PATTERN_FILE_NAME_WITH_PARCELLE_PATH, 
                currentDispositif,
                currentDatatype,
                currentParcelle,
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateDebutPeriode(), DATE_PATTERN),
                DateUtil.getUTCDateTextFromLocalDateTime(version.getDataset().getDateFinPeriode(), DATE_PATTERN),
                version.getVersionNumber()
        );
    }

    @Override
    public boolean isValidFileName(final String fileName, final VersionFile version) throws InvalidFileNameException {
        String finalFileName = fileName;
        finalFileName = this.cleanFileName(finalFileName);
        final Dispositif dispositif = RecorderPRO.getDispositifFromVersion(version);
        final String currentDispositif = dispositif.getCodeDispo();
        final String codeforFileName = dispositif.getCodeforFileName();
        final String currentDatatype = RecorderPRO.getDatatypeFromVersion(version).getCode();
        final String currentParcelle = RecorderPRO.getParcelleElementaireFromVersion(version).getName();
        final Matcher splitFilename = Pattern.compile(
                String.format(FileNameCheckerParcelleddmmyyyy.PATTERN, codeforFileName,
                        currentDatatype, currentParcelle)).matcher(finalFileName);
        this.testPath(codeforFileName, currentDatatype, currentParcelle, splitFilename);
        final String siteName = splitFilename.group(1);
        this.testDisp(version, codeforFileName, currentDatatype, currentParcelle, siteName);
        final String datatypeName = Utils.createCodeFromString(splitFilename.group(2));
        this.testDatatype(codeforFileName, currentDatatype, currentParcelle, datatypeName);
        final String parcelleName = Utils.createCodeFromString(splitFilename.group(3));
        this.testParcelle(version, codeforFileName, currentDatatype, currentParcelle, parcelleName);
        this.testDates(version, codeforFileName, currentDatatype, currentParcelle, splitFilename);
        return true;
    }

    /**
     *
     * @param currentSite
     * @param currentDatatype
     * @param currentParcelle
     * @param datatypeName
     * @throws InvalidFileNameException
     * @throws PersistenceException
     */
    protected void testDatatype(final String currentSite, final String currentDatatype,
            final String currentParcelle, String datatypeName) throws InvalidFileNameException, PersistenceException {
        try {
            if ((!currentDatatype.equals(Utils.createCodeFromString(datatypeName)) || 
                    !this.datatypeDAO.getByCode(Utils.createCodeFromString(datatypeName)).isPresent())
                    && !org.apache.commons.lang.StringUtils.EMPTY.equals(datatypeName)
                    && !this.dataTypeVariableUnitePRODAO.getNameVariableForDatatypeName(currentDatatype).orElse("").equalsIgnoreCase(datatypeName)) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                        currentDatatype, currentParcelle, this.getDatePattern(),
                        this.getDatePattern()));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, currentSite,
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }

    /**
     *
     * @param version
     * @param codeDispositif_nomLieu
     * @param currentDatatype
     * @param currentParcelle
     * @param siteName
     * @throws InvalidFileNameException
     */
    protected void testDisp(final VersionFile version, final String codeDispositif_nomLieu, final String currentDatatype, final String currentParcelle, final String siteName) throws InvalidFileNameException {

        String nomDispositif = Dispositif.getNomDispositif(codeDispositif_nomLieu);
        String nomLieu = Dispositif.getNomLieu(codeDispositif_nomLieu);
        try {
            String site = siteName.toUpperCase();
            Dispositif dispositif = this.dispositifDAO.getByNKey(nomDispositif, nomLieu).orElse(null);
            if (dispositif == null) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, String.format("%s-%s", nomDispositif, nomLieu),
                        currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
            }
            final String sitePath = dispositif.getCodeDispo();
            if (dispositif == null || !sitePath.equals(dispositif.getCodeDispo())) {
                throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, String.format("%s-%s", nomDispositif, nomLieu),
                        currentDatatype, currentParcelle, this.getDatePattern(),
                        this.getDatePattern()));
            }
        } catch (final PersistenceException e) {
            LOGGER.debug(e);
            throw new InvalidFileNameException(String.format(FileNameCheckerParcelleddmmyyyy.INVALID_FILE_NAME_WITH_PARCELLE, String.format("%s-%s", nomDispositif, nomLieu),
                    currentDatatype, currentParcelle, this.getDatePattern(), this.getDatePattern()));
        }
    }


}
