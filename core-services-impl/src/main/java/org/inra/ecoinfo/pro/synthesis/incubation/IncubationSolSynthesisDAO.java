/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.incubation;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol_;
import org.inra.ecoinfo.pro.synthesis.incubationsol.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.incubationsol.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class IncubationSolSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurIncubationSol, MesureIncubationSol> {

    @Override
    Class<ValeurIncubationSol> getValueClass() {
        return ValeurIncubationSol.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurIncubationSol, MesureIncubationSol> getMesureAttribute() {
        return ValeurIncubationSol_.mesureIncubationSol;
    }
}
