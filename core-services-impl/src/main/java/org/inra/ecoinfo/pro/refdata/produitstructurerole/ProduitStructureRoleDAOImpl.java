package org.inra.ecoinfo.pro.refdata.produitstructurerole;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.rolestructure.RoleStructure;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 *
 * @author ptcherniati
 */
public class ProduitStructureRoleDAOImpl extends AbstractJPADAO<StructureProduitRole> implements IProduitStructureRoleDAO {

    /**
     *
     * @return
     */
    @Override
    public List<StructureProduitRole> getAll() {
        return getAll(StructureProduitRole.class);
    }

    /**
     *
     * @param produits
     * @param structure
     * @param role
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<StructureProduitRole> getByNKey(Produits produits, Structure structure, RoleStructure role) {
        CriteriaQuery<StructureProduitRole> query = builder.createQuery(StructureProduitRole.class);
        Root<StructureProduitRole> structureProduitRole = query.from(StructureProduitRole.class);
        Join<StructureProduitRole, Produits> prod = structureProduitRole.join(StructureProduitRole_.produits);
        Join<StructureProduitRole, Structure> struct = structureProduitRole.join(StructureProduitRole_.structure);
        Join<StructureProduitRole, RoleStructure> rol = structureProduitRole.join(StructureProduitRole_.role);
        query
                .select(structureProduitRole)
                .where(
                        builder.equal(prod, produits),
                        builder.equal(struct, structure),
                        builder.equal(rol, role)
                );
        return getOptional(query);
    }
}
