/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres_;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationFluxChambreDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurFluxChambres> deleteValeurs = builder.createCriteriaDelete(ValeurFluxChambres.class);
        Root<ValeurFluxChambres> valeur = deleteValeurs.from(ValeurFluxChambres.class);
        Subquery<MesureFluxChambres> subquery = deleteValeurs.subquery(MesureFluxChambres.class);
        Root<MesureFluxChambres> m = subquery.from(MesureFluxChambres.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureFluxChambres_.versionFile), version));
        deleteValeurs.where(valeur.get(ValeurFluxChambres_.mesureFluxChambres).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureFluxChambres> deleteSequence = builder.createCriteriaDelete(MesureFluxChambres.class);
        Root<MesureFluxChambres> sequence = deleteSequence.from(MesureFluxChambres.class);
        deleteSequence.where(builder.equal(sequence.get(MesureFluxChambres_.versionFile), version));
        delete(deleteSequence);
    }

}
