/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.filiere;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Filiere> {

    IFiliereDAO filiereDAO;
    Properties filiereNomEN;

    /**
     *
     * @param filiere
     * @throws BusinessException
     */
    public void createFiliere(Filiere filiere) throws BusinessException {
        try {
            filiereDAO.saveOrUpdate(filiere);
            filiereDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create filiere");
        }
    }

    private void updateFiliere(final String nom, final Filiere dbfiliere) throws BusinessException {
        try {
            dbfiliere.setFiliere_nom(nom);
            filiereDAO.saveOrUpdate(dbfiliere);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update filiere");
        }

    }

    private void createOrUpdateFiliere(final String code, String nom, final Filiere dbfiliere) throws BusinessException {
        if (dbfiliere == null) {
            final Filiere filiere = new Filiere(nom);
            filiere.setFiliere_code(code);
            filiere.setFiliere_nom(nom);

            createFiliere(filiere);
        } else {
            updateFiliere(nom, dbfiliere);
        }
    }

    private void persistFiliere(final String code, final String nom) throws BusinessException, BusinessException {
        final Filiere dbfiliere = filiereDAO.getByNKey(nom).orElse(null);
        createOrUpdateFiliere(code, nom, dbfiliere);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();

                final Filiere dbfiliere = filiereDAO.getByNKey(nom).orElse(null);
                if (dbfiliere != null) {
                    filiereDAO.remove(dbfiliere);
                    values = csvp.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Filiere> getAllElements() throws BusinessException {
        return filiereDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Filiere.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistFiliere(code, nom);
                values = csvp.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Filiere filiere) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(filiere == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : filiere.getFiliere_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(filiere == null || filiere.getFiliere_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : filiereNomEN.getProperty(filiere.getFiliere_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IFiliereDAO getFiliereDAO() {
        return filiereDAO;
    }

    /**
     *
     * @param filiereDAO
     */
    public void setFiliereDAO(IFiliereDAO filiereDAO) {
        this.filiereDAO = filiereDAO;
    }

    @Override
    protected ModelGridMetadata<Filiere> initModelGridMetadata() {
        filiereNomEN = localizationManager.newProperties(Filiere.NAME_ENTITY_JPA, Filiere.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}
