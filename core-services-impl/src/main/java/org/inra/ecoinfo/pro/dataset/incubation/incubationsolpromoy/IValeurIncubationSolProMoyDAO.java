/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;

/**
 *
 * @author vjkoyao
 */
public interface IValeurIncubationSolProMoyDAO extends IDAO<ValeurIncubationSolProMoy> {

    /**
     *
     * @param realNode
     * @param valeur_ecart_type
     * @param mesureIncubationSolProMoy
     * @return
     */
    public Optional<ValeurIncubationSolProMoy> getByNkeys(RealNode realNode, float valeur_ecart_type, MesureIncubationSolProMoy mesureIncubationSolProMoy);

}
