package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.IMesureIncubationSolMoyDAO;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.IValeurIncubationSolMoyDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsol.IEchantillonSolDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordIncubationSolMoy extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordIncubationSolMoy.class);
    protected static final String BUNDLE_PATH_INCUBATION = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB = "MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB = "MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_INCUBATION_ECHAN_BD = "MSG_ERROR_INCUBATION_ECHAN_BD";

    protected IMesureIncubationSolMoyDAO<MesureIncubationSolMoy> mesureIncubationSolMoyDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IEchantillonSolDAO echantillonSolDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IValeurIncubationSolMoyDAO valeurIncubationSolMoyDAO;

    public ProcessRecordIncubationSolMoy() {
        super();
    }

    private void buildMesure(final IncubationSolMoyLineRecord mesuresLines, final VersionFile versionFile,
            final SortedSet<IncubationSolMoyLineRecord> ligneEnErreur, final ErrorsReport errorsReport, final ISessionPropertiesPRO sessionProperties) throws PersistenceException {

        LocalDate date_prel_sol = mesuresLines.getDate_prel_sol();
        String codeech = mesuresLines.getCodeech();

        final EchantillonsSol echantillonsSol = echantillonSolDAO.getByNKey(Utils.createCodeFromString(codeech)).orElse(null);
        if (echantillonsSol == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolMoy.MSG_ERROR_INCUBATION_ECHAN_BD), codeech));
        }
        LocalDate date_debut_incub = mesuresLines.getDate_debut_incub();
        int jour_incub = mesuresLines.getJour_incub();
        int ordre_manip = mesuresLines.getOrdre_manip();
        String humidite_incub = mesuresLines.getHumidite_incub();
        float temperature_incub = mesuresLines.getTemperature_incub();
        float masse_de_sol = mesuresLines.getMasse_de_sol();
        String code_interne_labo = mesuresLines.getCode_interne_labo();
        int numero_rep_analyse = mesuresLines.getNumero_rep_analyse();
        String labo_analyse = mesuresLines.getLabo_analyse();
        float n_mineral = mesuresLines.getN_mineral();
        String codevariable = mesuresLines.getCodevariable();
        float valeur_moyenne = mesuresLines.getValeur_moyenne();
        float valeur_ecart_type = mesuresLines.getValeur_ecart_type();
        String codemethode = mesuresLines.getCodemethode();
        String codeunite = mesuresLines.getCodeunite();
        String codehumidite = mesuresLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());

        String cunite = Utils.createCodeFromString(codeunite);

        String cmethode = Utils.createCodeFromString(codemethode);

        String chumidite = Utils.createCodeFromString(codehumidite);

        String cvariable = Utils.createCodeFromString(codevariable);

        VariablesPRO dbvariable = variPRODAO.betByNKey(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolMoy.MSG_ERROR_INCUBATION_NOT_VARIABLEPRO_DB), cvariable));
        }
        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolMoy.MSG_ERROR_INCUBATION_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumidite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolMoy.MSG_ERROR_INCUBATION_NOT_FOUND_HUMIDITE_DB), chumidite));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolMoy.MSG_ERROR_INCUBATION_NOT_UNITEPRO_DB), cunite));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(datatype, dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);

        if (dbdvum == null) {

            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordIncubationSolMoy.BUNDLE_PATH_INCUBATION,
                    ProcessRecordIncubationSolMoy.MSG_ERROR_INCUBATION_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumidite));
        }
        RealNode realNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        Long fichier = mesuresLines.getOriginalLineNumber();
        final VersionFile versionfile = this.versionFileDAO.getById(versionFile.getId()).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesureIncubationSolMoy mesureIncubationSolMoy = getOrCreate(date_prel_sol, codeech, date_debut_incub, fichier, n_mineral, jour_incub, humidite_incub,
                    temperature_incub, masse_de_sol, code_interne_labo, numero_rep_analyse, labo_analyse, versionfile, echantillonsSol, ordre_manip);
            persistValeurIncubationSolMoy(valeur_moyenne, valeur_ecart_type, realNode, mesureIncubationSolMoy);
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {

        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);

        final ErrorsReport errorsReport = new ErrorsReport();

        try {

            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);
            final Map<LocalDate, List<IncubationSolMoyLineRecord>> mesuresMapLines = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<IncubationSolMoyLineRecord> ligneEnErreur = new TreeSet();

            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);

            }

            this.recordErrors(errorsReport);

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (DateTimeException ex) {
            LoggerFactory.getLogger(ProcessRecordIncubationSolMoy.class.getName()).error(ex.getMessage(), ex);
        }
    }

    private long readLines(CSVParser parser, Map<LocalDate, List<IncubationSolMoyLineRecord>> mesuresMapLines,
            long lineCount, ErrorsReport errorsReport) throws IOException {
        String[] values;

        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;

            final LocalDate date_prel_sol = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeech = cleanerValues.nextToken();
            final int ordre_manip = Integer.parseInt(cleanerValues.nextToken());
            final float masse_de_sol = Float.parseFloat(cleanerValues.nextToken());
            final String humidite_incub = cleanerValues.nextToken();
            final float temperature_incub = Float.parseFloat(cleanerValues.nextToken());
            final float n_mineral = Float.parseFloat(cleanerValues.nextToken());
            final LocalDate date_debut_incub = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String labo_analyse = cleanerValues.nextToken();
            final String code_interne_labo = cleanerValues.nextToken();
            final int numero_rep_analyse = Integer.parseInt(cleanerValues.nextToken());
            final String codevariable = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();
            final int jour_incubation = Integer.parseInt(cleanerValues.nextToken());
            final float valeur_moyenne = Float.parseFloat(cleanerValues.nextToken());
            final float valeur_ecart_type = Float.parseFloat(cleanerValues.nextToken());

            final IncubationSolMoyLineRecord line = new IncubationSolMoyLineRecord(date_prel_sol, codeech, ordre_manip, masse_de_sol,
                    humidite_incub, temperature_incub, n_mineral, date_debut_incub, labo_analyse,
                    code_interne_labo, numero_rep_analyse, jour_incubation, lineCount, codevariable, valeur_moyenne,
                    valeur_ecart_type, codemethode, codeunite, codehumidite);

            try {
                if (!mesuresMapLines.containsKey(date_prel_sol)) {
                    mesuresMapLines.put(date_prel_sol, new LinkedList<IncubationSolMoyLineRecord>());
                }
                mesuresMapLines.get(date_prel_sol).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        date_prel_sol, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void buildLines(VersionFile versionFile, ISessionPropertiesPRO sessionProperties, ErrorsReport errorsReport,
            Map<LocalDate, List<IncubationSolMoyLineRecord>> mesuresMapLines, SortedSet<IncubationSolMoyLineRecord> ligneEnErreur) throws PersistenceException {

        Iterator<Entry<LocalDate, List<IncubationSolMoyLineRecord>>> iterator = mesuresMapLines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<IncubationSolMoyLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (IncubationSolMoyLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    private void recordErrors(ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureIncubationSolMoyDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IMesureIncubationSolMoyDAO<MesureIncubationSolMoy> getMesureIncubationSolMoyDAO() {
        return mesureIncubationSolMoyDAO;
    }

    public void setMesureIncubationSolMoyDAO(IMesureIncubationSolMoyDAO<MesureIncubationSolMoy> mesureIncubationSolMoyDAO) {
        this.mesureIncubationSolMoyDAO = mesureIncubationSolMoyDAO;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public IEchantillonSolDAO getEchantillonSolDAO() {
        return echantillonSolDAO;
    }

    public void setEchantillonSolDAO(IEchantillonSolDAO echantillonSolDAO) {
        this.echantillonSolDAO = echantillonSolDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    public IVersionFileDAO getVersionFileDAO() {
        return versionFileDAO;
    }

    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    public IValeurIncubationSolMoyDAO getValeurIncubationSolMoyDAO() {
        return valeurIncubationSolMoyDAO;
    }

    public void setValeurIncubationSolMoyDAO(IValeurIncubationSolMoyDAO valeurIncubationSolMoyDAO) {
        this.valeurIncubationSolMoyDAO = valeurIncubationSolMoyDAO;
    }

    private MesureIncubationSolMoy getOrCreate(LocalDate date_prel_sol, String codeech, LocalDate date_debut_incub, Long fichier, float n_mineral, int jour_incub, String humidite_incub, float temperature_incub, float masse_de_sol, String code_interne_labo, int numero_rep_analyse, String labo_analyse, VersionFile versionfile, EchantillonsSol echantillonsSol, int ordre_manip) throws PersistenceException {
        String datePrelSol = null;
        String intStringNumRep = null;
        String intStringJourIncub = null;
        String format = "dd/MMM/yyyy";

        datePrelSol = DateUtil.getUTCDateTextFromLocalDateTime(date_prel_sol, format);

        intStringNumRep = Integer.toString(numero_rep_analyse);
        intStringJourIncub = Integer.toString(jour_incub);

        String key = datePrelSol + "-" + codeech + "-" + ordre_manip + "-" + intStringNumRep + "-" + intStringJourIncub;
        MesureIncubationSolMoy mesureIncubationSolMoy = mesureIncubationSolMoyDAO.getByKey(key).orElse(null);

        if (mesureIncubationSolMoy == null) {
            mesureIncubationSolMoy = new MesureIncubationSolMoy(date_prel_sol, date_debut_incub, jour_incub, humidite_incub, temperature_incub,
                    n_mineral, masse_de_sol, code_interne_labo, numero_rep_analyse, labo_analyse, fichier, versionfile, echantillonsSol,
                    ordre_manip);

            mesureIncubationSolMoy.setLocalDate_prel_sol(date_prel_sol);
            mesureIncubationSolMoy.setLocalDate_debut_incub(date_debut_incub);
            mesureIncubationSolMoy.setJour_incub(jour_incub);
            mesureIncubationSolMoy.setHumidite_incub(humidite_incub);
            mesureIncubationSolMoy.setTemperature_incub(temperature_incub);
            mesureIncubationSolMoy.setN_mineral_apporte(n_mineral);
            mesureIncubationSolMoy.setMasse_de_sol(masse_de_sol);
            mesureIncubationSolMoy.setCode_interne_labo(code_interne_labo);
            mesureIncubationSolMoy.setNumero_rep_analyse(numero_rep_analyse);
            mesureIncubationSolMoy.setLabo_analyse(labo_analyse);
            mesureIncubationSolMoy.setLigneFichierEchange(fichier);
            mesureIncubationSolMoy.setVersionfile(versionfile);
            mesureIncubationSolMoy.setEchantillonsSol(echantillonsSol);
            mesureIncubationSolMoy.setOrdre_manip(ordre_manip);
            mesureIncubationSolMoy.setKeymesure(key);
            mesureIncubationSolMoyDAO.saveOrUpdate(mesureIncubationSolMoy);

        }

        return mesureIncubationSolMoy;
    }

    private void persistValeurIncubationSolMoy(float valeur_moyenne, float valeur_ecart_type, RealNode realNode, MesureIncubationSolMoy mesureIncubationSolMoy) throws PersistenceException {
        final ValeurIncubationSolMoy valeurIncubSolMoy = valeurIncubationSolMoyDAO.getByKeys(realNode, mesureIncubationSolMoy, valeur_ecart_type).orElse(null);
        createOrUpdateValeurIncubationSolMoy(valeur_moyenne, valeur_ecart_type, realNode, mesureIncubationSolMoy, valeurIncubSolMoy);
    }

    private void createOrUpdateValeurIncubationSolMoy(float valeur_moyenne, float valeur_ecart_type, RealNode realNode, MesureIncubationSolMoy mesureIncubationSolMoy, ValeurIncubationSolMoy valeurIncubSolMoy) throws PersistenceException {
        if (valeurIncubSolMoy == null) {
            ValeurIncubationSolMoy valeurIncubationSM = new ValeurIncubationSolMoy(valeur_moyenne, valeur_ecart_type, mesureIncubationSolMoy, realNode);
            valeurIncubationSM.setRealNode(realNode);
            valeurIncubationSM.setValeur(valeur_moyenne);
            valeurIncubationSM.setEcart_type(valeur_ecart_type);
            valeurIncubationSM.setMesureIncubationSolMoy(mesureIncubationSolMoy);

            createValeurISM(valeurIncubationSM);
        } else {
            updateValeurISM(realNode, mesureIncubationSolMoy, valeur_moyenne, valeur_ecart_type, valeurIncubSolMoy);
        }
    }

    private void createValeurISM(ValeurIncubationSolMoy valeurIncubationSM) throws PersistenceException {
        valeurIncubationSolMoyDAO.saveOrUpdate(valeurIncubationSM);
    }

    private void updateValeurISM(RealNode realNode, MesureIncubationSolMoy mesureIncubationSolMoy, float valeur_moyenne, float valeur_ecart_type, ValeurIncubationSolMoy valeurIncubSolMoy) throws PersistenceException {
        valeurIncubSolMoy.setRealNode(realNode);
        valeurIncubSolMoy.setMesureIncubationSolMoy(mesureIncubationSolMoy);
        valeurIncubSolMoy.setEcart_type(valeur_ecart_type);
        valeurIncubSolMoy.setValeur(valeur_moyenne);
        valeurIncubationSolMoyDAO.saveOrUpdate(valeurIncubSolMoy);
    }

}
