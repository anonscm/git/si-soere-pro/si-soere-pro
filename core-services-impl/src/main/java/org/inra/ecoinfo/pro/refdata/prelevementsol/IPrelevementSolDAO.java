package org.inra.ecoinfo.pro.refdata.prelevementsol;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.Placette;

/**
 *
 * @author ptcherniati
 */
public interface IPrelevementSolDAO extends IDAO<PrelevementSol> {

    /**
     *
     * @return
     */
    List<PrelevementSol> getAll();

    /**
     *
     * @param date_prelevement
     * @param limit_superieur
     * @param limit_inferieur
     * @param traitement
     * @param dispositif
     * @param bloc
     * @param pelementataire
     * @param placette
     * @return
     */
    Optional<PrelevementSol> getByNKey(LocalDate date_prelevement, int limit_superieur, int limit_inferieur, DescriptionTraitement traitement, Dispositif dispositif, Bloc bloc, ParcelleElementaire pelementataire, Placette placette);

    /**
     *
     * @param codesol
     * @return
     */
    Optional<PrelevementSol> getByNKey(String codesol);
}
