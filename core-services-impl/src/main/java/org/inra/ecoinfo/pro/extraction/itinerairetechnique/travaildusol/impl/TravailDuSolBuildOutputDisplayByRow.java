package org.inra.ecoinfo.pro.extraction.itinerairetechnique.travaildusol.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDatatypeManager;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.ITKExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class TravailDuSolBuildOutputDisplayByRow extends AbstractITKOutputBuilder<MesureTravailDuSol> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_TRAVAILSOL";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.itk.itk-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    static final String PATTERB_CSV_23_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";
    static final String LIST_COLUMN_VARIABLE = "LIST_COLUMN_VARIABLE";

    IVariablesPRODAO variPRODAO;

    final ComparatorVariable comparator = new ComparatorVariable();

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                TravailDuSolBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        TravailDuSolBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        TravailDuSolBuildOutputDisplayByRow.HEADER_RAW_DATA));

    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(ITKExtractor.CST_RESULT_EXTRACTION_TRAVAILSOL_CODE)
                .get(TravailDuSolExtractor.MAP_INDEX_TRAVAIL_DUSOL) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(ITKExtractor.CST_RESULT_EXTRACTION_TRAVAILSOL_CODE)
                        .get(TravailDuSolExtractor.MAP_INDEX_TRAVAIL_DUSOL).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ITKExtractor.CST_RESULT_EXTRACTION_TRAVAILSOL_CODE));
        return null;
    }

    /**
     *
     * @param mesuresTravailSol
     * @param mesuresTravailDuSolsMap
     */
    @Override
    protected void buildmap(List<MesureTravailDuSol> mesuresTravailSol, SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureTravailDuSol>>> mesuresTravailDuSolsMap) {
        java.util.Iterator<MesureTravailDuSol> itMesure = mesuresTravailSol
                .iterator();
        while (itMesure.hasNext()) {
            MesureTravailDuSol mesureTravailDuSol = itMesure
                    .next();
            DescriptionTraitement echan = mesureTravailDuSol.getDescriptionTraitement();
            Long siteId = echan.getDispositif().getId();
            if (mesuresTravailDuSolsMap.get(siteId) == null) {
                mesuresTravailDuSolsMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresTravailDuSolsMap.get(siteId).get(echan) == null) {
                mesuresTravailDuSolsMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresTravailDuSolsMap.get(siteId).get(echan).put(mesureTravailDuSol.getDatedebut(), mesureTravailDuSol);
            itMesure.remove();
        }

    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedRecolteCoupeVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresTravailDuSolsMap
     */
    @Override
    protected void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedRecolteCoupeVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureTravailDuSol>>> mesuresTravailDuSolsMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresTravailDuSolsMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureTravailDuSol>>> mesuresTravailduSolsMap, IntervalDate selectedIntervalDate) {
        try {
            String currentDispositif;
            String curentplacette;
            String currentTraitement;
            String currentParcelle;
            LocalDate datefin;
            String intervention;
            String materielt1;
            String materielt2;
            String materielt3;
            String niveau;
            String culture;
            String bbch;
            String stade;
            String typeobs;
            String observation;
            String commentaire;
            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    TravailDuSolBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    TravailDuSolBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE).split(";"));
            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureTravailDuSol>> mesureProduitBrutsMapByDisp = mesuresTravailduSolsMap
                        .get(dispositif.getId());
                if (mesureProduitBrutsMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesureTravailDuSol>>> itEchan = mesureProduitBrutsMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesureTravailDuSol>> echanEntry = itEchan
                            .next();
                    DescriptionTraitement echantillon = echanEntry.getKey();
                    SortedMap<LocalDate, MesureTravailDuSol> mesureTravailSolMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                    for (Entry<LocalDate, MesureTravailDuSol> entrySet : mesureTravailSolMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureTravailDuSol mesureTravailDuSol = entrySet.getValue();
                        currentDispositif = mesureTravailDuSol.getCodedispositif();
                        currentTraitement = mesureTravailDuSol.getDescriptionTraitement().getCode();
                        currentParcelle = mesureTravailDuSol.getNomparcelle();
                        curentplacette = mesureTravailDuSol.getNomplacette();
                        datefin = mesureTravailDuSol.getDatefin();
                        intervention = mesureTravailDuSol.getNomintervention();
                        materielt1 = mesureTravailDuSol.getMaterieltravailsol1();
                        materielt2 = mesureTravailDuSol.getMaterieltravailsol2();
                        materielt3 = mesureTravailDuSol.getMaterieltravailsol3();

                        culture = mesureTravailDuSol.getNomculture();
                        bbch = mesureTravailDuSol.getCodebbch();
                        stade = mesureTravailDuSol.getPrecisionstade();
                        typeobs = mesureTravailDuSol.getTypeobservation();
                        observation = mesureTravailDuSol.getNomobservation();
                        niveau = mesureTravailDuSol.getNiveauatteint();
                        commentaire = mesureTravailDuSol.getCommentaire();

                        String genericPattern = LineDataFixe(date, currentDispositif, currentTraitement, currentParcelle, curentplacette, datefin,
                                intervention, materielt1, materielt2, materielt3, culture,
                                bbch, stade, typeobs, observation, niveau, commentaire);
                        out.print(genericPattern);
                        LineDataVariable(mesureTravailDuSol, out, listVariable);
                    }
                    itEchan.remove();
                }
            }
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentTraitement, String currentParcelle,
            String courentPlacette, LocalDate datefin, String intervention, String materielt1, String materielt2, String materielt3,
            String culture, String bbch, String stade, String typeobs, String observation, String niveau,
            String commentaire) {
        String genericPatternTS = String.format(PATTERB_CSV_23_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentTraitement,
                currentParcelle,
                courentPlacette,
                datefin, intervention, materielt1, materielt2, materielt3,
                culture,
                bbch, stade, typeobs, observation, niveau, commentaire);
        return genericPatternTS;
    }

    private void LineDataVariable(MesureTravailDuSol mesureTravailSol, PrintStream out, List<String> listVariable) {
        //Localisation précise;Profondeur de travail;Conditions humidité air;Conditions température(°C);Vitesse du vent;Niveau atteint
        final List<ValeurTravailDuSol> valeurtravailsol = mesureTravailSol.getValeurtravailsol();
        listVariable.forEach((variableColumnName) -> {
            for (Iterator<ValeurTravailDuSol> ValeurIterator = valeurtravailsol.iterator(); ValeurIterator.hasNext();) {
                ValeurTravailDuSol valeur = ValeurIterator.next();
                if (!((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
                    continue;
                }
                String line;
                line = String.format(";%s", getValeurToString(valeur));
                out.print(line);
                ValeurIterator.remove();
            }
        });
        out.println();
    }

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return TravailDuSolExtractor.MAP_INDEX_TRAVAIL_DUSOL;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IITKDatatypeManager.CODE_DATATYPE_TRAVAIL_DU_SOL;
    }

    private String getValeurToString(ValeurTravailDuSol valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else if (valeur.getListeItineraire() != null) {
            return valeur.getListeItineraire().getListe_valeur();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(TravailDuSolBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
