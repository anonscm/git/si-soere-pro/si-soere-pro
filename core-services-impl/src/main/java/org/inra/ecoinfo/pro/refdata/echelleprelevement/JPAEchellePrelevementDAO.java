/**
 *
 */
package org.inra.ecoinfo.pro.refdata.echelleprelevement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAEchellePrelevementDAO extends AbstractJPADAO<EchellePrelevement> implements IEchellePrelevementDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.echelleprelevement.IEchellePrelevementDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<EchellePrelevement> getAll() {
        return getAllBy(EchellePrelevement.class, EchellePrelevement::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.echelleprelevement.IEchellePrelevementDAO #getByNKey(java.lang.String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @Override
    public Optional<EchellePrelevement> getByNKey(String nom) {
        CriteriaQuery<EchellePrelevement> query = builder.createQuery(EchellePrelevement.class);
        Root<EchellePrelevement> echellePrelevement = query.from(EchellePrelevement.class);
        query
                .select(echellePrelevement)
                .where(
                        builder.equal(echellePrelevement.get(EchellePrelevement_.nom), nom)
                );
        return getOptional(query);
    }
}
