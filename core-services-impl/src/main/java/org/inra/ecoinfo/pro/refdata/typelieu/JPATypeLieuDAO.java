/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typelieu;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeLieuDAO extends AbstractJPADAO<TypeLieu> implements ITypeLieuDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typelieu.ITypeLieuDAO#getByLibelle(java. lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<TypeLieu> getByNKey(String libelle) {
        CriteriaQuery<TypeLieu> query = builder.createQuery(TypeLieu.class);
        Root<TypeLieu> typeLieu = query.from(TypeLieu.class);
        query
                .select(typeLieu)
                .where(
                        builder.equal(typeLieu.get(TypeLieu_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.typelieu.ITypeLieuDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @Override
    public Optional<TypeLieu> getByCode(String code) {
        CriteriaQuery<TypeLieu> query = builder.createQuery(TypeLieu.class);
        Root<TypeLieu> typeLieu = query.from(TypeLieu.class);
        query
                .select(typeLieu)
                .where(
                        builder.equal(typeLieu.get(TypeLieu_.code), code)
                );
        return getOptional(query);
    }

}
