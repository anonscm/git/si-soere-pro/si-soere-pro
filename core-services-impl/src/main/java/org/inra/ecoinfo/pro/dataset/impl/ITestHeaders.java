/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public interface ITestHeaders {

    /**
     *
     */
    char SEPARATOR = ';';

    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return
     * @throws BusinessException
     */
    long testHeaders(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor)
            throws BusinessException;
    
}
