/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeechantillon;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface ITypeEchantillon extends IDAO<TypeEchantillon> {

    /**
     *
     * @return
     */
    List<TypeEchantillon> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<TypeEchantillon> getByNkey(String nom);

}
