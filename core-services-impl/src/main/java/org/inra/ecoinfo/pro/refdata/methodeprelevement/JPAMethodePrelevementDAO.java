/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methodeprelevement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAMethodePrelevementDAO extends AbstractJPADAO<MethodePrelevement> implements IMethodePrelevementDAO {

    /**
     *
     * @return
     */
    @Override
    public List<MethodePrelevement> getAll() {
        return getAll(MethodePrelevement.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<MethodePrelevement> getByNKey(String nom) {
        CriteriaQuery<MethodePrelevement> query = builder.createQuery(MethodePrelevement.class);
        Root<MethodePrelevement> methodePrelevement = query.from(MethodePrelevement.class);
        query
                .select(methodePrelevement)
                .where(
                        builder.equal(methodePrelevement.get(MethodePrelevement_.nom), nom)
                );
        return getOptional(query);
    }

}
