/*
 *
 */
package org.inra.ecoinfo.pro.refdata.etape;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class EtapeDAOImpl extends AbstractJPADAO<Etapes> implements IEtapeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Etapes> getAll() {
        CriteriaQuery<Etapes> query = builder.createQuery(Etapes.class);
        Root<Etapes> etapes = query.from(Etapes.class);
        query
                .select(etapes)
                .orderBy(builder.asc(etapes.get(Etapes_.etape_intitule)));
        return getResultList(query);
    }

    /**
     *
     * @param etape_intitule
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<Etapes> getByNKey(String etape_intitule) {
        CriteriaQuery<Etapes> query = builder.createQuery(Etapes.class);
        Root<Etapes> etapes = query.from(Etapes.class);
        query
                .select(etapes)
                .where(
                        builder.equal(etapes.get(Etapes_.etape_intitule), etape_intitule)
                );
        return getOptional(query);
    }
}
