/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizon;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAHorizonDAO extends AbstractJPADAO<Horizon> implements IHorizonDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Horizon> getAll() {
        return getAll(Horizon.class);
    }

    /**
     *
     * @param horizon
     * @return
     */
    @Override
    public Optional<Horizon> getByNKey(String horizon) {
        CriteriaQuery<Horizon> query = builder.createQuery(Horizon.class);
        Root<Horizon> forizon = query.from(Horizon.class);
        query
                .select(forizon)
                .where(
                        builder.equal(forizon.get(Horizon_.horizon_nom), horizon)
                );
        return getOptional(query);
    }

}
