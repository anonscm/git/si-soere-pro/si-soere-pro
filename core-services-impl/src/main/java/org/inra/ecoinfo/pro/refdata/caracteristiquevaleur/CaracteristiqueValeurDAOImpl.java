package org.inra.ecoinfo.pro.refdata.caracteristiquevaleur;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;


public class CaracteristiqueValeurDAOImpl extends AbstractJPADAO<CaracteristiqueValeur> implements ICaracteristiqueValeurDAO {


    @Override
    public List<CaracteristiqueValeur> getAll() {
        return getAllBy(CaracteristiqueValeur.class, CaracteristiqueValeur::getCv_nom);
    }

    @Override
    public Optional<CaracteristiqueValeur> getByCode(String code){
        CriteriaQuery<CaracteristiqueValeur> query = builder.createQuery(CaracteristiqueValeur.class);
        Root<CaracteristiqueValeur> carval = query.from(CaracteristiqueValeur.class);
        query
                .select(carval)
                .where(
                        builder.equal(carval.get(CaracteristiqueValeur_.cv_code), code)
                );
        return getOptional(query);
    }

    @Override
    public Optional<CaracteristiqueValeur> getByNKey(String nom){
        CriteriaQuery<CaracteristiqueValeur> query = builder.createQuery(CaracteristiqueValeur.class);
        Root<CaracteristiqueValeur> carval = query.from(CaracteristiqueValeur.class);
        query
                .select(carval)
                .where(
                        builder.equal(carval.get(CaracteristiqueValeur_.cv_nom), nom)
                );
        return getOptional(query);
    }

}
