/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.DataType;

/**
 *
 * @author adiankha
 */
public interface IDataTypeVariableQualifiantDAO extends IDAO<DatatypeVariableUnitePRO> {

    /**
     *
     * @return
     */
    List<DatatypeVariableUnitePRO> getAll();

    /**
     *
     * @param datatype
     * @param variable
     * @param unite
     * @param methode
     * @param humidite
     * @return
     */
    Optional<DatatypeVariableUnitePRO> getByNKey(String datatype, String variable, String unite, Methode methode, HumiditeExpression humidite);

    /**
     *
     * @param datatype
     * @param variable
     * @param unite
     * @param methode
     * @param humidite
     * @return
     */
    Optional<DatatypeVariableUnitePRO> getByNKey(DataType datatype, VariablesPRO variable, Unitepro unite, Methode methode, HumiditeExpression humidite);

    /**
     *
     * @param datatype
     * @param variablespro
     * @return
     */
    Optional<DatatypeVariableUnitePRO> getSemisKey(String datatype, String variablespro);

    /**
     *
     * @param datatypeName
     * @return
     */
    Map<String, DatatypeVariableUnitePRO> getAllDatatypeVariableUnitePROMethodeandCategorie(String datatypeName);

    /**
     *
     * @param name
     * @return
     */
    Optional<DatatypeVariableUnitePRO> getByNameVariable(String name);

    /**
     *
     * @param currentDatatype
     * @return
     */
    Optional<String> getNameVariableForDatatypeName(String currentDatatype);
}
