/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.incubation;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro_;
import org.inra.ecoinfo.pro.synthesis.incubationsolpro.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.incubationsolpro.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class IncubationSolProSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurIncubationSolPro, MesureIncubationSolPro> {

    @Override
    Class<ValeurIncubationSolPro> getValueClass() {
        return ValeurIncubationSolPro.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurIncubationSolPro, MesureIncubationSolPro> getMesureAttribute() {
        return ValeurIncubationSolPro_.mesureIncubationSolPro;
    }
}
