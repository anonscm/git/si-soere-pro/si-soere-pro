package org.inra.ecoinfo.pro.refdata.formephysique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.FormePhysique.FormePhysiques;

/**
 *
 * @author ptcherniati
 */
public interface IFormePhysiqueDAO extends IDAO<FormePhysiques> {

    /**
     *
     * @return
     */
    List<FormePhysiques> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<FormePhysiques> getByNKey(String code);

}
