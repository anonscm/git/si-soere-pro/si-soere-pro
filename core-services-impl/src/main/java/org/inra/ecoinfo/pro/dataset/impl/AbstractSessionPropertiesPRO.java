
package org.inra.ecoinfo.pro.dataset.impl;

import com.google.common.base.Strings;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public abstract class AbstractSessionPropertiesPRO implements ISessionPropertiesPRO {

    /**
     *
     */
    protected static final String CST_HYPHEN = "-";

    /**
     *
     */
    protected static final String CST_SLASH = "/";

    /**
     *
     */
    protected static final String REGEX_END_WORD = "$";

    /**
     *
     */
    protected static final String CST_UNDERSCORE = "_";

    /**
     *
     */
    protected static final String CST_NEW_LINE = "\n";

    /**
     *
     */
    protected static final String CST_DOT = ".";

    static final String INCLUSIVE_ADD_SORTED_MAP = "\0";

    /**
     *
     */
    protected static final String PATTERN_FILE_NAME = new StringBuffer("^(.*)")
            .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
            .append("(.*")
            .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
            .append(".*)")
            .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
            .append("(.*)")
            .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
            .append("(.*)")
            .append(".")
            .append(ISessionPropertiesPRO.FORMAT_FILE)
            .append(AbstractSessionPropertiesPRO.REGEX_END_WORD)
            .toString();

    static final long serialVersionUID = 1L;

    ILocalizationManager localizationManager;

    Dispositif dispositif = null;

    DataType datatype = null;

    DescriptionTraitement descriptionTraitement = null;

    Bloc bloc = null;

    ParcelleElementaire parcelleElementaire = null;

    Placette placette = null;

    EchantillonsSol echantillonsSol = null;

    EchantillonsProduit echantillonsProduit = null;

    ProduitDispositif produitDispositif = null;

    LocalDateTime dateDeDebut;

    LocalDateTime dateDeFin;

    /**
     * The date format.
     */
    String dateFormat = DateUtil.DD_MM_YYYY;

    String commentaire;

    int version;

    volatile SortedMap<String, Boolean> dates = new TreeMap();

    ITestDuplicates doublonsLine;

    IDatasetConfiguration datasetConfiguration;

    /**
     *
     * @param stringDate
     * @throws BadExpectedValueException
     * @throws DateTimeException
     */
    @Override
    public void addDate(final String stringDate) throws BadExpectedValueException, DateTimeException {
        try {
            if (this.dates != null && stringDate != null) {
                this.dates.put(stringDate, true);
            }
        } catch (final Exception e) {
            String message = org.apache.commons.lang.StringUtils.EMPTY;
            String formatDateDebut;
            String formatDateFin;
            formatDateDebut = DateUtil.getUTCDateTextFromLocalDateTime(this.dateDeDebut, DateUtil.DD_MM_YYYY_HH_MM);
            formatDateFin = DateUtil.getUTCDateTextFromLocalDateTime(this.dateDeFin, DateUtil.DD_MM_YYYY_HH_MM);
            message = String.format(
                    this.getLocalizationManager().getMessage(ISessionPropertiesPRO.BUNDLE_NAME,
                            ISessionPropertiesPRO.PROPERTY_MSG_LocalDate_OFF_LIMITS), stringDate,
                    formatDateDebut, formatDateFin);
            throw new BadExpectedValueException(message, e);
        }
    }

    /**
     *
     * @param dateFin
     * @param chronoUnit
     * @param step
     * @param dateFormatForCompare
     */
    protected void buildTree(final LocalDateTime dateFin, final ChronoUnit chronoUnit, final int step,
            String dateFormatForCompare) {
        if (this.dateDeDebut != null && dateFin != null && !dateDeDebut.isAfter(dateFin)) {
            LocalDateTime localDateTimeDebut = dateDeDebut;
            LocalDateTime localDateTimeFin = dateFin.plus(step, chronoUnit);
            LocalDateTime currentDate = localDateTimeDebut;
            this.dates = this.dates.subMap(
                    DateUtil.getUTCDateTextFromLocalDateTime(localDateTimeDebut, dateFormatForCompare),
                    DateUtil.getUTCDateTextFromLocalDateTime(localDateTimeFin, dateFormatForCompare)
                    + AbstractSessionPropertiesPRO.INCLUSIVE_ADD_SORTED_MAP);
            int a = 0;
            while (currentDate.isBefore(localDateTimeFin)) {
                String utcDateTextFromLocalDate = DateUtil.getUTCDateTextFromLocalDateTime(currentDate, dateFormatForCompare);
                if (!this.dates.containsKey(utcDateTextFromLocalDate)) {
                    this.dates.put(utcDateTextFromLocalDate, Boolean.FALSE);
                } else {
                }
                currentDate = currentDate.plus(step, chronoUnit);
                a++;
            }
        } else {
            this.initDate();
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String getCommentaire() {
        return this.commentaire;
    }

    public LocalDateTime getDateDeDebut() {
        return this.dateDeDebut;
    }

    public LocalDateTime getDateDeFin() {
        return this.dateDeFin;
    }

    /**
     *
     * @return
     */
    @Override
    public String getDateFormat() {
        return this.dateFormat;
    }

    public SortedMap<String, Boolean> getDates() {
        return this.dates;
    }

    /**
     *
     * @return
     */
    @Override
    public ITestDuplicates getDoublonsLine() {
        return this.doublonsLine;
    }

    /**
     *
     * @return
     */
    @Override
    public ILocalizationManager getLocalizationManager() {
        return this.localizationManager;
    }

    /**
     *
     * @param version
     * @return
     */
    @Override
    public String getNomDeFichier(VersionFile version) {
        return version == null ? org.apache.commons.lang.StringUtils.EMPTY : version.getDataset().buildDownloadFilename(
                this.datasetConfiguration);
    }

    /**
     *
     * @return
     */
    @Override
    public Dispositif getDispositif() {
        return this.dispositif;
    }

    /**
     *
     * @return
     */
    public DataType getDatatype() {
        return this.datatype;
    }

    /**
     *
     * @return
     */
    @Override
    public ParcelleElementaire getParcelleElementaire() {
        return this.parcelleElementaire;
    }

    /**
     *
     * @return
     */
    @Override
    public DescriptionTraitement getDescriptionTraitement() {
        return this.descriptionTraitement;
    }

    /**
     *
     * @return
     */
    @Override
    public Bloc getBloc() {
        return this.bloc;
    }

    /**
     *
     * @return
     */
    @Override
    public Placette getPlacette() {
        return this.placette;
    }

    /**
     *
     * @return
     */
    @Override
    public int getVersion() {
        return this.version;
    }

    public void initDate() {
        this.dates = new TreeMap();
    }

    /**
     *
     * @param dispositif
     */
    @Override
    public void setDispositif(final Dispositif dispositif) {
        this.dispositif = dispositif;
    }

    /**
     *
     * @param datatype
     */
    @Override
    public void setDataType(final DataType datatype) {
        this.datatype = datatype;
    }

    /**
     *
     * @param parcelleElementaire
     */
    @Override
    public void setParcelleElementaire(final ParcelleElementaire parcelleElementaire) {
        this.parcelleElementaire = parcelleElementaire;
    }

    /**
     *
     * @param descriptionTraitement
     */
    @Override
    public void setDescriptionTraitement(final DescriptionTraitement descriptionTraitement) {
        this.descriptionTraitement = descriptionTraitement;
    }

    /**
     *
     * @param bloc
     */
    public void setBloc(final Bloc bloc) {
        this.bloc = bloc;
    }

    /**
     *
     * @return
     */
    @Override
    public abstract ITestDuplicates getTestDuplicates();

    /**
     *
     * @return
     */
    public abstract DataType getDataType();

    /**
     *
     * @param placette
     */
    @Override
    public void setPlacette(final Placette placette) {
        this.placette = placette;
    }

    /**
     *
     * @return
     */
    public EchantillonsSol getEchantillonsSol() {
        return this.echantillonsSol;
    }

    /**
     *
     * @param echantillonsSol
     */
    @Override

    public void setEchantillonsSol(EchantillonsSol echantillonsSol) {
        this.echantillonsSol = echantillonsSol;
    }

    /**
     *
     * @return
     */
    @Override
    public EchantillonsProduit getEchantillonsProduit() {
        return echantillonsProduit;
    }

    ///A VOIR

    /**
     *
     * @param echantillonsProduit
     */
    public void setEchantillonsProduit(EchantillonsProduit echantillonsProduit) {
        this.echantillonsProduit = echantillonsProduit;
    }

    /**
     *
     * @return
     */
    public ProduitDispositif getProduitDispositif() {
        return produitDispositif;
    }

    /**
     *
     * @param produitDispositif
     */
    public void setProduitDispositif(ProduitDispositif produitDispositif) {
        this.produitDispositif = produitDispositif;
    }

    /**
     *
     * @param commentaire
     */
    @Override
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @param dateFormat
     */
    @Override
    public final void setDateFormat(final String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public final void setDates(final SortedMap<String, Boolean> dates) {
        this.dates = dates;
    }

    /**
     *
     * @param doublonsLine
     */
    @Override
    public void setDoublonsLine(final ITestDuplicates doublonsLine) {
        this.doublonsLine = doublonsLine;
    }

    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(final ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param version
     */
    @Override
    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public void testDates(final BadsFormatsReport badsFormatsReport) {
        if (this.dateDeDebut == null || this.dateDeFin == null) {
            return;
        }
        if (this.dates == null || this.dates.size() < 1) {
            badsFormatsReport.addException(new BadExpectedValueException(this
                    .getLocalizationManager().getMessage(ISessionPropertiesPRO.BUNDLE_NAME,
                            ISessionPropertiesPRO.PROPERTY_MSG_UNDEFINED_PERIOD)));
        }
    }

    @Override
    public void testNonMissingDates(final BadsFormatsReport badsFormatsReport) {
        if (this.dates == null || this.dates.isEmpty()) {
            return;
        }
        this.dates.entrySet().stream().filter((dateEntry) -> (!dateEntry.getValue())).forEachOrdered((dateEntry) -> {
            try {
                String formatDate;
                String formatDatedebut;
                String formatDateFin;
                formatDate = DateUtil.getUTCDateTextFromLocalDateTime(DateUtil.readLocalDateFromText(RecorderPRO.YYYYMMJJ, dateEntry.getKey()), RecorderPRO.YYYYMMJJ);
                formatDatedebut = DateUtil.getUTCDateTextFromLocalDateTime(dateDeDebut, DateUtil.DD_MM_YYYY);
                formatDateFin = DateUtil.getUTCDateTextFromLocalDateTime(dateDeFin, DateUtil.DD_MM_YYYY);
                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderPRO.getPROMessageWithBundle(
                                ISessionPropertiesPRO.BUNDLE_NAME,
                                ISessionPropertiesPRO.PROPERTY_MSG_MISSING_LocalDate), formatDate,
                                formatDatedebut, formatDateFin)));
            } catch (final DateTimeException e) {
                LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME).error("can't parse date");
            }
        });
    }

    /**
     *
     * @return
     */
    public IDatasetConfiguration getDatasetConfiguration() {
        return datasetConfiguration;
    }

    /**
     *
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    @Override
    public void setDateDeDebut(final LocalDateTime dateDeDebut) {
        this.dateDeDebut = dateDeDebut;
    }

    @Override
    public void setDateDeFin(LocalDateTime dateDeFin) {
        this.dateDeFin = dateDeFin;
    }

    @Override
    public void setDateDeFin(final LocalDateTime dateDeFin, final ChronoUnit chronoUnit, final int step) {
        this.dateDeFin = dateDeFin;
        this.buildTree(dateDeFin, chronoUnit, step, RecorderPRO.YYYYMMJJ);
    }

    /**
     *
     * @param dateString
     * @param timeString
     * @return
     */
    public String dateToString(String dateString, String timeString) {
        if (Strings.isNullOrEmpty(dateString)) {
            return StringUtils.EMPTY;
        }
        if (Strings.isNullOrEmpty(timeString)) {
            timeString = "00:00:00";
        }
        LocalDate date;
        LocalTime time;
        date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateString);
        if ("24:00".equals(timeString) || "24:00:00".equals(timeString)) {
            date = date.plusDays(1);
            timeString = "00:00:00";
        }
        timeString = String.format("%s:00", timeString).substring(0, 8);
        time = DateUtil.readLocalTimeFromText(RecorderPRO.HH_MM_SS, timeString);
        return RecorderPRO.getDateTimeForCompareUTC(date, time);
    }

}
