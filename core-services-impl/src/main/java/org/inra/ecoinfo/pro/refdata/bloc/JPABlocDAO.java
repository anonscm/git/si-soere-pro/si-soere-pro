/**
 *
 */
package org.inra.ecoinfo.pro.refdata.bloc;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif_;

/**
 * @author sophie
 *
 */
public class JPABlocDAO extends AbstractJPADAO<Bloc> implements IBlocDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO#getAll()
     */
    @Override
    public List<Bloc> getAll() {
        return getAllBy(Bloc.class, Bloc::getNom);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO#getByNKey(java.lang .String, org.inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */
    @Override
    public Optional<Bloc> getByNKey(String nom, Dispositif dispositif) {
        CriteriaQuery<Bloc> query = builder.createQuery(Bloc.class);
        Root<Bloc> bloc = query.from(Bloc.class);
        query
                .select(bloc)
                .where(
                        builder.equal(bloc.get(Bloc_.nom), nom),
                        builder.equal(bloc.get(Bloc_.dispositif), dispositif)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO#getLstBlocByDispositif(org .inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Bloc> getLstBlocByDispositif(Dispositif dispositif) {
        CriteriaQuery<Bloc> query = builder.createQuery(Bloc.class);
        Root<Bloc> bloc = query.from(Bloc.class);
        query
                .select(bloc)
                .where(
                        builder.equal(bloc.get(Bloc_.dispositif), dispositif)
                );
        return getResultList(query);
    }

    @Override
    public Optional<Bloc> getByFindBloc(String nombloc, String codedisp) {
        CriteriaQuery<Bloc> query = builder.createQuery(Bloc.class);
        Root<Bloc> bloc = query.from(Bloc.class);
        Join<Bloc, Dispositif> dis = bloc.join(Bloc_.dispositif);
        query
                .select(bloc)
                .where(
                        builder.equal(bloc.get(Bloc_.nom), nombloc),
                        builder.equal(dis.get(Dispositif_.codeDispo), codedisp)
                );
        return getOptional(query);
    }
}
