/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.materiel;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.typeintervention.ITypeInterventionDAO;
import org.inra.ecoinfo.pro.refdata.typeintervention.TypeIntervention;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<Materiel> {

    private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_BAD_INTERVENTION_ITK = "PROPERTY_MSG_BAD_INTERVENTION_ITK";
    private String[] ListeTypePossibles;
    public IMaterielDAO materielDAO;
    ITypeInterventionDAO typeInterventionDAO;
    Properties comentEn;
    Properties libelleEn;
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String materiel_nom = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String materiel_intervention = tokenizerValues.nextToken();
                String materiel_description = tokenizerValues.nextToken();
                String materiel_source = tokenizerValues.nextToken();
                TypeIntervention typeIntervention = typeInterventionDAO.getByNKey(materiel_intervention).orElse(null);                
                Materiel dbMateriel = materielDAO.getByNKey(materiel_nom, typeIntervention)
                        .orElseThrow(() -> new BusinessException("can't find materiel"));
                materielDAO.remove(dbMateriel);
                
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            
            String[] values = parser.getLine();
            long line = 0;
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Materiel.NAME_ENTITY_JPA);
                
                final String materiel_nom = tokenizerValues.nextToken();
                final String materiel_intervention = tokenizerValues.nextToken();
                final String materiel_description = tokenizerValues.nextToken();
                final String materiel_source = tokenizerValues.nextToken();
                final String materiel_commentaire = tokenizerValues.nextToken();
                
                TypeIntervention typeIntervention = typeInterventionDAO.getByNKey(materiel_intervention).orElse(null);
                if (typeIntervention == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_INTERVENTION_ITK), line, materiel_intervention));
                }
                
                if (!errorsReport.hasErrors()) {
                    
                    persistMateriel(materiel_nom, materiel_description, materiel_source, typeIntervention, materiel_commentaire);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<Materiel> getAllElements() throws BusinessException {
        return materielDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Materiel materiel) throws BusinessException {
        
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : materiel.getMateriel_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null || materiel.getMateriel_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(materiel.getMateriel_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : (materiel.getTypeIntervention() != null ? materiel.getTypeIntervention().getType_intervention_nom() : ""), ListeTypePossibles,
                        null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : materiel.getMateriel_description(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : materiel.getMateriel_source(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : materiel.getMateriel_commentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(materiel == null || materiel.getMateriel_commentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comentEn.getProperty(materiel.getMateriel_commentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        
        return lineModelGridMetadata;
    }
    
    private void persistMateriel(String materiel_nom, String materiel_description, String materiel_source, TypeIntervention typeIntervention, String materiel_commentaire) throws BusinessException {
        final Materiel dbmat = materielDAO.getByNKey(materiel_nom, typeIntervention).orElse(null);
        createOrUpdateMateriel(materiel_nom, materiel_description, materiel_source, typeIntervention, materiel_commentaire, dbmat);
    }
    
    private void createOrUpdateMateriel(String materiel_nom, String materiel_description, String materiel_source, TypeIntervention typeIntervention, String materiel_commentaire, Materiel dbmat) throws BusinessException {
        if (dbmat == null) {
            Materiel materiel = new Materiel(materiel_nom, materiel_description, materiel_source, typeIntervention, materiel_commentaire);
            
            materiel.setMateriel_nom(materiel_nom);
            materiel.setMateriel_description(materiel_description);
            materiel.setMateriel_source(materiel_source);
            materiel.setTypeIntervention(typeIntervention);
            materiel.setMateriel_commentaire(materiel_commentaire);
            createMateriel(materiel);
            
        } else {
            updateMateriel(materiel_nom, materiel_description, materiel_source, typeIntervention, materiel_commentaire, dbmat);
        }
    }
    
    private void createMateriel(Materiel materiel) throws BusinessException {
        try {
            materielDAO.saveOrUpdate(materiel);
            materielDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create materiel");
        }
    }
    
    private void updateMateriel(String materiel_nom, String materiel_description, String materiel_source, TypeIntervention typeIntervention, String materiel_commentaire, Materiel dbmat) throws BusinessException {
        try {
            dbmat.setMateriel_nom(materiel_nom);
            dbmat.setMateriel_description(materiel_description);
            dbmat.setMateriel_source(materiel_source);
            dbmat.setTypeIntervention(typeIntervention);
            dbmat.setMateriel_commentaire(materiel_commentaire);
            materielDAO.saveOrUpdate(dbmat);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update materiel");
        }
    }
    
    @Override
    protected ModelGridMetadata<Materiel> initModelGridMetadata() {
        InterventionPossibles();
        comentEn = localizationManager.newProperties(Materiel.NAME_ENTITY_JPA, Materiel.JPA_COLUMN_COMMENTAIRE, Locale.ENGLISH);
        libelleEn = localizationManager.newProperties(Materiel.NAME_ENTITY_JPA, Materiel.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    private void InterventionPossibles()  {
        
        List<TypeIntervention> groupetype = typeInterventionDAO.getAll(TypeIntervention.class);
        String[] listeIntPossibles = new String[groupetype.size() + 1];
        listeIntPossibles[0] = "";
        int index = 1;
        for (TypeIntervention typeIntervention : groupetype) {
            listeIntPossibles[index++] = typeIntervention.getType_intervention_nom();
        }
        this.ListeTypePossibles = listeIntPossibles;
    }
    
    public IMaterielDAO getMaterielDAO() {
        return materielDAO;
    }
    
    public void setMaterielDAO(IMaterielDAO materielDAO) {
        this.materielDAO = materielDAO;
    }
    
    public String[] getListeTypePossibles() {
        return ListeTypePossibles;
    }
    
    public void setListeTypePossibles(String[] ListeTypePossibles) {
        this.ListeTypePossibles = ListeTypePossibles;
    }
    
    public ITypeInterventionDAO getTypeInterventionDAO() {
        return typeInterventionDAO;
    }
    
    public void setTypeInterventionDAO(ITypeInterventionDAO typeInterventionDAO) {
        this.typeInterventionDAO = typeInterventionDAO;
    }
    
}
