/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.IMesureRecolteCoupeDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe_;

/**
 *
 * @author adiankha
 */
public class JPAMesureRecolteCoupeDAO extends AbstractJPADAO<MesureRecolteCoupe> implements IMesureRecolteCoupeDAO<MesureRecolteCoupe> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesureRecolteCoupe> getByKeys(String keymesure) {
        CriteriaQuery<MesureRecolteCoupe> query = builder.createQuery(MesureRecolteCoupe.class);
        Root<MesureRecolteCoupe> m = query.from(MesureRecolteCoupe.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureRecolteCoupe_.keymesure), keymesure)
                );
        return getOptional(query);
    }

}
