package org.inra.ecoinfo.pro.refdata.environnement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.activiteindustrielle.Activiteindustrielle;
import org.inra.ecoinfo.pro.refdata.activiteindustrielle.IActiviteIndustrielleDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<Environnement> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_ACTIVITEINDUSTRIELLE = "PROPERTY_MSG_ACTIVITEINDUSTRIELLE";
    private static final String PROPERTY_MSG_ENVIRONNEMENT_BAD_AI = "PROPERTY_MSG_ENVIRONNEMENT_BAD_AI";
    IEnvironnementDAO environnementDAO;
    IActiviteIndustrielleDAO activiteDAO;
    private String[] listeactiviteindustrielle;
    Properties CommentEn;

    private void createEnvironnement(Environnement environnement) throws BusinessException {
        try {
            environnementDAO.saveOrUpdate(environnement);
            environnementDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create environnement");
        }
    }

    private void updateEnvironnement(double axe, double agglo, String vent, String type,
            Activiteindustrielle activite, String comment, double distanceai, Environnement dbenvironnement) {
        dbenvironnement.setDistanceaxeroutier(axe);
        dbenvironnement.setDistanceagglomeration(agglo);
        dbenvironnement.setDirectionventdominant(vent);
        dbenvironnement.setTypeemission(type);
        dbenvironnement.setActiviteindustrielle(activite);
        dbenvironnement.setCommentaire(comment);
        dbenvironnement.setDistanceai(distanceai);
    }

    private void createOrUpdateEnvironnement(ErrorsReport errorsReport, double axe, double agglo, String vent, String type,
            Activiteindustrielle activite, String comment, double distanceai, Environnement dbenvironnement) throws BusinessException {
        if (dbenvironnement == null) {
            Environnement environnement = new Environnement(axe, agglo, vent, type, activite, distanceai, comment);
            environnement.setDistanceaxeroutier(axe);
            environnement.setDistanceagglomeration(agglo);
            environnement.setDirectionventdominant(vent);
            environnement.setTypeemission(type);
            Activiteindustrielle activites = activiteDAO.getByNKey(activite.getNom()).orElse(null);
            if (activites == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ACTIVITEINDUSTRIELLE), activite.getNom()));
            }
            environnement.setActiviteindustrielle(activite);
            environnement.setCommentaire(comment);
            createEnvironnement(environnement);
        } else {
            updateEnvironnement(axe, agglo, vent, type, activite, comment, distanceai, dbenvironnement);
        }
    }

    private void persistEnvironnement(ErrorsReport errorsReport, double axe, double agglo, String vent, String type,
            Activiteindustrielle activite, String comment, double distanceai) throws BusinessException, BusinessException {
        Environnement dbenvironnement = environnementDAO.getByNKey(axe, agglo, vent).orElse(null);
        createOrUpdateEnvironnement(errorsReport, axe, agglo, vent, type, activite, comment, distanceai, dbenvironnement);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Environnement environnement) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getDistanceaxeroutier() == 0.0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getDistanceaxeroutier(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getDistanceagglomeration() == 0.0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getDistanceagglomeration(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getDirectionventdominant() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getDirectionventdominant(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getActiviteindustrielle().getNom() != null
                        ? environnement.getActiviteindustrielle().getNom() : "",
                        listeactiviteindustrielle, null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getDistanceai() == EMPTY_DOUBLE_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getDistanceai(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getTypeemission() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getTypeemission(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : environnement.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(environnement == null || environnement.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : CommentEn.getProperty(environnement.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        return lineModelGridMetadata;

    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Environnement.NAME_ENTITY_JPA);
                String axe = tokenizerValues.nextToken();
                double axee = Double.parseDouble(axe);
                String agglo = tokenizerValues.nextToken();
                double agglom = Double.parseDouble(agglo);
                String vent = tokenizerValues.nextToken();
                Environnement dbenvi = environnementDAO.getByNKey(axee, agglom, vent)
                        .orElseThrow(() -> new BusinessException("can't find environnement"));
                environnementDAO.remove(dbenvi);
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Environnement> getAllElements() throws BusinessException {
        return environnementDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Environnement.NAME_ENTITY_JPA);
                double axee = verifieDouble(tokenizerValues, line, true, errorsReport);
                double agglom = verifieDouble(tokenizerValues, line, true, errorsReport);
                String vent = tokenizerValues.nextToken();
                int indexai = tokenizerValues.currentTokenIndex();
                String activite = tokenizerValues.nextToken();
                double xdai = verifieDouble(tokenizerValues, line, false, errorsReport);
                String type = tokenizerValues.nextToken();
                String comment = tokenizerValues.nextToken();
                Activiteindustrielle dbactivite = activiteDAO.getByNKey(activite).orElse(null);
                if (dbactivite == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ENVIRONNEMENT_BAD_AI), line, indexai, activite));
                }

                if (!errorsReport.hasErrors()) {
                    persistEnvironnement(errorsReport, axee, agglom, vent, type, dbactivite, comment, xdai);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    private void listeAIPossibles() {
        List<Activiteindustrielle> groupeai = activiteDAO.getAll();
        String[] listeactiviteindustrielle = new String[groupeai.size() + 1];
        listeactiviteindustrielle[0] = "";
        int index = 1;
        for (Activiteindustrielle activite : groupeai) {
            listeactiviteindustrielle[index++] = activite.getNom();
        }
        this.listeactiviteindustrielle = listeactiviteindustrielle;
    }

    @Override
    protected ModelGridMetadata<Environnement> initModelGridMetadata() {
        listeAIPossibles();
        CommentEn = localizationManager.newProperties(Environnement.NAME_ENTITY_JPA, Environnement.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public IEnvironnementDAO getEnvironnementDAO() {
        return environnementDAO;
    }

    /**
     *
     * @param environnementDAO
     */
    public void setEnvironnementDAO(IEnvironnementDAO environnementDAO) {
        this.environnementDAO = environnementDAO;
    }

    /**
     *
     * @return
     */
    public IActiviteIndustrielleDAO getActiviteDAO() {
        return activiteDAO;
    }

    /**
     *
     * @param activiteDAO
     */
    public void setActiviteDAO(IActiviteIndustrielleDAO activiteDAO) {
        this.activiteDAO = activiteDAO;
    }

}
