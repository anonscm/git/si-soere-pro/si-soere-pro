/**
 *
 */
package org.inra.ecoinfo.pro.refdata.lieu;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;

/**
 * @author sophie
 *
 */
public interface ILieuDAO extends IDAO<Lieu> {

    /**
     *
     * @return
     */
    List<Lieu> getAll();

    /**
     *
     * @param geolocalisation
     * @return
     */
    Optional<Lieu> getByGeolocalisation(Geolocalisation geolocalisation);

    /**
     *
     * @param nom
     * @return
     */
    Optional<Lieu> getByNKey(String nom);

}
