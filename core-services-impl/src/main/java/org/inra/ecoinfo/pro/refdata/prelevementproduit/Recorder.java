/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementproduit;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.echelleprelevement.IEchellePrelevementDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produitdispositif.IProduitDisposotifDAO;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<PrelevementProduit> {

    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PRODUIT = "PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PRODUIT";
    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_DISPOSITIF = "PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_DISPOSITIF";
    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_BLOC = "PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_BLOC";
    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PELEMT = "PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PELEMT";
    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PLACETTE = "PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PLACETTE";
    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUITDISPOSITIF_BAD = "PROPERTY_MSG_PRELEVEMENT_PRODUITDISPOSITIF_BAD";
    private static final String PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_TRAITEMENT = "PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_TRAITEMENT";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MESSAGE_BAD_DATE_FORMAT = "PROPERTY_MESSAGE_BAD_DATE_FORMAT";

    IPrelevementProduitDAO prelDAO;

    /**
     *
     */
    protected IProduitDisposotifDAO produitdispositifDAO;
    IEchellePrelevementDAO echellePrelevementDAO;
    IDispositifDAO dispositifDAO;
    IBlocDAO blocDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IPlacetteDAO placetteDAO;
    IProduitDAO produitDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    private String[] listeDispoPossible;
    private String[] listeEchellePossibles;
    private String[] listeProduitPossibles;
    private ConcurrentMap<String, String[]> listeTraitPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listePlacettesPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listeBlocsPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listeParcelleEPossibles = new ConcurrentHashMap();
    private Map<String, String[]> listePROPossibles;
    Properties commentaireEn;

    private void createPrelementProduit(PrelevementProduit prelevementproduit) throws BusinessException {
        try {
            prelDAO.saveOrUpdate(prelevementproduit);
            prelDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prelevementproduit");
        }
    }

    private void updatePrelevementProduit(
            PrelevementProduit dbprel, DescriptionTraitement traitement,
            String outil, String commentaire, Bloc bloc, ParcelleElementaire pelementaire,
            Placette placette, int numero, String endroit) throws BusinessException {
        try {
            dbprel.setBloc(bloc);
            dbprel.setPelementaire(pelementaire);
            dbprel.setPlacette(placette);
            dbprel.setOutils(outil);
            dbprel.setCommentaire(commentaire);
            dbprel.setTraitement(traitement);
            dbprel.setNumerorepetition(numero);
            dbprel.setEndroit(endroit);
            prelDAO.saveOrUpdate(dbprel);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update prelevementproduit");
        }

    }

    private void createOrUpdatePrelevementProduit(LocalDate dateprel, String codeprelevement,
            PrelevementProduit dbprel, DescriptionTraitement traitement,
            String outil, String commentaire, ProduitDispositif produitdispositif, Bloc bloc, ParcelleElementaire pelementaire,
            Placette placette, int numero, String endroit) throws BusinessException {
        if (dbprel == null) {
            PrelevementProduit prelevementproduit = new PrelevementProduit(dateprel, outil, outil, commentaire, produitdispositif, traitement,
                    bloc, pelementaire, placette, numero, endroit);
            prelevementproduit.setProduitdispositif(produitdispositif);
            prelevementproduit.setBloc(bloc);
            prelevementproduit.setPelementaire(pelementaire);
            prelevementproduit.setPlacette(placette);
            prelevementproduit.setDate_prelevement(dateprel);
            prelevementproduit.setCodeprelevement(codeprelevement);
            prelevementproduit.setOutils(outil);
            prelevementproduit.setCommentaire(commentaire);
            prelevementproduit.setTraitement(traitement);
            createPrelementProduit(prelevementproduit);
        } else {
            updatePrelevementProduit(dbprel, traitement, outil, commentaire, bloc, pelementaire, placette, numero, endroit);

        }

    }

    private void persitPrelevementProduit(LocalDate dateprel, String codeprelevement,
            String outil, String commentaire, DescriptionTraitement traitement,
            ProduitDispositif produitdispositif, Bloc bloc, ParcelleElementaire pelementaire,
            Placette placette, int numero, String endroit) throws BusinessException, BusinessException {
        PrelevementProduit dbprelevementProduit = prelDAO.getByNkey(dateprel, numero, produitdispositif, traitement, bloc, placette).orElse(null);
        createOrUpdatePrelevementProduit(dateprel, codeprelevement, dbprelevementProduit, traitement, outil, commentaire, produitdispositif, bloc, pelementaire, placette, numero, endroit);
    }

    private void initProduitDispositifPossibles() {
        Map<String, String[]> produitDispositif = new HashMap<>();
        Map<String, Set<String>> produitDispositifList = new HashMap<>();
        List<ProduitDispositif> groupeproduitdispositif = produitdispositifDAO.getAll(ProduitDispositif.class);
        groupeproduitdispositif.forEach((produitdispositif) -> {
            String dispo = produitdispositif.getDispositif().getCodeDispo();
            String prod = produitdispositif.getProduits().getProd_key();

            if (!produitDispositifList.containsKey(dispo)) {
                produitDispositifList.put(dispo, new TreeSet());
            }
            produitDispositifList.get(dispo).add(prod);
        });
        produitDispositifList.entrySet().forEach((entryProduit) -> {
            produitDispositif.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listePROPossibles = produitDispositif;
        Set<String> s1 = new HashSet<String>(Arrays.asList(listeDispoPossible));
        Set<String> s2 = listePROPossibles.keySet();
        s1.retainAll(s2);
        listeDispoPossible = s1.toArray(new String[s1.size()]);
    }

    private void initTraitementDispositifPossibles() {
        Map<String, List<String>> dispositifTraitList = new HashMap<>();
        descriptionTraitementDAO.getAll().stream()
                .forEach(dt -> {
                    List<String> traitementList = dispositifTraitList
                            .computeIfAbsent(dt.getDispositif().getNomDispositif_nomLieu(), k -> new LinkedList<>());
                    if(!traitementList.contains(dt.getTypeTraitement().getCode())){
                        traitementList.add(dt.getTypeTraitement().getCode());
                    }
                });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeTraitPossibles);
        String[] listeDispoPossible2 = readMapOfValuesPossibles(dispositifTraitList, listOfMapOfValuesPossibles, new LinkedList<String>());
        Set<String> s1 = new HashSet<String>(Arrays.asList(listeDispoPossible));
        Set<String> s2 = new HashSet<String>(Arrays.asList(listeDispoPossible2));
        s1.retainAll(s2);
        listeDispoPossible = s1.toArray(new String[s1.size()]);
    }

    private void initDispositifBlocPossibles() {
        Map<String, Map<String, Map<String, List<String>>>> dispBlocPEPlacette = new HashMap<String, Map<String, Map<String, List<String>>>>();
        placetteDAO.getAll(Placette.class)
                .stream()
                .forEach(pl -> {
                    dispBlocPEPlacette
                            .computeIfAbsent(pl.getParcelleElementaire().getBloc().getDispositif().getNomDispositif_nomLieu(), k -> new HashMap<String, Map<String, List<String>>>())
                            .computeIfAbsent(pl.getParcelleElementaire().getBloc().getNom(), k -> new HashMap<String, List<String>>())
                            .computeIfAbsent(pl.getParcelleElementaire().getName(), k -> new LinkedList<String>())
                            .add(pl.getNom());
                });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeBlocsPossibles);
        listOfMapOfValuesPossibles.add(listeParcelleEPossibles);
        listOfMapOfValuesPossibles.add(listePlacettesPossibles);
        listeDispoPossible = readMapOfValuesPossibles(dispBlocPEPlacette, listOfMapOfValuesPossibles, new LinkedList<String>());
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, PrelevementProduit.NAME_ENTITY_JPA);
                tokenizerValues.nextToken();
                final String date = tokenizerValues.nextToken();
                final String codeDispoLieu = tokenizerValues.nextToken();
                final String produit = tokenizerValues.nextToken();
                final String codetrait = tokenizerValues.nextToken();
                String bloc = tokenizerValues.nextToken();
                String nomparcelle = tokenizerValues.nextToken();
                String placette = tokenizerValues.nextToken();
                final int numero = Integer.parseInt(tokenizerValues.nextToken());
                final String endroit = tokenizerValues.nextToken();
                final String outil = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                String nomDispositif = Dispositif.getNomDispositif(codeDispoLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispoLieu);
                String codePrelevementProduit = PrelevementProduit.buildCodePrelevementProduit(date, numero, nomDispositif, nomLieu, produit, codetrait, bloc, nomparcelle, placette);
                PrelevementProduit dbprele = prelDAO.getByNKey(codePrelevementProduit)
                        .orElseThrow(()->new BusinessException("can't get prélèvement produit"));
                prelDAO.remove(dbprele);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, PrelevementProduit.NAME_ENTITY_JPA);
                final String date = tokenizerValues.nextToken();
                LocalDate localDate = null;  
                try {
                    localDate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, date);
                } catch (DateTimeParseException e) {
                    
                }
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String dispo = tokenizerValues.nextToken();
                int indexprod = tokenizerValues.currentTokenIndex();
                final String produit = tokenizerValues.nextToken();
                int indextrait = tokenizerValues.currentTokenIndex();
                final String codetrait = tokenizerValues.nextToken();
                int indexbloc = tokenizerValues.currentTokenIndex();
                String bloc = tokenizerValues.nextToken();
                int indexpe = tokenizerValues.currentTokenIndex();
                String nomParcelle = tokenizerValues.nextToken();
                int indexpla = tokenizerValues.currentTokenIndex();
                String placette = tokenizerValues.nextToken();
                final int numero = verifieInt(tokenizerValues, line, false, errorsReport);
                final String endroit = tokenizerValues.nextToken();
                final String outil = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                Produits dbprod = verifierExistProduit(produit, errorsReport, line, indexprod);
                String codeDispositif = Dispositif.getNomDispositif(dispo);
                String nomLieu = Dispositif.getNomLieu(dispo);
                Dispositif dbdispo = dispositifDAO.getByNKey(codeDispositif, nomLieu).orElse(null);
                if (dbdispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_DISPOSITIF), line, indexdispo, dispo));
                }
                ProduitDispositif dbprodispo = produitdispositifDAO.getByNKey(dbprod, dbdispo).orElse(null);
                if (dbprodispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUITDISPOSITIF_BAD), line, indexprod, indexdispo, dispo, produit));
                }
                DescriptionTraitement dbtraitement = descriptionTraitementDAO.getByNKey(codetrait, dbdispo).orElse(null);
                if (dbtraitement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_TRAITEMENT), line, indextrait, codetrait));
                }

                Bloc dbbloc = blocDAO.getByFindBloc(bloc, codeDispositif).orElse(null);
                if (dbbloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_BLOC), line, indexbloc, bloc));
                }

                ParcelleElementaire dbpe = parcelleElementaireDAO.getByNKey(nomParcelle, dbbloc).orElse(null);
                if (dbpe == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PELEMT), line, indexpe, nomParcelle));
                }

                Placette dbplacette = placetteDAO.getByNameAndParcelle(placette, dbpe).orElse(null);
                if (dbplacette == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PLACETTE), line, indexpla, placette, dbpe));
                }
                LocalDate finaldate = checkDateFormat(date, errorsReport, line);


                String codePrelevementProduit = PrelevementProduit.buildCodePrelevementProduit(date, numero, codeDispositif, nomLieu, produit, codetrait, bloc, nomParcelle, placette);
                createPrelevementProduit(errorsReport, localDate, codePrelevementProduit, outil, commentaire, dbtraitement, dbprodispo, dbbloc, dbpe, dbplacette, numero, endroit);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private LocalDate checkDateFormat(String madate, final ErrorsReport errorsReport, long line) throws NumberFormatException {
        try{
            return DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, madate);
        }catch(DateTimeParseException e){
              errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH,PROPERTY_MESSAGE_BAD_DATE_FORMAT), line, madate, DateUtil.DD_MM_YYYY));
              return null;
        }
    }


    private Produits verifierExistProduit(final String codepro, final ErrorsReport errorsReport, long line, int indexprod) throws BusinessException {
        Produits dbprod = produitDAO.getByNKey(codepro).orElse(null);
        if (dbprod == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRELEVEMENT_PRODUIT_BAD_PRODUIT), line, indexprod, codepro));
        }
        return dbprod;
    }

    private void createPrelevementProduit(final ErrorsReport errorsReport, LocalDate datep, String codeprelevement,
            String outil, String commentaire, DescriptionTraitement traitement,
            ProduitDispositif produitdispositif, Bloc bloc, ParcelleElementaire pelementaire,
            Placette placette, int numero, String endroit) throws BusinessException, PersistenceException {
        if (!errorsReport.hasErrors()) {
            persitPrelevementProduit(datep, codeprelevement, outil, commentaire, traitement, produitdispositif, bloc, pelementaire, placette, numero, endroit);

        }
    }

    @Override
    protected List<PrelevementProduit> getAllElements() throws BusinessException {
        return prelDAO.getAll();
    }

    String buildStringDateMiseEnService(final PrelevementProduit prelevementProduit) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (prelevementProduit != null && prelevementProduit.getDate_prelevement() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(prelevementProduit.getDate_prelevement(),this.datasetDescriptor.getColumns().get(0).getFormatType());
        }
        return dateDebut;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PrelevementProduit prelevementProduit) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementProduit == null || prelevementProduit.getDate_prelevement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.buildStringDateMiseEnService(prelevementProduit),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        String valeurProduit = prelevementProduit == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getProduitdispositif().getProduits().getProd_key() != null ? prelevementProduit.getProduitdispositif().getProduits().getProd_key() : "";
        ColumnModelGridMetadata columnPro = new ColumnModelGridMetadata(valeurProduit, listePROPossibles, null, true, false, true);

        String valeurdisp = prelevementProduit == null || prelevementProduit.getBloc() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getBloc().getDispositif().getNomDispositif_nomLieu();
        ColumnModelGridMetadata columnDisp = new ColumnModelGridMetadata(valeurdisp, listeDispoPossible, null, true, false, true);
        String valeurTrait = prelevementProduit == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getTraitement().getCode() != null ? prelevementProduit.getTraitement().getCode() : "";
        ColumnModelGridMetadata columnTrait = new ColumnModelGridMetadata(valeurTrait, listeTraitPossibles, null, true, false, true);
        String nomBloc = prelevementProduit == null ? "" : prelevementProduit.getBloc().getNom();
        ColumnModelGridMetadata columnBloc = new ColumnModelGridMetadata(prelevementProduit == null ? Constantes.STRING_EMPTY : nomBloc, listeBlocsPossibles,
                null, true, false, true);
        String valeurPE = prelevementProduit == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getPelementaire().getName() != null ? prelevementProduit.getPelementaire().getName() : "";
        ColumnModelGridMetadata columnPE = new ColumnModelGridMetadata(valeurPE, listeParcelleEPossibles, null, true, false, true);

        String valeurPlacette = prelevementProduit == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getPlacette().getNom() != null ? prelevementProduit.getPlacette().getNom() : "";
        ColumnModelGridMetadata columnPlacette = new ColumnModelGridMetadata(valeurPlacette, listePlacettesPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsDispositif = new LinkedList<ColumnModelGridMetadata>();

        List<ColumnModelGridMetadata> refsBloc = new LinkedList<ColumnModelGridMetadata>();

        List<ColumnModelGridMetadata> refsPE = new LinkedList<ColumnModelGridMetadata>();

        refsDispositif.add(columnPro);
        refsDispositif.add(columnTrait);
        refsDispositif.add(columnBloc);
        refsBloc.add(columnPE);
        refsPE.add(columnPlacette);

        columnTrait.setRefBy(columnDisp);
        columnBloc.setRefBy(columnDisp);
        columnDisp.setRefs(refsDispositif);
        columnPro.setValue(valeurProduit);

        columnPE.setRefBy(columnBloc);
        columnBloc.setRefs(refsBloc);

        columnPlacette.setRefBy(columnPE);
        columnPE.setRefs(refsPE);

        columnTrait.setValue(valeurTrait);
        columnBloc.setValue(nomBloc);
        columnPE.setValue(valeurPE);
        columnPlacette.setValue(valeurPlacette);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDisp);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPro);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTrait);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnBloc);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPE);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPlacette);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementProduit == null || prelevementProduit.getNumerorepetition() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getNumerorepetition(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementProduit == null || prelevementProduit.getEndroit() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getEndroit(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementProduit == null || prelevementProduit.getOutils() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getOutils(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementProduit == null || prelevementProduit.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementProduit.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementProduit == null || prelevementProduit.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(prelevementProduit.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<PrelevementProduit> initModelGridMetadata() {
        initDispositifBlocPossibles();
        initTraitementDispositifPossibles();
        commentaireEn = localizationManager.newProperties(PrelevementProduit.NAME_ENTITY_JPA, PrelevementProduit.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param prelDAO
     */
    public void setPrelDAO(IPrelevementProduitDAO prelDAO) {
        this.prelDAO = prelDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param blocDAO
     */
    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    /**
     *
     * @param parcelleElementaireDAO
     */
    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     *
     * @param placetteDAO
     */
    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    /**
     *
     * @param produitdispositifDAO
     */
    public void setProduitdispositifDAO(IProduitDisposotifDAO produitdispositifDAO) {
        this.produitdispositifDAO = produitdispositifDAO;
    }

    /**
     *
     * @param echellePrelevementDAO
     */
    public void setEchellePrelevementDAO(IEchellePrelevementDAO echellePrelevementDAO) {
        this.echellePrelevementDAO = echellePrelevementDAO;
    }

    /**
     *
     * @param descriptionTraitementDAO
     */
    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

}
