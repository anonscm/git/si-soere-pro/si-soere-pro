/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy_;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationIncubationSolProMoyDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurIncubationSolProMoy> deleteValeurs = builder.createCriteriaDelete(ValeurIncubationSolProMoy.class);
        Root<ValeurIncubationSolProMoy> valeur = deleteValeurs.from(ValeurIncubationSolProMoy.class);
        Subquery<MesureIncubationSolProMoy> subquery = deleteValeurs.subquery(MesureIncubationSolProMoy.class);
        Root<MesureIncubationSolProMoy> m = subquery.from(MesureIncubationSolProMoy.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureIncubationSolProMoy_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurIncubationSolProMoy_.mesureIncubationSolProMoy).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureIncubationSolProMoy> deleteSequence = builder.createCriteriaDelete(MesureIncubationSolProMoy.class);
        Root<MesureIncubationSolProMoy> sequence = deleteSequence.from(MesureIncubationSolProMoy.class);
        deleteSequence.where(builder.equal(sequence.get(MesureIncubationSolProMoy_.versionfile), version));
        delete(deleteSequence);
    }
}
