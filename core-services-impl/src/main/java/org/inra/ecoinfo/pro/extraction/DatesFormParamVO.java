package org.inra.ecoinfo.pro.extraction;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.faces.component.ValueHolder;
import javax.faces.event.AjaxBehaviorEvent;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;

/**
 * The Class DatesYearsContinuousFormParamVO.
 */
public class DatesFormParamVO extends AbstractDatesFormParam implements IDateFormParameter {

    /**
     * The Constant LABEL.
     */
    public static final String LABEL = "DatesYearsContinuousFormParam" ;

    /**
     *
     */
    public static final String PROPERTY_MSG_FROM_TO = "PROPERTY_MSG_FROM_TO" ;

    /**
     *
     */
    public static final String PROPERTY_MSG_FROM_TO_2 = "PROPERTY_MSG_FROM_TO_2" ;

    private LocalDate dateStart = null;

    private LocalDate dateEnd = null;

    /**
     * Instantiates a new dates years continuous form param vo.
     */
    public DatesFormParamVO() {
        super();
    }

    /**
     * Instantiates a new dates years continuous form param vo.
     *
     * @param localizationManager the localization manager
     */
    public DatesFormParamVO(ILocalizationManager localizationManager) {
        super(localizationManager);
        setPeriods(new LinkedList<>());
        getPeriods().add(buildNewMapPeriod());
    }

    /**
     * Adds the period years continuous.
     *
     * @return the string
     */
    public String addPeriodYearsContinuous() {
        getPeriods().add(buildNewMapPeriod());
        return null;
    }

    /**
     * Builds the continuous period.
     *
     * @param periodIndex the period index
     * @param periodStart the period start
     * @param periodEnd the period end
     * @param hQLAliasDate the h ql alias date
     * @throws DateTimeException the parse exception
     */
    protected void buildContinuousPeriod(int periodIndex, String periodStart, String periodEnd,
            String hQLAliasDate) throws DateTimeException {
        if (periodStart != null && periodEnd != null && periodStart.trim().length() > 0
                && periodEnd.trim().length() > 0) {
            final LocalDate formattedStartDate = DateUtil.readLocalDateFromText(AbstractDatesFormParam.DATE_FORMAT, periodStart);
            final String startParameterName = String.format("%s%d",
                    AbstractDatesFormParam.START_PREFIX, periodIndex);
            final String endParameterName = String.format("%s%d",
                    AbstractDatesFormParam.END_PREFIX, periodIndex);
            parametersMap.put(startParameterName, formattedStartDate);
            final LocalDate formattedEndDate = DateUtil.readLocalDateFromText(AbstractDatesFormParam.DATE_FORMAT, periodEnd);
            parametersMap.put(endParameterName, formattedEndDate);
            sQLCondition = sQLCondition.concat(String.format(AbstractDatesFormParam.SQL_CONDITION,
                    hQLAliasDate, startParameterName, endParameterName));
        }
    }

    /**
     * Builds the new map period.
     *
     * @return the map
     */
    private Map<String, String> buildNewMapPeriod() {
        final Map<String, String> firstMap = new HashMap<>();
        firstMap.put(AbstractDatesFormParam.START_INDEX, "");
        firstMap.put(AbstractDatesFormParam.END_INDEX, "");
        return firstMap;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#buildParameterMapAndSQLCondition
     * (java.lang.String)
     */
    /**
     *
     * @param hQLAliasDate
     * @throws DateTimeException
     */
    @Override
    protected void buildParameterMapAndSQLCondition(final String hQLAliasDate)
            throws DateTimeException {
        int periodIndex = 0;
        for (final Map<String, String> periodsMap : periods) {
            buildContinuousPeriod(periodIndex++,
                    periodsMap.get(AbstractDatesFormParam.START_INDEX),
                    periodsMap.get(AbstractDatesFormParam.END_INDEX), hQLAliasDate);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#buildSummary(java.io.PrintStream
     * )
     */
    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {
        printStream.print(
                String.format(
                        getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH, PROPERTY_MSG_FROM_TO),
                        dateStart == null ? periods.get(0).get(START_INDEX) : DateUtil.getUTCDateTextFromLocalDateTime(dateStart, DateUtil.DD_MM_YYYY),
                        dateEnd == null ? periods.get(0).get(END_INDEX) : DateUtil.getUTCDateTextFromLocalDateTime(dateEnd, DateUtil.DD_MM_YYYY)
                ));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#customValidate()
     */
    /**
     *
     */
    @Override
    public void customValidate() {
        LOGGER.info("unused");
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#getPatternDate()
     */
    /**
     *
     * @return
     */
    @Override
    public String getPatternDate() {
        return AbstractDatesFormParam.DATE_FORMAT;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.acbb.extraction.jsf.IDateFormParameter#getPeriodsFromDateFormParameter()
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        final List<Periode> periodes = new LinkedList<>();
        for (final Map<String, String> period : getPeriods()) {
            periodes.add(new Periode(period.get(AbstractDatesFormParam.START_INDEX), period
                    .get(AbstractDatesFormParam.END_INDEX)));
        }
        return periodes;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#getSummaryHTML()
     */
    /**
     *
     * @return @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintStream printStream;
        try {
            printStream = new PrintStream(bos, true, StandardCharsets.ISO_8859_1.name());
            if (!getIsValid()) {
                printValidityMessages(printStream);
                return bos.toString();
            }
            periods.forEach((periodsMap) -> {
                printStream.println(String.format(
                        "%s<br/>",
                        String.format(
                                getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH, PROPERTY_MSG_FROM_TO),
                                periodsMap.get(AbstractDatesFormParam.START_INDEX),
                                periodsMap.get(AbstractDatesFormParam.END_INDEX)))
                );
            });
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return bos.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#isEmpty()
     */
    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        for (final Map<String, String> periodsMap : periods) {
            for (final Entry<String, String> entry : periodsMap.entrySet()) {
                if (testPeriodEmpty(periodsMap.get(entry.getKey()))) {
                return true;
            }
            }
        }
        return false;
    }

    /**
     * Removes the period years continuous.
     *
     * @return the string
     */
    public String removePeriodYearsContinuous() {
        if (getPeriods().size() <= 1) {
            return null;
        }
        List<Map<String, String>> periods = getPeriods();
        periods = periods.subList(0, periods.size() - 1);
        setPeriods(periods);
        return null;
    }

    /**
     * Update date year continuous.
     *
     * @param index
     * @param value
     * @param key
     * @return the string
     */
    public String updateDateYearContinuous(int index, String key, String value) {
        getPeriods().get(index).put(key, value);
        return null;
    }

    /**
     *
     * @param event
     */
    public void changeDateStart(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        setDateStart(date);
        periods.get(0).put("start", date == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY));
    }

    /**
     *
     * @param event
     */
    public void changeDateEnd(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        setDateEnd(date);
        periods.get(0).put("end", date == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY));
    }

    /**
     *
     * @return @throws DateTimeException
     */
    public LocalDate getDateEnd() throws DateTimeException {
        return this.dateEnd;
    }

    /**
     *
     * @param dateEnd
     */
    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = dateEnd;
    }

    /**
     *
     * @return @throws DateTimeException
     */
    public LocalDate getDateStart() throws DateTimeException {
        return this.dateStart;
    }

    /**
     *
     * @param dateStart
     */
    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }

    /**
     * Intervals date.
     *
     *
     * @return the list
     */
    public List<IntervalDate> intervalsDate() {
        final List<IntervalDate> intervalsDate = new LinkedList();
        this.periods.forEach((period) -> {
            try {
                intervalsDate.add(IntervalDate.getIntervalDateddMMyyyy(
                        period.get(AbstractDatesFormParam.START_INDEX).replaceAll("/", "-"), period
                                .get(AbstractDatesFormParam.END_INDEX).replaceAll("/", "-")));
            } catch (final BadExpectedValueException e) {
                LOGGER.debug(e.getMessage(), e);
            }
        });
        return intervalsDate;

    }
}
