/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique;

/**
 *
 * @author adiankha
 */
public interface IITKDatatypeManager {

    /**
     *
     */
    String CODE_DATATYPE_RECOLTE_COUPE = "intervention_recolte_coupe";

    /**
     *
     */
    String CODE_DATATYPE_SEMIS_PLANTATION = "intervention_semis_plantation";

    /**
     *
     */
    String CODE_DATATYPE_TRAVAIL_DU_SOL = "intervention_travail_du_sol";

    /**
     *
     */
    String CODE_DATATYPE_APPORT = "intervention_apport";

    /**
     *
     */
    String CODE_DATATYPE_PRO_ETUDIE = "intervention_pro_etudie";

}
