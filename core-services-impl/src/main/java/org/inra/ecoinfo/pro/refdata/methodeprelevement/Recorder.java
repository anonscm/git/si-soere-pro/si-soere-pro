/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methodeprelevement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<MethodePrelevement> {

    IMethodePrelevementDAO methodePrelevementDAO;
    private Properties commentEn;

    private void createMethode(final MethodePrelevement especePlante) throws BusinessException {
        try {
            methodePrelevementDAO.saveOrUpdate(especePlante);
            methodePrelevementDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create especePlante");
        }
    }

    private void updateBDMethode(String code, String nom, String commentaire, MethodePrelevement dbespece) throws BusinessException {
        try {
            dbespece.setCode(code);
            dbespece.setNom(nom);
            dbespece.setCommentaire(commentaire);
            methodePrelevementDAO.saveOrUpdate(dbespece);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update especePlante");
        }
    }

    private void createOrUpdateMethode(final String code, String nom, String commentaire, final MethodePrelevement dbespece) throws BusinessException {
        if (dbespece == null) {
            final MethodePrelevement especePlante = new MethodePrelevement(nom, code, commentaire);
            especePlante.setCode(code);
            especePlante.setNom(nom);
            especePlante.setCommentaire(commentaire);
            createMethode(especePlante);
        } else {
            updateBDMethode(code, nom, commentaire, dbespece);
        }
    }

    private void persistMethode(final String code, final String nom, String commentaire) throws BusinessException, BusinessException {
        final MethodePrelevement dbMethode = methodePrelevementDAO.getByNKey(nom).orElse(null);
        createOrUpdateMethode(code, nom, commentaire, dbMethode);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final MethodePrelevement dbEspece = methodePrelevementDAO.getByNKey(intitule)
                        .orElseThrow(() -> new BusinessException("can't find especePlante"));
                methodePrelevementDAO.remove(dbEspece);
                values = csvp.getLine();

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MethodePrelevement.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistMethode(code, nom, commentaire);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<MethodePrelevement> getAllElements() throws BusinessException {
        return methodePrelevementDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MethodePrelevement methode) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(methode == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : methode.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(methode == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : methode.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(methode.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<MethodePrelevement> initModelGridMetadata() {
        commentEn = localizationManager.newProperties(MethodePrelevement.NAME_ENTITY_JPA, MethodePrelevement.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param methodePrelevementDAO
     */
    public void setMethodePrelevementDAO(IMethodePrelevementDAO methodePrelevementDAO) {
        this.methodePrelevementDAO = methodePrelevementDAO;
    }

}
