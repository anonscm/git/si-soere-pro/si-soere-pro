/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.impl;

import java.time.LocalDate;

/**
 *
 * @author adiankha
 */
public class TravailSolLineRecord implements Comparable<TravailSolLineRecord> {

    Long originalLineNumber;
    LocalDate datedebut;
    String codedispositif;
    String codetraitement;
    String codeparcelle;
    String nomplacette;
    String localisationprecide;
    LocalDate datefin;
    String interventionsol;
    String profondeurtravail;
    String materieltravail1;
    String materieltravail2;
    String materieltravail3;
    String largeurtravail;
    String culture;
    String codebbch;
    String precisionstade;
    String conditionhumidite;
    String conditiontemperature;
    String vitessevent;
    String observationqualite;
    String nomobservation;
    String niveauatteint;
    String commentaire;

    /**
     *
     */
    public TravailSolLineRecord() {
    }

    /**
     *
     * @param originalLineNumber
     * @param datedebut
     * @param codedispositif
     * @param codetraitement
     * @param codeparcelle
     * @param nomplacette
     * @param localisationprecide
     * @param datefin
     * @param interventionsol
     * @param profondeurtravail
     * @param materieltravail1
     * @param materieltravail2
     * @param materieltravail3
     * @param largeurtravail
     * @param culture
     * @param codebbch
     * @param precisionstade
     * @param conditionhumidite
     * @param conditiontemperature
     * @param vitessevent
     * @param observationqualite
     * @param nomobservation
     * @param niveauatteint
     * @param commentaire
     */
    public TravailSolLineRecord(Long originalLineNumber, LocalDate datedebut, String codedispositif, String codetraitement, String codeparcelle, String nomplacette, String localisationprecide, LocalDate datefin, String interventionsol, String profondeurtravail, String materieltravail1, String materieltravail2, String materieltravail3, String largeurtravail, String culture, String codebbch, String precisionstade, String conditionhumidite, String conditiontemperature, String vitessevent, String observationqualite, String nomobservation, String niveauatteint, String commentaire) {
        this.originalLineNumber = originalLineNumber;
        this.datedebut = datedebut;
        this.codedispositif = codedispositif;
        this.codetraitement = codetraitement;
        this.codeparcelle = codeparcelle;
        this.nomplacette = nomplacette;
        this.localisationprecide = localisationprecide;
        this.datefin = datefin;
        this.interventionsol = interventionsol;
        this.profondeurtravail = profondeurtravail;
        this.materieltravail1 = materieltravail1;
        this.materieltravail2 = materieltravail2;
        this.materieltravail3 = materieltravail3;
        this.largeurtravail = largeurtravail;
        this.culture = culture;
        this.codebbch = codebbch;
        this.precisionstade = precisionstade;
        this.conditionhumidite = conditionhumidite;
        this.conditiontemperature = conditiontemperature;
        this.vitessevent = vitessevent;
        this.observationqualite = observationqualite;
        this.nomobservation = nomobservation;
        this.niveauatteint = niveauatteint;
        this.commentaire = commentaire;
    }

    @Override
    public int compareTo(TravailSolLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatedebut() {
        return datedebut;
    }

    /**
     *
     * @param datedebut
     */
    public void setDatedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
    }

    /**
     *
     * @return
     */
    public String getCodedispositif() {
        return codedispositif;
    }

    /**
     *
     * @param codedispositif
     */
    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    /**
     *
     * @return
     */
    public String getCodetraitement() {
        return codetraitement;
    }

    /**
     *
     * @param codetraitement
     */
    public void setCodetraitement(String codetraitement) {
        this.codetraitement = codetraitement;
    }

    /**
     *
     * @return
     */
    public String getCodeparcelle() {
        return codeparcelle;
    }

    /**
     *
     * @param codeparcelle
     */
    public void setCodeparcelle(String codeparcelle) {
        this.codeparcelle = codeparcelle;
    }

    /**
     *
     * @return
     */
    public String getNomplacette() {
        return nomplacette;
    }

    /**
     *
     * @param nomplacette
     */
    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    /**
     *
     * @return
     */
    public String getLocalisationprecide() {
        return localisationprecide;
    }

    /**
     *
     * @param localisationprecide
     */
    public void setLocalisationprecide(String localisationprecide) {
        this.localisationprecide = localisationprecide;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatefin() {
        return datefin;
    }

    /**
     *
     * @param datefin
     */
    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    /**
     *
     * @return
     */
    public String getInterventionsol() {
        return interventionsol;
    }

    /**
     *
     * @param interventionsol
     */
    public void setInterventionsol(String interventionsol) {
        this.interventionsol = interventionsol;
    }

    /**
     *
     * @return
     */
    public String getProfondeurtravail() {
        return profondeurtravail;
    }

    /**
     *
     * @param profondeurtravail
     */
    public void setProfondeurtravail(String profondeurtravail) {
        this.profondeurtravail = profondeurtravail;
    }

    /**
     *
     * @return
     */
    public String getMaterieltravail1() {
        return materieltravail1;
    }

    /**
     *
     * @param materieltravail1
     */
    public void setMaterieltravail1(String materieltravail1) {
        this.materieltravail1 = materieltravail1;
    }

    /**
     *
     * @return
     */
    public String getMaterieltravail2() {
        return materieltravail2;
    }

    /**
     *
     * @param materieltravail2
     */
    public void setMaterieltravail2(String materieltravail2) {
        this.materieltravail2 = materieltravail2;
    }

    /**
     *
     * @return
     */
    public String getMaterieltravail3() {
        return materieltravail3;
    }

    /**
     *
     * @param materieltravail3
     */
    public void setMaterieltravail3(String materieltravail3) {
        this.materieltravail3 = materieltravail3;
    }

    /**
     *
     * @return
     */
    public String getLargeurtravail() {
        return largeurtravail;
    }

    /**
     *
     * @param largeurtravail
     */
    public void setLargeurtravail(String largeurtravail) {
        this.largeurtravail = largeurtravail;
    }

    /**
     *
     * @return
     */
    public String getCulture() {
        return culture;
    }

    /**
     *
     * @param culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getPrecisionstade() {
        return precisionstade;
    }

    /**
     *
     * @param precisionstade
     */
    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    /**
     *
     * @return
     */
    public String getConditionhumidite() {
        return conditionhumidite;
    }

    /**
     *
     * @param conditionhumidite
     */
    public void setConditionhumidite(String conditionhumidite) {
        this.conditionhumidite = conditionhumidite;
    }

    /**
     *
     * @return
     */
    public String getConditiontemperature() {
        return conditiontemperature;
    }

    /**
     *
     * @param conditiontemperature
     */
    public void setConditiontemperature(String conditiontemperature) {
        this.conditiontemperature = conditiontemperature;
    }

    /**
     *
     * @return
     */
    public String getVitessevent() {
        return vitessevent;
    }

    /**
     *
     * @param vitessevent
     */
    public void setVitessevent(String vitessevent) {
        this.vitessevent = vitessevent;
    }

    /**
     *
     * @return
     */
    public String getObservationqualite() {
        return observationqualite;
    }

    /**
     *
     * @param observationqualite
     */
    public void setObservationqualite(String observationqualite) {
        this.observationqualite = observationqualite;
    }

    /**
     *
     * @return
     */
    public String getNomobservation() {
        return nomobservation;
    }

    /**
     *
     * @param nomobservation
     */
    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteint() {
        return niveauatteint;
    }

    /**
     *
     * @param niveauatteint
     */
    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
