/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.impl;

import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
public class TestDuplicateApport extends AbstractTestDuplicate {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicateApport.class.getName());

    static final long serialVersionUID = 1L;

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";

    static final String PROPERTY_MSG_DUPLICATE_LINE_AUTRES_APPORT = "PROPERTY_MSG_DUPLICATE_LINE_AUTRES_APPORT";

    //  final SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine;
    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    /**
     *
     */
    public TestDuplicateApport() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    /**
     *
     * @param date
     * @param codedisp
     * @param codeTrait
     * @param parcelle
     * @param placette
     * @param quantiteapport
     * @param localisation
     * @param lineNumber
     */
    protected void addLine(final String date, String codedisp, String codeTrait, final String parcelle, String placette,
            final String quantiteapport, String localisation, final long lineNumber) {
        final String key = this.getKey(date, codedisp, codeTrait, parcelle, placette, quantiteapport, localisation);
        final String keySol = this.getKey(date, codedisp, codeTrait, parcelle, quantiteapport, localisation);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, quantiteapport);
            } else if (!this.lineCouvert.get(keySol).equals(localisation)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicateApport.BUNDLE_SOURCE_PATH,
                        TestDuplicateApport.PROPERTY_MSG_DUPLICATE_LINE_AUTRES_APPORT), lineNumber,
                        date, codedisp, codeTrait, parcelle, quantiteapport, parcelle, this.lineCouvert.get(keySol), localisation));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicateApport.BUNDLE_SOURCE_PATH,
                    TestDuplicateApport.PROPERTY_MSG_DUPLICATE_LINE_AUTRES_APPORT), lineNumber, date, codedisp, codeTrait, parcelle, quantiteapport, quantiteapport,
                    localisation, this.line.get(key)));

        }
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], values[3], values[4], values[5], values[6], lineNumber);
    }

}
