/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.amenagementavantplantation;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAAmenagementAvantPlantationDAO extends AbstractJPADAO<AmenagementAvantPlantation> implements IAmenagementAvantPlantationDAO {

    /**
     *
     * @return
     */
    @Override
    public List<AmenagementAvantPlantation> getAll() {
        return getAllBy(AmenagementAvantPlantation.class, AmenagementAvantPlantation::getAap_libelle);
    }

    /**
     *
     * @param libelle
     * @return
     */
    @Override
    public Optional<AmenagementAvantPlantation> getByNKey(String libelle) {
        CriteriaQuery<AmenagementAvantPlantation> query = builder.createQuery(AmenagementAvantPlantation.class);
        Root<AmenagementAvantPlantation> aap = query.from(AmenagementAvantPlantation.class);
        query
                .select(aap)
                .where(
                        builder.equal(aap.get(AmenagementAvantPlantation_.aap_libelle), libelle)
                );
        return getOptional(query);

    }

}
