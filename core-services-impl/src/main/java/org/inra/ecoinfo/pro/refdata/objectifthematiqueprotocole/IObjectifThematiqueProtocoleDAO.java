package org.inra.ecoinfo.pro.refdata.objectifthematiqueprotocole;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.thematiqueetudiee.ThematiqueEtudiee;

/**
 * @author sophie
 *
 */
public interface IObjectifThematiqueProtocoleDAO extends IDAO<ObjectifThematiqueProtocole> {

    /**
     *
     * @return
     */
    List<ObjectifThematiqueProtocole> getAll();

    /**
     *
     * @param protocole
     * @param thematiqueEtudiee
     * @param objectif
     * @return
     */
    Optional<ObjectifThematiqueProtocole> getByNKey(Protocole protocole, ThematiqueEtudiee thematiqueEtudiee, String objectif);

}
