package org.inra.ecoinfo.pro.refdata.caracteristiqueetape;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<CaracteristiqueEtapes> {

    /**
     *
     */
    protected ICaracteristiqueEtapeDAO caracetapeDAO;

    private Properties CSnomEn;
    private Properties commentEn;

    private void createCaracteristiqueEtape(final CaracteristiqueEtapes caractetapes) throws BusinessException {
        try {
            caracetapeDAO.saveOrUpdate(caractetapes);
            caracetapeDAO.flush();
        } catch (final PersistenceException e) {
            throw new BusinessException("can't create etape");
        }
    }

    private void createOrUpdateCaracteristiqueEtape(final String code, String nom, String commentaire, final CaracteristiqueEtapes dbcaractetapes) throws BusinessException {
        if (dbcaractetapes == null) {
            final CaracteristiqueEtapes caracetapes = new CaracteristiqueEtapes(nom, commentaire);
            caracetapes.setCetape_code(code);
            caracetapes.setCetape_intitule(nom);
            caracetapes.setCommentaire(commentaire);
            createCaracteristiqueEtape(caracetapes);
        } else {
            updateBDCaracteristiqueEtape(code, nom, commentaire, dbcaractetapes);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final CaracteristiqueEtapes dbcaracteristiqueetapes = caracetapeDAO.getByNKey(intitule).orElse(null);
                if (dbcaracteristiqueetapes != null) {
                    caracetapeDAO.remove(dbcaracteristiqueetapes);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<CaracteristiqueEtapes> getAllElements() throws BusinessException {
        return caracetapeDAO.getAll();
    }

    /**
     *
     * @return
     */
    public ICaracteristiqueEtapeDAO getCaracetapeDAO() {
        return caracetapeDAO;
    }

    /**
     *
     * @return
     */
    public Properties getCSnomEn() {
        return CSnomEn;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CaracteristiqueEtapes caracteristiqueetapes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(caracteristiqueetapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : caracteristiqueetapes.getCetape_intitule(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(caracteristiqueetapes == null || caracteristiqueetapes.getCetape_intitule() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : CSnomEn.getProperty(caracteristiqueetapes.getCetape_intitule()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(caracteristiqueetapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : caracteristiqueetapes.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(caracteristiqueetapes == null || caracteristiqueetapes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(caracteristiqueetapes.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<CaracteristiqueEtapes> initModelGridMetadata() {
        CSnomEn = localizationManager.newProperties(CaracteristiqueEtapes.NAME_ENTITY_JPA, CaracteristiqueEtapes.JPA_COLUMN_INTITULE, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(CaracteristiqueEtapes.NAME_ENTITY_JPA, CaracteristiqueEtapes.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void persistCaracteristiqueEtape(final String code, final String intitule, String commentaire) throws BusinessException {
        final CaracteristiqueEtapes dbcaractetapes = caracetapeDAO.getByNKey(intitule).orElse(null);
        createOrUpdateCaracteristiqueEtape(code, intitule, commentaire, dbcaractetapes);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CaracteristiqueEtapes.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final String commentaire = tokenizerValues.nextToken();
                persistCaracteristiqueEtape(code, intitule, commentaire);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);

        }
    }

    /**
     *
     * @param caracetapeDAO
     */
    public void setCaracetapeDAO(ICaracteristiqueEtapeDAO caracetapeDAO) {
        this.caracetapeDAO = caracetapeDAO;
    }

    /**
     *
     * @param cSnomEn
     */
    public void setCSnomEn(Properties cSnomEn) {
        CSnomEn = cSnomEn;
    }

    private void updateBDCaracteristiqueEtape(final String code, String nom, String commentaire, final CaracteristiqueEtapes dbcaractetapes) throws BusinessException {
        try {
            dbcaractetapes.setCetape_intitule(nom);
            dbcaractetapes.setCetape_code(code);
            dbcaractetapes.setCommentaire(commentaire);
            caracetapeDAO.saveOrUpdate(dbcaractetapes);
        } catch (final PersistenceException e) {
            throw new BusinessException("can't update etape");
        }
    }

}
