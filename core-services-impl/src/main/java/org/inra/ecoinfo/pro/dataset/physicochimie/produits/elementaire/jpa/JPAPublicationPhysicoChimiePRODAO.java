/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO_;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationPhysicoChimiePRODAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurPhysicoChimiePRO> deleteValeurs = builder.createCriteriaDelete(ValeurPhysicoChimiePRO.class);
        Root<ValeurPhysicoChimiePRO> valeur = deleteValeurs.from(ValeurPhysicoChimiePRO.class);
        Subquery<MesurePhysicoChimiePRO> subquery = deleteValeurs.subquery(MesurePhysicoChimiePRO.class);
        Root<MesurePhysicoChimiePRO> m = subquery.from(MesurePhysicoChimiePRO.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesurePhysicoChimiePRO_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurPhysicoChimiePRO_.mesurePhysicoChimiePRO).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesurePhysicoChimiePRO> deleteSequence = builder.createCriteriaDelete(MesurePhysicoChimiePRO.class);
        Root<MesurePhysicoChimiePRO> sequence = deleteSequence.from(MesurePhysicoChimiePRO.class);
        deleteSequence.where(builder.equal(sequence.get(MesurePhysicoChimiePRO_.versionfile), version));
        delete(deleteSequence);
    }

}
