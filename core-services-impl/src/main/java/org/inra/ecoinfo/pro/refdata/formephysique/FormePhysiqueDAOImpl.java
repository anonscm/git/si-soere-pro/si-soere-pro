package org.inra.ecoinfo.pro.refdata.formephysique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.FormePhysique.FormePhysiques;
import org.inra.ecoinfo.pro.refdata.FormePhysique.FormePhysiques_;

/**
 *
 * @author ptcherniati
 */
public class FormePhysiqueDAOImpl extends AbstractJPADAO<FormePhysiques> implements IFormePhysiqueDAO {

    /**
     *
     * @return
     */
    @Override
    public List<FormePhysiques> getAll() {
        return getAll(FormePhysiques.class);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<FormePhysiques> getByNKey(String code) {
        CriteriaQuery<FormePhysiques> query = builder.createQuery(FormePhysiques.class);
        Root<FormePhysiques> formePhysiques = query.from(FormePhysiques.class);
        query
                .select(formePhysiques)
                .where(
                        builder.equal(formePhysiques.get(FormePhysiques_.fp_code), code)
                );
        return getOptional(query);
    }

}
