/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.sol.elementaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieOutputDisplayByRow;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class SolBrutBuildOutputDisplayByRow extends PhysicoChimieOutputDisplayByRow {

    static final String PATTERN_WORD = "%s";
    static final String PATTERB_CSV_12_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%%s;%%s;%%s;%%s;%%s;%%s";
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA_SOLBRUT = "PROPERTY_HEADER_RAW_DATA_SOLBRUT";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedSolBrutVariables = getVariablesSelected(requestMetadatasMap, IPhysicoChimieSolDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE);
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<MesurePhysicoChimieSols> mesuresSolBrut = getMesures(resultsDatasMap, SolBrutExtractor.MAP_INDEX_SOLBRUT);
        final Set<String> dispositifsNames = buildListOfDispositifsForDatatype(selectedDispositifs, getDatatype());
        final Map<String, File> filesMap = this.buildOutputsFiles(dispositifsNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().stream().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>>> mesuresSolBrutsMap = new TreeMap();

        try {
            this.buildmap(mesuresSolBrut, mesuresSolBrutsMap);
        } catch (DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedDispositifs, selectedSolBrutVariables, selectedIntervalDate,
                outputPrintStreamMap, mesuresSolBrutsMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }



    private void buildmap(
            final List<MesurePhysicoChimieSols> mesuresSolBruts,
            final SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>>> mesuresSolBrutMap) {
        java.util.Iterator<MesurePhysicoChimieSols> itMesure = mesuresSolBruts
                .iterator();
        while (itMesure.hasNext()) {
            MesurePhysicoChimieSols mesureSolBrut = itMesure
                    .next();
            EchantillonsSol echan = mesureSolBrut.getEchantillon();
            Long siteId = echan.getPrelevementsol().getTraitement().getDispositif().getId();
            if (mesuresSolBrutMap.get(siteId) == null) {
                mesuresSolBrutMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresSolBrutMap.get(siteId).get(echan) == null) {
                mesuresSolBrutMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresSolBrutMap.get(siteId).get(echan).put(mesureSolBrut.getDatePrelevement(), mesureSolBrut);
            itMesure.remove();
        }
    }

    private void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedSolBrutVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>>> mesuresSolBrutsMap) {
        try {
            BuildDataLine(selectedDispositifs, selectedSolBrutVariables, outputPrintStreamMap, mesuresSolBrutsMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void BuildDataLine(final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedSolBrutVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>>> mesuresSolBrutsMap,
            final IntervalDate selectedIntervalDate) {
        PrintStream out;
        String currentDispositif;
        String currentEchan;
        String currentTraitement;
        String curentCodeBloc;
        String currentParcelle;
        String curentPlacette;
        int numero;
        double ls;
        double li;
        String currentlabo;
        for (final Dispositif dispositif : selectedDispositifs) {
            out = outputPrintStreamMap.get(getDispositifDatatype(dispositif, getDatatype()));
            final SortedMap<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>> mesureSolBrutsMapByDisp = mesuresSolBrutsMap
                    .get(dispositif.getId());
            if (mesureSolBrutsMapByDisp == null) {
                continue;
            }
            Iterator<Entry<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>>> itEchan = mesureSolBrutsMapByDisp
                    .entrySet().iterator();
            while (itEchan.hasNext()) {
                java.util.Map.Entry<EchantillonsSol, SortedMap<LocalDate, MesurePhysicoChimieSols>> echanEntry = itEchan
                        .next();
                EchantillonsSol echantillon = echanEntry.getKey();
                SortedMap<LocalDate, MesurePhysicoChimieSols> mesureSolBrutsMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                for (Entry<LocalDate, MesurePhysicoChimieSols> entrySet : mesureSolBrutsMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesurePhysicoChimieSols mesureSolBrut = entrySet.getValue();
                    currentDispositif = echantillon.getPrelevementsol().getTraitement().getDispositif().getName();
                    currentEchan = echantillon.getCodeesol() != null ? echantillon.getCodeesol()
                            : org.apache.commons.lang.StringUtils.EMPTY;
                    currentTraitement = mesureSolBrut.getEchantillon().getPrelevementsol().getTraitement().getCode();
                    curentCodeBloc = echantillon.getPrelevementsol().getBloc().getNom();
                    currentParcelle = echantillon.getPrelevementsol().getPelementaire().getCodeParcelleElementaire().getCode();
                    curentPlacette = echantillon.getPrelevementsol().getPlacette().getNom();
                    numero = mesureSolBrut.getNumero_repetition();
                    ls = echantillon.getPrelevementsol().getLimit_superieur();
                    li = echantillon.getPrelevementsol().getLimit_inferieur();
                    currentlabo = String.format(SolBrutBuildOutputDisplayByRow.PATTERN_WORD,
                            mesureSolBrut.getNom_laboratoire());

                    String genericPattern = LineDataFixe(date,
                            currentEchan,
                            curentCodeBloc, currentTraitement,
                            currentParcelle,
                            curentPlacette, numero, ls, li,
                            currentlabo);
                    LineDataVariable(mesureSolBrut, genericPattern, out, selectedSolBrutVariables);
                }
                itEchan.remove();
            }
        }
    }

    private void LineDataVariable(MesurePhysicoChimieSols mesureSolBrut, String genericPattern, PrintStream out, final List<VariablesPRO> selectedSolBrutVariables) {
        selectedSolBrutVariables.forEach((variablesPRO) -> {
            for (Iterator<ValeurPhysicoChimieSols> ValeurIterator = mesureSolBrut.getValeurPhysicoChimieSols().iterator(); ValeurIterator.hasNext();) {
                ValeurPhysicoChimieSols valeur = ValeurIterator.next();
                if (((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getId().equals(variablesPRO.getId())) {
                    String line = String.format(genericPattern,
                            valeur.getValeur(),
                            valeur.getStatutvaleur(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());

                    out.println(line);
                    ValeurIterator.remove();
                }
            }
        });
    }

    private String LineDataFixe(LocalDate date, String currentEchan, String curentCodeCulture, String currentTraitement,
            String currentParcelle, String curentPlacette, int numero, double ls, double li, String currentlabo){
        String genericPattern = String.format(PATTERB_CSV_12_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentEchan,
                curentCodeCulture,
                currentTraitement,
                currentParcelle,
                curentPlacette,
                numero, ls, li,
                currentlabo);
        return genericPattern;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(SolBrutBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(SolBrutBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        SolBrutBuildOutputDisplayByRow.HEADER_RAW_DATA_SOLBRUT));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (CollectionUtils.isEmpty(((DefaultParameter) parameters).getResults()
                .get(SolBrutExtractor.CST_RESULT_EXTRACTION_SOLBRUT_CODE)
                .getOrDefault(SolBrutExtractor.MAP_INDEX_SOLBRUT,
                        ((DefaultParameter) parameters).getResults()
                .get(SolBrutExtractor.CST_RESULT_EXTRACTION_SOLBRUT_CODE)
                .get(SolBrutExtractor.MAP_INDEX_0)))) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, SolBrutExtractor.CST_RESULT_EXTRACTION_SOLBRUT_CODE));
        return null;
    }

    private String getDatatype() {
        return "sols_physico-chimie_donnees_elementaires";
    }
}
