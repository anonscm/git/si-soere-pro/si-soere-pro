/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.environnementdispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.environnement.Environnement;

/**
 *
 * @author adiankha
 */
public interface IEnvironnementDispositifDAO extends IDAO<EnvironnementDispositif> {

    /**
     *
     * @return
     */
    List<EnvironnementDispositif> gettAll();

    /**
     *
     * @param environnement
     * @param dispositif
     * @return
     */
    Optional<EnvironnementDispositif> getByNKey(Environnement environnement, Dispositif dispositif);

}
