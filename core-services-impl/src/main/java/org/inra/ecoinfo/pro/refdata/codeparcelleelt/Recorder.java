package org.inra.ecoinfo.pro.refdata.codeparcelleelt;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<CodeParcelleElementaire> {

    ICodeParcelleElementaireDAO codeParcelleElementaireDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String code = tokenizerValues.nextToken();

                codeParcelleElementaireDAO.remove(codeParcelleElementaireDAO.getByNKey(code).orElseThrow(PersistenceException::new));

                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<CodeParcelleElementaire> getAllElements() throws BusinessException {
        return codeParcelleElementaireDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CodeParcelleElementaire codeParcelleElementaire) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(codeParcelleElementaire == null ? Constantes.STRING_EMPTY : codeParcelleElementaire.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = tokenizerValues.nextToken();
                CodeParcelleElementaire codeparcelleElementaire = new CodeParcelleElementaire(code);
                CodeParcelleElementaire dbCodeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(code).orElse(null);

                if (dbCodeParcelleElementaire == null) {
                    codeParcelleElementaireDAO.saveOrUpdate(codeparcelleElementaire);
                }

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param codeParcelleElementaireDAO the codeParcelleElementaireDAO to set
     */
    public void setCodeParcelleElementaireDAO(
            ICodeParcelleElementaireDAO codeParcelleElementaireDAO) {
        this.codeParcelleElementaireDAO = codeParcelleElementaireDAO;
    }

}
