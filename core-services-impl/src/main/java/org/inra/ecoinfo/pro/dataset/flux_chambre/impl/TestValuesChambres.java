/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class TestValuesChambres extends GenericTestValues {

    static final long serialVersionUID = 1L;

    /**
     *
     */
    public TestValuesChambres() {
        super();
    }

    /**
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException
     */
    @Override
    public void testValues(final long startline, final CSVParser parser,
            final VersionFile versionFile, final ISessionPropertiesPRO sessionProperties,
            final String encoding, final BadsFormatsReport badsFormatsReport,
            final DatasetDescriptorPRO datasetDescriptor,
            final String datatypeName) throws BusinessException {

        super.testValues(startline, parser, versionFile, sessionProperties, encoding, badsFormatsReport, datasetDescriptor, datatypeName);
    }

}
