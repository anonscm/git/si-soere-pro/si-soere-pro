/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeintrants;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IListeIntrantsDAO extends IDAO<ListeIntrants> {

    /**
     *
     * @return
     */
    List<ListeIntrants> getAll();

    /**
     *
     * @param commercial
     * @param element
     * @return
     */
    Optional<ListeIntrants> getByNKey(String commercial, String element);

}
