/*
 *
 */
package org.inra.ecoinfo.pro.refdata.etape;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IEtapeDAO extends IDAO<Etapes> {

    /**
     *
     * @return
     */
    List<Etapes> getAll();

    /**
     *
     * @param etape_intitule
     * @return
     */
    Optional<Etapes> getByNKey(String etape_intitule);

}
