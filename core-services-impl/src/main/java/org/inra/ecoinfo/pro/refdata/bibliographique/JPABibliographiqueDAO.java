/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.bibliographique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPABibliographiqueDAO extends AbstractJPADAO<Bibliographique> implements IBibliographiqueDAO{

    /**
     *
     * @return
     */
    @Override
    public List<Bibliographique> getAll(){
       return getAllBy(Bibliographique.class, Bibliographique::getBiblio_mycode);
    }

    /**
     *
     * @param doi
     * @return
     */
    @Override
    public Optional<Bibliographique> getByNKey(String doi){
         CriteriaQuery<Bibliographique> query = builder.createQuery(Bibliographique.class);
         Root<Bibliographique> bbg = query.from(Bibliographique.class);
         query
                 .select(bbg)
                 .where(builder.equal(bbg.get(Bibliographique_.biblio_mycode), Utils.createCodeFromString(doi)));
         return getOptional(query);
    }
    
}
