package org.inra.ecoinfo.pro.refdata.rolepersonneressourcedispositif;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.personneressource.PersonneRessource;
import org.inra.ecoinfo.pro.refdata.rolepersonneressource.RolePersonneRessource;


/**
 * @author sophie
 * 
 */
public class JPARolePersonneRessourceDispositifDAO extends AbstractJPADAO<RolePersonneRessourceDispositif> implements IRolePersonneRessourceDispositifDAO {

    /**
     *
     * @param personneRessource
     * @param dispositif
     * @param role
     * @return
     */
    @Override
    public Optional<RolePersonneRessourceDispositif> getByPersonneRessourceDispositifRole(PersonneRessource personneRessource, Dispositif dispositif, RolePersonneRessource role) {
        CriteriaQuery<RolePersonneRessourceDispositif> query = builder.createQuery(RolePersonneRessourceDispositif.class);
        Root<RolePersonneRessourceDispositif> rolePersonneRessourceDispositif = query.from(RolePersonneRessourceDispositif.class);
        Join<RolePersonneRessourceDispositif, Dispositif> disp = rolePersonneRessourceDispositif.join(RolePersonneRessourceDispositif_.dispositif);
        Join<RolePersonneRessourceDispositif, PersonneRessource> pr = rolePersonneRessourceDispositif.join(RolePersonneRessourceDispositif_.personneRessource);
        Join<RolePersonneRessourceDispositif, RolePersonneRessource> r = rolePersonneRessourceDispositif.join(RolePersonneRessourceDispositif_.role);
        query
                .select(rolePersonneRessourceDispositif)
                .where(
                        builder.equal(disp, dispositif),
                        builder.equal(pr, personneRessource),
                        builder.equal(r, role)
                );
        return getOptional(query);

    }

}
