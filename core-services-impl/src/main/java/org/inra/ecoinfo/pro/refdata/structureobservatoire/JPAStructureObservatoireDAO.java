package org.inra.ecoinfo.pro.refdata.structureobservatoire;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * @author sophie
 *
 */
public class JPAStructureObservatoireDAO extends AbstractJPADAO<StructureObservatoire> implements IStructureObservatoireDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.structureobservatoire.IStructureObservatoireDAO # getByStructureObservatoire(org.inra.ecoinfo.pro.refdata.structure.Structure , org.inra.ecoinfo.pro.refdata.observatoire.Observatoire)
     */

    /**
     *
     * @param structure
     * @param observatoire
     * @return
     */

    @Override
    public Optional<StructureObservatoire> getByNKey(Structure structure, Observatoire observatoire) {
        CriteriaQuery<StructureObservatoire> query = builder.createQuery(StructureObservatoire.class);
        Root<StructureObservatoire> structureObservatoire = query.from(StructureObservatoire.class);
        Join<StructureObservatoire, Observatoire> o = structureObservatoire.join(StructureObservatoire_.observatoire);
        Join<StructureObservatoire, Structure> str = structureObservatoire.join(StructureObservatoire_.structure);
        query
                .select(structureObservatoire)
                .where(
                        builder.equal(str, structure),
                        builder.equal(o, observatoire)
                );
        return getOptional(query);
    }

}
