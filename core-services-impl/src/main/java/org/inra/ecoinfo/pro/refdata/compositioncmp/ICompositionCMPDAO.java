package org.inra.ecoinfo.pro.refdata.compositioncmp;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.ValeurCaracteristiqueMP;

/**
 *
 * @author ptcherniati
 */
public interface ICompositionCMPDAO extends IDAO<CompositionsCMP> {

    /**
     *
     * @return
     */
    List<CompositionsCMP> getAll();

    /**
     *
     * @param ccmp_produitcode
     * @param vcmp
     * @param ccmp_matierepnom
     * @param ccmp_cmpnom
     * @return
     * @throws PersistenceException
     */
    Optional<CompositionsCMP> getByNKey(Melange melange, ValeurCaracteristiqueMP vcmp);

    // public CompositionsCMP FINDBYCODE(Produits produits,MatieresPremieres
    // matierepremieres,CaracteristiqueMatierePremieres cmp) throws
    // PersistenceException;
}
