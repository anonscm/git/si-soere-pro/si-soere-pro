package org.inra.ecoinfo.pro.refdata.unites;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Unites> {
    
    /**
     *
     */
    protected IUniteDAO unitesDAO;
    
    private Properties UniteEN;
    
    private void createOrUpdateUnites(final String code, String nom, String mycode, final Unites dbunites) throws BusinessException {
        if (dbunites == null) {
            Unites unites = new Unites(nom, code);
            unites.setCode(code);
            unites.setUnite(nom);
            unites.setMycode(mycode);
            createUnites(unites);
        } else {
            updateUnite(nom, mycode, code, dbunites);
        }
    }
    
    private void createUnites(final Unites unites) throws BusinessException {
        try {
            unitesDAO.saveOrUpdate(unites);
            unitesDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create unites");
        }
    }
    
    private void updateUnite(final String nom, String mycode, String code, final Unites dbunites) throws BusinessException {
        try {
            dbunites.setUnite(nom);
            dbunites.setCode(code);
            dbunites.setMycode(mycode);
            unitesDAO.saveOrUpdate(dbunites);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create unites");
        }
        
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = tokenizerValues.nextToken();
                Unites dbunites = unitesDAO.getByNKey(code)
                        .orElseThrow(() -> new BusinessException("can't get unit"));
                unitesDAO.remove(dbunites);
                
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public List<Unites> getAllElements() {
        return unitesDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Unites unites) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unites == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : unites.getCode(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(unites == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : unites.getUnite(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(unites == null || unites.getUnite() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : UniteEN.getProperty(unites.getUnite()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @return
     */
    public Properties getUniteEN() {
        return UniteEN;
    }
    
    @Override
    public ModelGridMetadata<Unites> initModelGridMetadata() {
        UniteEN = localizationManager.newProperties(Unites.NAME_ENTITY_JPA, Unites.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
    
    private void persistUnites(final String code, final String unite, String mycode) throws BusinessException {
        Unites dbunite = unitesDAO.getByNKey(code).orElse(null);
        createOrUpdateUnites(code, unite, mycode, dbunite);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Unites.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(valeur);
                persistUnites(code, valeur, mycode);
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    /**
     *
     * @param uniteEN
     */
    public void setUniteEN(Properties uniteEN) {
        UniteEN = uniteEN;
    }
    
    /**
     *
     * @param unitesDAO
     */
    public void setUnitesDAO(IUniteDAO unitesDAO) {
        this.unitesDAO = unitesDAO;
    }
    
    /**
     *
     * @return
     */
    public IUniteDAO getUnitesDAO() {
        return unitesDAO;
    }
    
}
