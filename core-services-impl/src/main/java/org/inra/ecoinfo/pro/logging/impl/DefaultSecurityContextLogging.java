package org.inra.ecoinfo.pro.logging.impl;

import java.time.LocalDateTime;
import org.slf4j.LoggerFactory;
import org.aspectj.lang.JoinPoint;
import org.inra.ecoinfo.mga.managedbean.IPolicyManager;
import org.inra.ecoinfo.utils.LoggerObject;
import org.slf4j.Logger;


public class DefaultSecurityContextLogging {

    public void logSecurityContextBefore(JoinPoint joinPoint) {
        Logger logger = LoggerFactory.getLogger("logging");

        logger.info(new LoggerObject(((IPolicyManager) joinPoint.getTarget()).getCurrentUserLogin().trim(), LocalDateTime.now(), "s'est déconnecté").toString());
    }
}