package org.inra.ecoinfo.pro.refdata.melange;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.component.Composant_;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
public class MelangeDAOImpl extends AbstractJPADAO<Melange> implements IMelangeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Melange> getAll() {
        return getAll(Melange.class);
    }

    /**
     *
     * @param produits
     * @param composant
     * @param pourcentage
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<Melange> getByNKey(Produits produits, Composant composant, double pourcentage) {
        CriteriaQuery<Melange> query = builder.createQuery(Melange.class);
        Root<Melange> melange = query.from(Melange.class);
        Join<Melange, Composant> comp = melange.join(Melange_.composant);
        Join<Melange, Produits> prod = melange.join(Melange_.produits);
        query
                .select(melange)
                .where(
                        builder.equal(prod, produits),
                        builder.equal(comp, composant),
                        builder.equal(melange.get(Melange.JPA_COLUMN_CENT), pourcentage)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeProduits
     * @param codeComposant
     * @param pourcentage
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<Melange> getByNKey(String codeProduits, String codeComposant, double pourcentage) {
        CriteriaQuery<Melange> query = builder.createQuery(Melange.class);
        Root<Melange> melange = query.from(Melange.class);
        Join<Melange, Composant> comp = melange.join(Melange_.composant);
        Join<Melange, Produits> prod = melange.join(Melange_.produits);
        query
                .select(melange)
                .where(
                        builder.equal(prod.get(Composant_.codecomposant), codeProduits),
                        builder.equal(comp.get(Composant_.codecomposant), codeComposant),
                        builder.equal(melange.get(Melange.JPA_COLUMN_CENT), pourcentage)
                );
        return getOptional(query);
    }
}
