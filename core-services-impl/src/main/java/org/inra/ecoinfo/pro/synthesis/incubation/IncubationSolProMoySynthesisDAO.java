/*
 * To change this license header, choose License Headers in ProMoyject ProMoyperties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.incubation;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy_;
import org.inra.ecoinfo.pro.synthesis.incubationsolpromoy.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.incubationsolpromoy.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class IncubationSolProMoySynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurIncubationSolProMoy, MesureIncubationSolProMoy> {

    @Override
    Class<ValeurIncubationSolProMoy> getValueClass() {
        return ValeurIncubationSolProMoy.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurIncubationSolProMoy, MesureIncubationSolProMoy> getMesureAttribute() {
        return ValeurIncubationSolProMoy_.mesureIncubationSolProMoy;
    }
}
