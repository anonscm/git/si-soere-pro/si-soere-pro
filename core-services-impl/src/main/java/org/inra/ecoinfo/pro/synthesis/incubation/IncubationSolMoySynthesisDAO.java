/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.incubation;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy_;
import org.inra.ecoinfo.pro.synthesis.incubationsolmoy.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.incubationsolmoy.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class IncubationSolMoySynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurIncubationSolMoy, MesureIncubationSolMoy> {

    @Override
    Class<ValeurIncubationSolMoy> getValueClass() {
        return ValeurIncubationSolMoy.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurIncubationSolMoy, MesureIncubationSolMoy> getMesureAttribute() {
        return ValeurIncubationSolMoy_.mesureIncubationSolMoy;
    }
}
