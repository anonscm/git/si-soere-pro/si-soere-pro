/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.moyenne.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteMoyenneDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne_;
import org.inra.ecoinfo.pro.extraction.physicochimie.jpa.JPAPhysicoChimieDAO;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantemoyenne.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPAPlanteMoyenneDAO extends JPAPhysicoChimieDAO<MesurePlanteMoyenne, ValeurPlanteMoyenne> {

    @Override
    protected Class<ValeurPlanteMoyenne> getValeurPhysicoChimieClass() {
        return ValeurPlanteMoyenne.class;
    }

    @Override
    protected Class<MesurePlanteMoyenne> getMesurePhysicoChimieClass() {
        return MesurePlanteMoyenne.class;
    }

    @Override
    protected SingularAttribute<ValeurPlanteMoyenne, MesurePlanteMoyenne> getMesureAttribute() {
        return ValeurPlanteMoyenne_.mesurePlanteMoyenne;
    }

    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected String getDatatypeCode() {
        return IPhysicoChimiePlanteMoyenneDatatypeManager.CODE_DATATYPE_PLANTE_MOYENNE;
    }

}
