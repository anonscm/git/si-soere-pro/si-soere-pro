/*
 *
 */
package org.inra.ecoinfo.pro.refdata.caracteristiqueetape;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ICaracteristiqueEtapeDAO.
 */
public interface ICaracteristiqueEtapeDAO extends IDAO<CaracteristiqueEtapes> {

    /**
     * Liste carac etape.
     *
     * @return the list< caracteristique etapes>
     */
    List<CaracteristiqueEtapes> getAll();

    /**
     * Find by ce.
     *
     * @param cetape_intitule
     * @return the caracteristique etapes
     */
    Optional<CaracteristiqueEtapes> getByNKey(String cetape_intitule);

}
