package org.inra.ecoinfo.pro.refdata.nomregionalsol;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis;

public interface INomRegionalSolDAO extends IDAO<Nomregionalsol> {

    List<Nomregionalsol> getAll();

    Optional<Nomregionalsol> getByNKey(Typesolarvalis typesolarvalis, Typepedologique typepedologique, Region region,String nom_regional);

    Optional<Nomregionalsol> getByNKey(String nomTypesolarvalisl, String nomTypepedologique , String region, String nom_regional);

  
}
