/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.IValeurPhysicoChimieSol;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols_;

/**
 *
 * @author adiankha
 */
public class JPAValeurPhysicoChimieSolDAO extends AbstractJPADAO<ValeurPhysicoChimieSols> implements IValeurPhysicoChimieSol{

    @Override
    public Optional<ValeurPhysicoChimieSols> getByNKeys(RealNode realNode, MesurePhysicoChimieSols mesureSol, String statut) {
        CriteriaQuery<ValeurPhysicoChimieSols> query = builder.createQuery(ValeurPhysicoChimieSols.class);
        Root<ValeurPhysicoChimieSols> v = query.from(ValeurPhysicoChimieSols.class);
        Join<ValeurPhysicoChimieSols, RealNode> rnVariable = v.join(ValeurPhysicoChimieSols_.realNode);
        Join<ValeurPhysicoChimieSols, MesurePhysicoChimieSols> m = v.join(ValeurPhysicoChimieSols_.mesurePhysicoChimieSols);
        
        query
                .where(
                        builder.equal(rnVariable, realNode),
                        builder.equal(m, mesureSol),
                        builder.equal(v.get(ValeurPhysicoChimieSols_.statutvaleur), statut)
                );
        return getOptional(query);
    }
    
    
}
