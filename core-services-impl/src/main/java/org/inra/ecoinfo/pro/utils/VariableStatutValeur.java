/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils;

/**
 *
 * @author adiankha
 */
public class VariableStatutValeur {
    
    String name;
            boolean hasSttatutValeur;
            String statutValeurName;
            
    /**
     *
     */
    public  VariableStatutValeur(){
                super();
            }
            
    /**
     *
     * @param name
     */
    public VariableStatutValeur (final String  name){
        super();
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

   

   

    @Override
    public boolean equals(Object obj) {
         if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final VariableDescriptor other = (VariableDescriptor) obj;
        if (this.hasSttatutValeur != other.hasQualityClass) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.hasSttatutValeur ? 1_231 : 1_237);
        result = prime * result + (this.name == null ? 0 : this.name.hashCode());
        return result;
    }

    /**
     *
     * @return
     */
    public boolean isHasSttatutValeur() {
        return hasSttatutValeur;
    }

    /**
     *
     * @param hasSttatutValeur
     */
    public void setHasSttatutValeur(boolean hasSttatutValeur) {
        this.hasSttatutValeur = hasSttatutValeur;
    }

    /**
     *
     * @return
     */
    public String getStatutValeurName() {
        return statutValeurName;
    }

    /**
     *
     * @param statutValeurName
     */
    public void setStatutValeurName(String statutValeurName) {
        this.statutValeurName = statutValeurName;
    }

    
    
    
    
}
