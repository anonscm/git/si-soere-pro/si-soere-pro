/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne_;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.IValeurPlanteMoyenneDAO;

/**
 *
 * @author adiankha
 */
public class JPAValeurPlanteMoyenneDAO extends AbstractJPADAO<ValeurPlanteMoyenne> implements IValeurPlanteMoyenneDAO {

    /**
     *
     * @param dvumRealNode
     * @param mesurePlanteMoyenne
     * @param ecartype
     * @return
     */
    @Override
    public Optional<ValeurPlanteMoyenne> getByNKeys(RealNode dvumRealNode, MesurePlanteMoyenne mesurePlanteMoyenne, Float ecartype) {
        CriteriaQuery<ValeurPlanteMoyenne> query = builder.createQuery(ValeurPlanteMoyenne.class);
        Root<ValeurPlanteMoyenne> v = query.from(ValeurPlanteMoyenne.class);
        Join<ValeurPlanteMoyenne, RealNode> rnVariable = v.join(ValeurPlanteMoyenne_.realNode);
        Join<ValeurPlanteMoyenne, MesurePlanteMoyenne> m = v.join(ValeurPlanteMoyenne_.mesurePlanteMoyenne);
        query
                .select(v)
                .where(
                        builder.equal(v.get(ValeurPlanteMoyenne_.ecarttype), ecartype),
                        builder.equal(rnVariable, dvumRealNode),
                        builder.equal(m, mesurePlanteMoyenne)
                );
        return getOptional(query);
    }

}
