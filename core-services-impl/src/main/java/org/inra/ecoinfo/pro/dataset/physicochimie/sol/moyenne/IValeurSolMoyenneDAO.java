/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;

/**
 *
 * @author adiankha
 */
public interface IValeurSolMoyenneDAO extends IDAO<ValeurPhysicoChimieSolsMoy>{

    /**
     *
     * @param dvum
     * @param mesureSol
     * @param ecartype
     * @return
     */
    Optional<ValeurPhysicoChimieSolsMoy>getByNKeys(RealNode dvum,MesurePhysicoChimieSolsMoy mesureSol, Float ecartype);
    
}
