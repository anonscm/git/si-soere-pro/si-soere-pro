/**
 *
 */
package org.inra.ecoinfo.pro.refdata.stationexperimentale;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IStationExperimentaleDAO extends IDAO<StationExperimentale> {

    /**
     *
     * @param nom
     * @return
     */
    Optional<StationExperimentale> getByNKey(String nom);

}
