/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie;

import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 * @param <M>
 */
public interface IPhysicoChimieDAO<M extends MesurePhysicoChimie> extends IDAO<M> {

    /**
     * Extract flux tours.
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user the value of user
     * @link(List<SiteACBB>)
     * @link(Date)
     * @link(Date)
     * @return the
     * java.util.List<org.inra.ecoinfo.acbb.dataset.flux.entity.MesureFluxTours>
     */
    List<M> extractMesurePhysicoChimie(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user);

    /**
     *
     * @param selectedDispositif
     * @param user
     * @param interval
     * @param selectedVariables
     * @return the java.lang.Long
     */
    Long getExtractionSize(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user);
    /**
     *
     * @param id
     * @return
     */
    List<Dispositif> getAvailablesDispositifs(IUser user);

    /**
     *
     * @param idVariable
     * @return
     */
    List<CategorieVariable> getAvailableCategorieVariable(Long idVariable);

    List<NodeDataSet> getAvailablesVariablesByDispositif(List<Dispositif> dispositifs, IUser user);
}
