package org.inra.ecoinfo.pro.refdata.pente;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class PenteDaoImpl extends AbstractJPADAO<Pente> implements IPenteDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Pente> getAll() {
        return getAll(Pente.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Pente> getByNKey(String nom) {
        CriteriaQuery<Pente> query = builder.createQuery(Pente.class);
        Root<Pente> pente = query.from(Pente.class);
        query
                .select(pente)
                .where(
                        builder.equal(pente.get(Pente_.nom), nom)
                );
        return getOptional(query);
    }

}
