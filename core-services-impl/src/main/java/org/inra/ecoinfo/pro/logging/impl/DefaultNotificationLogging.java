package org.inra.ecoinfo.pro.logging.impl;

import org.aspectj.lang.JoinPoint.StaticPart;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.LoggerObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultNotificationLogging {

    public void logAddNotificationExit(StaticPart staticPart, Object result) {
        Logger logger = LoggerFactory.getLogger("logging");
        logger.info(new LoggerObject(
                (Notification) ((org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint) staticPart).getArgs()[0]).toString());
    }
}
