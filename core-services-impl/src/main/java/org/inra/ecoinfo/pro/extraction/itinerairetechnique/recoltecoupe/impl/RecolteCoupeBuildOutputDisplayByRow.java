package org.inra.ecoinfo.pro.extraction.itinerairetechnique.recoltecoupe.impl;

import java.io.PrintStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDatatypeManager;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.ITKExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class RecolteCoupeBuildOutputDisplayByRow extends AbstractITKOutputBuilder<MesureRecolteCoupe> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_RECOLTECOUPE";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.itk.itk-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    static final String PATTERB_CSV_21_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;;;;;;;;;;;;;;;";
    static final String LIST_COLUMN_VARIABLE_RECOLTE = "LIST_COLUMN_VARIABLE_RECOLTE";
    static final String CST_0 = "0";

    IVariablesPRODAO variPRODAO;

    final ComparatorVariable comparator = new ComparatorVariable();

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {

        return String.format(
                RecolteCoupeBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        RecolteCoupeBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        RecolteCoupeBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE)
                .get(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE)
                        .get(RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ITKExtractor.CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE));
        return null;
    }

    /**
     *
     * @param mesuresRecolteCoupes
     * @param mesuresRecolteCoupesMap
     * @throws ParseException
     */
    @Override
    protected void buildmap(List<MesureRecolteCoupe> mesuresRecolteCoupes, SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureRecolteCoupe>>> mesuresRecolteCoupesMap) throws ParseException {
        java.util.Iterator<MesureRecolteCoupe> itMesure = mesuresRecolteCoupes
                .iterator();
        while (itMesure.hasNext()) {
            MesureRecolteCoupe mesureRecolteCoupe = itMesure
                    .next();
            DescriptionTraitement echan = mesureRecolteCoupe.getDescriptionTraitement();
            Long siteId = echan.getDispositif().getId();
            if (mesuresRecolteCoupesMap.get(siteId) == null) {
                mesuresRecolteCoupesMap.put(siteId, new TreeMap<>());
            }
            if (mesuresRecolteCoupesMap.get(siteId).get(echan) == null) {
                mesuresRecolteCoupesMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresRecolteCoupesMap.get(siteId).get(echan).put(mesureRecolteCoupe.getDatedebut(), mesureRecolteCoupe);
            itMesure.remove();
        }

    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedRecolteCoupeVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresRecolteCoupesMap
     */
    @Override
    protected void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedRecolteCoupeVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureRecolteCoupe>>> mesuresRecolteCoupesMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresRecolteCoupesMap, selectedIntervalDate);
        } catch (final ParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureRecolteCoupe>>> mesuresRecolteCoupesMap, IntervalDate selectedIntervalDate) throws ParseException {
        try {
            String currentDispositif;

            String curentplacette;
            String currentTraitement;
            String currentParcelle;
            LocalDate datefin;
            String culture;
            String bbch;
            String stade;
            String interventionrecolte;
            String materiel1;
            String materiel2;
            String materiel3;
            String typeobs;
            String observation;
            String niveau;
            String commentaire;
            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    RecolteCoupeBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    RecolteCoupeBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_RECOLTE).split(";"));
            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureRecolteCoupe>> mesureProduitBrutsMapByDisp = mesuresRecolteCoupesMap
                        .get(dispositif.getId());
                if (mesureProduitBrutsMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesureRecolteCoupe>>> itEchan = mesureProduitBrutsMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesureRecolteCoupe>> echanEntry = itEchan
                            .next();
                    DescriptionTraitement echantillon = echanEntry.getKey();
                    SortedMap<LocalDate, MesureRecolteCoupe> mesureProduitBrutsMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                    for (Entry<LocalDate, MesureRecolteCoupe> entrySet : mesureProduitBrutsMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureRecolteCoupe mesureRecolteCoupe = entrySet.getValue();
                        currentDispositif = mesureRecolteCoupe.getCodedispositif();
                        currentTraitement = mesureRecolteCoupe.getDescriptionTraitement().getCode();
                        currentParcelle = mesureRecolteCoupe.getNomparcelle();
                        curentplacette = mesureRecolteCoupe.getNomplacette();
                        datefin = mesureRecolteCoupe.getDatefin();
                        culture = mesureRecolteCoupe.getNomculture();
                        bbch = mesureRecolteCoupe.getCodebbch();
                        stade = mesureRecolteCoupe.getStadeprecision();
                        interventionrecolte = mesureRecolteCoupe.getNomintervention();
                        materiel1 = mesureRecolteCoupe.getMaterielplante1();
                        materiel2 = mesureRecolteCoupe.getMaterielplante2();
                        materiel3 = mesureRecolteCoupe.getMaterielplante3();
                        typeobs = mesureRecolteCoupe.getTypeobservation();
                        observation = mesureRecolteCoupe.getNomobservation();
                        niveau = mesureRecolteCoupe.getNiveauatteint();
                        commentaire = mesureRecolteCoupe.getCommentaire();
                        String genericPattern = LineDataFixe(date, currentDispositif, currentTraitement, currentParcelle, curentplacette, datefin, culture,
                                bbch, stade, interventionrecolte, materiel1, materiel2, materiel3, typeobs, observation, niveau, commentaire);
                        out.print(genericPattern);
                        LineDataVariable(mesureRecolteCoupe, out, listVariable);
                    }
                    itEchan.remove();
                }
            }
        } catch (final ParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentTraitement, String currentParcelle,
            String courentPlacette, LocalDate datefin, String culture, String bbch, String stade, String interventionrecolte,
            String materiel1, String materiel2, String materiel3, String typeobs, String observation, String niveau,
            String commentaire) throws ParseException {
        String genericPattern = String.format(PATTERB_CSV_21_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentTraitement,
                currentParcelle,
                courentPlacette,
                datefin,
                culture,
                bbch, stade, interventionrecolte, materiel1, materiel2, materiel3, typeobs, observation, niveau, commentaire);
        return genericPattern;
    }

    private void LineDataVariable(MesureRecolteCoupe mesureRecolteCoupe, PrintStream out, List<String> listVariable) {
        final List<ValeurRecolteCoupe> valeurRecolteCoupe = mesureRecolteCoupe.getValeurrecolte();
        listVariable.forEach((variableColumnName) -> {
            for (Iterator<ValeurRecolteCoupe> ValeurIterator = valeurRecolteCoupe.iterator(); ValeurIterator.hasNext();) {
                ValeurRecolteCoupe valeur = ValeurIterator.next();
                if (!((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
                    continue;
                }
                String line;
                line = String.format(";%s", getValeurToString(valeur));
                out.print(line);
                ValeurIterator.remove();
            }
        });
        out.println();
    }

    /**
     *
     * @param nombre
     * @param nombreDeChiffresSignificatifs
     * @return
     */
    protected String format(final float nombre, final int nombreDeChiffresSignificatifs) {
        float localNombre = nombre;
        if (Float.floatToRawIntBits(localNombre) == 0) {
            return RecolteCoupeBuildOutputDisplayByRow.CST_0;
        }
        int exposant = 0;
        final float arrondi = (float) Math.pow(10, nombreDeChiffresSignificatifs - 1);
        while (Math.abs(localNombre) <= arrondi) {
            localNombre *= RecolteCoupeBuildOutputDisplayByRow.TEN;
            exposant++;
        }
        return new StringBuffer().append(
                Math.round(localNombre)
                / Math.pow(RecolteCoupeBuildOutputDisplayByRow.TEN, exposant)).toString();
    }

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return RecolteCoupeExtractor.MAP_INDEX_RECOLTE_COUPE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IITKDatatypeManager.CODE_DATATYPE_RECOLTE_COUPE;
    }

    private String getValeurToString(ValeurRecolteCoupe valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else if (valeur.getListeItineraire() != null) {
            return valeur.getListeItineraire().getListe_valeur();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(RecolteCoupeBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
