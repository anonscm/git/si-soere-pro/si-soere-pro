/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationechantillon;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPALocalisationEchantillonDAO extends AbstractJPADAO<LocalisationEchantillon> implements ILocalisationEchantillonDAO {

    /**
     *
     * @return
     */
    @Override
    public List<LocalisationEchantillon> getAll() {
        return getAll(LocalisationEchantillon.class);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<LocalisationEchantillon> getByNKey(String code) {
        CriteriaQuery<LocalisationEchantillon> query = builder.createQuery(LocalisationEchantillon.class);
        Root<LocalisationEchantillon> localisationEchantillon = query.from(LocalisationEchantillon.class);
        query
                .select(localisationEchantillon)
                .where(
                        builder.equal(localisationEchantillon.get(LocalisationEchantillon_.code), code)
                );
        return getOptional(query);
    }

}
