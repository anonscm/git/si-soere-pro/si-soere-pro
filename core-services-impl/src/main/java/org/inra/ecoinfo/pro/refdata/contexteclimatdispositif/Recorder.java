package org.inra.ecoinfo.pro.refdata.contexteclimatdispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.contextclimatiquedispositif.ContexteClimatiqueDispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.typeclimat.ITypeClimatDAO;
import org.inra.ecoinfo.pro.refdata.typeclimat.Typeclimat;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<ContexteClimatiqueDispositif> {

    private static final String PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO = "PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    IContexteClimatDispositifDAO ccdDAO;
    IDispositifDAO dispositifDAO;
    ITypeClimatDAO typeclimatDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;
    ILieuDAO lieuDAO;
    Properties ccdComEN;

    private String[] listeDispositifPossibles;
    private String[] listeTypeclimatPossibles;
    private Map<String, String[]> listeLieuPossibles;
    private String[] listeBooleansPossible;

    private void createCCD(final ContexteClimatiqueDispositif ccd) throws BusinessException {
        try {
            ccdDAO.saveOrUpdate(ccd);
            ccdDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    /**
     *
     * @param preci
     * @param temp
     * @param etp
     * @param stationmeteo
     * @param nomstation
     * @param comment
     * @param annee
     * @param dispositif
     * @param typeclimat
     * @param dbccd
     * @throws BusinessException
     */
    public void updateCCD(double preci, double temp, double etp, String stationmeteo, String nomstation, String comment, long annee,
            Dispositif dispositif, Typeclimat typeclimat, ContexteClimatiqueDispositif dbccd) throws BusinessException {
        try {
            dbccd.setCcd_precipitation(preci);
            dbccd.setCcd_temperature(temp);
            dbccd.setCcd_etp(etp);
            dbccd.setCcd_stationmeteo(stationmeteo);
            dbccd.setCcd_nomstationmeteo(nomstation);
            dbccd.setDispositif(dispositif);
            dbccd.setTypeclimat(typeclimat);
            dbccd.setComment(comment);
            dbccd.setAnnee(annee);
            ccdDAO.saveOrUpdate(dbccd);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void createOrUpdateCCD(Dispositif dispositif, Typeclimat typeclimat, double preci, double temp, double etp, String stationmeteo, String nomstation, long annee, String comment,
            ContexteClimatiqueDispositif dbccd) throws BusinessException {
        if (dbccd == null) {
            ContexteClimatiqueDispositif ccd = new ContexteClimatiqueDispositif(dispositif, typeclimat, preci, temp, etp, stationmeteo, nomstation, annee, comment);
            ccd.setAnnee(annee);
            ccd.setCcd_precipitation(preci);
            ccd.setCcd_temperature(temp);
            ccd.setCcd_etp(etp);
            ccd.setCcd_stationmeteo(stationmeteo);
            ccd.setCcd_nomstationmeteo(nomstation);
            ccd.setComment(comment);
            ccd.setDispositif(dispositif);
            ccd.setTypeclimat(typeclimat);
            ccd.setAnnee(annee);
            createCCD(ccd);
        } else {
            updateCCD(preci, temp, etp, stationmeteo, nomstation, comment, annee, dispositif, typeclimat, dbccd);
        }
    }

    private void persitCDD(Dispositif dispositif,  Typeclimat typeclimat,double temp, double preci, double etp, String stationmeteo, String nomstation,long annee, 
            String comment) throws BusinessException {
        final ContexteClimatiqueDispositif dbccd = ccdDAO.getByNKey(dispositif, preci).orElse(null);
        createOrUpdateCCD(dispositif, typeclimat, preci, temp, etp, stationmeteo, nomstation, annee, comment, dbccd);
    }

    private void disposifPossibles() throws BusinessException {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCodeDispo();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void typeClimatPossibles() throws BusinessException {
        List<Typeclimat> groupetype = typeclimatDAO.getAll();
        String[] listetypeclimats = new String[groupetype.size() + 1];
        listetypeclimats[0] = "";
        int index = 1;
        for (Typeclimat typeclimat : groupetype) {
            listetypeclimats[index++] = typeclimat.getNom();
        }
        this.listeTypeclimatPossibles = listetypeclimats;
    }

    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    private void listeBooleanPossibles() throws BusinessException {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values, ContexteClimatiqueDispositif.NAME_ENTITY_JPA);
                String codeDispo = values[0];
                String lieu = values[1];
                double precipitation = Double.valueOf(values[4]);
                Dispositif dbdispo = dispositifDAO.getByNKey(codeDispo, lieu).orElse(null);
                ContexteClimatiqueDispositif ccDispo = ccdDAO.getByNKey(dbdispo, precipitation)
                        .orElseThrow(PersistenceException::new);
                ccdDAO.remove(ccDispo);
                values = csvp.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<ContexteClimatiqueDispositif> getAllElements() throws BusinessException {
        return ccdDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ContexteClimatiqueDispositif.NAME_ENTITY_JPA);
                int indexdisp = tokenizerValues.currentTokenIndex();
                String code = tokenizerValues.nextToken();
                String lieu = tokenizerValues.nextToken();
                int indexclimat = tokenizerValues.currentTokenIndex();
                String climat = tokenizerValues.nextToken();
                int annee = verifieInt(tokenizerValues, line, true, errorsReport);
                double precipitation = verifieDouble(tokenizerValues, line, true, errorsReport);
                double temperature = verifieDouble(tokenizerValues, line, true, errorsReport);
                final String nomsta = tokenizerValues.nextToken();
                final String station = tokenizerValues.nextToken();
                final double etp =  verifieDouble(tokenizerValues, line, false, errorsReport);
                final String comment = tokenizerValues.nextToken();

                Dispositif dbdispositif = dispositifDAO.getByNKey(code, lieu).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO), line, indexdisp, code, lieu));
                }
                Typeclimat dbclimat = typeclimatDAO.getByNKey(climat).orElse(null);
                if (dbclimat == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO), line, indexclimat, climat));
                }
                if (!errorsReport.hasErrors()) {
                    persitCDD(dbdispositif, dbclimat, temperature, precipitation, etp, station, nomsta, annee, comment);
                }
                values = csvp.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ContexteClimatiqueDispositif ccd) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String valeurProduit = ccd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : ccd.getDispositif() != null ? ccd.getDispositif().getCodeDispo() : "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifPossibles, null, true, false, true);
        String valeurProcede = ccd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : ccd.getDispositif() != null ? ccd.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getTypeclimat() != null ? ccd.getTypeclimat().getNom() : "",
                        listeTypeclimatPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getAnnee() == 0 ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getAnnee(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getCcd_precipitation() == 0.0 ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getCcd_precipitation(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getCcd_temperature() == 0.0 ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getCcd_temperature(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getCcd_nomstationmeteo() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getCcd_nomstationmeteo(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getCcd_stationmeteo() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ccd.getCcd_stationmeteo() != null
                        ? ccd.getCcd_stationmeteo() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getCcd_etp() == EMPTY_DOUBLE_VALUE ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getCcd_etp(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getComment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ccd.getComment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ccd == null || ccd.getComment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ccdComEN.getProperty(ccd.getComment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param ccdDAO
     */
    public void setCcdDAO(IContexteClimatDispositifDAO ccdDAO) {
        this.ccdDAO = ccdDAO;
    }

    /**
     *
     * @return
     */
    public IContexteClimatDispositifDAO getCcdDAO() {
        return ccdDAO;
    }

    @Override
    protected ModelGridMetadata<ContexteClimatiqueDispositif> initModelGridMetadata() {
        try {
            typeClimatPossibles();
            disposifPossibles();
            listeBooleanPossibles();
        } catch (BusinessException ex) {
            LOGGER.info("can't initModelGridMetadata ", ex);
        }
        ccdComEN = localizationManager.newProperties(ContexteClimatiqueDispositif.NAME_ENTITY_JPA, ContexteClimatiqueDispositif.COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    /**
     *
     * @return
     */
    public ITypeClimatDAO getTypeclimatDAO() {
        return typeclimatDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param typeclimatDAO
     */
    public void setTypeclimatDAO(ITypeClimatDAO typeclimatDAO) {
        this.typeclimatDAO = typeclimatDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param lieuDAO
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

}
