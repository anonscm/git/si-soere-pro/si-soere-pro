/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.categorievariable;

import java.util.List;
import org.inra.ecoinfo.pro.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;

/**
 *
 * @author adiankha
 */
public interface IGroupeVariableBuilder {
    
    /**
     *
     */
    static String BEAN_ID = "groupeVariableBuilder";
     
    /**
     *
     * @param variables
     * @return
     */
    List<GroupeVariableVO> build(List< VariablesPRO> variables);
    
}
