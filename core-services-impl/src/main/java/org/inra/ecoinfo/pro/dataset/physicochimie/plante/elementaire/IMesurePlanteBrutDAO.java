/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;

/**
 *
 * @author adiankha
 * @param <T>
 */
public interface IMesurePlanteBrutDAO<T> extends IDAO<MesurePlanteElementaire> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesurePlanteElementaire> getByKeys(String keymesure);

}
