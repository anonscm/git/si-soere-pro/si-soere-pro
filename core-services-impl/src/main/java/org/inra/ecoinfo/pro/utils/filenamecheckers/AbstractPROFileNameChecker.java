package org.inra.ecoinfo.pro.utils.filenamecheckers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.slf4j.LoggerFactory;
import org.inra.ecoinfo.dataset.exception.InvalidFileNameException;
import org.inra.ecoinfo.dataset.impl.filenamecheckers.AbstractFileNameChecker;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.slf4j.Logger;

/**
 *
 * @author adiankha
 */
public abstract class AbstractPROFileNameChecker extends AbstractFileNameChecker {

    /**
     * The Constant PATTERN @link(String).
     */
//    protected static final String PATTERN = "^((%s)-(%s)|.*?)_(%s|.*?)_(.*)_(.*)\\.csv$";
    public static final String INVALID_FILE_NAME = "%s_%s_%s_%s.csv";
    public static final String PATTERN_FILE_NAME_PATH = "%s_%s_%s_%s#V%d#.csv";
    public static final String INVALID_FILE_NAME_WITH_PARCELLE = "%s_%s_%s_%s_%s.csv";
    public static final String PATTERN_FILE_NAME_WITH_PARCELLE_PATH = "%s_%s_%s_%s_%s#V%d#.csv";

    static final Logger LOGGER = LoggerFactory.getLogger(AbstractPROFileNameChecker.class);
    protected static final String CST_STRING_EMPTY = "";

    protected IDatasetConfiguration datasetConfiguration;

    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    protected IDispositifDAO dispositifDAO;

    public AbstractPROFileNameChecker() {
        super();
    }

    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    @Override
    public boolean isValidFileName(final String fileName, final VersionFile version)
            throws InvalidFileNameException {
        String finalFileName = fileName;
        finalFileName = this.cleanFileName(finalFileName);
        Dispositif dispositif = RecorderPRO.getDispositifFromVersion(version);
        String expectedFileCodeDispositif = dispositif.getCodeforFileName();
        DataType datatype = RecorderPRO.getDatatypeFromVersion(version);
        final String codeDispositifLieu = dispositif.getCode();
        String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
        String codeLieu = Dispositif.getNomLieu(codeDispositifLieu);
        final String expectedDatatype = datatype.getCode();

        final Matcher splitFilename = Pattern.compile(
                String.format(AbstractFileNameChecker.PATTERN, expectedFileCodeDispositif, expectedDatatype))
                .matcher(finalFileName);
        this.testPath(expectedFileCodeDispositif, expectedDatatype, splitFilename);
        final String fileCodeDispositif = splitFilename.group(1);
        this.testDispositif(version, expectedFileCodeDispositif, expectedDatatype, fileCodeDispositif);
        final String datatypeName = Utils.createCodeFromString(splitFilename.group(2));
        this.testDatatype(expectedFileCodeDispositif, expectedDatatype, datatypeName);
        this.testDates(version, expectedFileCodeDispositif, expectedDatatype, splitFilename);
        return true;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    @Override
    protected void testDatatype(final String expectedFileCodeDispositifForFile, final String expectedDatatype,
            final String datatypeName) throws InvalidFileNameException {
            if (!expectedDatatype.equals(datatypeName)) {
                throw new InvalidFileNameException(String.format(INVALID_FILE_NAME,
                        expectedFileCodeDispositifForFile,
                        expectedDatatype,
                        getDatePattern(),
                        getDatePattern()));
            }

    }

    protected void testDispositif(final VersionFile version, final String expectedFileCodeDispositif,
            final String currentDatatype, final String fileCodeDispositif) throws InvalidFileNameException {
        String codeDispositif = Dispositif.getNomDispositif(expectedFileCodeDispositif);
        String codeLieu = Dispositif.getNomLieu(expectedFileCodeDispositif);
            if (!expectedFileCodeDispositif.equals(fileCodeDispositif)) {
                throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, expectedFileCodeDispositif, currentDatatype,
                        getDatePattern(), getDatePattern(), FILE_FORMAT));
            }
    }

    protected void testPath(String currentDispo, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        if (!splitFilename.find()) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME,
                    currentDispo, currentDatatype,
                    getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
    }

    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    public String getFilePath(Dataset dataset) {
        return dataset.buildDownloadFilename(datasetConfiguration);
    }

    @Override
    protected void testDates(VersionFile version, String currentDispositif, String currentDatatype, Matcher splitFilename) throws InvalidFileNameException {
        
        IntervalDate intervalDate;
        try {
            intervalDate = new IntervalDate(splitFilename.group(3), splitFilename.group(4), getDatePattern());
        } catch (Exception e1) {
            throw new InvalidFileNameException(String.format(INVALID_FILE_NAME, currentDispositif, currentDatatype, getDatePattern(), getDatePattern(), FILE_FORMAT));
        }
        if (version != null) {
            version.getDataset().setDateDebutPeriode(intervalDate.getBeginDate());
            version.getDataset().setDateFinPeriode(intervalDate.getEndDate());
        }
    }

}
