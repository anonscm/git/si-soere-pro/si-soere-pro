/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cultures;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.especeplante.EspecePlante;
import org.inra.ecoinfo.pro.refdata.especeplante.IEspecePlanteDAO;
import org.inra.ecoinfo.pro.refdata.typeculture.ITypeCultureDAO;
import org.inra.ecoinfo.pro.refdata.typeculture.TypeCulture;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Cultures> {

    private static final String PROPERTY_MSG_CULTURE_BAD_TCULTURE = "PROPERTY_MSG_CULTURE_BAD_TCULTURE";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_CULTURE_BAD_ESPECE = "PROPERTY_MSG_CULTURE_BAD_ESPECE";

    ICulturesDAO culturesDAO;
    ITypeCultureDAO typeCultureDAO;
    IEspecePlanteDAO especePlanteDAO;
    Properties comentEn;

    private String[] listeTCulturePossibles;
    private String[] listeEspecePossibles;

    /**
     *
     * @param cultures
     * @throws BusinessException
     */
    public void createCultures(Cultures cultures) throws BusinessException {
        try {
            culturesDAO.saveOrUpdate(cultures);
            culturesDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create CreateCultures");
        }
    }

    /**
     *
     * @param code
     * @param nom
     * @param mycode
     * @param espece
     * @param espece2
     * @param espece3
     * @param espece4
     * @param espece5
     * @param espece6
     * @param tculture
     * @param semis
     * @param recolte
     * @param vegetation
     * @param commentaire
     * @param dbCultures
     * @throws BusinessException
     */
    public void updateCultures(String code, String nom, String mycode, EspecePlante espece, String espece2, String espece3, String espece4, String espece5, String espece6,
            TypeCulture tculture, String semis, String recolte, String vegetation, String commentaire,
            Cultures dbCultures) throws BusinessException {
        try {

            dbCultures.setCode(code);
            dbCultures.setNom(nom);
            dbCultures.setMycode(code);
            dbCultures.setMycode(mycode);
            dbCultures.setEspecePlante(espece);
            dbCultures.setEspece2(espece2);
            dbCultures.setEspece3(espece3);
            dbCultures.setEspece4(espece4);
            dbCultures.setEspece5(espece5);
            dbCultures.setEspece6(espece6);
            dbCultures.setPeriodesemis(semis);
            dbCultures.setPerioderecolte(recolte);
            dbCultures.setDureevegetation(vegetation);
            dbCultures.setCommentaire(commentaire);
            culturesDAO.saveOrUpdate(dbCultures);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update CreateCultures");
        }

    }

    /**
     *
     * @param code
     * @param nom
     * @param mycode
     * @param espece
     * @param espece2
     * @param espece3
     * @param espece4
     * @param espece5
     * @param espece6
     * @param tculture
     * @param semis
     * @param recolte
     * @param vegetation
     * @param commentaire
     * @param dbCultures
     * @throws BusinessException
     */
    public void createOrUpdateCultures(String code, String nom, String mycode, EspecePlante espece, String espece2, String espece3, String espece4, String espece5, String espece6,
            TypeCulture tculture, String semis, String recolte, String vegetation, String commentaire,
            Cultures dbCultures) throws BusinessException {
        if (dbCultures == null) {
            Cultures cultures = new Cultures(code, nom, espece, espece2, espece3, espece4, espece5, espece6, tculture, semis, recolte, vegetation, commentaire);
            cultures.setCode(code);
            cultures.setNom(nom);
            cultures.setMycode(mycode);
            cultures.setEspecePlante(espece);
            cultures.setEspece2(espece2);
            cultures.setEspece3(espece3);
            cultures.setEspece4(espece4);
            cultures.setEspece5(espece5);
            cultures.setEspece6(espece6);
            cultures.setPeriodesemis(semis);
            cultures.setTypeculture(tculture);
            cultures.setPerioderecolte(recolte);
            cultures.setDureevegetation(vegetation);
            cultures.setCommentaire(commentaire);
            createCultures(cultures);
        } else {
            updateCultures(code, nom, mycode, espece, espece2, espece3, espece4, espece5, espece6, tculture, semis, recolte, vegetation, commentaire, dbCultures);
        }
    }

    /**
     *
     * @param code
     * @param nom
     * @param mycode
     * @param espece
     * @param espece2
     * @param espece3
     * @param espece4
     * @param espece5
     * @param espece6
     * @param tculture
     * @param semis
     * @param recolte
     * @param vegetation
     * @param commentaire
     * @throws BusinessException
     */
    public void persistCultures(String code, String nom, String mycode, EspecePlante espece, String espece2, String espece3, String espece4, String espece5, String espece6,
            TypeCulture tculture, String semis, String recolte, String vegetation, String commentaire)
            throws BusinessException {
        Cultures cultures = culturesDAO.getByNKey(code).orElse(null);
        createOrUpdateCultures(code, nom, mycode, espece, espece2, espece3, espece4, espece5, espece6, tculture, semis, recolte, vegetation, commentaire, cultures);
    }

    private void listeTypeCulturePossibles() throws BusinessException {
        List<TypeCulture> groupecv = typeCultureDAO.getAll();
        String[] listeCVPossibles = new String[groupecv.size() + 1];
        listeCVPossibles[0] = "";
        int index = 1;
        for (TypeCulture cepage : groupecv) {
            listeCVPossibles[index++] = cepage.getLibelle();
        }
        this.listeTCulturePossibles = listeCVPossibles;
    }

    private void listeEspecePossibles() throws BusinessException {
        List<EspecePlante> groupecv = especePlanteDAO.getAll();
        String[] listeEspecePossible = new String[groupecv.size() + 1];
        listeEspecePossible[0] = "";
        int index = 1;
        for (EspecePlante cepage : groupecv) {
            listeEspecePossible[index++] = cepage.getNom();
        }
        this.listeEspecePossibles = listeEspecePossible;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = values[1];
                final String libelleTypeCulture = values[2];
                final String nomEspece = values[3];
                TypeCulture typeCulture = typeCultureDAO.getByNKey(libelleTypeCulture).orElse(null);
                EspecePlante especePlante = especePlanteDAO.getByNKey(nomEspece).orElse(null);
                culturesDAO.remove(culturesDAO.getByNKey(nom)
                        .orElseThrow(()->new BusinessException("Can'fint culture")));
                values = csvp.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            long line = 0;
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Cultures.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(nom);
                long indextc = tokenizerValues.currentTokenIndex();
                final String tculture = tokenizerValues.nextToken();
                final String espece = tokenizerValues.nextToken();
                final String espece2 = tokenizerValues.nextToken();
                final String espece3 = tokenizerValues.nextToken();
                final String espece4 = tokenizerValues.nextToken();
                final String espece5 = tokenizerValues.nextToken();
                final String espece6 = tokenizerValues.nextToken();
                final String semis = tokenizerValues.nextToken();

                final String recolte = tokenizerValues.nextToken();
                final String vegetation = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();

                TypeCulture dbtculture = typeCultureDAO.getByNKey(tculture).orElse(null);
                if (dbtculture == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CULTURE_BAD_TCULTURE), line, indextc, tculture));
                }

                EspecePlante dbespece = especePlanteDAO.getByNKey(espece).orElse(null);
                if (dbespece == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CULTURE_BAD_ESPECE), line, indextc, espece));

                }
                if (!errorsReport.hasErrors()) {
                    persistCultures(code, nom, mycode, dbespece, espece2, espece3, espece4, espece5, espece6, dbtculture, semis, recolte, vegetation, commentaire);
                }
                values = csvp.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    protected List<Cultures> getAllElements() throws BusinessException {
        return culturesDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Cultures cultures) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getCode() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getCode(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getTypeculture() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getTypeculture().getLibelle() != null
                        ? cultures.getTypeculture().getLibelle() : "",
                        listeTCulturePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getEspecePlante() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getEspecePlante() != null
                        ? cultures.getEspecePlante() : "",
                        listeEspecePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getEspece2() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getEspece2() != null
                        ? cultures.getEspece2() : "",
                        listeEspecePossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getEspece3() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getEspece3() != null
                        ? cultures.getEspece3() : "",
                        listeEspecePossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getEspece4() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getEspece4() != null
                        ? cultures.getEspece4() : "",
                        listeEspecePossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getEspece5() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getEspece5() != null
                        ? cultures.getEspece5() : "",
                        listeEspecePossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getEspece6() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getEspece6() != null
                        ? cultures.getEspece6() : "",
                        listeEspecePossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getPeriodesemis() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getPeriodesemis(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getPerioderecolte() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getPerioderecolte(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getDureevegetation() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getDureevegetation(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultures.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultures == null || cultures.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comentEn.getProperty(cultures.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Cultures> initModelGridMetadata() {
        try {
            listeTypeCulturePossibles();
            listeEspecePossibles();
        } catch (BusinessException e) {
        }
        comentEn = localizationManager.newProperties(Cultures.NAME_ENTITY_JPA, Cultures.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param culturesDAO
     */
    public void setCulturesDAO(ICulturesDAO culturesDAO) {
        this.culturesDAO = culturesDAO;
    }

    /**
     *
     * @param typeCultureDAO
     */
    public void setTypeCultureDAO(ITypeCultureDAO typeCultureDAO) {
        this.typeCultureDAO = typeCultureDAO;
    }

    /**
     *
     * @param especePlanteDAO
     */
    public void setEspecePlanteDAO(IEspecePlanteDAO especePlanteDAO) {
        this.especePlanteDAO = especePlanteDAO;
    }

}
