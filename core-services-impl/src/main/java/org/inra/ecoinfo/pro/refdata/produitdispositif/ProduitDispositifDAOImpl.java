package org.inra.ecoinfo.pro.refdata.produitdispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
public class ProduitDispositifDAOImpl extends AbstractJPADAO<ProduitDispositif> implements IProduitDisposotifDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ProduitDispositif> getAll() {
        return getAll(ProduitDispositif.class);
    }

    /**
     *
     * @param produits
     * @param dispositif
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<ProduitDispositif> getByNKey(Produits produits, Dispositif dispositif) {
        CriteriaQuery<ProduitDispositif> query = builder.createQuery(ProduitDispositif.class);
        Root<ProduitDispositif> produitDispositif = query.from(ProduitDispositif.class);
        Join<ProduitDispositif, Produits> prod = produitDispositif.join(ProduitDispositif_.produits);
        Join<ProduitDispositif, Dispositif> dispo = produitDispositif.join(ProduitDispositif_.dispositif);
        query
                .select(produitDispositif)
                .where(
                        builder.equal(prod, produits),
                        builder.equal(dispo, dispositif)
                );
        return getOptional(query);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<ProduitDispositif> getByNKey(String code) {
        CriteriaQuery<ProduitDispositif> query = builder.createQuery(ProduitDispositif.class);
        Root<ProduitDispositif> produitDispositif = query.from(ProduitDispositif.class);
        query
                .select(produitDispositif)
                .where(
                        builder.equal(produitDispositif.get(ProduitDispositif_.code), code)
                );
        return getOptional(query);
    }
}
