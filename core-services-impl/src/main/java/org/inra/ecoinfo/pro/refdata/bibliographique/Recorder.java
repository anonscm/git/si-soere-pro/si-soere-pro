/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.bibliographique;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Bibliographique> {

    IBibliographiqueDAO biblioDAO;

    public void createBiblio(Bibliographique biblio) throws BusinessException {
        try {
            biblioDAO.saveOrUpdate(biblio);
            biblioDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create biblio");
        }
    }

    private void updatecreateBiblio(final String doi, String code, String auteur, String annee, String url, final Bibliographique dbbiblio) throws BusinessException {
        try {
            dbbiblio.setBiblio_doi(doi);
            dbbiblio.setBiblio_mycode(code);
            dbbiblio.setFist_auteur(auteur);
            dbbiblio.setAnnee(annee);
            dbbiblio.setUrl(url);
            biblioDAO.saveOrUpdate(dbbiblio);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update biblio");
        }
    }

    private void createOrUpdatecreateBiblio(final String doi, String code, String auteur, String annee, String url, final Bibliographique dbbiblio) throws BusinessException {
        if (dbbiblio == null) {
            final Bibliographique biblio = new Bibliographique(doi, auteur, annee, url);
            biblio.setBiblio_doi(doi);
            biblio.setBiblio_mycode(code);
            biblio.setFist_auteur(auteur);
            biblio.setAnnee(annee);
            biblio.setUrl(url);
            createBiblio(biblio);
        } else {
            updatecreateBiblio(doi, code, auteur, annee, url, dbbiblio);
        }
    }

    private void persistBiblio(final String doi, String code, String auteur, String annee, String url) throws BusinessException {
        final Bibliographique qualifiant = biblioDAO.getByNKey(doi).orElse(null);
        createOrUpdatecreateBiblio(doi, code, auteur, annee, url, qualifiant);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String doi = tokenizerValues.nextToken();

                biblioDAO.remove(biblioDAO.getByNKey(Utils.createCodeFromString(doi)).orElseThrow(() -> new BusinessException("can't delete biblio")));
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Bibliographique> getAllElements() throws BusinessException {
        return biblioDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Bibliographique.NAME_ENTITY_JPA);
                final String doi = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(doi);
                final String auteur = tokenizerValues.nextToken();
                final String annee = tokenizerValues.nextToken();
                final String url = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistBiblio(doi, mycode, auteur, annee, url);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Bibliographique biblio) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(biblio == null || biblio.getBiblio_doi() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : biblio.getBiblio_doi(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(biblio == null || biblio.getFist_auteur() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : biblio.getFist_auteur(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(biblio == null || biblio.getAnnee() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : biblio.getAnnee(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(biblio == null || biblio.getUrl() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : biblio.getUrl(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    public void setBiblioDAO(IBibliographiqueDAO biblioDAO) {
        this.biblioDAO = biblioDAO;
    }

}
