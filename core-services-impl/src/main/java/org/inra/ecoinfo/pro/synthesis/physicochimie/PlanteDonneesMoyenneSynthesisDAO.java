/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne_;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantebrut.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantebrut.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class PlanteDonneesMoyenneSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurPlanteMoyenne, MesurePlanteMoyenne> {

    @Override
    Class<ValeurPlanteMoyenne> getValueClass() {
        return ValeurPlanteMoyenne.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurPlanteMoyenne, MesurePlanteMoyenne> getMesureAttribute() {
        return ValeurPlanteMoyenne_.mesurePlanteMoyenne;
    }
}
