/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.sol.elementaire.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols_;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.jpa.JPAPhysicoChimieDAO;
import org.inra.ecoinfo.pro.synthesis.physicochimiesol.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPASolBrutDAO extends JPAPhysicoChimieDAO<MesurePhysicoChimieSols, ValeurPhysicoChimieSols>{

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurPhysicoChimieSols> getValeurPhysicoChimieClass() {
        return ValeurPhysicoChimieSols.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesurePhysicoChimieSols> getMesurePhysicoChimieClass() {
        return MesurePhysicoChimieSols.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurPhysicoChimieSols, MesurePhysicoChimieSols> getMesureAttribute() {
        return ValeurPhysicoChimieSols_.mesurePhysicoChimieSols;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
       return IPhysicoChimieSolDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE;
    }
    
  
}
