/**
 *
 */
package org.inra.ecoinfo.pro.refdata.rolestructure;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IRoleStructureDAO extends IDAO<RoleStructure> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<RoleStructure> getByNKey(String libelle);

}
