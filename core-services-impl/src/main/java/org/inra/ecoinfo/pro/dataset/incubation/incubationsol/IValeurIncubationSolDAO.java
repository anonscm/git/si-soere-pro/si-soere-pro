/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsol;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol;

/**
 *
 * @author vjkoyao
 */
public interface IValeurIncubationSolDAO extends IDAO<ValeurIncubationSol> {

    /**
     *
     * @param realNode
     * @param mesureIncubationSol
     * @param statutvaleur
     * @return
     */
    public Optional<ValeurIncubationSol> getByNkeys(RealNode realNode, MesureIncubationSol mesureIncubationSol, String statutvaleur);

}
