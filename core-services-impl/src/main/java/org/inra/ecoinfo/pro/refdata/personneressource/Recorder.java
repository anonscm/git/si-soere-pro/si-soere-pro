/**
 *
 */
package org.inra.ecoinfo.pro.refdata.personneressource;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.structure.IStructureDAO;
import org.inra.ecoinfo.pro.refdata.structure.Structure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<PersonneRessource> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IStructureDAO structureDAO;
    IPersonneRessourceDAO personneRessourceDAO;

    Map<String, String[]> precisionPossibles = new TreeMap<String, String[]>();

    /**
     * @param nom
     * @param prenom
     * @param eMail
     * @param structure
     * @param fonction
     * @param noTelPoste
     * @param dbPersonneRessource
     * @param personneRessource
     * @throws BusinessException
     */
    private void createOrUpdate(String nom, String prenom, String eMail, Structure structure, String fonction, String noTelPoste, PersonneRessource dbPersonneRessource, PersonneRessource personneRessource) throws BusinessException {
        try {
            if (dbPersonneRessource == null) {
                personneRessourceDAO.saveOrUpdate(personneRessource);
            } else {
                dbPersonneRessource.setNom(nom);
                dbPersonneRessource.setPrenom(prenom);
                dbPersonneRessource.setEmail(eMail);
                dbPersonneRessource.setStructure(structure);
                dbPersonneRessource.setFonction(fonction);
                dbPersonneRessource.setNoTelPoste(noTelPoste);
                personneRessourceDAO.saveOrUpdate(dbPersonneRessource);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create personneRessource");
        }
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getNomPrecisionStructurePossibles() throws BusinessException {
        List<Structure> lstStructures = structureDAO.getAll();

        String[] nomPrecisionStructurePossibles = new String[lstStructures.size()];
        int index = 0;
        for (Structure structure : lstStructures) {
            nomPrecisionStructurePossibles[index++] = structure.getNom() + " (" + structure.getPrecision() + ")";
        }

        return nomPrecisionStructurePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();
                String prenom = tokenizerValues.nextToken();
                String eMail = tokenizerValues.nextToken();

                personneRessourceDAO.remove(personneRessourceDAO.getByNKey(nom, prenom, eMail)
                .orElseThrow(()-> new BusinessException("can't get personneRessource")));

                values = parser.getLine();

            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<PersonneRessource> getAllElements() throws BusinessException {
        return personneRessourceDAO.getAll(PersonneRessource.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PersonneRessource personneRessource) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesFonction = localizationManager.newProperties(PersonneRessource.TABLE_NAME, RefDataConstantes.COLUMN_FONCTION_PERSONNERESSOURCE, Locale.ENGLISH);

        String localizedChampFonction = "";

        if (personneRessource != null) {
            localizedChampFonction = propertiesFonction.containsKey(personneRessource.getFonction()) ? propertiesFonction.getProperty(personneRessource.getFonction()) : personneRessource.getFonction();
        }

        //Nom
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : personneRessource.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        //Prénom
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : personneRessource.getPrenom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        //Email
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : personneRessource.getEmail(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        //Précision
        String nomPrecisionStructure = personneRessource == null ? "" : personneRessource.getStructure().getNom() + " (" + personneRessource.getStructure().getPrecision() + ")";
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : nomPrecisionStructure, getNomPrecisionStructurePossibles(), null, false, false, true));

        //Fonction
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : personneRessource.getFonction(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : localizedChampFonction, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        //No téléphone poste
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(personneRessource == null ? Constantes.STRING_EMPTY : personneRessource.getNoTelPoste(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, PersonneRessource.TABLE_NAME);

                String nom = tokenizerValues.nextToken();
                String prenom = tokenizerValues.nextToken();
                String eMail = tokenizerValues.nextToken();
                int index = tokenizerValues.currentTokenIndex();
                String nomPrecisionStructure = tokenizerValues.nextToken();
                String fonction = tokenizerValues.nextToken();
                String noTelPoste = tokenizerValues.nextToken();

                int index1 = nomPrecisionStructure.indexOf('(');
                int index2 = nomPrecisionStructure.indexOf(')');
                String nomStr = index1 != -1 ? nomPrecisionStructure.substring(0, index1).trim() : nomPrecisionStructure;
                String precisionStr = index1 != -1 && index2 != -1 ? nomPrecisionStructure.substring(index1 + 1, index2).trim() : nomPrecisionStructure;

                Structure structure = verifieStructure(nomStr, precisionStr, line + 1, index + 1, errorsReport);
                RefDataUtil.verifieNotelNoFax(noTelPoste, null, errorsReport, localizationManager);
                RefDataUtil.verifieEmail(eMail.toLowerCase(), errorsReport, localizationManager);

                PersonneRessource personneRessource = new PersonneRessource(nom, prenom, eMail, structure, fonction, noTelPoste);
                PersonneRessource dbPersonneRessource = personneRessourceDAO.getByNKey(nom, prenom, eMail).orElse(null);

                if (!errorsReport.hasErrors()) {
                    createOrUpdate(nom, prenom, eMail, structure, fonction, noTelPoste, dbPersonneRessource, personneRessource);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);

        }
    }

    /**
     * @param nomStr
     * @param precisionStr
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Structure verifieStructure(String nomStr, String precisionStr, long line, int index, ErrorsReport errorsReport) {
        Structure structure = structureDAO.getByNKey(nomStr, precisionStr).orElse(null);
        if (structure == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "STRUCT_NONDEFINI"), line, index, nomStr + " - " + precisionStr));
        }

        return structure;
    }

    /**
     * @param structureDAO the structureDAO to set
     */
    public void setStructureDAO(IStructureDAO structureDAO) {
        this.structureDAO = structureDAO;
    }

    /**
     * @param personneRessourceDAO the personneRessourceDAO to set
     */
    public void setPersonneRessourceDAO(IPersonneRessourceDAO personneRessourceDAO) {
        this.personneRessourceDAO = personneRessourceDAO;
    }

}
