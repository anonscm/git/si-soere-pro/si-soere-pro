/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre.impl;

import java.util.Collection;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationDAO;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class FluxChambreExtractor extends MO implements IExtractor{

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE = "extractionResultFluxchambre" ;

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.fluxchambre.messages";

    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";

    IExtractor chambreExtractor;
    
    
    
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        
        int extractionResult = 1;
        
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(FluxChambreParameterVO.FLUXCHAMBRE)))
                    .isEmpty()) {
                this.chambreExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        
         if (extractionResult == 0) {
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    FluxChambreExtractor.BUNDLE_SOURCE_PATH, this.localizationManager.getMessage(
                            FluxChambreExtractor.BUNDLE_SOURCE_PATH,
                            FluxChambreExtractor.MSG_EXTRACTION_ABORTED))), Notification.ERROR,
                    FluxChambreExtractor.PROPERTY_MSG_BADS_RIGHTS, (Utilisateur) policyManager.getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    FluxChambreExtractor.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
    }

    @Override
    public void setExtraction(Extraction extraction) {
       
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
         Long size = -1L;
        size = +chambreExtractor.getExtractionSize(parameters);
        return size;
    }

    
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setNotificationDAO(INotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    /**
     *
     * @param chambreExtractor
     */
    public void setChambreExtractor(IExtractor chambreExtractor) {
        this.chambreExtractor = chambreExtractor;
    }
    
    
    
}
