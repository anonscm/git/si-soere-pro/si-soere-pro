package org.inra.ecoinfo.pro.refdata.nomregionalsol;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.pro.refdata.region.Region_;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique_;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis_;

public class NomRegionalSolDaoImpl extends AbstractJPADAO<Nomregionalsol> implements INomRegionalSolDAO {

    @Override
    public List<Nomregionalsol> getAll() {
        return getAll(Nomregionalsol.class);
    }

    @Override
    public Optional<Nomregionalsol> getByNKey(Typesolarvalis typesolarvalis, Typepedologique typepedologique,Region region,String nom) {
        CriteriaQuery<Nomregionalsol> query = builder.createQuery(Nomregionalsol.class);
        Root<Nomregionalsol> nomregionalsol = query.from(Nomregionalsol.class);
        Join<Nomregionalsol, Typepedologique> tp = nomregionalsol.join(Nomregionalsol_.typepedologique);
        Join<Nomregionalsol, Typesolarvalis> ts = nomregionalsol.join(Nomregionalsol_.typesolarvalis);
        Join<Nomregionalsol, Region> rg = nomregionalsol.join(Nomregionalsol_.region);
        query
                .select(nomregionalsol)
                .where(
                        builder.equal(tp, typepedologique),
                        builder.equal(ts, typesolarvalis),
                        builder.equal(rg, region),
                        builder.equal(nomregionalsol.get(Nomregionalsol_.nom), nom)
                );
        return getOptional(query);
    }

    @Override
    public Optional<Nomregionalsol> getByNKey(String nomTypesolarvalis, String nomTypepedologique, String nomregion,String nom) {
        CriteriaQuery<Nomregionalsol> query = builder.createQuery(Nomregionalsol.class);
        Root<Nomregionalsol> nomregionalsol = query.from(Nomregionalsol.class);
        Join<Nomregionalsol, Typepedologique> tp = nomregionalsol.join(Nomregionalsol_.typepedologique);
        Join<Nomregionalsol, Typesolarvalis> ts = nomregionalsol.join(Nomregionalsol_.typesolarvalis);
        Join<Nomregionalsol, Region> rg = nomregionalsol.join(Nomregionalsol_.region);
        query
                .select(nomregionalsol)
                .where(
                        builder.equal(tp.get(Typepedologique_.nom), nomTypepedologique),
                        builder.equal(ts.get(Typesolarvalis_.nom), nomTypesolarvalis),
                        builder.equal(rg.get(Region_.nom), nomregion),
                        builder.equal(nomregionalsol.get(Nomregionalsol_.nom), nom)
                );
        return getOptional(query);
    }

  
}
