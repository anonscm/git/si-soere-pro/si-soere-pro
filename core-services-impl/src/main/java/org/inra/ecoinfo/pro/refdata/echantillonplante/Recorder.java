/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonplante;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.lavage.ILavageDAO;
import org.inra.ecoinfo.pro.refdata.lavage.Lavage;
import org.inra.ecoinfo.pro.refdata.prelevementplante.IPrelevementPlanteDAO;
import org.inra.ecoinfo.pro.refdata.prelevementplante.PrelevementPlante;
import org.inra.ecoinfo.pro.refdata.typeechantillon.ITypeEchantillon;
import org.inra.ecoinfo.pro.refdata.typeechantillon.TypeEchantillon;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<EchantillonPlante> {

    private static final String PROPERTY_MSG_ECHANTILLOSOL_BAD_PRELEVEMNTPLANTE = "PROPERTY_MSG_ECHANTILLOSOL_BAD_PRELEVEMNTPLANTE";
    private static final String PROPERTY_MSG_TYPEECHANTILLON_BAD_PRELEVEMNTPLANTE = "PROPERTY_MSG_TYPEECHANTILLON_BAD_PRELEVEMNTPLANTE";
    private static final String PROPERTY_MSG_LAVAGE_BAD_PRELEVEMNT_PLANTE = "PROPERTY_MSG_LAVAGE_BAD_PRELEVEMNT_PLANTE";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    IEchantillonPlanteDAO echantillonPlanteDAO;
    IPrelevementPlanteDAO prelevementPlanteDAO;
    ILavageDAO lavageDAO;
    ITypeEchantillon typeechantillonDAO;
    protected IBooleanDAO boolDAO;
    Properties commentaireEn;

    private String[] listeBooleansPossible;
    private String[] listePrelevementPossible;
    private String[] listeTypeEchantillonPossible;
    private String[] listeLavagePossible;

    private void createEnchantillonPlante(EchantillonPlante plante) throws BusinessException {
        try {
            echantillonPlanteDAO.saveOrUpdate(plante);
            echantillonPlanteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create echantillonPlante");
        }
    }

    private void updateEchantillonPlante(EchantillonPlante dbPlante, TypeEchantillon typeEchantillon,
            String codelabo, String homogeneisation, Lavage lavage, float sechage, float broyage, float tempconseravant,
            String caanalyse, float tcapresa, float masseavantenvoie,String commentaire) throws BusinessException {
        try {
            dbPlante.setMasseavantenvoie(masseavantenvoie);
            dbPlante.setTypeEchantillon(typeEchantillon);
            dbPlante.setCodelabo(codelabo);
            dbPlante.setHomogeneisation(homogeneisation);
            dbPlante.setLavage(lavage);
            dbPlante.setSechage(sechage);
            dbPlante.setBroyage(broyage);
            dbPlante.setTempconseravant(tempconseravant);
            dbPlante.setConserapresanalyse(caanalyse);
            dbPlante.setTempconserapres(tcapresa);
            dbPlante.setCommentaire(commentaire);
            echantillonPlanteDAO.saveOrUpdate(dbPlante);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update echantillonPlante");
        }

    }

    private void createOrUpdateEchantillonPlante(EchantillonPlante dbPlante, PrelevementPlante prelevementplante, long numeroechantillon, String codeplante, TypeEchantillon typeEchantillon,
            String codelabo, String homogeneisation, Lavage lavage, float sechage, float broyage, float tempconseravant,
            String caanalyse, float tcapresa, float masseavantenvoie, String commentaire)
            throws BusinessException {
        if (dbPlante == null) {
            EchantillonPlante plante = new EchantillonPlante(prelevementplante, numeroechantillon, codeplante, typeEchantillon,
                    codelabo, homogeneisation, lavage, sechage, broyage, tempconseravant,
                    caanalyse, tcapresa, masseavantenvoie, commentaire);
            createEnchantillonPlante(plante);
        } else {
            updateEchantillonPlante(dbPlante, typeEchantillon,
                    codelabo, homogeneisation, lavage, sechage, broyage, tempconseravant,
                    caanalyse, tcapresa, masseavantenvoie, commentaire);
        }
    }

    private void persistEchantillonPlante(PrelevementPlante prelevementplante, long numeroechantillon, String codeplante, TypeEchantillon typeEchantillon,
            String codelabo, String homogeneisation, Lavage lavage, float sechage, float broyage, float tempconseravant,
            String caanalyse, float tcapresa, float masseavantenvoie, String commentaire) throws BusinessException, BusinessException {
        EchantillonPlante dbPlante = echantillonPlanteDAO.getByNKey(prelevementplante, numeroechantillon).orElse(null);
        createOrUpdateEchantillonPlante(dbPlante, prelevementplante, numeroechantillon, codeplante, typeEchantillon,
                codelabo, homogeneisation, lavage, sechage, broyage, tempconseravant,
                caanalyse, tcapresa, masseavantenvoie, commentaire);
    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    private void listeTypeEchantillonPossibles() {
        List<TypeEchantillon> groupescode = typeechantillonDAO.getAll();
        String[] listypePossibles = new String[groupescode.size() + 1];
        listypePossibles[0] = "";
        int index = 1;
        for (TypeEchantillon codeprel : groupescode) {
            listypePossibles[index++] = codeprel.getTe_nom();
        }
        this.listeTypeEchantillonPossible = listypePossibles;
    }

    private void listeLavagePossibles() {
        List<Lavage> groupelavage = lavageDAO.getAll();
        String[] listlavagePossibles = new String[groupelavage.size() + 1];
        listlavagePossibles[0] = "";
        int index = 1;
        for (Lavage lavage : groupelavage) {
            listlavagePossibles[index++] = lavage.getNom();
        }
        this.listeLavagePossible = listlavagePossibles;
    }

    private void listePlantePossibles() {
        List<PrelevementPlante> groupeplante = prelevementPlanteDAO.getAll();
        String[] listplantesPossibles = new String[groupeplante.size() + 1];
        listplantesPossibles[0] = "";
        int index = 1;
        for (PrelevementPlante plante : groupeplante) {
            listplantesPossibles[index++] = plante.getCodeplante();
        }
        this.listePrelevementPossible = listplantesPossibles;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                String codePrelevement = values[0];
                Long noOrdre = Long.parseLong(values[1]);
                PrelevementPlante prelevement = prelevementPlanteDAO.getByNKey(codePrelevement).orElse(null);
                echantillonPlanteDAO.remove(echantillonPlanteDAO.getByNKey(prelevement, noOrdre)
                        .orElseThrow(() -> new BusinessException("can't find echantillonPlante")));
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            long line = 0;
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EchantillonPlante.JPA_NAME_ENTITY);
                int indexsol = tokenizerValues.currentTokenIndex();
                String codeplante = tokenizerValues.nextToken();
                long numeroechantillon = verifieLong(tokenizerValues, line, true, errorsReport);
                final String codeechan = tokenizerValues.nextToken(); //code interne labo
                int indextype = tokenizerValues.currentTokenIndex();
                final String type = tokenizerValues.nextToken();
                final String codelabo = tokenizerValues.nextToken(); //code interne labo
                final String homogeneisation = tokenizerValues.nextToken();
                int indexlavage = tokenizerValues.currentTokenIndex();
                final String lavage = tokenizerValues.nextToken();
                float sechage = verifieFloat(tokenizerValues, line, false, errorsReport);
                float broyage = verifieFloat(tokenizerValues, line, false, errorsReport);
                float tempconseravant = verifieFloat(tokenizerValues, line, false, errorsReport);
                final String conseranalyse = tokenizerValues.nextToken();
                float tcapresa = verifieFloat(tokenizerValues, line, false, errorsReport);
                float masseAvantEnvoi = verifieFloat(tokenizerValues, line, false, errorsReport);
                final String commentaire = tokenizerValues.nextToken();

                PrelevementPlante prelevementplante = prelevementPlanteDAO.getByNKey(codeplante).orElse(null);
                if (prelevementplante == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ECHANTILLOSOL_BAD_PRELEVEMNTPLANTE), line, indexsol, codeplante));
                }

                TypeEchantillon typeEchantillon = typeechantillonDAO.getByNkey(type).orElse(null);
                if (typeEchantillon == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_TYPEECHANTILLON_BAD_PRELEVEMNTPLANTE), line, indextype, type));
                }

                Lavage dblavage = lavageDAO.getByName(lavage).orElse(null);
                if (dblavage == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_LAVAGE_BAD_PRELEVEMNT_PLANTE), line, indexlavage, lavage));
                }

                String codeeplante = codeEchantillonBase(numeroechantillon, codeplante);
                final String codemy = Utils.createCodeFromString(codeeplante);

                if (!errorsReport.hasErrors()) {
                    persistEchantillonPlante(prelevementplante, numeroechantillon, codeplante, typeEchantillon,
                            codelabo, homogeneisation, dblavage, sechage, broyage, tempconseravant,
                            conseranalyse, tcapresa, masseAvantEnvoi, commentaire);
                }
                values = csvp.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private String codeEchantillonBase(long ordre, String codesol) {
        String home = "a_b";
        String hom = home.substring(1, 2);
        String keys = codesol + hom + ordre;
        return keys;
    }

    @Override
    protected List<EchantillonPlante> getAllElements() throws BusinessException {
        return echantillonPlanteDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EchantillonPlante plante) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getPrelevementplante().getCodeplante() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getPrelevementplante().getCodeplante() != null
                        ? plante.getPrelevementplante().getCodeplante() : "",
                        listePrelevementPossible, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getNumeroechantillon() == 0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getNumeroechantillon(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCodeeplante() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getCodeeplante(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getTypeEchantillon().getTe_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getTypeEchantillon().getTe_nom() != null
                        ? plante.getTypeEchantillon().getTe_nom() : "",
                        listeTypeEchantillonPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCodelabo() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getCodelabo(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getHomogeneisation() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getHomogeneisation() != null
                        ? plante.getHomogeneisation() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getLavage().getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getLavage().getNom() != null
                        ? plante.getLavage().getNom() : "",
                        listeLavagePossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getSechage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getSechage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getBroyage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getBroyage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getTempconseravant() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getTempconseravant(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getTempconserapres() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getConserapresanalyse() != null
                        ? plante.getConserapresanalyse() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getTempconserapres() == 0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getTempconserapres(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(plante.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    public void setEchantillonPlanteDAO(IEchantillonPlanteDAO echantillonPlanteDAO) {
        this.echantillonPlanteDAO = echantillonPlanteDAO;
    }

    public void setPrelevementPlanteDAO(IPrelevementPlanteDAO prelevementPlanteDAO) {
        this.prelevementPlanteDAO = prelevementPlanteDAO;
    }

    public void setLavageDAO(ILavageDAO lavageDAO) {
        this.lavageDAO = lavageDAO;
    }

    public void setTypeechantillonDAO(ITypeEchantillon typeechantillonDAO) {
        this.typeechantillonDAO = typeechantillonDAO;
    }

    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    @Override
    protected ModelGridMetadata<EchantillonPlante> initModelGridMetadata() {
        listeBooleanPossibles();
        listeTypeEchantillonPossibles();
        listeLavagePossibles();
        listePlantePossibles();
        commentaireEn = localizationManager.newProperties(EchantillonPlante.JPA_NAME_ENTITY, EchantillonPlante.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}
