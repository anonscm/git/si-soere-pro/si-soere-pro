/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import javax.persistence.Transient;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author adiankha
 */
public class DeleteRecord implements IDeleteRecord{
     static final long                             serialVersionUID = 1L;
     static private final Logger                          LOGGER           = LoggerFactory.getLogger(RecorderPRO.class.getName());

      @Transient
     private IVersionFileDAO                       versionFileDAO;

    
     @Transient
     private ILocalPublicationDAO                  publicationDAO;
     
   
     protected IDatasetConfiguration               datasetConfiguration;

    @Override
    public void deleteRecord(VersionFile versionFile) throws BusinessException{
        VersionFile finalVersionFile = versionFile;
         try {
             finalVersionFile = this.versionFileDAO.merge(finalVersionFile);
             this.publicationDAO.removeVersion(finalVersionFile);
         } catch (final PersistenceException e) {
              LOGGER.debug(e.getMessage(), e);
             throw new BusinessException(e.getMessage(), e);
         }
    }

    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    public void setPublicationDAO(ILocalPublicationDAO publicationDAO) {
        this.publicationDAO = publicationDAO;
    }

    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    public IVersionFileDAO getVersionFileDAO() {
        return versionFileDAO;
    }

    public ILocalPublicationDAO getPublicationDAO() {
        return publicationDAO;
    }


    
}
