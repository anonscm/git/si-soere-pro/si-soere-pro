/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import java.util.Collection;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class ITKExtractor extends MO implements IExtractor {

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults" ;

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_RECOLTECOUPE_CODE = "extractionResultRecolteCoupe";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_TRAVAILSOL_CODE = "extractionResultTravailSol";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_APPORT_CODE = "extractionResultApport" ;

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_SEMISPLANTATION_CODE = "extractionResultSemisPlantation" ;

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_PROETUDIE_CODE = "extractionResultProetudie" ;

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.listesitineraires.messages";

    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";

    IExtractor recolteCoupeExtractor;
    IExtractor travailSolExtractor;
    IExtractor semisPlantationExtractor;
    IExtractor proEtudieExtractor;
    IExtractor apportExtractor;

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        int extractionResult = 5;
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(ITKParameterVO.RECOLTECOUPE)))
                    .isEmpty()) {
                this.recolteCoupeExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(ITKParameterVO.TRAVAILSOL)))
                    .isEmpty()) {
                this.travailSolExtractor.extract(parameters);
            } else {
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
       
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(ITKParameterVO.SEMISPLANTATION)))
                    .isEmpty()) {
                this.semisPlantationExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(ITKParameterVO.APPORT)))
                    .isEmpty()) {
                this.apportExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        }
        try {
            if (!((Collection<VariablesPRO>) ((DefaultParameter) parameters).getParameters().get(
                    VariablesPRO.class.getSimpleName().concat(ITKParameterVO.PROETUDIE)))
                    .isEmpty()) {
                this.proEtudieExtractor.extract(parameters);
            }
        } catch (final BusinessException e) {
            if (e.getCause().getClass().equals(NoExtractionResultException.class)) {
                extractionResult--;
            } else {
                throw e;
            }
        } 
        if (extractionResult == 0) {
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    ITKExtractor.BUNDLE_SOURCE_PATH, this.localizationManager.getMessage(
                            ITKExtractor.BUNDLE_SOURCE_PATH,
                            ITKExtractor.MSG_EXTRACTION_ABORTED))), Notification.ERROR,
                    ITKExtractor.PROPERTY_MSG_BADS_RIGHTS, (Utilisateur) this.policyManager.getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    ITKExtractor.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
    }

    @Override
    public void setExtraction(Extraction extraction) {

    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = -1L;
        size = +recolteCoupeExtractor.getExtractionSize(parameters);
        size = +travailSolExtractor.getExtractionSize(parameters);
        size = +apportExtractor.getExtractionSize(parameters);
        size = +semisPlantationExtractor.getExtractionSize(parameters);
        size = +proEtudieExtractor.getExtractionSize(parameters);
        return size;
    }

    /**
     *
     * @param recolteCoupeExtractor
     */
    public void setRecolteCoupeExtractor(IExtractor recolteCoupeExtractor) {
        this.recolteCoupeExtractor = recolteCoupeExtractor;
    }

    /**
     *
     * @param travailSolExtractor
     */
    public void setTravailSolExtractor(IExtractor travailSolExtractor) {
        this.travailSolExtractor = travailSolExtractor;
    }

    /**
     *
     * @param semisPlantationExtractor
     */
    public void setSemisPlantationExtractor(IExtractor semisPlantationExtractor) {
        this.semisPlantationExtractor = semisPlantationExtractor;
    }

    /**
     *
     * @param proEtudieExtractor
     */
    public void setProEtudieExtractor(IExtractor proEtudieExtractor) {
        this.proEtudieExtractor = proEtudieExtractor;
    }

    /**
     *
     * @param apportExtractor
     */
    public void setApportExtractor(IExtractor apportExtractor) {
        this.apportExtractor = apportExtractor;
    }

}
