/**
 *
 */
package org.inra.ecoinfo.pro.refdata.modalite;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;

/**
 * @author sophie
 *
 */
public interface IModaliteDAO extends IDAO<Modalite> {

    List<Modalite> getAll();

    Optional<Modalite> getByValeurFacteur(String valeur, Facteur facteur);

    List<Modalite> getLstModByFacteur(Facteur facteur);
}
