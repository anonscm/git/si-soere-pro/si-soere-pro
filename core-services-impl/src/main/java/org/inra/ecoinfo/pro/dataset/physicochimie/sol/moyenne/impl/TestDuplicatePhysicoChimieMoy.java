/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;

/**
 *
 * @author vjkoyao
 */
public class TestDuplicatePhysicoChimieMoy extends AbstractTestDuplicate {
    protected static final String BUNDLE_SOURCE_PATH_MOY_SOL = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.messages";
    static final String PROPERTY_MSG_DUPLICATE_LINE_SOLMOY = "PROPERTY_MSG_DUPLICATE_LINE_PROMOY";

    SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine = null;
    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    /**
     *
     */
    public TestDuplicatePhysicoChimieMoy() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    /**
     *
     * @param date
     * @param codeTrait
     * @param couche
     * @param linf
     * @param sup
     * @param codevariable
     * @param methode
     * @param lineNumber
     */
    protected void addLine(final String date, String codeTrait, final String couche,String linf,String sup,
            final String codevariable, String methode, final long lineNumber) {
        final String key = this.getKey(date, codeTrait, couche, linf,sup,codevariable, methode);
        final String keySol = this.getKey(date, codeTrait,linf,sup, codevariable, methode);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, couche);
            } else if (!this.lineCouvert.get(keySol).equals(couche)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicatePhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                        TestDuplicatePhysicoChimieMoy.PROPERTY_MSG_DUPLICATE_LINE_SOLMOY), lineNumber,
                        date, codeTrait, codevariable, methode, this.lineCouvert.get(keySol), codeTrait));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicatePhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    TestDuplicatePhysicoChimieMoy.PROPERTY_MSG_DUPLICATE_LINE_SOLMOY), lineNumber, date, codeTrait, couche,linf,sup, codevariable,
                    methode, this.line.get(key)));

        }
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[3],values[4],values[5], values[7], values[11], lineNumber);
    }

    
}
