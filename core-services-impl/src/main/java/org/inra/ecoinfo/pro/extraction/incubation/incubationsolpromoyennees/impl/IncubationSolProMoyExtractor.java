/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolpromoyennees.impl;

import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationExtractor;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationParameterVO;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolProMoyExtractor extends AbstractIncubationExtractor<MesureIncubationSolProMoy> {

    /**
     *
     */
    public static final String INCUBATION_SOL_PRO_MOY = "incubation_sol_pro_moy";

    /**
     *
     */
    protected static final String MAP_INDEX_INCUBATION_SOL_PRO_MOY = "incubationsolpromoy";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return INCUBATION_SOL_PRO_MOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IncubationParameterVO.INCUBATIONSOLPROMOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_INCUBATION_SOL_PRO_MOY;
    }
}
