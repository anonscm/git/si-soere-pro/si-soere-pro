package org.inra.ecoinfo.pro.refdata.typepartenaire;
/**
 *
 */

/*public class Recorder extends AbstractCSVMetadataRecorder<TypePartenaire> {
	
	protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ITypePartenaireDAO typePartenaireDAO;
    */
    
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    /*@Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String libelleTypePartenaire = tokenizerValues.nextToken();

                TypePartenaire typePartenaire = typePartenaireDAO.getByLibelle(libelleTypePartenaire);

                typePartenaireDAO.remove(typePartenaire);
                
                values = parser.getLine();
            }
        } 
        catch (Exception e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    */

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    /*@Override
    protected List<TypePartenaire> getAllElements() throws BusinessException {
        return typePartenaireDAO.getAll(TypePartenaire.class);
    }
    */

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    /*@Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypePartenaire typePartenaire) throws BusinessException 
    {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesNameLibelle = localizationManager.newProperties(TypePartenaire.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_TPPAR, Locale.ENGLISH);

        String localizedChampNameLibelle = "";

        if (typePartenaire != null) 
        {
            localizedChampNameLibelle = propertiesNameLibelle.containsKey(typePartenaire.getLibelle()) ? propertiesNameLibelle.getProperty(typePartenaire.getLibelle()) : typePartenaire.getLibelle();
        }
        
        //Libellé du type de partenaire
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePartenaire == null ? Constantes.STRING_EMPTY : typePartenaire.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typePartenaire == null ? Constantes.STRING_EMPTY : localizedChampNameLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }
    */

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    /*@Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
    	ErrorsReport errorsReport = new ErrorsReport();
    	long line = 0;
    	
        try 
        {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) 
            {	
            	line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, TypePartenaire.TABLE_NAME);
                
                String libelleTypePartenaire = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                if(libelleTypePartenaire == null || libelleTypePartenaire.isEmpty())
                {
                	errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"),line+1, indexLibelle-1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                TypePartenaire typePartenaire = new TypePartenaire(libelleTypePartenaire);
                TypePartenaire dbTypePartenaire = typePartenaireDAO.getByLibelle(libelleTypePartenaire);
                
                if(!errorsReport.hasErrors())
                {
                	if (dbTypePartenaire == null) 
                	{
                		typePartenaireDAO.saveOrUpdate(typePartenaire);
                	} 
                	else 
                	{
                		dbTypePartenaire.setLibelle(libelleTypePartenaire);
                		typePartenaireDAO.saveOrUpdate(dbTypePartenaire);
                	}
                }
                
                values = parser.getLine();
            }
            
            RefDataUtil.gestionErreurs(errorsReport);
        } 
        catch (IOException e1) 
        {
            throw new BusinessException(e1.getMessage(), e1);
        } 
        catch (PersistenceException e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    */

	/**
	 * @param typePartenaireDAO the typePartenaireDAO to set
	 */
	/*public void setTypePartenaireDAO(ITypePartenaireDAO typePartenaireDAO) {
		this.typePartenaireDAO = typePartenaireDAO;
	}
    
    
}
*/