/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.statutvaleur;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAStatutValeurDAO extends AbstractJPADAO<StatutValeur> implements IStatutValeurDAO{

    /**
     *
     * @return
     */
    @Override
    public List<StatutValeur> getAll()  {
        return getAll(StatutValeur.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<StatutValeur> getByNKey(String nom) {
        CriteriaQuery<StatutValeur> query = builder.createQuery(StatutValeur.class);
        Root<StatutValeur> statutValeur = query.from(StatutValeur.class);
        query
                .select(statutValeur)
                .where(
                        builder.equal(statutValeur.get(StatutValeur_.sv_mycode), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }
    
}
