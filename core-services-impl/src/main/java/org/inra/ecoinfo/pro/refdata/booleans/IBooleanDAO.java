package org.inra.ecoinfo.pro.refdata.booleans;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IBooleanDAO extends IDAO<Booleans> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<Booleans> getByNKey(String libelle);

    /**
     *
     * @return
     */
    List<Booleans> getAll();

}
