/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.moyenne.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteMoyenneDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieOutputDisplayByRow;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class PlanteMoyenneBuildOutputDisplayByRow extends PhysicoChimieOutputDisplayByRow {

    static final String PATTERN_WORD = "%s";
    static final String PATTERB_CSV_12_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%%s;%%s;%%s;%%s;%%s;%%s";
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA_PLANTEMOYENNE = "PROPERTY_HEADER_RAW_DATA_PLANTEMOYENNE";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.plante.messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedPlanteMoyenneVariables = getVariablesSelected(requestMetadatasMap, IPhysicoChimiePlanteMoyenneDatatypeManager.CODE_DATATYPE_PLANTE_MOYENNE);
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<MesurePlanteMoyenne> mesuresPlanteMoyenne = getMesures(resultsDatasMap, PlanteMoyenneExtractor.MAP_INDEX_PLANTEMOYENNE);
        final Set<String> dispositifsNames = buildListOfDispositifsForDatatype(selectedDispositifs, getDatatype());
        final Map<String, File> filesMap = this.buildOutputsFiles(dispositifsNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>>> mesuresPlanteMoyennesMap = new TreeMap();

        try {
            this.buildmap(mesuresPlanteMoyenne, mesuresPlanteMoyennesMap);
        } catch (DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedDispositifs, selectedPlanteMoyenneVariables, selectedIntervalDate,
                outputPrintStreamMap, mesuresPlanteMoyennesMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void buildmap(
            final List<MesurePlanteMoyenne> mesuresPlanteMoyennes,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>>> mesuresPlanteMoyenneMap) {
        java.util.Iterator<MesurePlanteMoyenne> itMesure = mesuresPlanteMoyennes
                .iterator();
        while (itMesure.hasNext()) {
            MesurePlanteMoyenne mesurePlanteMoyenne = itMesure
                    .next();
            DescriptionTraitement traitement = mesurePlanteMoyenne.getDescriptionTraitement();
            Long siteId = traitement.getDispositif().getId();
            if (mesuresPlanteMoyenneMap.get(siteId) == null) {
                mesuresPlanteMoyenneMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresPlanteMoyenneMap.get(siteId).get(traitement) == null) {
                mesuresPlanteMoyenneMap.get(siteId).put(traitement,
                        new TreeMap<>());
            }
            mesuresPlanteMoyenneMap.get(siteId).get(traitement).put(mesurePlanteMoyenne.getDatePrelevement(), mesurePlanteMoyenne);
            itMesure.remove();
        }
    }

    private void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitBrutVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>>> mesuresPlanteMoyennesMap) {
        try {
            BuildDataLine(selectedDispositifs, selectedProduitBrutVariables, outputPrintStreamMap, mesuresPlanteMoyennesMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void BuildDataLine(final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitBrutVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>>> mesuresPlanteMoyennesMap,
            final IntervalDate selectedIntervalDate) {
        PrintStream out;
        String currentDispositif;
        String currentLieu;
        String currentCulture;
        String ppreleve;
        String codeTraitement;
        int currentRep;
        double coupe;
        String currentlabo;
        for (final Dispositif dispositif : selectedDispositifs) {
            out = outputPrintStreamMap.get(getDispositifDatatype(dispositif, getDatatype()));
            final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>> mesurePlanteMoyennesMapByDisp = mesuresPlanteMoyennesMap
                    .get(dispositif.getId());
            if (mesurePlanteMoyennesMapByDisp == null) {
                continue;
            }
            Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>>> itEchan = mesurePlanteMoyennesMapByDisp
                    .entrySet().iterator();
            while (itEchan.hasNext()) {
                java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesurePlanteMoyenne>> echanEntry = itEchan
                        .next();
                DescriptionTraitement traitement = echanEntry.getKey();
                SortedMap<LocalDate, MesurePlanteMoyenne> mesurePlanteMoyennesMap = echanEntry.getValue()
                        .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                for (Entry<LocalDate, MesurePlanteMoyenne> entrySet : mesurePlanteMoyennesMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesurePlanteMoyenne mesurePlanteMoyenne = entrySet.getValue();
                    currentDispositif = traitement.getDispositif().getName();
                    currentLieu = traitement.getDispositif().getLieu().getNom() != null ? traitement.getDispositif().getLieu().getNom()
                            : org.apache.commons.lang.StringUtils.EMPTY;
                    currentCulture = mesurePlanteMoyenne.getCultures().getNom();
                    ppreleve = mesurePlanteMoyenne.getPartie().getNom();
                    codeTraitement = mesurePlanteMoyenne.getDescriptionTraitement().getCode();
                    coupe = mesurePlanteMoyenne.getHauteurcoup();
                    currentRep = mesurePlanteMoyenne.getRepetition();
                    currentlabo = String.format(
                            PlanteMoyenneBuildOutputDisplayByRow.PATTERN_WORD,
                            mesurePlanteMoyenne.getNomlabo());

                    String genericPattern = LineDataFixe(date, currentDispositif, currentLieu, currentCulture, ppreleve, codeTraitement, currentRep, coupe, currentlabo);
                    LineDataVariable(mesurePlanteMoyenne, genericPattern, out, selectedProduitBrutVariables);
                }
                itEchan.remove();
            }
        }
    }

    private void LineDataVariable(MesurePlanteMoyenne mesureProduitBrut, String genericPattern, PrintStream out, final List<VariablesPRO> selectedProduitBrutVariables) {
        selectedProduitBrutVariables.stream().forEach((variablesPRO) -> {
            for (Iterator<ValeurPlanteMoyenne> ValeurIterator = mesureProduitBrut.getValeurPlanteMoyennes().iterator(); ValeurIterator.hasNext();) {
                ValeurPlanteMoyenne valeur = ValeurIterator.next();
                if (((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getId().equals(variablesPRO.getId())) {
                    String line = String.format(genericPattern,
                            valeur.getValeur(),
                            valeur.getEcarttype(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());
                    out.println(line);
                    ValeurIterator.remove();
                }
            }
        });
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentLieu,
            String currentCulture, String ppreleve, String codeTraitement,
            int currentRep, double coupe,
            String currentlabo) {
        String genericPattern = String.format(PATTERB_CSV_12_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentLieu, currentCulture, ppreleve, codeTraitement,
                currentRep, coupe,
                currentlabo);
        return genericPattern;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                PlanteMoyenneBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        PlanteMoyenneBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        PlanteMoyenneBuildOutputDisplayByRow.HEADER_RAW_DATA_PLANTEMOYENNE));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (CollectionUtils.isEmpty(((DefaultParameter) parameters).getResults()
                .get(PlanteMoyenneExtractor.CST_RESULT_EXTRACTION_PLANTEMOYENNE_CODE)
                .getOrDefault(PlanteMoyenneExtractor.MAP_INDEX_PLANTEMOYENNE,
                        ((DefaultParameter) parameters).getResults()
                .get(PlanteMoyenneExtractor.CST_RESULT_EXTRACTION_PLANTEMOYENNE_CODE)
                .get(PlanteMoyenneExtractor.MAP_INDEX_0)))) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, PlanteMoyenneExtractor.CST_RESULT_EXTRACTION_PLANTEMOYENNE_CODE));
        return null;
    }

    private String getDatatype() {
        return "plante_physico-chimie_moyennes";
    }

}
