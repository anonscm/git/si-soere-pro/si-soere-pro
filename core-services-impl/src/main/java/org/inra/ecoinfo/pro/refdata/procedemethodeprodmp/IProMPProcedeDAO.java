package org.inra.ecoinfo.pro.refdata.procedemethodeprodmp;


import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.process.Process;

/**
 *
 * @author ptcherniati
 */
public interface IProMPProcedeDAO extends IDAO<ProcedeMethodeProdMp> {

    /**
     *
     * @return
     */
    List<ProcedeMethodeProdMp> getAll();

    /**
     *
     * @param melange
     * @param process
     * @param ordre
     * @return
     */
    Optional<ProcedeMethodeProdMp> getByNKey(Melange melange, Process process, int ordre);

    /**
     *
     * @param codeProduit
     * @param NomMatierePremiere
     * @param pourcentage
     * @param intituleProcede
     * @param ordreProcede
     * @return
     */
    Optional<ProcedeMethodeProdMp> getByNKey(String codeProduit, String NomMatierePremiere, double pourcentage, String intituleProcede, double ordreProcede);

}
