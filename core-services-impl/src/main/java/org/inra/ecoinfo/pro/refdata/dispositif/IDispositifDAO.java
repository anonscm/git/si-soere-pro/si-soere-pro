/**
 *
 */
package org.inra.ecoinfo.pro.refdata.dispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;

/**
 * @author sophie
 *
 */
public interface IDispositifDAO extends IDAO<Dispositif> {

    /**
     *
     * @return
     */
    List<Dispositif> getAll();

    /**
     *
     * @param code
     * @param lieu
     * @return
     */
    Optional<Dispositif> getByNKey(String code, Lieu lieu);

    /**
     *
     * @param code
     * @param nomLieu
     * @return
     */
    Optional<Dispositif> getByNKey(String code, String nomLieu);

    /**
     *
     * @param geolocalisation
     * @return
     */
    Optional<Dispositif> getByGeolocalisation(Geolocalisation geolocalisation);

}
