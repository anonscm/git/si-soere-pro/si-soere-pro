/*
 *
 */
package org.inra.ecoinfo.pro.refdata.nomenclature;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;
import org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO;
import org.inra.ecoinfo.pro.refdata.filiere.Filiere;
import org.inra.ecoinfo.pro.refdata.filiere.IFiliereDAO;
import org.inra.ecoinfo.pro.refdata.precisionfiliere.IPrecisionFiliereDAO;
import org.inra.ecoinfo.pro.refdata.precisionfiliere.Precisionfiliere;
import org.inra.ecoinfo.pro.refdata.precisionpro.IPrecisionProDAO;
import org.inra.ecoinfo.pro.refdata.precisionpro.Precisionpro;
import org.inra.ecoinfo.pro.refdata.typenomenclature.ITypeNomenclatureDAO;
import org.inra.ecoinfo.pro.refdata.typenomenclature.TypeNomenclature;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Nomenclatures> {
    
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_TYPENOMEN = "PROPERTY_MSG_PRODUIT_BAD_NOMEN";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_FILIERE = "PROPERTY_MSG_NOMENCLATURE_BAD_FILIERE";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONFILIERE = "PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONFILIERE";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONPRO = "PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONPRO";
    private static final String PROPERTY_MSG_FACTEUR_NONDEFINI = "FACTEUR_NONDEFINI";
    
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    
    /**
     *
     */
    protected INomenclatureDAO nomenDAO;
    
    /**
     *
     */
    protected ITypeNomenclatureDAO typenomenclatureDAO;
    
    IFiliereDAO filiereDAO;
    IPrecisionFiliereDAO precisionfiliereDAO;
    IPrecisionProDAO precisionproDAO;
    
    IFacteurDAO facteurDAO;
    
    private String[] listeTypeNomenclaturePossibles;
    private String[] listeFilierePossibles;
    private String[] listePrecisionProPossibles;
    private String[] listePrecisionFilierePossibles;
    private String[] listeFacteurPossibles;
    
    private Properties nomenNomEN;
    
    private Properties commenEN;
    
    private void createNomenclatures(final Nomenclatures nomenclatures) throws BusinessException {
        try {
            nomenDAO.saveOrUpdate(nomenclatures);
            nomenDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create nomenclatures");
        }
    }
    
    private void updateBDNomenclatures(String code, String mycode, String nom, String comment, String etat,
            TypeNomenclature tnom, Filiere filiere, Precisionfiliere pfiliere, String ppro,
            final Nomenclatures dbnomenclatures, Facteur facteur,
            String provenance, String affichage) throws BusinessException {
        try {
            dbnomenclatures.setNomen_code(code);
            dbnomenclatures.setNomen_nom(nom);
            dbnomenclatures.setNomen_mycode(mycode);
            dbnomenclatures.setTypenomenclature(tnom);
            dbnomenclatures.setFiliere(filiere);
            dbnomenclatures.setEtatcoproduit(etat);
            dbnomenclatures.setNomen_comment(comment);
            dbnomenclatures.setPrecisionfiliere(pfiliere);
            dbnomenclatures.setPrecisionpro(ppro);
            dbnomenclatures.setFacteur(facteur);
            dbnomenclatures.setIntitule_affichage(affichage);
            dbnomenclatures.setProvenance(provenance);
            nomenDAO.saveOrUpdate(dbnomenclatures);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update nomenclatures");
        }
    }
    
    private void createOrUpdateNomenclatures(String code, String mycode, String nom, String comment, String etat,
            TypeNomenclature tnom, Filiere filiere, Precisionfiliere pfiliere, String ppro,
            final Nomenclatures dbnomenclatures, Facteur facteur, String provenance,
            String intitule_affichage) throws BusinessException {
        if (dbnomenclatures == null) {
            Nomenclatures nomenclatures = new Nomenclatures(mycode, mycode, provenance, comment, etat, tnom, filiere, pfiliere, ppro, facteur, provenance, intitule_affichage);
            nomenclatures.setNomen_code(code);
            nomenclatures.setNomen_mycode(mycode);
            nomenclatures.setTypenomenclature(tnom);
            nomenclatures.setNomen_comment(comment);
            nomenclatures.setNomen_nom(nom);
            nomenclatures.setEtatcoproduit(etat);
            nomenclatures.setFiliere(filiere);
            nomenclatures.setPrecisionfiliere(pfiliere);
            nomenclatures.setPrecisionpro(ppro);
            nomenclatures.setFacteur(facteur);
            nomenclatures.setIntitule_affichage(intitule_affichage);
            nomenclatures.setProvenance(provenance);
            createNomenclatures(nomenclatures);
        } else {
            updateBDNomenclatures(code, mycode, nom, comment, etat, tnom, filiere, pfiliere, ppro, dbnomenclatures, facteur, provenance, intitule_affichage);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TypeNomenclature tnomenclature = null;
                Nomenclatures nomenclature = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String libelle = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String typenoment = tokenizerValues.nextToken();
                nomenclature = nomenDAO.getByNKey(libelle, typenoment)
                        .orElseThrow(() -> new BusinessException("can(t find nomenclature"));
                nomenDAO.remove(nomenclature);
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<Nomenclatures> getAllElements() throws BusinessException {
        return nomenDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Nomenclatures nomenclatures) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_code() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getNomen_code(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false,
                        true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getNomen_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false,
                        true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenNomEN.getProperty(nomenclatures.getNomen_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getIntitule_affichage() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getIntitule_affichage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false,
                        true));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getProvenance() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getProvenance(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false,
                        false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomenclatures.getTypenomenclature() != null ? nomenclatures.getTypenomenclature().getTn_nom() : "",
                        listeTypeNomenclaturePossibles,
                        null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomenclatures.getFiliere().getFiliere_nom() != null ? nomenclatures.getFiliere().getFiliere_nom() : "",
                        listeFilierePossibles,
                        null, false, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomenclatures.getPrecisionfiliere().getPfiliere_nom() != null ? nomenclatures.getPrecisionfiliere().getPfiliere_nom() : "",
                        listePrecisionFilierePossibles,
                        null, false, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getPrecisionpro() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getPrecisionpro(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false,
                        false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomenclatures.getFacteur() != null ? nomenclatures.getFacteur().getLibelle() : "",
                        listeFacteurPossibles,
                        null, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getEtatcoproduit() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getEtatcoproduit(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomenclatures.getNomen_comment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomenclatures == null || nomenclatures.getNomen_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : commenEN.getProperty(nomenclatures.getNomen_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @return
     */
    public INomenclatureDAO getNomenDAO() {
        return nomenDAO;
    }
    
    @Override
    protected ModelGridMetadata<Nomenclatures> initModelGridMetadata() {
        nomenNomEN = localizationManager.newProperties(Nomenclatures._NAME_ENTITY_JPA, Nomenclatures.JPA_COLUMN_NAME, Locale.ENGLISH);
        commenEN = localizationManager.newProperties(Nomenclatures._NAME_ENTITY_JPA, Nomenclatures.JPA_COLUMN__COMMENT, Locale.ENGLISH);
        updateNamesTypeNomen();
        PrecisionProPossibles();
        PrecisionFilierePossibles();
        FilierePossibles();
        facteurPossibles();
        return super.initModelGridMetadata();
        
    }
    
    private void persistNomenclature(final String code, final String mycode, final String nom,
            final String comment, String etat, TypeNomenclature tnomen,
            Filiere filiere, Precisionfiliere pfiliere, String ppro, Facteur facteur,
            String provenance, String affichage) throws BusinessException, BusinessException {
        Nomenclatures dbnomenclatures = nomenDAO.getByNKey(nom, tnomen.getTn_nom()).orElse(null);
        createOrUpdateNomenclatures(code, mycode, nom, comment, etat, tnomen, filiere, pfiliere, ppro, dbnomenclatures, facteur, provenance, affichage);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nomenclatures._NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(nom);
                String affichage = tokenizerValues.nextToken();
                String provenance = tokenizerValues.nextToken();
                long indextn = tokenizerValues.currentTokenIndex();
                final String tnnom = tokenizerValues.nextToken();
                long indexf = tokenizerValues.currentTokenIndex();
                final String filiere = tokenizerValues.nextToken();
                long indexpf = tokenizerValues.currentTokenIndex();
                final String pfiliere = tokenizerValues.nextToken();
                long indexpp = tokenizerValues.currentTokenIndex();
                final String ppro = tokenizerValues.nextToken();
                long indexFacteur = tokenizerValues.currentTokenIndex();
                final String libelleFacteur = tokenizerValues.nextToken();
                final String etat = tokenizerValues.nextToken();
                final String commen = tokenizerValues.nextToken();
                
                TypeNomenclature dbtypenomen = verifieTypeNomenclature(tnnom, errorsReport, line, indextn);
                Filiere dbfiliere = verifeFiliere(filiere, errorsReport, line, indexf);
                Precisionfiliere dbpfiliere = verifiePrecisionFiliere(pfiliere, dbfiliere, errorsReport, line, indexpf);
                
                Facteur dbFacteur = verifieFacteur(libelleFacteur, errorsReport, line, indexFacteur);
                
                if (!errorsReport.hasErrors()) {
                    persistNomenclature(code, mycode, nom, commen, etat, dbtypenomen, dbfiliere, dbpfiliere, ppro, dbFacteur, provenance, affichage);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    private Facteur verifieFacteur(final String libelleFacteur, final ErrorsReport errorsReport, long line, long index) throws BusinessException {
        Facteur dbFacteur = facteurDAO.getByNKey(libelleFacteur).orElse(null);
        if (dbFacteur == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_FACTEUR_NONDEFINI), line, index, libelleFacteur));
        }
        return dbFacteur;
    }
    
    private Precisionfiliere verifiePrecisionFiliere(final String pfiliere, Filiere dbfiliere, final ErrorsReport errorsReport, long line, long indexpp) throws BusinessException {
        Precisionfiliere dbpfiliere = precisionfiliereDAO.getByNKey(pfiliere).orElse(null);
        if (dbfiliere == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_PRECISIONFILIERE), line, indexpp, pfiliere));
        }
        return dbpfiliere;
    }
    
    private Filiere verifeFiliere(final String filiere, final ErrorsReport errorsReport, long line, long indexpf) throws BusinessException {
        Filiere dbfiliere = filiereDAO.getByNKey(filiere).orElse(null);
        if (dbfiliere == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_FILIERE), line, indexpf, filiere));
        }
        return dbfiliere;
    }
    
    private TypeNomenclature verifieTypeNomenclature(final String tnnom, final ErrorsReport errorsReport, long line, long indexf) throws BusinessException {
        TypeNomenclature dbtypenomen = typenomenclatureDAO.getByNKey(Utils.createCodeFromString(tnnom)).orElse(null);
        if (dbtypenomen == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_TYPENOMEN), line, indexf, tnnom));
        }
        return dbtypenomen;
    }
    
    /**
     *
     * @return
     */
    public Properties getNomenNomEN() {
        return nomenNomEN;
    }
    
    /**
     *
     * @param nomenNomEN
     */
    public void setNomenNomEN(Properties nomenNomEN) {
        this.nomenNomEN = nomenNomEN;
    }
    
    /**
     *
     * @return
     */
    public Properties getCommenEN() {
        return commenEN;
    }
    
    /**
     *
     * @param commenEN
     */
    public void setCommenEN(Properties commenEN) {
        this.commenEN = commenEN;
    }
    
    /**
     *
     * @param typenomenclatureDAO
     */
    public void setTypenomenclatureDAO(ITypeNomenclatureDAO typenomenclatureDAO) {
        this.typenomenclatureDAO = typenomenclatureDAO;
    }
    
    /**
     *
     * @param nomenDAO
     */
    public void setNomenDAO(INomenclatureDAO nomenDAO) {
        this.nomenDAO = nomenDAO;
    }
    
    private void updateNamesTypeNomen() {
        List<TypeNomenclature> groupegtp = typenomenclatureDAO.getAll(TypeNomenclature.class);
        String[] listsTNPossibles = new String[groupegtp.size() + 1];
        listsTNPossibles[0] = "";
        int index = 1;
        for (TypeNomenclature typenome : groupegtp) {
            listsTNPossibles[index++] = typenome.getTn_nom();
        }
        this.listeTypeNomenclaturePossibles = listsTNPossibles;
    }
    
    private void FilierePossibles() {
        List<Filiere> lesfilieres = filiereDAO.getAll(Filiere.class);
        String[] listesFilierePossibles = new String[lesfilieres.size() + 1];
        listesFilierePossibles[0] = "";
        int index = 1;
        for (Filiere filiere : lesfilieres) {
            listesFilierePossibles[index++] = filiere.getFiliere_nom();
        }
        this.listeFilierePossibles = listesFilierePossibles;
    }
    
    private void PrecisionFilierePossibles() {
        List<Precisionfiliere> lespfilieres = precisionfiliereDAO.getAll(Precisionfiliere.class);
        String[] listePFilierePossibles = new String[lespfilieres.size() + 1];
        listePFilierePossibles[0] = "";
        int index = 1;
        for (Precisionfiliere pfiliere : lespfilieres) {
            listePFilierePossibles[index++] = pfiliere.getPfiliere_nom();
        }
        this.listePrecisionFilierePossibles = listePFilierePossibles;
    }
    
    private void PrecisionProPossibles() {
        List<Precisionpro> lesppros = precisionproDAO.getAll(Precisionpro.class);
        String[] listePProPossibles = new String[lesppros.size() + 1];
        listePProPossibles[0] = "";
        int index = 1;
        for (Precisionpro ppro : lesppros) {
            listePProPossibles[index++] = ppro.getP_nom();
        }
        this.listePrecisionProPossibles = listePProPossibles;
    }
    
    private void facteurPossibles() {
        List<Facteur> lstFacteurs = facteurDAO.getLstFacteurByIsAssocNomenclature(true);
        String[] facteurPossibles = new String[lstFacteurs.size() + 1];
        facteurPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (Facteur facteur : lstFacteurs) {
            facteurPossibles[index++] = facteur.getLibelle();
        }
        this.listeFacteurPossibles = facteurPossibles;
    }
    
    /**
     *
     * @return
     */
    public IFiliereDAO getFiliereDAO() {
        return filiereDAO;
    }
    
    /**
     *
     * @return
     */
    public IPrecisionFiliereDAO getPrecisionfiliereDAO() {
        return precisionfiliereDAO;
    }
    
    /**
     *
     * @return
     */
    public IPrecisionProDAO getPrecisionproDAO() {
        return precisionproDAO;
    }
    
    /**
     *
     * @param filiereDAO
     */
    public void setFiliereDAO(IFiliereDAO filiereDAO) {
        this.filiereDAO = filiereDAO;
    }
    
    /**
     *
     * @param precisionfiliereDAO
     */
    public void setPrecisionfiliereDAO(IPrecisionFiliereDAO precisionfiliereDAO) {
        this.precisionfiliereDAO = precisionfiliereDAO;
    }
    
    /**
     *
     * @param precisionproDAO
     */
    public void setPrecisionproDAO(IPrecisionProDAO precisionproDAO) {
        this.precisionproDAO = precisionproDAO;
    }
    
    /**
     *
     * @param facteurDAO
     */
    public void setFacteurDAO(IFacteurDAO facteurDAO) {
        this.facteurDAO = facteurDAO;
    }
    
}
