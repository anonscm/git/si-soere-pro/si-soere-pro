/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.elementaire.impl;

import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePRODatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.extraction.physicochimie.IPhysicoChimieDAO;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieExtractor;

/**
 *
 * @author adiankha
 */
public class ProduitBrutExtractor extends AbstractPhysicoChimieExtractor<MesurePhysicoChimieSols> {

    private static final String CST_EXTRACT = "EXTRACT_";

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     *
     */
    public static String MAP_INDEX_PRODUITBRUT = "produitBrut";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_PRODUITBRUT_CODE = "extractionResultProduit";

    private IPhysicoChimieDAO produitBrutDAO;

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return CST_RESULT_EXTRACTION_PRODUITBRUT_CODE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IPhysicoChimiePRODatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_PRODUITBRUT;
    }
}
