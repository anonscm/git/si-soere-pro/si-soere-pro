/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.especeplante;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IEspecePlanteDAO extends IDAO<EspecePlante> {

    /**
     *
     * @return
     */
    List<EspecePlante> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<EspecePlante> getByNKey(String code);

}
