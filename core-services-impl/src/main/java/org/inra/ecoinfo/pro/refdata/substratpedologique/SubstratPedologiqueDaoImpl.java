package org.inra.ecoinfo.pro.refdata.substratpedologique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class SubstratPedologiqueDaoImpl extends AbstractJPADAO<Substratpedologique> implements ISubstratPedologiqueDAO {

    private static final String QUERY_SUBSTRATPEDOLOGIQUE_NAME = "from Substratpedologique t where t.nom =:nom";
    private static final String QUERY_SUBSTRATPEDOLOGIQUE_KEY = "from Substratpedologique k where k.code =:code";

    /**
     *
     * @return
     */
    @Override
    public List<Substratpedologique> getAll() {
        return getAll(Substratpedologique.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Substratpedologique> getByNKey(String nom) {
        CriteriaQuery<Substratpedologique> query = builder.createQuery(Substratpedologique.class);
        Root<Substratpedologique> substratpedologique = query.from(Substratpedologique.class);
        query
                .select(substratpedologique)
                .where(
                        builder.equal(substratpedologique.get(Substratpedologique_.code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }

}
