/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.IValeurProduitBrutDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO_;

/**
 *
 * @author adiankha
 */
public class JPAValeurProduitBrutDAO extends AbstractJPADAO<ValeurPhysicoChimiePRO> implements IValeurProduitBrutDAO {

    @Override
    public Optional<ValeurPhysicoChimiePRO> getByNKeys(RealNode dvumRealNode, MesurePhysicoChimiePRO mesureBrut, String statut) {
        CriteriaQuery<ValeurPhysicoChimiePRO> query = builder.createQuery(ValeurPhysicoChimiePRO.class);
        Root<ValeurPhysicoChimiePRO> v = query.from(ValeurPhysicoChimiePRO.class);
        Join<ValeurPhysicoChimiePRO, RealNode> rnVariable = v.join(ValeurPhysicoChimiePRO_.realNode);
        Join<ValeurPhysicoChimiePRO, MesurePhysicoChimiePRO> m = v.join(ValeurPhysicoChimiePRO_.mesurePhysicoChimiePRO);
        query
                .select(v)
                .where(
                        builder.equal(v.get(ValeurPhysicoChimiePRO_.statutvaleur), statut),
                        builder.equal(rnVariable, dvumRealNode),
                        builder.equal(m, mesureBrut)
                );
        return getOptional(query);
    }

}
