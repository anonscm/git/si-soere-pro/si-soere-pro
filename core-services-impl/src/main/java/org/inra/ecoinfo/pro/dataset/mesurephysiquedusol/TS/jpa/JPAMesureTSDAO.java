/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.TS.jpa;

import java.sql.Time;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.TS.IMesureTSDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.MesureTS;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.MesureTS_;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.SousSequenceTS;

/**
 *
 * @author vjkoyao
 */
public class JPAMesureTSDAO extends AbstractJPADAO<MesureTS> implements IMesureTSDAO<MesureTS> {

    /**
     *
     * @param heure
     * @param sousSequenceTS
     * @return
     */
    @Override
    public Optional<MesureTS> getByNKeys(Time heure, SousSequenceTS sousSequenceTS) {
        CriteriaQuery<MesureTS> query = builder.createQuery(MesureTS.class);
        Root<MesureTS> m = query.from(MesureTS.class);
        Join<MesureTS, SousSequenceTS> ss = m.join(MesureTS_.sousSequenceTS);
        query
                .select(m)
                .where(
                        builder.equal(ss, sousSequenceTS),
                        builder.equal(m.get(MesureTS_.heure), heure)
                );
        return getOptional(query);
    }

}
