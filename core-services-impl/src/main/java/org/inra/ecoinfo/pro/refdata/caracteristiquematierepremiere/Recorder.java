package org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.ICaracteristiqueValeurDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.ITypeCaracteristiqueDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<CaracteristiqueMatierePremieres> {

    private static final String PROPERTY_MSG_CARACTMP_BAD_NOMTYPECMP = "PROPERTY_MSG_CARACTMP_BAD_NOMTYPECMP";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected ICaracteristiqueMatierePremiereDAO cmpDAO;

    /**
     *
     */
    protected ITypeCaracteristiqueDAO typecaracteristiqueDAO;

    /**
     *
     */
    protected ICaracteristiqueValeurDAO cvDAO;

    private String[] listeTcarPossibles;

    private void createCMP(final CaracteristiqueMatierePremieres cmp) throws BusinessException {
        try {
            cmpDAO.saveOrUpdate(cmp);
            cmpDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create cmp");
        }
    }

    private void createOrUpdateCMP(final String code, String nom, TypeCaracteristiques typec) throws BusinessException {
        final CaracteristiqueMatierePremieres dbcmp = cmpDAO.getByNKey(nom, typec)
                .orElse(null);
        if (dbcmp == null) {
            CaracteristiqueMatierePremieres cmp = new CaracteristiqueMatierePremieres(code, nom, typec);
            cmp.setCmp_code(code);
            cmp.setCmp_nom(nom);
            cmp.setTypecaracteristique(typec);
            createCMP(cmp);
        } else {
            updateBDCMP(code, nom, typec, dbcmp);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String type = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                cmpDAO.remove(cmpDAO.getByNKey(nom, typecaracteristiqueDAO.getByNKey(type).orElseThrow(PersistenceException::new)).orElseThrow(PersistenceException::new));
                values = parser.getLine();

            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<CaracteristiqueMatierePremieres> getAllElements() throws BusinessException {

        return cmpDAO.getAll();
    }

    /**
     *
     * @return
     */
    public ICaracteristiqueMatierePremiereDAO getCmpDAO() {
        return cmpDAO;
    }

    /**
     *
     * @return
     */
    public ICaracteristiqueValeurDAO getCvDAO() {
        return cvDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CaracteristiqueMatierePremieres cmp) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(cmp == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : cmp.getTypecaracteristique().getTcar_nom() != null ? cmp.getTypecaracteristique().getTcar_nom() : "",
                listeTcarPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(cmp == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : cmp.getCmp_nom(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;

    }

    /**
     *
     * @return
     */
    public ITypeCaracteristiqueDAO getTypecaracteristiqueDAO() {
        return typecaracteristiqueDAO;
    }

    private void getTypeCarPossibles() {
        List<TypeCaracteristiques> groupetcar = typecaracteristiqueDAO.getAll();
        String[] Listetcar = new String[groupetcar.size() + 1];
        Listetcar[0] = "";
        int index = 1;
        for (TypeCaracteristiques tcar : groupetcar) {
            Listetcar[index++] = tcar.getTcar_nom();
        }
        this.listeTcarPossibles = Listetcar;
    }

    @Override
    protected ModelGridMetadata<CaracteristiqueMatierePremieres> initModelGridMetadata() {
        getTypeCarPossibles();
        return super.initModelGridMetadata();

    }

    private void persistCMP(final String cmp_code, final String cmp_nom, TypeCaracteristiques tc) throws BusinessException {
        createOrUpdateCMP(cmp_code, cmp_nom, tc);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CaracteristiqueMatierePremieres.NAME_ENTITY_JPA);
                int index = tokenizerValues.currentTokenIndex();
                final String tcar = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);

                TypeCaracteristiques dbtcar = typecaracteristiqueDAO.getByNKey(tcar).orElse(null);
                if (dbtcar == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CARACTMP_BAD_NOMTYPECMP), line, index, tcar));
                }
                if (!errorsReport.hasErrors()) {
                    persistCMP(code, nom, dbtcar);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param cmpDAO
     */
    public void setCmpDAO(ICaracteristiqueMatierePremiereDAO cmpDAO) {
        this.cmpDAO = cmpDAO;
    }

    /**
     *
     * @param cvDAO
     */
    public void setCvDAO(ICaracteristiqueValeurDAO cvDAO) {
        this.cvDAO = cvDAO;
    }

    /**
     *
     * @param typecaracteristiqueDAO
     */
    public void setTypecaracteristiqueDAO(ITypeCaracteristiqueDAO typecaracteristiqueDAO) {
        this.typecaracteristiqueDAO = typecaracteristiqueDAO;
    }

    private void updateBDCMP(final String code, String nom, TypeCaracteristiques typec, final CaracteristiqueMatierePremieres dbcmp) throws BusinessException {
        try {
            dbcmp.setCmp_code(code);
            dbcmp.setCmp_nom(nom);
            dbcmp.setTypecaracteristique(typec);
            cmpDAO.saveOrUpdate(dbcmp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update cmp");
        }
    }

}
