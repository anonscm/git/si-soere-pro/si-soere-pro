/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre.jpa;

;
import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.flux_chambre.IMesureFluxChambreDAO;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres_;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 *
 * @author vjkoyao
 */


public class JPAMesureFluxChambreDAO extends AbstractJPADAO<MesureFluxChambres> implements IMesureFluxChambreDAO<MesureFluxChambres> {

    /**
     *
     * @param date_messure
     * @param nbChambre
     * @param parcelle
     * @return
     */
    @Override
    public Optional<MesureFluxChambres> getByNKeys(LocalDate date_messure, int nbChambre, ParcelleElementaire parcelle) {
        CriteriaQuery<MesureFluxChambres> query = builder.createQuery(MesureFluxChambres.class);
        Root<MesureFluxChambres> m = query.from(MesureFluxChambres.class);
        Join<MesureFluxChambres, ParcelleElementaire> par = m.join(MesureFluxChambres_.parcelleElementaire);
        query
                .select(m)
                .where(
                        builder.equal(m, m.get(MesureFluxChambres_.LocalDate)),
                        builder.equal(m, m.get(MesureFluxChambres_.nbrChambre)),
                        builder.equal(par, parcelle)
                );
        return getOptional(query);
    }

}
