/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolbrut.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationOutputBuilder;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolBuildOutputDisplayByRow extends AbstractIncubationOutputBuilder<MesureIncubationSol> {

    private static final int TEN = 10;

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.incubation.incubation-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";

    static final String PATTERN_CSV_N_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_INCUBATIONSOL";

    static final String LIST_COLUMN_VARIABLE_INCUBATION_SOL = "LIST_COLUMN_VARIABLE_INCUBATIONSOL";

    final ComparatorVariable comparator = new ComparatorVariable();
    IVariablesPRODAO variPRODAO;

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return IncubationSolExtractor.MAP_INDEX_INCUBATION_SOL;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IIncubationSolDatatypeManager.CODE_DATATYPE_INCUBATION_SOL;
    }

    /**
     *
     * @param mesure
     * @param mesuresMap
     */
    @Override
    protected void buildmap(List<MesureIncubationSol> mesure, SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSol>>> mesuresMap) {
        java.util.Iterator<MesureIncubationSol> itMesure = mesure
                .iterator();
        while (itMesure.hasNext()) {
            MesureIncubationSol mesureIncubationSol = itMesure
                    .next();
            EchantillonsSol echan = mesureIncubationSol.getEchantillonsSol();
            Long siteId = echan.getPrelevementsol().getDispositif().getId();
            if (mesuresMap.get(siteId) == null) {
                mesuresMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresMap.get(siteId).get(echan) == null) {
                mesuresMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresMap.get(siteId).get(echan).put(mesureIncubationSol.getLocalDate_debut_incub(), mesureIncubationSol);
            itMesure.remove();
        }
    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedFluxChambreVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresMap
     */
    @Override
    protected void readMap(
            List<Dispositif> selectedDispositifs,
            List<VariablesPRO> selectedFluxChambreVariables,
            IntervalDate selectedIntervalDate,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSol>>> mesuresMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                IncubationSolBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        IncubationSolBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        IncubationSolBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_CODE)
                .get(IncubationSolExtractor.MAP_INDEX_INCUBATION_SOL) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_CODE)
                        .get(IncubationSolExtractor.MAP_INDEX_INCUBATION_SOL).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_CODE));
        return null;
    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSol>>> mesuresMap, IntervalDate selectedIntervalDate) {

        try {

            String currentDispositif;
            String currentEchantillon;
            int ordre_manip;
            float Masse_de_sol_sec;
            String condition_incubation;
            String humidite_incubation;
            float temperature_incubation;
            float N_mineral_par_solution;
            LocalDate date_debut_incub;
            String labo_analyse;
            String code_interne_labo;
            int numero_rep_analyse;
            int jour_incub;

            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    IncubationSolBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    IncubationSolBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_INCUBATION_SOL));//.split(";")

            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSol>> mesureIncubSolByDisp = mesuresMap
                        .get(dispositif.getId());

                if (mesureIncubSolByDisp == null) {
                    continue;
                }

                Iterator<Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSol>>> itEchan = mesureIncubSolByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSol>> echanEntry = itEchan.next();
                    EchantillonsSol echantillonsSol = echanEntry.getKey();
                    SortedMap<LocalDate, MesureIncubationSol> mesureEchantMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());

                    for (Entry<LocalDate, MesureIncubationSol> entrySet : mesureEchantMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureIncubationSol mesureIncubationSol = entrySet.getValue();
                        currentDispositif = mesureIncubationSol.getEchantillonsSol().getPrelevementsol().getDispositif().getNomDispo();
                        currentEchantillon = mesureIncubationSol.getEchantillonsSol().getCodeechsol();
                        ordre_manip = mesureIncubationSol.getOrdre_manip();
                        Masse_de_sol_sec = mesureIncubationSol.getMasse_de_sol();
                        condition_incubation = mesureIncubationSol.getCondition_incub();
                        humidite_incubation = mesureIncubationSol.getHumidite_incub();
                        temperature_incubation = mesureIncubationSol.getTemperature_incub();
                        N_mineral_par_solution = mesureIncubationSol.getN_mineral_apporte();
                        date_debut_incub = mesureIncubationSol.getLocalDate_debut_incub();
                        labo_analyse = mesureIncubationSol.getLabo_analyse();
                        code_interne_labo = mesureIncubationSol.getCode_interne_labo();
                        numero_rep_analyse = mesureIncubationSol.getNumero_rep_analyse();
                        jour_incub = mesureIncubationSol.getJour_incub();
                        String genericPattern = LineDataFixe(date, currentDispositif, currentEchantillon, ordre_manip, Masse_de_sol_sec,
                                condition_incubation, humidite_incubation, temperature_incubation, N_mineral_par_solution, date_debut_incub, labo_analyse,
                                code_interne_labo, numero_rep_analyse, jour_incub);
                        out.print(genericPattern);
                        LineDataVariable(mesureIncubationSol, out);
                    }
                    itEchan.remove();
                }
            }

        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentEchantillon, int ordre_manip, float Masse_de_sol_sec, String condition_incubation, String humidite_incubation, float temperature_incubation, float N_mineral_par_solution,
            LocalDate date_debut_incub, String labo_analyse, String code_interne_labo, int numero_rep_analyse, int jour_incub) {

        String genericPattern = String.format(PATTERN_CSV_N_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentEchantillon,
                ordre_manip,
                Masse_de_sol_sec,
                condition_incubation,
                humidite_incubation,
                temperature_incubation,
                N_mineral_par_solution,
                date_debut_incub,
                labo_analyse,
                code_interne_labo,
                numero_rep_analyse,
                jour_incub);

        return genericPattern;

    }

    private String getValeurToString(ValeurIncubationSol valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else {
            return "NA";
        }
    }

    private void LineDataVariable(MesureIncubationSol mesureIncubationSol, PrintStream out) {
        final List<ValeurIncubationSol> valeurIncubationSols = mesureIncubationSol.getValeurIncubationSol();

        List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                IncubationSolBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                IncubationSolBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_INCUBATION_SOL));

        listVariable.forEach((_item) -> {
            for (Iterator<ValeurIncubationSol> ValeurIterator = valeurIncubationSols.iterator(); ValeurIterator.hasNext();) {
                ValeurIncubationSol valeur = ValeurIterator.next();
                String line;
                line = String.format(";%s;%s;%s;%s;%s;%s",
                        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                        getValeurToString(valeur),
                        valeur.getStatutvaleur(),
                        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());

                out.print(line);
                ValeurIterator.remove();
            }
        });
        out.println();
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public ComparatorVariable getComparator() {
        return comparator;
    }

    /**
     *
     * @return
     */
    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(IncubationSolBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
