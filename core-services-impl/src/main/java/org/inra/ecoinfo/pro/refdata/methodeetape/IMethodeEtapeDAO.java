package org.inra.ecoinfo.pro.refdata.methodeetape;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IMethodeEtapeDAO extends IDAO<MethodeEtapes> {

    /**
     *
     * @return
     */
    List<MethodeEtapes> getAll();

    /**
     *
     * @param me_nom
     * @return
     */
    Optional<MethodeEtapes> getByNKey(String me_nom);

}
