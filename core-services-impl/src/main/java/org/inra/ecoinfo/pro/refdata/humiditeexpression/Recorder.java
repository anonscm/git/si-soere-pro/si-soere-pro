/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.humiditeexpression;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<HumiditeExpression> {

    IHumiditeExpressionDAO humiditeDAO;
    Properties humEn;
    Properties codeEn;

    /**
     *
     * @param humidite
     * @throws PersistenceException
     */
    public void createHumidite(HumiditeExpression humidite) throws PersistenceException {
        humiditeDAO.saveOrUpdate(humidite);
        humiditeDAO.flush();
    }

    /**
     *
     * @param code
     * @param mycode
     * @param nom
     * @param dbHumidite
     * @throws PersistenceException
     */
    public void updateHumidite(String code, String mycode, String nom, HumiditeExpression dbHumidite) throws PersistenceException {
        dbHumidite.setCode(code);
        dbHumidite.setMycode(mycode);
        dbHumidite.setNom(nom);
        humiditeDAO.saveOrUpdate(dbHumidite);
    }

    /**
     *
     * @param code
     * @param mycode
     * @param nom
     * @param dbHumidite
     * @throws PersistenceException
     */
    public void createOrUpdateHumidite(String code, String mycode, String nom, HumiditeExpression dbHumidite) throws PersistenceException {
        if (dbHumidite == null) {
            HumiditeExpression humidite = new HumiditeExpression(code, nom);
            humidite.setCode(code);
            humidite.setMycode(mycode);
            humidite.setNom(nom);
            createHumidite(humidite);
        } else {
            updateHumidite(code, mycode, nom, dbHumidite);
        }
    }

    private void persitHumidite(String code, String mycode, String nom) throws PersistenceException {
        final HumiditeExpression humidite = humiditeDAO.getByCode(code).orElse(null);
        createOrUpdateHumidite(code, mycode, nom, humidite);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String code = tokenizerValues.nextToken();
                final HumiditeExpression dbhetero = humiditeDAO.getByCode(code)
                        .orElseThrow(() -> new BusinessException("can't get humidite expression"));
                humiditeDAO.remove(dbhetero);
                values = parser.getLine();

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, HumiditeExpression.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(code);
                persitHumidite(code, mycode, nom);
                values = parser.getLine();
            }

        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<HumiditeExpression> getAllElements() throws BusinessException {
        return humiditeDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(HumiditeExpression humidite) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(humidite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : humidite.getCode(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(humidite == null || humidite.getCode() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : codeEn.getProperty(humidite.getCode()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(humidite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : humidite.getNom(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(humidite == null || humidite.getCode() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : humEn.getProperty(humidite.getNom()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param humiditeDAO
     */
    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    @Override
    protected ModelGridMetadata<HumiditeExpression> initModelGridMetadata() {
        humEn = localizationManager.newProperties(HumiditeExpression.NAME_ENTITY_JPA, HumiditeExpression.JPA_COLUMN_NAME, Locale.ENGLISH);
        codeEn = localizationManager.newProperties(HumiditeExpression.NAME_ENTITY_JPA, HumiditeExpression.JPA_COLUMN_CODE, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}
