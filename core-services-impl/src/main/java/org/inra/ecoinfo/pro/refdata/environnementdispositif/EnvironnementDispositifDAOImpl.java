/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.environnementdispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.environnement.Environnement;

/**
 *
 * @author adiankha
 */
public class EnvironnementDispositifDAOImpl extends AbstractJPADAO<EnvironnementDispositif> implements IEnvironnementDispositifDAO {

    /**
     *
     * @return
     */
    @Override
    public List<EnvironnementDispositif> gettAll() {
        return getAllBy(EnvironnementDispositif.class, EnvironnementDispositif::getDispositif);
    }

    /**
     *
     * @param environnement
     * @param dispositif
     * @return
     */
    @Override
    public Optional<EnvironnementDispositif> getByNKey(Environnement environnement, Dispositif dispositif) {
        CriteriaQuery<EnvironnementDispositif> query = builder.createQuery(EnvironnementDispositif.class);
        Root<EnvironnementDispositif> environnementDispositif = query.from(EnvironnementDispositif.class);
        Join<EnvironnementDispositif, Dispositif> dispo = environnementDispositif.join(EnvironnementDispositif_.dispositif);
        Join<EnvironnementDispositif, Environnement> enviro = environnementDispositif.join(EnvironnementDispositif_.environnement);
        query
                .select(environnementDispositif)
                .where(
                        builder.equal(enviro, environnement),
                        builder.equal(dispo, dispositif)
                );
        return getOptional(query);
    }
}
