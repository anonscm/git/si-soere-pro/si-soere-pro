
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Transient;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
/**
 *
 * @author adiankha
 */
public abstract  class AbstractProcessRecord implements IProcessRecord, Serializable{

    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.pro.dataset.impl.messages";

    /**
     *
     */
    @Transient
    protected IVersionFileDAO versionFileDAO;
     
    /**
     *
     */
    @Transient
    protected ILocalizationManager localizationManager;
    
    /**
     *
     */
    @Transient
    protected IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    
    /**
     *
     */
    @Transient 
    protected IVariablesPRODAO variPRODAO;

    /**
     *
     */
    protected IDatasetConfiguration datasetConfiguration;

    /**
     *
     */
    protected IMgaRecorder mgaRecorder;

    /**
     *
     */
    protected INotificationsManager notificationsManager;

    /**
     *
     */
    public AbstractProcessRecord(){
          super();
      }

    /**
     *
     * @param datasetConfiguration
     */
    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    /**
     *
     * @param notificationsManager
     */
    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }
      
    /**
     *
     * @param sessionProperties
     * @return
     */
    protected Dispositif getDispositif(final ISessionPropertiesPRO sessionProperties) {

        return sessionProperties.getDispositif();

       }
       
    /**
     *
     * @param parser
     * @param datasetDescriptor
     * @return
     * @throws PersistenceException
     * @throws IOException
     */
    protected List<DatatypeVariableUnitePRO> buildVariablesHeaderAndSkipHeader(final CSVParser parser,
            final DatasetDescriptorPRO datasetDescriptor) throws PersistenceException, IOException {
       final List<DatatypeVariableUnitePRO> dbVariables = new LinkedList();
       datasetDescriptor.getColumns().stream().filter((colonne) -> !(RecorderPRO.PROPERTY_CST_DATE_TYPE.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_CODE_ECHANTILLON.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_NOM_LABORATOIRE.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_NUMERO_LABORATOIRE.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_NUMERO_REPETITION.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_CST_VALEUR_STATUT.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_CODE_HUMIDITE.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_CODE_METHODE.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_CODE_UNITE.equals(colonne.getFlagType())
               || RecorderPRO.PROPERTY_CODE_VARIABLE.equals(colonne.getFlagType())
               )); /*  if (colonne.isFlag()
        && (colonne.getFlagType().equals(RecorderPRO.PROPERTY_CST_VARIABLE_TYPE)
        || colonne.getFlagType().equals(
        RecorderPRO.PROPERTY_CST_LIST_VALEURS_STATUTS) || colonne
        .getFlagType()
        .equals(RecorderPRO.PROPERTY_CST_VALEUR_STATUT_TYPE))) {
        final VariablesPRO variablepro = (VariablesPRO) this.variPRODAO.getByAffichage(colonne.getName());
        dbVariables.add(variablepro);
        }*/
        for (int i = 0; i < datasetDescriptor.getEnTete(); i++) {

            parser.getLine();

        }
        return dbVariables;
    

        }
    
    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     *
     * @param dataTypeVariableUnitePRODAO
     */
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param fileEncoding
     * @param datasetDescriptorPRO
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, 
                                ISessionPropertiesPRO sessionProperties, 
                                String fileEncoding, 
                                DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException{
         this.verifieDispositifRepositoryEqualsDispositif(versionFile, sessionProperties);
        Utils.testNotNullArguments(this.localizationManager, versionFile, fileEncoding);
        Utils.testCastedArguments(versionFile, VersionFile.class, this.localizationManager);
    }
     
    /**
     *
     * @param version
     * @param sessionProperties
     * @throws BusinessException
     */
    protected void verifieDispositifRepositoryEqualsDispositif(final VersionFile version,
            final ISessionPropertiesPRO sessionProperties) throws BusinessException {
        final Dispositif dispositif = this.getDispositif(sessionProperties);
        final String dispositifCode = //((DispositifThemeDataType) version.getDataset()
                version.getDataset().getRealNode().getNodeByNodeableTypeResource(Dispositif.class).getNodeable().getCode();
        if (dispositif == null
                || !dispositif.getCode().equalsIgnoreCase(dispositifCode)) {

            final String error = String.format(RecorderPRO
                    .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_SITE_REPOSITORY),
                    dispositif == null ? "Null" : dispositif.getCode(), dispositifCode);

            final BusinessException businessException = new BusinessException(error);

            throw businessException;

        }
    } 
    
    /**
     *
     * @param errorsReport
     * @return
     */
    protected Notification buildWarningNotification(ErrorsReport errorsReport) {
        final Notification notification = new Notification();
       notification.setDate(LocalDateTime.now());
        notification.setLevel(Notification.WARN);
        notification.setMessage(String.format(RecorderPRO
                .getPROMessage(RecorderPRO.PROPERTY_MSG_WARNIN_INFO_IN8PUBLISH)));
        String bodyMessage = null;
        bodyMessage = errorsReport.getInfosMessages();
        notification.setBody(bodyMessage);
        return notification;
    }

    /**
     *
     * @return
     */
    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }   

    /**
     *
     * @param mgaRecorder
     */
    public void setMgaRecorder(IMgaRecorder mgaRecorder) {
        this.mgaRecorder = mgaRecorder;
    }
   
}
