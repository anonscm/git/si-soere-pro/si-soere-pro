package org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere.CaracteristiqueMatierePremieres;
import org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere.ICaracteristiqueMatierePremiereDAO;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.CaracteristiqueValeur;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.ICaracteristiqueValeurDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.ITypeCaracteristiqueDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<ValeurCaracteristiqueMP> {

    private static final String PROPERTY_MSG_NOMCARACTMP_BAD_CV = "PROPERTY_MSG_NOMCARACTMP_BAD_CV";

    private static final String PROPERTY_MSG_NOMCARACTMP_BAD_CMP = "PROPERTY_MSG_NOMCARACTMP_BAD_CMP";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    IValeurCaracteristiqueMPDAO vcmpDAO;

    ICaracteristiqueMatierePremiereDAO cmpDAO;

    ITypeCaracteristiqueDAO typecaracteristiqueDAO;

    ICaracteristiqueValeurDAO cvDAO;

    private String[] listeTcarPossibles;
    private String[] listeValeurPossible;
    private Map<String, String[]> listeCMPPossibles;

    private void createOrUpdateVCMP(CaracteristiqueMatierePremieres carmp, CaracteristiqueValeur cmpv, ValeurCaracteristiqueMP dbvcmp) throws BusinessException {
        if (dbvcmp == null) {
            ValeurCaracteristiqueMP vcmp = new ValeurCaracteristiqueMP(carmp, cmpv);
            vcmp.setCaracteristiquematierepremieres(carmp);
            vcmp.setCaracteristiquevaleur(cmpv);
            createValeurCaracteristiqueMP(vcmp);
        } else {
            updateBDValeurCaracteristiqueMP(carmp, cmpv, dbvcmp);
        }
    }

    private void createValeurCaracteristiqueMP(ValeurCaracteristiqueMP vcmp) throws BusinessException {
        try {
            vcmpDAO.saveOrUpdate(vcmp);
            vcmpDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create ValeurCaracteristiqueMP");
        }
    }

    private void updateBDValeurCaracteristiqueMP(CaracteristiqueMatierePremieres carmp, CaracteristiqueValeur vcmp, ValeurCaracteristiqueMP dbvcmp) throws BusinessException {
        try {
            dbvcmp.setCaracteristiquematierepremieres(carmp);
            dbvcmp.setCaracteristiquevaleur(vcmp);
            vcmpDAO.saveOrUpdate(dbvcmp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update ValeurCaracteristiqueMP");
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ValeurCaracteristiqueMP.NAME_ENTITY_JPA);
                final String type = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                /*TypeCaracteristiques typecarac = typecaracteristiqueDAO.FindbyName(type);
                CaracteristiqueMatierePremieres cmp = cmpDAO.getByNKey(nom, typecarac);
                CaracteristiqueValeur cv = cvDAO.getByNames(valeur);*/
                ValeurCaracteristiqueMP vcmp = vcmpDAO.getByNKey(type, nom, valeur)
                        .orElseThrow(() -> new BusinessException("can't get ValeurCaracteristiqueMP"));
                vcmpDAO.remove(vcmp);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<ValeurCaracteristiqueMP> getAllElements() throws BusinessException {
        return vcmpDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ValeurCaracteristiqueMP vcmp) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String nomtype = vcmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : vcmp.getCaracteristiquematierepremieres() != null ? vcmp.getCaracteristiquematierepremieres().getTypecaracteristique().getTcar_nom() : "";
        ColumnModelGridMetadata columnType = new ColumnModelGridMetadata(nomtype, listeTcarPossibles, null, true, false, true);
        String valeurnom = vcmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : vcmp.getCaracteristiquematierepremieres() != null ? vcmp.getCaracteristiquematierepremieres().getCmp_nom() : "";
        ColumnModelGridMetadata columnNom = new ColumnModelGridMetadata(valeurnom, listeCMPPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnNom);
        columnNom.setValue(valeurnom);
        columnType.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnType);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnNom);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(vcmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : vcmp.getCaracteristiquevaleur().getCv_nom() != null ? vcmp.getCaracteristiquevaleur().getCv_nom() : "", listeValeurPossible, null, true, false,
                        true));

        return lineModelGridMetadata;
    }

    private void getTypePossibles() {
        Map<String, String[]> typesnoms = new HashMap<>();
        Map<String, Set<String>> typesnomsList = new HashMap<>();
        List<CaracteristiqueMatierePremieres> groupetypes = cmpDAO.getAll();
        groupetypes.forEach((cmp) -> {
            String type = cmp.getTypecaracteristique().getTcar_nom();
            String nom = cmp.getCmp_nom();
            if (!typesnomsList.containsKey(type)) {
                typesnomsList.put(type, new TreeSet());
            }
            typesnomsList.get(type).add(nom);
        });
        typesnomsList.entrySet().forEach((entryType) -> {
            typesnoms.put(entryType.getKey(), entryType.getValue().toArray(new String[]{}));
        });
        this.listeCMPPossibles = typesnoms;
        listeTcarPossibles = typesnoms.keySet().toArray(new String[]{});
    }

    private void getValeurPossibles() {
        List<CaracteristiqueValeur> groupevcmp = cvDAO.getAll();
        String[] ListeValeurPossibles = new String[groupevcmp.size() + 1];
        ListeValeurPossibles[0] = "";
        int index = 1;
        for (CaracteristiqueValeur vcmp : groupevcmp) {
            ListeValeurPossibles[index++] = vcmp.getCv_nom();
        }
        this.listeValeurPossible = ListeValeurPossibles;
    }

    @Override
    protected ModelGridMetadata<ValeurCaracteristiqueMP> initModelGridMetadata() {
        getTypePossibles();
        getValeurPossibles();
        getTypePossibles();
        return super.initModelGridMetadata();
    }

    private void persistValeurCaracteristiqueMP(CaracteristiqueMatierePremieres cmp, CaracteristiqueValeur cv) throws BusinessException, BusinessException {
        ValeurCaracteristiqueMP dbvcmp = vcmpDAO.getByNKey(cmp.getTypecaracteristique().getTcar_nom(), cmp.getCmp_nom(), cv.getCv_nom()).orElse(null);
        createOrUpdateVCMP(cmp, cv, dbvcmp);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ValeurCaracteristiqueMP.NAME_ENTITY_JPA);
                int indextype = tokenizerValues.currentTokenIndex();
                final String type = tokenizerValues.nextToken();
                int indexnom = tokenizerValues.currentTokenIndex();
                final String nom = tokenizerValues.nextToken();
                int indexvaleur = tokenizerValues.currentTokenIndex();
                final String valeur = tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(valeur);

                CaracteristiqueMatierePremieres dbcmp = cmpDAO.getByNomAndType(nom, type).orElse(null);
                if (dbcmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMCARACTMP_BAD_CMP), line, indextype, type, indexnom, nom));
                }

                CaracteristiqueValeur dbcv = cvDAO.getByCode(code).orElse(null);
                if (dbcv == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMCARACTMP_BAD_CV), line, indexvaleur, valeur));
                }

                if (!errorsReport.hasErrors()) {
                    persistValeurCaracteristiqueMP(dbcmp, dbcv);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param cmpDAO
     */
    public void setCmpDAO(ICaracteristiqueMatierePremiereDAO cmpDAO) {
        this.cmpDAO = cmpDAO;
    }

    /**
     *
     * @param cvDAO
     */
    public void setCvDAO(ICaracteristiqueValeurDAO cvDAO) {
        this.cvDAO = cvDAO;
    }

    /**
     *
     * @param typecaracteristiqueDAO
     */
    public void setTypecaracteristiqueDAO(ITypeCaracteristiqueDAO typecaracteristiqueDAO) {
        this.typecaracteristiqueDAO = typecaracteristiqueDAO;
    }

    /**
     *
     * @param vcmpDAO
     */
    public void setVcmpDAO(IValeurCaracteristiqueMPDAO vcmpDAO) {
        this.vcmpDAO = vcmpDAO;
    }

}
