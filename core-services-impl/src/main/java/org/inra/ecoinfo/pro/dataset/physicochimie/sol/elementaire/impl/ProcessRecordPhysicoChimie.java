/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.IMesurePhysicoChimieDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.IValeurPhysicoChimieSol;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsol.IEchantillonSolDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordPhysicoChimie extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPhysicoChimie.class);

    protected static final String BUNDLE_PATH_SOL_BRUT = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.messages";
    private static final String MSG_ERROR_SOL_NOT_FOUND_DVU_DB = "MSG_ERROR_SOL_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_SOL_NOT_VARIABLEPRO_DB = "MSG_ERROR_SOL_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_SOL_NOT_FOUND_METHODE_DB = "MSG_ERROR_SOL_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_SOL_NOT_UNITEPRO_DB = "MSG_ERROR_SOL_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_SOL_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_SOL_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_SOL_ECHAN_BD = "MSG_ERROR_SOL_ECHAN_BD";
    private static final String MSG_ERROR_SOL_NOT_FOUND_MESURESOL_DB = "MSG_ERROR_SOL_NOT_FOUND_MESURESOL_DB";
    protected IMesurePhysicoChimieDAO<MesurePhysicoChimieSols> mesurePhysicoChimieDAO;
    protected IEchantillonSolDAO echantillonSolDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IValeurPhysicoChimieSol valeurPhysicoChimieDAO;

    public ProcessRecordPhysicoChimie() {
        super();
    }

    void buildMesure(final SolLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<SolLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException, ParseException {

        LocalDate date = mesureLines.getDateprelevement();
        String codeech = mesureLines.getCodeechantillon();
        final EchantillonsSol echantillonsSol = echantillonSolDAO.getByNKey(Utils.createCodeFromString(codeech)).orElse(null);
        if (echantillonsSol == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                    ProcessRecordPhysicoChimie.MSG_ERROR_SOL_ECHAN_BD), codeech));
        }
        String nomlabo = mesureLines.getNomlabo();
        int numerolabo = mesureLines.getNumerolabo();
        int numerorepet = mesureLines.getNumerorepet();
        Long fichier = mesureLines.getOriginalLineNumber();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);
        String variable = mesureLines.getCodevariable();
        float valeur = mesureLines.getValeurvariable();
        String statut = mesureLines.getStatutvaleur();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();
        String datatype = RecorderPRO.getDatatypeFromVersion(versionFile).getCode();

        String cdatatype = Utils.createCodeFromString(datatype);
        String cvariable = Utils.createCodeFromString(variable);
        String cunite = Utils.createCodeFromString(unite);
        String cmethode = Utils.createCodeFromString(methode);
        String chumitite = Utils.createCodeFromString(humidite);
        

        VariablesPRO dbvariable = variPRODAO.getByCodeUser(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                    ProcessRecordPhysicoChimie.MSG_ERROR_SOL_NOT_VARIABLEPRO_DB), cvariable));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                    ProcessRecordPhysicoChimie.MSG_ERROR_SOL_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                    ProcessRecordPhysicoChimie.MSG_ERROR_SOL_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                    ProcessRecordPhysicoChimie.MSG_ERROR_SOL_NOT_FOUND_HUMIDITE_DB), chumitite));
        }
        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(RecorderPRO.getDatatypeFromVersion(versionFile), dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                    ProcessRecordPhysicoChimie.MSG_ERROR_SOL_NOT_FOUND_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumitite));
        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesurePhysicoChimieSols dbMesureSol = getOrCreate(date, numerorepet, codeech, nomlabo, versionFile, echantillonsSol, numerolabo, fichier, versionFileDB);
            persitValeurSol(valeur, statut, dbdvumRealNode, dbMesureSol);
        }
    }

    private MesurePhysicoChimieSols getOrCreate(LocalDate date, int numerorepet, String codeech, String nomlabo, final VersionFile versionFile, final EchantillonsSol echantillonsSol, int numerolabo, Long fichier, final VersionFile versionFileDB) throws PersistenceException {
        String dateString = dateString = DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY);
        String intString = null;
        intString = Integer.toString(numerorepet);
        String key = dateString + "_" + codeech + "_" + nomlabo + "_" + intString;

        MesurePhysicoChimieSols mesurePhysicoChimie = mesurePhysicoChimieDAO.getByKeys(key).orElse(null);
        if (mesurePhysicoChimie == null) {
            mesurePhysicoChimie = new MesurePhysicoChimieSols(date, versionFile, echantillonsSol, numerolabo, numerorepet, nomlabo, fichier);
            mesurePhysicoChimie.setDatePrelevement(date);
            mesurePhysicoChimie.setEchantillon(echantillonsSol);
            mesurePhysicoChimie.setLigneFichierEchange(fichier);
            mesurePhysicoChimie.setNom_laboratoire(nomlabo);
            mesurePhysicoChimie.setNumero_laboratoire(numerorepet);
            mesurePhysicoChimie.setNumero_repetition(numerolabo);
            mesurePhysicoChimie.setVersionfile(versionFileDB);
            mesurePhysicoChimie.setKeymesure(key);
            mesurePhysicoChimieDAO.saveOrUpdate(mesurePhysicoChimie);
        }
        return mesurePhysicoChimie;
    }

    private void persitValeurSol(float valeur, String statut, RealNode dbdvumRealNode, MesurePhysicoChimieSols mesurePhysicoChimieSols) throws PersistenceException {
        final ValeurPhysicoChimieSols valeurSol = valeurPhysicoChimieDAO.getByNKeys(dbdvumRealNode, mesurePhysicoChimieSols, statut).orElse(null);
        createOrUpdateValeurSol(dbdvumRealNode, mesurePhysicoChimieSols, valeur, statut, valeurSol);
    }

    private void CreateDTVQ(final ValeurPhysicoChimieSols valeurPhysicoChimieSols) throws PersistenceException {
        valeurPhysicoChimieDAO.saveOrUpdate(valeurPhysicoChimieSols);
        // valeurPhysicoChimieDAO.flush();
    }

    private void updateValeurSol(RealNode dvumRealNode, MesurePhysicoChimieSols mesureSol, float valeur, String statutvaleur, ValeurPhysicoChimieSols valeurPhysicoChimieSols) throws PersistenceException {
        valeurPhysicoChimieSols.setRealNode(dvumRealNode);
        valeurPhysicoChimieSols.setMesurePhysicoChimieSols(mesureSol);
        valeurPhysicoChimieSols.setValeur(valeur);
        valeurPhysicoChimieSols.setStatutvaleur(statutvaleur);
        valeurPhysicoChimieDAO.saveOrUpdate(valeurPhysicoChimieSols);
    }

    void createOrUpdateValeurSol(RealNode dvumRealNode, MesurePhysicoChimieSols mesureSol, float valeur, String statutvaleur, ValeurPhysicoChimieSols valeurPhysicoChimieSols) throws PersistenceException {
        if (valeurPhysicoChimieSols == null) {
            ValeurPhysicoChimieSols valeurSol = new ValeurPhysicoChimieSols(valeur, statutvaleur, mesureSol, dvumRealNode);
            valeurSol.setRealNode(dvumRealNode);
            valeurSol.setMesurePhysicoChimieSols(mesureSol);
            valeurSol.setValeur(valeur);
            valeurSol.setStatutvaleur(statutvaleur);
            CreateDTVQ(valeurSol);
        } else {
            updateValeurSol(dvumRealNode, mesureSol, valeur, statutvaleur, valeurPhysicoChimieSols);
        }
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<SolLineRecord>> lines,
            final SortedSet<SolLineRecord> ligneEnErreur) throws PersistenceException,
            ParseException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<SolLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<SolLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (SolLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    public IMesurePhysicoChimieDAO<MesurePhysicoChimieSols> getMesurePhysicoChimieDAO() {
        return mesurePhysicoChimieDAO;
    }

    public void setMesurePhysicoChimieDAO(IMesurePhysicoChimieDAO<MesurePhysicoChimieSols> mesurePhysicoChimieDAO) {
        this.mesurePhysicoChimieDAO = mesurePhysicoChimieDAO;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, String fileEncoding,
            final DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<SolLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<SolLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | ParseException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordPhysicoChimie.class).error(ex.getMessage(), ex);
        }
    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    private long readLines(final CSVParser parser, final Map<LocalDate, List<SolLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException, ParseException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate dateprelevement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeech = cleanerValues.nextToken();
            final String labonom = cleanerValues.nextToken();
            final int numeroechan = Integer.parseInt(cleanerValues.nextToken());
            final int numerorepe = Integer.parseInt(cleanerValues.nextToken());
            final String codevpro = cleanerValues.nextToken();
            final float valeurpro = Float.parseFloat(cleanerValues.nextToken());
            final String statut = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();
            final SolLineRecord line = new SolLineRecord(dateprelevement, lineCount, codeech, labonom, numeroechan, numerorepe, codevpro, valeurpro, statut, codeunite, codemethode, codehumidite);
            if (!lines.containsKey(dateprelevement)) {
                lines.put(dateprelevement, new LinkedList<SolLineRecord>());
            }
            lines.get(dateprelevement).add(line);
        }
        return lineCount;
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesurePhysicoChimieDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    @Override
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public IEchantillonSolDAO getEchantillonSolDAO() {
        return echantillonSolDAO;
    }

    public void setEchantillonSolDAO(IEchantillonSolDAO echantillonSolDAO) {
        this.echantillonSolDAO = echantillonSolDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    public void setValeurPhysicoChimieDAO(IValeurPhysicoChimieSol valeurPhysicoChimieDAO) {
        this.valeurPhysicoChimieDAO = valeurPhysicoChimieDAO;
    }

}
