/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.especeplante;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAEspecePlanteDAO extends AbstractJPADAO<EspecePlante> implements IEspecePlanteDAO{

    @Override
    public List<EspecePlante> getAll()  {
       return getAllBy(EspecePlante.class, EspecePlante::getCode);
    }

    @Override
    public Optional<EspecePlante> getByNKey(String code) {
        CriteriaQuery<EspecePlante> query = builder.createQuery(EspecePlante.class);
        Root<EspecePlante> especePlante = query.from(EspecePlante.class);
        query
                .select(especePlante)
                .where(
                        builder.equal(especePlante.get(EspecePlante_.code), Utils.createCodeFromString(code))
                );
        return getOptional(query);
    }

   
    
}
