package org.inra.ecoinfo.pro.refdata.methodeprocede;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public interface IMethodeProcedesDAO extends IDAO<MethodeProcedes> {

    /**
     *
     * @return
     */
    List<MethodeProcedes> getAll();

    /**
     *
     * @param methodeprocess
     * @param process
     * @param pmp_ordre
     * @return
     * @throws PersistenceException
     */
    Optional<MethodeProcedes> getByNKey(Produits produits, Process process, int pmp_ordre);

    /**
     *
     * @param codeProduits
     * @param intituleProcess
     * @param pmp_ordre
     * @return
     */
    Optional<MethodeProcedes> getByNKey(String codeProduits, String intituleProcess, int pmp_ordre);

}
