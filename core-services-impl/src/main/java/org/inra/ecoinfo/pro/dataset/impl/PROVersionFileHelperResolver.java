/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelper;
import org.inra.ecoinfo.dataset.versioning.IVersionFileHelperResolver;
import org.inra.ecoinfo.dataset.versioning.exception.NoVersionFileHelperResolvedException;
import org.inra.ecoinfo.utils.Utils;
/**
 *
 * @author adiankha
 */
public class PROVersionFileHelperResolver implements IVersionFileHelperResolver{
     Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap;
     
    /**
     *
     * @return
     */
    public Map<String, IVersionFileHelper> getDatatypesVersionFileHelpersMap() {
        return this.datatypesVersionFileHelpersMap;
    }
    
    
    
    @Override
    public IVersionFileHelper resolveByDatatype(String datatypeName) throws NoVersionFileHelperResolvedException {
  
        final String datatypeCode = Utils.createCodeFromString(datatypeName);
        if (!this.datatypesVersionFileHelpersMap.containsKey(datatypeCode)) {
            this.datatypesVersionFileHelpersMap.put(datatypeCode, new VersionFileHelper());
            // throw new NoVersionFileHelperResolvedException(datatypeCode);
        }
        return this.datatypesVersionFileHelpersMap.get(datatypeCode);
    }

    /**
     *
     * @param datatypesVersionFileHelpersMap
     */
    public void setDatatypesVersionFileHelpersMap(
         final    Map<String, IVersionFileHelper> datatypesVersionFileHelpersMap) {
        this.datatypesVersionFileHelpersMap = datatypesVersionFileHelpersMap;
    }
}
