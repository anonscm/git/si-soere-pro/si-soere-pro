package org.inra.ecoinfo.pro.refdata;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.util.List;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.geolocalisation.IGeolocalisationDAO;
import org.inra.ecoinfo.pro.refdata.systemeprojection.ISystemeProjectionDAO;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 * @param <P>
 *
 */
public abstract class AbstractGenericRecorder<P> extends AbstractCSVMetadataRecorder<P> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    protected static final String REFDATAS_BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    protected static final String BAD_VALUE = "-9999";
    protected static final String EMPTY_VALUE = "-99999";
    protected static final float EMPTY_FLOAT_VALUE = -99999f;
    protected static final double EMPTY_DOUBLE_VALUE = -99999d;
    protected static final int EMPTY_INT_VALUE = -99999;
    protected static final String PROPERTY_MSG_INVALID_INT_VALUE = "PROPERTY_MSG_INVALID_INT_VALUE";
    protected static final String PROPERTY_MSG_INVALID_FLOAT_VALUE = "PROPERTY_MSG_INVALID_FLOAT_VALUE";
    protected static final String PROPERTY_MSG_INVALID_DOUBLE_VALUE = "PROPERTY_MSG_INVALID_DOUBLE_VALUE";
    protected static final String PROPERTY_MSG_MISSING_VALUE = "PROPERTY_MSG_MISSING_VALUE";

    /**
     *
     */
    protected ISystemeProjectionDAO systemeProjectionDAO;

    /**
     *
     */
    protected IGeolocalisationDAO geolocalisationDAO;

    public double verifieDouble(TokenizerValues tokenizerValues, long line, boolean nullable, ErrorsReport errorsReport) throws BusinessException {
        double doubleValue = EMPTY_DOUBLE_VALUE;
        String value = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(value)) {
            if (nullable) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_VALUE), line, tokenizerValues.currentTokenIndex()));
            } else {
                return doubleValue;
            }
        }
        try {
            doubleValue = Double.parseDouble(value);
        } catch (NumberFormatException | NullPointerException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_INVALID_DOUBLE_VALUE), line, tokenizerValues.currentTokenIndex(), value));

        }
        return doubleValue;
    }

    public int verifieInt(TokenizerValues tokenizerValues, long line, boolean nullable, ErrorsReport errorsReport) throws BusinessException {
        int intvalue = EMPTY_INT_VALUE;
        String value = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(value)) {
            if (nullable) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_VALUE), line, tokenizerValues.currentTokenIndex()));
            } else {
                return intvalue;
            }
        }
        try {
            intvalue = Integer.parseInt(value);
        } catch (NumberFormatException | NullPointerException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_INVALID_INT_VALUE), line, tokenizerValues.currentTokenIndex(), value));

        }
        return intvalue;
    }

    public long verifieLong(TokenizerValues tokenizerValues, long line, boolean nullable, ErrorsReport errorsReport) throws BusinessException {
        long longValue = EMPTY_INT_VALUE;
        String value = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(value)) {
            if (nullable) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_VALUE), line, tokenizerValues.currentTokenIndex()));
            } else {
                return longValue;
            }
        }
        try {
            longValue = Long.parseLong(value);
        } catch (NumberFormatException | NullPointerException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_INVALID_INT_VALUE), line, tokenizerValues.currentTokenIndex(), value));

        }
        return longValue;
    }

    public float verifieFloat(TokenizerValues tokenizerValues, long line, boolean nullable, ErrorsReport errorsReport) throws BusinessException {
        float floatValue = EMPTY_FLOAT_VALUE;
        String value = tokenizerValues.nextToken();
        if (Strings.isNullOrEmpty(value)) {
            if (nullable) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_MISSING_VALUE), line, tokenizerValues.currentTokenIndex()));
            } else {
                return floatValue;
            }
        }
        try {
            floatValue = Float.parseFloat(value);
        } catch (NumberFormatException | NullPointerException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(REFDATAS_BUNDLE_SOURCE_PATH, PROPERTY_MSG_INVALID_FLOAT_VALUE), line, tokenizerValues.currentTokenIndex(), value));

        }
        return floatValue;
    }

    /**
     * @param systemeProjection
     * @param latitude
     * @param longitude
     * @param geolocalisation
     * @param dbParcelleElementaire
     * @param ancienneGeolocZone
     * @param errorsReport
     * @param ligne
     * @param colonne
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     */
    public Geolocalisation traiteGeolocalisation(String systemeProjection, String latitude, String longitude, Geolocalisation geolocalisation,
            P dbZone, Geolocalisation ancienneGeolocZone, ErrorsReport errorsReport, long ligne, int colonne) throws BusinessException, PersistenceException {
        if (systemeProjection != null && latitude != null && longitude != null) {
            if (dbZone != null) {

                geolocalisation = createAndSaveGeolocalisation(geolocalisation, dbZone, ancienneGeolocZone,
                        errorsReport, ligne, colonne,
                        systemeProjection, latitude, longitude);
            } else {
                SystemeProjection systProjection = systemeProjectionDAO.getByNKey(systemeProjection).orElse(null);
                if (systProjection == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "SYSTEMPROJECTION_NONDEFINI"), ligne, colonne, systemeProjection));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

                geolocalisation = new Geolocalisation(systProjection, latitude, longitude);
                geolocalisationDAO.saveOrUpdate(geolocalisation);
            }
        }

        return geolocalisation;
    }

    /**
     * @param geolocalisation
     * @param dbZone
     * @param geolocDbZone
     * @param errorsReport
     * @param systemeProjection
     * @param colonne
     * @param latitude
     * @param longitude
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     */
    public Geolocalisation createAndSaveGeolocalisation(Geolocalisation geolocalisation, P dbZone, Geolocalisation geolocDbZone,
            ErrorsReport errorsReport, long ligne, int colonne,
            String systemeProjection, String latitude, String longitude) throws BusinessException,
            PersistenceException {
        // on peut avoir en base des géolocalisation identiques pour des
        // éléments diférents (dispositif, PE, placette, lieu). Par
        // exemple, si on prend comme point de géolocalisation le milieu d'un
        // élément, le point de géolocalisation d'une placette elle-même située
        // au milieu d'une PE pourra être identique à celui de cette PE.

        // Par contre, 2 placettes différentes (ou 2 dispositifs, ou 2 PEs, ..
        // différents) ne pourront être au même endroit
        // et avoir un même point de géolocalisation
        // --> message d'erreur.
        // Vérification si le système de projection existe
        SystemeProjection systProjection = systemeProjectionDAO.getByNKey(systemeProjection).orElse(null);
        if (systProjection == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "SYSTEMPROJECTION_NONDEFINI"), ligne, colonne, systemeProjection));
            throw new BusinessException(errorsReport.getErrorsMessages());
        }

        // Récupération de la géolocalisation en base
        List<Geolocalisation> lstDbGeolocalisation = geolocalisationDAO.getBySystProjectLatitudeLongitude(systemeProjection, latitude, longitude);

        if (lstDbGeolocalisation != null && !lstDbGeolocalisation.isEmpty()) // la géolocalisation existe déjà en base
        {
            // vérifie si au moins une géolocalisation de la liste est déjà
            // associée à une autre zone
            // ou à la zone sur laquelle doit s'effectuer une mise-à-jour de la
            // géolocalisation
            int i = 0;
            do {
                geolocalisation = verifLocalisationExiste(lstDbGeolocalisation.get(i), dbZone, errorsReport);
                i++;
            } while (geolocalisation == null && i < lstDbGeolocalisation.size());

            // la géolocalisation existe en base mais elle est associée à
            // d'autres éléments (dispositif, PE ou Lieu) que la zone concernée par la mise-à-jour
            // On crée la géolocalisation pour la zone
            if (geolocalisation == null) {
                geolocalisation = new Geolocalisation(systProjection, latitude, longitude);
                geolocalisationDAO.saveOrUpdate(geolocalisation);
            }
        } else // c'est une nouvelle géolocalisation qui n'existe pas encore en base
        {
            // on crée la géolocalisation ou on la remet-à-jour en base (elle
            // sera ensuite associée à la zone)
            geolocalisation = geolocDbZone;
            // la zone à modifier n'avait pas de géolocalisation, donc créer la
            // géolocalisation et la sauvegarder en base
            if (geolocalisation == null) {
                geolocalisation = new Geolocalisation(systProjection, latitude, longitude);
                geolocalisationDAO.saveOrUpdate(geolocalisation);
            } else // on met-à-jour la géolocalisation
            {
                if (systemeProjection != null && latitude != null && longitude != null) {
                    geolocalisation.setSystemeProjection(systProjection);
                    geolocalisation.setLatitude(latitude);
                    geolocalisation.setLongitude(longitude);

                    geolocalisationDAO.saveOrUpdate(geolocalisation);
                } else if (systemeProjection == null || latitude == null || longitude == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "GEOLOC_INCORRECTE")));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            }
        }

        return geolocalisation;
    }

    @Override
    public abstract void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException;

    @Override
    protected abstract List<P> getAllElements() throws BusinessException;

    @Override
    public abstract void processRecord(CSVParser parser, File file, String encoding) throws BusinessException;

    /**
     * @param geolocalisationDAO the geolocalisationDAO to set
     */
    public void setGeolocalisationDAO(IGeolocalisationDAO geolocalisationDAO) {
        this.geolocalisationDAO = geolocalisationDAO;
    }

    /**
     * @param systemeProjectionDAO the systemeProjectionDAO to set
     */
    public void setSystemeProjectionDAO(ISystemeProjectionDAO systemeProjectionDAO) {
        this.systemeProjectionDAO = systemeProjectionDAO;
    }

    // définie dans les classes qui héritent de AbstractGenericRecorder
    /**
     *
     * @param dbGeolocalisation
     * @param dbZone
     * @param errorsReport
     * @return
     * @throws BusinessException
     */
    public Geolocalisation verifLocalisationExiste(Geolocalisation dbGeolocalisation, P dbZone, ErrorsReport errorsReport) throws BusinessException{
        return null;
    }

}
