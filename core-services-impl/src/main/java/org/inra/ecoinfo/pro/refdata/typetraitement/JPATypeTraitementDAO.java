/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typetraitement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeTraitementDAO extends AbstractJPADAO<TypeTraitement> implements ITypeTraitementDAO {
    
    @Override
    public List<TypeTraitement> getAll(){
        return getAllBy(TypeTraitement.class, TypeTraitement::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typetraitement.ITypeTraitementDAO#getByLibelle (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    @SuppressWarnings("unchecked")
    public Optional<TypeTraitement> getByNKey(String libelle) {
        CriteriaQuery<TypeTraitement> query = builder.createQuery(TypeTraitement.class);
        Root<TypeTraitement> typeTraitement = query.from(TypeTraitement.class);
        query
                .select(typeTraitement)
                .where(
                        builder.equal(typeTraitement.get(TypeTraitement_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.typetraitement.ITypeTraitementDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeTraitement> getByCode(String code) {
        CriteriaQuery<TypeTraitement> query = builder.createQuery(TypeTraitement.class);
        Root<TypeTraitement> typeTraitement = query.from(TypeTraitement.class);
        query
                .select(typeTraitement)
                .where(
                        builder.equal(typeTraitement.get(TypeTraitement_.code), code)
                );
        return getOptional(query);
    }

}
