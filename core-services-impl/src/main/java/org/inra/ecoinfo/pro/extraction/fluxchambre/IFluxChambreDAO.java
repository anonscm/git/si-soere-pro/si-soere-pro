/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre;

import java.util.List;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.IntervalDate;


/**
 *
 * @author vjkoyao
 */
public interface IFluxChambreDAO {

    /**
     *
     * @param dispositifs
     * @param intervals
     * @param user
     * @return
     */
    List<NodeDataSet> getAvailablesVariablesByDispositif(List<Dispositif> dispositifs, IUser user);

    /**
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user
     * @return
     */
    List<MesureFluxChambres> extractFluxChambre(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user);

    /**
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user
     * @return
     */
    Long sizeFluxChambre(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user);

    /**
     *
     * @param currentUser
     * @return
     */
    public List<Dispositif> getAvailableDispositif(IUser currentUser);
    
}
