/**
 *
 */
package org.inra.ecoinfo.pro.refdata.statutplassocieplacette;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.statutplacette.StatutPlacette;

/**
 * @author sophie
 *
 */
public interface IStatutPlAssociesPlacetteDAO extends IDAO<StatutPlacettesAssociesPlacette> {

    /**
     *
     * @param placette
     * @param statutPlacette
     * @return
     */
    Optional<StatutPlacettesAssociesPlacette> getByNKey(Placette placette, StatutPlacette statutPlacette);

}
