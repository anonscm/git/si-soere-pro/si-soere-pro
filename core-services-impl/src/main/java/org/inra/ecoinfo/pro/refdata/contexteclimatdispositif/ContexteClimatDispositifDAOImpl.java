/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contexteclimatdispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.contextclimatiquedispositif.ContexteClimatiqueDispositif;
import org.inra.ecoinfo.pro.refdata.contextclimatiquedispositif.ContexteClimatiqueDispositif_;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author adiankha
 */
public class ContexteClimatDispositifDAOImpl extends AbstractJPADAO<ContexteClimatiqueDispositif> implements IContexteClimatDispositifDAO {

    /**
     *
     * @param dispositif
     * @param precipitation
     * @return
     */
    @Override
    public Optional<ContexteClimatiqueDispositif> getByNKey(Dispositif dispositif, double precipitation) {
        CriteriaQuery<ContexteClimatiqueDispositif> query = builder.createQuery(ContexteClimatiqueDispositif.class);
        Root<ContexteClimatiqueDispositif> contexteClimatiqueDispositif = query.from(ContexteClimatiqueDispositif.class);
        Join<ContexteClimatiqueDispositif, Dispositif> disp = contexteClimatiqueDispositif.join(ContexteClimatiqueDispositif_.dispositif);
        query
                .select(contexteClimatiqueDispositif)
                .where(
                        builder.equal(contexteClimatiqueDispositif.get(ContexteClimatiqueDispositif_.ccd_precipitation), precipitation),
                        builder.equal(disp, dispositif)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<ContexteClimatiqueDispositif> getAll() {
        return getAllBy(ContexteClimatiqueDispositif.class, ContexteClimatiqueDispositif_.dispositif);
    }
}
