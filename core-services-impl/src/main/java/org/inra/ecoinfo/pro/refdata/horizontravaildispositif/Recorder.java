/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizontravaildispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.horizontravailledispositif.HorizonTravailDispositif;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.ITeneurCalcaireInitialDAO;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.Teneurcalcaireinitial;
import org.inra.ecoinfo.pro.refdata.texturesol.ITextureSolDAO;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<HorizonTravailDispositif> {

    private static final String PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE = "PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE";
    private static final String PROPERTY_MSG_HTD_TENEUR_SOL = "PROPERTY_MSG_HTD_TENEUR_SOL";
    private static final String PROPERTY_MSG_TEXTURE = "PROPERTY_MSG_TEXTURE";
    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    IHorizonTravailDispositifDAO htdDAO;
    IDispositifDAO dispositifDAO;
    ITextureSolDAO textureDAO;
    ITeneurCalcaireInitialDAO tcinitialDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;

    private String[] listeDispositifsPossibles;
    private Map<String, String[]> listeLieuPossibles;
    private String[] listeTeneurPossibles;
    private Map<String, String[]> listeTextureSolPossibles;
    private String[] listeBooleansPossible;
    Properties CommentEn;

    private void createHTD(final HorizonTravailDispositif htd) throws BusinessException {
        try {
            htdDAO.saveOrUpdate(htd);
            htdDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create HorizonTravailDispositif");
        }
    }

    private void updateHTD(double ph, double horizon, double teneur, double percent, String grossiers, String comment,
            final HorizonTravailDispositif dbhtd, Dispositif dispositif, Teneurcalcaireinitial teneurca) throws BusinessException {
        try {
            dbhtd.setPhinitial(ph);
            dbhtd.setHorizon_surface(horizon);
            dbhtd.setTeneurcarbone(teneur);
            dbhtd.setPourcentcailloux(percent);
            dbhtd.setElementgrossiers(grossiers);
            dbhtd.setCommentaire(comment);
            dbhtd.setDispositif(dispositif);
            dbhtd.setTeneur(teneurca);
            htdDAO.saveOrUpdate(dbhtd);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update HorizonTravailDispositif");
        }
    }

    private void createOrUpdateHTD(double ph, double horizon, double teneur, double percent, String grossiers, String comment,
            final HorizonTravailDispositif dbhtd, Dispositif dispositif, Teneurcalcaireinitial teneurca) throws BusinessException {
        if (dbhtd == null) {
            HorizonTravailDispositif htd = new HorizonTravailDispositif(ph, horizon, percent, teneur, grossiers, comment, dispositif, teneurca);
            htd.setPhinitial(ph);
            htd.setHorizon_surface(horizon);
            htd.setTeneurcarbone(teneur);
            htd.setPourcentcailloux(percent);
            htd.setElementgrossiers(grossiers);
            htd.setCommentaire(comment);
            htd.setDispositif(dispositif);
            htd.setTeneur(teneurca);
            createHTD(htd);
        } else {
            updateHTD(ph, horizon, teneur, percent, grossiers, comment, dbhtd, dispositif, teneurca);
        }
    }

    private void persistHTD(double ph, double horizon, double teneur, double percent, String grossiers, String comment, Dispositif dispositif,
            Teneurcalcaireinitial teneurcalcaireinitial)
            throws BusinessException, BusinessException {
        final HorizonTravailDispositif dbhtd = htdDAO.getByNKey(dispositif, teneurcalcaireinitial, teneur).orElse(null);
        createOrUpdateHTD(ph, horizon, teneur, percent, grossiers, comment, dbhtd, dispositif, teneurcalcaireinitial);

    }

    private void initDispositifPossibles() {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCodeDispo();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifsPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void initTextureSolPossibles() {
        Map<String, String[]> texturesolteneur = new HashMap<>();
        Map<String, Set<String>> textureteneurList = new HashMap<>();
        List<Teneurcalcaireinitial> groupetexture = tcinitialDAO.getAll(Teneurcalcaireinitial.class);
        groupetexture.forEach((teneur) -> {
            String teneurcalcaireinitial = teneur.getNom();
            String texture = teneur.getTexturesol().getNom();
            if (!textureteneurList.containsKey(teneurcalcaireinitial)) {
                textureteneurList.put(teneurcalcaireinitial, new TreeSet());
            }
            textureteneurList.get(teneurcalcaireinitial).add(texture);
        });
        textureteneurList.entrySet().forEach((entryProduit) -> {
            texturesolteneur.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeTextureSolPossibles = texturesolteneur;
        listeTeneurPossibles = texturesolteneur.keySet().toArray(new String[]{});
    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, HorizonTravailDispositif.NAME_ENTITY_JPA);
                String dispo = values[0];
                String lieu = values[1];
                Dispositif dbdispo = dispositifDAO.getByNKey(dispo, lieu).orElse(null);
                double teneurCarboneOrganique = Double.parseDouble(values[6]);
                String teneurCalcairetotal = Utils.createCodeFromString(values[3]);
                String libelleTextureSol = Utils.createCodeFromString(values[4]);
                Texturesol dbTexture = textureDAO.getByNKey(libelleTextureSol)
                        .orElseThrow(() -> new BusinessException("can't get  texture"));
                Teneurcalcaireinitial teneurcalcaireinitial = tcinitialDAO.getByNKey(teneurCalcairetotal, dbTexture).orElse(null);
                HorizonTravailDispositif dbpedologie = htdDAO.getByNKey(dbdispo, teneurcalcaireinitial, teneurCarboneOrganique)
                        .orElseThrow(() -> new BusinessException("can't get  HorizonTravailDispositif"));
                htdDAO.remove(dbpedologie);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<HorizonTravailDispositif> getAllElements() throws BusinessException {
        return htdDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, HorizonTravailDispositif.NAME_ENTITY_JPA);
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String code = tokenizerValues.nextToken();
                int indexlieu = tokenizerValues.currentTokenIndex();
                final String lieu = tokenizerValues.nextToken();
                double horizonc = verifieDouble(tokenizerValues, line, true, errorsReport);
                int indexteneur = tokenizerValues.currentTokenIndex();
                final String teneurcal = Utils.createCodeFromString(tokenizerValues.nextToken());
                int indextexture = tokenizerValues.currentTokenIndex();
                final String libelleTextureSol = Utils.createCodeFromString(tokenizerValues.nextToken());
                double phc = verifieDouble(tokenizerValues, line, false, errorsReport);
                double teneurc = verifieDouble(tokenizerValues, line, true, errorsReport);
                final String grossiers = tokenizerValues.nextToken();
                double persentc = verifieDouble(tokenizerValues, line, false, errorsReport);
                final String comment = tokenizerValues.nextToken();

                Dispositif dbdispo = dispositifDAO.getByNKey(code, lieu).orElse(null);
                if (dbdispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE), line, indexdispo, code, indexlieu, lieu));
                }
                Texturesol dbTexture = textureDAO.getByNKey(libelleTextureSol).orElse(null);
                if (dbTexture == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_TEXTURE), line, indextexture, libelleTextureSol));
                }
                Teneurcalcaireinitial dbteneur = tcinitialDAO.getByNKey(teneurcal, dbTexture).orElse(null);
                if (dbteneur == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_HTD_TENEUR_SOL), line, indextexture, libelleTextureSol, indexteneur, lieu));
                }
                if (!errorsReport.hasErrors()) {
                    persistHTD(phc, horizonc, teneurc, persentc, grossiers, comment, dbdispo, dbteneur);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(HorizonTravailDispositif htd) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String valeurProduit = htd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getDispositif() != null ? htd.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifsPossibles, null, true, false, true);
        String valeurProcede = htd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getDispositif() != null ? htd.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(htd == null || htd.getHorizon_surface() == EMPTY_DOUBLE_VALUE
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getHorizon_surface(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        String valeurTeneur = htd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getTeneur() != null ? htd.getTeneur().getNom() : "";
        ColumnModelGridMetadata columnTeneur = new ColumnModelGridMetadata(valeurTeneur, listeTeneurPossibles, null, false, false, true);
        String valeurTexture = htd == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getTeneur() != null ? htd.getTeneur().getTexturesol().getNom() : "";
        ColumnModelGridMetadata columnTexture = new ColumnModelGridMetadata(valeurTexture, listeTextureSolPossibles, null, false, false, true);
        List<ColumnModelGridMetadata> refTeneur = new LinkedList<ColumnModelGridMetadata>();
        refTeneur.add(columnTexture);
        columnTexture.setValue(valeurTexture);
        columnTeneur.setRefs(refTeneur);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTeneur);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTexture);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(htd == null || htd.getPhinitial() == EMPTY_DOUBLE_VALUE ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : htd.getPhinitial(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(htd == null || htd.getTeneurcarbone() == EMPTY_DOUBLE_VALUE
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getTeneurcarbone(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(htd == null || htd.getElementgrossiers() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getElementgrossiers() != null
                        ? htd.getElementgrossiers() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(htd == null || htd.getPourcentcailloux() == EMPTY_DOUBLE_VALUE
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getPourcentcailloux(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(htd == null || htd.getCommentaire() == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : htd.getCommentaire(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(htd == null || htd.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : CommentEn.getProperty(htd.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IHorizonTravailDispositifDAO getHtdDAO() {
        return htdDAO;
    }

    /**
     *
     * @param htdDAO
     */
    public void setHtdDAO(IHorizonTravailDispositifDAO htdDAO) {
        this.htdDAO = htdDAO;
    }

    @Override
    protected ModelGridMetadata<HorizonTravailDispositif> initModelGridMetadata() {
        initDispositifPossibles();
        initTextureSolPossibles();
        listeBooleanPossibles();
        CommentEn = localizationManager.newProperties(HorizonTravailDispositif.NAME_ENTITY_JPA, HorizonTravailDispositif.COLUMN_COMMENT_JPA, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @return
     */
    public ITextureSolDAO getTextureDAO() {
        return textureDAO;
    }

    /**
     *
     * @param textureDAO
     */
    public void setTextureDAO(ITextureSolDAO textureDAO) {
        this.textureDAO = textureDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param tcinitialDAO
     */
    public void setTcinitialDAO(ITeneurCalcaireInitialDAO tcinitialDAO) {
        this.tcinitialDAO = tcinitialDAO;
    }

}
