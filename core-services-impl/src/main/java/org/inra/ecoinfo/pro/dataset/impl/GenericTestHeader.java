/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.LocalDateTime;
import javax.persistence.Transient;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class GenericTestHeader implements ITestHeaders {

    protected static final Logger LOGGER = LoggerFactory.getLogger(GenericTestHeader.class);

    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.messages";
    private static final String PROPERTY_MSG_DISPOSITIF_LIEU_BAD_DATASET = "PROPERTY_MSG_DISPOSITIF_LIEU_BAD_DATASET";
    private static final String PROPERTY_MSG_DISPOSITIF_TRAITEMENT_BAD_DATASET = "PROPERTY_MSG_DISPOSITIF_TRAITEMENT_BAD_DATASET";
    private static final String PROPERTY_MSG_DISPOSITIF_BLOC_BAD_DATASET = "PROPERTY_MSG_DISPOSITIF_BLOC_BAD_DATASET";
    private static final String PROPERTY_MSG_DISPOSITIF_BLOC_BD_NULL = "PROPERTY_MSG_DISPOSITIF_BLOC_BD_NULL";
    private static final String PROPERTY_MSG_DISPOSITIF_BD_NULL = "PROPERTY_MSG_DISPOSITIF_BLOC_BD_NULL";
    private static final String PROPERTY_MSG_DISPOSITIF_TRAITEMENT_BD_NULL = "PROPERTY_MSG_DISPOSITIF_TRAITEMENT_BD_NULL";
    private static final String PROPERTY_MSG_BLOC_PARCELLE_BAD_DATASET = "PROPERTY_MSG_BLOC_PARCELLE_BAD_DATASET";
    private static final String PROPERTY_MSG_PARCELLE_PLACETTE_BAD_DATASET = "PROPERTY_MSG_PARCELLE_PLACETTE_BAD_DATASET";
    public static final String KERNEL_BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.dataset.impl.messages";
    protected static final String PROPERTY_MSG_MISMATCH_COLUMN_HEADER = "PROPERTY_MSG_MISMATCH_COLUMN_HEADER";
    final ErrorsReport errorsReport = new ErrorsReport();

    @Transient
    ILocalizationManager localizationManager;

    @Transient
    IDatasetConfiguration datasetConfiguration;
    @Transient
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;

    @Transient
    IDispositifDAO dispositifDAO;

    @Transient
    IDescriptionTraitementDAO descriptionTraitementDAO;

    @Transient
    IPlacetteDAO placetteDAO;

    @Transient
    IBlocDAO blocDAO;

    @Transient
    IDatatypeDAO datatypeDAO;

    @Transient
    IParcelleElementaireDAO parcelleElementaireDAO;

    @Transient
    ILieuDAO lieuDAO;

    final protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public GenericTestHeader() {
        super();
    }

    private String buildDownloadFilename(VersionFile version) {

        return version.getDataset().buildDownloadFilename(this.datasetConfiguration);

    }

    public Boolean estLigneVide(String[] values) {

        for (final String string : values) {

            if (!string.equals(org.apache.commons.lang.StringUtils.EMPTY)) {

                return false;

            }

        }

        return true;

    }

    protected Dispositif getDispositif(String codeDisp) {
        String nomDispositif = Dispositif.getNomDispositif(codeDisp);
        String nomLieu = Dispositif.getNomLieu(codeDisp);
        Dispositif dispositif = null;
        final String code = codeDisp;
        dispositif = this.dispositifDAO.getByNKey(nomDispositif, nomLieu).orElse(null);
        if (dispositif == null) {

            return null;

        }
        return dispositif;
    }

    protected long jumpLines(final CSVParser parser, final long lineNumber,
            final int numberOfJumpedLines) throws IOException {

        long finalLineNumber = lineNumber;

        for (int j = 0; j < numberOfJumpedLines; j++) {

            int line = Math.max(0, parser.getLastLineNumber());

            parser.getLine();

            finalLineNumber += parser.getLastLineNumber() - line;

        }

        return finalLineNumber;

    }

    protected long readBeginAndEndDates(final VersionFile version,
            final BadsFormatsReport badsFormatsReport, final CSVParser parser,
            final long lineNumber, final ISessionPropertiesPRO sessionProperties)
            throws IOException, BusinessException {

        long returnLineNumber = this.readBeginDate(version, badsFormatsReport, parser, lineNumber, sessionProperties);
        returnLineNumber = this.readEndDate(version, badsFormatsReport, parser, returnLineNumber, sessionProperties);
        this.testIntervalDate(badsFormatsReport, returnLineNumber, sessionProperties);
        if (sessionProperties.getDateDeDebut() != null && sessionProperties.getDateDeFin() != null) {

            this.testAreConsistentDates(badsFormatsReport, version, sessionProperties,
                    returnLineNumber);

        }
        return returnLineNumber;

    }

    protected long readBeginDate(final VersionFile versionFile,
            final BadsFormatsReport badsFormatsReport, final CSVParser parser,
            final long lineNumber, final ISessionPropertiesPRO sessionProperties)
            throws IOException, BusinessException {

        long returnLineNumber = lineNumber;

        try {

            String[] line = parser.getLine();

            final String date = line[1];

            String time = line[2];

            if (Strings.isNullOrEmpty(time)) {

                time = "00:00:00";

            }

            returnLineNumber++;

            if (!date.matches(RecorderPRO.PATTERN_DATE)
                    || !time.matches(RecorderPRO.PATTERN_TIME_SECONDE)) {

                if (!date.matches(RecorderPRO.PATTERN_DATE)) {

                    badsFormatsReport.addException(new BadExpectedValueException(String.format(
                            RecorderPRO
                                    .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_BEGIN_DATE),
                            date, returnLineNumber, 1, RecorderPRO.PROPERTY_CST_FRENCH_DATE)));

                    sessionProperties.setDateDeDebut(null);

                }

                if (!time.matches(RecorderPRO.PATTERN_TIME_SECONDE)) {

                    badsFormatsReport.addException(new BadExpectedValueException(String.format(
                            RecorderPRO
                                    .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_BEGIN_TIME),
                            time, returnLineNumber, 2, RecorderPRO.PROPERTY_CST_FRENCH_TIME)));

                    sessionProperties.setDateDeDebut(null);

                }

            } else {
                try {
                    sessionProperties.setDateDeDebut(RecorderPRO
                            .getDateTimeLocalFromDateStringaAndTimeString(date, time));
                } catch (final BusinessException e) {

                    badsFormatsReport.addException(new BadExpectedValueException(e.getMessage()));

                    sessionProperties.setDateDeDebut(null);

                }
            }
        } catch (final ArrayIndexOutOfBoundsException e) {

            returnLineNumber++;

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_BEGIN_DATE),
                    returnLineNumber, 1)));

        }

        return returnLineNumber;

    }

    protected long readDatatype(final BadsFormatsReport badsFormatsReport, final CSVParser parser,
            final long lineNumber, final String datatypeName) throws IOException {

        long finalLineNumber = lineNumber;

        String[] values;

        values = parser.getLine();

        finalLineNumber++;

        String dataType = null;

        try {

            if (values != null && values.length > 1) {

                dataType = Utils.createCodeFromString(values[1]);

            } else {

                throw new IndexOutOfBoundsException();

            }

            if (dataType == null || !dataType.equalsIgnoreCase(datatypeName)) {

                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATATYPE),
                        finalLineNumber, 2, datatypeName)));

            }

        } catch (final IndexOutOfBoundsException e1) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_DATATYPE),
                    finalLineNumber, 2, datatypeName)));

        }

        return finalLineNumber;

    }

    protected long readCommentaire(final CSVParser parser, final long lineNumber,
            final ISessionPropertiesPRO sessionProperties) throws IOException {

        long finalLineNumber = lineNumber;

        String[] values;

        values = parser.getLine();

        finalLineNumber++;

        try {

            sessionProperties.setCommentaire(values[1]);

        } catch (final Exception e) {

            sessionProperties.setCommentaire(org.apache.commons.lang.StringUtils.EMPTY);

        }

        return finalLineNumber;

    }

    protected long readEndDate(final VersionFile versionFile,
            final BadsFormatsReport badsFormatsReport, final CSVParser parser,
            final long lineNumber, final ISessionPropertiesPRO sessionProperties)
            throws IOException {

        long finalLineNumber = lineNumber;

        try {

            String[] line = parser.getLine();

            final String date = line[1];

            String time = line[2];

            if (Strings.isNullOrEmpty(time)) {

                time = "00:00:00";

            }

            finalLineNumber++;

            if (!date.matches(RecorderPRO.PATTERN_DATE)
                    || !time.matches(RecorderPRO.PATTERN_TIME_SECONDE)) {

                if (!date.matches(RecorderPRO.PATTERN_DATE)) {

                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderPRO
                                                    .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_END_DATE),
                                            date, finalLineNumber, 1,
                                            RecorderPRO.PROPERTY_CST_FRENCH_DATE)));

                    sessionProperties.setDateDeFin(null);

                }

                if (!time.matches(RecorderPRO.PATTERN_TIME_SECONDE)) {

                    badsFormatsReport
                            .addException(new BadExpectedValueException(
                                    String.format(
                                            RecorderPRO
                                                    .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_END_TIME),
                                            time, finalLineNumber, 1,
                                            RecorderPRO.PROPERTY_CST_FRENCH_TIME)));

                    sessionProperties.setDateDeFin(null);

                }

            } else {

                try {

                    sessionProperties.setDateDeFin(RecorderPRO
                            .getDateTimeLocalFromDateStringaAndTimeString(date, time));

                } catch (final BusinessException e) {

                    badsFormatsReport.addException(new BadExpectedValueException(e.getMessage()));

                    sessionProperties.setDateDeFin(null);

                }

            }

        } catch (final ArrayIndexOutOfBoundsException e) {

            finalLineNumber++;

            badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderPRO
                    .getPROMessage(RecorderPRO
                            .getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_END_DATE)),
                    finalLineNumber, 1)));

        }

        return finalLineNumber;

    }

    protected long readEmptyLine(final BadsFormatsReport badsFormatsReport, final CSVParser parser,
            final long lineNumber) throws IOException {

        long finalLineNumber = lineNumber;
        String[] values;
        values = parser.getLine();
        finalLineNumber++;
        if (!this.estLigneVide(values)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_EMPTY_LINE),
                    finalLineNumber)));
        }
        return finalLineNumber;

    }

    protected long readLineHeader(final BadsFormatsReport badsFormatsReport,
            final CSVParser parser, final long lineNumber,
            final DatasetDescriptor datasetDescriptor,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws IOException {

        final long finalLineNumber = lineNumber + 1;

        String[] values;

        int index;

        values = parser.getLine();

        String value;

        for (index = 0; index < values.length; index++) {

            if (index > datasetDescriptor.getColumns().size() - 1) {

                break;

            }

            value = values[index].trim();

            final Column column = datasetDescriptor.getColumns().get(index);

            final String columnName = column.getName();

            if (!columnName.trim().equalsIgnoreCase(value)) {

                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderPRO
                                .getPROMessage(RecorderPRO.PROPERTY_MSG_MISMACH_COLUMN_HEADER),
                        finalLineNumber, index + 1, value, columnName.trim().toLowerCase())));

            }

        }

        return finalLineNumber;

    }

    long readDispositif(final BadsFormatsReport badsFormatsReport, final long lineNumber,
            final String[] values, final ISessionPropertiesPRO sessionProperties)
            throws BusinessException {
        long finalLineNumber = lineNumber;
        if (values == null) {

            throw new BusinessException(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_EMPTY_FILE));

        }
        finalLineNumber++;
        try {
            final String dispCode = values[1];
            sessionProperties.setDispositif(this.getDispositif(dispCode));
            if (sessionProperties.getDispositif() == null) {

                badsFormatsReport.addException(new BadExpectedValueException(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DISPOSITIF),
                        dispCode, finalLineNumber, 2)));

            }
        } catch (final ArrayIndexOutOfBoundsException e) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_DISPOSITIF),
                    finalLineNumber, 2)));

        }
        return finalLineNumber;
    }

    protected long readDispositif(final VersionFile version, final BadsFormatsReport badsFormatsReport,
            final CSVParser parser, final long lineNumber,
            final ISessionPropertiesPRO sessionProperties) throws IOException, BusinessException {

        final String[] values = parser.getLine();
        long line = this.readDispositif(badsFormatsReport, lineNumber, values, sessionProperties);
        this.testIsConsistentDispositif(version, badsFormatsReport, lineNumber, sessionProperties);
        return line;

    }

    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor) throws BusinessException {
        try {

            final byte[] datasSanitized = this.sanitizeData(versionFile, LOGGER,
                    badsFormatsReport);

        } catch (final BadDelimiterException | BadFormatException e) {

            badsFormatsReport.addException(e);

        }
        return 0;
    }

    protected DescriptionTraitement getTraitement(String codeTrait, String codeDispositifLieu) {
        String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
        String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
        DescriptionTraitement dbtraitement = null;
        if (codeTrait == null || codeDispositif == null) {
            return null;
        } else {
            Dispositif dbdispositif = null;
            dbdispositif = dispositifDAO.getByNKey(nomLieu, nomLieu).orElse(null);
            if (dbdispositif == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_DISPOSITIF_TRAITEMENT_BAD_DATASET), codeDispositif));
            } else {
                dbtraitement = this.descriptionTraitementDAO.getByNKey(codeTrait, dbdispositif).orElse(null);
            }
            if (dbtraitement == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_DISPOSITIF_TRAITEMENT_BD_NULL), codeDispositif));
            }
            return dbtraitement;
        }
    }

    protected Bloc getBloc(String nomBloc, String codeDispositifLieu) throws PersistenceException {
        String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
        String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

        if (nomBloc == null || codeDispositif == null) {
            return null;
        } else {
            Dispositif dbdispositif = null;
            dbdispositif = dispositifDAO.getByNKey(codeDispositif, nomLieu).orElse(null);
            if (dbdispositif == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_DISPOSITIF_BLOC_BAD_DATASET), codeDispositif));
            }
            Bloc dbBloc = this.blocDAO.getByNKey(nomBloc, dbdispositif).orElse(null);
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_DISPOSITIF_BLOC_BD_NULL), nomBloc));
            return dbBloc;
        }

    }

    protected ParcelleElementaire getParcelle(final Bloc bloc, String nomParcelle) throws PersistenceException {

        if (nomParcelle == null || bloc == null) {
            return null;
        } else {
            final String codeParcelle = Utils.createCodeFromString(nomParcelle);
            ParcelleElementaire dbParcelle = this.parcelleElementaireDAO.getByNKey(codeParcelle, bloc).orElse(null);
            if (dbParcelle == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_BLOC_PARCELLE_BAD_DATASET), nomParcelle));
            }
            return dbParcelle;
        }
    }

    protected Placette getPlacette(String codeDispositifLieu, String nomBloc, String nomParcelle, String nomPlcette) throws PersistenceException {
        String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
        String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
        if (nomParcelle == null || nomBloc == null || codeDispositif == null) {
            return null;
        } else {
            Dispositif dbDispositif = dispositifDAO.getByNKey(codeDispositif, nomLieu).orElse(null);
            if (dbDispositif == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_DISPOSITIF_BLOC_BAD_DATASET), codeDispositif));
            }
            Bloc dbBloc = blocDAO.getByNKey(nomBloc, dbDispositif).orElse(null);
            if (dbBloc == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_DISPOSITIF_BLOC_BD_NULL), nomBloc, codeDispositif));
            }
            ParcelleElementaire dbParcelle = this.parcelleElementaireDAO.getByNKey(nomBloc, dbBloc).orElse(null);
            if (dbParcelle == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_BLOC_PARCELLE_BAD_DATASET), nomBloc, nomParcelle));
            }
            Placette dbplacette = this.placetteDAO.getByNameAndParcelle(nomPlcette, dbParcelle).orElse(null);
            if (dbplacette == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(GenericTestHeader.BUNDLE_SOURCE_PATH, GenericTestHeader.PROPERTY_MSG_PARCELLE_PLACETTE_BAD_DATASET), nomPlcette, nomParcelle));
            }
            return dbplacette;
        }
    }

    private byte[] sanitizeData(VersionFile versionFile, Logger logger, BadsFormatsReport badsFormatsReport) throws BadFormatException {
        byte[] datasSanitized = null;
        try {

            datasSanitized = Utils.sanitizeData(versionFile.getData(), this.localizationManager);

        } catch (final BusinessException e) {

            logger.debug("can't sanitize", e);

            badsFormatsReport.addException(e);

            throw new BadFormatException(badsFormatsReport);

        }
        return datasSanitized;

    }

    private void updateMinMaxDatatypeVariableQualifiantMethode(CSVParser csvParser, DatasetDescriptorPRO datasetDescriptor) {
        throw new UnsupportedOperationException("Not supported yet. Update Min Max"); //To change body of generated methods, choose Tools | Templates.
    }

    private void testIsConsistentDispositif(VersionFile version, BadsFormatsReport badsFormatsReport, long lineNumber, ISessionPropertiesPRO sessionProperties) {
        if (sessionProperties.getDispositif() == null) {

            return;

        }
        Dispositif dispositif = (Dispositif) version.getDataset().getRealNode().getNodeByNodeableTypeResource(Dispositif.class).getNodeable();
        if (!sessionProperties.getDispositif().equals(dispositif)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_IMPROPER_DISPOSITIF),
                    this.localizationManager.newProperties(Nodeable.getLocalisationEntite(Dispositif.class),
                            Nodeable.ENTITE_COLUMN_NAME).getProperty(
                                    sessionProperties.getDispositif().getCode(),
                                    sessionProperties.getDispositif().getCode()), lineNumber, 1, version
                    .getDataset().buildDownloadFilename(this.datasetConfiguration))));
        }
    }

    private void testIntervalDate(final BadsFormatsReport badsFormatsReport,
            final long lineNumber, final ISessionPropertiesPRO sessionProperties) {

        if (sessionProperties.getDateDeDebut() != null
                && sessionProperties.getDateDeFin() != null
                && sessionProperties.getDateDeDebut().isBefore(sessionProperties.getDateDeFin())) {

            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_INTERVAL_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(sessionProperties.getDateDeDebut(), sessionProperties.getDateFormat()),
                    lineNumber - 1, 1,
                    DateUtil.getUTCDateTextFromLocalDateTime(sessionProperties.getDateDeFin(), sessionProperties.getDateFormat()),
                    lineNumber, 1)));

        }

    }

    private void testAreConsistentDates(BadsFormatsReport badsFormatsReport, VersionFile version,
            ISessionPropertiesPRO sessionProperties, final long lineNumber) {
        LocalDateTime dateDeDebutLocale = null, dateDeFinLocale = null;
        dateDeDebutLocale = sessionProperties.getDateDeDebut();
        dateDeFinLocale = sessionProperties.getDateDeFin();
        if (dateDeDebutLocale == null
                || !version.getDataset().getDateDebutPeriode().equals(dateDeDebutLocale)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_IMPROPER_BEGIN_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(dateDeDebutLocale, DateUtil.DD_MM_YYYY_HH_MM),
                    lineNumber, 1, this.buildDownloadFilename(version))));
        }
        if (dateDeFinLocale == null
                || !version.getDataset().getDateFinPeriode().equals(dateDeFinLocale)) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_IMPROPER_END_DATE),
                    DateUtil.getUTCDateTextFromLocalDateTime(dateDeFinLocale, DateUtil.DD_MM_YYYY_HH_MM),
                    lineNumber, 2, this.buildDownloadFilename(version))));
        }
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void setDatasetConfiguration(IDatasetConfiguration datasetConfiguration) {
        this.datasetConfiguration = datasetConfiguration;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    private void updateMinMaxDatatypeVariableUnite(CSVParser parser,
            DatasetDescriptorPRO datasetDescriptor) {

        String[] headersValues;
        String[] methodeValues;
        String[] categorieValues;

        try {

            final String[][] allValues = parser.getAllValues();

            headersValues = datasetDescriptor.getHeadersLine() - 1 > 0 ? allValues[datasetDescriptor.getHeadersLine() - 1] : null;

        } catch (final IOException e) {
            LOGGER.info("can't parse all values");
            return;

        }
        for (int i = 0; i < headersValues.length; i++) {
            try {
                final String header = headersValues[i];

            } catch (final NumberFormatException e) {
                LOGGER.info("can't parse methode/categorie value");
            }
        }
        for (final DatatypeVariableUnitePRO datatypeUniteVariablePRO : this.dataTypeVariableUnitePRODAO
                .getAllDatatypeVariableUnitePROMethodeandCategorie(datasetDescriptor.getName())
                .values()) {
        }
    }

    private void updateMinMaxWrite(String writeMethode, String dbMethode, String dbCategorie, String writeCategorie,
            DatasetDescriptorPRO datasetDescriptor,
            DatatypeVariableUnitePRO datatypeUniteVariablePRO) {
        throw new UnsupportedOperationException("Not supported yet. Update min/Max"); //To change body of generated methods, choose Tools | Templates.
    }

    ///A completer + Revoir les .properties
    public long readDispAndParcelle(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, ISessionPropertiesPRO sessionProperties) throws BusinessException, IOException {
        long finalLineNumber = lineNumber;
        final String[] values = parser.getLine();
        final long returnLineNumber = this.readDispositif(badsFormatsReport, finalLineNumber, values, sessionProperties);

        if (sessionProperties.getDispositif() == null) {

            finalLineNumber++;
            return finalLineNumber;
        }

        try {

            final String parcelleCode = values[2];

            sessionProperties.setParcelleElementaire(
                    this.getParcelle(sessionProperties.getBloc(), parcelleCode));
            //sessionProperties.setParcelleElementaire(this.getParcelle(sessionProperties.getDispositif().getCodeDispo(), parcelleCode));
            if (sessionProperties.getParcelleElementaire() == null) {
                final String message = String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_PLOT),
                        parcelleCode, returnLineNumber, 3, sessionProperties.getDispositif().getCodeDispo());

                final BadExpectedValueException badFormatException = new BadExpectedValueException(
                        message);

                badsFormatsReport.addException(badFormatException);
            }

        } catch (final PersistenceException | ArrayIndexOutOfBoundsException e) {
            final String message = String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_PLOT),
                    returnLineNumber, 3);

            final BadExpectedValueException badFormatException = new BadExpectedValueException(
                    message);

            badsFormatsReport.addException(badFormatException);
        }

        return returnLineNumber;
    }

}
