/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.historiquedispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.faitremarquable.Faitremarquable;

/**
 *
 * @author adiankha
 */
public class HistoriqueDispositifDAOImpl extends AbstractJPADAO<HistoriqueDispositif> implements IHistoriqueDispositifDAO {

    /**
     *
     * @return
     */
    @Override
    public List<HistoriqueDispositif> getAll() {
        return getAll(HistoriqueDispositif.class);
    }

    /**
     *
     * @param faitremarquable
     * @param dispositif
     * @return
     */
    @Override
    public Optional<HistoriqueDispositif> getByNKey(Faitremarquable faitremarquable, Dispositif dispositif) {
        CriteriaQuery<HistoriqueDispositif> query = builder.createQuery(HistoriqueDispositif.class);
        Root<HistoriqueDispositif> historiqueDispositif = query.from(HistoriqueDispositif.class);
        Join<HistoriqueDispositif, Dispositif> disp = historiqueDispositif.join(HistoriqueDispositif_.dispositif);
        Join<HistoriqueDispositif, Faitremarquable> fr = historiqueDispositif.join(HistoriqueDispositif_.faitremarquable);
        query
                .select(historiqueDispositif)
                .where(
                        builder.equal(disp, dispositif),
                        builder.equal(fr, faitremarquable)
                );
        return getOptional(query);
    }

}
