/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.travaildusol.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol_;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.jpa.JPAITKDAO;
import org.inra.ecoinfo.pro.synthesis.travaildusol.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPATravailDuSolDAO extends JPAITKDAO<MesureTravailDuSol, ValeurTravailDuSol> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTravailDuSol> getValeurITKClass() {
        return ValeurTravailDuSol.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTravailDuSol> getMesureITKClass() {
        return MesureTravailDuSol.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurTravailDuSol, MesureTravailDuSol> getMesureAttribute() {
        return ValeurTravailDuSol_.mesuretravaildusol;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }
}
