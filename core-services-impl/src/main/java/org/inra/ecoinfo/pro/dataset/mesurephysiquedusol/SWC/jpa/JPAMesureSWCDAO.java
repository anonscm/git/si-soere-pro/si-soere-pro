/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC.jpa;

import java.sql.Time;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC.IMesureSWCDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.MesureSWC;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.MesureSWC_;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.SousSequenceSWC;
import static org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.MesureTS_.sousSequenceTS;

/**
 *
 * @author vjkoyao
 */
public class JPAMesureSWCDAO extends AbstractJPADAO<MesureSWC> implements IMesureSWCDAO<MesureSWC> {

    private static String QUERY_GET_BY_KEY = "from MesureSWC m where m.sousSequenceSWC.id=:sousSequenceSWC and m.heure=:heure";

    /**
     *
     * @param heure
     * @param sousSequenceSWC
     * @return
     */
    @Override
    public Optional<MesureSWC> getByNKeys(Time heure, SousSequenceSWC sousSequenceSWC) {
        CriteriaQuery<MesureSWC> query = builder.createQuery(MesureSWC.class);
        Root<MesureSWC> m = query.from(MesureSWC.class);
        Join<MesureSWC, SousSequenceSWC> ss = m.join(MesureSWC_.sousSequenceSWC);
        query
                .select(m)
                .where(
                        builder.equal(ss, sousSequenceTS),
                        builder.equal(m.get(MesureSWC_.heure), heure)
                );
        return getOptional(query);
    }

}
