/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.famille;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<Famille> {

    private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_BAD_CATEGORIE_ITK = "PROPERTY_MSG_BAD_CATEGORIE_ITK";

    /**
     *
     */
    protected IFamilleDAO familleDAO;

    /**
     *
     */
    protected String[] ListeCategoriesPossibles;

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {

            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final Famille dbFamille = familleDAO.getByNKey(intitule)
                        .orElseThrow(() -> new BusinessException("can't get famille"));
                familleDAO.remove(dbFamille);
                values = parser.getLine();

            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();

        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            long line = 0;
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Famille.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);

                if (!errorsReport.hasErrors()) {
                    persistFamille(nom, code);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Famille> getAllElements() throws BusinessException {
        return familleDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Famille famille) throws BusinessException {

        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(famille == null || famille.getFamille_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : famille.getFamille_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true,
                        false, true));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Famille> initModelGridMetadata() {
        return super.initModelGridMetadata();

    }

    private void persistFamille(String famille_nom, String code) throws BusinessException {
        final Famille famille = familleDAO.getByNKey(famille_nom).orElse(null);
        createOrUpdateFamille(famille_nom, code, famille);
    }

    private void createOrUpdateFamille(String nom, String code, Famille famille) throws BusinessException {
        if (famille == null) {
            final Famille familles = new Famille(nom);
            familles.setFamille_nom(nom);
            familles.setFamille_code(code);
            createFamille(familles);

        } else {
            updateFamille(nom, code, famille);
        }
    }

    private void createFamille(Famille familles) throws BusinessException {
        try {
            familleDAO.saveOrUpdate(familles);
            familleDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create famille");
        }
    }

    private void updateFamille(String famille_nom, String code, Famille famille) throws BusinessException {
        try {
            famille.setFamille_nom(famille_nom);
            famille.setFamille_code(code);
            familleDAO.saveOrUpdate(famille);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update famille");
        }
    }

    /**
     *
     * @return
     */
    public IFamilleDAO getFamilleDAO() {
        return familleDAO;
    }

    /**
     *
     * @param familleDAO
     */
    public void setFamilleDAO(IFamilleDAO familleDAO) {
        this.familleDAO = familleDAO;
    }

    /**
     *
     * @return
     */
    public String[] getListeCategoriesPossibles() {
        return ListeCategoriesPossibles;
    }

    /**
     *
     * @param ListeCategoriesPossibles
     */
    public void setListeCategoriesPossibles(String[] ListeCategoriesPossibles) {
        this.ListeCategoriesPossibles = ListeCategoriesPossibles;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
