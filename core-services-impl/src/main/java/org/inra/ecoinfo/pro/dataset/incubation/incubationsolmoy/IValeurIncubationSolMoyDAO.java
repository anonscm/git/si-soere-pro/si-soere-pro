/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;

/**
 *
 * @author vjkoyao
 */
public interface IValeurIncubationSolMoyDAO extends IDAO<ValeurIncubationSolMoy> {

    /**
     *
     * @param realNode
     * @param mesureIncubationSolMoy
     * @param valeur_ecart_type
     * @return
     */
    public Optional<ValeurIncubationSolMoy> getByKeys(RealNode realNode, MesureIncubationSolMoy mesureIncubationSolMoy, float valeur_ecart_type);

}
