/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.impl;

import java.time.LocalDate;

/**
 *
 * @author adiankha
 */
public class PlanteMoyLineRecord implements Comparable<PlanteMoyLineRecord> {

    LocalDate dateprelevement;
    Long originalLineNumber;
    String codeTraitement;
    String codeCulture;
    String codePartie;
    double hauteur;
    String nomlabo;
    int numerorepet;
    String codevariable;
    String codeunite;
    String codemethode;
    String codehumidite;
    float moyenne;
    float ecarttype;

    /**
     *
     */
    public PlanteMoyLineRecord() {
        super();
    }

    /**
     *
     * @param dateprelevement
     * @param originalLineNumber
     * @param codeTraitement
     * @param codeCulture
     * @param codePartie
     * @param hauteur
     * @param nomlabo
     * @param numerorepet
     * @param codevariable
     * @param codeunite
     * @param codemethode
     * @param codehumidite
     * @param moyenne
     * @param ecarttype
     */
    public PlanteMoyLineRecord(LocalDate dateprelevement, Long originalLineNumber, String codeTraitement, String codeCulture, String codePartie, double hauteur, String nomlabo, int numerorepet, String codevariable, String codeunite, String codemethode, String codehumidite, float moyenne, float ecarttype) {
        this.dateprelevement = dateprelevement;
        this.originalLineNumber = originalLineNumber;
        this.codeTraitement = codeTraitement;
        this.codeCulture = codeCulture;
        this.codePartie = codePartie;
        this.hauteur = hauteur;
        this.nomlabo = nomlabo;
        this.numerorepet = numerorepet;
        this.codevariable = codevariable;
        this.codeunite = codeunite;
        this.codemethode = codemethode;
        this.codehumidite = codehumidite;
        this.moyenne = moyenne;
        this.ecarttype = ecarttype;
    }

    /**
     *
     * @param line
     */
    public void copy(PlanteMoyLineRecord line) {
        this.dateprelevement = line.getDateprelevement();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.codeTraitement = line.getCodeTraitement();
        this.codeCulture = line.getCodeCulture();
        this.codePartie = line.getCodePartie();
        this.hauteur = line.getHauteur();
        this.nomlabo = line.getNomlabo();
        this.numerorepet = line.getNumerorepet();
        this.codevariable = line.getCodevariable();
        this.codehumidite = line.getCodehumidite();
        this.moyenne = line.getMoyenne();
        this.codeunite = line.getCodeunite();
        this.ecarttype = line.getEcarttype();
        this.codemethode = line.getCodemethode();
    }

    @Override
    public int compareTo(PlanteMoyLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    /**
     *
     * @param dateprelevement
     */
    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodeTraitement() {
        return codeTraitement;
    }

    /**
     *
     * @param codeTraitement
     */
    public void setCodeTraitement(String codeTraitement) {
        this.codeTraitement = codeTraitement;
    }

    /**
     *
     * @return
     */
    public String getCodeCulture() {
        return codeCulture;
    }

    /**
     *
     * @param codeCulture
     */
    public void setCodeCulture(String codeCulture) {
        this.codeCulture = codeCulture;
    }

    /**
     *
     * @return
     */
    public String getCodePartie() {
        return codePartie;
    }

    /**
     *
     * @param codePartie
     */
    public void setCodePartie(String codePartie) {
        this.codePartie = codePartie;
    }

    /**
     *
     * @return
     */
    public double getHauteur() {
        return hauteur;
    }

    /**
     *
     * @param hauteur
     */
    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public int getNumerorepet() {
        return numerorepet;
    }

    /**
     *
     * @param numerorepet
     */
    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public float getMoyenne() {
        return moyenne;
    }

    /**
     *
     * @param moyenne
     */
    public void setMoyenne(float moyenne) {
        this.moyenne = moyenne;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

}
