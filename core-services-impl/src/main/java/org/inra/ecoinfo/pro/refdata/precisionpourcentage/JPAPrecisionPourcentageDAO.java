/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpourcentage;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAPrecisionPourcentageDAO extends AbstractJPADAO<PrecisionPourcentage> implements IPrecisionPourcentageDAO {

    /**
     *
     * @return
     */
    @Override
    public List<PrecisionPourcentage> getAll() {
        return getAll(PrecisionPourcentage.class);
    }

    /**
     *
     * @param libelle
     * @return
     */
    @Override
    public Optional<PrecisionPourcentage> getBYNKey(String libelle) {
        CriteriaQuery<PrecisionPourcentage> query = builder.createQuery(PrecisionPourcentage.class);
        Root<PrecisionPourcentage> precisionPourcentage = query.from(PrecisionPourcentage.class);
        query
                .select(precisionPourcentage)
                .where(
                        builder.equal(precisionPourcentage.get(PrecisionPourcentage_.libelle), libelle)
                );
        return getOptional(query);
    }

}
