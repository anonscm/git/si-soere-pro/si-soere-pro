/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.IMesureApportDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport_;

/**
 *
 * @author vjkoyao
 */
public class JPAMesureApportDAO extends AbstractJPADAO<MesureApport> implements IMesureApportDAO<MesureApport> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesureApport> getByKeys(String keymesure) {
        CriteriaQuery<MesureApport> query = builder.createQuery(MesureApport.class);
        Root<MesureApport> m = query.from(MesureApport.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureApport_.keymesure), keymesure)
                );
        return getOptional(query);
    }

}
