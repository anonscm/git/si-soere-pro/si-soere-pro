package org.inra.ecoinfo.pro.refdata.caracteristiquevaleur;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<CaracteristiqueValeur> {

    /**
     *
     */
    protected ICaracteristiqueValeurDAO cvDAO;
    private Properties NomCVEn;

    /**
     *
     */
    protected Properties propertiesCommentEn;

    private void createCV(final CaracteristiqueValeur cv) throws BusinessException {
        try {
            cvDAO.saveOrUpdate(cv);
            cvDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create cv");
        }
    }

    private void createOrUpdateCV(final String code, String nom, String commentaire, final CaracteristiqueValeur dbcv) throws BusinessException {
        if (dbcv == null) {
            final CaracteristiqueValeur cv = new CaracteristiqueValeur(nom, commentaire);
            cv.setCv_code(code);
            cv.setCv_nom(nom);
            cv.setCommentaire(commentaire);
            createCV(cv);
        } else {
            updateCV(commentaire, dbcv);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String libelle = tokenizerValues.nextToken();
                cvDAO.remove(cvDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("Can't find caracteristique valeur"))
                );
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<CaracteristiqueValeur> getAllElements() throws BusinessException {
        return cvDAO.getAll();
    }

    /**
     *
     * @return
     */
    public ICaracteristiqueValeurDAO getCvDAO() {
        return cvDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CaracteristiqueValeur cvaleur) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(cvaleur == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : cvaleur.getCv_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cvaleur == null || cvaleur.getCv_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : NomCVEn.getProperty(cvaleur.getCv_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(cvaleur == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : cvaleur.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cvaleur == null || cvaleur.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesCommentEn.getProperty(cvaleur.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));

        return lineModelGridMetadata;
    }

    private void persistCV(String code, String nom, String commentaire) throws BusinessException {
        final CaracteristiqueValeur dbcv = cvDAO.getByNKey(nom).orElse(null);
        createOrUpdateCV(code, nom, commentaire, dbcv);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CaracteristiqueValeur.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                persistCV(code, nom, commentaire);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected ModelGridMetadata<CaracteristiqueValeur> initModelGridMetadata() {
        NomCVEn = localizationManager.newProperties(CaracteristiqueValeur.NAME_ENTITY_JPA, CaracteristiqueValeur.JPA_VALUE_COLUMN, Locale.ENGLISH);
        propertiesCommentEn = localizationManager.newProperties(CaracteristiqueValeur.NAME_ENTITY_JPA, CaracteristiqueValeur.JPA_COMMENT_COLUMN, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param cvDAO
     */
    public void setCvDAO(ICaracteristiqueValeurDAO cvDAO) {
        this.cvDAO = cvDAO;
    }

    private void updateCV(String commentaire, final CaracteristiqueValeur dbcv) throws BusinessException {
        try {
            dbcv.setCommentaire(commentaire);
            cvDAO.saveOrUpdate(dbcv);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update cv");
        }
    }

    /**
     *
     * @param NomCVEn
     */
    public void setNomCVEn(Properties NomCVEn) {
        this.NomCVEn = NomCVEn;
    }

    /**
     *
     * @return
     */
    public Properties getNomCVEn() {
        return NomCVEn;
    }

}
