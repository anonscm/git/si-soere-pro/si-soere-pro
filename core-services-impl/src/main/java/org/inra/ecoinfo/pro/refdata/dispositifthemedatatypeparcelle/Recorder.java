package org.inra.ecoinfo.pro.refdata.dispositifthemedatatypeparcelle;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.IMgaBuilder;
import org.inra.ecoinfo.mga.business.Utils;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.configurator.AbstractMgaIOConfigurator;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfiguration;
import org.inra.ecoinfo.mga.configurator.IMgaIOConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.mga.middleware.IMgaRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.theme.IThemeDAO;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.StreamUtils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<RealNode> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    static final String PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_THEME_NOT_FOUND_IN_DB";
    static final String PROPERTY_MSG_ERROR_CODE_DISP_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_DISP_NOT_FOUND_IN_DB";
    static final String PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE = "PROPERTY_MSG_DATATYPE_MISSING_IN_DATABASE";
    private static final String PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB = "PROPERTY_MSG_ERROR_CODE_PARCELLE_NOT_FOUND_IN_DB";
    static final String PROPERTY_MSG_BAD_DISPOSITIF_THEME_DATATYPE = "PROPERTY_MSG_BAD_DISPOSITIF_THEME_DATATYPE";
    static final String PROPERTY_MSG_DISP_THEME_DATA_MISSING_IN_DATABASE = "PROPERTY_MSG_DISP_THEME_DATA_MISSING_IN_DATABASE";
    static final String BUNDLE_NAME_DATASET = "org.inra.ecoinfo.pro.dataset.messages";
    static final String DISPOSITIFLIEU = "DISPOSITIFLIEU";
    static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    final static int CODE_CONF_WITH_PARCELLE = AbstractMgaIOConfigurator.DATASET_CONFIGURATION;
    final static int CODE_CONF_WITH_NO_PARCELLE = 7;

    /**
     *
     */
    protected static final String PATTERN_ORDERED_PATH_WITH_PARCELLE = "^([^,]*),([^,]*),([^,]*),([^,]*)$";
    IDispositifDAO dispositifDAO;
    IThemeDAO themeDAO;
    IDatatypeDAO datatypeDAO;
    IDispositifThemeDatatypeParcelleDAO dispositifThemeDatatypeParcelleDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;

    final Map<String, String[]> listeDispoPossibles = new ConcurrentHashMap();
    final Map<String, String[]> listeThemePossibles = new ConcurrentHashMap();
    final Map<String, String[]> listeDatatypePossibles = new ConcurrentHashMap();
    final Map<String, String[]> parcellesPossibles = new ConcurrentHashMap();

    @Override
    public void deleteRecord(CSVParser parser, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();

        Stream<String> buildOrderedPaths
                = mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                        configurator.getConfiguration(CODE_CONF_WITH_NO_PARCELLE)
                                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)))
                                .getEntryOrder(),
                        PatternConfigurator.SPLITER, false);
        buildOrderedPaths
                .map(p -> completePath(p))
                .map(p -> mgaServiceBuilder.getRecorder().getRealNodeByNKey(p).orElse(null))
                .forEach(rn -> mgaServiceBuilder.getRecorder().remove(rn));
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
        treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION);
        treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
        policyManager.clearTreeFromSession();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();
        List listNodes = new ArrayList();
        try {
            List<String> buildOrderedPaths = buildOrderedPathes(file);
            Stream<INode> listChild = buildLeaves(buildOrderedPaths)
                    .peek(node -> listNodes.add(node));
            persistNodes(listChild, parser);
            RefDataUtil.gestionErreurs(errorsReport);
            addVariableNodes(listNodes);
            treeApplicationCacheManager.removeSkeletonTree(CODE_CONF_WITH_NO_PARCELLE);
            treeApplicationCacheManager.removeSkeletonTree(CODE_CONF_WITH_PARCELLE);
            treeApplicationCacheManager.removeSkeletonTree(AbstractMgaIOConfigurator.DATASET_CONFIGURATION_RIGHTS);
            policyManager.clearTreeFromSession();
        } catch (final javax.persistence.PersistenceException | BusinessException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private String completePath(String p) {
        p = p.replaceAll("^((.*?)_\\((.*?)\\)),([^,]*?),([^,]*?),([^,]*?)$", "$3_$2,$3_$2_$6,$4,$5");
        p = p.replaceAll("^((.*?)_\\((.*?)\\)),([^,]*?),([^,]*?)$", "$3_$2,$4,$5");
        return p;
    }

    /**
     * <p>
     * from file, build new pathes for
     * agroecosystemeSiteThemeDatatypeParcelle</p>
     *
     * @param file
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    protected List<String> buildOrderedPathes(final File file) throws BusinessException {
        Pattern pattern = Pattern.compile("^\\s*(.*)\\s*\\(\\s*(.*)\\s*\\)\\s*$");
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        List<String> pathes = dispositifThemeDatatypeParcelleDAO.getPathes();
        ErrorsReport errorsReport = new ErrorsReport();
        final IMgaIOConfiguration configurationWithParcelle = configurator.getConfiguration(CODE_CONF_WITH_PARCELLE)
                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)));
        List<String> buildOrderedPaths
                = mgaBuilder.buildOrderedPaths(file.getAbsolutePath(),
                        configurator.getConfiguration(CODE_CONF_WITH_PARCELLE)
                                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)))
                                .getEntryOrder(),
                        PatternConfigurator.SPLITER, true)
                        .map(p -> completePath(p))
                        .filter(p -> !pathes.contains(p))
                        .collect(Collectors.toList());
        RefDataUtil.gestionErreurs(errorsReport);
        return buildOrderedPaths;
    }

    /*private Stream<String> buildOrderedPaths(final String path, final IMgaIOConfiguration configuration, final String splitter, boolean skipHeader, ErrorsReport errorsReport) {
        try (
                final FileInputStream fis = new FileInputStream(path);
                final BufferedInputStream bufferedInputStream = new BufferedInputStream(fis);) {
            final String fileEncoding = Utils.detectStreamEncoding(bufferedInputStream);
            return Files.lines(Paths.get(path), Charset.forName(fileEncoding))
                    .skip(skipHeader ? 1 : 0)
                    .map(line -> orderLine(line, splitter, configuration, errorsReport))
                    .distinct()
                    .sorted();
        } catch (IOException ex) {
            LoggerFactory.getLogger(getClass()).error("buildOrderedPaths", ex);
        }

        return null;
    }*/
    /**
     * <p>
     * from a list af ordered pathes build a list of INode leaves</p>
     *
     * @param buildOrderedPaths
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws javax.persistence.PersistenceException
     */
    protected Stream<INode> buildLeaves(List<String> buildOrderedPaths) throws BusinessException {
        IMgaIOConfigurator configurator = mgaServiceBuilder.getMgaIOConfigurator();
        IMgaBuilder mgaBuilder = mgaServiceBuilder.getMgaBuilder();
        IMgaRecorder mgaRecorder = mgaServiceBuilder.getRecorder();
        Stream<INodeable> nodeables = mgaRecorder.getNodeables();
        HashMap<String, INodeable> entities = new HashMap();
        nodeables.forEach((nodeable) -> {
            entities.put(nodeable.getUniqueCode(), nodeable);
            dispositifDAO.getAll().stream()
                    .forEach(
                            d -> entities.put(Dispositif.class.getSimpleName() + PatternConfigurator.ANCESTOR_SEPARATOR + Utils.createCodeFromString(d.getNomDispositif_nomLieu()), d));
        });
        final IMgaIOConfiguration configurationWithParcelle = configurator.getConfiguration(CODE_CONF_WITH_PARCELLE)
                .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_PARCELLE)));
        Class<INodeable> stickyLeafType = configurationWithParcelle
                .getStickyLeafType();
        List<INodeable> stickyNodeables = null;
        if (configurationWithParcelle.getStickyLeafType() != null) {
            stickyNodeables = mgaRecorder.getNodeables(stickyLeafType).collect(Collectors.toList());
        }
        Map<Boolean, List<String>> partitionOnPathes = buildOrderedPaths.stream()
                .collect(Collectors.partitioningBy(p -> p.matches(PATTERN_ORDERED_PATH_WITH_PARCELLE)));
        Stream<INode> listChild = mgaBuilder.buildLeavesForPathes(
                partitionOnPathes.get(Boolean.TRUE).stream(),
                configurationWithParcelle.getEntryType(),
                entities,
                WhichTree.TREEDATASET,
                stickyNodeables).collect(Collectors.toList()).stream();

        return StreamUtils.concat(listChild, mgaBuilder.buildLeavesForPathes(
                partitionOnPathes.get(Boolean.FALSE).stream(),
                configurator.getConfiguration(CODE_CONF_WITH_NO_PARCELLE)
                        .orElseThrow(() -> new BusinessException(String.format("no configuration %s", CODE_CONF_WITH_NO_PARCELLE))).getEntryType(),
                entities,
                WhichTree.TREEDATASET,
                stickyNodeables));
    }

    @Override
    protected List<RealNode> getAllElements() throws BusinessException {

        List<RealNode> nodes = null;
        try {
            nodes = dispositifThemeDatatypeParcelleDAO.loadRealDatatypeNodes();
            nodes = nodes.stream().sorted((o1, o2) -> o1.getPath().compareTo(o2.getPath())).collect(Collectors.toList());
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        return nodes;
    }

    private void dispositifsPossibles() {
        List<Dispositif> groupeDispositif = dispositifDAO.getAll();
        String[] listedispositif = new String[groupeDispositif.size() + 1];
        listedispositif[0] = "";
        int index = 1;
        for (Dispositif dispositif : groupeDispositif) {
            listedispositif[index++] = dispositif.getNomDispositif_nomLieu();
        }
        this.listeDispoPossibles.put(ColumnModelGridMetadata.NULL_KEY, listedispositif);
    }

    private void ThemePossibles() {
        List<Theme> groupetheme = themeDAO.getAll();
        String[] ListeTheme = new String[groupetheme.size() + 1];
        ListeTheme[0] = "";
        int index = 1;
        for (Theme theme : groupetheme) {
            ListeTheme[index++] = theme.getCode();
        }
        this.listeThemePossibles.put(ColumnModelGridMetadata.NULL_KEY, ListeTheme);
    }

    private void dataTypePossibles() {
        List<DataType> groupedt = datatypeDAO.getAll();
        String[] Listedatatype = new String[groupedt.size() + 1];
        Listedatatype[0] = "";
        int index = 1;
        for (DataType datatype : groupedt) {
            Listedatatype[index++] = datatype.getCode();
        }
        this.listeDatatypePossibles.put(ColumnModelGridMetadata.NULL_KEY, Listedatatype);
    }

    void parcellePossibles() {
        final List<Dispositif> localSites = this.dispositifDAO.getAll();
        this.parcellesPossibles.clear();
        localSites.forEach((dispositif) -> {
            final List<ParcelleElementaire> parcelles = this.parcelleElementaireDAO.getByDispositif(dispositif);
            final String[] namesParcellesPossibles = new String[parcelles.size() + 1];
            if (!parcelles.isEmpty()) {
                int index = 0;
                namesParcellesPossibles[index++] = org.apache.commons.lang.StringUtils.EMPTY;
                for (final ParcelleElementaire parcelle : parcelles) {
                    namesParcellesPossibles[index++] = parcelle.getCodeParcelleElementaire().getCode();
                }
                this.parcellesPossibles.put(dispositif.getNomDispositif_nomLieu(), namesParcellesPossibles);
            } else {
                this.parcellesPossibles.put(dispositif.getNomDispositif_nomLieu(), new String[]{""});
            }
            this.parcellesPossibles.put(dispositif.getName(), namesParcellesPossibles);
        });
    }

    /**
     *
     * @param <T>
     * @param clazz
     * @param node
     * @param method
     * @return
     */
    protected <T extends Nodeable> String getNodeableCode(Class<T> clazz, RealNode node, Function<RealNode, String> method) {
        return Optional.ofNullable(node)
                .map(n -> n.getNodeByNodeableTypeResource(clazz))
                .map(method)
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(RealNode node) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        final ColumnModelGridMetadata siteColumn = new ColumnModelGridMetadata(getNodeableCode(Dispositif.class, node, (n) -> ((Dispositif) n.getNodeable()).getNomDispositif_nomLieu()), this.listeDispoPossibles, null, true, false, true);
        final ColumnModelGridMetadata themeColumn = new ColumnModelGridMetadata(getNodeableCode(Theme.class, node, (n) -> n.getCode()), this.listeThemePossibles, null, true, false, true);
        final ColumnModelGridMetadata datatypeColumn = new ColumnModelGridMetadata(getNodeableCode(DataType.class, node, (n) -> n.getCode()), this.listeDatatypePossibles, null, true, false, true);
        final List<ColumnModelGridMetadata> refsSite = new LinkedList();
        final String parcelleCode = getNodeableCode(ParcelleElementaire.class, node, (n) -> ((ParcelleElementaire) n.getNodeable()).getCodeParcelleElementaire().getCode());
        final ColumnModelGridMetadata parcelleColumn = new ColumnModelGridMetadata(parcelleCode,
                this.parcellesPossibles, null, true, false, false);
        refsSite.add(parcelleColumn);
        siteColumn.setRefs(refsSite);
        parcelleColumn.setValue(parcelleCode);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(siteColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(themeColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(datatypeColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(parcelleColumn);
        return lineModelGridMetadata;

    }

    @Override
    protected ModelGridMetadata<RealNode> initModelGridMetadata() {
        dispositifsPossibles();
        ThemePossibles();
        dataTypePossibles();
        parcellePossibles();
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param line
     * @param splitter
     * @param order
     * @return
     */
    protected String orderLine(String line, String splitter, Integer[] order) {
        StringBuilder tmp = new StringBuilder();
        String[] splitedLine = line.split(splitter);
        for (int i = 1; i < order.length; i++) {
            if (order[i] < splitedLine.length) {
                tmp.append(Utils.createCodeFromString(splitedLine[order[i]])).append(PatternConfigurator.PATH_SEPARATOR);
            }
        }
        tmp.deleteCharAt(tmp.length() - 1);
        return tmp.toString();
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @return
     */
    public IThemeDAO getThemeDAO() {
        return themeDAO;
    }

    /**
     *
     * @param themeDAO
     */
    public void setThemeDAO(IThemeDAO themeDAO) {
        this.themeDAO = themeDAO;
    }

    /**
     *
     * @return
     */
    public IDatatypeDAO getDatatypeDAO() {
        return datatypeDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    /**
     *
     * @return
     */
    public IDispositifThemeDatatypeParcelleDAO getDispositifThemeDatatypeParcelleDAO() {
        return dispositifThemeDatatypeParcelleDAO;
    }

    /**
     *
     * @param dispositifThemeDatatypeParcelleDAO
     */
    public void setDispositifThemeDatatypeParcelleDAO(IDispositifThemeDatatypeParcelleDAO dispositifThemeDatatypeParcelleDAO) {
        this.dispositifThemeDatatypeParcelleDAO = dispositifThemeDatatypeParcelleDAO;
    }

    /**
     *
     * @return
     */
    public IParcelleElementaireDAO getParcelleElementaireDAO() {
        return parcelleElementaireDAO;
    }

    /**
     *
     * @param parcelleElementaireDAO
     */
    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     *
     * @return
     */
    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    private void addVariableNodes(List<INode> listNodes) {
        Map<DataType, List<DatatypeVariableUnitePRO>> datatypeNodes = dataTypeVariableUnitePRODAO.getAll().stream()
                .collect(Collectors.groupingBy(DatatypeVariableUnitePRO::getDatatype));
        listNodes.stream()
                .forEach((node) -> {
                    DataType datatype = (DataType) node.getNodeable();
                    if (datatypeNodes.containsKey(datatype)) {
                        datatypeNodes.get(datatype).forEach((dvu) -> {
                            String path = String.format("%s%s%s", node.getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = new RealNode(node.getRealNode(), null, dvu, path);
                            mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) node, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        });
                    }
                });
    }

    private void joinVariablenodes(INode dtd, List<INode> dvus) {
        dvus
                .forEach((dvu) -> {
                    dvu.setParent(dtd);
                    if (dvu.getRealNode().getParent() == null) {
                        dvu.getRealNode().setParent(dtd.getRealNode());
                    }
                });
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

}
