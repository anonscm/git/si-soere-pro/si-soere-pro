/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class GenericTestFormat implements ITestFormat{
    static final long serialVersionUID = 1L;
    static final Logger LOGGER = LoggerFactory.getLogger(GenericTestFormat.class);

    private String datatypeName;
    private ITestHeaders testHeaders;
    private ITestValues testValues;

    public GenericTestFormat() {
        super();
    }

   

    public String getDatatypeName() {
        return datatypeName;
    }

   

    
    
    @Override
    public void setDatatypeName(final String datatypeName) {
        this.datatypeName = datatypeName;
    }

    @Override
    public void setTestHeaders(final ITestHeaders testHeaders) {
        this.testHeaders = testHeaders;
    }

    @Override
    public void setTestValues(final ITestValues testValues) {
        this.testValues = testValues;
    }

    @Override
    public void testFormat(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties, 
                                                       String encoding, DatasetDescriptorPRO datasetDescriptor) 
                                                            throws BadFormatException {
        
        
        final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(
                RecorderPRO.getPROMessage(ITestFormat.PROPERTY_MSG_ERROR_BAD_FORMAT));
        LOGGER.debug(RecorderPRO.getPROMessage(ITestFormat.PROPERTY_MSG_CHECKING_FORMAT_FILE));
        long lineNumber = -1;
        try {
            lineNumber = this.testHeaders.testHeaders(parser, versionFile, sessionProperties,
                                                      encoding, badsFormatsReport, datasetDescriptor);
          //  lineNumber = lineNumber-1;
            if (datasetDescriptor.getEnTete() != lineNumber) {
                final String message = String.format(
                        RecorderPRO.getPROMessage(ITestFormat.PROPERTY_MSG_BAD_HEADER_SIZE),
                        lineNumber, datasetDescriptor.getEnTete());
                LOGGER.debug(message);
                throw new BusinessException(message);
            }
            this.testValues.testValues(lineNumber, parser, versionFile, sessionProperties,
                                       encoding, badsFormatsReport, datasetDescriptor, this.getDatatypeName());
        } catch (final BusinessException e) {
            badsFormatsReport.addException(e);
            throw new BadFormatException(badsFormatsReport);
        }
        if (badsFormatsReport.hasErrors()) {
            LOGGER.debug(badsFormatsReport.getMessages());
            throw new BadFormatException(badsFormatsReport);
        }
          
    }
    
}
