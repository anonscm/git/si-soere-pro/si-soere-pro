/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contextescultures;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.amenagementavantplantation.AmenagementAvantPlantation;
import org.inra.ecoinfo.pro.refdata.amenagementavantplantation.IAmenagementAvantPlantationDAO;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.cepagevariete.CepageVariete;
import org.inra.ecoinfo.pro.refdata.cepagevariete.ICepageVarieteDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.portegreffe.IPorteGreffeDAO;
import org.inra.ecoinfo.pro.refdata.portegreffe.PorteGreffe;
import org.inra.ecoinfo.pro.refdata.typeculture.ITypeCultureDAO;
import org.inra.ecoinfo.pro.refdata.typeculture.TypeCulture;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<ContextesCultures> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    private static final String PROPERTY_MSG_CULTUREPERENNES_BAD_TCULTURE = "PROPERTY_MSG_CULTUREPERENNES_BAD_TCULTURE";
    private static final String PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU = "PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU";
    private static final String PROPERTY_MSG_AAP_BAD_CULTURE = "PROPERTY_MSG_AAP_BAD_CULTURE";
    private static final String PROPERTY_MSG_CEPAGE_BAD_CULTURE = "PROPERTY_MSG_CEPAGE_BAD_CULTURE";
    private static final String PROPERTY_MSG_PORTEGREFFE_BAD_CULTURE = "PROPERTY_MSG_PORTEGREFFE_BAD_CULTURE";
    private static final String PROPERTY_MSG_ANNEE_BAD_CULTURE = "PROPERTY_MSG_ANNEE_BAD_CULTURE";
    IContextesCulturesDAO contextesculturesDAO;
    IDispositifDAO dispositifDAO;
    ITypeCultureDAO typeCultureDAO;
    ICepageVarieteDAO cepagevarieteDAO;
    IAmenagementAvantPlantationDAO avpDAO;
    IPorteGreffeDAO portegreffeDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;

    private String[] listeTCulturePossibles;
    private String[] listeDispositifsPossibles;
    private Map<String, String[]> listeLieuPossibles;
    private String[] listeAAPPossibles;
    private String[] listeCepagePossibles;
    private String[] listePortePossibles;
    private String[] listeBooleansPossible;

    /**
     *
     * @param culturesperennes
     * @throws BusinessException
     */
    public void createCulturePerennes(ContextesCultures culturesperennes) throws BusinessException {
        try {
            contextesculturesDAO.saveOrUpdate(culturesperennes);
            contextesculturesDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create culture perenne");
        }
    }

    private void updateCulturesPerennes(Dispositif dispositif, TypeCulture typeculture, Integer densite, double eir, double eip, String orientation, ContextesCultures dbcp,
            Integer and, Integer anf, String ehr, String ehir, String clone,
            CepageVariete cv, PorteGreffe pg, AmenagementAvantPlantation aap) throws BusinessException {
        try {
            dbcp.setDispositif(dispositif);
            dbcp.setTypeculture(typeculture);
            dbcp.setCp_densite(densite);
            dbcp.setCp_ecartinterplan(eip);
            dbcp.setCp_ecartinterrang(eir);
            dbcp.setCp_orientation(orientation);
            dbcp.setAmenagementavantplantation(aap);
            dbcp.setCepagevariete(cv);
            dbcp.setPortegreffe(pg);
            dbcp.setCc_anneedebut(and);
            dbcp.setCc_anneefin(anf);
            dbcp.setCc_clone(clone);
            dbcp.setCc_enherbementrang(ehr);
            dbcp.setCc_enherbementinterrang(ehir);
            contextesculturesDAO.saveOrUpdate(dbcp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update culture perenne");
        }
    }

    private void createOrUpdateCulturesPerennes(Dispositif dispositif, TypeCulture typeculture, Integer densite, double eir, double eip,
            Integer and, Integer anf, String ehr, String ehir, String clone,
            String orientation, ContextesCultures dbcp, CepageVariete cv, PorteGreffe pg, AmenagementAvantPlantation aap) throws BusinessException {
        if (dbcp == null) {
            final ContextesCultures culturesperennes = new ContextesCultures(densite, eir, eip, orientation, dispositif, typeculture, and, anf, ehr, ehir, clone, aap, cv, pg);
            culturesperennes.setDispositif(dispositif);
            culturesperennes.setTypeculture(typeculture);
            culturesperennes.setCp_densite(densite);
            culturesperennes.setCp_ecartinterrang(eir);
            culturesperennes.setCp_ecartinterplan(eip);
            culturesperennes.setCp_orientation(orientation);
            culturesperennes.setAmenagementavantplantation(aap);
            culturesperennes.setCepagevariete(cv);
            culturesperennes.setPortegreffe(pg);
            culturesperennes.setCc_anneedebut(and);
            culturesperennes.setCc_anneefin(anf);
            culturesperennes.setCc_enherbementrang(ehr);
            culturesperennes.setCc_enherbementinterrang(ehir);
            createCulturePerennes(culturesperennes);
        } else {
            updateCulturesPerennes(dispositif, typeculture, densite, eir, eip, orientation, dbcp, and, anf, ehr, ehir, clone, cv, pg, aap);
        }
    }

    private void persistCulturesPerennes(Dispositif dispositif, TypeCulture typeculture, Integer densite, double eir, double eip, String orientation,
            Integer and, Integer anf, String ehr, String ehir, String clone,
            CepageVariete cv, PorteGreffe pg, AmenagementAvantPlantation aap) throws BusinessException, BusinessException {
        final ContextesCultures cp = contextesculturesDAO.getByNKey(dispositif, typeculture, densite, and).orElse(null);
        createOrUpdateCulturesPerennes(dispositif, typeculture, densite, eir, eip, and, anf, ehr, ehir, clone, orientation, cp, cv, pg, aap);
    }

    private void listeTypeCulturePossibles() throws BusinessException {
        List<TypeCulture> groupecv = typeCultureDAO.getAll();
        String[] listeCVPossibles = new String[groupecv.size() + 1];
        listeCVPossibles[0] = "";
        int index = 1;
        for (TypeCulture cepage : groupecv) {
            listeCVPossibles[index++] = cepage.getLibelle();
        }
        this.listeTCulturePossibles = listeCVPossibles;
    }

    private void initDispositifPossibles() throws BusinessException {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCodeDispo();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifsPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void listeBooleanPossibles() throws BusinessException {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String disp = tokenizerValues.nextToken();
                final String lieu = tokenizerValues.nextToken();
                final String type = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String densite = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                final String annee = tokenizerValues.nextToken();
                int densi = Integer.parseInt(densite);
                int an = Integer.parseInt(annee);
                Dispositif dispositif = dispositifDAO.getByNKey(disp, lieu).orElse(null);
                TypeCulture ctype = typeCultureDAO.getByNKey(type).orElse(null);
                final ContextesCultures dbcp = contextesculturesDAO.getByNKey(dispositif, ctype, densi, an).orElseThrow(PersistenceException::new);
                contextesculturesDAO.remove(dbcp);
                values = parser.getLine();

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void listeaAPPossibles() throws BusinessException {
        List<AmenagementAvantPlantation> groupeaap = avpDAO.getAll();
        String[] listeaapPossibles = new String[groupeaap.size() + 1];
        listeaapPossibles[0] = "";
        int index = 1;
        for (AmenagementAvantPlantation aap : groupeaap) {
            listeaapPossibles[index++] = aap.getAap_libelle();
        }
        this.listeAAPPossibles = listeaapPossibles;
    }

    private void listeCepagePossibles() throws BusinessException {
        List<CepageVariete> groupecepage = cepagevarieteDAO.getAll();
        String[] listecepagePossibles = new String[groupecepage.size() + 1];
        listecepagePossibles[0] = "";
        int index = 1;
        for (CepageVariete cepagev : groupecepage) {
            listecepagePossibles[index++] = cepagev.getCv_libelle();
        }
        this.listeCepagePossibles = listecepagePossibles;
    }

    private void listePortePossibles() throws BusinessException {
        List<PorteGreffe> groupepg = portegreffeDAO.getAll();
        String[] listeportePossibles = new String[groupepg.size() + 1];
        listeportePossibles[0] = "";
        int index = 1;
        for (PorteGreffe porteg : groupepg) {
            listeportePossibles[index++] = porteg.getPg_libelle();
        }
        this.listePortePossibles = listeportePossibles;
    }

    @Override
    protected List<ContextesCultures> getAllElements() throws BusinessException {
        return contextesculturesDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ContextesCultures.NAME_ENTITY_JPA);
                long index = tokenizerValues.currentTokenIndex();
                final String code = tokenizerValues.nextToken();
                final String lieu = tokenizerValues.nextToken();
                long indextc = tokenizerValues.currentTokenIndex();
                final String tculture = tokenizerValues.nextToken();
                long indexaap = tokenizerValues.currentTokenIndex();
                final String aap = tokenizerValues.nextToken();
                long indexc = tokenizerValues.currentTokenIndex();
                final String cepage = tokenizerValues.nextToken();
                long indexp = tokenizerValues.currentTokenIndex();
                final String porte = tokenizerValues.nextToken();
                Integer den = verifieInt(tokenizerValues, line, true, errorsReport);
                double Eir = verifieDouble(tokenizerValues, 0, true, errorsReport);
                double Eip = verifieDouble(tokenizerValues, 0, true, errorsReport);
                final String orientation = tokenizerValues.nextToken();
                final String ehr = tokenizerValues.nextToken();
                final String ehir = tokenizerValues.nextToken();
                Integer and = verifieInt(tokenizerValues, line, true, errorsReport);
                Integer anf = verifieInt(tokenizerValues, line, false, errorsReport);
                final String clone = tokenizerValues.nextToken();
                long indexan = tokenizerValues.currentTokenIndex();
                if (and > anf) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ANNEE_BAD_CULTURE), line, indexan, anf, and));
                }
                Dispositif dbdispositif = dispositifDAO.getByNKey(code, lieu).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU), line, index, code, lieu));
                }
                TypeCulture dbtculture = typeCultureDAO.getByNKey(tculture).orElse(null);
                if (dbtculture == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CULTUREPERENNES_BAD_TCULTURE), line, indextc, tculture));
                }
                AmenagementAvantPlantation dbaap = avpDAO.getByNKey(aap).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_AAP_BAD_CULTURE), line, indexaap, aap));
                }
                CepageVariete dbcv = cepagevarieteDAO.getByNKey(cepage).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CEPAGE_BAD_CULTURE), line, indexc, cepage));
                }
                PorteGreffe dbpg = portegreffeDAO.getByNKey(porte).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PORTEGREFFE_BAD_CULTURE), line, indexp, porte));
                }
                if (!errorsReport.hasErrors()) {
                    persistCulturesPerennes(dbdispositif, dbtculture, den, Eir, Eip, orientation, and, anf, ehr, ehir, clone, dbcv, dbpg, dbaap);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ContextesCultures cultureperennes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        String valeurProduit = cultureperennes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getDispositif() != null ? cultureperennes.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifsPossibles, null, true, false, true);
        String valeurProcede = cultureperennes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getDispositif() != null ? cultureperennes.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : cultureperennes.getTypeculture().getLibelle() != null ? cultureperennes.getTypeculture().getLibelle() : "",
                        listeTCulturePossibles,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : cultureperennes.getAmenagementavantplantation().getAap_libelle() != null
                        ? cultureperennes.getAmenagementavantplantation().getAap_libelle() : "",
                        listeAAPPossibles,
                        null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : cultureperennes.getCepagevariete().getCv_libelle() != null ? cultureperennes.getCepagevariete().getCv_libelle() : "",
                        listeCepagePossibles, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : cultureperennes.getPortegreffe().getPg_libelle() != null ? cultureperennes.getPortegreffe().getPg_libelle() : "",
                        listePortePossibles,
                        null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCp_densite() == 0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCp_densite(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCp_ecartinterrang() == 0.0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCp_ecartinterrang(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCp_ecartinterplan() == 0.0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCp_ecartinterplan(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCp_orientation() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCp_orientation(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCc_enherbementrang() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCc_enherbementrang() != null
                        ? cultureperennes.getCc_enherbementrang() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCc_enherbementinterrang() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCc_enherbementinterrang() != null
                        ? cultureperennes.getCc_enherbementinterrang() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCc_anneedebut() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCc_anneedebut(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCc_anneefin() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCc_anneefin(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(cultureperennes == null || cultureperennes.getCc_clone() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : cultureperennes.getCc_clone(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param contextesculturesDAO
     */
    public void setContextesculturesDAO(IContextesCulturesDAO contextesculturesDAO) {
        this.contextesculturesDAO = contextesculturesDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param typeCultureDAO
     */
    public void setTypeCultureDAO(ITypeCultureDAO typeCultureDAO) {
        this.typeCultureDAO = typeCultureDAO;
    }

    /**
     *
     * @param cepagevarieteDAO
     */
    public void setCepagevarieteDAO(ICepageVarieteDAO cepagevarieteDAO) {
        this.cepagevarieteDAO = cepagevarieteDAO;
    }

    /**
     *
     * @param avpDAO
     */
    public void setAvpDAO(IAmenagementAvantPlantationDAO avpDAO) {
        this.avpDAO = avpDAO;
    }

    /**
     *
     * @param portegreffeDAO
     */
    public void setPortegreffeDAO(IPorteGreffeDAO portegreffeDAO) {
        this.portegreffeDAO = portegreffeDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    @Override
    protected ModelGridMetadata<ContextesCultures> initModelGridMetadata() {
        try {
            listeTypeCulturePossibles();
            initDispositifPossibles();
            listeaAPPossibles();
            listeCepagePossibles();
            listePortePossibles();
            listeBooleanPossibles();
        } catch (BusinessException e) {
        }
        return super.initModelGridMetadata();

    }

}
