/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typedispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeDispositifDAO extends IDAO<TypeDispositif> {

    /**
     *
     * @return
     */
    List<TypeDispositif> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeDispositif> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<TypeDispositif> getByCode(String code);

}
