/**
 *
 */
package org.inra.ecoinfo.pro.refdata.region;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.pays.Pays;

/**
 * @author sophie
 *
 */
public class JPARegionDAO extends AbstractJPADAO<Region> implements IRegionDAO {
    
    public List<Region> getAll(){
        return getAllBy(Region.class, Region::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.pays.IRegionDAO#getByNom(java.lang.String)
     */
    @Override
    public Optional<Region> getByNom(String nom) {
        CriteriaQuery<Region> query = builder.createQuery(Region.class);
        Root<Region> region = query.from(Region.class);
        query
                .select(region)
                .where(
                        builder.equal(region.get(Region_.nom), nom)
                );
        return getOptional(query);

    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.region.IRegionDAO#getByCode(java.lang.String)
     */
    @Override
    public Optional<Region> getByNKey(String code) {
        CriteriaQuery<Region> query = builder.createQuery(Region.class);
        Root<Region> region = query.from(Region.class);
        query
                .select(region)
                .where(
                        builder.equal(region.get(Region_.code), code)
                );
        return getOptional(query);

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.pays.IRegionDAO#getByPays(org.inra.ecoinfo .pro.refdata.pays.Pays)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Region> getByPays(Pays pays) {
        CriteriaQuery<Region> query = builder.createQuery(Region.class);
        Root<Region> region = query.from(Region.class);
        query
                .select(region)
                .where(
                        builder.equal(region.get(Region_.pays), pays)
                );
        return getResultList(query);
    }

}
