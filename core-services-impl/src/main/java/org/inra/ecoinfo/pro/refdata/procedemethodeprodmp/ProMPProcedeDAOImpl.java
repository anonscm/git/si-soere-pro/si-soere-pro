package org.inra.ecoinfo.pro.refdata.procedemethodeprodmp;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.component.Composant_;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.melange.Melange_;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.process.Process_;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produit.Produits_;

/**
 *
 * @author ptcherniati
 */
public class ProMPProcedeDAOImpl extends AbstractJPADAO<ProcedeMethodeProdMp> implements IProMPProcedeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ProcedeMethodeProdMp> getAll() {
        return getAll(ProcedeMethodeProdMp.class);
    }

    /**
     *
     * @param melange
     * @param process
     * @param ordre
     * @return
     */
    @Override
    public Optional<ProcedeMethodeProdMp> getByNKey(Melange melange, Process process, int ordre) {
        CriteriaQuery<ProcedeMethodeProdMp> query = builder.createQuery(ProcedeMethodeProdMp.class);
        Root<ProcedeMethodeProdMp> procedeMethodeProdMp = query.from(ProcedeMethodeProdMp.class);
        Join<ProcedeMethodeProdMp, Melange> mel = procedeMethodeProdMp.join(ProcedeMethodeProdMp_.melange);
        Join<ProcedeMethodeProdMp, Process> procs = procedeMethodeProdMp.join(ProcedeMethodeProdMp_.process);
        query
                .select(procedeMethodeProdMp)
                .where(
                        builder.equal(procedeMethodeProdMp.get(ProcedeMethodeProdMp_.odreprocede), ordre),
                        builder.equal(mel, melange),
                        builder.equal(procs, process)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeProduit
     * @param nomMatierePremiere
     * @param pourcentage
     * @param intituleProcede
     * @param ordreProcede
     * @return
     */
    @Override
    public Optional<ProcedeMethodeProdMp>  getByNKey(String codeProduit, String nomMatierePremiere, double pourcentage, 
            String intituleProcede, double ordreProcede) {
        CriteriaQuery<ProcedeMethodeProdMp> query = builder.createQuery(ProcedeMethodeProdMp.class);
        Root<ProcedeMethodeProdMp> procedeMethodeProdMp = query.from(ProcedeMethodeProdMp.class);
        Join<ProcedeMethodeProdMp, Melange> mel = procedeMethodeProdMp.join(ProcedeMethodeProdMp_.melange);
        Join<ProcedeMethodeProdMp, Process> procs = procedeMethodeProdMp.join(ProcedeMethodeProdMp_.process);
        Join<Melange, Produits> produit = mel.join(Melange_.produits);
        Join<Melange, Composant> composant = mel.join(Melange_.composant);
        query
                .select(procedeMethodeProdMp)
                .where(
                        builder.equal(produit.get(Produits_.codecomposant), codeProduit),
                        builder.equal(composant.get(Composant_.codecomposant), nomMatierePremiere),
                        builder.equal(mel.get(Melange_.pourcentage), pourcentage),
                        builder.equal(procs.get(Process_.process_intitule), intituleProcede),
                        builder.equal(procedeMethodeProdMp.get(ProcedeMethodeProdMp_.odreprocede), ordreProcede)
                );
        return getOptional(query);
    }
}
