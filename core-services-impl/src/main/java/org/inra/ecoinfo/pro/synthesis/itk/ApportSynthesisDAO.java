/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.itk;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport_;
import org.inra.ecoinfo.pro.synthesis.apport.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.apport.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class ApportSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype,ValeurApport, MesureApport> {

    @Override
    Class<ValeurApport> getValueClass() {
        return ValeurApport.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurApport, MesureApport> getMesureAttribute() {
        return ValeurApport_.mesureinterventionapport;
    }

    @Override
    Boolean isValue() {
        return Boolean.TRUE;
    }

}
