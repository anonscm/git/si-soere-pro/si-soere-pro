/**
 *
 */
package org.inra.ecoinfo.pro.refdata.placette;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 * @author sophie
 *
 */
public class JPAPlacetteDAO extends AbstractJPADAO<Placette> implements IPlacetteDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO#getByCodeParcelleElt (java.lang.String, org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire)
     */

    /**
     *
     * @param nom
     * @param parcelleElementaire
     * @return
     */

    @Override
    public Optional<Placette> getByNameAndParcelle(String nom, ParcelleElementaire parcelleElementaire) {
        CriteriaQuery<Placette> query = builder.createQuery(Placette.class);
        Root<Placette> placette = query.from(Placette.class);
        Join<Placette, ParcelleElementaire> pelt = placette.join(Placette_.parcelleElementaire);
        query
                .select(placette)
                .where(
                        builder.equal(pelt, parcelleElementaire),
                        builder.equal(placette.get(Placette_.nom), nom)
                );
        return getOptional(query);
    }

    @Override
    public Optional<Placette> getByNKey(String code, ParcelleElementaire parcelleElementaire) {
        CriteriaQuery<Placette> query = builder.createQuery(Placette.class);
        Root<Placette> placette = query.from(Placette.class);
        Join<Placette, ParcelleElementaire> pelt = placette.join(Placette_.parcelleElementaire);
        query
                .select(placette)
                .where(
                        builder.equal(pelt, parcelleElementaire),
                        builder.equal(placette.get(Placette_.code), code)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO#getByParcelleElt(org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire)
     */

    /**
     *
     * @param parcelleElementaire
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Placette> getByParcelleElt(ParcelleElementaire parcelleElementaire) {
        CriteriaQuery<Placette> query = builder.createQuery(Placette.class);
        Root<Placette> placette = query.from(Placette.class);
        Join<Placette, ParcelleElementaire> pelt = placette.join(Placette_.parcelleElementaire);
        query
                .select(placette)
                .where(
                        builder.equal(pelt, parcelleElementaire)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO#getByGeolocalisation (org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation)
     */

    /**
     *
     * @param geolocalisation
     * @return
     */

    @Override
    public Optional<Placette> getByGeolocalisation(Geolocalisation geolocalisation) {
        CriteriaQuery<Placette> query = builder.createQuery(Placette.class);
        Root<Placette> placette = query.from(Placette.class);
        Join<Placette, Geolocalisation> geo = placette.join(Placette_.geolocalisation);
        query
                .select(placette)
                .where(
                        builder.equal(geo, geolocalisation)
                );
        return getOptional(query);
    }
}
