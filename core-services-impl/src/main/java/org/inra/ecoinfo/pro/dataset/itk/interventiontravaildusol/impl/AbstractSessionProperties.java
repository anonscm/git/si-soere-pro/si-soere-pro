/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.impl;

import org.inra.ecoinfo.pro.dataset.impl.AbstractSessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.impl.ISessionPropertiesApport;

/**
 *
 * @author adiankha
 */
public abstract class AbstractSessionProperties extends AbstractSessionPropertiesPRO implements ISessionPropertiesApport {

    /**
     *
     */
    public AbstractSessionProperties() {
    }

}
