package org.inra.ecoinfo.pro.refdata.dispositifobservatoire;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;

/**
 * @author sophie
 *
 */
public class JPADispositifObservatoireDAO extends AbstractJPADAO<DispositifObservatoire> implements IDispositifObservatoireDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.dispositifobservatoire.IDispositifObservatoireDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<DispositifObservatoire> getAll() {
        return getAllBy(DispositifObservatoire.class, DispositifObservatoire::getDispositif);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.dispositifobservatoire. IDispositifObservatoireDAO #getByNKey(org.inra.ecoinfo.pro .refdata.dispositif.Dispositif, org.inra.ecoinfo.pro.refdata.observatoire.Observatoire)
     */

    /**
     *
     * @param dispositif
     * @param observatoire
     * @return
     */

    @Override
    public Optional<DispositifObservatoire> getByNKey(Dispositif dispositif, Observatoire observatoire) {
        CriteriaQuery<DispositifObservatoire> query = builder.createQuery(DispositifObservatoire.class);
        Root<DispositifObservatoire> dispositifObservatoire = query.from(DispositifObservatoire.class);
        Join<DispositifObservatoire, Dispositif> dispo = dispositifObservatoire.join(DispositifObservatoire_.dispositif);
        Join<DispositifObservatoire, Observatoire> obse = dispositifObservatoire.join(DispositifObservatoire_.observatoire);
        query
                .select(dispositifObservatoire)
                .where(
                        builder.equal(dispo, dispositif),
                        builder.equal(obse, observatoire)
                );
        return getOptional(query);
    }
}
