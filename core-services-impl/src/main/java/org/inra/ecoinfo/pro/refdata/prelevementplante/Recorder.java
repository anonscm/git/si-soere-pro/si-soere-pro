/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementplante;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.biomasse.IBiomasseDAO;
import org.inra.ecoinfo.pro.refdata.biomasseprelevee.BiomassePrelevee;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.cultures.Cultures;
import org.inra.ecoinfo.pro.refdata.cultures.ICulturesDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.especeplante.IEspecePlanteDAO;
import org.inra.ecoinfo.pro.refdata.methodeprelevement.IMethodePrelevementDAO;
import org.inra.ecoinfo.pro.refdata.methodeprelevement.MethodePrelevement;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.partieprelevee.IPartiePreleveeDAO;
import org.inra.ecoinfo.pro.refdata.partieprelevee.PartiePrelevee;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.stadedeveloppement.IStadeDAO;
import org.inra.ecoinfo.pro.refdata.stadedeveloppement.StadeDeveloppement;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<PrelevementPlante> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_PRE_PLANTE_BAD_CODEBLOC = "PROPERTY_MSG_PRE_PLANTE_BAD_CODEBLOC";
    private static final String PROPERTY_MSG_PRE_PLANTE_BAD_CODEPE = "PROPERTY_MSG_PRE_PLANTE_BAD_CODEPE";
    private static final String PROPERTY_MSG_PRE_PLANTE_BAD_CODEPLACETTE = "PROPERTY_MSG_PRE_PLANTE_BAD_CODEPLACETTE";
    private static final String PROPERTY_MSG_PRE_PLANTE_BAD_DISPOSITIF = "PROPERTY_MSG_PRE_PLANTE_BAD_DISPOSITIF";
    private static final String PROPERTY_MSG_PRE_PLANTE_BAD_METHODE = "PROPERTY_MSG_PRE_PLANTE_BAD_METHODE";
    private static final String PROPERTY_MSG_PRE_PLANTE_BAD_TRAITEMENT = "PROPERTY_MSG_PRE_PLANTE_BAD_TRAITEMENT";
    private static final String PROPERTY_MSG_PRE_PLANTE_ESPECE = "PROPERTY_MSG_PRE_PLANTE_ESPECE";
    private static final String PROPERTY_MSG_PRE_PLANTE_STADE = "PROPERTY_MSG_PRE_PLANTE_STADE";
    private static final String PROPERTY_MSG_PRE_PLANTE_BIOMASSE = "PROPERTY_MSG_PRE_PLANTE_BIOMASSE";
    private static final String PROPERTY_MSG_PRE_PLANTE_PARTIE = "PROPERTY_MSG_PRE_PLANTE_PARTIE";
    private static final String PROPERTY_MESSAGE_BAD_DATE_FORMAT = "PROPERTY_MESSAGE_BAD_DATE_FORMAT";

    IPrelevementPlanteDAO prelevementPlanteDAO;
    IDispositifDAO dispositifDAO;
    IBlocDAO blocDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IPlacetteDAO placetteDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;
    IMethodePrelevementDAO methodePrelevementDAO;
    IPartiePreleveeDAO partiePreleveeDAO;
    IEspecePlanteDAO especePlanteDAO;
    IBiomasseDAO biomasseDAO;
    IStadeDAO stadeDAO;
    ICulturesDAO culturesDAO;
    private Properties commentEn;

    private String[] listeDispoPossible;
    private String[] listeStade;
    private String[] listeBiomasse;
    private String[] listePartie;
    private String[] listeEspece;
    private String[] listeMethode;
    private ConcurrentMap<String, String[]> listeTraitPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listePlacettesPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listeBlocsPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listeParcelleEPossibles = new ConcurrentHashMap();
    private String[] listeBooleansPossible;

    /**
     *
     * @param prelevement
     * @throws BusinessException
     */
    public void createPrelevementPlante(PrelevementPlante prelevement) throws BusinessException {
        try {
            prelevementPlanteDAO.saveOrUpdate(prelevement);
            prelevementPlanteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prelevement");
        }
    }

    /**
     *
     * @param date_prelevement
     * @param georefe
     * @param hauteur
     * @param commentaire
     * @param codeplante
     * @param bbch
     * @param traitement
     * @param bloc
     * @param dispositif
     * @param pelementaire
     * @param placette
     * @param cultures
     * @param partiePrelevee
     * @param stadeDeveloppement
     * @param methodePrelevement
     * @param biomassePrelevee
     * @param dbPrelevementPlante
     * @throws BusinessException
     */
    public void updatePrelevementPlante(LocalDate date_prelevement, String georefe, double hauteur, String commentaire, String codeplante, String bbch,
            DescriptionTraitement traitement, Bloc bloc, Dispositif dispositif, ParcelleElementaire pelementaire, Placette placette,
            Cultures cultures, PartiePrelevee partiePrelevee, StadeDeveloppement stadeDeveloppement, MethodePrelevement methodePrelevement,
            BiomassePrelevee biomassePrelevee, PrelevementPlante dbPrelevementPlante) throws BusinessException {
        try {
            dbPrelevementPlante.setDateplante(date_prelevement);
            dbPrelevementPlante.setReferencement(georefe);
            dbPrelevementPlante.setHauteurcoupe(hauteur);
            dbPrelevementPlante.setCommentaire(commentaire);
            dbPrelevementPlante.setCodeplante(codeplante);
            dbPrelevementPlante.setDispositif(dispositif);
            dbPrelevementPlante.setTraitement(traitement);
            dbPrelevementPlante.setBloc(bloc);
            dbPrelevementPlante.setPelementaire(pelementaire);
            dbPrelevementPlante.setPlacette(placette);
            dbPrelevementPlante.setCodebbch(bbch);
            dbPrelevementPlante.setPartiePrelevee(partiePrelevee);
            dbPrelevementPlante.setCultures(cultures);
            dbPrelevementPlante.setPartiePrelevee(partiePrelevee);
            dbPrelevementPlante.setStadeDeveloppement(stadeDeveloppement);
            dbPrelevementPlante.setMethodePrelevement(methodePrelevement);
            dbPrelevementPlante.setBiomassePrelevee(biomassePrelevee);
            prelevementPlanteDAO.saveOrUpdate(dbPrelevementPlante);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update prelevement");
        }
    }

    /**
     *
     * @param date_prelevement
     * @param georefe
     * @param hauteur
     * @param commentaire
     * @param codeplante
     * @param bbch
     * @param traitement
     * @param bloc
     * @param dispositif
     * @param pelementaire
     * @param placette
     * @param cultures
     * @param partiePrelevee
     * @param stadeDeveloppement
     * @param methodePrelevement
     * @param biomassePrelevee
     * @param dbPrelevementPlante
     * @throws BusinessException
     */
    public void createOeupdatePrelevementPlante(LocalDate date_prelevement, String georefe, double hauteur, String commentaire, String codeplante, String bbch,
            DescriptionTraitement traitement, Bloc bloc, Dispositif dispositif, ParcelleElementaire pelementaire, Placette placette,
            Cultures cultures, PartiePrelevee partiePrelevee, StadeDeveloppement stadeDeveloppement, MethodePrelevement methodePrelevement,
            BiomassePrelevee biomassePrelevee, PrelevementPlante dbPrelevementPlante) throws BusinessException {
        if (dbPrelevementPlante == null) {
            PrelevementPlante plante = new PrelevementPlante(date_prelevement, georefe, hauteur, commentaire, codeplante, bbch, traitement, bloc, dispositif, pelementaire, placette, cultures, partiePrelevee, stadeDeveloppement, methodePrelevement, biomassePrelevee);
            plante.setDateplante(date_prelevement);
            plante.setReferencement(georefe);
            plante.setHauteurcoupe(hauteur);
            plante.setCommentaire(commentaire);
            plante.setCodeplante(codeplante);
            plante.setDispositif(dispositif);
            plante.setTraitement(traitement);
            plante.setBloc(bloc);
            plante.setPelementaire(pelementaire);
            plante.setPlacette(placette);
            plante.setPartiePrelevee(partiePrelevee);
            plante.setCultures(cultures);
            plante.setPartiePrelevee(partiePrelevee);
            plante.setStadeDeveloppement(stadeDeveloppement);
            plante.setMethodePrelevement(methodePrelevement);
            plante.setBiomassePrelevee(biomassePrelevee);
            createPrelevementPlante(plante);
        } else {
            updatePrelevementPlante(date_prelevement, georefe, hauteur, commentaire, codeplante, bbch, traitement, bloc, dispositif, pelementaire, placette, cultures, partiePrelevee, stadeDeveloppement, methodePrelevement, biomassePrelevee, dbPrelevementPlante);
        }

    }

    /**
     *
     * @param date_prelevement
     * @param georefe
     * @param hauteur
     * @param commentaire
     * @param codeplante
     * @param bbch
     * @param traitement
     * @param bloc
     * @param dispositif
     * @param pelementaire
     * @param placette
     * @param cultures
     * @param partiePrelevee
     * @param stadeDeveloppement
     * @param methodePrelevement
     * @param biomassePrelevee
     * @throws BusinessException
     */
    public void persitPrelevementPlante(LocalDate date_prelevement, String georefe, double hauteur, String commentaire, String codeplante, String bbch,
            DescriptionTraitement traitement, Bloc bloc, Dispositif dispositif, ParcelleElementaire pelementaire, Placette placette,
            Cultures cultures, PartiePrelevee partiePrelevee, StadeDeveloppement stadeDeveloppement, MethodePrelevement methodePrelevement,
            BiomassePrelevee biomassePrelevee) throws BusinessException {
        PrelevementPlante prelevementPlante = prelevementPlanteDAO.getByNKey(codeplante).orElse(null);
        createOeupdatePrelevementPlante(date_prelevement, georefe, hauteur, commentaire, codeplante, bbch, traitement, bloc, dispositif, pelementaire, placette, cultures, partiePrelevee, stadeDeveloppement, methodePrelevement, biomassePrelevee, prelevementPlante);

    }

    private void initTraitementDispositifPossibles() {
        Map<String, List<String>> dispositifTraitList = new HashMap<>();
        descriptionTraitementDAO.getAll().stream()
                .forEach(dt -> {
                    List<String> traitementList = dispositifTraitList
                            .computeIfAbsent(dt.getDispositif().getNomDispositif_nomLieu(), k -> new LinkedList<>());
                    if(!traitementList.contains(dt.getTypeTraitement().getCode())){
                        traitementList.add(dt.getTypeTraitement().getCode());
                    }
                });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeTraitPossibles);
        String[] listeDispoPossible2 = readMapOfValuesPossibles(dispositifTraitList, listOfMapOfValuesPossibles, new LinkedList<String>());
        Set<String> s1 = new HashSet<String>(Arrays.asList(listeDispoPossible));
        Set<String> s2 = new HashSet<String>(Arrays.asList(listeDispoPossible2));
        s1.retainAll(s2);
        listeDispoPossible = s1.toArray(new String[s1.size()]);
    }

    private void initDispositifBlocPossibles() {
        Map<String, Map<String, Map<String, List<String>>>> dispBlocPEPlacette = new HashMap<String, Map<String, Map<String, List<String>>>>();
        placetteDAO.getAll(Placette.class)
                .stream()
                .forEach(pl -> {
                    dispBlocPEPlacette
                            .computeIfAbsent(pl.getParcelleElementaire().getBloc().getDispositif().getNomDispositif_nomLieu(), k -> new HashMap<String, Map<String, List<String>>>())
                            .computeIfAbsent(pl.getParcelleElementaire().getBloc().getNom(), k -> new HashMap<String, List<String>>())
                            .computeIfAbsent(pl.getParcelleElementaire().getName(), k -> new LinkedList<String>())
                            .add(pl.getNom());
                });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeBlocsPossibles);
        listOfMapOfValuesPossibles.add(listeParcelleEPossibles);
        listOfMapOfValuesPossibles.add(listePlacettesPossibles);
        listeDispoPossible = readMapOfValuesPossibles(dispBlocPEPlacette, listOfMapOfValuesPossibles, new LinkedList<String>());
    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    private void listeEspecePossibles() {
        List<Cultures> groupesespece = culturesDAO.getAll(Cultures.class);
        String[] listedesEspecePossibles = new String[groupesespece.size() + 1];
        listedesEspecePossibles[0] = "";
        int index = 1;
        for (Cultures especes : groupesespece) {
            listedesEspecePossibles[index++] = especes.getNom();
        }
        this.listeEspece = listedesEspecePossibles;
    }

    private void listePartiePossibles() {
        List<PartiePrelevee> groupesparties = partiePreleveeDAO.getAll(PartiePrelevee.class);
        String[] listedesPartiesPossibles = new String[groupesparties.size() + 1];
        listedesPartiesPossibles[0] = "";
        int index = 1;
        for (PartiePrelevee parties : groupesparties) {
            listedesPartiesPossibles[index++] = parties.getCode_util();
        }
        this.listePartie = listedesPartiesPossibles;
    }

    private void listeMethodePossibles() {
        List<MethodePrelevement> groupesmethodes = methodePrelevementDAO.getAll(MethodePrelevement.class);
        String[] listedesPartiesPossibles = new String[groupesmethodes.size() + 1];
        listedesPartiesPossibles[0] = "";
        int index = 1;
        for (MethodePrelevement methodes : groupesmethodes) {
            listedesPartiesPossibles[index++] = methodes.getNom();
        }
        this.listeMethode = listedesPartiesPossibles;
    }

    private void listeBiomassePossibles() {
        List<BiomassePrelevee> groupesbio = biomasseDAO.getAll();
        String[] listedesBioPossibles = new String[groupesbio.size() + 1];
        listedesBioPossibles[0] = "";
        int index = 1;
        for (BiomassePrelevee biomasse : groupesbio) {
            listedesBioPossibles[index++] = biomasse.getNom();
        }
        this.listeBiomasse = listedesBioPossibles;
    }

    private void listeStadePossibles() {
        List<StadeDeveloppement> groupesstades = stadeDAO.getAll(StadeDeveloppement.class);
        String[] listedesStadePossibles = new String[groupesstades.size() + 1];
        listedesStadePossibles[0] = "";
        int index = 1;
        for (StadeDeveloppement stades : groupesstades) {
            listedesStadePossibles[index++] = stades.getCodebbch();
        }
        this.listeStade = listedesStadePossibles;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String date = tokenizerValues.nextToken();

                String dispo = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(dispo);
                String nomLieu = Dispositif.getNomLieu(dispo);
                String trait = tokenizerValues.nextToken();
                String bloc = tokenizerValues.nextToken();
                String pe = tokenizerValues.nextToken();
                String placette = tokenizerValues.nextToken();
                tokenizerValues.nextToken();

                String culture = tokenizerValues.nextToken();
                String partie = tokenizerValues.nextToken();

                String codefinal = PrelevementPlante.buildCodePlante(date, dispo, nomLieu, trait, bloc, pe, placette, culture, partie);

                PrelevementPlante dbprelplante = prelevementPlanteDAO.getByNKey(codefinal)
                        .orElseThrow(() -> new BusinessException("can't get prelevement"));
                prelevementPlanteDAO.remove(dbprelplante);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            long line = 0;
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, PrelevementPlante.JPA_NAME_ENTITY);
                String datePrelevement = tokenizerValues.nextToken();
                LocalDate date = checkDateFormat(datePrelevement, errorsReport, line);
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String dispo = tokenizerValues.nextToken();
                int indextrait = tokenizerValues.currentTokenIndex();
                String codeDispositif = Dispositif.getNomDispositif(dispo);
                String nomLieu = Dispositif.getNomLieu(dispo);
                final String codeTraitement = tokenizerValues.nextToken();
                int indexbloc = tokenizerValues.currentTokenIndex();
                String nomBloc = tokenizerValues.nextToken();
                int indexpe = tokenizerValues.currentTokenIndex();
                String codeParcelleElementaire = tokenizerValues.nextToken();
                int indexpla = tokenizerValues.currentTokenIndex();
                String nomPlacette = tokenizerValues.nextToken();
                final String pointGeoReferencement = tokenizerValues.nextToken();
                int indexespece = tokenizerValues.currentTokenIndex();
                String nomCulture = tokenizerValues.nextToken();
                int indexpartie = tokenizerValues.currentTokenIndex();
                String partiePrelevee = tokenizerValues.nextToken();
                int indexstade = tokenizerValues.currentTokenIndex();
                String stadePrincipal = tokenizerValues.nextToken();
                String bbch = tokenizerValues.nextToken();
                int indexmethode = tokenizerValues.currentTokenIndex();
                String methode = tokenizerValues.nextToken();
                int indexbio = tokenizerValues.currentTokenIndex();
                String biomasse = tokenizerValues.nextToken();
                double haut = verifieDouble(tokenizerValues, line, false, errorsReport);

                String commentaire = tokenizerValues.nextToken();

                Dispositif dbDispositif = dispositifDAO.getByNKey(codeDispositif, nomLieu).orElse(null);
                if (dbDispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BAD_DISPOSITIF), line, indexdispo, dispo));
                }

                DescriptionTraitement dbtraitement = descriptionTraitementDAO.getByNKey(codeTraitement, dbDispositif).orElse(null);
                if (dbtraitement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BAD_TRAITEMENT), line, indextrait, codeTraitement));
                }

                Bloc dbbloc = blocDAO.getByFindBloc(nomBloc, codeDispositif).orElse(null);
                if (dbbloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BAD_CODEBLOC), line, indexbloc, nomBloc));
                }

                ParcelleElementaire dbpe = parcelleElementaireDAO.getByNKey(codeParcelleElementaire, dbbloc).orElse(null);
                if (dbpe == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BAD_CODEPE), line, indexpe, codeParcelleElementaire));
                }

                Placette dbplacette = placetteDAO.getByNameAndParcelle(nomPlacette, dbpe).orElse(null);
                if (dbplacette == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BAD_CODEPLACETTE), line, indexpla, nomPlacette));
                }

                Cultures dbep = culturesDAO.getByNKey(nomCulture).orElse(null);
                if (dbep == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_ESPECE), line, indexespece, nomCulture));
                }

                PartiePrelevee dbpp = partiePreleveeDAO.getByNKey(partiePrelevee).orElse(null);
                if (dbpp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_PARTIE), line, indexpartie, partiePrelevee));
                }

                StadeDeveloppement dbsd = stadeDAO.getByNKey(stadePrincipal).orElse(null);
                if (dbsd == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_STADE), line, indexstade, stadePrincipal));
                }

                MethodePrelevement dbmp = methodePrelevementDAO.getByNKey(methode).orElse(null);
                if (dbmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BAD_METHODE), line, indexmethode, methode));
                }

                BiomassePrelevee dbbmp = biomasseDAO.getByName(biomasse).orElse(null);
                if (dbbmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_PLANTE_BIOMASSE), line, indexbio, biomasse));
                }

                String codefinal = PrelevementPlante.buildCodePlante(datePrelevement, codeDispositif, nomLieu, codeTraitement, nomBloc, codeParcelleElementaire, nomPlacette, nomCulture, partiePrelevee);

                if (!errorsReport.hasErrors()) {
                    persitPrelevementPlante(date, pointGeoReferencement, haut, commentaire, codefinal, bbch, dbtraitement, dbbloc, dbDispositif, dbpe, dbplacette, dbep, dbpp, dbsd, dbmp, dbbmp);
                }

                values = csvp.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private LocalDate checkDateFormat(String madate, final ErrorsReport errorsReport, long line) throws NumberFormatException {
        try {
            return DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, madate);
        } catch (DateTimeParseException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(PRO_TYPO_SOURCE_PATH, PROPERTY_MESSAGE_BAD_DATE_FORMAT), line, madate, DateUtil.DD_MM_YYYY));
            return null;
        }
    }

    @Override
    protected List<PrelevementPlante> getAllElements() throws BusinessException {
        return prelevementPlanteDAO.getAll();
    }

    String buildStringDateMiseEnService(final PrelevementPlante prelevementPlante) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (prelevementPlante != null && prelevementPlante.getDateplante() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(prelevementPlante.getDateplante(), this.datasetDescriptor.getColumns().get(0).getFormatType());
        }
        return dateDebut;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PrelevementPlante plante) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.buildStringDateMiseEnService(plante),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        String valeurdisp = plante == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getBloc().getDispositif().getCodeDispo()!= null ? plante.getBloc().getDispositif().getNomDispositif_nomLieu() : "";
        ColumnModelGridMetadata columnDisp = new ColumnModelGridMetadata(valeurdisp, listeDispoPossible, null, true, false, true);
        String valeurTrait = plante == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getTraitement().getCode() != null ? plante.getTraitement().getCode() : "";
        ColumnModelGridMetadata columnTrait = new ColumnModelGridMetadata(valeurTrait, listeTraitPossibles, null, true, false, true);
        String nomBloc = plante == null ? "" : plante.getBloc().getNom();
        ColumnModelGridMetadata columnBloc = new ColumnModelGridMetadata(plante == null ? Constantes.STRING_EMPTY : nomBloc, listeBlocsPossibles,
                null, true, false, true);
        String valeurPE = plante == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getPelementaire().getName() != null ? plante.getPelementaire().getName() : "";
        ColumnModelGridMetadata columnPE = new ColumnModelGridMetadata(valeurPE, listeParcelleEPossibles, null, true, false, true);

        String valeurPlacette = plante == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getPlacette().getNom() != null ? plante.getPlacette().getNom() : "";
        ColumnModelGridMetadata columnPlacette = new ColumnModelGridMetadata(valeurPlacette, listePlacettesPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsDispositif = new LinkedList<ColumnModelGridMetadata>();

        List<ColumnModelGridMetadata> refsBloc = new LinkedList<ColumnModelGridMetadata>();

        List<ColumnModelGridMetadata> refsPE = new LinkedList<ColumnModelGridMetadata>();

        refsDispositif.add(columnTrait);
        refsDispositif.add(columnBloc);
        refsBloc.add(columnPE);
        refsPE.add(columnPlacette);

        columnTrait.setRefBy(columnDisp);
        columnBloc.setRefBy(columnDisp);
        columnDisp.setRefs(refsDispositif);

        columnPE.setRefBy(columnBloc);
        columnBloc.setRefs(refsBloc);

        columnPlacette.setRefBy(columnPE);
        columnPE.setRefs(refsPE);

        columnTrait.setValue(valeurTrait);
        columnBloc.setValue(nomBloc);
        columnPE.setValue(valeurPE);
        columnPlacette.setValue(valeurPlacette);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDisp);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTrait);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnBloc);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPE);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPlacette);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getReferencement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getReferencement() != null
                        ? plante.getReferencement() : "",
                        listeBooleansPossible, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCultures() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getCultures().getNom() != null
                        ? plante.getCultures().getNom() : "",
                        listeEspece, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getPartiePrelevee() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getPartiePrelevee().getCode_util() != null
                        ? plante.getPartiePrelevee().getCode_util() : "",
                        listePartie, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getStadeDeveloppement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getStadeDeveloppement().getCodebbch() != null
                        ? plante.getStadeDeveloppement().getCodebbch() : "",
                        listeStade, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCodebbch() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getCodebbch(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getMethodePrelevement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getMethodePrelevement().getNom() != null
                        ? plante.getMethodePrelevement().getNom() : "",
                        listeMethode, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getBiomassePrelevee() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getBiomassePrelevee().getNom() != null
                        ? plante.getBiomassePrelevee().getNom() : "",
                        listeBiomasse, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getHauteurcoupe() == EMPTY_DOUBLE_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getHauteurcoupe(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : plante.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(plante == null || plante.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(plante.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<PrelevementPlante> initModelGridMetadata() {
        initDispositifBlocPossibles();
        initTraitementDispositifPossibles();
        listeBooleanPossibles();
        listeStadePossibles();
        listeBiomassePossibles();
        listeEspecePossibles();
        listePartiePossibles();
        listeMethodePossibles();
        commentEn = localizationManager.newProperties(PrelevementPlante.JPA_NAME_ENTITY, PrelevementPlante.JPA_COLUMN_COMMENTAIRE, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param prelevementPlanteDAO
     */
    public void setPrelevementPlanteDAO(IPrelevementPlanteDAO prelevementPlanteDAO) {
        this.prelevementPlanteDAO = prelevementPlanteDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param blocDAO
     */
    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    /**
     *
     * @param parcelleElementaireDAO
     */
    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     *
     * @param placetteDAO
     */
    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param partiePreleveeDAO
     */
    public void setPartiePreleveeDAO(IPartiePreleveeDAO partiePreleveeDAO) {
        this.partiePreleveeDAO = partiePreleveeDAO;
    }

    /**
     *
     * @param biomasseDAO
     */
    public void setBiomasseDAO(IBiomasseDAO biomasseDAO) {
        this.biomasseDAO = biomasseDAO;
    }

    /**
     *
     * @param stadeDAO
     */
    public void setStadeDAO(IStadeDAO stadeDAO) {
        this.stadeDAO = stadeDAO;
    }

    /**
     *
     * @param descriptionTraitementDAO
     */
    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    /**
     *
     * @param methodePrelevementDAO
     */
    public void setMethodePrelevementDAO(IMethodePrelevementDAO methodePrelevementDAO) {
        this.methodePrelevementDAO = methodePrelevementDAO;
    }

    /**
     *
     * @param especePlanteDAO
     */
    public void setEspecePlanteDAO(IEspecePlanteDAO especePlanteDAO) {
        this.especePlanteDAO = especePlanteDAO;
    }

    /**
     *
     * @return
     */
    public ICulturesDAO getCulturesDAO() {
        return culturesDAO;
    }

    /**
     *
     * @param culturesDAO
     */
    public void setCulturesDAO(ICulturesDAO culturesDAO) {
        this.culturesDAO = culturesDAO;
    }

}
