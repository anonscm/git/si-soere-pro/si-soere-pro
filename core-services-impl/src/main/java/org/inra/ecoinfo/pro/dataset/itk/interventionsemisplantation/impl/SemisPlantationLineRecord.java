/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.impl;

import java.time.LocalDate;

/**
 *
 * @author adiankha
 */
public class SemisPlantationLineRecord implements Comparable<SemisPlantationLineRecord> {

    Long originalLineNumber;
    String codedispositif;
    String codetraitement;
    String codeparcelle;
    String nomplacette;
    String culture;
    String objectculture;
    String filieredestination;
    LocalDate datesemisouplantation;
    String varieteousepage;
    float ratio;
    float quantiteapport;
    String unite;
    String materiel1;
    String materiel2;
    String materiel3;
    int largeurtravail;
    String conditionhumidite;
    String conditiontemperature;
    String vitessevent;
    String observationqualite;
    String nomobservation;
    String niveauatteint;
    String commentaire;

    /**
     *
     */
    public SemisPlantationLineRecord() {
    }

    /**
     *
     * @param originalLineNumber
     * @param codedispositif
     * @param codetraitement
     * @param codeparcelle
     * @param nomplacette
     * @param culture
     * @param objectculture
     * @param filieredestination
     * @param datesemisouplantation
     * @param varieteousepage
     * @param ratio
     * @param quantiteapport
     * @param unite
     * @param materiel1
     * @param materiel2
     * @param materiel3
     * @param largeurtravail
     * @param conditionhumidite
     * @param conditiontemperature
     * @param vitessevent
     * @param observationqualite
     * @param nomobservation
     * @param niveauatteint
     * @param commentaire
     */
    public SemisPlantationLineRecord(Long originalLineNumber, String codedispositif, String codetraitement, String codeparcelle, String nomplacette, String culture, String objectculture, String filieredestination, LocalDate datesemisouplantation, String varieteousepage, float ratio, float quantiteapport, String unite, String materiel1, String materiel2, String materiel3, int largeurtravail, String conditionhumidite, String conditiontemperature, String vitessevent, String observationqualite, String nomobservation, String niveauatteint, String commentaire) {
        this.originalLineNumber = originalLineNumber;
        this.codedispositif = codedispositif;
        this.codetraitement = codetraitement;
        this.codeparcelle = codeparcelle;
        this.nomplacette = nomplacette;
        this.culture = culture;
        this.objectculture = objectculture;
        this.filieredestination = filieredestination;
        this.datesemisouplantation = datesemisouplantation;
        this.varieteousepage = varieteousepage;
        this.ratio = ratio;
        this.quantiteapport = quantiteapport;
        this.unite = unite;
        this.materiel1 = materiel1;
        this.materiel2 = materiel2;
        this.materiel3 = materiel3;
        this.largeurtravail = largeurtravail;
        this.conditionhumidite = conditionhumidite;
        this.conditiontemperature = conditiontemperature;
        this.vitessevent = vitessevent;
        this.observationqualite = observationqualite;
        this.nomobservation = nomobservation;
        this.niveauatteint = niveauatteint;
        this.commentaire = commentaire;
    }

    @Override
    public int compareTo(SemisPlantationLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodedispositif() {
        return codedispositif;
    }

    /**
     *
     * @param codedispositif
     */
    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    /**
     *
     * @return
     */
    public String getCodetraitement() {
        return codetraitement;
    }

    /**
     *
     * @param codetraitement
     */
    public void setCodetraitement(String codetraitement) {
        this.codetraitement = codetraitement;
    }

    /**
     *
     * @return
     */
    public String getCodeparcelle() {
        return codeparcelle;
    }

    /**
     *
     * @param codeparcelle
     */
    public void setCodeparcelle(String codeparcelle) {
        this.codeparcelle = codeparcelle;
    }

    /**
     *
     * @return
     */
    public String getNomplacette() {
        return nomplacette;
    }

    /**
     *
     * @param nomplacette
     */
    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    /**
     *
     * @return
     */
    public String getCulture() {
        return culture;
    }

    /**
     *
     * @param culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     *
     * @return
     */
    public String getObjectculture() {
        return objectculture;
    }

    /**
     *
     * @param objectculture
     */
    public void setObjectculture(String objectculture) {
        this.objectculture = objectculture;
    }

    /**
     *
     * @return
     */
    public String getFilieredestination() {
        return filieredestination;
    }

    /**
     *
     * @param filieredestination
     */
    public void setFilieredestination(String filieredestination) {
        this.filieredestination = filieredestination;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatesemisouplantation() {
        return datesemisouplantation;
    }

    /**
     *
     * @param datesemisouplantation
     */
    public void setDatesemisouplantation(LocalDate datesemisouplantation) {
        this.datesemisouplantation = datesemisouplantation;
    }

    /**
     *
     * @return
     */
    public String getVarieteousepage() {
        return varieteousepage;
    }

    /**
     *
     * @param varieteousepage
     */
    public void setVarieteousepage(String varieteousepage) {
        this.varieteousepage = varieteousepage;
    }

    /**
     *
     * @return
     */
    public float getRatio() {
        return ratio;
    }

    /**
     *
     * @param ratio
     */
    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    /**
     *
     * @return
     */
    public float getQuantiteapport() {
        return quantiteapport;
    }

    /**
     *
     * @param quantiteapport
     */
    public void setQuantiteapport(float quantiteapport) {
        this.quantiteapport = quantiteapport;
    }

    /**
     *
     * @return
     */
    public String getUnite() {
        return unite;
    }

    /**
     *
     * @param unite
     */
    public void setUnite(String unite) {
        this.unite = unite;
    }

    /**
     *
     * @return
     */
    public String getMateriel1() {
        return materiel1;
    }

    /**
     *
     * @param materiel1
     */
    public void setMateriel1(String materiel1) {
        this.materiel1 = materiel1;
    }

    /**
     *
     * @return
     */
    public String getMateriel2() {
        return materiel2;
    }

    /**
     *
     * @param materiel2
     */
    public void setMateriel2(String materiel2) {
        this.materiel2 = materiel2;
    }

    /**
     *
     * @return
     */
    public String getMateriel3() {
        return materiel3;
    }

    /**
     *
     * @param materiel3
     */
    public void setMateriel3(String materiel3) {
        this.materiel3 = materiel3;
    }

    /**
     *
     * @return
     */
    public int getLargeurtravail() {
        return largeurtravail;
    }

    /**
     *
     * @param largeurtravail
     */
    public void setLargeurtravail(int largeurtravail) {
        this.largeurtravail = largeurtravail;
    }

    /**
     *
     * @return
     */
    public String getConditionhumidite() {
        return conditionhumidite;
    }

    /**
     *
     * @param conditionhumidite
     */
    public void setConditionhumidite(String conditionhumidite) {
        this.conditionhumidite = conditionhumidite;
    }

    /**
     *
     * @return
     */
    public String getConditiontemperature() {
        return conditiontemperature;
    }

    /**
     *
     * @param conditiontemperature
     */
    public void setConditiontemperature(String conditiontemperature) {
        this.conditiontemperature = conditiontemperature;
    }

    /**
     *
     * @return
     */
    public String getVitessevent() {
        return vitessevent;
    }

    /**
     *
     * @param vitessevent
     */
    public void setVitessevent(String vitessevent) {
        this.vitessevent = vitessevent;
    }

    /**
     *
     * @return
     */
    public String getObservationqualite() {
        return observationqualite;
    }

    /**
     *
     * @param observationqualite
     */
    public void setObservationqualite(String observationqualite) {
        this.observationqualite = observationqualite;
    }

    /**
     *
     * @return
     */
    public String getNomobservation() {
        return nomobservation;
    }

    /**
     *
     * @param nomobservation
     */
    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteint() {
        return niveauatteint;
    }

    /**
     *
     * @param niveauatteint
     */
    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
