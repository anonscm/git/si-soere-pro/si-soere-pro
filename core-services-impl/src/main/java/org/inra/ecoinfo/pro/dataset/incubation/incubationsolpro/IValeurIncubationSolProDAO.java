/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;

/**
 *
 * @author vjkoyao
 */
public interface IValeurIncubationSolProDAO extends IDAO<ValeurIncubationSolPro> {

    /**
     *
     * @param realNode
     * @param statutvaleur
     * @param mesureIncubationSolPro
     * @return
     */
    public Optional<ValeurIncubationSolPro> getByNKeys(RealNode realNode, String statutvaleur, MesureIncubationSolPro mesureIncubationSolPro);

}
