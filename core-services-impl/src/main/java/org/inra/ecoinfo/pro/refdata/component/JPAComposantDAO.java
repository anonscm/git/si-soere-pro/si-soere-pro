/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.matierepremiere.MatieresPremieres;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
public class JPAComposantDAO extends AbstractJPADAO<Composant> implements IComposantDAO {

    /**
     *
     * @param composant_key
     * @param isProduit
     * @return
     */
    @Override
    public Optional<? extends Composant> getByNKey(String composant_key, boolean isProduit) {
        CriteriaQuery<Composant> query = builder.createQuery(Composant.class);
        Root<Composant> composant = query.from(Composant.class);
        query
                .select(composant)
                .where(
                        builder.equal(composant.get(Composant_.codecomposant), composant_key),
                        builder.equal(composant.get(Composant_.ispro), isProduit)
                );
        return getOptional(query);
    }
    @Override
    public Optional<? extends Composant> getByCodeComposant(String composant_key) {
        CriteriaQuery<Composant> query = builder.createQuery(Composant.class);
        Root<Composant> composant = query.from(Composant.class);
        query
                .select(composant)
                .where(
                        builder.equal(composant.get(Composant_.codecomposant), composant_key)
                );
        return getOptional(query);
    }

    @Override
    public List<? extends Composant> getAll() {
        List<Composant> composants = new LinkedList<>();
        composants.addAll(getAllBy(MatieresPremieres.class, Composant::getCodecomposant));
        composants.addAll(getAllBy(Produits.class, Composant::getCodecomposant));
        return composants;
    }
}
