/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteMoyenneDatatypeManager;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class TestHeadersPlanteMoyenne extends GenericTestHeader {

    public TestHeadersPlanteMoyenne() {
        super();
    }

    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor) throws BusinessException {
        super.testHeaders(parser, versionFile, sessionProperties, encoding, badsFormatsReport, datasetDescriptor);

        final ISessionPropertiesPlanteMoyenne sessionPropertiesPhysicoChimiePRO = (ISessionPropertiesPlanteMoyenne) sessionProperties;
        sessionPropertiesPhysicoChimiePRO.initDate();
        long lineNumber = 0;
        try {
            lineNumber = this.readDispositif(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    IPhysicoChimiePlanteMoyenneDatatypeManager.CODE_DATATYPE_PLANTE_MOYENNE);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, sessionProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 0);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, sessionProperties);
            lineNumber = this.jumpLines(parser, lineNumber, 0);
        } catch (final IOException e) {
            LOGGER.debug("can't read header", e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }

}
