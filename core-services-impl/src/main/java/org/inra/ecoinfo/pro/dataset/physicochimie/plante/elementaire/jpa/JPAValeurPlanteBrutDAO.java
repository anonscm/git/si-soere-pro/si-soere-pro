/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.IValeurPlanteBrutDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire_;

/**
 *
 * @author adiankha
 */
public class JPAValeurPlanteBrutDAO extends AbstractJPADAO<ValeurPlanteElementaire> implements IValeurPlanteBrutDAO{
    
    /**
     *
     * @param realNode
     * @param mesurePlanteBrut
     * @param valeur
     * @param statut
     * @return
     */
    @Override
    public Optional<ValeurPlanteElementaire> getByNKeys(RealNode realNode, MesurePlanteElementaire mesurePlanteBrut, Float valeur, String statut) {
        
        CriteriaQuery<ValeurPlanteElementaire> query = builder.createQuery(ValeurPlanteElementaire.class);
        Root<ValeurPlanteElementaire> v = query.from(ValeurPlanteElementaire.class);
        Join<ValeurPlanteElementaire, RealNode> rnVariable = v.join(ValeurPlanteElementaire_.realNode);
        Join<ValeurPlanteElementaire, MesurePlanteElementaire> m = v.join(ValeurPlanteElementaire_.mesurePlanteElementaire);
        query
                .select(v)
                .where(
                        builder.equal(v.get(ValeurPlanteElementaire_.valeur), valeur),
                        builder.equal(v.get(ValeurPlanteElementaire_.statutvaleur), statut),
                        builder.equal(rnVariable, realNode),
                        builder.equal(m, mesurePlanteBrut)
                );
        return getOptional(query);
    }
   
   
  
    
}
