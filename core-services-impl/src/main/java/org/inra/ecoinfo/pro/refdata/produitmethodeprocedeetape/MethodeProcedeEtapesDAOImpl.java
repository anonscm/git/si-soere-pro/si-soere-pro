package org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.etape.Etapes_;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes_;
import org.inra.ecoinfo.pro.refdata.methodeprocede.MethodeProcedes;
import org.inra.ecoinfo.pro.refdata.methodeprocede.MethodeProcedes_;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.process.Process_;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produit.Produits_;

/**
 *
 * @author adiankha
 *
 */
public class MethodeProcedeEtapesDAOImpl extends AbstractJPADAO<MethodeProcedeEtapes> implements IMethodeProcedeEtapesDAO {

    /**
     *
     * @return
     */
    @Override
    public List<MethodeProcedeEtapes> getAll() {
        return getAll(MethodeProcedeEtapes.class);
    }

    /**
     *
     * @param methodeProcedes
     * @param methodeEtapes
     * @param etape
     * @param ordre
     * @return
     */
    @Override
    public Optional<MethodeProcedeEtapes> getByNKey(MethodeProcedes methodeProcedes, MethodeEtapes methodeEtapes, Etapes etape, int ordre) {
        CriteriaQuery<MethodeProcedeEtapes> query = builder.createQuery(MethodeProcedeEtapes.class);
        Root<MethodeProcedeEtapes> methodeProcedeEtapes = query.from(MethodeProcedeEtapes.class);
        Join<MethodeProcedeEtapes, Etapes> etp = methodeProcedeEtapes.join(MethodeProcedeEtapes_.etapes);
        Join<MethodeProcedeEtapes, MethodeEtapes> me = methodeProcedeEtapes.join(MethodeProcedeEtapes_.methodeetapes);
        Join<MethodeProcedeEtapes, MethodeProcedes> mp = methodeProcedeEtapes.join(MethodeProcedeEtapes_.methodeprocedes);
        query
                .select(methodeProcedeEtapes)
                .where(
                        builder.equal(etp, etape),
                        builder.equal(me, methodeEtapes),
                        builder.equal(mp, methodeProcedes),
                        builder.equal(methodeProcedeEtapes.get(MethodeProcedeEtapes_.ordre), ordre)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeProduits
     * @param intituleProcede
     * @param ordreProcede
     * @param methodeEtape
     * @param intituleEtape
     * @param ordreEtape
     * @return
     */
    @Override
    public Optional<MethodeProcedeEtapes> getByNKey(String codeProduits, String intituleProcede, int ordreProcede, String methodeEtape, String intituleEtape, int ordreEtape) {
        CriteriaQuery<MethodeProcedeEtapes> query = builder.createQuery(MethodeProcedeEtapes.class);
        Root<MethodeProcedeEtapes> methodeProcedeEtapes = query.from(MethodeProcedeEtapes.class);
        Join<MethodeProcedeEtapes, Etapes> etp = methodeProcedeEtapes.join(MethodeProcedeEtapes_.etapes);
        Join<MethodeProcedeEtapes, MethodeEtapes> me = methodeProcedeEtapes.join(MethodeProcedeEtapes_.methodeetapes);
        Join<MethodeProcedeEtapes, MethodeProcedes> mp = methodeProcedeEtapes.join(MethodeProcedeEtapes_.methodeprocedes);
        Join<MethodeProcedes, Process> prc = mp.join(MethodeProcedes_.process);
        Join<MethodeProcedes, Produits> prd = mp.join(MethodeProcedes_.produits);
        query
                .select(methodeProcedeEtapes)
                .where(
                        builder.equal(prd.get(Produits_.codecomposant), codeProduits),
                        builder.equal(prc.get(Process_.process_intitule), intituleProcede),
                        builder.equal(mp.get(MethodeProcedes_.pmp_ordre), ordreProcede),
                        builder.equal(me.get(MethodeEtapes_.me_code), methodeEtape),
                        builder.equal(etp.get(Etapes_.etape_intitule), intituleEtape),
                        builder.equal(methodeProcedeEtapes.get(MethodeProcedeEtapes_.ordre), ordreEtape)
                );
        return getOptional(query);
    }

}
