/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.IMesurePhysicoChimieMoyDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.IValeurSolMoyenneDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableEcarttype;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordPhysicoChimieMoy extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPhysicoChimieMoy.class);

    protected static final String BUNDLE_SOURCE_PATH_MOY_SOL = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.messages";
    private static final String MSG_ERROR_VERSIONFILE_NOT_FOUND_IN_DB = "MSG_ERROR_VERSIONFILE_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_TRAITEMENT_SOLMOY_NOT_FOUND_IN_DB = "MSG_ERROR_TRAITEMENT_SOLMOY_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_SOLMOY_DVUMU_NOT_FOUND_IN_DB = "MSG_ERROR_SOLMOY_DVUMU_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_SOLMOY_NOT_VARIABLEPRO_DB = "MSG_ERROR_SOLMOY_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_SOLMOY_NOT_FOUND_METHODE_DB = "MSG_ERROR_SOLMOY_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_SOLMOY_NOT_UNITEPRO_DB = "MSG_ERROR_SOLMOY_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_SOLMOY_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_SOLMOY_NOT_FOUND_HUMIDITE_DB";
    protected IMesurePhysicoChimieMoyDAO<IMesurePhysicoChimieMoyDAO> mesurePhysicoChimieMoyDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IValeurSolMoyenneDAO valeurMoyenneDAO;

    public ProcessRecordPhysicoChimieMoy() {
        super();
    }

    void buildMesure(final SolMoyLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<SolMoyLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException, PersistenceException {

        LocalDate dateprelevement = mesureLines.getDateprelevement();
        String codetrait = Utils.createCodeFromString(mesureLines.getCodetraitement());
        String codedisp = Utils.createCodeFromString(sessionPropertiesPRO.getDispositif().getCode());
        String nomlabo = mesureLines.getNomlabo();
        String nomcouche = mesureLines.getNomcouche();
        double limitsup = mesureLines.getLimitsup();
        double limitinf = mesureLines.getLimitinf();
        int numerorepet = mesureLines.getNumerorepet();
        Long fichier = mesureLines.getOriginalLineNumber();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);
        if (versionFileDB == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH_MOY_SOL, MSG_ERROR_VERSIONFILE_NOT_FOUND_IN_DB), versionFile));
        }
        final DescriptionTraitement destrait = descriptionTraitementDAO.getByCodeUnique(Utils.createCodeFromString(codedisp + "_" + codetrait)).orElse(null);
        if (destrait == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    ProcessRecordPhysicoChimieMoy.MSG_ERROR_TRAITEMENT_SOLMOY_NOT_FOUND_IN_DB), codedisp, codetrait));
        }
        String variable = mesureLines.getCodevariable();
        float moyenne = mesureLines.getMoyenne();
        float ecarttype = mesureLines.getEcarttype();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);
        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cunite = Utils.createCodeFromString(unite);
        String cmethode = Utils.createCodeFromString(methode);
        String chumitite = Utils.createCodeFromString(humidite);
        String cvariable = Utils.createCodeFromString(variable);

        VariablesPRO dbvariable = variPRODAO.getByCodeUser(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    ProcessRecordPhysicoChimieMoy.MSG_ERROR_SOLMOY_NOT_VARIABLEPRO_DB), cvariable));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    ProcessRecordPhysicoChimieMoy.MSG_ERROR_SOLMOY_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    ProcessRecordPhysicoChimieMoy.MSG_ERROR_SOLMOY_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    ProcessRecordPhysicoChimieMoy.MSG_ERROR_SOLMOY_NOT_FOUND_HUMIDITE_DB), chumitite));
        }
        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(RecorderPRO.getDatatypeFromVersion(versionFile), dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimieMoy.BUNDLE_SOURCE_PATH_MOY_SOL,
                    ProcessRecordPhysicoChimieMoy.MSG_ERROR_SOLMOY_DVUMU_NOT_FOUND_IN_DB), cdatatype, cvariable, cunite, cmethode, chumitite));
        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesurePhysicoChimieSolsMoy dbMesureSol = getOrCreate(dateprelevement, limitsup, limitinf, codetrait, nomlabo, nomcouche, numerorepet, versionFile, destrait, fichier, versionFileDB);
            persitValeurSol(moyenne, ecarttype, dbdvumRealNode, dbMesureSol);
        }
    }

    private MesurePhysicoChimieSolsMoy getOrCreate(LocalDate dateprelevement, double limitsup, double limitinf, String codetrait, String nomlabo, String nomcouche, int numerorepet, final VersionFile versionFile, final DescriptionTraitement destrait, Long fichier, final VersionFile versionFileDB) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        String intString1 = null;
        String intString2 = null;
        intString1 = Double.toString(limitsup);
        intString2 = Double.toString(limitinf);
        String key = dateString + "_" + codetrait + "_" + nomlabo + "_" + intString1 + "_" + intString2 + "_" + nomcouche;
        MesurePhysicoChimieSolsMoy mesurePhysicoChimiemoy = mesurePhysicoChimieMoyDAO.getByKeys(key).orElse(null);
        if (mesurePhysicoChimiemoy == null) {
            mesurePhysicoChimiemoy = new MesurePhysicoChimieSolsMoy(dateprelevement, nomlabo, nomcouche, numerorepet, limitsup, limitinf, versionFile, destrait, fichier);
            mesurePhysicoChimiemoy.setDatePrelevement(dateprelevement);
            mesurePhysicoChimiemoy.setNom_laboratoire(nomlabo);
            mesurePhysicoChimiemoy.setNom_couche(nomcouche);
            mesurePhysicoChimiemoy.setLimit_sup(limitsup);
            mesurePhysicoChimiemoy.setLimit_inf(limitinf);
            mesurePhysicoChimiemoy.setNbre_repetition(numerorepet);
            mesurePhysicoChimiemoy.setVersionfile(versionFileDB);
            mesurePhysicoChimiemoy.setLigneFichierEchange(fichier);
            mesurePhysicoChimiemoy.setKeymesuremoyenne(key);
            mesurePhysicoChimieMoyDAO.saveOrUpdate(mesurePhysicoChimiemoy);
        }
        return mesurePhysicoChimiemoy;
    }

    private void persitValeurSol(float valeur, float ecartype, RealNode dbdvumRealNode, MesurePhysicoChimieSolsMoy mesurePhysicoChimieSols) throws PersistenceException {
        final ValeurPhysicoChimieSolsMoy valeurSol = valeurMoyenneDAO.getByNKeys(dbdvumRealNode, mesurePhysicoChimieSols, ecartype).orElse(null);
        createOrUpdateValeurSol(dbdvumRealNode, mesurePhysicoChimieSols, valeur, ecartype, valeurSol);
    }

    private void CreateDTVQ(final ValeurPhysicoChimieSolsMoy valeurPhysicoChimieSols) throws PersistenceException {
        valeurMoyenneDAO.saveOrUpdate(valeurPhysicoChimieSols);
    }

    private void updateValeurSol(RealNode dbdvumRealNode, MesurePhysicoChimieSolsMoy mesureSol, float valeur, float ecartype, ValeurPhysicoChimieSolsMoy valeurPhysicoChimieSols) throws PersistenceException {
        valeurPhysicoChimieSols.setRealNode(dbdvumRealNode);
        valeurPhysicoChimieSols.setMesurePhysicoChimieSolsMoy(mesureSol);
        valeurPhysicoChimieSols.setValeur(valeur);
        valeurPhysicoChimieSols.setEcarttype(ecartype);
        valeurMoyenneDAO.saveOrUpdate(valeurPhysicoChimieSols);
    }

    void createOrUpdateValeurSol(RealNode dbdvumRealNode, MesurePhysicoChimieSolsMoy mesureSol, float valeur, float ecartype, ValeurPhysicoChimieSolsMoy valeurPhysicoChimieSols) throws PersistenceException {
        if (valeurPhysicoChimieSols == null) {
            ValeurPhysicoChimieSolsMoy valeurSol = new ValeurPhysicoChimieSolsMoy(valeur, ecartype, mesureSol, dbdvumRealNode);
            valeurSol.setRealNode(dbdvumRealNode);
            valeurSol.setMesurePhysicoChimieSolsMoy(mesureSol);
            valeurSol.setValeur(valeur);
            valeurSol.setEcarttype(ecartype);
            CreateDTVQ(valeurSol);
        } else {
            updateValeurSol(dbdvumRealNode, mesureSol, valeur, ecartype, valeurPhysicoChimieSols);
        }
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<SolMoyLineRecord>> lines,
            final SortedSet<SolMoyLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<SolMoyLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<SolMoyLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (SolMoyLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final String fileEncoding,
            final DatasetDescriptorPRO datasetDescriptor) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            final Map<LocalDate, List<SolMoyLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableEcarttype> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptor.getEnTete();
            this.readLines(parser, dbVariables, mesuresMapLines, lineCount, variablesDescriptor, errorsReport, datasetDescriptor);
            final SortedSet<SolMoyLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordPhysicoChimieMoy.class).error(ex.getMessage(), ex);
        }
    }

    private long readLines(final CSVParser parser, final List<DatatypeVariableUnitePRO> dbVariables, final Map<LocalDate, List<SolMoyLineRecord>> lines,
            long lineCount, final Map<String, VariableEcarttype> variablesDescriptor,
            ErrorsReport errorsReport, DatasetDescriptorPRO datasetDescriptor) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate dateprelevement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codetrait = cleanerValues.nextToken();
            final String labonom = cleanerValues.nextToken();
            final String nomcouche = cleanerValues.nextToken();
            double limitsup = Double.parseDouble(cleanerValues.nextToken());
            double limitinf = Double.parseDouble(cleanerValues.nextToken());
            final int numerorepe = Integer.parseInt(cleanerValues.nextToken());
            final String codevpro = cleanerValues.nextToken();
            final float valeurpro = Float.parseFloat(cleanerValues.nextToken());
            final float ecart = Float.parseFloat(cleanerValues.nextToken());
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();

            final SolMoyLineRecord line = new SolMoyLineRecord(dateprelevement, lineCount, codetrait, labonom, nomcouche, limitsup, limitinf, numerorepe, codevpro, valeurpro, ecart, codeunite, codemethode, codehumidite);
            try {
                if (!lines.containsKey(dateprelevement)) {
                    lines.put(dateprelevement, new LinkedList<SolMoyLineRecord>());
                }
                lines.get(dateprelevement).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        dateprelevement, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;

    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public IMesurePhysicoChimieMoyDAO<IMesurePhysicoChimieMoyDAO> getMesurePhysicoChimieMoyDAO() {
        return mesurePhysicoChimieMoyDAO;
    }

    public void setMesurePhysicoChimieMoyDAO(IMesurePhysicoChimieMoyDAO<IMesurePhysicoChimieMoyDAO> mesurePhysicoChimieMoyDAO) {
        this.mesurePhysicoChimieMoyDAO = mesurePhysicoChimieMoyDAO;
    }

    public IDescriptionTraitementDAO getDescriptionTraitementDAO() {
        return descriptionTraitementDAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    public void setValeurMoyenneDAO(IValeurSolMoyenneDAO valeurMoyenneDAO) {
        this.valeurMoyenneDAO = valeurMoyenneDAO;
    }

}
