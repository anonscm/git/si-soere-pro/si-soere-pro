/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.util.List;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class PhysicoChimieExtractor extends MO implements IExtractor {
    
    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";
    
    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";
    
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.physicochimie.messages";
    
    static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";
    
    static final String PROPERTY_MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";
    
    List<AbstractPhysicoChimieExtractor> physicoChimieExtractors;

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        int extractionResult = physicoChimieExtractors.size();
        for (AbstractPhysicoChimieExtractor physicoChimieExtractor : physicoChimieExtractors) {
            try {
                physicoChimieExtractor.extract(parameters);
            } catch (final BusinessException e) {
                if (e instanceof NoExtractionResultException || e.getCause().getClass().equals(NoExtractionResultException.class)) {
                    extractionResult--;
                } else {
                    throw e;
                }
            }            
        }
        
        if (extractionResult == 0) {
            this.sendNotification(String.format(this.localizationManager.getMessage(
                    PhysicoChimieExtractor.BUNDLE_SOURCE_PATH, this.localizationManager.getMessage(
                            PhysicoChimieExtractor.BUNDLE_SOURCE_PATH,
                            PhysicoChimieExtractor.MSG_EXTRACTION_ABORTED))), Notification.ERROR,
                    PhysicoChimieExtractor.PROPERTY_MSG_BADS_RIGHTS, (Utilisateur) this.policyManager.getCurrentUser());
            throw new NoExtractionResultException(this.localizationManager.getMessage(
                    NoExtractionResultException.BUNDLE_SOURCE_PATH,
                    PhysicoChimieExtractor.PROPERTY_MSG_NO_EXTRACTION_RESULT));
        }
        
    }
    
    @Override
    public void setExtraction(Extraction extraction) {
    }
    
    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        Long size = -1L;
        size = physicoChimieExtractors.stream().map((physicoChimieExtractor) -> physicoChimieExtractor.getExtractionSize(parameters)).reduce(size, (accumulator, _item) -> accumulator + _item);
        return size;
    }

    /**
     *
     * @param physicoChimieExtractors
     */
    public void setPhysicoChimieExtractors(List physicoChimieExtractors) {
        this.physicoChimieExtractors = physicoChimieExtractors;
    }
    
}
