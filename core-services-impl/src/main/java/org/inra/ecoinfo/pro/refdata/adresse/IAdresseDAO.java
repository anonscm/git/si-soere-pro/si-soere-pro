/**
 *
 */
package org.inra.ecoinfo.pro.refdata.adresse;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.commune.Commune;


/**
 * @author sophie
 * 
 */
public interface IAdresseDAO extends IDAO<Adresse> {

    /**
     *
     * @param noNomRue
     * @param commune
     * @return
     */
    Optional<Adresse> getByNKey(String noNomRue, Commune commune);

}
