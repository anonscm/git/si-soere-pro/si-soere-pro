/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.utils.exceptions.EmptyFileException;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadValueTypeException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.NullValueException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class GenericTestValues implements ITestValues{
     static protected final Logger                        LOGGER           = LoggerFactory.getLogger(GenericTestValues.class);
    
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;

    @Override
    public void testValues(long startline, CSVParser parser, 
            VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String encoding,
            BadsFormatsReport badsFormatsReport, 
            DatasetDescriptorPRO datasetDescriptor, 
            String datatypeName) throws BusinessException {
        
       long lineNumber = startline;
         final long headerCountLine = lineNumber;
         final Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees =  this.dataTypeVariableUnitePRODAO.
                 getAllDatatypeVariableUnitePROMethodeandCategorie(datatypeName);               
         String[] values;
         // On parcourt chaque ligne du fichier
         ITestDuplicates testDuplicates = this.getTestDuplicates(sessionProperties);
         String[] dates = this.getIntervalDateFromVersion(
                 this.getIntervalDateFromVersion(versionFile));
         try {
             while ((values = parser.getLine()) != null) {
                 int index = 0;
                 lineNumber++;
                 if (testDuplicates != null) {
                     testDuplicates.addLine(values, lineNumber, dates, versionFile);
                 }
                 // On parcourt chaque colonne d'une ligne
                 for (String value = values[index]; index < values.length; index++) {
                     if (index > datasetDescriptor.getColumns().size() - 1) {
                         break;
                     }
                     value = values[index];
                     final Column column = datasetDescriptor.getColumns().get(index);
                     this.checkValue(badsFormatsReport, lineNumber, index, value, column, values,
                             variablesTypesDonnees, datasetDescriptor, sessionProperties);
                 }
             }
         } catch (final IOException e) {
             LOGGER.debug("can't read file",e);
             badsFormatsReport.addException(e);
         }
         if (lineNumber == headerCountLine) {
             badsFormatsReport.addException(new EmptyFileException(RecorderPRO
                     .getPROMessage(RecorderPRO.PROPERTY_MSG_NO_DATA)));
         }
         if (testDuplicates != null && testDuplicates.hasError()) {
             testDuplicates.addErrors(badsFormatsReport);
         }
     }

    
    
      protected void testValues(long startline, CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor, DatatypeVariableUnitePRO dvu) throws BusinessException, PersistenceException {
         
         
        
     }
     
     
     protected IntervalDate getIntervalDateFromVersion(VersionFile versionFile) {
         if (versionFile == null) {
             return null;
         }
         try {
             IntervalDate intervalDate = new IntervalDate(versionFile.getDataset()
                     .getDateDebutPeriode(), versionFile.getDataset().getDateFinPeriode(),
                     RecorderPRO.DD_MM_YYYY_HHMMSS_READ);
             return intervalDate;
         } catch (BadExpectedValueException e) {
             LOGGER.info("pas de version", e);
             return null;
         }
     }
     
     
     
      protected String[] getIntervalDateFromVersion(IntervalDate intervalDate) {
         if (intervalDate == null
                 || !intervalDate.getDateFormat().equals(
                         RecorderPRO.DD_MM_YYYY_HHMMSS_READ)) {
             return null;
         }
         String[] beginDate = intervalDate.getBeginDateToString().split(";");
         String[] endDate = intervalDate.getEndDateToString().split(";");
         String[] dates = new String[4];
         dates[0] = beginDate[0];
         dates[1] = beginDate[1];
         dates[2] = endDate[0];
         dates[3] = endDate[1];
         return dates;
     }
     
      
      
      
//      private void test(String methode, String categorie, 
//             BadsFormatsReport badsFormatsReport, long lineNumber, int index, Column column) {
//         final boolean isBadCategorie = categorie != null ;
//         final boolean isBadMethode = methode != null ;
//         final boolean isOutOfRangeValue = isBadMethode || isBadCategorie;
//         if (isOutOfRangeValue) {
//             badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderPRO
//                     .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE),
//                     lineNumber, index + 1, column.getName(), methode, categorie)));
//         }
//
//     }
     
     private void testUniteMethodeCategorie(String methode, String categorie, /*String unite,*/
             BadsFormatsReport badsFormatsReport, long lineNumber, int index, Column column) {
    
              
         if (categorie == null || methode ==null /*|| unite!= null*/) {
       
             badsFormatsReport.addException(new BadExpectedValueException(String.format(RecorderPRO
                     .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_CMU_VALUE),
                     lineNumber, index + 1, column.getName(), methode, categorie/*,unite*/)));
         }

     }
      
    private ITestDuplicates getTestDuplicates(ISessionPropertiesPRO sessionPropertiesPRO) {
        return sessionPropertiesPRO.getTestDuplicates();
    }

     
    protected void checkValue(final BadsFormatsReport badsFormatsReport, final long lineNumber, final int index, final String value, 
            final  Column column, String[] values, 
            Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees,
            final DatasetDescriptorPRO datasetDescriptor,
            final ISessionPropertiesPRO sessionPropertiesPRO) {
         
        String cleanValue = value;
         if (!column.isNullable() && org.apache.commons.lang.StringUtils.isEmpty(cleanValue)) {
             final String message = String.format(
                     RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_MISSING_VALUE),
                     lineNumber, index + 1, column.getName());
             final NullValueException badFormatException = new NullValueException(message);
             badsFormatsReport.addException(badFormatException);
         }
         if (cleanValue != null) {
             cleanValue = this.cleanValue(cleanValue);
         }
         final String valueType = column.getValueType();
         /*if (RecorderPRO.PROPERTY_CST_FLOAT_TYPE.equals(valueType) && cleanValue.length() > 0) {
             this.checkFloatTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                     column, variablesTypesDonnees, datasetDescriptor, sessionPropertiesPRO);
         } else if (RecorderPRO.PROPERTY_CST_INTEGER_TYPE.equals(valueType)
                 && cleanValue.length() > 0) {
             this.checkIntegerTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                     column, variablesTypesDonnees, datasetDescriptor, sessionPropertiesPRO);
         } else */if (column.isFlag()
                 && RecorderPRO.PROPERTY_CST_DATE_TYPE.equals(column.getFlagType())) {
             this.checkDateTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                     column, variablesTypesDonnees, datasetDescriptor, sessionPropertiesPRO);
         } else if (column.isFlag()
                 && RecorderPRO.PROPERTY_CST_TIME_TYPE.equals(column.getFlagType())) {
             this.checkTimeTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                     column, variablesTypesDonnees, datasetDescriptor, sessionPropertiesPRO);
         } else {
             this.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, cleanValue,
                     column, variablesTypesDonnees, datasetDescriptor, sessionPropertiesPRO);
         }
    }
    
    
    protected String cleanValue(final String value) {
         String returnValue = value;
         returnValue = returnValue.replaceAll(RecorderPRO.CST_COMMA, RecorderPRO.CST_DOT);
         returnValue = returnValue.replaceAll(RecorderPRO.CST_SPACE, org.apache.commons.lang.StringUtils.EMPTY);
         return returnValue;
     }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }


   

    
     protected String checkFloatTypeValue(final String[] values, final BadsFormatsReport badsFormatsReport,
             final long lineNumber,final int index, final String value,
             final Column column, final Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees, 
             final DatasetDescriptorPRO datasetDescriptor,
             final ISessionPropertiesPRO sessionPropertiesPRO) {
        
        
         String floatValue = null;
         try {
             floatValue = value;
          //   this.checkIntervalValue(badsFormatsReport, lineNumber, index, column,
         //            variablesTypesDonnees, datasetDescriptor);
         } catch (final NumberFormatException e) {
             badsFormatsReport.addException(new BadValueTypeException(String.format(
                     RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_FLOAT_VALUE),
                     lineNumber, index + 1, column.getName(), value)));
         }
         return floatValue;
         
    }

    
    
     protected String checkIntegerTypeValue(final String[] values,final BadsFormatsReport badsFormatsReport,
             final long lineNumber,final int index, final String value, final Column column, 
             final Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees,
             final DatasetDescriptorPRO datasetDescriptor,
             final ISessionPropertiesPRO sessionPropertiesPRO) {
        
        
         try {
             final String intValue = value;
        //     this.checkIntervalValue(badsFormatsReport, lineNumber, index, column,
        //             variablesTypesDonnees, datasetDescriptor);
             return intValue;
         } catch (final NumberFormatException e) {
             badsFormatsReport.addException(new BadValueTypeException(String.format(
                     RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_INT_VALUE),
                     lineNumber, index + 1, column.getName(), value)));
             return RecorderPRO.CST_INVALID_BAD_MEASURE;
         }
        
    }

    
    
   protected LocalDate checkDateTypeValue(final String[] values, final BadsFormatsReport badsFormatsReport, final long lineNumber,
           final int index, final String value, final Column column, 
           final Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees,
           final DatasetDescriptorPRO datasetDescriptor, 
           final ISessionPropertiesPRO sessionPropertiesPRO) {
        String dateFormat = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType();
        try {
            final LocalDate date = DateUtil.readLocalDateFromText(dateFormat, value);
            if (!value.equals(DateUtil.getUTCDateTextFromLocalDateTime(date, dateFormat))) {
                badsFormatsReport.addException(new NullValueException(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE), value,
                        lineNumber, index + 1, column.getName(), dateFormat)));
                return null;
            }
            return date;
        } catch (final DateTimeException e) {
            if (!column.isInull() && !value.isEmpty()) {
                badsFormatsReport.addException(new NullValueException(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE), value,
                        lineNumber, index + 1, column.getName(), dateFormat)));
                return null;
            } else {
                return null;
            }
        }
        
    }

    
    
     protected LocalTime checkTimeTypeValue(final String[] values, final BadsFormatsReport badsFormatsReport, final long lineNumber, 
             final int index, final String value,final  Column column,
             final Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees,
             final DatasetDescriptorPRO datasetDescriptor, 
             final ISessionPropertiesPRO sessionPropertiesPRO) {
        String localValue = value;
        String dateFormat = Strings.isNullOrEmpty(column.getFormatType()) ? DateUtil.DD_MM_YYYY : column.getFormatType();
        try {
            LocalTime time;
            localValue = String.format("%-" + dateFormat.length() + "."
                    + dateFormat.length() + "s", value + ":00");
            if (localValue.startsWith("24:00")) {
                localValue = localValue.replaceFirst("24:00", "00:00");
            }
            time = DateUtil.readLocalTimeFromText(dateFormat, localValue);
            if (!localValue.equals(DateUtil.getUTCDateTextFromLocalDateTime(time, dateFormat))) {
                badsFormatsReport
                        .addException(new NullValueException(
                                String.format(RecorderPRO
                                        .getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_TIME),
                                        localValue, lineNumber, index + 1, column.getName(),
                                        dateFormat)));
            }
            return time;
        } catch (final DateTimeException e) {
            badsFormatsReport.addException(new NullValueException(String.format(
                    RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_TIME),
                    localValue, lineNumber, index + 1, column.getName(), dateFormat)));
            return null;
        }
        
    }

    protected void checkOtherTypeValue(String[] values, BadsFormatsReport badsFormatsReport, 
                            long lineNumber, int index, 
                                        String value, Column column,
                                                Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees,
                                                        DatasetDescriptorPRO datasetDescriptor, ISessionPropertiesPRO sessionPropertiesPRO) {
         if (column.isFlag()
                && RecorderPRO.PROPERTY_CST_STATUT_VALEUR_TYPE.equals(column.getFlagType())) {
        }
    }
     
      protected boolean canTest(final Column column,
             final Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees
             ) throws NumberFormatException {
         boolean canTest = RecorderPRO.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType());
         canTest = canTest && variablesTypesDonnees.containsKey(column.getName());
        // canTest = canTest
         //        && RecorderPRO.CST_INVALID_BAD_MEASURE !=floatValue.intValue() ;
         return canTest;
     }
}
