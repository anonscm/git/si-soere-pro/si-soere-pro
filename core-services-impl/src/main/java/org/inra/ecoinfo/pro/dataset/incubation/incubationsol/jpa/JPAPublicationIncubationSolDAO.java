/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsol.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol_;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationIncubationSolDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurIncubationSol> deleteValeurs = builder.createCriteriaDelete(ValeurIncubationSol.class);
        Root<ValeurIncubationSol> valeur = deleteValeurs.from(ValeurIncubationSol.class);
        Subquery<MesureIncubationSol> subquery = deleteValeurs.subquery(MesureIncubationSol.class);
        Root<MesureIncubationSol> m = subquery.from(MesureIncubationSol.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureIncubationSol_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurIncubationSol_.mesureIncubationSol).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureIncubationSol> deleteSequence = builder.createCriteriaDelete(MesureIncubationSol.class);
        Root<MesureIncubationSol> sequence = deleteSequence.from(MesureIncubationSol.class);
        deleteSequence.where(builder.equal(sequence.get(MesureIncubationSol_.versionfile), version));
        delete(deleteSequence);
    }
}
