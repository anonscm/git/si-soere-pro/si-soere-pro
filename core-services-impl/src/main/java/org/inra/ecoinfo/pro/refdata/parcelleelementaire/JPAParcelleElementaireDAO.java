/**
 *
 */
package org.inra.ecoinfo.pro.refdata.parcelleelementaire;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif_;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.utils.Utils;

/**
 * @author sophie
 *
 */
public class JPAParcelleElementaireDAO extends AbstractJPADAO<ParcelleElementaire> implements IParcelleElementaireDAO {

    private static final String QUERY_PELEMENTAIRE_KEY = "from ParcelleElementaire prc where prc.nom=:nom";
    private static final String QUERY_PELEMENTAIRE_BLOC = "from ParcelleElementaire prc where prc.nom=:nom and prc.bloc.nom=:bloc and prc.bloc.dispositif.code=:code";
    private static String QUERY_HQL_GET_BY_CODE = "from ParcelleElementaire prc where prc.nom=:codePE and prc.bloc.dispositif.code=:code";

    /**
     *
     */
    public static String QUERY_HQL_GET_BY_ID = "from ParcelleElementaire prc where prc.nom=:codePE and prc.bloc.dispositif.id=:code";

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<ParcelleElementaire> getAll() {
        return getAll(ParcelleElementaire.class);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO #getByCodeDispositif (org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire, org.inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */

    /**
     *
     * @param code
     * @param dispositif
     * @return
     */

    @Override
    public Optional<ParcelleElementaire> getByNKey(CodeParcelleElementaire codePE, Dispositif dispositif) {
        CriteriaQuery<ParcelleElementaire> query = builder.createQuery(ParcelleElementaire.class);
        Root<ParcelleElementaire> parcelleElementaire = query.from(ParcelleElementaire.class);
        Join<ParcelleElementaire, CodeParcelleElementaire> codeParcelle = parcelleElementaire.join(ParcelleElementaire_.codeParcelleElementaire);
        Join<ParcelleElementaire, Dispositif> dispo = parcelleElementaire.join(ParcelleElementaire_.dispositif);
        query
                .select(parcelleElementaire)
                .where(
                        builder.equal(codeParcelle, codePE),
                        builder.equal(dispo, dispositif)
                );
        return getOptional(query);
    }
    
    /**
     *
     * @param code
     * @param dispositif
     * @return
     */
    @Override
    public Optional<ParcelleElementaire> getByNKey(String nomOrMyCode, Bloc bloc) {
        CriteriaQuery<ParcelleElementaire> query = builder.createQuery(ParcelleElementaire.class);
        Root<ParcelleElementaire> parcelleElementaire = query.from(ParcelleElementaire.class);
        Join<ParcelleElementaire, Bloc> bl = parcelleElementaire.join(ParcelleElementaire_.bloc);
        query
                .select(parcelleElementaire)
                .where(
                        builder.equal(parcelleElementaire.get(ParcelleElementaire_.myCode), Utils.createCodeFromString(nomOrMyCode)),
                        builder.equal(bl, bloc)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO #getByDispositif(org.inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */

    /**
     *
     * @param dispositif
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<ParcelleElementaire> getByDispositif(Dispositif dispositif) {
        CriteriaQuery<ParcelleElementaire> query = builder.createQuery(ParcelleElementaire.class);
        Root<ParcelleElementaire> parcelleElementaire = query.from(ParcelleElementaire.class);
        Join<ParcelleElementaire, Dispositif> dispo = parcelleElementaire.join(ParcelleElementaire_.dispositif);
        query
                .select(parcelleElementaire)
                .where(
                        builder.equal(dispo, dispositif)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO #getByGeolocalisation (org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation)
     */

    /**
     *
     * @param geolocalisation
     * @return
     */

    @Override
    public Optional<ParcelleElementaire> getByGeolocalisation(Geolocalisation geolocalisation) {
        CriteriaQuery<ParcelleElementaire> query = builder.createQuery(ParcelleElementaire.class);
        Root<ParcelleElementaire> parcelleElementaire = query.from(ParcelleElementaire.class);
        Join<ParcelleElementaire, CodeParcelleElementaire> codeParcelle = parcelleElementaire.join(ParcelleElementaire_.codeParcelleElementaire);
        Join<ParcelleElementaire, Dispositif> dispo = parcelleElementaire.join(ParcelleElementaire_.dispositif);
        query
                .select(parcelleElementaire)
                .where(
                        builder.equal(dispo.get(Dispositif_.geolocalisation), geolocalisation)
                );
        return getOptional(query);
    }
}
