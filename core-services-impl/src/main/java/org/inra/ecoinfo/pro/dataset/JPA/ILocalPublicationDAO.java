/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.JPA;

import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;

/**
 *
 * @author adiankha
 */
public interface ILocalPublicationDAO extends IVersionFileDAO {

    /**
     *
     * @param version
     */
    void removeVersion(VersionFile version);

}
