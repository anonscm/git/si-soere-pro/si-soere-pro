/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.pedologie;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.substratpedologique.Substratpedologique;

/**
 *
 * @author adiankha
 */
public interface IPedologieDAO extends IDAO<Pedologie> {

    /**
     *
     * @return
     */
    List<Pedologie> getAll();

    /**
     *
     * @param dispositif
     * @param nomregionalsol
     * @param substratpedologique
     * @return
     */
    Optional<Pedologie> getByNKey(Dispositif dispositif,  Substratpedologique substratpedologique);

}
