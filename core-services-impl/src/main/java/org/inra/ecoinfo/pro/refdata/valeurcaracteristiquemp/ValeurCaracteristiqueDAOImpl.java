package org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere.CaracteristiqueMatierePremieres;
import org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere.CaracteristiqueMatierePremieres_;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.CaracteristiqueValeur;
import org.inra.ecoinfo.pro.refdata.caracteristiquevaleur.CaracteristiqueValeur_;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques_;

/**
 *
 * @author ptcherniati
 */
public class ValeurCaracteristiqueDAOImpl extends AbstractJPADAO<ValeurCaracteristiqueMP> implements IValeurCaracteristiqueMPDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ValeurCaracteristiqueMP> getAll() {
        return getAllBy(ValeurCaracteristiqueMP.class, ValeurCaracteristiqueMP_.caracteristiquematierepremieres);
    }

    /**
     *
     * @param cmp
     * @param cv
     * @return
     */
    @Override
    public Optional<ValeurCaracteristiqueMP> getByNKey(CaracteristiqueMatierePremieres cmp, CaracteristiqueValeur cv) {
        CriteriaQuery<ValeurCaracteristiqueMP> query = builder.createQuery(ValeurCaracteristiqueMP.class);
        Root<ValeurCaracteristiqueMP> valeurCaracteristiqueMP = query.from(ValeurCaracteristiqueMP.class);
        Join<ValeurCaracteristiqueMP, CaracteristiqueMatierePremieres> caracteristiqueMatirePremiere = valeurCaracteristiqueMP.join(ValeurCaracteristiqueMP_.caracteristiquematierepremieres);
        Join<ValeurCaracteristiqueMP, CaracteristiqueValeur> caracteristiqueValeur = valeurCaracteristiqueMP.join(ValeurCaracteristiqueMP_.caracteristiquevaleur);
        query
                .select(valeurCaracteristiqueMP)
                .where(
                        builder.equal(caracteristiqueMatirePremiere, cmp),
                        builder.equal(caracteristiqueValeur, cv)
                );
        return getOptional(query);
    }

    /**
     *
     * @param type
     * @param nom
     * @param valeur
     * @return
     */
    @Override
    public Optional<ValeurCaracteristiqueMP> getByNKey(String type, String nom, String valeur) {
        CriteriaQuery<ValeurCaracteristiqueMP> query = builder.createQuery(ValeurCaracteristiqueMP.class);
        Root<ValeurCaracteristiqueMP> valeurCaracteristiqueMP = query.from(ValeurCaracteristiqueMP.class);
        Join<ValeurCaracteristiqueMP, CaracteristiqueMatierePremieres> caracteristiqueMatirePremiere = valeurCaracteristiqueMP.join(ValeurCaracteristiqueMP_.caracteristiquematierepremieres);
        Join<ValeurCaracteristiqueMP, CaracteristiqueValeur> caracteristiqueValeur = valeurCaracteristiqueMP.join(ValeurCaracteristiqueMP_.caracteristiquevaleur);
        Join<CaracteristiqueMatierePremieres, TypeCaracteristiques> tc = caracteristiqueMatirePremiere.join(CaracteristiqueMatierePremieres_.typecaracteristique);
        query
                .select(valeurCaracteristiqueMP)
                .where(
                        builder.equal(tc.get(TypeCaracteristiques_.tcar_nom), type),
                        builder.equal(caracteristiqueMatirePremiere.get(CaracteristiqueMatierePremieres_.cmp_nom), nom),
                        builder.equal(caracteristiqueValeur.get(CaracteristiqueValeur_.cv_nom), valeur)
                );
        return getOptional(query);
    }

}
