package org.inra.ecoinfo.pro.refdata.typepedologique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;

/**
 *
 * @author ptcherniati
 */
public interface ITypePedologiqueDAO extends IDAO<Typepedologique> {

    /**
     *
     * @return
     */
    List<Typepedologique> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Typepedologique> getByNKey(String nom);

}
