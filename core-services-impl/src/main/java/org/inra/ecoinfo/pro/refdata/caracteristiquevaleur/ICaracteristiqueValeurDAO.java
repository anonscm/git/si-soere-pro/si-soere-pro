package org.inra.ecoinfo.pro.refdata.caracteristiquevaleur;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface ICaracteristiqueValeurDAO extends IDAO<CaracteristiqueValeur> {

    /**
     *
     * @return
     */
    List<CaracteristiqueValeur> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<CaracteristiqueValeur> getByCode(String code);

    /**
     *
     * @param nom
     * @return
     */
    Optional<CaracteristiqueValeur> getByNKey(String nom);

}
