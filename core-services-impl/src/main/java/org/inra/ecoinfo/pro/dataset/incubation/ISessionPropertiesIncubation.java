/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation;

import java.text.ParseException;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;

/**
 *
 * @author vjkoyao
 */
public interface ISessionPropertiesIncubation extends ISessionPropertiesPRO {

    /**
     *
     * @param LocalDate
     * @param time
     * @return
     * @throws ParseException
     */
    String LocalDateToString(String LocalDate, String time) throws ParseException;
}
