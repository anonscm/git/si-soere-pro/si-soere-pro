/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.heterogeneitedispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.heterogeinite.Heterogeinite;

/**
 *
 * @author adiankha
 */
public class HeterogeneiteDispositifDAOImpl extends AbstractJPADAO<HeterogeineteDispositif> implements IHeterogeneiteDispositifDAO {

    /**
     *
     * @return
     */
    @Override
    public List<HeterogeineteDispositif> getAll() {
        return getAll(HeterogeineteDispositif.class);
    }

    /**
     *
     * @param dispositif
     * @param hetero
     * @return
     */
    @Override
    public Optional<HeterogeineteDispositif> getByNKey(Dispositif dispositif, Heterogeinite hetero) {
        CriteriaQuery<HeterogeineteDispositif> query = builder.createQuery(HeterogeineteDispositif.class);
        Root<HeterogeineteDispositif> heterogeineteDispositif = query.from(HeterogeineteDispositif.class);
        Path<Dispositif> disp = heterogeineteDispositif.get(HeterogeineteDispositif_.dispositif);
        Path<Heterogeinite> heter = heterogeineteDispositif.get(HeterogeineteDispositif_.heterogeinite);
        query
                .select(heterogeineteDispositif)
                .where(
                        builder.equal(disp, dispositif),
                        builder.equal(heter, hetero)
                );
        return getOptional(query);
    }

}
