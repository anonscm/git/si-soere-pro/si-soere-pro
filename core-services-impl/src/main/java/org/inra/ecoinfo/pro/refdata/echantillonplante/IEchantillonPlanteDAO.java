/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonplante;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.prelevementplante.PrelevementPlante;

/**
 *
 * @author adiankha
 */
public interface IEchantillonPlanteDAO extends IDAO<EchantillonPlante> {

    /**
     *
     * @return
     */
    List<EchantillonPlante> getAll();

    /**
     *
     * @param prelevement
     * @param ordre
     * @return
     */
    Optional<EchantillonPlante> getByNKey(PrelevementPlante prelevement, long ordre);

    /**
     *
     * @param codeplante
     * @return
     */
    Optional<EchantillonPlante> getByNKey(String codeplante);

}
