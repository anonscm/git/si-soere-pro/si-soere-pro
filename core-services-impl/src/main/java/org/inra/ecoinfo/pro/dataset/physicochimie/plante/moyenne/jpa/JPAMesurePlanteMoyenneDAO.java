/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne_;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.IMesurePlanteMoyenneDAO;

/**
 *
 * @author adiankha
 */
public class JPAMesurePlanteMoyenneDAO extends AbstractJPADAO<MesurePlanteMoyenne> implements IMesurePlanteMoyenneDAO<MesurePlanteMoyenne> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesurePlanteMoyenne> getByKeys(String keymesure) {
        CriteriaQuery<MesurePlanteMoyenne> query = builder.createQuery(MesurePlanteMoyenne.class);
        Root<MesurePlanteMoyenne> m = query.from(MesurePlanteMoyenne.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesurePlanteMoyenne_.keymesure), keymesure)
                );
        return getOptional(query);

    }

}
