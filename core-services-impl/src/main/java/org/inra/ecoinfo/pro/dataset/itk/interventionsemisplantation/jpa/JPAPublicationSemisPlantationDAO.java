/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation_;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationSemisPlantationDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurSemisPlantation> deleteValeurs = builder.createCriteriaDelete(ValeurSemisPlantation.class);
        Root<ValeurSemisPlantation> valeur = deleteValeurs.from(ValeurSemisPlantation.class);
        Subquery<MesureSemisPlantation> subquery = deleteValeurs.subquery(MesureSemisPlantation.class);
        Root<MesureSemisPlantation> m = subquery.from(MesureSemisPlantation.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureSemisPlantation_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurSemisPlantation_.mesureinterventionsemis).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureSemisPlantation> deleteSequence = builder.createCriteriaDelete(MesureSemisPlantation.class);
        Root<MesureSemisPlantation> sequence = deleteSequence.from(MesureSemisPlantation.class);
        deleteSequence.where(builder.equal(sequence.get(MesureSemisPlantation_.versionfile), version));
        delete(deleteSequence);
    }

}
