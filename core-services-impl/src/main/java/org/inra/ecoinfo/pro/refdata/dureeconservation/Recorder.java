/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dureeconservation;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<DureeConservation> {

    IDureeConcervationDAO dureeconservDAO;
    Properties commentaireEn;

    /**
     *
     * @param dureeconser
     * @throws BusinessException
     */
    public void createTEchantillon(DureeConservation dureeconser) throws BusinessException {
        try {
            dureeconservDAO.saveOrUpdate(dureeconser);
            dureeconservDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create dureeconser");
        }
    }

    private void updatecreateDConservqtion(final String nom, String code, String coment, final DureeConservation dbdureeconserv) throws BusinessException {
        try {
            dbdureeconserv.setDc_code(code);
            dbdureeconserv.setDc_nom(nom);
            dbdureeconserv.setDc_comment(coment);
            dureeconservDAO.saveOrUpdate(dbdureeconserv);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update dureeconser");
        }
    }

    private void createOrUpdatecreateDConservqtion(final String nom, String code, String coment, final DureeConservation dbdureeconserv) throws BusinessException {
        if (dbdureeconserv == null) {
            final DureeConservation dureeconser = new DureeConservation(nom, coment);
            dureeconser.setDc_code(code);
            dureeconser.setDc_nom(nom);
            dureeconser.setDc_comment(coment);
            createTEchantillon(dureeconser);
        } else {
            updatecreateDConservqtion(nom, code, coment, dbdureeconserv);
        }
    }

    private void persistDConservqtion(String nom, String code, String coment) throws BusinessException, BusinessException {
        final DureeConservation dbdureeconserv = dureeconservDAO.getByNKey(nom).orElse(null);
        createOrUpdatecreateDConservqtion(nom, code, coment, dbdureeconserv);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();

                dureeconservDAO.remove(dureeconservDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't find dureeconserv")));
                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, DureeConservation.NAME_ENTITY_JPA);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistDConservqtion(intitule, code, commentaire);
                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<DureeConservation> getAllElements() throws BusinessException {
        return dureeconservDAO.gelAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DureeConservation dureeconserv) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dureeconserv == null || dureeconserv.getDc_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : dureeconserv.getDc_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dureeconserv == null || dureeconserv.getDc_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : dureeconserv.getDc_comment(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dureeconserv == null || dureeconserv.getDc_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(dureeconserv.getDc_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<DureeConservation> initModelGridMetadata() {
        commentaireEn = localizationManager.newProperties(DureeConservation.NAME_ENTITY_JPA, DureeConservation.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param dureeconservDAO
     */
    public void setDureeconservDAO(IDureeConcervationDAO dureeconservDAO) {
        this.dureeconservDAO = dureeconservDAO;
    }

}
