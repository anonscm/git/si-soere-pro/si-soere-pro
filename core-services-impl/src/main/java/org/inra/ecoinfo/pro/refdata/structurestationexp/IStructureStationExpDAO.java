/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structurestationexp;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.StationExperimentale;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * @author sophie
 *
 */
public interface IStructureStationExpDAO extends IDAO<StructureStationExperimentale> {

    /**
     *
     * @param structure
     * @param stationExperimentale
     * @return
     */
    Optional<StructureStationExperimentale> getByNKey(Structure structure, StationExperimentale stationExperimentale);

}
