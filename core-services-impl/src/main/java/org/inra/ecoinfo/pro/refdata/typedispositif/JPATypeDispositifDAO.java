/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typedispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeDispositifDAO extends AbstractJPADAO<TypeDispositif> implements ITypeDispositifDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.typedispositif.ITypeDispositifDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<TypeDispositif> getAll() {
        return getAllBy(TypeDispositif.class, TypeDispositif::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typedispositif.ITypeDispositifDAO#getByLibelle (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeDispositif> getByNKey(String libelle) {
        CriteriaQuery<TypeDispositif> query = builder.createQuery(TypeDispositif.class);
        Root<TypeDispositif> typeDispositif = query.from(TypeDispositif.class);
        query
                .select(typeDispositif)
                .where(
                        builder.equal(typeDispositif.get(TypeDispositif_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.typedispositif.ITypeDispositifDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeDispositif> getByCode(String code
    ) {
        CriteriaQuery<TypeDispositif> query = builder.createQuery(TypeDispositif.class);
        Root<TypeDispositif> typeDispositif = query.from(TypeDispositif.class);
        query
                .select(typeDispositif)
                .where(
                        builder.equal(typeDispositif.get(TypeDispositif_.code), code)
                );
        return getOptional(query);
    }

}
