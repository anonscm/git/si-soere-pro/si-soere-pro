package org.inra.ecoinfo.pro.refdata.dispositifobservatoire;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.observatoire.IObservatoireDAO;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<DispositifObservatoire> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    IObservatoireDAO observatoireDAO;
    IDispositifObservatoireDAO dispositifObservatoireDAO;

    /**
     * @return @throws PersistenceException
     */
    private String[] getDispositifPossibles() {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll();
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) {
            dispositifPossibles[index++] = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
        }
        return dispositifPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getObservatoirePossibles() {
        List<Observatoire> lstObservatoires = observatoireDAO.getAll();
        String[] observatoirePossibles = new String[lstObservatoires.size()];
        int index = 0;
        for (Observatoire observatoire : lstObservatoires) {
            observatoirePossibles[index++] = observatoire.getNom();
        }
        return observatoirePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomObservatoire = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                Observatoire observatoire = observatoireDAO.getByNKey(nomObservatoire).orElse(null);
                
                DispositifObservatoire structureObservatoire = dispositifObservatoireDAO.getByNKey(dispositif, observatoire)
                        .orElseThrow(() -> new BusinessException("can't find structureObservatoire"));
                dispositifObservatoireDAO.remove(structureObservatoire);
                
                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<DispositifObservatoire> getAllElements() throws BusinessException {
        return dispositifObservatoireDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DispositifObservatoire dispositifObservatoire) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        String codeDipositifLieu = dispositifObservatoire == null ? "" : dispositifObservatoire.getDispositif().getNomDispositif_nomLieu();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(dispositifObservatoire == null ? Constantes.STRING_EMPTY : codeDipositifLieu, getDispositifPossibles(), null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositifObservatoire == null ? Constantes.STRING_EMPTY : dispositifObservatoire.getObservatoire().getNom(), getObservatoirePossibles(), null, true, false, true));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                int indexdis = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                int indexobs = tokenizerValues.currentTokenIndex();
                String nomObservatoire = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                if (lieu == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line + 1, indexdis + 1, nomLieu));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                if (dispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line + 1, indexdis + 1, codeDispositif));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                
                Observatoire observatoire = observatoireDAO.getByNKey(nomObservatoire).orElse(null);
                if (observatoire == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "OBSERVATOIRE_NONDEFINI"), line + 1, indexobs + 1, nomObservatoire));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                
                DispositifObservatoire structureObservatoire = new DispositifObservatoire(dispositif, observatoire);
                DispositifObservatoire dbDispositifObservatoire = dispositifObservatoireDAO.getByNKey(dispositif, observatoire).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbDispositifObservatoire == null) {
                        dispositifObservatoireDAO.saveOrUpdate(structureObservatoire);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param observatoireDAO the observatoireDAO to set
     */
    public void setObservatoireDAO(IObservatoireDAO observatoireDAO) {
        this.observatoireDAO = observatoireDAO;
    }

    /**
     * @param dispositifObservatoireDAO the dispositifObservatoireDAO to set
     */
    public void setDispositifObservatoireDAO(
            IDispositifObservatoireDAO dispositifObservatoireDAO) {
        this.dispositifObservatoireDAO = dispositifObservatoireDAO;
    }
    
}
