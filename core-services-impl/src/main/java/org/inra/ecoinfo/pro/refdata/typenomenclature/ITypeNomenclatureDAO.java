/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typenomenclature;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface ITypeNomenclatureDAO extends IDAO<TypeNomenclature> {

    /**
     *
     * @param nom
     * @return
     */
    Optional<TypeNomenclature> getByNKey(String nom);

    /**
     *
     * @return
     */
    List<TypeNomenclature> getAll();

}
