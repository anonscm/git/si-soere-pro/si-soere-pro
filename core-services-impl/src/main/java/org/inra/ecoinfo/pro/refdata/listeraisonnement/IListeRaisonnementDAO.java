/**
 *
 */
package org.inra.ecoinfo.pro.refdata.listeraisonnement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IListeRaisonnementDAO extends IDAO<ListeRaisonnement> {

    /**
     *
     * @return
     */
    List<ListeRaisonnement> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<ListeRaisonnement> getByNKey(String libelle);

}
