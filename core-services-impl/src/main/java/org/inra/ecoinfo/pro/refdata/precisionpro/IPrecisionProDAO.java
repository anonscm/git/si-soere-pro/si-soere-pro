/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpro;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IPrecisionProDAO extends IDAO<Precisionpro> {

    /**
     *
     * @return
     */
    List<Precisionpro> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Precisionpro> getByNKey(String nom);

}
