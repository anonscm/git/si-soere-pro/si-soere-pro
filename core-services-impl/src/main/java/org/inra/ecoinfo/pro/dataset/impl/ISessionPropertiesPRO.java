/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.SortedMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 *
 * @author adiankha
 */
public interface ISessionPropertiesPRO extends Serializable {

    /**
     *
     */
    String BUNDLE_NAME = "org.inra.ecoinfo.pro.dataset.messages";

    /**
     *
     */
    String PROPERTY_MSG_UNDEFINED_PERIOD = "PROPERTY_MSG_UNDEFINED_PERIOD";

    /**
     *
     */
    String PROPERTY_MSG_MISSING_LocalDate = "PROPERTY_MSG_MISSING_LocalDate";

    /**
     *
     */
    String PROPERTY_MSG_LocalDate_OFF_LIMITS = "PROPERTY_MSG_LocalDate_OFF_LIMITS";

    /**
     *
     */
    String PROPERTY_MSG_BAD_NAME_FILE = "PROPERTY_MSG_BAD_NAME_FILE";

    /**
     *
     */
    String PROPERTY_MSG_BAD_SITE = "PROPERTY_MSG_BAD_SITE";

    /**
     *
     */
    String PROPERTY_MSG_BAD_DATATYPE = "PROPERTY_MSG_BAD_DATATYPE";

    /**
     *
     */
    String PROPERTY_MSG_BAD_BEGIN_LocalDate = "PROPERTY_MSG_BAD_BEGIN_LocalDate";

    /**
     *
     */
    String PROPERTY_MSG_BAD_END_LocalDate = "PROPERTY_MSG_BAD_END_LocalDate";

    /**
     *
     */
    String FORMAT_FILE = "csv";

    /**
     *
     * @param stringLocalDate
     * @throws BadExpectedValueException
     * @throws DateTimeException
     */
    void addDate(String stringLocalDate) throws BadExpectedValueException, DateTimeException;

    /**
     *
     * @return
     */
    String getCommentaire();

    /**
     *
     * @return
     */
    LocalDateTime getLocalDateDeDebut();

    /**
     *
     * @return
     */
    LocalDateTime getLocalDateDeFin();

    /**
     *
     * @return
     */
    Dispositif getDispositif();

    /**
     *
     * @return
     */
    Produits getProduit();

    /**
     *
     * @return
     */
    ParcelleElementaire getParcelleElementaire();

    /**
     *
     * @return
     */
    DescriptionTraitement getDescriptionTraitement();

    /**
     *
     * @return
     */
    Bloc getBloc();

    /**
     *
     * @return
     */
    Placette getPlacette();

    /**
     *
     * @return
     */
    EchantillonsSol getEchantillonsSol();

    /**
     *
     * @return
     */
    EchantillonsProduit getEchantillonsProduit();

    /**
     *
     * @return
     */
    ProduitDispositif getProduitDispositif();

    /**
     *
     * @return
     */
    DataType getDataType();

    /**
     *
     * @return
     */
    String getDateFormat();

    /**
     *
     * @return
     */
    SortedMap<String, Boolean> getLocalDates();

    /**
     *
     * @return
     */
    ITestDuplicates getDoublonsLine();

    /**
     *
     * @return
     */
    ILocalizationManager getLocalizationManager();

    /**
     *
     * @param version
     * @return
     */
    String getNomDeFichier(VersionFile version);

    /**
     *
     * @return
     */
    ITestDuplicates getTestDuplicates();

    /**
     *
     * @param dispositif
     */
    void setDispositif(Dispositif dispositif);

    /**
     *
     * @param parcelleElementaire
     */
    void setParcelleElementaire(ParcelleElementaire parcelleElementaire);

    /**
     *
     * @param descriptionTraitement
     */
    void setDescriptionTraitement(DescriptionTraitement descriptionTraitement);

    /**
     *
     * @param bloc
     */
    void setBloc(Bloc bloc);

    /**
     *
     * @param placette
     */
    void setPlacette(Placette placette);

    /**
     *
     * @param echantillonsSol
     */
    void setEchantillonsSol(EchantillonsSol echantillonsSol);

    /**
     *
     * @param echantillonsProduit
     */
    void setEchantillonProduit(EchantillonsProduit echantillonsProduit);

    /**
     *
     * @param datatype
     */
    void setDataType(DataType datatype);

    /**
     *
     * @return
     */
    int getVersion();

    /**
     *
     * @param produitDispositif
     */
    void setProduitDispositif(ProduitDispositif produitDispositif);

    /**
     *
     * @param produits
     */
    void setProduits(Produits produits);

    /**
     *
     */
    void initLocalDate();

    /**
     *
     * @param commentaire
     */
    void setCommentaire(String commentaire);

    /**
     *
     * @param LocalDateDeDebut
     */
    void setLocalDateDeDebut(LocalDateTime LocalDateDeDebut);

    /**
     *
     * @param LocalDateDeFin
     */
    void setLocalDateDeFin(LocalDateTime LocalDateDeFin);

    /**
     *
     * @param LocalDateDeFin
     * @param fieldStep
     * @param step
     */
    void setLocalDateDeFin(LocalDateTime LocalDateDeFin, int fieldStep, int step);

    /**
     *
     * @param dateFormat
     */
    void setDateFormat(String dateFormat);

    /**
     *
     * @param LocalDates
     */
    void setLocalDates(SortedMap<String, Boolean> LocalDates);

    /**
     *
     * @param doublonsLine
     */
    void setDoublonsLine(ITestDuplicates doublonsLine);

    /**
     *
     * @param localizationManager
     */
    void setLocalizationManager(ILocalizationManager localizationManager);

    /**
     *
     * @param version
     */
    void setVersion(int version);

    /**
     *
     * @param badsFormatsReport
     */
    void testLocalDates(BadsFormatsReport badsFormatsReport);

    /**
     *
     * @param badsFormatsReport
     */
    void testNonMissingLocalDates(BadsFormatsReport badsFormatsReport);

    /**
     * Gets the date debut traitement.
     *
     *
     * @return the date debut traitement
     */
    LocalDate getDateDebutTraitement();

    /**
     * Gets the date de debut.
     *
     *
     * @return the date de debut
     */
    LocalDateTime getDateDeDebut();

    /**
     * Gets the date de fin.
     *
     *
     * @return the date de fin
     */
    LocalDateTime getDateDeFin();

    /**
     * Gets the dates.
     *
     *
     * @return the dates
     */
    SortedMap<String, Boolean> getDates();

    /**
     * Inits the date.
     */
    void initDate();

    /**
     * Sets the date debut traitement.
     *
     *
     * @param dateDebutTraitement the new date debut traitement {@link Date} the
     * new date debut traitement
     */
    void setDateDebutTraitement(LocalDate dateDebutTraitement);

    /**
     * Sets the date de debut.
     *
     *
     * @param dateDeDebut the new date de debut {@link Date} the new date de
     * debut
     */
    void setDateDeDebut(LocalDateTime dateDeDebut);

    /**
     * Sets the date de fin.
     *
     *
     * @param dateDeFin the new date de fin {@link Date} the new date de fin
     */
    void setDateDeFin(LocalDateTime dateDeFin);

    /**
     * Sets the date de fin.
     *
     *
     * @param dateDeFinp
     * @param chronoUnit
     * @link(Date)
     * @param step int the step {@link Date} the date de fin
     * @link(Date) the date de fin
     */
    void setDateDeFin(LocalDateTime dateDeFinp, ChronoUnit chronoUnit, int step);

    /**
     * Sets the dates.
     *
     *
     * @param dates the dates
     */
    void setDates(SortedMap<String, Boolean> dates);

    /**
     * Test dates.
     *
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @link(BadsFormatsReport) the bads formats report {@link BadsFormatsReport} the bads formats
     *                          report
     */
    void testDates(BadsFormatsReport badsFormatsReport);

    /**
     * Test non missing dates.
     *
     *
     * @param badsFormatsReport
     * @link(BadsFormatsReport) the bads formats report
     * @link(BadsFormatsReport) the bads formats report {@link BadsFormatsReport the bads formats
     *                          report
     */
    void testNonMissingDates(BadsFormatsReport badsFormatsReport);

}
