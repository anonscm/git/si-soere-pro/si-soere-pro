/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.apport.impl;

import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKExtractor;

/**
 *
 * @author adiankha
 */
public class ApportExtractor extends AbstractITKExtractor<MesureApport, ValeurApport> {

    /**
     *
     */
    public static final String APPORT = "apport";

    /**
     *
     */
    protected static final String MAP_INDEX_APPORT = "apport";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return APPORT;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return APPORT;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_APPORT;
    }
}
