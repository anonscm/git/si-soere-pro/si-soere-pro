/**
 *
 */
package org.inra.ecoinfo.pro.refdata.dispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.systemeconduite.ISystemeConduiteEssaiDAO;
import org.inra.ecoinfo.pro.refdata.systemeconduite.SystemeConduiteEssai;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;
import org.inra.ecoinfo.pro.refdata.typedispositif.ITypeDispositifDAO;
import org.inra.ecoinfo.pro.refdata.typedispositif.TypeDispositif;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public class Recorder extends AbstractGenericRecorder<Dispositif> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ITypeDispositifDAO typeDispositifDAO;
    ILieuDAO lieuDAO;
    ISystemeConduiteEssaiDAO systemeConduiteEssaiDAO;
    IDispositifDAO dispositifDAO;

    /**
     * @return @throws PersistenceException
     */
    private String[] getLieuPossibles() throws BusinessException {
        List<Lieu> lstLieux = lieuDAO.getAll();
        String[] lieuPossibles = new String[lstLieux.size()];
        int index = 0;
        for (Lieu lieu : lstLieux) {
            if (lieu.getCommune() != null) {
                lieuPossibles[index++] = lieu.getNom() + " (" + lieu.getCommune().getNomCommune() + ")";
            } else {
                lieuPossibles[index++] = lieu.getNom();
            }
        }
        return lieuPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getSystemeConduitePossibles() throws BusinessException {
        List<SystemeConduiteEssai> lstSystemeConduites = systemeConduiteEssaiDAO.getAll();
        String[] systemConduitePossibles = new String[lstSystemeConduites.size() + 1];
        systemConduitePossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (SystemeConduiteEssai systemeConduiteEssai : lstSystemeConduites) {
            systemConduitePossibles[index++] = systemeConduiteEssai.getLibelle();
        }
        return systemConduitePossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getSystemeProjectionPossibles() throws BusinessException {
        List<SystemeProjection> lstSystemeProjections = systemeProjectionDAO.getAll();
        String[] systemeProjectionPossibles = new String[lstSystemeProjections.size() + 1];
        systemeProjectionPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (SystemeProjection systemeProjection : lstSystemeProjections) {
            systemeProjectionPossibles[index++] = systemeProjection.getNom();
        }
        return systemeProjectionPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getTypeDispositifPossibles() throws BusinessException {
        List<TypeDispositif> lstTypeDispositifs = typeDispositifDAO.getAll();
        String[] typeDispositifPossibles = new String[lstTypeDispositifs.size()];
        int index = 0;
        for (TypeDispositif typeDispositif : lstTypeDispositifs) {
            typeDispositifPossibles[index++] = typeDispositif.getLibelle();
        }
        return typeDispositifPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken(); //nom
                String code = tokenizerValues.nextToken();
                tokenizerValues.nextToken(); //libellé du type de dispositif
                String nomLieuCommune = tokenizerValues.nextToken();
                String nomLieu = Lieu.getNomLieu(nomLieuCommune);
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                mgaServiceBuilder.getRecorder().remove(this.dispositifDAO.getByNKey(code, lieu)
                        .orElseThrow(() -> new BusinessException("can't find Dispositif")));
            }
        } catch (final IOException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#getAllElements()
     */
    @Override
    protected List<Dispositif> getAllElements() throws BusinessException {
        return dispositifDAO.getAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#getNewLineModelGridMetadata(org.inra.ecoinfo.pro.refdata.ZonePrelevement)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Dispositif dispositif) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesRaison = localizationManager.newProperties(Dispositif.TABLE_NAME, RefDataConstantes.COLUMN_RAISON_CHGT_SYSTCONDUITE_DISP, Locale.ENGLISH);
        
        String localizedChampRaison = "";
        
        if (dispositif != null) {
            if (dispositif.getRaisonChgtSystemeConduite() != null) {
                localizedChampRaison = propertiesRaison.containsKey(dispositif.getRaisonChgtSystemeConduite()) ? propertiesRaison.getProperty(dispositif.getRaisonChgtSystemeConduite()) : dispositif.getRaisonChgtSystemeConduite();
            }
        }

        // Nom du dispositif
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : dispositif.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        // Code du dispositif
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : dispositif.getCodeDispo(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // Libellé du type de dispositif
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : dispositif.getTypeDispositif().getLibelle(), getTypeDispositifPossibles(), null, false, false, true));
        
        String nomLieu = dispositif == null ? ""
                : (dispositif.getLieu() == null ? ""
                : (dispositif.getLieu().getCommune() == null ? dispositif.getLieu().getNom() : dispositif.getLieu().getNomLieu_nomCommune()));
        // Nom du lieu
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : nomLieu, getLieuPossibles(), null, true, false, true));

        // Surface totale
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : dispositif.getSurfaceTotale(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Année de début du dispositif
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY
                        : (dispositif.getAnneeDebutDispositif() == null ? Constantes.STRING_EMPTY : dispositif.getAnneeDebutDispositif()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        // Année de fin du dispositif
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY
                        : (dispositif.getAnneeFinDispositif() == null ? Constantes.STRING_EMPTY : dispositif.getAnneeFinDispositif()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Année arrêt d'épandage
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY
                        : (dispositif.getAnneeArretEpandage() == null ? Constantes.STRING_EMPTY : dispositif.getAnneeArretEpandage()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Système de conduite du(des) dispositif(s)
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : dispositif.getSystemeConduite().getLibelle(), getSystemeConduitePossibles(), null, true, false, true));
        
        String isChgtSystConduiteDisp = dispositif == null ? ""
                : (dispositif.getIsChgtSystemeConduite() == null ? ""
                : (dispositif.getIsChgtSystemeConduite() ? RefDataConstantes.valBooleanObligatoire[0] : RefDataConstantes.valBooleanObligatoire[1]));

        // Changement de système de conduite en cours d'expérimentation (oui/non)
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : isChgtSystConduiteDisp, RefDataConstantes.valBooleanObligatoire, null, false, false, true));

        // Changement de système de conduite en cours d'expérimentation
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY
                        : (dispositif.getChgtSystemeConduite() == null ? Constantes.STRING_EMPTY : dispositif.getChgtSystemeConduite().getLibelle()), getSystemeConduitePossibles(), null, false, false, false));

        // Année de changement de système de conduite
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY
                        : (dispositif.getAnneeChgtSystemeConduite() == null ? Constantes.STRING_EMPTY : dispositif.getAnneeChgtSystemeConduite()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Raison du changement de système de conduite
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY
                        : (dispositif.getRaisonChgtSystemeConduite() == null ? Constantes.STRING_EMPTY : dispositif.getRaisonChgtSystemeConduite()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : localizedChampRaison, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Géolocalisation : système de projection
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : (dispositif.getGeolocalisation() == null ? Constantes.STRING_EMPTY
                        : (dispositif.getGeolocalisation().getSystemeProjection() == null ? Constantes.STRING_EMPTY : dispositif.getGeolocalisation().getSystemeProjection().getNom())),
                        getSystemeProjectionPossibles(), null, false, false, false));
        // Géolocalisation : latitude
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : (dispositif.getGeolocalisation() == null ? Constantes.STRING_EMPTY : dispositif.getGeolocalisation().getLatitude()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        // Géolocalisation : longitude
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dispositif == null ? Constantes.STRING_EMPTY : (dispositif.getGeolocalisation() == null ? Constantes.STRING_EMPTY : dispositif.getGeolocalisation().getLongitude()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /**
     * @param typeDispositif
     * @param typeCulture
     * @param lieu
     * @param systemeConduite
     * @param changementSystemeConduite
     * @param geolocalisation
     * @param nom
     * @param code
     * @param surfaceTotale
     * @param isChgtSystConduite
     * @param anneeDebut
     * @param anneeFin
     * @param anneeArretEp
     * @param anneeChgtSystemeConduite
     * @param raisonChgtSystemeConduite
     * @param dbDispositif
     * @param dispositif
     * @throws PersistenceException
     */
    private void createOrUpdate(TypeDispositif typeDispositif, Lieu lieu, SystemeConduiteEssai systemeConduite, SystemeConduiteEssai changementSystemeConduite, Geolocalisation geolocalisation, String nom, String code,
            Float surfaceTotale, Boolean isChgtSystConduite, String anneeDebut, String anneeFin, String anneeArretEp, String anneeChgtSystemeConduite, String raisonChgtSystemeConduite, Dispositif dbDispositif, Dispositif dispositif) throws BusinessException {
        try {
            if (dbDispositif == null) {
                dispositifDAO.saveOrUpdate(dispositif);
            } else {
                // si on met à jour le dispositif en lui enlevant sa geolocalisation, alors supprimer son ancienne geolocalisation de la base
                if (geolocalisation == null) {
                    Geolocalisation dbDispGeolocalisation = dbDispositif.getGeolocalisation();
                    if (dbDispGeolocalisation != null) {
                        geolocalisationDAO.remove(dbDispGeolocalisation);
                    }
                }
                
                dbDispositif.setTypeDispositif(typeDispositif);
                dbDispositif.setLieu(lieu);
                dbDispositif.setSystemeConduite(systemeConduite);
                dbDispositif.setChgtSystemeConduite(changementSystemeConduite);
                dbDispositif.setName(nom);
                dbDispositif.setCode(code);
                dbDispositif.setSurfaceTotale(surfaceTotale);
                dbDispositif.setIsChgtSystemeConduite(isChgtSystConduite);
                dbDispositif.setAnneeDebutDispositif(anneeDebut);
                dbDispositif.setAnneeFinDispositif(anneeFin);
                dbDispositif.setAnneeArretEpandage(anneeArretEp);
                dbDispositif.setAnneeChgtSystemeConduite(anneeChgtSystemeConduite);
                dbDispositif.setRaisonChgtSystemeConduite(raisonChgtSystemeConduite);
                dbDispositif.setGeolocalisation(geolocalisation);
                
                dispositifDAO.saveOrUpdate(dbDispositif);
            }
        } catch (PersistenceException e) {
            throw new BusinessException("can't saveOrUpdateDispositif", e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            
            String[] values = null;
            values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Dispositif.TABLE_NAME);
                
                String nom = tokenizerValues.nextToken();
                String code = tokenizerValues.nextToken();
                int indexType = tokenizerValues.currentTokenIndex();
                String libelleTypeDisp = tokenizerValues.nextToken();
                int indexLieu = tokenizerValues.currentTokenIndex();
                String nomLieuCommune = tokenizerValues.nextToken();
                String surfaceTotale = tokenizerValues.nextToken();
                int indexAnneeDebut = tokenizerValues.currentTokenIndex();
                String anneeDebut = tokenizerValues.nextToken();
                int indexAnneeFin = tokenizerValues.currentTokenIndex();
                String anneeFin = tokenizerValues.nextToken();
                int indexAnneeArretEp = tokenizerValues.currentTokenIndex();
                String anneeArretEp = tokenizerValues.nextToken();
                int indexSysteme = tokenizerValues.currentTokenIndex();
                String systemeConduiteDisp = tokenizerValues.nextToken();
                int indexIsChgt = tokenizerValues.currentTokenIndex();
                String isChangementSystemeConduite = tokenizerValues.nextToken();
                int indexCh = tokenizerValues.currentTokenIndex();
                String chgtSystemeConduite = tokenizerValues.nextToken();
                int indexAnneeChgt = tokenizerValues.currentTokenIndex();
                String anneeChgtSystemeConduite = tokenizerValues.nextToken();
                String raisonChgtSystemeConduite = tokenizerValues.nextToken();
                int indexSystProj = tokenizerValues.currentTokenIndex();
                String systemeProjection = tokenizerValues.nextToken();
                String latitude = tokenizerValues.nextToken();
                String longitude = tokenizerValues.nextToken();
                String nomLieu = Lieu.getNomLieu(nomLieuCommune);
                
                TypeDispositif typeDispositif = verifieTypeDispositif(libelleTypeDisp, line + 1, indexType + 1, errorsReport);
                Lieu lieu = verifieLieu(nomLieu, line + 1, indexLieu + 1, errorsReport);
                SystemeConduiteEssai systemeConduite = verifieSystemeConduiteEssai(systemeConduiteDisp, line + 1, indexSysteme + 1, errorsReport);
                verifieIsChangementSystemeConduite(isChangementSystemeConduite, line + 1, indexIsChgt + 1, errorsReport);
                SystemeConduiteEssai changementSystemeConduite = verifieChgtSystemeConduite(chgtSystemeConduite, line + 1, indexCh + 1, errorsReport);
                verifieFormatToutesDates(anneeFin, anneeDebut, anneeArretEp, anneeChgtSystemeConduite, line + 1, indexAnneeFin + 1, indexAnneeDebut + 1,
                        indexAnneeArretEp + 1, indexAnneeChgt + 1, errorsReport);
                verifieAnneesToutesDates(nom + " (" + nomLieu + ")", anneeFin, anneeDebut, anneeArretEp, anneeChgtSystemeConduite, line, indexAnneeFin, indexAnneeDebut,
                        indexAnneeArretEp, indexAnneeChgt, errorsReport);
                
                Dispositif dbDispositif = dispositifDAO.getByNKey(code, lieu).orElse(null);

                // La géolocalisation n'est pas obligatoire
                Geolocalisation geolocalisation = null;
                if (code != null && !code.isEmpty() && lieu != null) {
                    //on traite la géolocalisation si pas d'erreur sur le Lieu et donc pas sur dbDispositif
                    Geolocalisation ancienGeolocZone = dbDispositif != null ? dbDispositif.getGeolocalisation() : null;
                    geolocalisation = traiteGeolocalisation(systemeProjection, latitude, longitude, geolocalisation, dbDispositif, ancienGeolocZone, errorsReport, line + 1, indexSystProj + 1);
                }
                
                if (!errorsReport.hasErrors()) {
                    // création du dispositif
                    Float surfaceTotaleDispositif = surfaceTotale == null ? null : new Float(surfaceTotale);
                    
                    Boolean isChgtSystConduite = isChangementSystemeConduite != null && isChangementSystemeConduite.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[0]) ? Boolean.valueOf("true") : Boolean.valueOf("false");
                    if (!isChgtSystConduite) {
                        changementSystemeConduite = null;
                        anneeChgtSystemeConduite = null;
                        raisonChgtSystemeConduite = null;
                    }
                    
                    Dispositif dispositif = new Dispositif(typeDispositif, lieu, systemeConduite, changementSystemeConduite, geolocalisation, nom, code, surfaceTotaleDispositif, isChgtSystConduite, anneeDebut, anneeFin, anneeArretEp, anneeChgtSystemeConduite, raisonChgtSystemeConduite);
                    createOrUpdate(typeDispositif, lieu, systemeConduite, changementSystemeConduite, geolocalisation, nom, code, surfaceTotaleDispositif, isChgtSystConduite, anneeDebut, anneeFin, anneeArretEp, anneeChgtSystemeConduite, raisonChgtSystemeConduite,
                            dbDispositif, dispositif);
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param libelleTypeDisp
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws PersistenceException
     */
    private TypeDispositif verifieTypeDispositif(String libelleTypeDisp, long line, int index, ErrorsReport errorsReport) throws BusinessException {
        TypeDispositif typeDispositif = typeDispositifDAO.getByNKey(libelleTypeDisp).orElse(null);
        if (typeDispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "TYPDISP_NONDEFINI"), line, index, libelleTypeDisp));
        }
        
        return typeDispositif;
    }

    /**
     * @param nomLieu
     * @param line
     * @param indexDispLieu
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }
        
        return lieu;
    }

    /**
     * @param systemeConduiteDisp
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private SystemeConduiteEssai verifieSystemeConduiteEssai(String systemeConduiteDisp, long line, int index, ErrorsReport errorsReport) {
        SystemeConduiteEssai systemeConduite = systemeConduiteEssaiDAO.getByNKey(systemeConduiteDisp).orElse(null);
        if (systemeConduite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "SYSTEMECONDUITE_NONDEFINI"), line, index, systemeConduiteDisp));
        }
        
        return systemeConduite;
    }

    /**
     * @param isChangementSystemeConduite
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieIsChangementSystemeConduite(String isChangementSystemeConduite, long line, int index, ErrorsReport errorsReport) {
        if (isChangementSystemeConduite == null || isChangementSystemeConduite != null && !isChangementSystemeConduite.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[0])
                && !isChangementSystemeConduite.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[1])) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ERROR_BOOLEAN"), line, index, datasetDescriptor.getColumns().get(7).getName()));
        }
    }

    /**
     * @param chgtSystemeConduite
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private SystemeConduiteEssai verifieChgtSystemeConduite(String chgtSystemeConduite, long line, int index, ErrorsReport errorsReport) {
        SystemeConduiteEssai changementSystemeConduite = null;
        if (chgtSystemeConduite != null) {
            changementSystemeConduite = systemeConduiteEssaiDAO.getByNKey(chgtSystemeConduite).orElse(null);
            if (changementSystemeConduite == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "SYSTEMECONDUITE_NONDEFINI"), line, index, chgtSystemeConduite));
            }
        }
        
        return changementSystemeConduite;
    }

    /**
     * @param anneeFin
     * @param anneeDebut
     * @param anneeArretEp
     * @param anneeChgtSystemeConduite
     * @param line
     * @param indexAnneeFin
     * @param indexAnneeDebut
     * @param indexAnneeArretEp
     * @param indexAnneeChgt
     * @param errorsReport
     */
    private void verifieFormatToutesDates(String anneeFin, String anneeDebut, String anneeArretEp, String anneeChgtSystemeConduite, long line, int indexAnneeFin, int indexAnneeDebut,
            int indexAnneeArretEp, int indexAnneeChgt, ErrorsReport errorsReport) {
        if (anneeFin != null) {
            verifieAnneeFormat(anneeFin, line, indexAnneeFin, errorsReport);
        }
        
        verifieAnneeFormat(anneeDebut, line, indexAnneeDebut, errorsReport);
        
        if (anneeArretEp != null) {
            verifieAnneeFormat(anneeArretEp, line + 1, indexAnneeArretEp + 1, errorsReport);
        }
        
        if (anneeChgtSystemeConduite != null) {
            verifieAnneeFormat(anneeChgtSystemeConduite, line + 1, indexAnneeChgt + 1, errorsReport);
        }
    }

    /**
     * @param annee
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieAnneeFormat(String annee, long line, int index, ErrorsReport errorsReport) {
        if (annee != null) {
            try {
                DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, String.format("01/01/%s", annee));
            } catch (DateTimeParseException e) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
            }
        }
    }

    /**
     * @param nomDispLieu
     * @param anneeFin
     * @param anneeDebut
     * @param anneeArretEp
     * @param anneeChgtSystemeConduite
     * @param line
     * @param indexAnneeFin
     * @param indexAnneeDebut
     * @param indexAnneeArretEp
     * @param indexAnneeChgt
     * @param errorsReport
     */
    private void verifieAnneesToutesDates(String nomDispLieu, String anneeFin, String anneeDebut, String anneeArretEp, String anneeChgtSystemeConduite, long line, int indexAnneeFin, int indexAnneeDebut,
            int indexAnneeArretEp, int indexAnneeChgt, ErrorsReport errorsReport) {
        if (anneeFin != null) {
            verifieAnnees(nomDispLieu, anneeDebut, anneeFin, "ERROR_DATE_DISPOSITIF", line + 1, indexAnneeDebut + 1, indexAnneeFin + 1, errorsReport);
        }
        if (anneeFin != null && anneeArretEp != null) {
            verifieAnnees(nomDispLieu, anneeArretEp, anneeFin, "ERROR_DATE_ARRET_FIN", line + 1, indexAnneeArretEp + 1, indexAnneeFin + 1, errorsReport);
        }
        if (anneeArretEp != null) {
            verifieAnnees(nomDispLieu, anneeDebut, anneeArretEp, "ERROR_DATE_ARRET_DEB", line + 1, indexAnneeDebut + 1, indexAnneeArretEp + 1, errorsReport);
        }
        if (anneeFin != null && anneeChgtSystemeConduite != null) {
            verifieAnnees(nomDispLieu, anneeChgtSystemeConduite, anneeFin, "ERROR_CHGT_FIN", line + 1, indexAnneeChgt + 1, indexAnneeFin + 1, errorsReport);
        }
        if (anneeChgtSystemeConduite != null) {
            verifieAnnees(nomDispLieu, anneeDebut, anneeChgtSystemeConduite, "ERROR_CHGT_DEB", line + 1, indexAnneeDebut + 1, indexAnneeChgt + 1, errorsReport);
        }
    }

    /**
     * @param annee1
     * @param annee2
     * @param nomDispositifNomLieu
     * @param line
     * @param indexD
     * @param indexF
     * @param errorsReport
     */
    private void verifieAnnees(String nomDispositifNomLieu, String annee1, String annee2, String param1, long line, int indexD, int indexF, ErrorsReport errorsReport) {
        if (annee2 != null && new Integer(annee2) < new Integer(annee1)) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, param1), line, indexF, annee2, nomDispositifNomLieu, annee1));
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#verifLocalisationExiste(org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation, java.lang.Object, org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport)
     */

    /**
     *
     * @param dbGeolocalisation
     * @param dbDispositif
     * @param errorsReport
     * @return
     * @throws BusinessException
     */

    @Override
    public Geolocalisation verifLocalisationExiste(Geolocalisation dbGeolocalisation, Dispositif dbDispositif, ErrorsReport errorsReport) throws BusinessException {
        Geolocalisation geolocalisation = null;

        // vérifie si cette géolocalisation est déjà associée à un dispositif et retrouve à quel dispositif
        Dispositif dispositifGeolocalise = dispositifDAO.getByGeolocalisation(dbGeolocalisation).orElse(null);
        
        if (dispositifGeolocalise != null) {
            if (dbDispositif != null) {
                // la géolocalisation du dispositif est à modifier en base mais la géolocalisation saisie
                // est déjà associée à un autre dispositif en base. Dans ce cas erreur, deux dispositifs différents
                // ne pourront pas être géolocalisés au même endroit
                if (!dbDispositif.equals(dispositifGeolocalise)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPGEOLOC_EXISTE"), dispositifGeolocalise.getCode()));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                } else // la géolocalisation du dispositif sera à modifier en base
                {
                    geolocalisation = dbGeolocalisation;
                }
            }
        }
        
        return geolocalisation;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param systemeConduiteEssaiDAO the systemeConduiteEssaiDAO to set
     */
    public void setSystemeConduiteEssaiDAO(ISystemeConduiteEssaiDAO systemeConduiteEssaiDAO) {
        this.systemeConduiteEssaiDAO = systemeConduiteEssaiDAO;
    }

    /**
     * @param typeDispositifDAO the typeDispositifDAO to set
     */
    public void setTypeDispositifDAO(ITypeDispositifDAO typeDispositifDAO) {
        this.typeDispositifDAO = typeDispositifDAO;
    }
}
