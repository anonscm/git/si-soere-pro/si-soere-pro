/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolpromoyennees.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy_;
import org.inra.ecoinfo.pro.extraction.incubation.IIncubationDatatypeManager;
import org.inra.ecoinfo.pro.extraction.incubation.jpa.AbstractJPAIncubationDAO;
import org.inra.ecoinfo.pro.synthesis.incubationsolpromoy.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author vjkoyao
 */
public class JPAIncubationSolProMoyDAO extends AbstractJPAIncubationDAO<MesureIncubationSolProMoy, ValeurIncubationSolProMoy>{

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurIncubationSolProMoy> getValeurIncubationClass() {
        return ValeurIncubationSolProMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureIncubationSolProMoy> getMesureIncubationClass() {
        return MesureIncubationSolProMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurIncubationSolProMoy, MesureIncubationSolProMoy> getMesureAttribute() {
        return ValeurIncubationSolProMoy_.mesureIncubationSolProMoy;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IIncubationDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_PRO_MOY;
    }
}
