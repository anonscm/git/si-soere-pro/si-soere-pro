package org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.etape.IEtapeDAO;
import org.inra.ecoinfo.pro.refdata.methodeetape.IMethodeEtapeDAO;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.methodeprocede.IMethodeProcedesDAO;
import org.inra.ecoinfo.pro.refdata.methodeprocede.MethodeProcedes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<MethodeProcedeEtapes> {
    
    private static final String PROPERTY_MSG_PPME_BAD_ME = "PROPERTY_MSG_PPME_BAD_ME";
    
    private static final String PROPERTY_MSG_PPME_BAD_PROD_PROCEDE = "PROPERTY_MSG_PPME_BAD_PROD_PROCEDE";
    
    private static final String PROPERTY_MSG_PPME_BAD_ETAPE = "PROPERTY_MSG_PPME_BAD_ETAPE";
    
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    
    /**
     *
     */
    protected IMethodeProcedeEtapesDAO methodeprocedeetapeDAO;

    /**
     *
     */
    protected IEtapeDAO etapeDAO;

    /**
     *
     */
    protected IMethodeEtapeDAO methodeetapeDAO;

    /**
     *
     */
    protected IMethodeProcedesDAO methodeprocedeDAO;
    
    private String[] listeEtapesPossibles;
    private Map<String, String[]> listeProcedesPossibles;
    private String[] listeMEtapesPossibles;
    private String[] listeProduitsPossibles;
    Properties commentaireEn;
    
    private void CreateMethodeProcedeEtapes(final MethodeProcedeEtapes methodeprocedeetapes) throws BusinessException {
        try {
            methodeprocedeetapeDAO.saveOrUpdate(methodeprocedeetapes);
            methodeprocedeetapeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create methodeprocedeetapes");
        }
    }
    
    private void updateBDMethodeProcedeEtapes(final MethodeProcedes methodep, Etapes etape, MethodeEtapes metape, int ordre, String commentaire, final MethodeProcedeEtapes dbmethodeprocedeetapes) throws BusinessException {
        try {
            dbmethodeprocedeetapes.setMethodeprocedes(methodep);
            dbmethodeprocedeetapes.setEtapes(etape);
            dbmethodeprocedeetapes.setMethodeetapes(metape);
            dbmethodeprocedeetapes.setOrdre(ordre);
            dbmethodeprocedeetapes.setCommentaire(commentaire);
            methodeprocedeetapeDAO.saveOrUpdate(dbmethodeprocedeetapes);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update methodeprocedeetapes");
        }
    }
    
    private void createOrUpdateMethodeProcedeEtapes(final MethodeProcedes methodep, Etapes etape, MethodeEtapes metape, int ordre, String commentaire, final MethodeProcedeEtapes dbmethodeprocedeetapes) throws BusinessException {
        if (dbmethodeprocedeetapes == null) {
            final MethodeProcedeEtapes methodeprocedeetapes = new MethodeProcedeEtapes(methodep, etape, metape, ordre, commentaire);
            methodeprocedeetapes.setMethodeprocedes(methodep);
            methodeprocedeetapes.setMethodeetapes(metape);
            methodeprocedeetapes.setEtapes(etape);
            methodeprocedeetapes.setOrdre(ordre);
            methodeprocedeetapes.setCommentaire(commentaire);
            CreateMethodeProcedeEtapes(methodeprocedeetapes);
        } else {
            updateBDMethodeProcedeEtapes(methodep, etape, metape, ordre, commentaire, dbmethodeprocedeetapes);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MethodeProcedeEtapes.NAME_ENTITY_JPA);
                String codeProduit = tokenizerValues.nextToken();
                String intituleProcede = tokenizerValues.nextToken();
                Integer ordreProcede = Integer.parseInt(tokenizerValues.nextToken());
                final String intituleEtape = tokenizerValues.nextToken();
                String methodeEtapeIntitule = tokenizerValues.nextToken();
                Integer ordreEtape = Integer.parseInt(tokenizerValues.nextToken());
                Etapes dbetapes = etapeDAO.getByNKey(intituleEtape).orElse(null);
                MethodeEtapes dbMethodeetape = methodeetapeDAO.getByNKey(methodeEtapeIntitule).orElse(null);
                MethodeProcedes dbMethodeProcedes = methodeprocedeDAO.getByNKey(codeProduit, intituleProcede, ordreProcede).orElse(null);
                MethodeProcedeEtapes dbmpe = methodeprocedeetapeDAO.getByNKey(dbMethodeProcedes, dbMethodeetape, dbetapes, ordreEtape)
                        .orElseThrow(() -> new BusinessException("can't get methodeprocedeetape"));
                methodeprocedeetapeDAO.remove(dbmpe);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected List<MethodeProcedeEtapes> getAllElements() throws BusinessException {
        
        return methodeprocedeetapeDAO.getAll();
    }
    
    /**
     *
     * @return
     */
    public IEtapeDAO getEtapeDAO() {
        return etapeDAO;
    }
    
    /**
     *
     * @return
     */
    public IMethodeEtapeDAO getMethodeetapeDAO() {
        return methodeetapeDAO;
    }
    
    /**
     *
     * @return
     */
    public IMethodeProcedesDAO getMethodeprocedeDAO() {
        return methodeprocedeDAO;
    }
    
    /**
     *
     * @return
     */
    public IMethodeProcedeEtapesDAO getMethodeprocedeetapeDAO() {
        return methodeprocedeetapeDAO;
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MethodeProcedeEtapes methodeprocedeetapes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        org.inra.ecoinfo.pro.refdata.methodeprocede.Recorder.initNewLine(lineModelGridMetadata, Optional.ofNullable(methodeprocedeetapes).map(mpe->mpe.getMethodeprocedes()));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedeetapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedeetapes.getEtapes() != null ? methodeprocedeetapes.getEtapes().getEtape_intitule() : "", listeEtapesPossibles, null,
                        true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedeetapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedeetapes.getMethodeetapes() != null ? methodeprocedeetapes.getMethodeetapes().getMe_nom() : "", listeMEtapesPossibles,
                        null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedeetapes == null || methodeprocedeetapes.getOrdre() == 0 ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedeetapes.getOrdre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedeetapes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : methodeprocedeetapes.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedeetapes == null || methodeprocedeetapes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(methodeprocedeetapes.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        return lineModelGridMetadata;
        
    }
    
    private void initEtapesPossibles() {
        List<Etapes> groupeetapes = etapeDAO.getAll(Etapes.class);
        String[] Listeetapes = new String[groupeetapes.size() + 1];
        Listeetapes[0] = "";
        int index = 1;
        for (Etapes etapes : groupeetapes) {
            Listeetapes[index++] = etapes.getEtape_intitule();
        }
        this.listeEtapesPossibles = Listeetapes;
    }
    
    @Override
    protected ModelGridMetadata<MethodeProcedeEtapes> initModelGridMetadata() {
        initEtapesPossibles();
        methodeEtapesPossibles();
        org.inra.ecoinfo.pro.refdata.methodeprocede.Recorder.initMethodeProcedesPossibles(methodeprocedeDAO);
        commentaireEn = localizationManager.newProperties(MethodeProcedeEtapes.NAME_ENTITY_JPA, MethodeProcedeEtapes.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    private void methodeEtapesPossibles() {
        List<MethodeEtapes> groupemetapes = methodeetapeDAO.getAll(MethodeEtapes.class);
        String[] Listemetapes = new String[groupemetapes.size() + 1];
        Listemetapes[0] = "";
        int index = 1;
        for (MethodeEtapes metapes : groupemetapes) {
            Listemetapes[index++] = metapes.getMe_nom();
        }
        this.listeMEtapesPossibles = Listemetapes;
    }
    
    private void persistMethodeProcedeEtapes(int ordreEtape, MethodeProcedes methodep, Etapes etapes, String commentaire, MethodeEtapes methodeetapes) throws BusinessException,
            BusinessException {
        final MethodeProcedeEtapes dbmethodeprocedeetapes = methodeprocedeetapeDAO.getByNKey(methodep, methodeetapes, etapes, ordreEtape).orElse(null);
        createOrUpdateMethodeProcedeEtapes(methodep, etapes, methodeetapes, ordreEtape, commentaire, dbmethodeprocedeetapes);
        
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MethodeProcedeEtapes.NAME_ENTITY_JPA);
                int indexprod = tokenizerValues.currentTokenIndex();
                final String codeProduit = tokenizerValues.nextToken();
                int indexprocess = tokenizerValues.currentTokenIndex();
                final String intituleProcede = tokenizerValues.nextToken();
                final int ordreProcede = verifieInt(tokenizerValues, line, true, errorsReport);
                int indexetape = tokenizerValues.currentTokenIndex();
                final String intituleEtape = tokenizerValues.nextToken();
                int indexme = tokenizerValues.currentTokenIndex();
                final String methodeEtape = tokenizerValues.nextToken();
                final int ordreEtape = verifieInt(tokenizerValues, line, true, errorsReport);
                String commentaire = tokenizerValues.nextToken();
                MethodeProcedes dbMethodeProcede = methodeprocedeDAO.getByNKey(codeProduit, intituleProcede, ordreProcede).orElse(null);
                if (dbMethodeProcede == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PPME_BAD_PROD_PROCEDE), line, indexprod, codeProduit, indexprocess, intituleProcede, ordreProcede));
                    
                }
                Etapes dbEtapes = etapeDAO.getByNKey(intituleEtape).orElse(null);
                if (dbEtapes == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PPME_BAD_ETAPE), line, indexetape, intituleEtape));
                    
                }
                MethodeEtapes dbMethodeetapes = methodeetapeDAO.getByNKey(methodeEtape).orElse(null);
                if (dbMethodeetapes == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PPME_BAD_ME), line, indexme, methodeEtape));
                    
                }
                if (!errorsReport.hasErrors()) {
                    persistMethodeProcedeEtapes(ordreEtape, dbMethodeProcede, dbEtapes, commentaire, dbMethodeetapes);
                }
                values = parser.getLine();
                
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
            
        }
        
    }
    
    /**
     *
     * @param etapeDAO
     */
    public void setEtapeDAO(IEtapeDAO etapeDAO) {
        this.etapeDAO = etapeDAO;
    }
    
    /**
     *
     * @param methodeetapeDAO
     */
    public void setMethodeetapeDAO(IMethodeEtapeDAO methodeetapeDAO) {
        this.methodeetapeDAO = methodeetapeDAO;
    }
    
    /**
     *
     * @param methodeprocedeDAO
     */
    public void setMethodeprocedeDAO(IMethodeProcedesDAO methodeprocedeDAO) {
        this.methodeprocedeDAO = methodeprocedeDAO;
    }
    
    /**
     *
     * @param methodeprocedeetapeDAO
     */
    public void setMethodeprocedeetapeDAO(IMethodeProcedeEtapesDAO methodeprocedeetapeDAO) {
        this.methodeprocedeetapeDAO = methodeprocedeetapeDAO;
    }
    
}
