/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.impl;

import java.time.LocalDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolMoyLineRecord implements Comparable<IncubationSolMoyLineRecord> {

    LocalDate date_prel_sol;
    String codeech;
    int ordre_manip;
    float masse_de_sol;
    String humidite_incub;
    float temperature_incub;
    float n_mineral;
    LocalDate date_debut_incub;
    String labo_analyse;
    String code_interne_labo;
    int numero_rep_analyse;
    String codevariable;
    float valeur_moyenne;
    float valeur_ecart_type;
    String codemethode;
    String codeunite;
    String codehumidite;
    int jour_incub;
    Long originalLineNumber;

    /**
     *
     */
    public IncubationSolMoyLineRecord() {
        super();
    }

    /**
     *
     * @param date_prel_sol
     * @param codeech
     * @param ordre_manip
     * @param masse_de_sol
     * @param humidite_incub
     * @param temperature_incub
     * @param n_mineral
     * @param date_debut_incub
     * @param labo_analyse
     * @param code_interne_labo
     * @param numero_rep_analyse
     * @param jour_incub
     * @param originalLineNumber
     * @param codevariable
     * @param valeur_moyenne
     * @param valeur_ecart_type
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     */
    public IncubationSolMoyLineRecord(LocalDate date_prel_sol, String codeech, int ordre_manip, float masse_de_sol, String humidite_incub, float temperature_incub, float n_mineral, LocalDate date_debut_incub, String labo_analyse, String code_interne_labo, int numero_rep_analyse, int jour_incub, Long originalLineNumber, String codevariable, float valeur_moyenne, float valeur_ecart_type, String codemethode, String codeunite, String codehumidite) {
        this.date_prel_sol = date_prel_sol;
        this.codeech = codeech;
        this.ordre_manip = ordre_manip;
        this.masse_de_sol = masse_de_sol;
        this.humidite_incub = humidite_incub;
        this.temperature_incub = temperature_incub;
        this.n_mineral = n_mineral;
        this.date_debut_incub = date_debut_incub;
        this.labo_analyse = labo_analyse;
        this.code_interne_labo = code_interne_labo;
        this.numero_rep_analyse = numero_rep_analyse;
        this.jour_incub = jour_incub;
        this.originalLineNumber = originalLineNumber;
        this.codevariable = codevariable;
        this.valeur_moyenne = valeur_moyenne;
        this.valeur_ecart_type = valeur_ecart_type;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @param line
     */
    public void copy(final IncubationSolMoyLineRecord line) {
        this.date_prel_sol = line.getDate_prel_sol();
        this.codeech = line.getCodeech();
        this.ordre_manip = line.getOrdre_manip();
        this.masse_de_sol = line.getMasse_de_sol();
        this.humidite_incub = line.getHumidite_incub();
        this.temperature_incub = line.getTemperature_incub();
        this.n_mineral = line.getN_mineral();
        this.date_debut_incub = line.getDate_debut_incub();
        this.labo_analyse = line.getLabo_analyse();
        this.code_interne_labo = line.getCode_interne_labo();
        this.numero_rep_analyse = line.getNumero_rep_analyse();
        this.jour_incub = line.getJour_incub();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.codevariable = line.getCodevariable();
        this.valeur_moyenne = line.getValeur_moyenne();
        this.valeur_ecart_type = line.getValeur_ecart_type();
        this.codemethode = line.getCodemethode();
        this.codeunite = line.getCodeunite();
        this.codehumidite = line.getCodehumidite();
    }

    @Override
    public int compareTo(IncubationSolMoyLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_prel_sol() {
        return date_prel_sol;
    }

    /**
     *
     * @param date_prel_sol
     */
    public void setDate_prel_sol(LocalDate date_prel_sol) {
        this.date_prel_sol = date_prel_sol;
    }

    /**
     *
     * @return
     */
    public String getCodeech() {
        return codeech;
    }

    /**
     *
     * @param codeech
     */
    public void setCodeech(String codeech) {
        this.codeech = codeech;
    }

    /**
     *
     * @return
     */
    public int getOrdre_manip() {
        return ordre_manip;
    }

    /**
     *
     * @param ordre_manip
     */
    public void setOrdre_manip(int ordre_manip) {
        this.ordre_manip = ordre_manip;
    }

    /**
     *
     * @return
     */
    public float getMasse_de_sol() {
        return masse_de_sol;
    }

    /**
     *
     * @param masse_de_sol
     */
    public void setMasse_de_sol(float masse_de_sol) {
        this.masse_de_sol = masse_de_sol;
    }

    /**
     *
     * @return
     */
    public String getHumidite_incub() {
        return humidite_incub;
    }

    /**
     *
     * @param humidite_incub
     */
    public void setHumidite_incub(String humidite_incub) {
        this.humidite_incub = humidite_incub;
    }

    /**
     *
     * @return
     */
    public float getTemperature_incub() {
        return temperature_incub;
    }

    /**
     *
     * @param temperature_incub
     */
    public void setTemperature_incub(float temperature_incub) {
        this.temperature_incub = temperature_incub;
    }

    /**
     *
     * @return
     */
    public float getN_mineral() {
        return n_mineral;
    }

    /**
     *
     * @param n_mineral
     */
    public void setN_mineral(float n_mineral) {
        this.n_mineral = n_mineral;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_debut_incub() {
        return date_debut_incub;
    }

    /**
     *
     * @param date_debut_incub
     */
    public void setDate_debut_incub(LocalDate date_debut_incub) {
        this.date_debut_incub = date_debut_incub;
    }

    /**
     *
     * @return
     */
    public String getLabo_analyse() {
        return labo_analyse;
    }

    /**
     *
     * @param labo_analyse
     */
    public void setLabo_analyse(String labo_analyse) {
        this.labo_analyse = labo_analyse;
    }

    /**
     *
     * @return
     */
    public String getCode_interne_labo() {
        return code_interne_labo;
    }

    /**
     *
     * @param code_interne_labo
     */
    public void setCode_interne_labo(String code_interne_labo) {
        this.code_interne_labo = code_interne_labo;
    }

    /**
     *
     * @return
     */
    public int getNumero_rep_analyse() {
        return numero_rep_analyse;
    }

    /**
     *
     * @param numero_rep_analyse
     */
    public void setNumero_rep_analyse(int numero_rep_analyse) {
        this.numero_rep_analyse = numero_rep_analyse;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeur_moyenne() {
        return valeur_moyenne;
    }

    /**
     *
     * @param valeur_moyenne
     */
    public void setValeur_moyenne(float valeur_moyenne) {
        this.valeur_moyenne = valeur_moyenne;
    }

    /**
     *
     * @return
     */
    public float getValeur_ecart_type() {
        return valeur_ecart_type;
    }

    /**
     *
     * @param valeur_ecart_type
     */
    public void setValeur_ecart_type(float valeur_ecart_type) {
        this.valeur_ecart_type = valeur_ecart_type;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public int getJour_incub() {
        return jour_incub;
    }

    /**
     *
     * @param jour_incub
     */
    public void setJour_incub(int jour_incub) {
        this.jour_incub = jour_incub;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

}
