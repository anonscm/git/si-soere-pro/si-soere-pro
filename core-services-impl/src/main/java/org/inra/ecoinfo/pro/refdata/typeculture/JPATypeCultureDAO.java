/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typeculture;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeCultureDAO extends AbstractJPADAO<TypeCulture> implements ITypeCultureDAO {

    @Override
    public List<TypeCulture> getAll() {
        return super.getAllBy(TypeCulture.class, TypeCulture::getLibelle);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typeculture.ITypeCultureDAO#getByNKey (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    @SuppressWarnings("unchecked")
    public Optional<TypeCulture> getByNKey(String libelle) {
        CriteriaQuery<TypeCulture> query = builder.createQuery(TypeCulture.class);
        Root<TypeCulture> typeCulture = query.from(TypeCulture.class);
        query
                .select(typeCulture)
                .where(
                        builder.equal(typeCulture.get(TypeCulture_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.typeculture.ITypeCultureDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeCulture> getByCode(String code) {
        CriteriaQuery<TypeCulture> query = builder.createQuery(TypeCulture.class);
        Root<TypeCulture> typeCulture = query.from(TypeCulture.class);
        query
                .select(typeCulture)
                .where(
                        builder.equal(typeCulture.get(TypeCulture_.code), code)
                );
        return getOptional(query);
    }

}
