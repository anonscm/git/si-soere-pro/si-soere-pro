/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dispositifthemedatatypeparcelle;

import java.util.List;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author vjkoyao
 */
public interface IDispositifThemeDatatypeParcelleDAO extends IDAO<INode> {

    List<INode> getDispThemeDatatypeParcelleCodeNode(String codeDispositif, String codeTheme, String codeDatatype, String codePE);

    List<String> getPathes();

    List<RealNode> loadRealDatatypeNodes();
}
