/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.lavage;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface ILavageDAO extends IDAO<Lavage> {

    /**
     *
     * @return
     */
    List<Lavage> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Lavage> getByName(String nom);

    /**
     *
     * @param code
     * @return
     */
    Optional<Lavage> getByNKey(String code);

}
