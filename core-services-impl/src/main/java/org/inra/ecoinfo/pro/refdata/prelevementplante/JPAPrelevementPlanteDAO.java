/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementplante;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.cultures.Cultures;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.partieprelevee.PartiePrelevee;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAPrelevementPlanteDAO extends AbstractJPADAO<PrelevementPlante> implements IPrelevementPlanteDAO {

    /**
     *
     * @return
     */
    @Override
    public List<PrelevementPlante> getAll() {
        return getAllBy(PrelevementPlante.class, PrelevementPlante::getCodeplante);
    }

    /**
     *
     * @param datePrelevement
     * @param descriptionTraitement
     * @param placette
     * @param bloc
     * @param cultures
     * @param partiePrelevee
     * @return
     */
    @Override
    public Optional<PrelevementPlante> getByNKey(
            LocalDate datePrelevement,
            DescriptionTraitement descriptionTraitement,
            Placette placette,
            Bloc bloc,
            Cultures cultures,
            PartiePrelevee partiePrelevee
    ) {
        CriteriaQuery<PrelevementPlante> query = builder.createQuery(PrelevementPlante.class);
        Root<PrelevementPlante> prelevementPlante = query.from(PrelevementPlante.class);
        Join<PrelevementPlante, DescriptionTraitement> descTrait = prelevementPlante.join(PrelevementPlante_.traitement);
        Join<PrelevementPlante, Placette> plcte = prelevementPlante.join(PrelevementPlante_.placette);
        Join<PrelevementPlante, Bloc> blo = prelevementPlante.join(PrelevementPlante_.bloc);
        Join<PrelevementPlante, Cultures> cult = prelevementPlante.join(PrelevementPlante_.cultures);
        Join<PrelevementPlante, PartiePrelevee> parPre = prelevementPlante.join(PrelevementPlante_.partiePrelevee);
        query
                .select(prelevementPlante)
                .where(
                        builder.equal(prelevementPlante.get(PrelevementPlante_.dateplante), datePrelevement),
                        builder.equal(descTrait, descriptionTraitement),
                        builder.equal(plcte, placette),
                        builder.equal(blo, bloc),
                        builder.equal(cult, cultures),
                        builder.equal(parPre, partiePrelevee)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codePlante
     * @return
     */
    public Optional<PrelevementPlante> getByNKey(String codePlante) {
        CriteriaQuery<PrelevementPlante> query = builder.createQuery(PrelevementPlante.class);
        Root<PrelevementPlante> prelevementPlante = query.from(PrelevementPlante.class);
        query
                .select(prelevementPlante)
                .where(
                        builder.equal(prelevementPlante.get(PrelevementPlante_.codeplante), Utils.createCodeFromString(codePlante))
                );
        return getOptional(query);

    }
}
