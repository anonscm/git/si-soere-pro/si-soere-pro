/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.IMesurePhysicoChimiePROMoyDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.IValeurProduitMoyenneDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produitdispositif.IProduitDisposotifDAO;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordPhysicoChimiePROMoy extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPhysicoChimiePROMoy.class);
    protected static final String BUNDLE_SOURCE_PATH_MOY_PRO = "org.inra.ecoinfo.pro.dataset.physicochimie.produit.moyenne.messages";

    private static final String MSG_ERROR_PRODUIT_NOT_FOUND_IN_DB = "MSG_ERROR_PRODUIT_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_DVUMU_PROMOY_NOT_FOUND_IN_DB = "MSG_ERROR_DVUMU_PROMOY_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_PROMOY_NOT_VARIABLEPRO_DB = "MSG_ERROR_PROMOY_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_PROMOY_NOT_FOUND_METHODE_DB = "MSG_ERROR_PROMOY_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_PROMOY_NOT_UNITEPRO_DB = "MSG_ERROR_PROMOY_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_PROMOY_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_PROMOY_NOT_FOUND_HUMIDITE_DB";
    protected IMesurePhysicoChimiePROMoyDAO<MesurePhysicoChimiePROMoy> mesurePhysicoChimiePROMoyDAO;
    protected IProduitDisposotifDAO produitdispositifDAO;
    protected IProduitDAO produitDAO;
    IDispositifDAO dispositifDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IValeurProduitMoyenneDAO valeurProduitMoyenneDAO;

    public ProcessRecordPhysicoChimiePROMoy() {
        super();
    }

    void buildMesure(final ProMoyLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<ProMoyLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException, PersistenceException {

        LocalDate dateprelevement = mesureLines.getDateprelevement();
        String codeprod = Utils.createCodeFromString(mesureLines.getCodeprod());
        String codedisp = Utils.createCodeFromString(sessionPropertiesPRO.getDispositif().getCode());
        String lieu = Utils.createCodeFromString(sessionPropertiesPRO.getDispositif().getLieu().getNom());
        String nomlieu = mesureLines.getNomlieu();
        String nomlabo = mesureLines.getNomlabo();
        int numerorepet = mesureLines.getNumerorepet();
        Long fichier = mesureLines.getOriginalLineNumber();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);
        final ProduitDispositif dbprodis = produitdispositifDAO.getByNKey(Utils.createCodeFromString(codeprod + "_" + codedisp + "_" + lieu)).orElse(null);
        if (dbprodis == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    ProcessRecordPhysicoChimiePROMoy.MSG_ERROR_PRODUIT_NOT_FOUND_IN_DB), codeprod, codedisp, nomlieu));
        }
        String variable = mesureLines.getCodevariable();
        float moyenne = mesureLines.getMoyenne();
        float ecarttype = mesureLines.getEcarttype();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);
        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cunite = Utils.createCodeFromString(unite);
        String cmethode = Utils.createCodeFromString(methode);
        String chumitite = Utils.createCodeFromString(humidite);
        String cvariable = Utils.createCodeFromString(variable);

        VariablesPRO dbvariable = variPRODAO.getByCodeUser(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    ProcessRecordPhysicoChimiePROMoy.MSG_ERROR_PROMOY_NOT_VARIABLEPRO_DB), cvariable));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    ProcessRecordPhysicoChimiePROMoy.MSG_ERROR_PROMOY_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    ProcessRecordPhysicoChimiePROMoy.MSG_ERROR_PROMOY_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    ProcessRecordPhysicoChimiePROMoy.MSG_ERROR_PROMOY_NOT_FOUND_HUMIDITE_DB), chumitite));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(RecorderPRO.getDatatypeFromVersion(versionFile), dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    ProcessRecordPhysicoChimiePROMoy.MSG_ERROR_DVUMU_PROMOY_NOT_FOUND_IN_DB), cdatatype, cvariable, cunite, cmethode, chumitite));

        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesurePhysicoChimiePROMoy dbmesureMoyenne = getOrCreate(dateprelevement, numerorepet, codeprod, nomlabo, fichier, versionFile, dbprodis, versionFileDB);
            persitPM(moyenne, ecarttype, dbmesureMoyenne, dbdvumRealNode);
        }
    }

    private MesurePhysicoChimiePROMoy getOrCreate(LocalDate dateprelevement, int numerorepet, String codeprod, String nomlabo, Long fichier, final VersionFile versionFile, final ProduitDispositif dbprodis, final VersionFile versionFileDB) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(dateprelevement, format);
        String intString1 = null;
        intString1 = Integer.toString(numerorepet);
        String key = dateString + "_" + codeprod + "_" + nomlabo + "_" + intString1;

        MesurePhysicoChimiePROMoy mesurePhysicoChimiePROMoy = mesurePhysicoChimiePROMoyDAO.getByKeys(key).orElse(null);
        if (mesurePhysicoChimiePROMoy == null) {
            mesurePhysicoChimiePROMoy = new MesurePhysicoChimiePROMoy(fichier, dateprelevement, nomlabo, numerorepet, versionFile, dbprodis);
            mesurePhysicoChimiePROMoy.setDatePrelevement(dateprelevement);
            mesurePhysicoChimiePROMoy.setDisppro(dbprodis);
            mesurePhysicoChimiePROMoy.setLigneFichierEchange(fichier);
            mesurePhysicoChimiePROMoy.setNom_laboratoire(nomlabo);
            mesurePhysicoChimiePROMoy.setNbre_repetition(numerorepet);
            mesurePhysicoChimiePROMoy.setKeymesure(key);
            mesurePhysicoChimiePROMoy.setVersionfile(versionFileDB);
            mesurePhysicoChimiePROMoyDAO.saveOrUpdate(mesurePhysicoChimiePROMoy);
        }
        return mesurePhysicoChimiePROMoy;
    }

    void create(ValeurPhysicoChimiePROMoy valeurPhysicoChimiePROMoy) throws PersistenceException {
        valeurProduitMoyenneDAO.saveOrUpdate(valeurPhysicoChimiePROMoy);
    }

    void createProduitMoyenne(float moyenne, float ecarttype, MesurePhysicoChimiePROMoy mesurePhysicoChimiePROMoy, RealNode dbdvumRealNode, ValeurPhysicoChimiePROMoy dbvpm) throws PersistenceException {
        if (dbvpm == null) {
            ValeurPhysicoChimiePROMoy valeurPhysicoChimiePROMoy = new ValeurPhysicoChimiePROMoy(moyenne, ecarttype, mesurePhysicoChimiePROMoy, dbdvumRealNode);
            valeurPhysicoChimiePROMoy.setRealNode(dbdvumRealNode);
            valeurPhysicoChimiePROMoy.setMesurePhysicoChimiePROMoy(mesurePhysicoChimiePROMoy);
            valeurPhysicoChimiePROMoy.setValeur(moyenne);
            valeurPhysicoChimiePROMoy.setEcarttype(ecarttype);
            create(valeurPhysicoChimiePROMoy);
        }
    }

    void persitPM(float moyenne, float ecarttype, MesurePhysicoChimiePROMoy mesurePhysicoChimiePROMoy, RealNode dbdvumRealNode) throws PersistenceException {
        ValeurPhysicoChimiePROMoy valeurPhysicoChimiePROMoy = valeurProduitMoyenneDAO.getByNKeys(dbdvumRealNode, mesurePhysicoChimiePROMoy, ecarttype).orElse(null);
        createProduitMoyenne(moyenne, ecarttype, mesurePhysicoChimiePROMoy, dbdvumRealNode, valeurPhysicoChimiePROMoy);
    }

    private long readLines(final CSVParser parser, final List<DatatypeVariableUnitePRO> dbVariables, final Map<LocalDate, List<ProMoyLineRecord>> lines,
            long lineCount, ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate dateprelevement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeprod = cleanerValues.nextToken();
            final String nomlieu = cleanerValues.nextToken();
            final String nomlabo = cleanerValues.nextToken();
            final int numerorepe = Integer.parseInt(cleanerValues.nextToken());
            final String codevpro = cleanerValues.nextToken();
            final float valeurpro = Float.parseFloat(cleanerValues.nextToken());
            final float ecart = Float.parseFloat(cleanerValues.nextToken());
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();

            final ProMoyLineRecord line = new ProMoyLineRecord(dateprelevement, lineCount, codeprod, nomlieu, nomlabo, numerorepe, codevpro, codeunite, codemethode, codehumidite, valeurpro, ecart);
            try {
                if (!lines.containsKey(dateprelevement)) {
                    lines.put(dateprelevement, new LinkedList<ProMoyLineRecord>());
                }
                lines.get(dateprelevement).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        dateprelevement, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<ProMoyLineRecord>> lines,
            final SortedSet<ProMoyLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<ProMoyLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<ProMoyLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (ProMoyLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    @Override
    public void processRecord(final CSVParser parser, final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final String fileEncoding,
            final DatasetDescriptorPRO datasetDescriptor) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            final Map<LocalDate, List<ProMoyLineRecord>> mesuresMapLines = new HashMap();
            final long lineCount = datasetDescriptor.getEnTete();
            this.readLines(parser, dbVariables, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<ProMoyLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordPhysicoChimiePROMoy.class).error(ex.getMessage(), ex);
        }
    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    public IMesurePhysicoChimiePROMoyDAO<MesurePhysicoChimiePROMoy> getMesurePhysicoChimiePROMoyDAO() {
        return mesurePhysicoChimiePROMoyDAO;
    }

    public void setMesurePhysicoChimiePROMoyDAO(IMesurePhysicoChimiePROMoyDAO<MesurePhysicoChimiePROMoy> mesurePhysicoChimiePROMoyDAO) {
        this.mesurePhysicoChimiePROMoyDAO = mesurePhysicoChimiePROMoyDAO;
    }

    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesurePhysicoChimiePROMoyDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IProduitDisposotifDAO getProduitdispositifDAO() {
        return produitdispositifDAO;
    }

    public void setProduitdispositifDAO(IProduitDisposotifDAO produitdispositifDAO) {
        this.produitdispositifDAO = produitdispositifDAO;
    }

    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    public void setValeurProduitMoyenneDAO(IValeurProduitMoyenneDAO valeurProduitMoyenneDAO) {
        this.valeurProduitMoyenneDAO = valeurProduitMoyenneDAO;
    }

}
