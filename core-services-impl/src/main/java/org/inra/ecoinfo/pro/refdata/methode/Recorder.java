/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methode;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Methode> {

    IMethodeDAO methodeDAO;
    Properties commenEn;

    /**
     *
     * @param methode
     * @throws BusinessException
     */
    public void createMethode(Methode methode) throws BusinessException {
        try {
            methodeDAO.saveOrUpdate(methode);
            methodeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create methode");
        }
    }

    private void updatecreateMethode(final String nom, String code, String mycode, String pretraitement,
            String solution, String donsol, String dosage, String biblio, String norme, final Methode dbmethode, String commentaire) throws BusinessException {
        try {
            dbmethode.setMethode_code(code);
            dbmethode.setMethode_nom(nom);
            dbmethode.setMethode_mycode(mycode);
            dbmethode.setPretraittement(pretraitement);
            dbmethode.setMise_ensolution(solution);
            dbmethode.setReference_donsol(donsol);
            dbmethode.setDosage(dosage);
            dbmethode.setReference_bibliographique(biblio);
            dbmethode.setMethode_norme(norme);
            dbmethode.setCommentaire(commentaire);
            methodeDAO.saveOrUpdate(dbmethode);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update methode");
        }
    }

    private void createOrUpdatecreateMethode(final String nom, String code, String mycode, String pretraitement,
            String solution, String donsol, String dosage, String biblio, String norme, final Methode dbmethode, String commentaire) throws BusinessException {
        if (dbmethode == null) {
            final Methode methode = new Methode(code, nom, mycode, pretraitement, solution, donsol, dosage, biblio, norme, commentaire);
            createMethode(methode);
        } else {
            updatecreateMethode(nom, code, mycode, pretraitement, solution, donsol, dosage, biblio, norme, dbmethode, commentaire);
        }
    }

    private void persistMethode(final String nom, String code, String mycode, String pretraitement,
            String solution, String donsol, String dosage, String biblio, String norme, String commentaire) throws BusinessException, BusinessException {
        final Methode variable = methodeDAO.getByNKey(mycode).orElse(null);
        createOrUpdatecreateMethode(nom, code, mycode, pretraitement, solution, donsol, dosage, biblio, norme, variable, commentaire);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = tokenizerValues.nextToken();
                methodeDAO.remove(methodeDAO.getByNKey(Utils.createCodeFromString(code))
                        .orElseThrow(() -> new BusinessException("can't find methode")));
                values = csvp.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Methode> getAllElements() throws BusinessException {
        return methodeDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Methode.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(code);
                final String traitement = tokenizerValues.nextToken();
                final String solution = tokenizerValues.nextToken();
                final String donsol = tokenizerValues.nextToken();
                final String dosage = tokenizerValues.nextToken();
                final String biblio = tokenizerValues.nextToken();
                final String norme = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistMethode(nom, code, mycode, traitement, solution, donsol, dosage, biblio, norme, commentaire);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Methode methode) throws BusinessException {

        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getMethode_code() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getMethode_code(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getMethode_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getMethode_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getPretraittement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getPretraittement(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getMise_ensolution() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getMise_ensolution(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getReference_donsol() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getReference_donsol(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getDosage() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getDosage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getReference_bibliographique() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getReference_bibliographique(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getMethode_norme() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methode.getMethode_norme(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(methode == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : methode.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methode == null || methode.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commenEn.getProperty(methode.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @param methodeDAO
     */
    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    @Override
    protected ModelGridMetadata<Methode> initModelGridMetadata() {
        commenEn = localizationManager.newProperties(Methode.NAME_ENTITY_JPA, Methode.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

}
