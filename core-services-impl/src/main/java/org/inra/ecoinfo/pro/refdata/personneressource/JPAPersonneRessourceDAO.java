/**
 *
 */
package org.inra.ecoinfo.pro.refdata.personneressource;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAPersonneRessourceDAO extends AbstractJPADAO<PersonneRessource> implements IPersonneRessourceDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.personneressource.IPersonneRessourceDAO# getByEMail(java.lang.String)
     */

    /**
     *
     * @param eMail
     * @return
     */

    @Override
    public Optional<PersonneRessource> getByEMail(String eMail) {
        CriteriaQuery<PersonneRessource> query = builder.createQuery(PersonneRessource.class);
        Root<PersonneRessource> personneRessource = query.from(PersonneRessource.class);
        query
                .select(personneRessource)
                .where(
                        builder.equal(personneRessource.get(PersonneRessource_.email), eMail)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.personneressource.IPersonneRessourceDAO# getByNom(java.lang.String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<PersonneRessource> getByNom(String nom) {
        CriteriaQuery<PersonneRessource> query = builder.createQuery(PersonneRessource.class);
        Root<PersonneRessource> personneRessource = query.from(PersonneRessource.class);
        query
                .select(personneRessource)
                .where(
                        builder.equal(personneRessource.get(PersonneRessource_.nom), nom)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.personneressource.IPersonneRessourceDAO# getByNomPrenom(java.lang.String, java.lang.String)
     */

    /**
     *
     * @param nom
     * @param prenom
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<PersonneRessource> getByNomPrenom(String nom, String prenom) {
        CriteriaQuery<PersonneRessource> query = builder.createQuery(PersonneRessource.class);
        Root<PersonneRessource> personneRessource = query.from(PersonneRessource.class);
        query
                .select(personneRessource)
                .where(
                        builder.equal(personneRessource.get(PersonneRessource_.nom), nom),
                        builder.equal(personneRessource.get(PersonneRessource_.prenom), prenom)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.personne_ressource.IPersonneRessourceDAO #getByNomPrenomEMail(java.lang.String, java.lang.String, java.lang.String)
     */

    /**
     *
     * @param nom
     * @param prenom
     * @param eMail
     * @return
     */

    @Override
    public Optional<PersonneRessource> getByNKey(String nom, String prenom, String eMail) {
        CriteriaQuery<PersonneRessource> query = builder.createQuery(PersonneRessource.class);
        Root<PersonneRessource> personneRessource = query.from(PersonneRessource.class);
        query
                .select(personneRessource)
                .where(
                        builder.equal(personneRessource.get(PersonneRessource_.nom), nom),
                        builder.equal(personneRessource.get(PersonneRessource_.prenom), prenom),
                        builder.equal(personneRessource.get(PersonneRessource_.email), eMail)
                );
        return getOptional(query);
    }
}
