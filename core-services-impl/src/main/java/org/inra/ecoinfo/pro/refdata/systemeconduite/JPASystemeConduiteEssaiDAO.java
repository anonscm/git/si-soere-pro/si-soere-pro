/**
 *
 */
package org.inra.ecoinfo.pro.refdata.systemeconduite;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPASystemeConduiteEssaiDAO extends AbstractJPADAO<SystemeConduiteEssai> implements ISystemeConduiteEssaiDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.systemeconduite.ISystemeConduiteEssaiDAO #getByLibelle(java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<SystemeConduiteEssai> getByNKey(String libelle) {
        CriteriaQuery<SystemeConduiteEssai> query = builder.createQuery(SystemeConduiteEssai.class);
        Root<SystemeConduiteEssai> systemeConduiteEssai = query.from(SystemeConduiteEssai.class);
        query
                .select(systemeConduiteEssai)
                .where(
                        builder.equal(systemeConduiteEssai.get(SystemeConduiteEssai_.libelle), libelle)
                );
        return getOptional(query);
    }

    @Override
    public List<SystemeConduiteEssai> getAll() {
        return getAllBy(SystemeConduiteEssai.class, SystemeConduiteEssai::getLibelle);
    }

}
