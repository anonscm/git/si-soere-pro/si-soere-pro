/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.BadDelimiterException;
import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.AbstractRecorder;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.utils.PROMessages;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.system.Allocator;
import org.inra.ecoinfo.utils.ApplicationContextHolder;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class RecorderPRO extends AbstractRecorder implements IRecorderPRO {

    public static final String PRO_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.pro.dataset.messages";
    public static final String PROPERTY_MSG_ERROR_BAD_FORMAT = "PROPERTY_MSG_ERROR_BAD_FORMAT";
    public static final String DATASET_DESCRIPTOR_PATH_PATTERN = "org/inra/ecoinfo/pro/dataset/%s";
    public static final String DD_MM_YYYY_HHMMSS_DOUBLONS_FILE = "dd/MM/yyyy;HH:mm";
    public static final String YYYYMMJJHHMMSS = "yyyyMMddHHmmss";
    public static final String DD_MM_YYYY_HHMMSS_READ = "dd/MM/yyyyHH:mm:ss";
    public static final String DD_MM_YYYY_HHMMSS_FILE = "dd-MM-yyyy-HHmmss";
    public static final String PROPERTY_MSG_MISSING_EMPTY_LINE = "PROPERTY_MSG_MISSING_EMPTY_LINE";
    public static final String PROPERTY_MSG_INVALID_DATATYPE = "PROPERTY_MSG_INVALID_DATATYPE";
    public static final String PROPERTY_MSG_MISSING_DATATYPE = "PROPERTY_MSG_MISSING_DATATYPE";
    public static final String PROPERTY_MSG_MISSING_BEGIN_DATE = "PROPERTY_MSG_MISSING_BEGIN_DATE";
    public static final String PROPERTY_MSG_MISSING_END_DATE = "PROPERTY_MSG_MISSING_END_DATE";
    public static final String PROPERTY_MSG_INVALID_BEGIN_DATE = "PROPERTY_MSG_INVALID_BEGIN_DATE";
    public static final String PATTERN_DATE = "^(([0-2][0-9]/(0[0-9]|1[0-2]))|(30/(04|06|09|11))|(3[01]/(01|03|05|07|08|10|12)))/[0-2][0-9]{3}";
    public static final String PATTERN_TIME_SECONDE = "^([0-1][0-9]|2[0-3]):([0-6][0-9]):([0-6][0-9])$|24:00:00";
    public static final String PROPERTY_MSG_INVALID_BEGIN_TIME = "PROPERTY_MSG_INVALID_BEGIN_TIME";
    public static final String PROPERTY_MSG_INVALID_DATE_TIME = "PROPERTY_MSG_INVALID_DATE_TIME";
    public static final String PROPERTY_MSG_INVALID_END_TIME = "PROPERTY_MSG_INVALID_END_TIME";
    public static final String PROPERTY_MSG_INVALID_END_DATE = "PROPERTY_MSG_INVALID_END_DATE";
    public static final String PROPERTY_MSG_MISMACH_COLUMN_HEADER = "PROPERTY_MSG_MISMACH_COLUMN_HEADER";
    public static final String PROPERTY_MSG_EMPTY_FILE = "PROPERTY_MSG_EMPTY_FILE";
    public static final String PROPERTY_MSG_INVALID_DISPOSITIF = "PROPERTY_MSG_INVALID_DISPOSITIF";
    public static final String PROPERTY_MSG_MISSING_DISPOSITIF = "PROPERTY_MSG_MISSING_DISPOSITIF";
    public static final String PROPERTY_MSG_IMPROPER_DISPOSITIF = "PROPERTY_MSG_IMPROPER_DISPOSITIF";
    public static final String PROPERTY_MSG_INVALID_INTERVAL_DATE = "PROPERTY_MSG_INVALID_INTERVAL_DATE";
    public static final String PROPERTY_MSG_IMPROPER_BEGIN_DATE = "PROPERTY_MSG_IMPROPER_BEGIN_DATE";
    public static final String PROPERTY_MSG_IMPROPER_END_DATE = "PROPERTY_MSG_IMPROPER_END_DATE";
    public static final String CST_UNDERSCORE = "_";
    public static final String PROPERTY_CST_BAD_VALUE = "NV";
    public static final String PROPERTY_MSG_NO_DATA = "PROPERTY_MSG_NO_DATA";

    //public static final String PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE = "PROPERTY_MSG_INVALID_INTERVAL_FLOAT_VALUE" 
    public static final String PROPERTY_MSG_INVALID_CMU_VALUE = "PROPERTY_MSG_INVALID_CMU_VALUE";
    //CMU (Categorie, Variable, unite)
    public static final String CST_DOT = ".";
    public static final String CST_COMMA = ",";
    public static final String CST_SPACE = " ";
    public static final String PROPERTY_CST_DATE_TYPE = "date";
    public static final String PROPERTY_CST_INTEGER_TYPE = "integer";
    public static final String PROPERTY_CST_FLOAT_TYPE = "float";
    public static final String PROPERTY_CST_TIME_TYPE = "time";
    public static final String PROPERTY_CODE_ECHANTILLON = "code_échantillon";
    public static final String PROPERTY_NOM_LABORATOIRE = "laboratoire_analyse";
    public static final String PROPERTY_NUMERO_LABORATOIRE = "numéro_échantillon";
    public static final String PROPERTY_NUMERO_REPETITION = "numéro_répétition";
    public static final String PROPERTY_CODE_VARIABLE = "code_variable";
    public static final String PROPERTY_CODE_METHODE = "code_methode";
    public static final String PROPERTY_CODE_UNITE = "code_unite";
    public static final String PROPERTY_CODE_HUMIDITE = "code_humidite";
    public static final String PROPERTY_CST_GAP_FIELD_TYPE = "gap field";
    public static final String PROPERTY_CST_VALEUR_STATUT = "statut_valeur";
    public static final String PROPERTY_MSG_MISSING_VALUE = "PROPERTY_MSG_MISSING_VALUE";
    public static final String PROPERTY_MSG_INVALID_DATE = "PROPERTY_MSG_INVALID_DATE";
    public static final String PROPERTY_MSG_INVALID_FLOAT_VALUE = "PROPERTY_MSG_INVALID_FLOAT_VALUE";
    public static final String PROPERTY_MSG_INVALID_INT_VALUE = "PROPERTY_MSG_INVALID_INT_VALUE";
    public static final String CST_INVALID_BAD_MEASURE = null;
    //int CST_INVALID_BAD_MEASURE = -9_999 ou A DEFINIR {
    public static final String PROPERTY_MSG_INVALID_TIME = "PROPERTY_MSG_INVALID_TIME";
    public static final String PROPERTY_CST_VARIABLE_TYPE = "variable";
    public static final String PROPERTY_CST_STATUT_VALEUR_TYPE = "statut_valeur";
    public static final String PROPERTY_CST_LIST_VALEURS_STATUTS = "liste_statuts_valeurs";
    public static final String PROPERTY_CST_VALEUR_STATUT_TYPE = "valeur_qualitative";
    public static final String PROPERTY_MSG_ERROR = "PROPERTY_MSG_ERROR";
    public static final String PROPERTY_CST_FRENCH_TIME = DateUtil.HH_MM_SS;
    public static final String PROPERTY_CST_FRENCH_DATE = DateUtil.DD_MM_YYYY;
    public static String HH_MM_SS = "HH:mm:ss";

    public static final String YYYYMMJJ = "yyyyMMdd";
    public static final String HHMMSS = "HHmmss";
    public static final String PROPERTY_MSG_INVALID_SITE_REPOSITORY = "PROPERTY_MSG_INVALID_SITE_REPOSITORY";
    public static final String PROPERTY_MSG_DOUBLON_DATE = "PROPERTY_MSG_DOUBLON_DATE";
    public static final String PROPERTY_MSG_ERROR_INSERTION_MEASURE = "PROPERTY_MSG_ERROR_INSERTION_MEASURE";
    public static final String PROPERTY_MSG_INVALID_STATUT_VALEUR = "PROPERTY_MSG_INVALID_STATUT_VALEUR";
    public static final String PROPERTY_MSG_WARNIN_INFO_IN8PUBLISH = "PROPERTY_MSG_WARNIN_INFO_IN8PUBLISH";
    public static final String PROPERTY_MSG_INVALID_VARIABLE = "PROPERTY_MSG_INVALID_VARIABLE";
    public static final String PROPERTY_CST_NOT_AVALAIBALE = "NA";
    public static final String PROPERTY_CST_BAD_VALUE_FIELD = ";NV";
    public static final String PROPERTY_MSG_INVALID_PLOT = "PROPERTY_MSG_INVALID_PLOT";
    public static final String PROPERTY_MSG_MISSING_PLOT = "PROPERTY_MSG_MISSING_PLOT";

    private static volatile ILocalizationManager localizationManager;

    public static String getPROMessage(final String key) {
        return PROMessages.getPROMessage(key);
    }

    public static String getPROMessageWithBundle(String bundlePath, String key) {
        return PROMessages.getPROMessageWithBundle(bundlePath, key);
    }

    public static String getDateTimeForCompareUTC(LocalDate date, LocalTime time) {
        LocalDateTime dateTime = date.atTime(time);
        return DateUtil.getUTCDateTextFromLocalDateTime(dateTime, "YYYYMMJJHHMMSS");
    }

    public static final LocalDateTime getDateTimeLocalFromDateStringaAndTimeString(String dateString, String timeString) throws BusinessException {

        if (Strings.isNullOrEmpty(dateString)) {
            return null;
        }
        if (Strings.isNullOrEmpty(timeString)) {
            timeString = "00:00:00";
        }
        timeString = (timeString + ":00").substring(0, DateUtil.HH_MM_SS.length());
        return DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY_HH_MM_SS, String.format("%s %s", dateString, timeString));
    }

    /**
     *
     * @param beginDateddmmyyhhmmss
     * @param endDateddmmyyhhmmss
     * @return
     * @throws BadExpectedValueException
     * @throws DateTimeException
     */
    public static IntervalDate getIntervalDateLocaleForFile(final String beginDateddmmyyhhmmss,
            final String endDateddmmyyhhmmss) throws BadExpectedValueException, DateTimeException {
        final IntervalDate intervalDate = new IntervalDate(beginDateddmmyyhhmmss,
                endDateddmmyyhhmmss, RecorderPRO.DD_MM_YYYY_HHMMSS_FILE);
        return intervalDate;
    }

    /**
     *
     * @param beginDateddmmyyhhmmss
     * @param endDateddmmyyhhmmss
     * @return
     * @throws BadExpectedValueException
     * @throws DateTimeException
     */
    public static IntervalDate getIntervalDateUTCForFile(final String beginDateddmmyyhhmmss,
            final String endDateddmmyyhhmmss) throws BadExpectedValueException, DateTimeException {
        final IntervalDate intervalDate = new IntervalDate(beginDateddmmyyhhmmss,
                endDateddmmyyhhmmss, RecorderPRO.DD_MM_YYYY_HHMMSS_FILE);
        return intervalDate;
    }

    public static Dispositif getDispositifFromVersion(final VersionFile version) {
        return (Dispositif) version.getDataset().getRealNode().getNodeByNodeableTypeResource(Dispositif.class).getNodeable();
    }

    public static ParcelleElementaire getParcelleElementaireFromVersion(final VersionFile version) {
        return (ParcelleElementaire) version.getDataset().getRealNode().getNodeByNodeableTypeResource(ParcelleElementaire.class).getNodeable();
    }

    public static DataType getDatatypeFromVersion(final VersionFile version) {
        return (DataType) version.getDataset().getRealNode().getNodeByNodeableTypeResource(DataType.class).getNodeable();
    }
    protected INotificationsManager notificationsManager;
    IDeleteRecord deleteRecord;
    ITestFormat testFormat;
    IProcessRecord processRecord;
    String sessionPropertiesName;
    String datasetDescriptorPath;

    public RecorderPRO() {
        super();
    }

    private DatasetDescriptorPRO getDatasetDescriptor() throws IOException, SAXException,
            URISyntaxException {

        final String path = String.format(RecorderPRO.DATASET_DESCRIPTOR_PATH_PATTERN,
                this.datasetDescriptorPath);
        return DatasetDescriptorBuilderPRO.buildDescriptorPRO(this.getClass().getClassLoader()
                .getResourceAsStream(path));
    }

    @Override
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {

    }

    @Override
    public void testFormat(VersionFile versionFile, String encoding) throws BusinessException {
        try {
            final ISessionPropertiesPRO sessionProperties;
            sessionProperties = this.getSessionProperties();
            CSVParser parser = null;
            DatasetDescriptorPRO datasetDescriptorPRO = null;
            final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(this
                    .getLocalizationManager().getMessage(RecorderPRO.PRO_DATASET_BUNDLE_NAME,
                            RecorderPRO.PROPERTY_MSG_ERROR_BAD_FORMAT));
            byte[] datasSanitized = null;
            try {
                datasSanitized = Utils.sanitizeData(versionFile.getData(),
                        this.getLocalizationManager());
            } catch (final BusinessException e) {
                AbstractRecorder.LOGGER.debug("can't sanitized", e);
                badsFormatsReport.addException(e);
                try {
                    throw new BadFormatException(badsFormatsReport);
                } catch (BadFormatException ex) {
                    throw new BusinessException(ex);
                }
            }
            try {
                parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                        datasSanitized), encoding), AbstractRecorder.SEPARATOR);

                datasetDescriptorPRO = this.getDatasetDescriptor();

            } catch (final BadDelimiterException | SAXException | URISyntaxException e) {
                AbstractRecorder.LOGGER.debug("can't parse file", e);
                throw new BusinessException(e.getMessage(), e);
            } catch (final UnsupportedEncodingException e) {
                AbstractRecorder.LOGGER.debug("bad encoding", e);
                throw new BusinessException(e.getMessage(), e);
            } catch (final IOException e) {
                AbstractRecorder.LOGGER.debug("can't read file", e);
                throw new BusinessException(e.getMessage(), e);
            }

            this.testFormat(parser, versionFile, sessionProperties, encoding, datasetDescriptorPRO);

        } catch (BadFormatException ex) {
            throw new BusinessException(ex);
        }
    }

    @Override
    public void record(VersionFile versionFile, String fileEncoding) throws BusinessException {
        try {
            final ISessionPropertiesPRO sessionProperties = this.getSessionProperties();
            final BadsFormatsReport badsFormatsReport = new BadsFormatsReport(this
                    .getLocalizationManager().getMessage(RecorderPRO.PRO_DATASET_BUNDLE_NAME,
                            RecorderPRO.PROPERTY_MSG_ERROR_BAD_FORMAT));
            byte[] datasSanitized = null;
            try {
                datasSanitized = Utils.sanitizeData(versionFile.getData(),
                        this.getLocalizationManager());
            } catch (final BusinessException e) {
                try {
                    AbstractRecorder.LOGGER.debug("can't sanitized", e);
                    badsFormatsReport.addException(e);
                    throw new BadFormatException(badsFormatsReport);
                } catch (BadFormatException ex) {
                    LOGGER.info("BadFormatException", ex);
                }
            }
            CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                    datasSanitized), fileEncoding), AbstractRecorder.SEPARATOR);
            final DatasetDescriptorPRO datasetDescriptorPRO = this.getDatasetDescriptor();
            this.testFormat(parser, versionFile, sessionProperties, fileEncoding,
                    datasetDescriptorPRO);
            parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(
                    versionFile.getData()), fileEncoding), AbstractRecorder.SEPARATOR);
            this.processRecord(parser, versionFile, sessionProperties, fileEncoding,
                    datasetDescriptorPRO);
        } catch (final BadDelimiterException | IOException | SAXException | URISyntaxException | BadFormatException ex) {
            throw new BusinessException(ex);
        } catch (final Exception ex) {
            AbstractRecorder.LOGGER.error("uncatched error", ex);
            throw new BusinessException(ex);
        }
    }

    public void setNotificationsManager(INotificationsManager notificationsManager) {
        this.notificationsManager = notificationsManager;
    }

    public void setDatasetDescriptorPath(final String datasetDescriptorPath) {
        this.datasetDescriptorPath = datasetDescriptorPath;
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager);
    }

    @Override
    protected void processRecord(CSVParser csvp, VersionFile vf, String fileEncoding) throws BusinessException {

    }

    public int testHeader(final BadsFormatsReport badsFormatsReport, final CSVParser parser)
            throws BusinessException {
        return 0;
    }

    @Override
    public void deleteRecords(VersionFile versionFile) throws BusinessException {
        versionFile.getId();
        this.deleteRecord.deleteRecord(versionFile);
    }

    @Override
    public void setDeleteRecord(final IDeleteRecord deleteRecord) {
        this.deleteRecord = deleteRecord;
    }

    @Override
    public void setProcessRecord(final IProcessRecord processRecord) {
        this.processRecord = processRecord;
    }

    @Override
    public void setSessionPropertiesName(final String sessionPropertiesName) {
        this.sessionPropertiesName = sessionPropertiesName;
    }

    @Override
    public void setTestFormat(final ITestFormat testFormat) {
        this.testFormat = testFormat;
    }

    private ISessionPropertiesPRO getSessionProperties() {
        return (ISessionPropertiesPRO) ApplicationContextHolder.getContext().getBean(
                this.sessionPropertiesName);
    }

    private void testFormat(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String encoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BadFormatException,
            BusinessException {
        this.testFormat.testFormat(parser, versionFile, sessionProperties, encoding, datasetDescriptorPRO);
    }

    private void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties, String fileEncoding,
            DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        Allocator allocator = Allocator.getInstance();
        try {
            allocator.allocate("publish", versionFile.getFileSize() * 100);
            this.processRecord.processRecord(parser, versionFile, sessionProperties, fileEncoding,
                    datasetDescriptorPRO);
        } catch (InterruptedException | BusinessException e) {
            AbstractRecorder.LOGGER.error("can't allocate ", e);
            throw new BusinessException(e);
        } finally {
            allocator.free("publish");
        }
    }

    @Override
    public void setDatasetDescriptor(DatasetDescriptor datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

}
