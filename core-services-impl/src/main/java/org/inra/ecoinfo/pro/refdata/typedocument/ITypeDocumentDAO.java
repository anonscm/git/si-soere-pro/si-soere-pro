package org.inra.ecoinfo.pro.refdata.typedocument;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeDocumentDAO extends IDAO<TypeDocument> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeDocument> getByNKey(String libelle);

    /**
     *
     * @return
     */
    List<TypeDocument> gettAll();
}
