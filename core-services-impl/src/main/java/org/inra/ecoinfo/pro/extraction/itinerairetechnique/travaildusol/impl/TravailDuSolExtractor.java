/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.travaildusol.impl;

import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKExtractor;

/**
 *
 * @author adiankha
 */
public class TravailDuSolExtractor extends AbstractITKExtractor<MesureTravailDuSol, ValeurTravailDuSol> {

    /**
     *
     */
    public static final String TRAVAIL_DUSOL = "travail_dusol";

    /**
     *
     */
    protected static final String MAP_INDEX_TRAVAIL_DUSOL = "travaildusol";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return TRAVAIL_DUSOL;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return TRAVAIL_DUSOL;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_TRAVAIL_DUSOL;
    }

}
