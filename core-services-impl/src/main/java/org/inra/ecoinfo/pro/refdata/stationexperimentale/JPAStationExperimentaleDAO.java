/**
 *
 */
package org.inra.ecoinfo.pro.refdata.stationexperimentale;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAStationExperimentaleDAO extends AbstractJPADAO<StationExperimentale> implements IStationExperimentaleDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.stationexperimentale.IStationExperimentaleDAO #getByNom(java.lang.String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @Override
    public Optional<StationExperimentale> getByNKey(String nom) {
        CriteriaQuery<StationExperimentale> query = builder.createQuery(StationExperimentale.class);
        Root<StationExperimentale> methodeProcess = query.from(StationExperimentale.class);
        query
                .select(methodeProcess)
                .where(
                        builder.equal(methodeProcess.get(StationExperimentale_.nom), nom)
                );
        return getOptional(query);

    }

}
