/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.observationqualitative;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.typeobservationqualitative.ITypeObservationQualitativeDAO;
import org.inra.ecoinfo.pro.refdata.typeobservationqualitative.TypeObservationQualitative;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<ObservationQalitative> {

    private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_BAD_TYPE_OBSERVATION_ITK = "PROPERTY_MSG_BAD_TYPE_OBSERVATION_ITK";

    /**
     *
     */
    public IObservationQualitativeDAO observationQualitativeDAO;
    ITypeObservationQualitativeDAO typeObservationQualitativeDAO;
    private String[] ListeTypePossibles;
    Properties libelleEn;

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                ObservationQalitative obs = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String observation_qua_nom = tokenizerValues.nextToken();
                String type_intervention_nom = tokenizerValues.nextToken();
                TypeObservationQualitative typeIntervention = typeObservationQualitativeDAO.getByNKey(type_intervention_nom).orElse(null);

                observationQualitativeDAO.remove(observationQualitativeDAO.getByNKey(observation_qua_nom, typeIntervention)
                        .orElseThrow(() -> new BusinessException("can't find obesrvationqualitativeF")));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            long line = 0;
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ObservationQalitative.NAME_ENTITY_JPA);
                final String observation_qua_nom = tokenizerValues.nextToken();
                final String type_observation_nom = tokenizerValues.nextToken();
                int index = tokenizerValues.currentTokenIndex();

                TypeObservationQualitative typeObservationQualitative = typeObservationQualitativeDAO.getByNKey(type_observation_nom).orElse(null);
                if (typeObservationQualitative == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_TYPE_OBSERVATION_ITK), line, index, type_observation_nom));
                }

                if (!errorsReport.hasErrors()) {

                    persistObservation(observation_qua_nom, typeObservationQualitative);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<ObservationQalitative> getAllElements() throws BusinessException {
        return observationQualitativeDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ObservationQalitative observation) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(observation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : observation.getObservation_qua_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(observation == null || observation.getObservation_qua_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(observation.getObservation_qua_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(observation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : (observation.getTypeObservationQualitative() != null ? observation.getTypeObservationQualitative().getType_observation_nom() : ""), ListeTypePossibles,
                        null, true, false, true));

        return lineModelGridMetadata;
    }

    private void persistObservation(String observation_qua_nom, TypeObservationQualitative type_observation) throws BusinessException {
        final ObservationQalitative dbObs = observationQualitativeDAO.getByNKey(observation_qua_nom, type_observation).orElse(null);
        createOrUpdateObservation(observation_qua_nom, type_observation, dbObs);

    }

    private void createOrUpdateObservation(String observation_qua_nom, TypeObservationQualitative type_observation_nom, ObservationQalitative dbObs) throws BusinessException {
        if (dbObs == null) {
            ObservationQalitative observation = new ObservationQalitative(observation_qua_nom, type_observation_nom);

            observation.setObservation_qua_nom(observation_qua_nom);
            observation.setTypeObservationQualitative(type_observation_nom);

            createObservation(observation);

        } else {
            updateObservation(observation_qua_nom, dbObs);
        }
    }

    private void createObservation(ObservationQalitative observation) throws BusinessException {
        try {
            observationQualitativeDAO.saveOrUpdate(observation);
            observationQualitativeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create observation");
        }
    }

    private void updateObservation(String observation_qua_nom, ObservationQalitative dbObs) throws BusinessException {
        try {
            dbObs.setObservation_qua_nom(observation_qua_nom);
            observationQualitativeDAO.saveOrUpdate(dbObs);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update observation");
        }
    }

    /**
     *
     * @return
     */
    public IObservationQualitativeDAO getObservationQualitativeDAO() {
        return observationQualitativeDAO;
    }

    /**
     *
     * @param observationQualitativeDAO
     */
    public void setObservationQualitativeDAO(IObservationQualitativeDAO observationQualitativeDAO) {
        this.observationQualitativeDAO = observationQualitativeDAO;
    }

    /**
     *
     * @return
     */
    public ITypeObservationQualitativeDAO getTypeObservationQualitativeDAO() {
        return typeObservationQualitativeDAO;
    }

    /**
     *
     * @param typeObservationQualitativeDAO
     */
    public void setTypeObservationQualitativeDAO(ITypeObservationQualitativeDAO typeObservationQualitativeDAO) {
        this.typeObservationQualitativeDAO = typeObservationQualitativeDAO;
    }

    @Override
    protected ModelGridMetadata<ObservationQalitative> initModelGridMetadata() {
        TypeObservationPossibles();
        libelleEn = localizationManager.newProperties(ObservationQalitative.NAME_ENTITY_JPA, ObservationQalitative.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void TypeObservationPossibles() {

        List<TypeObservationQualitative> groupetype = typeObservationQualitativeDAO.getAll(TypeObservationQualitative.class);
        String[] listeTypePossibles = new String[groupetype.size() + 1];
        listeTypePossibles[0] = "";
        int index = 1;
        for (TypeObservationQualitative typeObservationQualitative : groupetype) {
            listeTypePossibles[index++] = typeObservationQualitative.getType_observation_nom();
        }
        this.ListeTypePossibles = listeTypePossibles;
    }
}
