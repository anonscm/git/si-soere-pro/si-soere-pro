/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.biomasse;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.biomasseprelevee.BiomassePrelevee;
import org.inra.ecoinfo.pro.refdata.biomasseprelevee.BiomassePrelevee_;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPABiomasseDAO extends AbstractJPADAO<BiomassePrelevee> implements IBiomasseDAO {

    /**
     *
     * @return
     */
    @Override
    public List<BiomassePrelevee> getAll() {
        return getAllBy(BiomassePrelevee.class, BiomassePrelevee::getCode);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<BiomassePrelevee> getByName(String nom) {
        CriteriaQuery<BiomassePrelevee> query = builder.createQuery(BiomassePrelevee.class);
        Root<BiomassePrelevee> biomasse = query.from(BiomassePrelevee.class);
        query
                .select(biomasse)
                .where(builder.equal(biomasse.get(BiomassePrelevee_.code), Utils.createCodeFromString(nom)));
        return getOptional(query);

    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<BiomassePrelevee> getByKey(String code) {
        CriteriaQuery<BiomassePrelevee> query = builder.createQuery(BiomassePrelevee.class);
        Root<BiomassePrelevee> biomasse = query.from(BiomassePrelevee.class);
        query
                .select(biomasse)
                .where(builder.equal(biomasse.get(BiomassePrelevee_.code), code));
        return getOptional(query);
    }
}
