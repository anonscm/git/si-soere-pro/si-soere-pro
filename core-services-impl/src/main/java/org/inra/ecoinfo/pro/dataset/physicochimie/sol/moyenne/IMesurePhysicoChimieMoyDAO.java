/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne;

;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesurePhysicoChimieMoyDAO<T> extends IDAO<MesurePhysicoChimieSolsMoy> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesurePhysicoChimieSolsMoy> getByKeys(String keymesure);
}
