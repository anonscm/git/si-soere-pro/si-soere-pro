/*
 *
 */
package org.inra.ecoinfo.pro.refdata.nomenclature;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.typenomenclature.TypeNomenclature;
import org.inra.ecoinfo.pro.refdata.typenomenclature.TypeNomenclature_;

// TODO: Auto-generated Javadoc
/**
 * The Class NomenclatureDAOImpl.
 */
public class NomenclatureDAOImpl extends AbstractJPADAO<Nomenclatures> implements INomenclatureDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Nomenclatures> getAll() {
        return getAll(Nomenclatures.class);
    }

    /**
     *
     * @param nom
     * @param tnnom
     * @return
     */
    public Optional<Nomenclatures> getByNKey(String nom, String tnnom) {
        CriteriaQuery<Nomenclatures> query = builder.createQuery(Nomenclatures.class);
        Root<Nomenclatures> nomenclatures = query.from(Nomenclatures.class);
        Join<Nomenclatures, TypeNomenclature> tn = nomenclatures.join(Nomenclatures_.typenomenclature);
        query
                .select(nomenclatures)
                .where(
                        builder.equal(nomenclatures.get(Nomenclatures_.nomen_nom), nom),
                        builder.equal(tn.get(TypeNomenclature_.tn_nom), tnnom)
                );
        return getOptional(query);

    }

    /**
     *
     * @param typeNomenclature
     * @return
     */
    public List<Nomenclatures> getByTypeNomenclature(TypeNomenclature typeNomenclature) {
        CriteriaQuery<Nomenclatures> query = builder.createQuery(Nomenclatures.class);
        Root<Nomenclatures> nomenclatures = query.from(Nomenclatures.class);
        Join<Nomenclatures, TypeNomenclature> tn = nomenclatures.join(Nomenclatures_.typenomenclature);
        query
                .select(nomenclatures)
                .where(
                        builder.equal(tn, typeNomenclature)
                );
        return getResultList(query);

    }

}
