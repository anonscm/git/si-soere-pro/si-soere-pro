/*
 *
 */
package org.inra.ecoinfo.pro.refdata.caracteristiqueetape;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class CaracteristiqueEtapeDAOImpl extends AbstractJPADAO<CaracteristiqueEtapes> implements ICaracteristiqueEtapeDAO {

    @Override
    public List<CaracteristiqueEtapes> getAll() {
        return getAllBy(CaracteristiqueEtapes.class, CaracteristiqueEtapes::getCetape_intitule);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Optional<CaracteristiqueEtapes> getByNKey(String cetape_intitule) {
        CriteriaQuery<CaracteristiqueEtapes> query = builder.createQuery(CaracteristiqueEtapes.class);
        Root<CaracteristiqueEtapes> bloc = query.from(CaracteristiqueEtapes.class);
        query
                .select(bloc)
                .where(
                        builder.equal(bloc.get(CaracteristiqueEtapes_.cetape_intitule), cetape_intitule)
                );
        return getOptional(query);

    }
}
