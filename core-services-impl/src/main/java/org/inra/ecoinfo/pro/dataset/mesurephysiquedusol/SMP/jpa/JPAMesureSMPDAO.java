/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SMP.jpa;

;
import java.time.LocalTime;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SMP.IMesureSMPDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.MesureSMP;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.MesureSMP_;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.SousSequenceSMP;
import static org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.MesureTS_.sousSequenceTS;

/**
 *
 * @author vjkoyao
 */


public class JPAMesureSMPDAO extends AbstractJPADAO<MesureSMP> implements IMesureSMPDAO<MesureSMP> {

    /**
     *
     * @param heure
     * @param sousSequenceMPS
     * @return
     */
    @Override
    public Optional<MesureSMP> getByNKeys(LocalTime heure, SousSequenceSMP sousSequenceMPS) {
        CriteriaQuery<MesureSMP> query = builder.createQuery(MesureSMP.class);
        Root<MesureSMP> m = query.from(MesureSMP.class);
        Join<MesureSMP, SousSequenceSMP> ss = m.join(MesureSMP_.sousSequenceSMP);
        query
                .select(m)
                .where(
                        builder.equal(ss, sousSequenceTS),
                        builder.equal(m.get(MesureSMP_.heure), heure)
                );
        return getOptional(query);
    }

}
