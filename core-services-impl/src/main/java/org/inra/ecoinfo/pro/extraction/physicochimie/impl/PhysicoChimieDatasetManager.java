/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteBrutDataTypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteMoyenneDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePRODatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePROMoyDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.jsf.IDispositifManager;
import org.inra.ecoinfo.pro.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.IPhysicoChimieDAO;
import org.inra.ecoinfo.pro.refdata.categorievariable.IGroupeVariableBuilder;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author ptcherniati
 * @param <M>
 */
public class PhysicoChimieDatasetManager<M extends MesurePhysicoChimie> extends MO implements IDispositifManager, IVariableManager<Dispositif> {

    Map<String, IPhysicoChimieDAO> extractionsDAO = new HashMap();
    IPhysicoChimieDAO proBrutDAO;
    IPhysicoChimieDAO proMoyenneDAO;
    IPhysicoChimieDAO planteBrutDAO;
    IPhysicoChimieDAO planteMoyenneDAO;
    IPhysicoChimieDAO solBrutDAO;
    IPhysicoChimieDAO solMoyenneDAO;
    IGroupeVariableBuilder groupeVariableBuilder;

    /**
     *
     * @return
     */
    @Override
    public Collection<Dispositif> getAvailablesDispositifs() {
        Collection<Dispositif> availablesDispositif = new LinkedList<>();
        extractionsDAO.entrySet()
                .stream()
                .forEach((entry) -> {
                    final List<Dispositif> dispositifs = entry.getValue().getAvailablesDispositifs(policyManager.getCurrentUser());
                    dispositifs.stream()
                            .filter(dispositif -> !availablesDispositif.contains(dispositif))
                            .forEach(dispositif -> availablesDispositif.add(dispositif));
                });
        return availablesDispositif;
    }

    /**
     *
     * @param proBrutDAO
     */
    public void setProBrutDAO(IPhysicoChimieDAO proBrutDAO) {
        this.proBrutDAO = proBrutDAO;
        this.extractionsDAO.put(IPhysicoChimiePRODatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO, proBrutDAO);
    }

    /**
     *
     * @param proMoyenneDAO
     */
    public void setProMoyenneDAO(IPhysicoChimieDAO proMoyenneDAO) {
        this.proMoyenneDAO = proMoyenneDAO;
        this.extractionsDAO.put(IPhysicoChimiePROMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO_MOY, proMoyenneDAO);
    }

    /**
     *
     * @param planteBrutDAO
     */
    public void setPlanteBrutDAO(IPhysicoChimieDAO planteBrutDAO) {
        this.planteBrutDAO = planteBrutDAO;
        this.extractionsDAO.put(IPhysicoChimiePlanteBrutDataTypeManager.CODE_DATATYPE_PLANTE_BRUT, planteBrutDAO);
    }

    /**
     *
     * @param planteMoyenneDAO
     */
    public void setPlanteMoyenneDAO(IPhysicoChimieDAO planteMoyenneDAO) {
        this.planteMoyenneDAO = planteMoyenneDAO;
        this.extractionsDAO.put(IPhysicoChimiePlanteMoyenneDatatypeManager.CODE_DATATYPE_PLANTE_MOYENNE, planteMoyenneDAO);
    }

    /**
     *
     * @param solBrutDAO
     */
    public void setSolBrutDAO(IPhysicoChimieDAO solBrutDAO) {
        this.solBrutDAO = solBrutDAO;
        this.extractionsDAO.put(IPhysicoChimieSolDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE, solBrutDAO);
    }

    /**
     *
     * @param solMoyenneDAO
     */
    public void setSolMoyenneDAO(IPhysicoChimieDAO solMoyenneDAO) {
        this.solMoyenneDAO = solMoyenneDAO;
        this.extractionsDAO.put(IPhysicoChimieSolMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_MOY, solMoyenneDAO);
    }

    /**
     *
     * @param groupeVariableBuilder
     */
    public void setGroupeVariableBuilder(IGroupeVariableBuilder groupeVariableBuilder) {
        this.groupeVariableBuilder = groupeVariableBuilder;
    }

    /**
     *
     * @param linkedList
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByDispositif(List<Dispositif> linkedList) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        extractionsDAO.entrySet().forEach((entry) -> {
            String datatype = entry.getKey();
            IPhysicoChimieDAO physicoChimieDAO = entry.getValue();
            variablesByDatatype.put(datatype, physicoChimieDAO.getAvailablesVariablesByDispositif(linkedList, policyManager.getCurrentUser()));
        });
        return variablesByDatatype;
    }

}
