/*
 *
 */
package org.inra.ecoinfo.pro.refdata.matierepremiere;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.inra.ecoinfo.AbstractJPADAO;

// TODO: Auto-generated Javadoc
/**
 * The Class MatierePremiereDAOImpl.
 */
public class MatierePremiereDAOImpl extends AbstractJPADAO<MatieresPremieres> implements IMatierePremiereDAO {

    @Override
    public List<MatieresPremieres> getAll() {
        return getAll(MatieresPremieres.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Optional<MatieresPremieres> getByNKey(String nom) {
        CriteriaQuery<MatieresPremieres> query = builder.createQuery(MatieresPremieres.class);
        Root<MatieresPremieres> matieresPremieres = query.from(MatieresPremieres.class);
        query
                .select(matieresPremieres)
                .where(
                        builder.equal(matieresPremieres.get(MatieresPremieres_.codecomposant), nom)
                );
        return getOptional(query);
    }
}
