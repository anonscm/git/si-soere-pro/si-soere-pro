/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;

/**
 *
 * @author adiankha
 */
public interface ITestFormat extends Serializable{
    
    /**
     *
     */
    String PROPERTY_MSG_ERROR_BAD_FORMAT     = "PROPERTY_MSG_ERROR_BAD_FORMAT";

    /**
     *
     */
    String PROPERTY_MSG_CHECKING_FORMAT_FILE = "PROPERTY_MSG_CHECKING_FORMAT_FILE";

    /**
     *
     */
    String PROPERTY_MSG_BAD_HEADER_SIZE      = "PROPERTY_MSG_BAD_HEADER_SIZE";

    /**
     *
     */
    String DATASET_DESCRIPTOR_XML            = "dataset-descriptor.xml";

    /**
     *
     * @param datatypeName
     */
    void setDatatypeName(String datatypeName);

    /**
     *
     * @param testHeaders
     */
    void setTestHeaders(ITestHeaders testHeaders);

    /**
     *
     * @param testValues
     */
    void setTestValues(ITestValues testValues);

    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param datasetDescriptor
     * @throws BadFormatException
     */
    void testFormat(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String encoding,
            DatasetDescriptorPRO datasetDescriptor) throws BadFormatException;
    
}
