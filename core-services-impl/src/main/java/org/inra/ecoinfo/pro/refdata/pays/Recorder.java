/**
 *
 */
package org.inra.ecoinfo.pro.refdata.pays;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Pays> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IPaysDAO paysDAO;
    private Properties propertiesNomPays;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Pays.TABLE_NAME);

                int indexPays = tokenizerValues.currentTokenIndex();
                String nomPays = tokenizerValues.nextToken();

                //nom du pays vérifié car à cause de nom en anglais la colonne Nom du pays est indiquée comme pouvant être null dans le dataset-descriptor
                if (nomPays == null || nomPays.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexPays + 1, datasetDescriptor.getColumns().get(0).getName()));
                }

                Pays dbPays = paysDAO.getByNKey(nomPays).orElse(null);

                if (dbPays == null && !errorsReport.hasErrors()) {
                    dbPays = new Pays(nomPays, Utils.createCodeFromString(nomPays));
                    paysDAO.saveOrUpdate(dbPays);
                }

                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (NoResultException e2) {
            throw new BusinessException(e2.getMessage(), e2);
        } catch (NonUniqueResultException e3) {
            throw new BusinessException(e3.getMessage(), e3);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nomPays = tokenizerValues.nextToken();

                Pays pays = paysDAO.getByNKey(nomPays)
                        .orElseThrow(() -> new BusinessException("can't get pays"));

                paysDAO.remove(pays);

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Pays pays) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String localizedChampNomPays = "";

        if (pays != null) {
            localizedChampNomPays = propertiesNomPays.getProperty(pays.getNom(), pays.getNom());
        }

        // Nom du pays
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pays == null ? Constantes.STRING_EMPTY : pays.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(pays == null ? Constantes.STRING_EMPTY : localizedChampNomPays, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Pays> initModelGridMetadata() {

        propertiesNomPays = localizationManager.newProperties(Pays.TABLE_NAME, RefDataConstantes.COLUMN_NOM_PAYS, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements() Pays.class
     */
    @Override
    protected List<Pays> getAllElements() throws BusinessException {
        return paysDAO.getAll();
    }

    /**
     * @param paysDAO the paysDAO to set
     */
    public void setPaysDAO(IPaysDAO paysDAO) {
        this.paysDAO = paysDAO;
    }

}
