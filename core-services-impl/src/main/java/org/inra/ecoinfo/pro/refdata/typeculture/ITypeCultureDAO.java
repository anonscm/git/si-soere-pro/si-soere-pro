/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typeculture;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeCultureDAO extends IDAO<TypeCulture> {

    List<TypeCulture> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeCulture> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<TypeCulture> getByCode(String code);

}
