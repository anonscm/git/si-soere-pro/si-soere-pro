
package org.inra.ecoinfo.pro.refdata.environnementdispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.environnement.Environnement;
import org.inra.ecoinfo.pro.refdata.environnement.IEnvironnementDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<EnvironnementDispositif> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO = "PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO";
    private static final String PROPERTY_MSG_ENVIRONNEMENT = "PROPERTY_MSG_ENVIRONNEMENT";
    private static final String PROPERTY_MSG_ENVIRONNEMENT_BAD_CODE_ENVIRONNEMENT = "PROPERTY_MSG_ENVIRONNEMENT_BAD_CODE_ENVIRONNEMENT";
    IEnvironnementDispositifDAO envidispoDAO;
    IEnvironnementDAO environnementDAO;
    IDispositifDAO dispositifDAO;
    Properties CommentaireEn;

    private String[] listeDispositifPossibles;
    private String[] listCodeEnvironnement;
    private Map<String, String[]> listeLieuPossibles;

    private void createEnviDisposi(EnvironnementDispositif envidispo) throws BusinessException {
        try {
            envidispoDAO.saveOrUpdate(envidispo);
            envidispoDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void updateEnviDispo(EnvironnementDispositif dbenvidispo, String commentaire, Environnement environnement, Dispositif dispositif) {
        dbenvidispo.setDispositif(dispositif);
        dbenvidispo.setEnvironnement(environnement);
        dbenvidispo.setCommentaire(commentaire);
    }

    private void createOrUpdateEnviDispo(EnvironnementDispositif dbenvidispo, String commentaire, Environnement environnement, Dispositif dispositif) throws BusinessException {
        if (dbenvidispo == null) {
            EnvironnementDispositif envidispo = new EnvironnementDispositif(commentaire, dispositif, environnement);
            envidispo.setCommentaire(commentaire);
            envidispo.setEnvironnement(environnement);
            envidispo.setDispositif(dispositif);
            createEnviDisposi(envidispo);
        } else {
            updateEnviDispo(dbenvidispo, commentaire, environnement, dispositif);
        }
    }

    private void persistEnviDispo(String comentaire, Environnement environnement, Dispositif dispositif) throws BusinessException, BusinessException {
        EnvironnementDispositif dbenvidispo = envidispoDAO.getByNKey(environnement, dispositif).orElse(null);
        createOrUpdateEnviDispo(dbenvidispo, comentaire, environnement, dispositif);
    }

    private void disposifPossibles() {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCodeDispo();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void typeDirectionVentPossibles() {
        List<Environnement> environnement = environnementDAO.getAll();
        String[] listevent = new String[environnement.size() + 1];
        listevent[0] = "";
        int index = 1;
        for (Environnement env : environnement) {
            listevent[index++] = Environnement.buildCodeEnvironnement(env.getDirectionventdominant(), env.getDistanceaxeroutier(), env.getDistanceagglomeration());
        }
        this.listCodeEnvironnement = listevent;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, EnvironnementDispositif.NAME_ENTITY_JPA);
                String dispo = tokenizerValues.nextToken();
                String lieu = tokenizerValues.nextToken();
                Dispositif dbdispo = dispositifDAO.getByNKey(dispo, lieu).orElse(null);
                String codeEnvironnement = tokenizerValues.nextToken();
                Environnement environnemment = getEnvironnemment(codeEnvironnement, new ErrorsReport(), -1);
                EnvironnementDispositif dbpedologie = envidispoDAO.getByNKey(environnemment, dbdispo)
                        .orElseThrow(() -> new BusinessException("can't get environnement dispositif"));
                envidispoDAO.remove(dbpedologie);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<EnvironnementDispositif> getAllElements() throws BusinessException {
        return envidispoDAO.gettAll();
    }

    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            long line = 0;
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EnvironnementDispositif.NAME_ENTITY_JPA);
                int indexdispo = tokenizerValues.currentTokenIndex();
                String codedispo = tokenizerValues.nextToken();
                String lieu = tokenizerValues.nextToken();
                int index1 = tokenizerValues.currentTokenIndex();
                String codeEnvironnement = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();
                Environnement dbenvi = null;
                try {
                    dbenvi = getEnvironnemment(codeEnvironnement, errorsReport, line);
                } catch (NumberFormatException nfe) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ENVIRONNEMENT), line, index1, codeEnvironnement));

                }
                if (dbenvi == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ENVIRONNEMENT), line, codeEnvironnement));
                }
                Dispositif dbdispositif = dispositifDAO.getByNKey(codedispo, lieu).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO), line, indexdispo, codedispo, lieu));
                }
                if (!errorsReport.hasErrors()) {
                    persistEnviDispo(commentaire, dbenvi, dbdispositif);
                }
                values = csvp.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EnvironnementDispositif envidispo) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String valeurproduit = envidispo == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : envidispo.getDispositif() != null ? envidispo.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurproduit, listeDispositifPossibles, null, true, false, true);
        String valeurLieu = envidispo == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : envidispo.getDispositif() != null ? envidispo.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurLieu, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurLieu);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

         lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(envidispo == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        envidispo.getEnvironnement().getDirectionventdominant() != null ? 
                                Environnement.buildCodeEnvironnement(
                                        envidispo.getEnvironnement().getDirectionventdominant(),
                                        envidispo.getEnvironnement().getDistanceaxeroutier(),
                                        envidispo.getEnvironnement().getDistanceagglomeration()
                                ) : 
                                "", 
                        listCodeEnvironnement, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(envidispo == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        envidispo.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(envidispo == null || envidispo.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : 
                        CommentaireEn.getProperty(envidispo.getCommentaire()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @param envidispoDAO
     */
    public void setEnvidispoDAO(IEnvironnementDispositifDAO envidispoDAO) {
        this.envidispoDAO = envidispoDAO;
    }

    /**
     *
     * @param environnementDAO
     */
    public void setEnvironnementDAO(IEnvironnementDAO environnementDAO) {
        this.environnementDAO = environnementDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    @Override
    protected ModelGridMetadata<EnvironnementDispositif> initModelGridMetadata() {
        typeDirectionVentPossibles();
        disposifPossibles();
        CommentaireEn = localizationManager.newProperties(EnvironnementDispositif.NAME_ENTITY_JPA, EnvironnementDispositif.COLUMN_COMMENT_JPA, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public Properties getCommentaireEn() {
        return CommentaireEn;
    }

    /**
     *
     * @param CommentaireEn
     */
    public void setCommentaireEn(Properties CommentaireEn) {
        this.CommentaireEn = CommentaireEn;
    }

    private Environnement getEnvironnemment(String codeEnvironnement, ErrorsReport errorsReport, long line) {
        List<String> parseCode = Stream.of(codeEnvironnement.split("_")).collect(Collectors.toList());
        String directionDuvent = parseCode.get(0);
        Double axe = axe = Double.parseDouble(parseCode.get(1));
        Double agglo = Double.parseDouble(parseCode.get(2));
        assert Environnement.buildCodeEnvironnement(directionDuvent, INTEGER_TYPE, INTEGER_TYPE).equals(codeEnvironnement) : "bad code environnement";
        return environnementDAO.getByNKey(axe, agglo, directionDuvent).orElse(null);
    }

}
