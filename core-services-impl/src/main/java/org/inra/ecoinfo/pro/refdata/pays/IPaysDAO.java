/**
 *
 */
package org.inra.ecoinfo.pro.refdata.pays;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IPaysDAO extends IDAO<Pays> {

    /**
     *
     * @return
     */
    List<Pays> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Pays> getByNKey(String nom);
}
