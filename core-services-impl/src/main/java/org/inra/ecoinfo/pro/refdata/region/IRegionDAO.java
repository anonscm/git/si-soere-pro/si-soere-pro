/**
 *
 */
package org.inra.ecoinfo.pro.refdata.region;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.pays.Pays;

/**
 * @author sophie
 *
 */
public interface IRegionDAO extends IDAO<Region> {

    Optional<Region> getByNom(String nom);

    Optional<Region> getByNKey(String code);

    List<Region> getByPays(Pays pays);

    List<Region> getAll();

}
