/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.impl;

import java.time.LocalDate;

/**
 *
 * @author adiankha
 */
public class RecolteCoupeLineRecord implements Comparable<RecolteCoupeLineRecord> {

    Long originalLineNumber;
    LocalDate datedebut;
    String codedispositif;
    String codetraitement;
    String codeparcelle;
    String nomplacette;
    String localisationprecise;
    LocalDate datefin;
    String culture;
    String codebbch;
    String precisionstade;
    String interventionrecolte;
    String materielplante1;
    String materielplante2;
    String materielplante3;
    String conditionhumidite;
    String conditiontemperature;
    String vitessevent;
    String observationqualite;
    String nomobservation;
    String niveauatteint;
    String commentaire;

    /**
     *
     */
    public RecolteCoupeLineRecord() {
    }

    /**
     *
     * @param originalLineNumber
     * @param datedebut
     * @param codedispositif
     * @param codetraitement
     * @param codeparcelle
     * @param nomplacette
     * @param localisationprecise
     * @param datefin
     * @param culture
     * @param codebbch
     * @param precisionstade
     * @param interventionrecolte
     * @param materielplante1
     * @param materielplante2
     * @param materielplante3
     * @param conditionhumidite
     * @param conditiontemperature
     * @param vitessevent
     * @param observationqualite
     * @param nomobservation
     * @param niveauatteint
     * @param commentaire
     */
    public RecolteCoupeLineRecord(Long originalLineNumber, LocalDate datedebut, String codedispositif, String codetraitement, String codeparcelle, String nomplacette, String localisationprecise, LocalDate datefin, String culture, String codebbch, String precisionstade, String interventionrecolte, String materielplante1, String materielplante2, String materielplante3, String conditionhumidite, String conditiontemperature, String vitessevent, String observationqualite, String nomobservation, String niveauatteint, String commentaire) {
        this.originalLineNumber = originalLineNumber;
        this.datedebut = datedebut;
        this.codedispositif = codedispositif;
        this.codetraitement = codetraitement;
        this.codeparcelle = codeparcelle;
        this.nomplacette = nomplacette;
        this.localisationprecise = localisationprecise;
        this.datefin = datefin;
        this.culture = culture;
        this.codebbch = codebbch;
        this.precisionstade = precisionstade;
        this.interventionrecolte = interventionrecolte;
        this.materielplante1 = materielplante1;
        this.materielplante2 = materielplante2;
        this.materielplante3 = materielplante3;
        this.conditionhumidite = conditionhumidite;
        this.conditiontemperature = conditiontemperature;
        this.vitessevent = vitessevent;
        this.observationqualite = observationqualite;
        this.nomobservation = nomobservation;
        this.niveauatteint = niveauatteint;
        this.commentaire = commentaire;
    }

    @Override
    public int compareTo(RecolteCoupeLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatedebut() {
        return datedebut;
    }

    /**
     *
     * @param datedebut
     */
    public void setDatedebut(LocalDate datedebut) {
        this.datedebut = datedebut;
    }

    /**
     *
     * @return
     */
    public String getCodedispositif() {
        return codedispositif;
    }

    /**
     *
     * @param codedispositif
     */
    public void setCodedispositif(String codedispositif) {
        this.codedispositif = codedispositif;
    }

    /**
     *
     * @return
     */
    public String getCodetraitement() {
        return codetraitement;
    }

    /**
     *
     * @param codetraitement
     */
    public void setCodetraitement(String codetraitement) {
        this.codetraitement = codetraitement;
    }

    /**
     *
     * @return
     */
    public String getCodeparcelle() {
        return codeparcelle;
    }

    /**
     *
     * @param codeparcelle
     */
    public void setCodeparcelle(String codeparcelle) {
        this.codeparcelle = codeparcelle;
    }

    /**
     *
     * @return
     */
    public String getNomplacette() {
        return nomplacette;
    }

    /**
     *
     * @param nomplacette
     */
    public void setNomplacette(String nomplacette) {
        this.nomplacette = nomplacette;
    }

    /**
     *
     * @return
     */
    public String getLocalisationprecise() {
        return localisationprecise;
    }

    /**
     *
     * @param localisationprecise
     */
    public void setLocalisationprecise(String localisationprecise) {
        this.localisationprecise = localisationprecise;
    }

    /**
     *
     * @return
     */
    public LocalDate getDatefin() {
        return datefin;
    }

    /**
     *
     * @param datefin
     */
    public void setDatefin(LocalDate datefin) {
        this.datefin = datefin;
    }

    /**
     *
     * @return
     */
    public String getCulture() {
        return culture;
    }

    /**
     *
     * @param culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     *
     * @return
     */
    public String getCodebbch() {
        return codebbch;
    }

    /**
     *
     * @param codebbch
     */
    public void setCodebbch(String codebbch) {
        this.codebbch = codebbch;
    }

    /**
     *
     * @return
     */
    public String getPrecisionstade() {
        return precisionstade;
    }

    /**
     *
     * @param precisionstade
     */
    public void setPrecisionstade(String precisionstade) {
        this.precisionstade = precisionstade;
    }

    /**
     *
     * @return
     */
    public String getInterventionrecolte() {
        return interventionrecolte;
    }

    /**
     *
     * @param interventionrecolte
     */
    public void setInterventionrecolte(String interventionrecolte) {
        this.interventionrecolte = interventionrecolte;
    }

    /**
     *
     * @return
     */
    public String getMaterielplante1() {
        return materielplante1;
    }

    /**
     *
     * @param materielplante1
     */
    public void setMaterielplante1(String materielplante1) {
        this.materielplante1 = materielplante1;
    }

    /**
     *
     * @return
     */
    public String getMaterielplante2() {
        return materielplante2;
    }

    /**
     *
     * @param materielplante2
     */
    public void setMaterielplante2(String materielplante2) {
        this.materielplante2 = materielplante2;
    }

    /**
     *
     * @return
     */
    public String getMaterielplante3() {
        return materielplante3;
    }

    /**
     *
     * @param materielplante3
     */
    public void setMaterielplante3(String materielplante3) {
        this.materielplante3 = materielplante3;
    }

    /**
     *
     * @return
     */
    public String getConditionhumidite() {
        return conditionhumidite;
    }

    /**
     *
     * @param conditionhumidite
     */
    public void setConditionhumidite(String conditionhumidite) {
        this.conditionhumidite = conditionhumidite;
    }

    /**
     *
     * @return
     */
    public String getConditiontemperature() {
        return conditiontemperature;
    }

    /**
     *
     * @param conditiontemperature
     */
    public void setConditiontemperature(String conditiontemperature) {
        this.conditiontemperature = conditiontemperature;
    }

    /**
     *
     * @return
     */
    public String getVitessevent() {
        return vitessevent;
    }

    /**
     *
     * @param vitessevent
     */
    public void setVitessevent(String vitessevent) {
        this.vitessevent = vitessevent;
    }

    /**
     *
     * @return
     */
    public String getObservationqualite() {
        return observationqualite;
    }

    /**
     *
     * @param observationqualite
     */
    public void setObservationqualite(String observationqualite) {
        this.observationqualite = observationqualite;
    }

    /**
     *
     * @return
     */
    public String getNomobservation() {
        return nomobservation;
    }

    /**
     *
     * @param nomobservation
     */
    public void setNomobservation(String nomobservation) {
        this.nomobservation = nomobservation;
    }

    /**
     *
     * @return
     */
    public String getNiveauatteint() {
        return niveauatteint;
    }

    /**
     *
     * @param niveauatteint
     */
    public void setNiveauatteint(String niveauatteint) {
        this.niveauatteint = niveauatteint;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

}
