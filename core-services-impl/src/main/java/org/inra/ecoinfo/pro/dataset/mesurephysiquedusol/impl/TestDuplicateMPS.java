/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.ITestDuplicates;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
public class TestDuplicateMPS extends AbstractTestDuplicate {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicateMPS.class
            .getName());

    static final long serialVersionUID = 1L;

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";

    final SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine;

    /**
     *
     */
    public TestDuplicateMPS() {
        this.dateTimeLine = new TreeMap();
    }

    void addLine(final String date, final String time, final long lineNumber) {
        if (!this.dateTimeLine.containsKey(date)) {
            final SortedMap<String, SortedSet<Long>> timeMap = new TreeMap();
            this.dateTimeLine.put(date, timeMap);
        }
        if (this.dateTimeLine.get(date).containsKey(time)) {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicateMPS.BUNDLE_SOURCE_PATH,
                    ITestDuplicates.PROPERTY_MSG_DOUBLON_LINE), lineNumber, date, time,
                    this.dateTimeLine.get(date).get(time).first().intValue()));
        } else {
            final SortedSet<Long> setLine = new TreeSet();
            setLine.add(lineNumber);
            this.dateTimeLine.get(date).put(time, setLine);
        }
        this.dateTimeLine.get(date).get(time).add(lineNumber);
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(final String[] values, final long lineNumber, String[] dates,
            VersionFile versionFile) {
        this.addLine(values[0], values[1], lineNumber);
    }

}
