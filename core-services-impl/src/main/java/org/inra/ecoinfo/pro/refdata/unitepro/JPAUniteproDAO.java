/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.unitepro;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAUniteproDAO extends AbstractJPADAO<Unitepro> implements IUniteproDAO {

    @Override
    public List<Unitepro> getAll() {
        return getAllBy(Unitepro.class, Unitepro::getCode);
    }

    @Override
    public Optional<Unitepro> getByNKey(String code) {
        CriteriaQuery<Unitepro> query = builder.createQuery(Unitepro.class);
        Root<Unitepro> unitepro = query.from(Unitepro.class);
        query
                .select(unitepro)
                .where(
                        builder.equal(unitepro.get(Unitepro_.code), code)
                );
        return getOptional(query);
    }

    @Override
    public Optional<Unitepro> getByMyKey(String mycode) {
        CriteriaQuery<Unitepro> query = builder.createQuery(Unitepro.class);
        Root<Unitepro> unitepro = query.from(Unitepro.class);
        query
                .select(unitepro)
                .where(
                        builder.equal(unitepro.get(Unitepro_.mycode), mycode)
                );
        return getOptional(query);
    }

}
