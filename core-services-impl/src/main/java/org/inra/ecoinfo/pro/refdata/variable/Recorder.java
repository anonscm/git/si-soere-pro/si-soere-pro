/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.variable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.categorievariable.CategorieVariable;
import org.inra.ecoinfo.pro.refdata.categorievariable.ICategorieVariableDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariablesPRO> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_DTVQ_CV_BAD_CODE = "PROPERTY_MSG_DTVQ_CV_BAD_CODE";

    IVariablesPRODAO variPRODAO;
    Properties nomEn;
    Properties definitionEn;
    Properties codeVariableEn;
    ICategorieVariableDAO categorievariableDAO;
    private String[] listeCategorieVariablePossibles;

    void createOrUpdateVariablesPRO(String code, final String affichage, CategorieVariable categorie, final String definition,
            final String isQualitativeString, final VariablesPRO dbVariable)
            throws BusinessException {
        try {
            boolean isQualitative = false;
            if (Boolean.TRUE.toString().equalsIgnoreCase(isQualitativeString)) {
                isQualitative = true;
            }
            if (dbVariable == null) {
                String codeUser = Utils.createCodeFromString(affichage);
                final VariablesPRO variable = new VariablesPRO(code, affichage, definition, isQualitative, codeUser, categorie);
                this.variPRODAO.saveOrUpdate(variable);
            } else {
                dbVariable.setCategorievariable(categorie);
                dbVariable.setDefinition(definition);
                dbVariable.setIsQualitative(isQualitative);
                this.variPRODAO.saveOrUpdate(dbVariable);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create variable");
        }
    }

    private void persistVariable(final String code, String affichage, CategorieVariable categorie, final String definition, final String isQualitativeString
    ) throws BusinessException, BusinessException {
        final VariablesPRO dbvariable = (VariablesPRO) this.variPRODAO.getByCode(code).orElse(null);

        this.createOrUpdateVariablesPRO(code, affichage, categorie, definition, isQualitativeString, dbvariable);
    }

    @Override
    public void deleteRecord(final CSVParser csvp, final File file, final String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nom = tokenizerValues.nextToken();
                VariablesPRO dbvariable = variPRODAO.getByNames(nom)
                        .orElseThrow(()->new BusinessException("can't get variable"));
                variPRODAO.remove(dbvariable);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<VariablesPRO> getAllElements() throws BusinessException {
        List<VariablesPRO> all = this.variPRODAO.getAllVariablePRO();
        Collections.sort(all);
        return all;
    }

    private void initCategorieVariable()  {
        List<CategorieVariable> groupecategorie = categorievariableDAO.gettAll();
        String[] listeCategorie = new String[groupecategorie.size() + 1];
        listeCategorie[0] = "";
        int index = 1;
        for (CategorieVariable cv : groupecategorie) {
            listeCategorie[index++] = cv.getCvariable_code();
        }
        this.listeCategorieVariablePossibles = listeCategorie;
    }

    @Override
    public void processRecord(final CSVParser csvp, final File file, final String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            long line = 0;
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, VariablesPRO.NAME_ENTITY_JPA);
                final String affichage = tokenizerValues.nextToken();
                final String code = tokenizerValues.nextToken();
                int indexcat = tokenizerValues.currentTokenIndex();
                final String categorie = tokenizerValues.nextToken();
                final CategorieVariable dbcategorie = categorievariableDAO.getByMyKey(Utils.createCodeFromString(categorie)).orElse(null);
                if (dbcategorie == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_DTVQ_CV_BAD_CODE), line, indexcat, categorie));
                }
                final String definition = tokenizerValues.nextToken();
                final String isQualitativeString = values.length > 4 ? tokenizerValues.nextToken()
                        : "false";
                if (!errorsReport.hasErrors()) {
                    persistVariable(code, affichage, dbcategorie, definition, isQualitativeString);
                }
                values = csvp.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(VariablesPRO variable) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null || variable.getAffichage()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getAffichage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null || variable.getAffichage()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : codeVariableEn.getProperty(variable.getAffichage()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null || variable.getName()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null || variable.getName() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomEn.getProperty(variable.getName()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : variable.getCategorievariable() != null ? variable.getCategorievariable().getCvariable_code()
                        : "", listeCategorieVariablePossibles,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null || variable.getDefinition() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getDefinition(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(variable == null || variable.getDefinition() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : definitionEn.getProperty(variable.getDefinition()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        final String[] booleans = new String[]{Boolean.FALSE.toString(), Boolean.TRUE.toString()};

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable != null && variable.getIsQualitative(), booleans, null, false, false, true));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<VariablesPRO> initModelGridMetadata() {
        initCategorieVariable();
        codeVariableEn = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariablesPRO.class), "nom", Locale.ENGLISH);
        nomEn = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariablesPRO.class), "affichage", Locale.ENGLISH);
        definitionEn = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariablesPRO.class), "definition", Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    public void setCategorievariableDAO(ICategorieVariableDAO categorievariableDAO) {
        this.categorievariableDAO = categorievariableDAO;
    }

}
