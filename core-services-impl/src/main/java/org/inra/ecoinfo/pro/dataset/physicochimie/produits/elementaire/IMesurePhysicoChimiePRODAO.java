/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesurePhysicoChimiePRODAO<T> extends IDAO<MesurePhysicoChimiePRO> {

    /**
     *
     * @param code_unique_echantillon
     * @param nomlabo
     * @param dateprelevement
     * @return
     */
    Optional<MesurePhysicoChimiePRO> getByNKeys(String code_unique_echantillon, String nomlabo, LocalDate dateprelevement);

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesurePhysicoChimiePRO> getByKeys(String keymesure);
    
}
