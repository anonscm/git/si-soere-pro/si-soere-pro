/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 * @param <V>
 */
public abstract class AbstractITKExtractor<M extends MesureITK, V extends ValeurITK>  extends MO implements IExtractor{

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.messages";

    /**
     *
     */
    protected static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     *
     */
    protected static final String MSG_BADS_RIGHTS = "PROPERTY_MSG_BADS_RIGHTS";
    private Extraction extraction;
    IITKDAO<M, V> itkDAO;

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        List<VariablesPRO> variables = sortSelectedVariables(parameters);
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        final Map<String, List> filteredResultsDatasMap = resultsDatasMap;
        parameters
                .getResults()
                .computeIfAbsent(getResultExtractionCode(), k -> new HashMap())
                .put(MAP_INDEX_0, filteredResultsDatasMap.get(getResultMapIndex()));
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    List<VariablesPRO> sortSelectedVariables(IParameter parameters) {
        return Optional.ofNullable(parameters)
                .map(p -> p.getParameters())
                .map(ps -> (List<VariablesPRO>) ps.get(VariablesPRO.class.getSimpleName().concat(getVariableParameter())))
                .orElse(new LinkedList<>())
                .stream()
                .sorted((o1, o2) -> o1.getId().toString().compareTo(o2.getId().toString()))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final IntervalDate intervalDate = retrieveDates(requestMetadatasMap);
            final List<INodeable> selectedDispositif = retrieveDispositif(requestMetadatasMap);
            final List<VariablesPRO> selectedVariables = retrieveVariables(requestMetadatasMap);
            final List<M> mesures = itkDAO.extractMesureITK(selectedDispositif, selectedVariables, intervalDate, policyManager.getCurrentUser());
            if (CollectionUtils.isEmpty(mesures)) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        NoExtractionResultException.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
            extractedDatasMap.put(getResultMapIndex(), mesures);
        } catch (final DateTimeParseException | BadExpectedValueException | BusinessException e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     */
    protected List<VariablesPRO> retrieveVariables(Map<String, Object> requestMetadatasMap) {
        return (List<VariablesPRO>) requestMetadatasMap
                .get(getVariableParameter());
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     */
    protected List<INodeable> retrieveDispositif(Map<String, Object> requestMetadatasMap) {
        return (List<INodeable>) requestMetadatasMap
                .get(Dispositif.class.getSimpleName());
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BadExpectedValueException
     */
    protected IntervalDate retrieveDates(Map<String, Object> requestMetadatasMap) throws BadExpectedValueException {
        return (IntervalDate) requestMetadatasMap
                .get(IntervalDate.class.getSimpleName());
    }

    @Override
    public void setExtraction(Extraction extraction) {
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    abstract protected String getResultExtractionCode();

    /**
     *
     * @return
     */
    abstract protected String getVariableParameter();

    /**
     *
     * @return
     */
    abstract protected String getResultMapIndex();

    /**
     *
     * @param itkDAO
     */
    public void setItkDAO(IITKDAO<M, V> itkDAO) {
        this.itkDAO = itkDAO;
    }
    
}
