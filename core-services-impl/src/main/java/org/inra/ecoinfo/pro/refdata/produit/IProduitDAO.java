/*
 *
 */
package org.inra.ecoinfo.pro.refdata.produit;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

// TODO: Auto-generated Javadoc
/**
 * The Interface IProduitDAO.
 */
public interface IProduitDAO extends IDAO<Produits> {

    /**
     *
     * @return
     */
    List<Produits> getAll();

    /**
     *
     * @param codeuser
     * @param noDepartement
     * @param an
     * @param detenteur
     * @return
     */
    Optional<Produits> getByNKey(String codeuser, String noDepartement, int an,  String detenteur);

    /**
     *
     * @param prod_key
     * @return
     */
    Optional<Produits> getByNKey(String prod_key);
}
