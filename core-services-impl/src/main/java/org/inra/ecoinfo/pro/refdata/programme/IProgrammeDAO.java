/**
 *
 */
package org.inra.ecoinfo.pro.refdata.programme;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IProgrammeDAO extends IDAO<Programme> {

    /**
     *
     * @param acronyme
     * @param nom
     * @return
     */
    Optional<Programme> getByNKey(String acronyme, String nom);

}
