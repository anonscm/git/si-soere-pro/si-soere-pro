/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy_;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationIncubationSolMoyDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurIncubationSolMoy> deleteValeurs = builder.createCriteriaDelete(ValeurIncubationSolMoy.class);
        Root<ValeurIncubationSolMoy> valeur = deleteValeurs.from(ValeurIncubationSolMoy.class);
        Subquery<MesureIncubationSolMoy> subquery = deleteValeurs.subquery(MesureIncubationSolMoy.class);
        Root<MesureIncubationSolMoy> m = subquery.from(MesureIncubationSolMoy.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureIncubationSolMoy_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurIncubationSolMoy_.mesureIncubationSolMoy).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureIncubationSolMoy> deleteSequence = builder.createCriteriaDelete(MesureIncubationSolMoy.class);
        Root<MesureIncubationSolMoy> sequence = deleteSequence.from(MesureIncubationSolMoy.class);
        deleteSequence.where(builder.equal(sequence.get(MesureIncubationSolMoy_.versionfile), version));
        delete(deleteSequence);
    }

}
