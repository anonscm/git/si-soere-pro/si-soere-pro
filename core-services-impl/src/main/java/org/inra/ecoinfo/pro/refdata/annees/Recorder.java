package org.inra.ecoinfo.pro.refdata.annees;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

public class Recorder extends AbstractCSVMetadataRecorder<Annees> {

    protected IAnneesDAO anneeDAO;

    /**
     *
     * @param annees
     * @throws BusinessException
     */
    private void createAnnees(final String nom) throws BusinessException {
        try {
            final Annees annees = new Annees(nom);
            anneeDAO.saveOrUpdate(annees);
            anneeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("Can't save années", ex);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Annees.NAME_ENTITY_JPA);
                final String valeur = tokenizerValues.nextToken();
                final Annees dbannees = anneeDAO.getByNKey(valeur).orElse(null);
                if (dbannees != null) {
                    anneeDAO.remove(dbannees);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    public List<Annees> getAllElements() {
        return anneeDAO.getAll();
    }

    public IAnneesDAO getAnneeDAO() {
        return anneeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Annees annees) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(annees == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : annees.getAnnee_valeur(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param errorsreport
     * @param code
     * @param nom
     * @param famille
     * @throws BusinessException
     */
    private void persistAnnees(final String nom) throws BusinessException {
        final Annees dbannees = anneeDAO.getByNKey(nom).orElse(null);
        if (dbannees == null) {
            createAnnees(nom);
        };
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            // Parcourir chaque ligne du fichier
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Annees.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                persistAnnees(nom);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    public void setAnneeDAO(IAnneesDAO anneeDAO) {
        this.anneeDAO = anneeDAO;
    }

}
