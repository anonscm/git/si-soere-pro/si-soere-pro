/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;

/**
 *
 * @author adiankha
 * @param <T>
 */
public interface IMesureSemisPlantationDAO <T>  extends IDAO<MesureSemisPlantation>{

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesureSemisPlantation> getByKeys(String keymesure);
}
