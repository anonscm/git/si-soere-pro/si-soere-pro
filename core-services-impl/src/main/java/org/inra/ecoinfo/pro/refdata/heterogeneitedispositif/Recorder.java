/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.heterogeneitedispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.heterogeinite.Heterogeinite;
import org.inra.ecoinfo.pro.refdata.heterogeinite.IHeterogeiniteDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<HeterogeineteDispositif> {

    private static final String PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO = "PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO";
    private static final String PROPERTY_MSG_HETERODISPO_BAD_NOMHETERO = "PROPERTY_MSG_HETERODISPO_BAD_NOMHETERO";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    IHeterogeneiteDispositifDAO heterodispoDAO;
    IDispositifDAO dispositifDAO;
    IHeterogeiniteDAO heteroDAO;
    private String[] listeDispositifPossibles;
    private String[] listeHeterogeneitePossibles;
    private Map<String, String[]> listeLieuPossibles;
    Properties ComHDEn;

    private void createHeteroDispositif(final HeterogeineteDispositif heterodispo) throws BusinessException {
        try {
            heterodispoDAO.saveOrUpdate(heterodispo);
            heterodispoDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create HeterogeineteDispositif");
        }
    }

    private void updateHeteroDispositif(String comment, HeterogeineteDispositif dbheterodispo, Dispositif dispositif,
            Heterogeinite hetero) throws BusinessException {
        try {
            dbheterodispo.setDispositif(dispositif);
            dbheterodispo.setHeterogeinite(hetero);
            dbheterodispo.setHd_comment(comment);
            heterodispoDAO.saveOrUpdate(dbheterodispo);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update HeterogeineteDispositif");
        }
    }

    private void createOrUpdateHistoDispo(String comment, HeterogeineteDispositif dbheterodispo, Dispositif dispositif,
            Heterogeinite hetero) throws BusinessException {
        if (dbheterodispo == null) {
            final HeterogeineteDispositif heterodispo = new HeterogeineteDispositif(dispositif, hetero, comment);
            heterodispo.setHd_comment(comment);
            heterodispo.setDispositif(dispositif);
            heterodispo.setHeterogeinite(hetero);
            createHeteroDispositif(heterodispo);
        } else {
            updateHeteroDispositif(comment, dbheterodispo, dispositif, hetero);
        }
    }

    private void persistHistoDispo(Dispositif dispositif, Heterogeinite hetero, String comment) throws BusinessException, BusinessException {
        final HeterogeineteDispositif dbheterodispo = heterodispoDAO.getByNKey(dispositif, hetero).orElse(null);
        createOrUpdateHistoDispo(comment, dbheterodispo, dispositif, hetero);
    }

    private void disposifPossibles() {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCode();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void heteroPossibles() {
        List<Heterogeinite> groupehetero = heteroDAO.getAll(Heterogeinite.class);
        String[] listeHetero = new String[groupehetero.size() + 1];
        listeHetero[0] = "";
        int index = 1;
        for (Heterogeinite hetero : groupehetero) {
            listeHetero[index++] = hetero.getNom();
        }
        this.listeHeterogeneitePossibles = listeHetero;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                Dispositif dispositif = null;
                Heterogeinite hetero = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values, HeterogeineteDispositif.NAME_ENTITY_JPA);
                String code = tokenizerValues.nextToken();
                String lieu = tokenizerValues.nextToken();
                String heter = tokenizerValues.nextToken();
                dispositif = dispositifDAO.getByNKey(code, lieu).orElse(null);
                hetero = heteroDAO.getByNKey(heter).orElse(null);
                HeterogeineteDispositif heteroge = heterodispoDAO.getByNKey(dispositif, hetero)
                        .orElseThrow(() -> new BusinessException("can't fine HeterogeineteDispositif"));
                heterodispoDAO.remove(heteroge);
                values = csvp.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<HeterogeineteDispositif> getAllElements() throws BusinessException {
        return heterodispoDAO.getAll();

    }

    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            long line = 0;
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, HeterogeineteDispositif.NAME_ENTITY_JPA);
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String dispo = tokenizerValues.nextToken();
                final String lieu = tokenizerValues.nextToken();
                int indexhe = tokenizerValues.currentTokenIndex();
                final String hetero = tokenizerValues.nextToken();
                final String comment = tokenizerValues.nextToken();
                Dispositif dbdispositif = dispositifDAO.getByNKey(dispo, lieu).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HETERODISPO_BAD_CODEDISPO), line, indexdispo, dispo));
                }
                Heterogeinite dbhetero = heteroDAO.getByNKey(hetero).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HETERODISPO_BAD_NOMHETERO), line, indexhe, hetero));
                }

                if (!errorsReport.hasErrors()) {
                    persistHistoDispo(dbdispositif, dbhetero, comment);
                }
                values = csvp.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(HeterogeineteDispositif heterogeneitedispositif) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        String valeurProduit = heterogeneitedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : heterogeneitedispositif.getDispositif() != null ? heterogeneitedispositif.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifPossibles, null, true, false, true);
        String valeurProcede = heterogeneitedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : heterogeneitedispositif.getDispositif() != null ? heterogeneitedispositif.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(heterogeneitedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : heterogeneitedispositif.getHeterogeinite() != null ? heterogeneitedispositif.getHeterogeinite().getNom() : "",
                        listeHeterogeneitePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(heterogeneitedispositif == null || heterogeneitedispositif.getHd_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : heterogeneitedispositif.getHd_comment(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(heterogeneitedispositif == null || heterogeneitedispositif.getHd_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ComHDEn.getProperty(heterogeneitedispositif.getHd_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<HeterogeineteDispositif> initModelGridMetadata() {

        ComHDEn = localizationManager.newProperties(HeterogeineteDispositif.NAME_ENTITY_JPA, HeterogeineteDispositif.COLUMN_COMMENT_JPA, Locale.ENGLISH);
        disposifPossibles();
        heteroPossibles();

        return super.initModelGridMetadata();

    }

    /**
     *
     * @return
     */
    public IHeterogeneiteDispositifDAO getHeterodispoDAO() {
        return heterodispoDAO;
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    /**
     *
     * @return
     */
    public IHeterogeiniteDAO getHeteroDAO() {
        return heteroDAO;
    }

    /**
     *
     * @param heterodispoDAO
     */
    public void setHeterodispoDAO(IHeterogeneiteDispositifDAO heterodispoDAO) {
        this.heterodispoDAO = heterodispoDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param heteroDAO
     */
    public void setHeteroDAO(IHeterogeiniteDAO heteroDAO) {
        this.heteroDAO = heteroDAO;
    }

}
