/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.elementaire.impl;

import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteBrutDataTypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieExtractor;

/**
 *
 * @author adiankha
 */
public class PlanteBrutExtractor extends AbstractPhysicoChimieExtractor<MesurePlanteElementaire> {

    private static final String CST_EXTRACT = "EXTRACT_";

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     *
     */
    public static String MAP_INDEX_PLANTEBRUT = "planteBrut";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_PLANTEBRUT_CODE = "extractionResultPlante";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return CST_RESULT_EXTRACTION_PLANTEBRUT_CODE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IPhysicoChimiePlanteBrutDataTypeManager.CODE_DATATYPE_PLANTE_BRUT;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_PLANTEBRUT;
    }

}
