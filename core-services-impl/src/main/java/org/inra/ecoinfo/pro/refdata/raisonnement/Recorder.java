/**
 *
 */
package org.inra.ecoinfo.pro.refdata.raisonnement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.IListeRaisonnementDAO;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Raisonnement> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    IListeRaisonnementDAO listeRaisonnementDAO;
    IRaisonnementDAO raisonnementDAO;

    /**
     * @param listeRaisonnement
     * @param libelleRaisonnement
     * @param dbRaisonnement
     * @param raisonnement
     * @throws BusinessException
     */
    private void createOrUpdate(ListeRaisonnement listeRaisonnement, String libelleRaisonnement, Raisonnement dbRaisonnement, Raisonnement raisonnement) throws BusinessException {
        try {
            if (dbRaisonnement == null) {
                raisonnementDAO.saveOrUpdate(raisonnement);
            } else {
                dbRaisonnement.setLibelle(libelleRaisonnement);
                dbRaisonnement.setListeRaisonnement(listeRaisonnement);
                
                raisonnementDAO.saveOrUpdate(dbRaisonnement);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create raisonnement");
        }
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getLibelleListeRaisonnementPossibles() throws BusinessException {
        List<ListeRaisonnement> lstListeRaisonnements = listeRaisonnementDAO.getAll(ListeRaisonnement.class);
        String[] listeRaisonnementPossibles = new String[lstListeRaisonnements.size()];
        int index = 0;
        for (ListeRaisonnement listeRaisonnement : lstListeRaisonnements) {
            listeRaisonnementPossibles[index++] = listeRaisonnement.getLibelle();
        }
        return listeRaisonnementPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelleListeRaisonnement = tokenizerValues.nextToken();
                String libelleRaisonnement = tokenizerValues.nextToken();
                
                ListeRaisonnement listeRaisonnement = listeRaisonnementDAO.getByNKey(libelleListeRaisonnement).orElse(null);
                if (listeRaisonnement != null) {
                    raisonnementDAO.remove(raisonnementDAO.getByNKey(libelleRaisonnement, listeRaisonnement)
                            .orElseThrow(() -> new BusinessException("can't get raisonneemnt")));
                }                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Raisonnement> getAllElements() throws BusinessException {
        return raisonnementDAO.getAll(Raisonnement.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Raisonnement raisonnement) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesLibelle = localizationManager.newProperties(Raisonnement.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_RAIS, Locale.ENGLISH);
        
        String localizedChampLibelle = "";
        
        if (raisonnement != null) {
            localizedChampLibelle = propertiesLibelle.containsKey(raisonnement.getLibelle()) ? propertiesLibelle.getProperty(raisonnement.getLibelle()) : raisonnement.getLibelle();
        }
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(raisonnement == null ? Constantes.STRING_EMPTY : raisonnement.getListeRaisonnement().getLibelle(), getLibelleListeRaisonnementPossibles(), null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(raisonnement == null ? Constantes.STRING_EMPTY : raisonnement.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(raisonnement == null ? Constantes.STRING_EMPTY : localizedChampLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Raisonnement.TABLE_NAME);
                
                String libelleListeRaisonnement = tokenizerValues.nextToken();
                int indexligne = tokenizerValues.currentTokenIndex();
                String libelleRaisonnement = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                ListeRaisonnement listeRaisonnement = listeRaisonnementDAO.getByNKey(libelleListeRaisonnement).orElse(null);
                
                if (listeRaisonnement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LISTERAISONNEMENT_NONDEFINI"), line + 1, indexligne, libelleListeRaisonnement));
                }
                
                if (libelleRaisonnement == null || libelleRaisonnement.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(1).getName()));
                }
                
                Raisonnement raisonnement = new Raisonnement(libelleRaisonnement, listeRaisonnement);
                Raisonnement dbRaisonnement = raisonnementDAO.getByNKey(libelleRaisonnement, listeRaisonnement).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    createOrUpdate(listeRaisonnement, libelleRaisonnement, dbRaisonnement, raisonnement);
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param listeRaisonnementDAO
     */
    public void setListeRaisonnementDAO(IListeRaisonnementDAO listeRaisonnementDAO) {
        this.listeRaisonnementDAO = listeRaisonnementDAO;
    }

    /**
     * @param raisonnementDAO
     */
    public void setRaisonnementDAO(IRaisonnementDAO raisonnementDAO) {
        this.raisonnementDAO = raisonnementDAO;
    }
    
}
