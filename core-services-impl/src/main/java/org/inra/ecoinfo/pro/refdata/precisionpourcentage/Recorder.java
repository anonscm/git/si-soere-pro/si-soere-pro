/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpourcentage;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<PrecisionPourcentage> {
    
    IPrecisionPourcentageDAO precisionPourcenDAO;
    Properties libelleEn;
    
    /**
     *
     * @param precisionpourcent
     * @throws BusinessException
     */
    public void createPrecisionPourcentage(PrecisionPourcentage precisionpourcent) throws BusinessException {
        try {
            precisionPourcenDAO.saveOrUpdate(precisionpourcent);
            precisionPourcenDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create placette");
        }
    }
    
    private void updatePrecisionPourcentage(String code, final String libelle, final PrecisionPourcentage dbprecisionpourcent) throws BusinessException {
        try {
            dbprecisionpourcent.setLibelle(libelle);
            dbprecisionpourcent.setCode(code);
            precisionPourcenDAO.saveOrUpdate(dbprecisionpourcent);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create placette");
        }
    }
    
    private void createOrUpdatePrecisionPourcentage(final String code, final String libelle, final PrecisionPourcentage dbprecisionpourcent) throws BusinessException {
        if (dbprecisionpourcent == null) {
            final PrecisionPourcentage precisionpourcent = new PrecisionPourcentage(libelle);
            precisionpourcent.setCode(code);
            precisionpourcent.setLibelle(libelle);
            createPrecisionPourcentage(precisionpourcent);
        } else {
            updatePrecisionPourcentage(code, libelle, dbprecisionpourcent);
        }
    }
    
    private void persistPrecisionPourcentage(final String code, final String libelle) throws BusinessException, BusinessException {
        final PrecisionPourcentage dbpourcent = precisionPourcenDAO.getBYNKey(libelle).orElse(null);
        createOrUpdatePrecisionPourcentage(code, libelle, dbpourcent);
    }
    
    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, PrecisionPourcentage.NAME_ENTITY_JPA);
                String nom = tokenizerValues.nextToken();
                PrecisionPourcentage dbporte = precisionPourcenDAO.getBYNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get PrecisionPourcentage"));
                precisionPourcenDAO.remove(dbporte);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, PrecisionPourcentage.NAME_ENTITY_JPA);
                final String libelle = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(libelle);
                persistPrecisionPourcentage(code, libelle);
                values = csvp.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List getAllElements() throws BusinessException {
        return precisionPourcenDAO.getAll();
    }
    
    /**
     *
     * @param precisionPourcenDAO
     */
    public void setPrecisionPourcenDAO(IPrecisionPourcentageDAO precisionPourcenDAO) {
        this.precisionPourcenDAO = precisionPourcenDAO;
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PrecisionPourcentage precisionpourcent) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(precisionpourcent == null || precisionpourcent.getLibelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : precisionpourcent.getLibelle(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(precisionpourcent == null || precisionpourcent.getLibelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(precisionpourcent.getLibelle()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<PrecisionPourcentage> initModelGridMetadata() {
        libelleEn = localizationManager.newProperties(PrecisionPourcentage.NAME_ENTITY_JPA, PrecisionPourcentage.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
}
