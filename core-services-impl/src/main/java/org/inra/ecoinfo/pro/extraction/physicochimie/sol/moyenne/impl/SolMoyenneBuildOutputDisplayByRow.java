/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.sol.moyenne.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieOutputDisplayByRow;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class SolMoyenneBuildOutputDisplayByRow extends PhysicoChimieOutputDisplayByRow {

    static final String PATTERN_WORD = "%s";
    static final String PATTERB_CSV_12_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%%s;%%s;%%s;%%s;%%s;%%s";
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA_SOLMOYENNE = "PROPERTY_HEADER_RAW_DATA_SOLMOYENNE";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.sol.messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedSolMoyenneVariables = getVariablesSelected(requestMetadatasMap, IPhysicoChimieSolMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_MOY);
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<MesurePhysicoChimieSolsMoy> mesuresSolMoyenne = getMesures(resultsDatasMap, SolMoyenneExtractor.MAP_INDEX_SOLMOYENNE);
        final Set<String> dispositifsNames = buildListOfDispositifsForDatatype(selectedDispositifs, getDatatype());
        final Map<String, File> filesMap = this.buildOutputsFiles(dispositifsNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().stream().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>>> mesuresSolMoyennesMap = new TreeMap();

        try {
            this.buildmap(mesuresSolMoyenne, mesuresSolMoyennesMap);
        } catch (DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedDispositifs, selectedSolMoyenneVariables, selectedIntervalDate,
                outputPrintStreamMap, mesuresSolMoyennesMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void buildmap(
            final List<MesurePhysicoChimieSolsMoy> mesuresSolMoyennes,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>>> mesuresSolMoyenneMap) {
        java.util.Iterator<MesurePhysicoChimieSolsMoy> itMesure = mesuresSolMoyennes
                .iterator();
        while (itMesure.hasNext()) {
            MesurePhysicoChimieSolsMoy mesureSolMoyenne = itMesure
                    .next();
            DescriptionTraitement traitement = mesureSolMoyenne.getDescriptionTraitement();
            Long siteId = traitement.getDispositif().getId();
            if (mesuresSolMoyenneMap.get(siteId) == null) {
                mesuresSolMoyenneMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresSolMoyenneMap.get(siteId).get(traitement) == null) {
                mesuresSolMoyenneMap.get(siteId).put(traitement,
                        new TreeMap<>());
            }
            mesuresSolMoyenneMap.get(siteId).get(traitement).put(mesureSolMoyenne.getDatePrelevement(), mesureSolMoyenne);
            itMesure.remove();
        }
    }

    private void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedSolMoyenneVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>>> mesuresSolMoyennesMap) {
        try {
            BuildDataLine(selectedDispositifs, selectedSolMoyenneVariables, outputPrintStreamMap, mesuresSolMoyennesMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void BuildDataLine(final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedSolMoyenneVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>>> mesuresSolMoyennesMap,
            final IntervalDate selectedIntervalDate){
        PrintStream out;
        String currentDispositif;
        String currentLieu;
        String codeTraitement;
        String nomCouche;
        double sup;
        double inf;
        int currentRep;
        String currentlabo;
        for (final Dispositif dispositif : selectedDispositifs) {
            out = outputPrintStreamMap.get(getDispositifDatatype(dispositif, getDatatype()));
            final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>> mesurePlanteMoyennesMapByDisp = mesuresSolMoyennesMap
                    .get(dispositif.getId());
            if (mesurePlanteMoyennesMapByDisp == null) {
                continue;
            }
            Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>>> itEchan = mesurePlanteMoyennesMapByDisp
                    .entrySet().iterator();
            while (itEchan.hasNext()) {
                java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesurePhysicoChimieSolsMoy>> echanEntry = itEchan
                        .next();
                DescriptionTraitement traitement = echanEntry.getKey();
                SortedMap<LocalDate, MesurePhysicoChimieSolsMoy> mesureSolMoyennesMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                for (Entry<LocalDate, MesurePhysicoChimieSolsMoy> entrySet : mesureSolMoyennesMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesurePhysicoChimieSolsMoy mesurePlanteMoyenne = entrySet.getValue();
                    currentDispositif = traitement.getDispositif().getCode();
                    currentLieu = traitement.getDispositif().getLieu().getNom() != null ? traitement.getDispositif().getLieu().getNom()
                            : org.apache.commons.lang.StringUtils.EMPTY;
                    codeTraitement = traitement.getCode();
                    nomCouche = mesurePlanteMoyenne.getNom_couche();
                    sup = mesurePlanteMoyenne.getLimit_sup();
                    inf = mesurePlanteMoyenne.getLimit_inf();
                    currentRep = mesurePlanteMoyenne.getNbre_repetition();
                    currentlabo = String.format(
                            SolMoyenneBuildOutputDisplayByRow.PATTERN_WORD,
                            mesurePlanteMoyenne.getNom_laboratoire());

                    String genericPattern = LineDataFixe(date, currentDispositif, currentLieu, codeTraitement, nomCouche, sup, inf, currentRep, currentlabo);
                    LineDataVariable(mesurePlanteMoyenne, genericPattern, out, selectedSolMoyenneVariables);
                }
                itEchan.remove();
            }
        }
    }

    private void LineDataVariable(MesurePhysicoChimieSolsMoy mesureSolMoyenne, String genericPattern, PrintStream out,
            final List<VariablesPRO> selectedSolMoyenneVariables) {
        selectedSolMoyenneVariables.forEach((variablesPRO) -> {
            for (Iterator<ValeurPhysicoChimieSolsMoy> ValeurIterator = mesureSolMoyenne.getValeurPhysicoChimieSolMoy().iterator(); ValeurIterator.hasNext();) {
                ValeurPhysicoChimieSolsMoy valeur = ValeurIterator.next();
                if (((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getId().equals(variablesPRO.getId())) {
                    String line = String.format(genericPattern,
                            valeur.getValeur(),
                            valeur.getEcarttype(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());
                    out.println(line);
                    ValeurIterator.remove();
                }
            }
        });
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentLieu, String codeTraitement, String couche,
            double sup, double inf,
            int currentRep,
            String currentlabo){
        String genericPattern = String.format(PATTERB_CSV_12_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentLieu, codeTraitement, couche,
                sup, inf,
                currentRep,
                currentlabo);
        return genericPattern;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                SolMoyenneBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        SolMoyenneBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        SolMoyenneBuildOutputDisplayByRow.HEADER_RAW_DATA_SOLMOYENNE));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (CollectionUtils.isEmpty(((DefaultParameter) parameters).getResults()
                .get(SolMoyenneExtractor.CST_RESULT_EXTRACTION_SOLMOYENNE_CODE)
                .getOrDefault(SolMoyenneExtractor.MAP_INDEX_SOLMOYENNE,
                        ((DefaultParameter) parameters).getResults()
                .get(SolMoyenneExtractor.CST_RESULT_EXTRACTION_SOLMOYENNE_CODE)
                .get(SolMoyenneExtractor.MAP_INDEX_0)))) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, SolMoyenneExtractor.CST_RESULT_EXTRACTION_SOLMOYENNE_CODE));
        return null;
    }

    private String getDatatype() {
        return "sols_physico-chimie_moyennes";
    }
}
