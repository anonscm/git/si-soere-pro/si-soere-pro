/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport_;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationApportDAO extends JPAVersionFileDAO implements ILocalPublicationDAO{

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurApport> deleteValeurs = builder.createCriteriaDelete(ValeurApport.class);
        Root<ValeurApport> valeur = deleteValeurs.from(ValeurApport.class);
        Subquery<MesureApport> subquery = deleteValeurs.subquery(MesureApport.class);
        Root<MesureApport> m = subquery.from(MesureApport.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureApport_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurApport_.mesureinterventionapport).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureApport> deleteSequence = builder.createCriteriaDelete(MesureApport.class);
        Root<MesureApport> sequence = deleteSequence.from(MesureApport.class);
        deleteSequence.where(builder.equal(sequence.get(MesureApport_.versionfile), version));
        delete(deleteSequence);
    }

    
}
