/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.impl;

import com.Ostermiller.util.CSVParser;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class TestValuesIncubationSolMoy extends GenericTestValues {

    /**
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException
     */
    @Override
    public void testValues(long startline, CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor, String datatypeName) throws BusinessException {
        super.testValues(startline, parser, versionFile, sessionProperties, encoding, badsFormatsReport, datasetDescriptor, datatypeName); //To change body of generated methods, choose Tools | Templates.
    }

}
