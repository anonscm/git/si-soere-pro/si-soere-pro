/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre.impl;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class FluxChambreOutputBuildersResolver  extends AbstractOutputBuilder implements IOutputsBuildersResolver, IOutputBuilder{
    
    private static final int    KO             = 1_024;

    private static final int    MILLIARD       = 1_000_000_000;

  
    static final String         EXTRACTION     = "extraction";

  
    static final String         FILE_SEPARATOR = System.getProperty("file.separator");

   
    static final String         SEPARATOR_TEXT = "_";

   
    static final String         EXTENSION_ZIP  = ".zip";

   
    IOutputBuilder fluxChambreOutputBuilder;
    IOutputBuilder              requestReminderOutputBuilder;
    ICoreConfiguration          configuration;
    
    /**
     *
     * @param condition
     * @param outputBuilder
     * @param outputsBuilders
     */
    protected void addOutputBuilderByCondition(final Boolean condition,
            final IOutputBuilder outputBuilder, final List<IOutputBuilder> outputsBuilders) {
        if (condition) {
            outputsBuilders.add(outputBuilder);
        }
    }

    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        final List<IOutputBuilder> outputsBuilders = new LinkedList();
        final Map<String, Map<String, List>> results = (Map<String, Map<String, List>>) metadatasMap
                .get(FluxChambreExtractor.CST_RESULTS);
         this.addOutputBuilderByCondition(
                results.get(FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE) != null,
                this.fluxChambreOutputBuilder, outputsBuilders);
        outputsBuilders.add(this.requestReminderOutputBuilder);
        return outputsBuilders;
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream=super.buildOutput(parameters);
        try {
            parameters.getParameters().put(FluxChambreExtractor.CST_RESULTS, parameters.getResults());
            final List<IOutputBuilder> buildouputBuilders = this
                    .resolveOutputsBuilders(parameters.getParameters());
            NoExtractionResultException noDataToExtract;
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                noDataToExtract = null;
                for (final IOutputBuilder iOutputBuilder : buildouputBuilders) {
                    try {
                        iOutputBuilder.buildOutput(parameters);
                    } catch (final NoExtractionResultException e) {
                        noDataToExtract = e;
                    }
                }
                for (final Map<String, File> filesMap : ((DefaultParameter) parameters)
                        .getFilesMaps()) {
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
        return rObuildZipOutputStream;
    }

    /**
     *
     * @param fluxChambreOutputBuilder
     */
    public void setFluxChambreOutputBuilder(IOutputBuilder fluxChambreOutputBuilder) {
        this.fluxChambreOutputBuilder = fluxChambreOutputBuilder;
    }

    /**
     *
     * @param requestReminderOutputBuilder
     */
    public void setRequestReminderOutputBuilder(IOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
