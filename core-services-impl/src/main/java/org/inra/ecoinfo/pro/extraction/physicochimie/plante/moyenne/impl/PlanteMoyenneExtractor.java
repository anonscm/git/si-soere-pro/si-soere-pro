/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.moyenne.impl;

import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteMoyenneDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieExtractor;

/**
 *
 * @author adiankha
 */
public class PlanteMoyenneExtractor extends AbstractPhysicoChimieExtractor<MesurePlanteElementaire> {

    private static final String CST_EXTRACT = "EXTRACT_";

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     *
     */
    public static String MAP_INDEX_PLANTEMOYENNE = "planteMoyenne";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_PLANTEMOYENNE_CODE = "extractionResultPlanteMoyenne";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return CST_RESULT_EXTRACTION_PLANTEMOYENNE_CODE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IPhysicoChimiePlanteMoyenneDatatypeManager.CODE_DATATYPE_PLANTE_MOYENNE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_PLANTEMOYENNE;
    }

}
