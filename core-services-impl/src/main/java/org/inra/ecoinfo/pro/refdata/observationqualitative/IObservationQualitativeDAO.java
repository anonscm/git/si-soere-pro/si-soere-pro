/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.observationqualitative;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.typeobservationqualitative.TypeObservationQualitative;

/**
 *
 * @author vjkoyao
 */
public interface IObservationQualitativeDAO extends IDAO<ObservationQalitative> {

    /**
     *
     * @return
     */
    List<ObservationQalitative> getAll();

    /**
     *
     * @param observation
     * @param typeobservation
     * @return
     */
    Optional<ObservationQalitative> getByNKey(String observation, TypeObservationQualitative typeobservation);

}
