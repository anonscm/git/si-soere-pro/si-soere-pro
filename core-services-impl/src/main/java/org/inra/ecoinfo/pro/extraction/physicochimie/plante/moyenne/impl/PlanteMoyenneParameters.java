/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.moyenne.impl;

import java.util.Map;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieParameter;

/**
 *
 * @author adiankha
 */
public class PlanteMoyenneParameters extends AbstractPhysicoChimieParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_PLANTEMOYENNE = "physico_chimie_des_plantes_moyennes";

    /**
     *
     */
    public static final String PLANTEMOYENNE = "PlanteMoyenne";

    /**
     *
     * @param metadatasMap
     */
    public PlanteMoyenneParameters(Map<String, Object> metadatasMap) {
        super(metadatasMap);
    }

    @Override
    public String getExtractionTypeCode() {
        return PlanteMoyenneParameters.CODE_EXTRACTIONTYPE_PLANTEMOYENNE;
    }
}
