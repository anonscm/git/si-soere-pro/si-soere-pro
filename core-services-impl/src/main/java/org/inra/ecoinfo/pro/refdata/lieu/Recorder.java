/**
 *
 */
package org.inra.ecoinfo.pro.refdata.lieu;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.commune.Commune;
import org.inra.ecoinfo.pro.refdata.commune.ICommuneDAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.IStationExperimentaleDAO;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.StationExperimentale;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;
import org.inra.ecoinfo.pro.refdata.typelieu.ITypeLieuDAO;
import org.inra.ecoinfo.pro.refdata.typelieu.TypeLieu;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<Lieu> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ITypeLieuDAO typeLieuDAO;
    IStationExperimentaleDAO stationExperimentaleDAO;
    ICommuneDAO communeDAO;
    ILieuDAO lieuDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nomLieu = tokenizerValues.nextToken();
                int indexType = tokenizerValues.currentTokenIndex();
                String libelleTypeLieu = tokenizerValues.nextToken();
                int indexCommune = tokenizerValues.currentTokenIndex();
                String nomCommuneCodePostal = tokenizerValues.nextToken();
                String numeroCadastre = tokenizerValues.nextToken();
                int indexStationExp = tokenizerValues.currentTokenIndex();
                String nomStationExp = tokenizerValues.nextToken();
                int indexSystProj = tokenizerValues.currentTokenIndex();
                String systemeProjection = tokenizerValues.nextToken();
                String latitude = tokenizerValues.nextToken();
                String longitude = tokenizerValues.nextToken();

                TypeLieu typeLieu = verifieTypeLieu(libelleTypeLieu, line + 1, indexType + 1, errorsReport);
                StationExperimentale stationExperimentale = verifieStationExperimentale(nomStationExp, line + 1, indexStationExp, errorsReport);
                Commune commune = verifieCommune(nomCommuneCodePostal, line + 1, indexCommune + 1, errorsReport);

                Lieu dbLieu = lieuDAO.getByNKey(nomLieu).orElse(null);

                // La géolocalisation n'est pas obligatoire
                Geolocalisation geolocalisation = null;
                if (!Strings.isNullOrEmpty(nomLieu)) {
                    //on traite la géolocalisation si pas d'erreur sur dbLieu
                    Geolocalisation ancienGeolocZone = dbLieu != null ? dbLieu.getGeolocalisation() : null;
                    geolocalisation = traiteGeolocalisation(systemeProjection, latitude, longitude, geolocalisation, dbLieu, ancienGeolocZone, errorsReport, line + 1, indexSystProj + 1);
                }

                if (!errorsReport.hasErrors()) {
                    Lieu lieu = new Lieu(typeLieu, stationExperimentale, commune, geolocalisation, nomLieu, numeroCadastre);
                    createOrUpdate(typeLieu, stationExperimentale, commune, geolocalisation, nomLieu, numeroCadastre, dbLieu, lieu);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param libelleTypeLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private TypeLieu verifieTypeLieu(String libelleTypeLieu, long line, int index, ErrorsReport errorsReport) {
        TypeLieu typeLieu = typeLieuDAO.getByNKey(libelleTypeLieu).orElse(null);
        if (typeLieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TYPELIEU_NONDEFINI"), line, index, libelleTypeLieu));
        }

        return typeLieu;
    }

    /**
     * @param nomStationExp
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private StationExperimentale verifieStationExperimentale(String nomStationExp, long line, int index, ErrorsReport errorsReport) {
        // La station expérimentale n'est pas obligatoire
        StationExperimentale stationExperimentale = null;
        if (nomStationExp != null && !nomStationExp.trim().isEmpty()) {
            stationExperimentale = stationExperimentaleDAO.getByNKey(nomStationExp).orElse(null);
            if (stationExperimentale == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "STATIONEXP_NONDEFINI"), line, index, nomStationExp));
            }
        }

        return stationExperimentale;
    }

    /**
     * @param nomCommuneCodePostal
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Commune verifieCommune(String nomCommuneCodePostal, long line, int index, ErrorsReport errorsReport) {
        // La commune n'est pas obligatoire
        Commune commune = null;
        if (nomCommuneCodePostal != null && !nomCommuneCodePostal.trim().isEmpty()) {
            String nomCommune = Commune.getNomCommune(nomCommuneCodePostal);
            String codePostal = Commune.getCodePostal(nomCommuneCodePostal);

            // Vérifier si la Commune existe
            commune = communeDAO.getByNKey(nomCommune, codePostal).orElse(null);
            if (commune == null) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "COMMUNE_NONDEFINI"), line, index, nomCommuneCodePostal));
            }
        }

        return commune;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#verifLocalisationExiste(org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation, java.lang.Object, org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport)
     */

    /**
     *
     * @param dbGeolocalisation
     * @param dbLieu
     * @param errorsReport
     * @return
     * @throws BusinessException
     */

    @Override
    public Geolocalisation verifLocalisationExiste(Geolocalisation dbGeolocalisation, Lieu dbLieu, ErrorsReport errorsReport) throws BusinessException {
        Geolocalisation geolocalisation = null;

        // vérifie si cette géolocalisation est déjà associée à un lieu et retrouve auquel
        Lieu lieuGeolocalise = lieuDAO.getByGeolocalisation(dbGeolocalisation).orElse(null);

        if (lieuGeolocalise != null) {
            if (dbLieu != null) {
                // la géolocalisation du lieu est à modifier en base mais la géolocalisation saisie
                // est déjà associée à un autre lieu en base. Dans ce cas erreur, deux lieux différents
                // ne pourront pas être géolocalisés au même endroit
                if (!dbLieu.equals(lieuGeolocalise)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LIEUGEOLOC_EXISTE"), lieuGeolocalise.getNom()));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                } else // la géolocalisation du lieu sera à modifier en base
                {
                    geolocalisation = dbGeolocalisation;
                }
            }
        }

        return geolocalisation;
    }

    /**
     * @param typeLieu
     * @param stationExperimentale
     * @param commune
     * @param geolocalisation
     * @param nomLieu
     * @param numeroCadastre
     * @param dbLieu
     * @param lieu
     * @throws PersistenceException
     */
    private void createOrUpdate(TypeLieu typeLieu, StationExperimentale stationExperimentale, Commune commune, Geolocalisation geolocalisation, String nomLieu, String numeroCadastre, Lieu dbLieu, Lieu lieu) throws BusinessException {
        try {
            if (dbLieu == null) {
                lieuDAO.saveOrUpdate(lieu);
            } else {
                // si on met à jour le lieu en lui enlevant sa geolocalisation, alors supprimer cette ancienne geolocalisation de la base
                if (geolocalisation == null) {
                    Geolocalisation dbDispGeolocalisation = dbLieu.getGeolocalisation();
                    if (dbDispGeolocalisation != null) {
                        geolocalisationDAO.remove(dbDispGeolocalisation);
                    }
                }

                dbLieu.setTypeLieu(typeLieu);
                dbLieu.setNom(nomLieu);
                dbLieu.setStationExperimentale(stationExperimentale);
                dbLieu.setCommune(commune);
                dbLieu.setNumeroCadastre(numeroCadastre);
                dbLieu.setGeolocalisation(geolocalisation);

                lieuDAO.saveOrUpdate(dbLieu);
            }
        } catch (PersistenceException e) {
            throw new BusinessException("can't save or update lieu");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nomLieu = tokenizerValues.nextToken();

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Geolocalisation geolocalisation = lieu.getGeolocalisation();

                // si on supprime le lieu, on supprime également sa géolocalisation
                if (geolocalisation != null) {
                    geolocalisationDAO.remove(geolocalisation);
                }

                lieuDAO.remove(lieu);
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getTypeLieuPossibles() throws BusinessException {
        List<TypeLieu> lstTypeLieux = typeLieuDAO.getAll(TypeLieu.class);
        String[] typeLieuPossibles = new String[lstTypeLieux.size()];
        int index = 0;
        for (TypeLieu typeLieu : lstTypeLieux) {
            typeLieuPossibles[index++] = typeLieu.getLibelle();
        }
        return typeLieuPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getStationExperimentalePossibles() throws BusinessException {
        List<StationExperimentale> lstStationExperimentales = stationExperimentaleDAO.getAll(StationExperimentale.class);
        String[] stationExperimentalePossibles = new String[lstStationExperimentales.size()];
        int index = 0;
        for (StationExperimentale stationExperimentale : lstStationExperimentales) {
            stationExperimentalePossibles[index++] = stationExperimentale.getNom();
        }
        return stationExperimentalePossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getCommunePossibles() throws BusinessException {
        List<Commune> lstCommunes = communeDAO.getAll(Commune.class);
        String[] communePossibles = new String[lstCommunes.size()];
        int index = 0;
        for (Commune commune : lstCommunes) {
            communePossibles[index++] = commune.getNomCommune_codePostal();
        }
        return communePossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getSystemeProjectionPossibles() throws BusinessException {
        List<SystemeProjection> lstSystemeProjections = systemeProjectionDAO.getAll();
        String[] systemeProjectionPossibles = new String[lstSystemeProjections.size() + 1];
        systemeProjectionPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (SystemeProjection systemeProjection : lstSystemeProjections) {
            systemeProjectionPossibles[index++] = systemeProjection.getNom();
        }
        return systemeProjectionPossibles;
    }


    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Lieu lieu) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        // Nom du lieu
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : lieu.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // Libellé du type de lieu
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : lieu.getTypeLieu().getLibelle(), getTypeLieuPossibles(), null, false, false, true));

        // Nom de la commune (Code postal)
        String nomCommuneCodePost = (lieu == null) ? null
                : lieu.getCommune().getNomCommune_codePostal();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : nomCommuneCodePost, getCommunePossibles(), null, false, false, false));

        // Numéro de cadastre
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : lieu.getNumeroCadastre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Nom de la station experimentale
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : lieu.getStationExperimentale().getNom(), getStationExperimentalePossibles(), null, false, false, false));

        // Géolocalisation : système de projection
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : (lieu.getGeolocalisation() == null ? Constantes.STRING_EMPTY
                        : (lieu.getGeolocalisation().getSystemeProjection() == null ? Constantes.STRING_EMPTY : lieu.getGeolocalisation().getSystemeProjection().getNom())), getSystemeProjectionPossibles(), null,
                        false, false, false));
        // Géolocalisation : latitude
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : (lieu.getGeolocalisation() == null ? Constantes.STRING_EMPTY : lieu.getGeolocalisation().getLatitude()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
        // Géolocalisation : longitude
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lieu == null ? Constantes.STRING_EMPTY : (lieu.getGeolocalisation() == null ? Constantes.STRING_EMPTY : lieu.getGeolocalisation().getLongitude()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Lieu> getAllElements() throws BusinessException {
        return lieuDAO.getAll();
    }

    /**
     * @param typeLieuDAO the typeLieuDAO to set
     */
    public void setTypeLieuDAO(ITypeLieuDAO typeLieuDAO) {
        this.typeLieuDAO = typeLieuDAO;
    }

    /**
     * @param stationExperimentaleDAO the stationExperimentaleDAO to set
     */
    public void setStationExperimentaleDAO(IStationExperimentaleDAO stationExperimentaleDAO) {
        this.stationExperimentaleDAO = stationExperimentaleDAO;
    }

    /**
     * @param communeDAO the communeDAO to set
     */
    public void setCommuneDAO(ICommuneDAO communeDAO) {
        this.communeDAO = communeDAO;
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

}
