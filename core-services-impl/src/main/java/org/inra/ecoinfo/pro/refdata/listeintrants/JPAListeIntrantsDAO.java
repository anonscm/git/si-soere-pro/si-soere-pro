/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeintrants;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAListeIntrantsDAO extends AbstractJPADAO<ListeIntrants> implements IListeIntrantsDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ListeIntrants> getAll() {
        return getAll(ListeIntrants.class);
    }

    /**
     *
     * @param commercial
     * @param element
     * @return
     */
    @Override
    public Optional<ListeIntrants> getByNKey(String commercial, String element) {
        CriteriaQuery<ListeIntrants> query = builder.createQuery(ListeIntrants.class);
        Root<ListeIntrants> listeIntrants = query.from(ListeIntrants.class);
        query
                .select(listeIntrants)
                .where(
                        builder.equal(listeIntrants.get(ListeIntrants_.knomcommercial), commercial),
                        builder.equal(listeIntrants.get(ListeIntrants_.knomelement), element)
                );
        return getOptional(query);
    }
}
