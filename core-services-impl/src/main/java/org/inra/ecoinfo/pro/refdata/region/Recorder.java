/**
 *
 */
package org.inra.ecoinfo.pro.refdata.region;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.pays.IPaysDAO;
import org.inra.ecoinfo.pro.refdata.pays.Pays;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

public class Recorder extends AbstractCSVMetadataRecorder<Region> {

    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IPaysDAO paysDAO;
    IRegionDAO regionDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                int indexPays = tokenizerValues.currentTokenIndex();
                String nomPays = tokenizerValues.nextToken();
                String nomRegion = tokenizerValues.nextToken();

                String code = Utils.createCodeFromString(nomRegion);

                Pays pays = verifiePays(nomPays, line + 1, indexPays + 1, errorsReport);

                Region dbRegion = regionDAO.getByNKey(code).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbRegion == null) {
                        Region region = new Region(nomRegion, code, pays);
                        regionDAO.saveOrUpdate(region);
                    } else {
                        dbRegion.setPays(pays);
                        dbRegion.setNom(nomRegion);
                        dbRegion.setCode(code);
                        regionDAO.saveOrUpdate(dbRegion);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (NoResultException e2) {
            throw new BusinessException(e2.getMessage(), e2);
        } catch (NonUniqueResultException e3) {
            throw new BusinessException(e3.getMessage(), e3);
        }
    }

    /**
     * @param nomPays
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Pays verifiePays(String nomPays, long line, int index, ErrorsReport errorsReport) {
        Pays pays = paysDAO.getByNKey(nomPays).orElse(null);
        if (pays == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "PAYS_NONDEFINI"), line, index, nomPays));
        }

        return pays;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                tokenizerValues.nextToken();
                String nomRegion = tokenizerValues.nextToken();

                Region region = regionDAO.getByNom(nomRegion)
                        .orElseThrow(() -> new BusinessException("can't get region"));

                regionDAO.remove(region);

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Region region) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        // Nom du pays
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(region == null ? Constantes.STRING_EMPTY
                        : (region.getPays() == null ? Constantes.STRING_EMPTY : region.getPays().getNom()), getPaysPossibles(), null, true, false, true));
        // Nom de la région
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(region == null ? Constantes.STRING_EMPTY : region.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getPaysPossibles() throws BusinessException {
        List<Pays> lstPays = paysDAO.getAll(Pays.class);
        String[] paysPossibles = new String[lstPays.size()];
        int index = 0;
        for (Pays pays : lstPays) {
            paysPossibles[index++] = pays.getNom();
        }

        return paysPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Region> getAllElements() throws BusinessException {
        return regionDAO.getAll();
    }

    /**
     * @param paysDAO the paysDAO to set
     */
    public void setPaysDAO(IPaysDAO paysDAO) {
        this.paysDAO = paysDAO;
    }

    /**
     * @param regionDAO the regionDAO to set
     */
    public void setRegionDAO(IRegionDAO regionDAO) {
        this.regionDAO = regionDAO;
    }

}
