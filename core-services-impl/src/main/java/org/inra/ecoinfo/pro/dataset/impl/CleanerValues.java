/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import java.util.Arrays;

/**
 *
 * @author adiankha
 */
public class CleanerValues {
     int      valueIndex = 0;
    String[] values;

    /**
     *
     * @param val
     */
    public CleanerValues(final String[] val) {
        if (val == null) {
            this.values = new String[0];
        } else {
            this.values = Arrays.copyOf(val, val.length);
        }

    }

    /**
     *
     * @param values
     */
    public CleanerValues(float[] values) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public String currentToken() {
        return this.values[this.valueIndex].toLowerCase().trim();
    }

    /**
     *
     * @return
     */
    public int currentTokenIndex() {
        return this.valueIndex;
    }

    /**
     *
     * @return
     */
    public String nextToken() {
        final String value = this.values[this.valueIndex++];
        return value == null ? null : value.toLowerCase().trim();
    }
    
}
