/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy_;
import org.inra.ecoinfo.pro.synthesis.physicochimiepromoy.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.physicochimiepromoy.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class ProDonneesMoyenneSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurPhysicoChimiePROMoy, MesurePhysicoChimiePROMoy> {

    @Override
    Class<ValeurPhysicoChimiePROMoy> getValueClass() {
        return ValeurPhysicoChimiePROMoy.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurPhysicoChimiePROMoy, MesurePhysicoChimiePROMoy> getMesureAttribute() {
        return ValeurPhysicoChimiePROMoy_.mesurePhysicoChimiePROMoy;
    }
}
