/**
 *
 */
package org.inra.ecoinfo.pro.refdata.lieu;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;

/**
 * @author sophie
 *
 */
public class JPALieuDAO extends AbstractJPADAO<Lieu> implements ILieuDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<Lieu> getAll() {
        return getAllBy(Lieu.class, Lieu::getNom);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO#getByGeolocalisation(org.inra .ecoinfo.pro.refdata.geolocalisation.Geolocalisation)
     */

    /**
     *
     * @param geolocalisation
     * @return
     */

    @Override
    public Optional<Lieu> getByGeolocalisation(Geolocalisation geolocalisation) {
        CriteriaQuery<Lieu> query = builder.createQuery(Lieu.class);
        Root<Lieu> lieu = query.from(Lieu.class);
        query
                .select(lieu)
                .where(
                        builder.equal(lieu.get(Lieu_.geolocalisation), geolocalisation)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO#getByNKey(java.lang.String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @Override
    public Optional<Lieu> getByNKey(String nom) {
        CriteriaQuery<Lieu> query = builder.createQuery(Lieu.class);
        Root<Lieu> lieu = query.from(Lieu.class);
        query
                .select(lieu)
                .where(
                        builder.equal(lieu.get(Lieu_.nom), nom)
                );
        return getOptional(query);
    }

}
