package org.inra.ecoinfo.pro.refdata.statut;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IStatutDAO extends IDAO<Statut> {

    /**
     *
     * @return
     */
    List<Statut> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Statut> getByNKey(String nom);

}
