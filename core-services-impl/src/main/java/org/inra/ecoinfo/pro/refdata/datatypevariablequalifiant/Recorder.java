package org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<DatatypeVariableUnitePRO> {

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.refdata.impl.messages";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_DTVQ_BAD_UNITS = "PROPERTY_MSG_DTVQ_BAD_UNITS";
    private static final String PROPERTY_MSG_DTVQ_BAD_VQ = "PROPERTY_MSG_DTVQ_BAD_VQ";
    private static final String PROPERTY_MSG_DTVQ_DATATYPE_BAD_NAME = "PROPERTY_MSG_DTVQ_DATATYPE_BAD_NAME";
    private static final String PROPERTY_MSG_VARIABLE_QUALIFIANT_BAD_VARIABLE = "PROPERTY_MSG_VARIABLE_QUALIFIANT_BAD_VARIABLE";
    private static final String PROPERTY_MSG_DTVQM_BAD_METHODE = "PROPERTY_MSG_DTVQM_BAD_METHODE";
    private static final String PROPERTY_MSG_DTVQ_HUMIDITE_BAD_CODE = "PROPERTY_MSG_DTVQ_HUMIDITE_BAD_CODE";

    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IUniteDAO uniteDAO;
    IVariablesPRODAO variPRODAO;
    IDatatypeDAO datatypeDAO;
    IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;

    /**
     *
     */
    protected IDataTypeVariableQualifiantDAO datatypeVariableUniteDAO;

    private String[] listeUnitsPossibles;
    private String[] listeDataTypePossibles;
    private String[] listeCodeVariablePossibles;
    private String[] listeMethodePossibles;
    private String[] listeHumiditePossibles;

    /**
     *
     */
    public void getNamesUnitesPossibles() {
        List<Unitepro> unites = uniteproDAO.getAll();
        String[] getNamesUnitesPossibles = new String[unites.size() + 1];
        getNamesUnitesPossibles[0] = "";
        int index = 1;
        for (Unitepro unite : unites) {
            getNamesUnitesPossibles[index++] = unite.getCode();
        }
        this.listeUnitsPossibles = getNamesUnitesPossibles;
    }

    /**
     *
     */
    public void getNamesVariablesPossibles() {
        List<Variable> variables = variPRODAO.getAll();
        String[] namesVariablesPossibles = new String[variables.size() + 1];
        namesVariablesPossibles[0] = "";
        int index = 1;
        for (Variable variable : variables) {
            namesVariablesPossibles[index++] = variable.getName();
        }
        this.listeCodeVariablePossibles = namesVariablesPossibles;
    }

    private void dataTypePossibles() {
        List<DataType> groupedt = datatypeDAO.getAll();
        String[] Listedatatype = new String[groupedt.size() + 1];
        Listedatatype[0] = "";
        int index = 1;
        for (DataType datatype : groupedt) {
            Listedatatype[index++] = datatype.getName();
        }
        this.listeDataTypePossibles = Listedatatype;
    }

    private void methodePossibles() {
        List<Methode> groupunits = methodeDAO.getAll();
        String[] Listeunits = new String[groupunits.size() + 1];
        Listeunits[0] = "";
        int index = 1;
        for (Methode methode : groupunits) {
            Listeunits[index++] = methode.getMethode_code();
        }
        this.listeMethodePossibles = Listeunits;
    }

    private void initHumidite() {
        List<HumiditeExpression> groupehumidite = humiditeDAO.getAll();
        String[] listeHumidite = new String[groupehumidite.size() + 1];
        listeHumidite[0] = "";
        int index = 1;
        for (HumiditeExpression humid : groupehumidite) {
            listeHumidite[index++] = humid.getCode();
        }
        this.listeHumiditePossibles = listeHumidite;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String string) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = tokenizerValues.nextToken();
                final String uniteCode = tokenizerValues.nextToken();
                final String methodeCode = tokenizerValues.nextToken();
                final String humiditeCode = tokenizerValues.nextToken();
                Methode methode = methodeDAO.getByNKey(methodeCode).orElseThrow(() -> new BusinessException(String.format("no methode %s", methodeCode)));
                HumiditeExpression humiditeExpression = humiditeDAO.getByNKey(humiditeCode).orElseThrow(() -> new BusinessException(String.format("no humidite %s", humiditeCode)));
                dataTypeVariableUnitePRODAO.remove(this.dataTypeVariableUnitePRODAO
                        .getByNKey(datatypeCode, variableCode, uniteCode, methode, humiditeExpression)
                        .orElseThrow(PersistenceException::new)
                );
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    @Override
    protected List<DatatypeVariableUnitePRO> getAllElements() throws BusinessException {
        List<DatatypeVariableUnitePRO> all = this.dataTypeVariableUnitePRODAO
                .getAll();
        Collections.sort(all);
        return all;
    }

    @Override
    public void processRecord(CSVParser parser, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        Map<DataType, List<DatatypeVariableUnitePRO>> nodeablesDVU = new HashMap();
        try {
            this.skipHeader(parser);
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                int indexdt = tokenizerValues.currentTokenIndex();
                final String datatype = tokenizerValues.nextToken();
                int indexv = tokenizerValues.currentTokenIndex();
                final String variable = tokenizerValues.nextToken();
                int indexu = tokenizerValues.currentTokenIndex();
                final String unite = tokenizerValues.nextToken();
                int indexm = tokenizerValues.currentTokenIndex();
                final String methode = tokenizerValues.nextToken();
                int indexca = tokenizerValues.currentTokenIndex();
                final String humidite = tokenizerValues.nextToken();
                if(errorsReport.hasErrors()){
                    break;
                }
                final DatatypeVariableUnitePRO nodeable = persistDVUMH(errorsReport, parser.getLastLineNumber(), datatype, variable, unite, methode, humidite);
                if (nodeable != null) {
                    nodeablesDVU.computeIfAbsent(nodeable.getDatatype(), k -> new LinkedList<>())
                            .add(nodeable);
                }
                if (parser.getLastLineNumber() % 50 == 0) {
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                }
            }
            RefDataUtil.gestionErreurs(errorsReport);
            try {

                Map<DataType, List<INode>> datatypeNodesMap = mgaServiceBuilder.loadNodesByTypeResource(WhichTree.TREEDATASET, DataType.class)
                        .filter(n -> nodeablesDVU.containsKey(n.getNodeable()))
                        .collect(Collectors.groupingBy(
                                p -> ((DataType) p.getNodeable()))
                        );
                int[] i = new int[]{0};
                for (Iterator<Map.Entry<DataType, List<DatatypeVariableUnitePRO>>> iteratorOnDVU = nodeablesDVU.entrySet().iterator(); iteratorOnDVU.hasNext();) {
                    Map.Entry<DataType, List<DatatypeVariableUnitePRO>> dvusEntry = iteratorOnDVU.next();
                    DataType datatype = dvusEntry.getKey();
                    List<DatatypeVariableUnitePRO> dvus = dvusEntry.getValue();
                    List<INode> datatypeNodeList = datatypeNodesMap.get(datatype);
                    if(datatypeNodeList==null){
                        continue;
                    }
                    for (Iterator<INode> iteratorOnDatatypeNodes = datatypeNodeList.iterator(); iteratorOnDatatypeNodes.hasNext();) {
                        INode datatypeNode = iteratorOnDatatypeNodes.next();
                        for (Iterator<DatatypeVariableUnitePRO> dvuIterator = dvus.iterator(); dvuIterator.hasNext();) {
                            DatatypeVariableUnitePRO dvu = dvuIterator.next();
                            RealNode parentRn = datatypeNode.getRealNode();
                            String path = String.format("%s%s%s", parentRn.getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = new RealNode(parentRn, null, dvu, path);
                            mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) datatypeNode, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        }
                        iteratorOnDatatypeNodes.remove();
                    }
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                    iteratorOnDVU.remove();
                }
                policyManager.clearTreeFromSession();

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            } catch (final BusinessException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DatatypeVariableUnitePRO datatypevariableunitepro) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypevariableunitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypevariableunitepro.getDatatype().getName() != null
                                ? datatypevariableunitepro.getDatatype().getName() : "",
                        listeDataTypePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypevariableunitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypevariableunitepro.getVariablespro().getName() != null
                                ? datatypevariableunitepro.getVariablespro().getName() : "",
                        listeCodeVariablePossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        datatypevariableunitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                                : datatypevariableunitepro.getUnitepro().getCode() != null
                                ? datatypevariableunitepro.getUnitepro().getCode() : "",
                        listeUnitsPossibles,
                        null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypevariableunitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : datatypevariableunitepro.getMethode().getMethode_code() != null
                        ? datatypevariableunitepro.getMethode().getMethode_code() : "", listeMethodePossibles,
                        null, true, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypevariableunitepro == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : datatypevariableunitepro.getHumiditeexpression().getCode() != null
                        ? datatypevariableunitepro.getHumiditeexpression().getCode() : "", listeHumiditePossibles,
                        null, true, false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param uniteDAO
     */
    public void setUniteDAO(IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }

    @Override
    protected ModelGridMetadata<DatatypeVariableUnitePRO> initModelGridMetadata() {

        getNamesUnitesPossibles();
        getNamesVariablesPossibles();
        dataTypePossibles();
        methodePossibles();
        initHumidite();

        return super.initModelGridMetadata();

    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    /**
     *
     * @return
     */
    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    /**
     *
     * @param methodeDAO
     */
    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    /**
     *
     * @param humiditeDAO
     */
    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    /**
     *
     * @param dataTypeVariableUnitePRODAO
     */
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    /**
     *
     * @param uniteproDAO
     */
    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteDAO(IDataTypeVariableQualifiantDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }

    private DatatypeVariableUnitePRO persistDVUMH(ErrorsReport errorsReport, long line, String datatypeCode, String variableCode, String uniteCode, String methodeCode, String humiditeCode) {
        final DataType dbDatatype = this.retrieveDBDatatype(errorsReport, line, 1, datatypeCode);
        final VariablesPRO dbVariable = this.retrieveDBVariable(errorsReport, line, 2, variableCode);
        final Unitepro dbUnite = this.retrieveDBUnite(errorsReport, line, 3, uniteCode);
        final Methode dbMethode = this.retrieveDBMethode(errorsReport, line, 4, methodeCode);
        final HumiditeExpression dbHumiditeExpression = this.retrieveDBHumiditeExpression(errorsReport, line, 5, humiditeCode);
        DatatypeVariableUnitePRO datatypeVariableUnitePRO = this.dataTypeVariableUnitePRODAO
                .getByNKey(dbDatatype, dbVariable, dbUnite, dbMethode, dbHumiditeExpression).orElse(null);
        if (!errorsReport.hasErrors() && datatypeVariableUnitePRO == null) {
            try {
                datatypeVariableUnitePRO = new DatatypeVariableUnitePRO(dbDatatype, dbUnite, dbVariable, dbMethode, dbHumiditeExpression);
                this.dataTypeVariableUnitePRODAO.saveOrUpdate(datatypeVariableUnitePRO);
                this.datatypeVariableUniteDAO.flush();
                return datatypeVariableUnitePRO;
            } catch (PersistenceException ex) {
                errorsReport.addErrorMessage("");
            }
        }
        return null;
    }

    private DataType retrieveDBDatatype(ErrorsReport errorsReport, long line, int index, String datatypeCode) {
        DataType datatype = datatypeDAO.getByCode(Utils.createCodeFromString(datatypeCode)).orElse(null);
        if (datatype == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_DTVQ_DATATYPE_BAD_NAME), line, index, datatypeCode));

        }
        return datatype;
    }

    private VariablesPRO retrieveDBVariable(ErrorsReport errorsReport, long line, int index, String variableCode) {
        VariablesPRO variable = (VariablesPRO) variPRODAO.betByNKey(variableCode).orElse(null);
        if (variable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_VARIABLE_QUALIFIANT_BAD_VARIABLE), line, index, variableCode));

        }
        return variable;
    }

    private Unitepro retrieveDBUnite(ErrorsReport errorsReport, long line, int index, String uniteCode) {
        Unitepro unite = uniteproDAO.getByNKey(uniteCode).orElse(null);
        if (unite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_DTVQ_BAD_UNITS), line, index, uniteCode));

        }
        return unite;
    }

    private Methode retrieveDBMethode(ErrorsReport errorsReport, long line, int index, String methodeCode) {
        Methode methode = methodeDAO.getByNKey(methodeCode).orElse(null);
        if (methode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_DTVQM_BAD_METHODE), line, index, methodeCode));

        }
        return methode;
    }

    private HumiditeExpression retrieveDBHumiditeExpression(ErrorsReport errorsReport, long line, int index, String humiditeCode) {
        HumiditeExpression humidite = humiditeDAO.getByCode(humiditeCode).orElse(null);
        if (humidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_DTVQ_BAD_UNITS), line, index, humiditeCode));

        }
        return humidite;
    }

}
