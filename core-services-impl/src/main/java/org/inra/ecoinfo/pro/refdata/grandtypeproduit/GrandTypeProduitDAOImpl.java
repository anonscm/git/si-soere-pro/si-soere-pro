/*
 *
 */
package org.inra.ecoinfo.pro.refdata.grandtypeproduit;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits_;

/**
 *
 * @author ptcherniati
 */
public class GrandTypeProduitDAOImpl extends AbstractJPADAO<GrandTypeProduits> implements IGrandTypeProduitDAO {

    /**
     *
     * @return
     */
    @Override
    public List<GrandTypeProduits> getAll() {
        return getAll(GrandTypeProduits.class);

    }

    /**
     *
     * @param gtp_nom
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<GrandTypeProduits> getByNKey(String gtp_nom) {
        CriteriaQuery<GrandTypeProduits> query = builder.createQuery(GrandTypeProduits.class);
        Root<GrandTypeProduits> grandTypeProduits = query.from(GrandTypeProduits.class);
        query
                .select(grandTypeProduits)
                .where(
                        builder.equal(grandTypeProduits.get(GrandTypeProduits_.gtp_nom), gtp_nom)
                );
        return getOptional(query);
    }
}
