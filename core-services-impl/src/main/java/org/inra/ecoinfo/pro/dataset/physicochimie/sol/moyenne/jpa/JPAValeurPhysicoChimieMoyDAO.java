/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSolsMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.IValeurSolMoyenneDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAValeurPhysicoChimieMoyDAO extends AbstractJPADAO<ValeurPhysicoChimieSolsMoy> implements IValeurSolMoyenneDAO {

    /**
     *
     * @param dvumRealNode
     * @param mesureSol
     * @param ecartype
     * @return
     */
    @Override
    public Optional<ValeurPhysicoChimieSolsMoy> getByNKeys(RealNode dvumRealNode, MesurePhysicoChimieSolsMoy mesureSol, Float ecartype) {
        CriteriaQuery<ValeurPhysicoChimieSolsMoy> query = builder.createQuery(ValeurPhysicoChimieSolsMoy.class);
        Root<ValeurPhysicoChimieSolsMoy> v = query.from(ValeurPhysicoChimieSolsMoy.class);
        Join<ValeurPhysicoChimieSolsMoy, RealNode> realNode = v.join(ValeurPhysicoChimieSolsMoy_.realNode);
        query
                .select(v)
                .where(
                        builder.equal(v.get(ValeurPhysicoChimieSolsMoy_.ecarttype), ecartype),
                        builder.equal(v.get(ValeurPhysicoChimieSolsMoy_.mesurePhysicoChimieSolsMoy), mesureSol),
                        builder.equal(realNode, dvumRealNode)
                );
        return getOptional(query);

    }

}
