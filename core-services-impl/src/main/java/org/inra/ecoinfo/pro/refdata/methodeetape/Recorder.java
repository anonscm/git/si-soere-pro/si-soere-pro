package org.inra.ecoinfo.pro.refdata.methodeetape;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<MethodeEtapes> {
    
    /**
     *
     */
    protected IMethodeEtapeDAO methodeetapeDAO;

    /**
     *
     */
    protected Properties NomMEEN;

    /**
     *
     */
    protected Properties commentEn;
    
    private void createMethodeME(final MethodeEtapes methodeetapes) throws BusinessException {
        try {
            methodeetapeDAO.saveOrUpdate(methodeetapes);
            methodeetapeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create methodeetapes");
        }
    }
    
    private void updateBDME(final String nom, String code, String commentaire, final MethodeEtapes dbmethodeetapes) throws BusinessException {
        try {
            dbmethodeetapes.setMe_nom(nom);
            dbmethodeetapes.setMe_code(code);
            dbmethodeetapes.setCommentaire(commentaire);
            methodeetapeDAO.saveOrUpdate(dbmethodeetapes);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update methodeetapes");
        }
    }
    
    private void createOrUpdateME(final String code, String nom, String commentaire, final MethodeEtapes dbmethodeetapes) throws BusinessException {
        if (dbmethodeetapes == null) {
            final MethodeEtapes methodeetapes = new MethodeEtapes(nom);
            methodeetapes.setMe_code(code);
            methodeetapes.setMe_nom(nom);
            methodeetapes.setCommentaire(commentaire);
            createMethodeME(methodeetapes);
        } else {
            updateBDME(nom, code, commentaire, dbmethodeetapes);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MethodeEtapes.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final MethodeEtapes dbme = methodeetapeDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't find methodeetapes"));
                methodeetapeDAO.remove(dbme);
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected List<MethodeEtapes> getAllElements() throws BusinessException {
        return methodeetapeDAO.getAll();
    }
    
    /**
     *
     * @return
     */
    public IMethodeEtapeDAO getMethodeetapeDAO() {
        return methodeetapeDAO;
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MethodeEtapes methodeetapes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeetapes == null || methodeetapes.getMe_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeetapes.getMe_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeetapes == null || methodeetapes.getMe_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : NomMEEN.getProperty(methodeetapes.getMe_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeetapes == null || methodeetapes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeetapes.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeetapes == null || methodeetapes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(methodeetapes.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        return lineModelGridMetadata;
    }
    
    private void persistME(String code, String nom, String commentaire) throws BusinessException, BusinessException {
        final MethodeEtapes dbmethodeetapes = methodeetapeDAO.getByNKey(nom).orElse(null);
        createOrUpdateME(nom, code, commentaire, dbmethodeetapes);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MethodeEtapes.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                String commentaire = tokenizerValues.nextToken();
                persistME(code, nom, commentaire);
                values = parser.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    /**
     *
     * @param methodeetapeDAO
     */
    public void setMethodeetapeDAO(IMethodeEtapeDAO methodeetapeDAO) {
        this.methodeetapeDAO = methodeetapeDAO;
    }
    
    @Override
    public ModelGridMetadata<MethodeEtapes> initModelGridMetadata() {
        NomMEEN = localizationManager.newProperties(MethodeEtapes.NAME_ENTITY_JPA, MethodeEtapes.JPA_COLUMN_NAME, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(MethodeEtapes.NAME_ENTITY_JPA, MethodeEtapes.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    /**
     *
     * @param NomMEEN
     */
    public void setNomMEEN(Properties NomMEEN) {
        this.NomMEEN = NomMEEN;
    }
    
    /**
     *
     * @return
     */
    public Properties getNomMEEN() {
        return NomMEEN;
    }
    
}
