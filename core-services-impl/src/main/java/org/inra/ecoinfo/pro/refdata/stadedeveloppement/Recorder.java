/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.stadedeveloppement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<StadeDeveloppement> {

    IStadeDAO stadeDAO;
    private Properties commentEn;

    private void createStade(final StadeDeveloppement stadeDeveloppement) throws BusinessException {
        try {
            stadeDAO.saveOrUpdate(stadeDeveloppement);
            stadeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create stadeDeveloppement");
        }
    }

    private void updateBDStade(String code, String mycode, String bbch, String commentaire, StadeDeveloppement dbespece) throws BusinessException {
        try {
            dbespece.setCode(code);
            //  dbespece.setNom(nom);
            dbespece.setMycode(mycode);
            dbespece.setCodebbch(bbch);
            dbespece.setCommentaire(commentaire);
            stadeDAO.saveOrUpdate(dbespece);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update stadeDeveloppement");
        }
    }

    private void createOrUpdateStade(final String code, String mycode, String bbch, String commentaire, final StadeDeveloppement dbespece) throws BusinessException {
        if (dbespece == null) {
            final StadeDeveloppement stade = new StadeDeveloppement(code, bbch, mycode, commentaire);
            stade.setCode(code);
            // stade.setNom(nom);
            stade.setMycode(mycode);
            stade.setCodebbch(bbch);
            stade.setCommentaire(commentaire);
            createStade(stade);
        } else {
            updateBDStade(code, mycode, bbch, commentaire, dbespece);
        }
    }

    private void persistStade(final String code, String mycode, String bbch, String commentaire) throws BusinessException, BusinessException {
        final StadeDeveloppement dbMethode = stadeDAO.getByNKey(code).orElse(null);
        createOrUpdateStade(code, mycode, bbch, commentaire, dbMethode);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                //final String code = Utils.createCodeFromString(intitule);
                final StadeDeveloppement dbEspece = stadeDAO.getByNKey(intitule)
                        .orElseThrow(() -> new BusinessException("can't get stadeDeveloppement"));
                stadeDAO.remove(dbEspece);

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, StadeDeveloppement.NAME_ENTITY_JPA);
                //final String code = tokenizerValues.nextToken();
                final String bbch = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                final String mycode = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistStade(nom, mycode, bbch, commentaire);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<StadeDeveloppement> getAllElements() throws BusinessException {
        return stadeDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StadeDeveloppement stade) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        /* lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(stade == null ? AbstractCSVMetadataRecorder.EMPTY_STRING :
                        stade.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));*/
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(stade == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : stade.getCodebbch(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(stade == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : stade.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(stade == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : stade.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(stade == null || stade.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(stade.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<StadeDeveloppement> initModelGridMetadata() {
        commentEn = localizationManager.newProperties(StadeDeveloppement.NAME_ENTITY_JPA, StadeDeveloppement.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param stadeDAO
     */
    public void setStadeDAO(IStadeDAO stadeDAO) {
        this.stadeDAO = stadeDAO;
    }

}
