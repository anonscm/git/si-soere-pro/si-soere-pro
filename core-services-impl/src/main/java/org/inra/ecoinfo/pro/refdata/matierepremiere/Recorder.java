/*
 *
 */
package org.inra.ecoinfo.pro.refdata.matierepremiere;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

// TODO: Auto-generated Javadoc
/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<MatieresPremieres> {

    /**
     * The matierePremiere dao.
     */
    protected IMatierePremiereDAO matierePremiereDAO;

    /**
     * The MP non en.
     */
    private Properties MPNonEN;
    private Properties commentEn;

    private void createMatierePremiere(final MatieresPremieres matierePremiere) throws BusinessException {
        try {
            matierePremiereDAO.saveOrUpdate(matierePremiere);
            matierePremiereDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create matiere premiere");
        }
    }

    private void createOrUpdateMatierePremiere(final String code, String nom, String commentaire, final MatieresPremieres dbmatierePremiere) throws BusinessException {
        if (dbmatierePremiere == null) {
            final MatieresPremieres matierePremieres = new MatieresPremieres(nom, code, commentaire);
            matierePremieres.setMatierep_code(code);
            matierePremieres.setMatierep_nom(nom);
            matierePremieres.setCommentaire(commentaire);
            createMatierePremiere(matierePremieres);
        } else {
            updateBDMatierePremiere(code, nom, commentaire, dbmatierePremiere);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                MatieresPremieres mpremiere = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String libelle = tokenizerValues.nextToken();
                mpremiere = matierePremiereDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't find matiere premiere"));
                matierePremiereDAO.remove(mpremiere);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<MatieresPremieres> getAllElements() throws BusinessException {

        return matierePremiereDAO.getAll();
    }

    public IMatierePremiereDAO getMatierePremiereDAO() {
        return matierePremiereDAO;
    }

    public Properties getMPNonEN() {
        return MPNonEN;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MatieresPremieres matierePremieres) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(matierePremieres == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : matierePremieres.getMatierep_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(matierePremieres == null || matierePremieres.getMatierep_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : MPNonEN.getProperty(matierePremieres.getMatierep_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(matierePremieres == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : matierePremieres.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(matierePremieres == null || matierePremieres.getMatierep_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(matierePremieres.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<MatieresPremieres> initModelGridMetadata() {
        MPNonEN = localizationManager.newProperties(MatieresPremieres.JPA_NAME_ENTITY, MatieresPremieres.JPA_COLUMN_MYNAME, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(MatieresPremieres.JPA_NAME_ENTITY, MatieresPremieres.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void persistMatierePremiere(final String nom, String code, String commentaire) throws BusinessException, BusinessException {
        final MatieresPremieres dbmatierePremiere = matierePremiereDAO.getByNKey(nom).orElse(null);
        createOrUpdateMatierePremiere(code, nom, commentaire, dbmatierePremiere);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MatieresPremieres.JPA_NAME_ENTITY);
                final String nom = tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(nom);
                String commentaire = tokenizerValues.nextToken();
                persistMatierePremiere(nom, code, commentaire);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    public void setMatierePremiereDAO(IMatierePremiereDAO matierePremiereDAO) {
        this.matierePremiereDAO = matierePremiereDAO;
    }

    public void setMPNonEN(Properties mPNonEN) {
        MPNonEN = mPNonEN;
    }

    private void updateBDMatierePremiere(final String code, String nom, String commentaire, final MatieresPremieres dbmatierePremiere) throws BusinessException {
        try {
            dbmatierePremiere.setMatierep_code(code);
            dbmatierePremiere.setMatierep_nom(nom);
            dbmatierePremiere.setCommentaire(commentaire);
            matierePremiereDAO.saveOrUpdate(dbmatierePremiere);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update matiere premiere");
        }
    }

}
