/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie_;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationProEtudieDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurProEtudie> deleteValeurs = builder.createCriteriaDelete(ValeurProEtudie.class);
        Root<ValeurProEtudie> valeur = deleteValeurs.from(ValeurProEtudie.class);
        Subquery<MesureProEtudie> subquery = deleteValeurs.subquery(MesureProEtudie.class);
        Root<MesureProEtudie> m = subquery.from(MesureProEtudie.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureProEtudie_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurProEtudie_.mesureinterventionproetudie).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureProEtudie> deleteSequence = builder.createCriteriaDelete(MesureProEtudie.class);
        Root<MesureProEtudie> sequence = deleteSequence.from(MesureProEtudie.class);
        deleteSequence.where(builder.equal(sequence.get(MesureProEtudie_.versionfile), version));
        delete(deleteSequence);
    }

}
