/*
 *
 */
package org.inra.ecoinfo.pro.refdata.nomenclature;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.typenomenclature.TypeNomenclature;


// TODO: Auto-generated Javadoc
/**
 * The Interface INomenclatureDAO.
 */
public interface INomenclatureDAO extends IDAO<Nomenclatures> {

    /**
     *
     * @return
     */
    List<Nomenclatures> getAll();
    
    /**
     *
     * @param nom
     * @param tnnom
     * @return
     */
    Optional<Nomenclatures> getByNKey(String nom,String tnnom);
    
    /**
     *
     * @param typeNomenclature
     * @return
     */
    List<Nomenclatures> getByTypeNomenclature(TypeNomenclature typeNomenclature);

}
