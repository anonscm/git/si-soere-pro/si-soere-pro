package org.inra.ecoinfo.pro.refdata.melange;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.component.IComposantDAO;
import org.inra.ecoinfo.pro.refdata.precisionpourcentage.IPrecisionPourcentageDAO;
import org.inra.ecoinfo.pro.refdata.precisionpourcentage.PrecisionPourcentage;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public class Recorder extends AbstractGenericRecorder<Melange> {

    private static final String PROPERTY_MSG_MELANGE_BAD_POURCENT = "PROPERTY_MSG_MELANGE_BAD_POURCENT";

    private static final String PROPERTY_MSG_MELANGE_BAD_CODECOMPOSANT = "PROPERTY_MSG_MELANGE_BAD_CODECOMPOSANT";

    private static final String PROPERTY_MSG_MELANGE_BAD_PROCUITCODE = "PROPERTY_MSG_MELANGE_BAD_PROCUITCODE";

    private static final String PROPERTY_MSG_MELANGE_BAD_PRECISION = "PROPERTY_MSG_MELANGE_BAD_PRECISION";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    public static String[] listeProduitsPossibles;

    /**
     *
     */
    public static Map<String, String[]> listeComposantPossibles;

    /**
     *
     */
    public static Map<String, String[]> listePourcentagePossibles;

    /**
     *
     * @param listeProduitsPossibles
     */
    public static void setListeProduitsPossibles(String[] listeProduitsPossibles) {
        Recorder.listeProduitsPossibles = listeProduitsPossibles;
    }

    /**
     *
     * @param listeComposantPossibles
     */
    public static void setListeComposantPossibles(Map<String, String[]> listeComposantPossibles) {
        Recorder.listeComposantPossibles = listeComposantPossibles;
    }

    /**
     *
     * @param listePourcentagePossibles
     */
    public static void setListePourcentagePossibles(Map<String, String[]> listePourcentagePossibles) {
        Recorder.listePourcentagePossibles = listePourcentagePossibles;
    }

    /**
     *
     * @param melangeDAO
     */
    public static void initMelangesPossibles(IMelangeDAO melangeDAO) {
        Map<String, Map<String, List<String>>> melangesMap = new HashMap<String, Map<String, List<String>>>();
        List<Melange> melanges = melangeDAO.getAll();
        melanges.forEach((melange) -> {
            String codeProduit = melange.getProduits().getCodecomposant();
            String codeComposant = melange.getComposant().getCodecomposant();
            String pourcentage = Double.toString(melange.getPourcentage());
            melangesMap
                    .computeIfAbsent(codeProduit, k -> new HashMap<String, List<String>>())
                    .computeIfAbsent(codeComposant, k -> new LinkedList())
                    .add(pourcentage);
        });
        Recorder.setListeProduitsPossibles(melangesMap.keySet().toArray(new String[]{}));
        ConcurrentHashMap<String, String[]> listeComposantPossibles = new ConcurrentHashMap<String, String[]>();
        ConcurrentHashMap<String, String[]> listePourcentagePossibles = new ConcurrentHashMap<String, String[]>();
        melangesMap.entrySet().forEach((entryType) -> {
            String produit = entryType.getKey();
            Set<String> composant = entryType.getValue().keySet();
            listeComposantPossibles.put(produit, composant.toArray(new String[]{}));
            entryType.getValue().entrySet().forEach((entryCar) -> {
                String parcelle = entryCar.getKey();
                List<String> values = entryCar.getValue();
                String parcelleElementaire = String.format("%s/%s", produit, parcelle);
                listePourcentagePossibles.put(parcelleElementaire, values.toArray(new String[]{}));
            });
        });
        Recorder.setListeComposantPossibles(listeComposantPossibles);
        Recorder.setListePourcentagePossibles(listePourcentagePossibles);
    }

    /**
     *
     * @param lineModelGridMetadata
     * @param melange
     */
    public static void initNewLine(LineModelGridMetadata lineModelGridMetadata, Melange melange) {
        final Object codeProduit = melange == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : melange.getProduits() != null
                ? melange.getProduits().getProd_key()
                : "";
        ColumnModelGridMetadata produitColumn = new ColumnModelGridMetadata(codeProduit, listeProduitsPossibles, null, true, false, true);
        final String codeComposant = melange == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : melange.getComposant() != null
                ? melange.getComposant().getCodecomposant()
                : "";
        final String percent = melange == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : Double.toString(melange.getPourcentage());
        ColumnModelGridMetadata composantColumn = new ColumnModelGridMetadata(codeComposant, listeComposantPossibles, null, true, false, true);
        ColumnModelGridMetadata percentColumn = new ColumnModelGridMetadata(percent, listePourcentagePossibles, null, true, false, true);
        List<ColumnModelGridMetadata> produitsRefs = new LinkedList<ColumnModelGridMetadata>();
        produitsRefs.add(composantColumn);
        composantColumn.setRefBy(produitColumn);
        produitColumn.setRefs(produitsRefs);
        composantColumn.setValue(codeComposant);
        List<ColumnModelGridMetadata> composantsRefs = new LinkedList<ColumnModelGridMetadata>();
        composantsRefs.add(percentColumn);
        percentColumn.setRefBy(composantColumn);
        composantColumn.setRefs(composantsRefs);
        percentColumn.setValue(percent);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(produitColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(composantColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(percentColumn);
    }

    /**
     *
     */
    protected IMelangeDAO melangeDAO;

    /**
     *
     */
    protected IProduitDAO produitDAO;

    IComposantDAO composantDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;
    IPrecisionPourcentageDAO precisionPourcenDAO;
    Properties commentEn;

    private String[] listePrecisionPossibles;

    private String[] listeestProPossibles;

    private void createMelange(final Melange melange) throws BusinessException {
        try {
            melangeDAO.saveOrUpdate(melange);
            melangeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create melange");
        }
    }

    private void updateBDMelange(Produits prod, Composant compo, double pourcent, String comment, final Melange dbmelange,
            PrecisionPourcentage precision) throws BusinessException {
        try {
            dbmelange.setProduits(prod);
            dbmelange.setComposant(compo);
            dbmelange.setPourcentage(pourcent);
            dbmelange.setCommentaire(comment);
            dbmelange.setPrecisionpourcentage(precision);
            melangeDAO.saveOrUpdate(dbmelange);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update melange");
        }
    }

    private void createOrUpdateMelange(Produits prod, Composant compo, double pourcent, String comment, final Melange dbmelange,
            boolean isPro, PrecisionPourcentage precision) throws BusinessException {
        if (dbmelange == null) {
            Melange melange = new Melange(prod, compo, precision, pourcent, comment);
            melange.setProduits(prod);
            melange.setComposant(compo);
            melange.setPourcentage(pourcent);
            melange.setCommentaire(comment);
            melange.setPrecisionpourcentage(precision);
            createMelange(melange);
        } else {
            updateBDMelange(prod, compo, pourcent, comment, dbmelange, precision);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Melange.NAME_ENTITY_JAP);
                final String prod_code = tokenizerValues.nextToken();
                boolean isPro = Composant.BOOLEAN_OUI.equalsIgnoreCase(tokenizerValues.nextToken());
                final String codecomposant = tokenizerValues.nextToken();
                final String pourcent = tokenizerValues.nextToken();
                double cent = Double.parseDouble(pourcent);
                Produits dbproduits = produitDAO.getByNKey(prod_code).orElse(null);
                Composant dbcomposant = composantDAO.getByNKey(codecomposant, isPro).orElse(null);
                final Melange dbmelange = melangeDAO.getByNKey(dbproduits, dbcomposant, cent)
                        .orElseThrow(() -> new BusinessException("can't find melange"));
                melangeDAO.remove(dbmelange);
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Melange> getAllElements() throws BusinessException {
        return melangeDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IComposantDAO getComposantDAO() {
        return composantDAO;
    }

    /**
     *
     * @return
     */
    public IMelangeDAO getMelangeDAO() {
        return melangeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Melange melange) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(melange == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : melange.getProduits() != null ? melange.getProduits().getProd_key() : "",
                        listeProduitsPossibles, null, true, false, true));

        String estpro = melange == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : melange.getComposant() != null ? melange.getComposant().getIsProString() : "";
        ColumnModelGridMetadata columnIspro = new ColumnModelGridMetadata(estpro, listeestProPossibles, null, true, false, true);
        String codeComposant = melange == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : melange.getComposant() != null ? melange.getComposant().getCodecomposant() : "";
        ColumnModelGridMetadata columnProcede = new ColumnModelGridMetadata(codeComposant, listeComposantPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnProcede);
        columnProcede.setValue(codeComposant);
        columnIspro.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnIspro);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnProcede);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(melange == null || melange.getPourcentage() == 0 ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : melange.getPourcentage(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(melange == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : melange.getComposant() != null ? melange.getPrecisionpourcentage().getLibelle()
                        : "", listePrecisionPossibles, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(melange == null || melange.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : melange.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(melange == null || melange.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(melange.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    @Override
    protected ModelGridMetadata<Melange> initModelGridMetadata() {
        listeProduitsPossibles();
        listePrecisoin();
        isProPossibles();
        commentEn = localizationManager.newProperties(Melange.NAME_ENTITY_JAP, Melange.JPA_COLUMN_VALUE, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void isProPossibles() {
        Map<String, String[]> produitsMatierep = new HashMap<>();
        Map<String, Set<String>> composantList = new HashMap<>();
        List<? extends Composant> groupeispro = composantDAO.getAll();
        groupeispro.forEach((composant) -> {
            String estpro = composant.getIsProString();
            String nomcomposant = composant.getCodecomposant();
            if (!composantList.containsKey(estpro)) {
                composantList.put(estpro, new TreeSet());
            }
            composantList.get(estpro).add(nomcomposant);
        });
        composantList.entrySet().forEach((entryProduit) -> {
            produitsMatierep.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeComposantPossibles = produitsMatierep;
        listeestProPossibles = produitsMatierep.keySet().toArray(new String[]{});
    }

    private void listePrecisoin() {
        List<PrecisionPourcentage> lescodes = precisionPourcenDAO.getAll(PrecisionPourcentage.class);
        String[] ListeCodes = new String[lescodes.size() + 1];
        ListeCodes[0] = "";
        int index = 1;
        for (PrecisionPourcentage precision : lescodes) {
            ListeCodes[index++] = precision.getLibelle();
        }
        this.listePrecisionPossibles = ListeCodes;
    }

    private void listeProduitsPossibles() {
        List<Produits> groupeproduits = produitDAO.getAll();
        String[] ListeProduitsPossibles = new String[groupeproduits.size() + 1];
        ListeProduitsPossibles[0] = "";
        int index = 1;
        for (Produits produits : groupeproduits) {
            ListeProduitsPossibles[index++] = produits.getProd_key();
        }
        this.listeProduitsPossibles = ListeProduitsPossibles;
    }

    private void persistMelange(Produits produits, Composant composant, double pourcent, String commentaire,
            boolean isPro, PrecisionPourcentage precision) throws BusinessException, BusinessException {
        final Melange dbmelange = melangeDAO.getByNKey(produits, composant, pourcent).orElse(null);
        createOrUpdateMelange(produits, composant, pourcent, commentaire, dbmelange, isPro, precision);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Melange.NAME_ENTITY_JAP);
                int indexprod = tokenizerValues.currentTokenIndex();
                final String prod_code = tokenizerValues.nextToken();
                int indexcomp = tokenizerValues.currentTokenIndex();
                final boolean isPro = Composant.BOOLEAN_OUI.equals(tokenizerValues.nextToken());
                final String codecomposant = tokenizerValues.nextToken();
                int indexpourcent = tokenizerValues.currentTokenIndex();
                final double pourcentage = verifieDouble(tokenizerValues, line, true, errorsReport);
                if (pourcentage > 100) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_MELANGE_BAD_POURCENT), line, indexpourcent, pourcentage));
                }
                int indexpp = tokenizerValues.currentTokenIndex();
                final String precision = tokenizerValues.nextToken();
                final String comment = tokenizerValues.nextToken();

                Produits dbprod = produitDAO.getByNKey(prod_code).orElse(null);
                if (dbprod == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_MELANGE_BAD_PROCUITCODE), line, indexprod, prod_code));
                }
                Composant dbcomposant = composantDAO.getByNKey(codecomposant, isPro).orElse(null);
                if (dbcomposant == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_MELANGE_BAD_CODECOMPOSANT), line, indexcomp, codecomposant));
                }

                PrecisionPourcentage dbprecision = precisionPourcenDAO.getBYNKey(precision).orElse(null);
                if (dbprecision == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_MELANGE_BAD_PRECISION), line, indexpp, precision));
                }

                if (!errorsReport.hasErrors()) {
                    persistMelange(dbprod, dbcomposant, pourcentage, comment, isPro, dbprecision);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param composantDAO
     */
    public void setComposantDAO(IComposantDAO composantDAO) {
        this.composantDAO = composantDAO;
    }

    /**
     *
     * @param melangeDAO
     */
    public void setMelangeDAO(IMelangeDAO melangeDAO) {
        this.melangeDAO = melangeDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param precisionPourcenDAO
     */
    public void setPrecisionPourcenDAO(IPrecisionPourcentageDAO precisionPourcenDAO) {
        this.precisionPourcenDAO = precisionPourcenDAO;
    }


}
