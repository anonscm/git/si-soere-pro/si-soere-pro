/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.elementaire.impl;

import java.util.Map;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieParameter;

/**
 *
 * @author adiankha
 */
public class PlanteBrutParameters extends AbstractPhysicoChimieParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_PLANTEBRUT = "physico_chimie_des_plantes_elementaires" ;
    
    /**
     *
     */
    public static final String  PLANTEBRUT                    = "PlanteBrut" ;

    /**
     *
     * @param metadatasMap
     */
    public PlanteBrutParameters(Map<String, Object> metadatasMap) {
        super(metadatasMap);
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_PLANTEBRUT;
    }
}
