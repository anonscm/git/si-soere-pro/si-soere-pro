/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC.jpa;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationSWCDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     */
    public static String QUERY_DELETE_MEASURE = "delete from MesureSWC m where m.versionfile=:version";

    /**
     *
     */
    public static String QUERY_DELETE_VALEUR = "delete MesureSWC in (select id from MesureSWC mswc where mswc.versionfile=:version)";

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
    }
}
