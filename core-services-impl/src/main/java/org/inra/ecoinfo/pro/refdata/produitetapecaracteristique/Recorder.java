package org.inra.ecoinfo.pro.refdata.produitetapecaracteristique;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.CaracteristiqueEtapes;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.ICaracteristiqueEtapeDAO;
import org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape.IMethodeProcedeEtapesDAO;
import org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape.MethodeProcedeEtapes;
import org.inra.ecoinfo.pro.refdata.unites.IUniteDAO;
import org.inra.ecoinfo.pro.refdata.unites.Unites;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<EtapeCaracterisitique> {

    private static final String PROPERTY_MSG_PEC_BAD_KEY = "PROPERTY_MSG_PEC_BAD_KEY";

    private static final String PROPERTY_MSG_PEC_BAD_NOM = "PROPERTY_MSG_PEC_BAD_NOM";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected IEtapeCaracteristiqueDAO etapecaracteristiqueDAO;

    /**
     *
     */
    protected ICaracteristiqueEtapeDAO caracetapeDAO;

    /**
     *
     */
    protected IMethodeProcedeEtapesDAO methodeprocedeetapeDAO;

    /**
     *
     */
    protected IUniteDAO unitesDAO;
    private String[] listeCaracteristiqueEtapesPossibles;
    private String[] listeUnitesPossibles;
    private String[] listeProduitsPossibles;
    private ConcurrentMap<String, String[]> listeProcedePossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeOrdreProcedesPossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeEtapePossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeMethodEtapePossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeOrdresEtapePossibles = new ConcurrentHashMap<>();
    Properties commentaireEn;

    private void caracteristiqueEtapesPossibles() {
        List<CaracteristiqueEtapes> groupecaracteristiqueetapes = caracetapeDAO.getAll();
        String[] Listecaracteristiqueetapes = new String[groupecaracteristiqueetapes.size() + 1];
        Listecaracteristiqueetapes[0] = "";
        int index = 1;
        for (CaracteristiqueEtapes caracteristiqueetapes : groupecaracteristiqueetapes) {
            Listecaracteristiqueetapes[index++] = caracteristiqueetapes.getCetape_intitule();
        }
        this.listeCaracteristiqueEtapesPossibles = Listecaracteristiqueetapes;
    }

    private void createEtapeCaracteristique(final EtapeCaracterisitique etapecaracteristique) throws BusinessException {
        try {
            etapecaracteristiqueDAO.saveOrUpdate(etapecaracteristique);
            etapecaracteristiqueDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create etapecaracteristique");
        }
    }

    private void createOrUpdateEtapeCaracteristique(final MethodeProcedeEtapes mpe, int valeur, String unite, String commentaire, CaracteristiqueEtapes ce, final EtapeCaracterisitique dbEtapeCaracteristiques) throws BusinessException {
        if (dbEtapeCaracteristiques == null) {
            final EtapeCaracterisitique etapecaracteristique = new EtapeCaracterisitique(mpe, ce, valeur, unite, commentaire);
            etapecaracteristique.setMethodeprocedeetapes(mpe);
            etapecaracteristique.setCaracteristiqueetapes(ce);
            etapecaracteristique.setEce_unite(unite);
            etapecaracteristique.setEce_valeur(valeur);
            etapecaracteristique.setCommentaire(commentaire);
            createEtapeCaracteristique(etapecaracteristique);
        } else {
            updateBDEtapeCaracteristique(mpe, valeur, unite, ce, commentaire, dbEtapeCaracteristiques);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            int line = 0;
            ErrorsReport errorsReport = new ErrorsReport();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeProduits = tokenizerValues.nextToken();
                final String intituleProcede = tokenizerValues.nextToken();
                final int ordreProcede = verifieInt(tokenizerValues, line, true, errorsReport);
                final String etape = tokenizerValues.nextToken();
                final String methodeEtape = tokenizerValues.nextToken();
                final int ordreEtape = verifieInt(tokenizerValues, line, true, errorsReport);
                final String caracteristiqueEtape = tokenizerValues.nextToken();
                int value = verifieInt(tokenizerValues, line, true, errorsReport);
                final String unite = tokenizerValues.nextToken();

                final MethodeProcedeEtapes dbmpe = methodeprocedeetapeDAO.getByNKey(codeProduits, intituleProcede, ordreProcede, methodeEtape, etape, ordreEtape).orElse(null);

                final CaracteristiqueEtapes dbce = caracetapeDAO.getByNKey(caracteristiqueEtape).orElse(null);

                final EtapeCaracterisitique dbec = etapecaracteristiqueDAO.getByNKey(dbmpe, dbce, value, unite)
                        .orElseThrow(() -> new BusinessException("can't get EtapeCaracterisitique"));

                etapecaracteristiqueDAO.remove(dbec);
                values = parser.getLine();

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<EtapeCaracterisitique> getAllElements() throws BusinessException {
        return etapecaracteristiqueDAO.getAll();
    }

    /**
     *
     * @return
     */
    public ICaracteristiqueEtapeDAO getCaracetapeDAO() {
        return caracetapeDAO;
    }

    /**
     *
     * @return
     */
    public IEtapeCaracteristiqueDAO getEtapecaracteristiqueDAO() {
        return etapecaracteristiqueDAO;
    }

    /**
     *
     * @return
     */
    public IMethodeProcedeEtapesDAO getMethodeprocedeetapeDAO() {
        return methodeprocedeetapeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EtapeCaracterisitique etapecaracteristique) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        initNewLine(lineModelGridMetadata, etapecaracteristique.getMethodeprocedeetapes());
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(
                        etapecaracteristique == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : etapecaracteristique.getCaracteristiqueetapes() != null ? etapecaracteristique.getCaracteristiqueetapes().getCetape_intitule() : "",
                        listeCaracteristiqueEtapesPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapecaracteristique == null || etapecaracteristique.getEce_valeur() == 0 ? AbstractCSVMetadataRecorder.EMPTY_STRING : etapecaracteristique.getEce_valeur(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapecaracteristique == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : etapecaracteristique.getEce_unite() != null ? etapecaracteristique.getEce_unite() : "", listeUnitesPossibles, null, false, false,
                        false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapecaracteristique == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : etapecaracteristique.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(etapecaracteristique == null || etapecaracteristique.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(etapecaracteristique.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IUniteDAO getUnitesDAO() {
        return unitesDAO;
    }

    @Override
    protected ModelGridMetadata<EtapeCaracterisitique> initModelGridMetadata() {
        initMethodeProduitsProcedesEtapesPossibles();
        caracteristiqueEtapesPossibles();
        unitesNomPossibles();
        commentaireEn = localizationManager.newProperties(EtapeCaracterisitique.NAME_ENTITY_JAP, EtapeCaracterisitique.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param errorsReport
     * @param etapes
     * @param caracteristiqueetapes
     * @throws BusinessException
     * @throws BusinessException
     */
    private void persistEtapeCaracteristique(MethodeProcedeEtapes mpe, CaracteristiqueEtapes caracteristiqueetapes, int valeur, String unite, String commentaire) throws BusinessException, BusinessException {
        final EtapeCaracterisitique dbetapecaracteristique = etapecaracteristiqueDAO.getByNKey(mpe, caracteristiqueetapes, valeur, unite).orElse(null);
        createOrUpdateEtapeCaracteristique(mpe, valeur, unite, commentaire, caracteristiqueetapes, dbetapecaracteristique);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EtapeCaracterisitique.NAME_ENTITY_JAP);
                int indexprod = tokenizerValues.currentTokenIndex();
                final String codeProduits = tokenizerValues.nextToken();
                int indexprocede = tokenizerValues.currentTokenIndex();
                final String intituleProcede = tokenizerValues.nextToken();
                final int ordreProcede = Integer.parseInt(tokenizerValues.nextToken());
                int indexetape = tokenizerValues.currentTokenIndex();
                final String intituleEtape = tokenizerValues.nextToken();
                int indexme = tokenizerValues.currentTokenIndex();
                final String methodeEtape = tokenizerValues.nextToken();
                final int ordreEtape = Integer.parseInt(tokenizerValues.nextToken());
                int indexce = tokenizerValues.currentTokenIndex();
                final String caracteristiqueEtape = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                int value = Integer.parseInt(valeur);
                final String unite = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();

                MethodeProcedeEtapes dbmpe = methodeprocedeetapeDAO.getByNKey(codeProduits, intituleProcede, ordreProcede, methodeEtape, intituleEtape, ordreEtape).orElse(null);
                if (dbmpe == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PEC_BAD_KEY), line, indexprod, codeProduits, indexprocede, intituleProcede, indexetape, intituleEtape, indexme, methodeEtape));

                }
                CaracteristiqueEtapes dbce = caracetapeDAO.getByNKey(caracteristiqueEtape).orElse(null);
                if (dbce == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PEC_BAD_NOM), line, indexce, caracteristiqueEtape));

                }
                if (!errorsReport.hasErrors()) {
                    persistEtapeCaracteristique(dbmpe, dbce, value, unite, commentaire);
                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);

        }

    }

    /**
     *
     * @param caracetapeDAO
     */
    public void setCaracetapeDAO(ICaracteristiqueEtapeDAO caracetapeDAO) {
        this.caracetapeDAO = caracetapeDAO;
    }

    /**
     *
     * @param etapecaracteristiqueDAO
     */
    public void setEtapecaracteristiqueDAO(IEtapeCaracteristiqueDAO etapecaracteristiqueDAO) {
        this.etapecaracteristiqueDAO = etapecaracteristiqueDAO;
    }

    /**
     *
     * @param methodeprocedeetapeDAO
     */
    public void setMethodeprocedeetapeDAO(IMethodeProcedeEtapesDAO methodeprocedeetapeDAO) {
        this.methodeprocedeetapeDAO = methodeprocedeetapeDAO;
    }

    /**
     *
     * @param unitesDAO
     */
    public void setUnitesDAO(IUniteDAO unitesDAO) {
        this.unitesDAO = unitesDAO;
    }

    private void unitesNomPossibles() {
        List<Unites> groupeunite = unitesDAO.getAll(Unites.class);
        String[] Listeunite = new String[groupeunite.size() + 1];
        Listeunite[0] = "";
        int index = 1;
        for (Unites unites : groupeunite) {
            Listeunite[index++] = unites.getUnite();
        }
        this.listeUnitesPossibles = Listeunite;
    }

    /**
     *
     * @param etapearacteristiques
     * @param dbEtapeCaracteristiques
     */
    private void updateBDEtapeCaracteristique(final MethodeProcedeEtapes mpe, int valeur, String unite, CaracteristiqueEtapes ce, String commentaire, final EtapeCaracterisitique dbEtapeCaracteristiques) throws BusinessException {
        try {
            dbEtapeCaracteristiques.setMethodeprocedeetapes(mpe);
            dbEtapeCaracteristiques.setCaracteristiqueetapes(ce);
            dbEtapeCaracteristiques.setEce_unite(unite);
            dbEtapeCaracteristiques.setEce_valeur(valeur);
            dbEtapeCaracteristiques.setCommentaire(commentaire);
            etapecaracteristiqueDAO.saveOrUpdate(dbEtapeCaracteristiques);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update etapecaracteristique");
        }

    }

    /**
     *
     */
    public void initMethodeProduitsProcedesEtapesPossibles() {
        Map<String, Map<String, Map<String, Map<String, Map<String, List<String>>>>>> methodeProcedesMap = new HashMap<String, Map<String, Map<String, Map<String, Map<String, List<String>>>>>>();
        List<MethodeProcedeEtapes> methodeProcedeEtapes = methodeprocedeetapeDAO.getAll();
        methodeProcedeEtapes.forEach((methodeProcedeEtape) -> {
            String codeProduit = methodeProcedeEtape.getMethodeprocedes().getProduits().getCodecomposant();
            String process_intitule = methodeProcedeEtape.getMethodeprocedes().getProcess().getProcess_intitule();
            int pmp_ordre = methodeProcedeEtape.getMethodeprocedes().getPmp_ordre();
            String intituleEtape = methodeProcedeEtape.getEtapes().getEtape_intitule();
            String me_code = methodeProcedeEtape.getMethodeetapes().getMe_code();
            int ordreEtape = methodeProcedeEtape.getOrdre();
            methodeProcedesMap
                    .computeIfAbsent(codeProduit, k -> new HashMap<String, Map<String, Map<String, Map<String, List<String>>>>>())
                    .computeIfAbsent(process_intitule, k -> new HashMap<String, Map<String, Map<String, List<String>>>>())
                    .computeIfAbsent(Integer.toString(pmp_ordre), k -> new HashMap<String, Map<String, List<String>>>())
                    .computeIfAbsent(intituleEtape, k -> new HashMap<String, List<String>>())
                    .computeIfAbsent(me_code, k -> new LinkedList<String>())
                    .add(Integer.toString(ordreEtape));
        });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeProcedePossibles);
        listOfMapOfValuesPossibles.add(listeOrdreProcedesPossibles);
        listOfMapOfValuesPossibles.add(listeEtapePossibles);
        listOfMapOfValuesPossibles.add(listeMethodEtapePossibles);
        listOfMapOfValuesPossibles.add(listeOrdresEtapePossibles);
        this.listeProduitsPossibles = readMapOfValuesPossibles(methodeProcedesMap, listOfMapOfValuesPossibles, new LinkedList<String>());
    }

    /**
     *
     * @param lineModelGridMetadata
     * @param methodeProcedeEtapes
     */
    public void initNewLine(LineModelGridMetadata lineModelGridMetadata, MethodeProcedeEtapes methodeProcedeEtapes) {
        final String codeProduit = methodeProcedeEtapes == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : methodeProcedeEtapes.getMethodeprocedes() != null
                ? methodeProcedeEtapes.getMethodeprocedes().getProduits().getProd_key()
                : "";
        ColumnModelGridMetadata produitColumn = new ColumnModelGridMetadata(codeProduit, listeProduitsPossibles, null, true, false, true);

        final String codeComposant = methodeProcedeEtapes == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : methodeProcedeEtapes.getMethodeprocedes() != null
                ? methodeProcedeEtapes.getMethodeprocedes().getProcess().getProcess_intitule()
                : "";
        ColumnModelGridMetadata processColumn = new ColumnModelGridMetadata(codeComposant, listeProcedePossibles, null, true, false, true);

        final String ordreProcede = methodeProcedeEtapes == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : Integer.toString(methodeProcedeEtapes.getMethodeprocedes().getPmp_ordre());
        ColumnModelGridMetadata orderProcedeColumn = new ColumnModelGridMetadata(ordreProcede, listeOrdreProcedesPossibles, null, true, false, true);

        final String codeEtape = methodeProcedeEtapes == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : methodeProcedeEtapes.getEtapes() != null
                ? methodeProcedeEtapes.getEtapes().getEtape_intitule()
                : "";
        ColumnModelGridMetadata etapeColumn = new ColumnModelGridMetadata(codeEtape, listeEtapePossibles, null, true, false, true);

        final String codeMethodeEtape = methodeProcedeEtapes == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : methodeProcedeEtapes.getMethodeetapes() != null
                ? methodeProcedeEtapes.getMethodeetapes().getMe_code()
                : "";
        ColumnModelGridMetadata methodeEtapeColumn = new ColumnModelGridMetadata(codeMethodeEtape, listeMethodEtapePossibles, null, true, false, true);

        final String codeOrdreEtape = methodeProcedeEtapes == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : methodeProcedeEtapes.getMethodeetapes() != null
                ? Integer.toString(methodeProcedeEtapes.getOrdre())
                : "";
        ColumnModelGridMetadata ordreEtapeColumn = new ColumnModelGridMetadata(codeOrdreEtape, listeOrdresEtapePossibles, null, true, false, true);
        Deque<ColumnModelGridMetadata> columnsToBeLinked = new LinkedList();
        columnsToBeLinked.add(produitColumn);
        columnsToBeLinked.add(processColumn);
        columnsToBeLinked.add(orderProcedeColumn);
        columnsToBeLinked.add(etapeColumn);
        columnsToBeLinked.add(methodeEtapeColumn);
        columnsToBeLinked.add(ordreEtapeColumn);
        setColumnRefRecursive(columnsToBeLinked);
        columnsToBeLinked.stream().forEach(c -> lineModelGridMetadata.getColumnsModelGridMetadatas().add(c));
    }
}
