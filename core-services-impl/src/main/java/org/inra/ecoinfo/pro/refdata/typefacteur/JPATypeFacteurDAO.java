/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typefacteur;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeFacteurDAO extends AbstractJPADAO<TypeFacteur> implements ITypeFacteurDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typefacteur.ITypeFacteurDAO#getByLibelle (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeFacteur> getByNKey(String libelle) {
        CriteriaQuery<TypeFacteur> query = builder.createQuery(TypeFacteur.class);
        Root<TypeFacteur> typeFacteur = query.from(TypeFacteur.class);
        query
                .select(typeFacteur)
                .where(
                        builder.equal(typeFacteur.get(TypeFacteur_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.typefacteur.ITypeFacteurDAO#getByCode(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<TypeFacteur> getByCode(String code) {
        CriteriaQuery<TypeFacteur> query = builder.createQuery(TypeFacteur.class);
        Root<TypeFacteur> typeFacteur = query.from(TypeFacteur.class);
        query
                .select(typeFacteur)
                .where(
                        builder.equal(typeFacteur.get(TypeFacteur_.code), code)
                );
        return getOptional(query);
    }

    @Override
    public List<TypeFacteur> getAll() {
        return getAllBy(TypeFacteur.class, TypeFacteur::getCode);
    }

}
