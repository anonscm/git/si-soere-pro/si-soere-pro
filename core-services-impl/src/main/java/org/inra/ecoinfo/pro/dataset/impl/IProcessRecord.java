/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.io.Serializable;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public interface IProcessRecord extends Serializable{
    
    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param fileEncoding
     * @param datasetDescriptorPRO
     * @throws BusinessException
     */
    void processRecord(CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String fileEncoding,
            DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException;
    
    /**
     *
     * @return
     * @throws IOException
     * @throws SAXException
     */
    DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException;

    /**
     *
     */
    static final String DATASET_DESCRIPTOR_XML = "dataset-descriptor.xml";


   
}
