/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.fluxchambres;

import java.time.LocalDate;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres_;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres_;
import org.inra.ecoinfo.pro.synthesis.fluxchambre.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.fluxchambre.SynthesisValue;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptcherniati
 */
public class SynthesisFluxChambresDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {

    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(SynthesisValue.class);
        Root<ValeurFluxChambres> v = query.from(ValeurFluxChambres.class);
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> varRn2 = node.join(NodeDataSet_.realNode);

        Join<ValeurFluxChambres, MesureFluxChambres> m = v.join(ValeurFluxChambres_.mesureFluxChambres);
        Join<ValeurFluxChambres, RealNode> varRn = v.join(ValeurFluxChambres_.realNode);
        final Join<RealNode, RealNode> themeRn = varRn.join(RealNode_.parent);
        final Join<RealNode, RealNode> datatypeRn = themeRn.join(RealNode_.parent);
        Join<RealNode, RealNode> parcelleRn = datatypeRn.join(RealNode_.parent);
        Join<RealNode, RealNode> dispositifRn = parcelleRn.join(RealNode_.parent);
        query.distinct(true);
        Path<Float> valuePath = v.get(ValeurFluxChambres_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = m.get(MesureFluxChambres_.LocalDate);
        final Path<String> dispositifPath = dispositifRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(builder.construct(
                        SynthesisValue.class,
                        dateMesure,
                        dispositifPath,
                        variableCode,
                        builder.avg(valuePath),
                        idNode
                    )
                )
                .where(builder.equal(varRn2, varRn),
                        builder.or(
                                builder.isNull(valuePath),
                                builder.gt(valuePath, -9999)
                        )
                )
                .groupBy(
                        dateMesure,
                        dispositifPath,
                        variableCode,
                        idNode
                )
                .orderBy(
                        builder.asc(dispositifPath),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

}
