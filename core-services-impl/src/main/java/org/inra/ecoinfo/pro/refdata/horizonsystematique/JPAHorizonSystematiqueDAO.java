/*
 *
 */
package org.inra.ecoinfo.pro.refdata.horizonsystematique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class JPAHorizonSystematiqueDAO extends AbstractJPADAO<HorizonSystematique> implements IHorizonSystematiqueDAO {

    /**
     *
     * @return
     */
    @Override
    public List<HorizonSystematique> getAll() {
        return getAll(HorizonSystematique.class);
    }

    /**
     *
     * @param horizon_systematique_nom
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<HorizonSystematique> getByNKey(String horizon_systematique_nom) {
        CriteriaQuery<HorizonSystematique> query = builder.createQuery(HorizonSystematique.class);
        Root<HorizonSystematique> horizonSystematique = query.from(HorizonSystematique.class);
        query
                .select(horizonSystematique)
                .where(
                        builder.equal(horizonSystematique.get(HorizonSystematique_.horizon_systematique_nom), horizon_systematique_nom)
                );
        return getOptional(query);
    }

}
