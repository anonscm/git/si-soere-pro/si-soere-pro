/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationPhysicoChimiePROMoyDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurPhysicoChimiePROMoy> deleteValeurs = builder.createCriteriaDelete(ValeurPhysicoChimiePROMoy.class);
        Root<ValeurPhysicoChimiePROMoy> valeur = deleteValeurs.from(ValeurPhysicoChimiePROMoy.class);
        Subquery<MesurePhysicoChimiePROMoy> subquery = deleteValeurs.subquery(MesurePhysicoChimiePROMoy.class);
        Root<MesurePhysicoChimiePROMoy> m = subquery.from(MesurePhysicoChimiePROMoy.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesurePhysicoChimiePROMoy_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurPhysicoChimiePROMoy_.mesurePhysicoChimiePROMoy).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesurePhysicoChimiePROMoy> deleteSequence = builder.createCriteriaDelete(MesurePhysicoChimiePROMoy.class);
        Root<MesurePhysicoChimiePROMoy> sequence = deleteSequence.from(MesurePhysicoChimiePROMoy.class);
        deleteSequence.where(builder.equal(sequence.get(MesurePhysicoChimiePROMoy_.versionfile), version));
        delete(deleteSequence);
    }
}
