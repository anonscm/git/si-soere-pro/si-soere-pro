/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.flux_chambre.impl;

import org.inra.ecoinfo.pro.dataset.impl.AbstractSessionPropertiesPRO;

/**
 *
 * @author vjkoyao
 */
public abstract class AbstractFluxChambreSessionProperties extends AbstractSessionPropertiesPRO implements ISessionPropertiesFluxChambre {

    /**
     *
     */
    public AbstractFluxChambreSessionProperties() {
        super();
    }

}
