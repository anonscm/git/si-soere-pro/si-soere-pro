/*
 *
 */
package org.inra.ecoinfo.pro.refdata.grandtypeproduit;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits;

// TODO: Auto-generated Javadoc
/**
 * The Interface IGrandTypeProduitDAO.
 */
public interface IGrandTypeProduitDAO extends IDAO<GrandTypeProduits> {

    /**
     *
     * @return
     */
    List<GrandTypeProduits> getAll();

    /**
     *
     * @param gtp_nom
     * @return
     */
    Optional<GrandTypeProduits> getByNKey(String gtp_nom);

}
