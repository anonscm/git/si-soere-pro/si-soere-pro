/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.categorievariable;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface ICategorieVariableDAO extends IDAO<CategorieVariable>{
    
    /**
     *
     * @return
     */
    List<CategorieVariable> gettAll();

    /**
     *
     * @param cvariable
     * @return
     */
    Optional<CategorieVariable> getByName(String cvariable);

    /**
     *
     * @param codecvariable
     * @return
     */
    Optional<CategorieVariable> getByNKey(String codecvariable);

    /**
     *
     * @param mycode
     * @return
     */
    Optional<CategorieVariable> getByMyKey(String mycode);
    
    
}
