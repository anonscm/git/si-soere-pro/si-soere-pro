/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contextescultures;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.typeculture.TypeCulture;

/**
 *
 * @author adiankha
 */
public class JPAContextesCulturesDAO extends AbstractJPADAO<ContextesCultures> implements IContextesCulturesDAO {

    private static final String QUERY_CULTURE_PERENNE = "from ContextesCultures cp where cp.dispositif.code =:code and cp.dispositif.lieu.nom=:lieu and cp.typeculture.libelle=:libelle and cp.cp_densite=:densite and cp.cc_anneedebut=:anneed";

    /**
     *
     * @return
     */
    @Override
    public List<ContextesCultures> getAll() {
        return getAllBy(ContextesCultures.class, ContextesCultures_.dispositif);
    }

    /**
     *
     * @param dispositif
     * @param typeculture
     * @param densite
     * @param anneed
     * @return
     */
    @Override
    public Optional<ContextesCultures> getByNKey(Dispositif dispositif, TypeCulture typeculture, Integer densite, Integer anneed) {
        CriteriaQuery<ContextesCultures> query = builder.createQuery(ContextesCultures.class);
        Root<ContextesCultures> contextesCultures = query.from(ContextesCultures.class);
        Join<ContextesCultures, Dispositif> disp = contextesCultures.join(ContextesCultures_.dispositif);
        Join<ContextesCultures, TypeCulture> tc = contextesCultures.join(ContextesCultures_.typeculture);
        query
                .select(contextesCultures)
                .where(
                        builder.equal(contextesCultures.get(ContextesCultures_.cp_densite), densite),
                        builder.equal(contextesCultures.get(ContextesCultures_.cc_anneedebut), densite),
                        builder.equal(disp, dispositif),
                        builder.equal(tc, typeculture)
                );
        return getOptional(query);
    }

}
