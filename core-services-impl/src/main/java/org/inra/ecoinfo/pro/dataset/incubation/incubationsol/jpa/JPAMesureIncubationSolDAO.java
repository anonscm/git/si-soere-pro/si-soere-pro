/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsol.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsol.IMesureIncubationSolDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAMesureIncubationSolDAO extends AbstractJPADAO<MesureIncubationSol> implements IMesureIncubationSolDAO<MesureIncubationSol> {

    /**
     *
     * @param key
     * @return
     */
    @Override
    public Optional<MesureIncubationSol> getByKey(String key) {
        CriteriaQuery<MesureIncubationSol> query = builder.createQuery(MesureIncubationSol.class);
        Root<MesureIncubationSol> m = query.from(MesureIncubationSol.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureIncubationSol_.keymesure), key)
                );
        return getOptional(query);
    }

}
