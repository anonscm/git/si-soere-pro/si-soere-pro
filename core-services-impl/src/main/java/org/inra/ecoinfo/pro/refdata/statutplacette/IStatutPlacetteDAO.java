/**
 *
 */
package org.inra.ecoinfo.pro.refdata.statutplacette;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IStatutPlacetteDAO extends IDAO<StatutPlacette> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<StatutPlacette> getByNKey(String libelle);

}
