/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpro;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class PrecisionProDAOImpl extends AbstractJPADAO<Precisionpro> implements IPrecisionProDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Precisionpro> getAll() {
        return getAll(Precisionpro.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Precisionpro> getByNKey(String nom) {
        CriteriaQuery<Precisionpro> query = builder.createQuery(Precisionpro.class);
        Root<Precisionpro> precisionpro = query.from(Precisionpro.class);
        query
                .select(precisionpro)
                .where(
                        builder.equal(precisionpro.get(Precisionpro_.p_nom), nom)
                );
        return getOptional(query);
    }

}
