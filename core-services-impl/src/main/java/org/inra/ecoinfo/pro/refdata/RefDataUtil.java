/**
 *
 */
package org.inra.ecoinfo.pro.refdata;

import java.util.regex.Pattern;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author sophie
 *
 */
public class RefDataUtil {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    protected static final String BAD_VALUE = "-9999";
    protected static final String EMPTY_VALUE = "-99999";
    protected static final String PROPERTY_MSG_INVALID_INT_VALUE = "PROPERTY_MSG_INVALID_INT_VALUE";
    protected static final String PROPERTY_MSG_INVALID_FLOAT_VALUE = "PROPERTY_MSG_INVALID_FLOAT_VALUE";
    protected static final String PROPERTY_MSG_INVALID_DOUBLE_VALUE = "PROPERTY_MSG_INVALID_DOUBLE_VALUE";
    protected static final String EMAIL_BADFORMAT = "EMAIL_BADFORMAT";
    protected static final String NUMEROTEL_BADFORMAT = "NUMEROTEL_BADFORMAT";

    /**
     * @param errorsReport
     * @throws BusinessException
     */
    @SuppressWarnings("rawtypes")
    public static void gestionErreurs(ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    /**
     * @param eMail
     * @param errorsReport
     * @param localizationManager
     * @throws BusinessException
     */
    @SuppressWarnings("rawtypes")
    public static void verifieEmail(String eMail, ErrorsReport errorsReport, ILocalizationManager localizationManager) throws BusinessException {
        String regex = Constantes.REGEX_EMAIL;

        if (eMail != null && !eMail.isEmpty()) {
            boolean emailOk = Pattern.matches(regex, eMail);

            if (!emailOk) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(RefDataUtil.BUNDLE_SOURCE_PATH, EMAIL_BADFORMAT), eMail));
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        }
    }

    /**
     * @param noTel
     * @param noFax
     * @param errorsReport
     * @param localizationManager
     * @throws BusinessException
     */
    @SuppressWarnings("rawtypes")
    public static void verifieNotelNoFax(String noTel, String noFax, ErrorsReport errorsReport, ILocalizationManager localizationManager) throws BusinessException {
        String regex = Constantes.REGEX_TEL_FAX;

        if (noTel != null && !noTel.isEmpty()) {
            boolean telOk = Pattern.matches(regex, noTel);
            if (!telOk) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(RefDataUtil.BUNDLE_SOURCE_PATH, NUMEROTEL_BADFORMAT), noTel));
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        }

        /* if (noFax != null && !noFax.isEmpty()) 
        {
            boolean faxOk = Pattern.matches(regex, noFax);
            if (!faxOk) 
            {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(RefDataUtil.BUNDLE_SOURCE_PATH, "NUMEROFAX_BADFORMAT"), noFax));
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        }*/
    }

}
