/**
 *
 */
package org.inra.ecoinfo.pro.refdata.bloc;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.codebloc.CodeBloc;
import org.inra.ecoinfo.pro.refdata.codebloc.ICodeBlocDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Bloc> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    ICodeBlocDAO codeBlocDAO;
    IBlocDAO blocDAO;
    private String[] dispositifPossibles;
    private String[] codeBlocPossibles;

    /**
     * @param dispositif
     * @param nomBloc
     * @param codeBloc
     * @param dbBloc
     * @param bloc
     * @throws PersistenceException
     */
    private void createBloc(final Bloc bloc) throws BusinessException {
        try {
            blocDAO.saveOrUpdate(bloc);
            blocDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create bloc");
        }
    }

    private void updateBDBloc(Dispositif dispositif, String nomBloc, CodeBloc codeBloc, Bloc dbBloc) throws BusinessException {
        try {
            dbBloc.setCode(codeBloc);
            dbBloc.setDispositif(dispositif);
            dbBloc.setNom(nomBloc);
            blocDAO.saveOrUpdate(dbBloc);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update bloc");
        }

    }

    private void createOrUpdate(Dispositif dispositif, String nomBloc, CodeBloc codeBloc, Bloc dbBloc) throws BusinessException {
        if (dbBloc == null) {
            Bloc bloc = new Bloc(dispositif, nomBloc, codeBloc);
            bloc.setCode(codeBloc);
            bloc.setDispositif(dispositif);
            bloc.setNom(nomBloc);
            createBloc(bloc);
        } else {
            updateBDBloc(dispositif, nomBloc, codeBloc, dbBloc);
        }
    }

    private void persistBloc(Dispositif dispositif, String nomBloc, CodeBloc codeBloc) throws BusinessException {
        final Bloc dbbloc = blocDAO.getByNKey(nomBloc, dispositif).orElse(null);
        createOrUpdate(dispositif, nomBloc, codeBloc, dbbloc);
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getCodeBlocPossibles() {
        List<CodeBloc> lstCodeBlocs = codeBlocDAO.getAll();
        String[] codeBlocPossibles = new String[lstCodeBlocs.size()];
        int index = 0;
        for (CodeBloc codeBloc : lstCodeBlocs) {
            codeBlocPossibles[index++] = codeBloc.getCode();
        }
        return codeBlocPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getDispositifPossibles() {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll();
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) {
            dispositifPossibles[index++] = dispositif.getNomDispositif_nomLieu();
        }
        return dispositifPossibles;
    }

    @Override
    protected ModelGridMetadata<Bloc> initModelGridMetadata() {
        this.dispositifPossibles = getDispositifPossibles();
        this.codeBlocPossibles = getCodeBlocPossibles();
        return super.initModelGridMetadata(); 
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomBloc = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                Bloc bloc = blocDAO.getByNKey(nomBloc, dispositif).orElseThrow(PersistenceException::new);

                blocDAO.remove(bloc);

                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Bloc> getAllElements() throws BusinessException {
        return blocDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Bloc bloc) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(bloc == null ? Constantes.STRING_EMPTY : bloc.getDispositif().getNomDispositif_nomLieu(), dispositifPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(bloc == null ? Constantes.STRING_EMPTY : bloc.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(bloc == null ? Constantes.STRING_EMPTY : bloc.getCode().getCode(), codeBlocPossibles, null, false, false, true));

        return lineModelGridMetadata;
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexdis = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomBloc = tokenizerValues.nextToken();
                int indexcode = tokenizerValues.currentTokenIndex();
                String code = tokenizerValues.nextToken();

                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                if (lieu == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line + 1, indexdis + 1, nomLieu));
                }

                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                if (dispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line + 1, indexdis + 1, codeDispositif));
                }

                CodeBloc codeBloc = codeBlocDAO.getByNKey(code).orElse(null);
                if (codeBloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "CODEBLOC_NONDEFINI"), line + 1, indexcode + 1, code));
                }

                if (!errorsReport.hasErrors()) {
                    persistBloc(dispositif, nomBloc, codeBloc);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param codeBlocDAO the codeBlocDAO to set
     */
    public void setCodeBlocDAO(ICodeBlocDAO codeBlocDAO) {
        this.codeBlocDAO = codeBlocDAO;
    }

    /**
     * @param blocDAO the blocDAO to set
     */
    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

}
