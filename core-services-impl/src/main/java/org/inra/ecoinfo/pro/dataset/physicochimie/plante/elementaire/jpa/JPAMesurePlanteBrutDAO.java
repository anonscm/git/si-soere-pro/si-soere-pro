/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.IMesurePlanteBrutDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire_;

/**
 *
 * @author adiankha
 */
public class JPAMesurePlanteBrutDAO extends AbstractJPADAO<MesurePlanteElementaire> implements IMesurePlanteBrutDAO<MesurePlanteElementaire> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesurePlanteElementaire> getByKeys(String keymesure) {
        CriteriaQuery<MesurePlanteElementaire> query = builder.createQuery(MesurePlanteElementaire.class);
        Root<MesurePlanteElementaire> m = query.from(MesurePlanteElementaire.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesurePlanteElementaire_.keymesure), keymesure)
                );
        return getOptional(query);
    }

}
