/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.elementaire.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteBrutDataTypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire_;
import org.inra.ecoinfo.pro.extraction.physicochimie.jpa.JPAPhysicoChimieDAO;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantebrut.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPAPlanteBrutDAO extends JPAPhysicoChimieDAO<MesurePlanteElementaire, ValeurPlanteElementaire> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurPlanteElementaire> getValeurPhysicoChimieClass() {
        return ValeurPlanteElementaire.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesurePlanteElementaire> getMesurePhysicoChimieClass() {
        return MesurePlanteElementaire.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurPlanteElementaire, MesurePlanteElementaire> getMesureAttribute() {
        return ValeurPlanteElementaire_.mesurePlanteElementaire;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IPhysicoChimiePlanteBrutDataTypeManager.CODE_DATATYPE_PLANTE_BRUT;
    }
}
