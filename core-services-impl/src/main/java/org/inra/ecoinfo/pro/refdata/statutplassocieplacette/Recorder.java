/**
 *
 */
package org.inra.ecoinfo.pro.refdata.statutplassocieplacette;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.ICodeParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.statutplacette.IStatutPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.statutplacette.StatutPlacette;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<StatutPlacettesAssociesPlacette> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    ICodeParcelleElementaireDAO codeParcelleElementaireDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IPlacetteDAO placetteDAO;
    IStatutPlacetteDAO statutPlacetteDAO;
    IStatutPlAssociesPlacetteDAO statutPlAssociesPlacetteDAO;
    private String[] listeParcellePossible;
    private ConcurrentMap<String, String[]> listePlacettesPossibles = new ConcurrentHashMap<>();
    private String[] listeStatutPlacettePossible;

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexPEDispLieu = tokenizerValues.currentTokenIndex();
                String codePEDispositif = tokenizerValues.nextToken();
                int indexCodePl = tokenizerValues.currentTokenIndex();
                String codePlacette = tokenizerValues.nextToken();
                int indexLibelleStPl = tokenizerValues.currentTokenIndex();
                String libelleStatutPl = tokenizerValues.nextToken();

                String codePE = ParcelleElementaire.getCodeParcelle(codePEDispositif);
                String codeDispositif = ParcelleElementaire.getNomDispositif(codePEDispositif);
                String nomLieu = ParcelleElementaire.getNomLieu(codePEDispositif);

                Lieu lieu = verifieLieu(nomLieu, line + 1, indexPEDispLieu + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line + 1, indexPEDispLieu + 1, errorsReport);
                CodeParcelleElementaire codeParcelleElementaire = verifieCodeParcelleElementaire(codePE, line + 1, indexPEDispLieu + 1, errorsReport);
                ParcelleElementaire parcelleElementaire = verifieParcelleElementaire(codePE, codeDispositif, codeParcelleElementaire, dispositif, line + 1, indexPEDispLieu + 1, errorsReport);
                verifieCodePlacette(codePlacette, line + 1, indexCodePl + 1, errorsReport);
                Placette placette = verifiePlacette(codePlacette, parcelleElementaire, dispositif, line, indexCodePl, errorsReport);
                StatutPlacette statutPlacette = verifieStatutPlacette(libelleStatutPl, codePE, codeDispositif, lieu, line + 1, indexLibelleStPl + 1, errorsReport);

                StatutPlacettesAssociesPlacette statutPlAssocPlacette = new StatutPlacettesAssociesPlacette(statutPlacette, placette);
                StatutPlacettesAssociesPlacette dbStatutPlacettesAssociesPlacette = statutPlAssociesPlacetteDAO.getByNKey(placette, statutPlacette).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbStatutPlacettesAssociesPlacette == null) {
                        statutPlAssociesPlacetteDAO.saveOrUpdate(statutPlAssocPlacette);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }

        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }

        return dispositif;
    }

    /**
     * @param codePE
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private CodeParcelleElementaire verifieCodeParcelleElementaire(String codePE, long line, int index, ErrorsReport errorsReport) {
        CodeParcelleElementaire codeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(codePE).orElse(null);
        if (codeParcelleElementaire == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPARCELLEELT_NONDEFINI"), line, index, codePE));
        }

        return codeParcelleElementaire;
    }

    /**
     * @param codePE
     * @param codeDispositif
     * @param codeParcelleElementaire
     * @param dispositif
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private ParcelleElementaire verifieParcelleElementaire(String codePE, String codeDispositif, CodeParcelleElementaire codeParcelleElementaire, Dispositif dispositif, long line, int index, ErrorsReport errorsReport) {
        ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByNKey(codeParcelleElementaire, dispositif).orElse(null);
        if (parcelleElementaire == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PRCELT_NONDEFINI"), line, index, codePE, codeDispositif));
        }

        return parcelleElementaire;
    }

    /**
     * @param codePlacette
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieCodePlacette(String codePlacette, long line, int index, ErrorsReport errorsReport) {
        if (codePlacette == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line, index, datasetDescriptor.getColumns().get(2).getName()));
        }

        try {
            Integer codePlt = Integer.parseInt(codePlacette);
            if (codePlt < 1 && codePlt > 30) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPLACETTE_BADFORMAT"), line, index, codePlacette));
            }
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPLACETTE_BADFORMAT"), line, index, codePlacette));
        }
    }

    /**
     * @param codePlacette
     * @param parcelleElementaire
     * @param dispositif
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Placette verifiePlacette(String codePlacette, ParcelleElementaire parcelleElementaire, Dispositif dispositif, long line, int index, ErrorsReport errorsReport) {
        Placette placette = placetteDAO.getByNKey(codePlacette, parcelleElementaire).orElse(null);

        if (placette == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PLACETTE_NONDEFINI"), line, index, codePlacette, parcelleElementaire.getCodeParcelleElementaire().getCode(), dispositif.getCode() + "(" + dispositif.getLieu().getNom() + ")"));
        }

        return placette;
    }

    /**
     * @param statut
     * @param codePE
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private StatutPlacette verifieStatutPlacette(String statut, String codePE, String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        StatutPlacette statutPlacette = statutPlacetteDAO.getByNKey(statut).orElse(null);
        if (statutPlacette == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "STATUTPLACETTE_NONDEFINI"), line, index, statut, codePE, codeDispositif));
        }

        return statutPlacette;

    }

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexPEDispLieu = tokenizerValues.currentTokenIndex();
                String codePEDispositif = tokenizerValues.nextToken();
                int indexCodePl = tokenizerValues.currentTokenIndex();
                String codePlacette = tokenizerValues.nextToken();
                int indexLibelleStPl = tokenizerValues.currentTokenIndex();
                String libelleStatutPl = tokenizerValues.nextToken();

                String codePE = ParcelleElementaire.getCodeParcelle(codePEDispositif);
                String codeDispositif = ParcelleElementaire.getNomDispositif(codePEDispositif);
                String nomLieu = ParcelleElementaire.getNomLieu(codePEDispositif);

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                CodeParcelleElementaire codeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(codePE).orElse(null);
                ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByNKey(codeParcelleElementaire, dispositif).orElse(null);
                Placette placette = placetteDAO.getByNameAndParcelle(codePlacette, parcelleElementaire).orElse(null);
                StatutPlacette statutPlacette = statutPlacetteDAO.getByNKey(libelleStatutPl).orElse(null);

                StatutPlacettesAssociesPlacette statutPlacettesAssociesPlacette = statutPlAssociesPlacetteDAO.getByNKey(placette, statutPlacette).orElse(null);
                statutPlAssociesPlacetteDAO.remove(statutPlacettesAssociesPlacette);

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    public void initNewLine(LineModelGridMetadata lineModelGridMetadata, StatutPlacettesAssociesPlacette statutPlacettesAssociesPlacette) {
        ColumnModelGridMetadata plotColumn = new ColumnModelGridMetadata(
                Optional.ofNullable(statutPlacettesAssociesPlacette)
                        .map(spa->spa.getPlacette())
                        .map(pl->pl.getParcelleElementaire())
                        .map(pe->pe.getCodeParcelleNomDispositif_nomLieu())
                        .orElse(Constantes.STRING_EMPTY), 
                listeParcellePossible, null, true, false, true);

        //Code de la placette
        String codePlacette = statutPlacettesAssociesPlacette == null ? ""
                : (statutPlacettesAssociesPlacette.getPlacette() == null ? ""
                : (statutPlacettesAssociesPlacette.getPlacette().getCode()));

        ColumnModelGridMetadata spotColumn = new ColumnModelGridMetadata(statutPlacettesAssociesPlacette == null ? Constantes.STRING_EMPTY : codePlacette,
                listePlacettesPossibles, null, true, false, true);
        Deque<ColumnModelGridMetadata> columnsToBeLinked= new LinkedList();
        columnsToBeLinked.add(plotColumn);
        columnsToBeLinked.add(spotColumn);
        setColumnRefRecursive(columnsToBeLinked);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(plotColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(spotColumn);
    }

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StatutPlacettesAssociesPlacette statutPlacettesAssociesPlacette) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        initNewLine(lineModelGridMetadata, statutPlacettesAssociesPlacette);

        // Libellé du statut
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(statutPlacettesAssociesPlacette == null ? Constantes.STRING_EMPTY
                : (statutPlacettesAssociesPlacette.getPlacette() == null ? Constantes.STRING_EMPTY
                : (statutPlacettesAssociesPlacette.getStatutPlacette() == null ? Constantes.STRING_EMPTY
                : (statutPlacettesAssociesPlacette.getStatutPlacette().getLibelle()))), listeStatutPlacettePossible, null, true, false, true));

        return lineModelGridMetadata;
    }

    private void initPlacettesBlocPossibles() {
        Map<String, List<String>> parcellePlacetteMap = new HashMap<String, List<String>>();
        placetteDAO.getAll(Placette.class)
                .stream()
                .forEach(placette -> parcellePlacetteMap
                    .computeIfAbsent(placette.getParcelleElementaire().getCodeParcelleNomDispositif_nomLieu(), k -> new LinkedList<>())
                .add(placette.getCode())
                );
        LinkedList<ConcurrentMap
                <String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listePlacettesPossibles);
        this.listeParcellePossible = readMapOfValuesPossibles(parcellePlacetteMap, listOfMapOfValuesPossibles, new LinkedList<String>());
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getStatutPlacettePossibles() {
        List<StatutPlacette> lstStatutPlacettes = statutPlacetteDAO.getAll(StatutPlacette.class);
        String[] statutPlacettePossibles = new String[lstStatutPlacettes.size()];
        int index = 0;
        for (StatutPlacette statutPlacette : lstStatutPlacettes) {
            statutPlacettePossibles[index++] = statutPlacette.getLibelle();
        }
        return statutPlacettePossibles;
    }

    @Override
    protected ModelGridMetadata<StatutPlacettesAssociesPlacette> initModelGridMetadata() {
        initPlacettesBlocPossibles();
        listeStatutPlacettePossible = getStatutPlacettePossibles();
        return super.initModelGridMetadata();
    }

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<StatutPlacettesAssociesPlacette> getAllElements() throws BusinessException {
        return statutPlAssociesPlacetteDAO.getAll(StatutPlacettesAssociesPlacette.class);
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param codeParcelleElementaireDAO the codeParcelleElementaireDAO to set
     */
    public void setCodeParcelleElementaireDAO(
            ICodeParcelleElementaireDAO codeParcelleElementaireDAO) {
        this.codeParcelleElementaireDAO = codeParcelleElementaireDAO;
    }

    /**
     * @param parcelleElementaireDAO the parcelleElementaireDAO to set
     */
    public void setParcelleElementaireDAO(
            IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     * @param placetteDAO the placetteDAO to set
     */
    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    /**
     * @param statutPlacetteDAO the statutPlacetteDAO to set
     */
    public void setStatutPlacetteDAO(IStatutPlacetteDAO statutPlacetteDAO) {
        this.statutPlacetteDAO = statutPlacetteDAO;
    }

    /**
     * @param statutPlAssociesPlacetteDAO the statutPlAssociesPlacetteDAO to set
     */
    public void setStatutPlAssociesPlacetteDAO(
            IStatutPlAssociesPlacetteDAO statutPlAssociesPlacetteDAO) {
        this.statutPlAssociesPlacetteDAO = statutPlAssociesPlacetteDAO;
    }

}
