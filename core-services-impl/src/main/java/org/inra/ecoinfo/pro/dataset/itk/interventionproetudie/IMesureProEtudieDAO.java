/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;

/**
 *
 * @author adiankha
 * @param <T>
 */
public interface IMesureProEtudieDAO<T> extends IDAO<MesureProEtudie> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesureProEtudie> getByKeys(String keymesure);
}
