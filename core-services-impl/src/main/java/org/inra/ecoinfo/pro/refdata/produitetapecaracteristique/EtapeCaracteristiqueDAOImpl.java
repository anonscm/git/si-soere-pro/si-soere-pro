package org.inra.ecoinfo.pro.refdata.produitetapecaracteristique;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.caracteristiqueetape.CaracteristiqueEtapes;
import org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape.MethodeProcedeEtapes;

/**
 *
 * @author ptcherniati
 */
public class EtapeCaracteristiqueDAOImpl extends AbstractJPADAO<EtapeCaracterisitique> implements IEtapeCaracteristiqueDAO {

    /**
     *
     * @return
     */
    @Override
    public List<EtapeCaracterisitique> getAll() {
        return getAll(EtapeCaracterisitique.class);
    }

    /**
     *
     * @param mpe
     * @param caracteristiqueetapes
     * @param valeur
     * @param unite
     * @return
     */
    @Override
    public Optional<EtapeCaracterisitique> getByNKey(MethodeProcedeEtapes mpe, CaracteristiqueEtapes caracteristiqueetapes, int valeur, String unite) {
        CriteriaQuery<EtapeCaracterisitique> query = builder.createQuery(EtapeCaracterisitique.class);
        Root<EtapeCaracterisitique> etapeCaracterisitique = query.from(EtapeCaracterisitique.class);
        Join<EtapeCaracterisitique, MethodeProcedeEtapes> dbmpe = etapeCaracterisitique.join(EtapeCaracterisitique_.methodeprocedeetapes);
        Join<EtapeCaracterisitique, CaracteristiqueEtapes> ce = etapeCaracterisitique.join(EtapeCaracterisitique_.caracteristiqueetapes);
        query
                .select(etapeCaracterisitique)
                .where(
                        builder.equal(etapeCaracterisitique.get(EtapeCaracterisitique_.ece_unite), unite),
                        builder.equal(etapeCaracterisitique.get(EtapeCaracterisitique_.ece_valeur), valeur),
                        builder.equal(dbmpe, mpe),
                        builder.equal(ce, caracteristiqueetapes)
                );
        return getOptional(query);
    }

}
