/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols_;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationPhysicoChimieDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurPhysicoChimieSols> deleteValeurs = builder.createCriteriaDelete(ValeurPhysicoChimieSols.class);
        Root<ValeurPhysicoChimieSols> valeur = deleteValeurs.from(ValeurPhysicoChimieSols.class);
        Subquery<MesurePhysicoChimieSols> subquery = deleteValeurs.subquery(MesurePhysicoChimieSols.class);
        Root<MesurePhysicoChimieSols> m = subquery.from(MesurePhysicoChimieSols.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesurePhysicoChimieSols_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurPhysicoChimieSols_.mesurePhysicoChimieSols).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesurePhysicoChimieSols> deleteSequence = builder.createCriteriaDelete(MesurePhysicoChimieSols.class);
        Root<MesurePhysicoChimieSols> sequence = deleteSequence.from(MesurePhysicoChimieSols.class);
        deleteSequence.where(builder.equal(sequence.get(MesurePhysicoChimieSols_.versionfile), version));
        delete(deleteSequence);
    }

}
