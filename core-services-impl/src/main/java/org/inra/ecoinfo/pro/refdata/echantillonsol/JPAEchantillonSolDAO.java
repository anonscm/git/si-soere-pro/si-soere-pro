/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsol;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol_;
import org.inra.ecoinfo.pro.refdata.prelevementsol.PrelevementSol;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha Vivianne
 */
public class JPAEchantillonSolDAO extends AbstractJPADAO<EchantillonsSol> implements IEchantillonSolDAO {

    /**
     *
     * @return
     */
    @Override
    public List<EchantillonsSol> getAll() {
        return getAllBy(EchantillonsSol.class, EchantillonsSol::getCodeechsol);
    }

    /**
     *
     * @param baseEchantillon
     * @param prelevementSol
     * @return
     */
    @Override
    public Optional<EchantillonsSol> getByNKey(Long baseEchantillon, PrelevementSol prelevementSol) {
        CriteriaQuery<EchantillonsSol> query = builder.createQuery(EchantillonsSol.class);
        Root<EchantillonsSol> echantillonsSol = query.from(EchantillonsSol.class);
        Join<EchantillonsSol, PrelevementSol> prelevement = echantillonsSol.join(EchantillonsSol_.prelevementsol);
        query
                .select(echantillonsSol)
                .where(
                        builder.equal(echantillonsSol.get(EchantillonsSol_.codebaseechantillon), baseEchantillon),
                        builder.equal(prelevement, prelevementSol)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeechantillonSol
     * @return
     */
    @Override
    public Optional<EchantillonsSol> getByNKey(String codeechantillonSol) {
        CriteriaQuery<EchantillonsSol> query = builder.createQuery(EchantillonsSol.class);
        Root<EchantillonsSol> echantillonsSol = query.from(EchantillonsSol.class);
        Join<EchantillonsSol, PrelevementSol> prelevement = echantillonsSol.join(EchantillonsSol_.prelevementsol);
        query
                .select(echantillonsSol)
                .where(
                        builder.equal(echantillonsSol.get(EchantillonsSol_.codeesol), Utils.createCodeFromString(codeechantillonSol))
                );
        return getOptional(query);
    }
}
