package org.inra.ecoinfo.pro.refdata.substratpedologique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface ISubstratPedologiqueDAO extends IDAO<Substratpedologique> {

    /**
     *
     * @return
     */
    List<Substratpedologique> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Substratpedologique> getByNKey(String nom);
}
