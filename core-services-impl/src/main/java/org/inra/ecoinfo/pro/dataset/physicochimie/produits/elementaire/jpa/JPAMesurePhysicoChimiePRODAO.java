/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.jpa;

import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.IMesurePhysicoChimiePRODAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO_;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit_;

/**
 *
 * @author vjkoyao
 */
public class JPAMesurePhysicoChimiePRODAO extends AbstractJPADAO<MesurePhysicoChimiePRO> implements IMesurePhysicoChimiePRODAO<MesurePhysicoChimiePRO> {

    @Override
    public Optional<MesurePhysicoChimiePRO> getByNKeys(String code_unique_echantillon, String nomlabo, LocalDate dateprelevement) {
        CriteriaQuery<MesurePhysicoChimiePRO> query = builder.createQuery(MesurePhysicoChimiePRO.class);
        Root<MesurePhysicoChimiePRO> m = query.from(MesurePhysicoChimiePRO.class);
        Join<MesurePhysicoChimiePRO, EchantillonsProduit> echantillon = m.join(MesurePhysicoChimiePRO_.echantillonpro);
        query
                .select(m)
                .where(
                        builder.equal(echantillon.get(EchantillonsProduit_.code_unique), code_unique_echantillon),
                        builder.equal(m.get(MesurePhysicoChimiePRO_.localDatePrelevement), dateprelevement),
                        builder.equal(m.get(MesurePhysicoChimiePRO_.nom_laboratoire), nomlabo)
                );
        return getOptional(query);
    }

    @Override
    public Optional<MesurePhysicoChimiePRO> getByKeys(String keymesure) {
        CriteriaQuery<MesurePhysicoChimiePRO> query = builder.createQuery(MesurePhysicoChimiePRO.class);
        Root<MesurePhysicoChimiePRO> m = query.from(MesurePhysicoChimiePRO.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesurePhysicoChimiePRO_.keymesure), keymesure)
                );
        return getOptional(query);
    }

}
