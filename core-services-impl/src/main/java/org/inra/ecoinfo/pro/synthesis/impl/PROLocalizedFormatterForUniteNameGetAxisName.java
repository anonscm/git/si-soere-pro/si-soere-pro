/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

/**
 *
 * @author tcherniatinsky
 */
class PROLocalizedFormatterForUniteNameGetAxisName implements ILocalizedFormatter<DatatypeVariableUnitePRO> {

    ILocalizationManager localizationManager;

    public PROLocalizedFormatterForUniteNameGetAxisName() {
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    @Override
    public String format(DatatypeVariableUnitePRO dvu, Locale locale, Object... arguments) {
        return Optional.ofNullable(dvu).map(nodeabledvu -> getVariableAxixName(nodeabledvu, locale))
                .orElse("Error while retrieving variable definition)");
    }

    private String getVariableAxixName(DatatypeVariableUnitePRO dvu, Locale locale) {
        Variable variable = dvu.getVariablespro();
        Unite unite = dvu.getUnitepro();
        final Properties propertiesVariablesName = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariablesPRO.class), Nodeable.ENTITE_COLUMN_NAME, locale);
        final Properties propertiesUnitDisplay = localizationManager.newProperties(Unite.NAME_ENTITY_JPA, "nom", locale);
        String unitName = propertiesUnitDisplay.getProperty(unite.getName(), unite.getName());
        String variableName = propertiesVariablesName.getProperty(variable.getName(), variable.getName());
        String variableAffichage = variable.getAffichage();
        return String.format("<b>%s</b><br /> (%s - <i>%s</i>)", variableName, variableAffichage, unitName);
    }

}
