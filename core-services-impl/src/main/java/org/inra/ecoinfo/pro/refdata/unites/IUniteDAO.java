package org.inra.ecoinfo.pro.refdata.unites;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IUniteDAO extends IDAO<Unites> {

    /**
     *
     * @return
     */
    List<Unites> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Unites> getByunite(String nom);

    /**
     *
     * @param code
     * @return
     */
    Optional<Unites> getByNKey(String code);

}
