/**
 *
 */
package org.inra.ecoinfo.pro.refdata.thematiqueetudiee;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAThematiqueEtudieeDAO extends AbstractJPADAO<ThematiqueEtudiee> implements IThematiqueEtudieeDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.thematiqueetudiee.IThematiqueEtudieeDAO# getByLibelle(java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<ThematiqueEtudiee> getByNKey(String libelle) {
        CriteriaQuery<ThematiqueEtudiee> query = builder.createQuery(ThematiqueEtudiee.class);
        Root<ThematiqueEtudiee> thematiqueEtudiee = query.from(ThematiqueEtudiee.class);
        query
                .select(thematiqueEtudiee)
                .where(
                        builder.equal(thematiqueEtudiee.get(ThematiqueEtudiee_.libelle), libelle)
                );
        return getOptional(query);
    }

}
