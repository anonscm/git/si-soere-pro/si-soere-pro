/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;

/**
 *
 * @author adiankha
 */
public interface IValeurProduitBrutDAO extends IDAO<ValeurPhysicoChimiePRO> {

    /**
     *
     * @param dvumRealNode
     * @param mesureSol
     * @param statut
     * @return
     */
    Optional<ValeurPhysicoChimiePRO> getByNKeys(RealNode dvumRealNode, MesurePhysicoChimiePRO mesureSol, String statut);

}
