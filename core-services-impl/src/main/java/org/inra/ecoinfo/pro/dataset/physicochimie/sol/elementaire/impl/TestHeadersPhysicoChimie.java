/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.ISessionProportiesPhysicoChimie;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolDatatypeManager;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class TestHeadersPhysicoChimie extends GenericTestHeader{
    private static final Logger LOGGER = LoggerFactory.getLogger(TestHeadersPhysicoChimie.class);

    public TestHeadersPhysicoChimie() {
        super();
    }

    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile, 
            ISessionPropertiesPRO sessionProperties, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor) throws BusinessException {
       super.testHeaders(parser, versionFile, sessionProperties, encoding, badsFormatsReport,
                datasetDescriptor);
        final ISessionProportiesPhysicoChimie sessionPropertiesPhysicoChimie = (ISessionProportiesPhysicoChimie) sessionProperties;
        sessionPropertiesPhysicoChimie.initDate();
        long lineNumber = 0;
        try {
            lineNumber = this.readDispositif(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    IPhysicoChimieSolDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, sessionProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 0);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, sessionProperties);
            lineNumber = this.jumpLines(parser, lineNumber, 0);
        } catch (final IOException e) {
            LOGGER.debug("can't read file", e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
        
    }
    
    
    
}
