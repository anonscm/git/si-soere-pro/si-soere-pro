/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.semisplantation.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation_;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.jpa.JPAITKDAO;
import org.inra.ecoinfo.pro.synthesis.semisplantation.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPASemisPlantationDAO extends JPAITKDAO<MesureSemisPlantation, ValeurSemisPlantation> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurSemisPlantation> getValeurITKClass() {
        return ValeurSemisPlantation.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureSemisPlantation> getMesureITKClass() {
        return MesureSemisPlantation.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurSemisPlantation, MesureSemisPlantation> getMesureAttribute() {
        return ValeurSemisPlantation_.mesureinterventionsemis;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }
}
