/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.observationqualitative;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.typeobservationqualitative.TypeObservationQualitative;

/**
 *
 * @author vjkoyao
 */
public class JPAObservationQualitativeDAO extends AbstractJPADAO<ObservationQalitative> implements IObservationQualitativeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ObservationQalitative> getAll() {
        return getAll(ObservationQalitative.class);
    }

    /**
     *
     * @param observation
     * @param typeobservation
     * @return
     */
    @Override
    public Optional<ObservationQalitative> getByNKey(String observation, TypeObservationQualitative typeobservation) {
        CriteriaQuery<ObservationQalitative> query = builder.createQuery(ObservationQalitative.class);
        Root<ObservationQalitative> observationQalitative = query.from(ObservationQalitative.class);
        Join<ObservationQalitative, TypeObservationQualitative> toq = observationQalitative.join(ObservationQalitative_.typeObservationQualitative);
        query
                .select(observationQalitative)
                .where(
                        builder.equal(observationQalitative.get(ObservationQalitative_.observation_qua_nom), observation),
                        builder.equal(toq, typeobservation)
                );
        return getOptional(query);
    }

}
