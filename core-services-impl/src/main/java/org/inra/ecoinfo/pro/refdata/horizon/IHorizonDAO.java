/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizon;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IHorizonDAO extends IDAO<Horizon> {

    /**
     *
     * @return
     */
    List<Horizon> getAll();

    /**
     *
     * @param horizon
     * @return
     */
    Optional<Horizon> getByNKey(String horizon);

}
