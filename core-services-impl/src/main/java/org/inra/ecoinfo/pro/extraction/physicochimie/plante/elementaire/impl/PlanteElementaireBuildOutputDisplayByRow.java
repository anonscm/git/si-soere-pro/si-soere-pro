/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.plante.elementaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteBrutDataTypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteElementaire;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteElementaire;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieOutputDisplayByRow;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonplante.EchantillonPlante;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class PlanteElementaireBuildOutputDisplayByRow extends PhysicoChimieOutputDisplayByRow {

    static final String PATTERN_WORD = "%s";
    static final String PATTERB_CSV_12_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%%s;%%s;%%s;%%s;%%s;%%s";
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA_PLANTEBRUT = "PROPERTY_HEADER_RAW_DATA_PLANTEBRUT";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.plante.messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";


    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedProduitBrutVariables = getVariablesSelected(requestMetadatasMap, IPhysicoChimiePlanteBrutDataTypeManager.CODE_DATATYPE_PLANTE_BRUT);
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<MesurePlanteElementaire> mesuresProduitBrut = getMesures(resultsDatasMap, PlanteBrutExtractor.MAP_INDEX_PLANTEBRUT);
        final Set<String> dispositifsNames = buildListOfDispositifsForDatatype(selectedDispositifs, getDatatype());
        final Map<String, File> filesMap = this.buildOutputsFiles(dispositifsNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>>> mesuresProduitBrutsMap = new TreeMap();

        try {
            this.buildmap(mesuresProduitBrut, mesuresProduitBrutsMap);
        } catch (DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedDispositifs, selectedProduitBrutVariables, selectedIntervalDate,
                outputPrintStreamMap, mesuresProduitBrutsMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    private void buildmap(
            final List<MesurePlanteElementaire> mesuresProduitBruts,
            final SortedMap<Long, SortedMap<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>>> mesuresProduitBrutMap){
        java.util.Iterator<MesurePlanteElementaire> itMesure = mesuresProduitBruts
                .iterator();
        while (itMesure.hasNext()) {
            MesurePlanteElementaire mesurePlanteBrut = itMesure
                    .next();
            EchantillonPlante echan = mesurePlanteBrut.getEchantillonPlante();
            Long siteId = echan.getPrelevementplante().getTraitement().getDispositif().getId();
            if (mesuresProduitBrutMap.get(siteId) == null) {
                mesuresProduitBrutMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresProduitBrutMap.get(siteId).get(echan) == null) {
                mesuresProduitBrutMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresProduitBrutMap.get(siteId).get(echan).put(mesurePlanteBrut.getDatePrelevement(), mesurePlanteBrut);
            itMesure.remove();
        }
    }

    private void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitBrutVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>>> mesuresProduitBrutsMap) {
        try {
            BuildDataLine(selectedDispositifs, selectedProduitBrutVariables, outputPrintStreamMap, mesuresProduitBrutsMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void BuildDataLine(final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitBrutVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>>> mesuresProduitBrutsMap,
            final IntervalDate selectedIntervalDate)  {

        try {
            PrintStream out;
            String currentDispositif;
            String currentEchan;
            String curentCodeCulture;
            String currentTraitement;
            String currentParcelle;
            String courentPlacette;
            String codePrelevee;
            int numero;
            String currentlabo;
            for (final Dispositif dispositif : selectedDispositifs) {
            out = outputPrintStreamMap.get(getDispositifDatatype(dispositif, getDatatype()));
                final SortedMap<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>> mesureProduitBrutsMapByDisp = mesuresProduitBrutsMap
                        .get(dispositif.getId());
                if (mesureProduitBrutsMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>>> itEchan = mesureProduitBrutsMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<EchantillonPlante, SortedMap<LocalDate, MesurePlanteElementaire>> echanEntry = itEchan
                            .next();
                    EchantillonPlante echantillon = echanEntry.getKey();
                    SortedMap<LocalDate, MesurePlanteElementaire> mesureProduitBrutsMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                    for (Entry<LocalDate, MesurePlanteElementaire> entrySet : mesureProduitBrutsMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesurePlanteElementaire mesurePlanteBrut = entrySet.getValue();
                        currentDispositif = echantillon.getPrelevementplante().getTraitement().getDispositif().getName();
                        currentEchan = echantillon.getCodeplante() != null ? echantillon.getCodeplante()
                                : org.apache.commons.lang.StringUtils.EMPTY;
                        curentCodeCulture = echantillon.getPrelevementplante().getCultures().getNom();
                        currentParcelle = echantillon.getPrelevementplante().getPelementaire().getCodeParcelleElementaire().getCode();
                        currentTraitement = mesurePlanteBrut.getEchantillonPlante().getPrelevementplante().getTraitement().getNom();
                        courentPlacette = mesurePlanteBrut.getEchantillonPlante().getPrelevementplante().getPlacette().getNom();
                        codePrelevee = mesurePlanteBrut.getEchantillonPlante().getPrelevementplante().getPartiePrelevee().getCode();
                        numero = mesurePlanteBrut.getNumero_repetition();
                        currentlabo = String.format(
                                PlanteElementaireBuildOutputDisplayByRow.PATTERN_WORD,
                                mesurePlanteBrut.getNomlabo());

                        String genericPattern = LineDataFixe(date,
                                currentEchan,
                                curentCodeCulture,
                                currentParcelle, currentTraitement,
                                courentPlacette, codePrelevee, numero,
                                currentlabo);
                        LineDataVariable(mesurePlanteBrut, genericPattern, out, selectedProduitBrutVariables);
                    }
                    itEchan.remove();
                }
            }
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void LineDataVariable(MesurePlanteElementaire mesureProduitBrut, String genericPattern, PrintStream out, final List<VariablesPRO> selectedProduitBrutVariables) {
        selectedProduitBrutVariables.stream().forEach((variablesPRO) -> {
            for (Iterator<ValeurPlanteElementaire> ValeurIterator = mesureProduitBrut.getValeurPlanteElementaire().iterator(); ValeurIterator.hasNext();) {
                ValeurPlanteElementaire valeur = ValeurIterator.next();
                if (((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getId().equals(variablesPRO.getId())) {
                    String line = String.format(genericPattern,
                            valeur.getValeur(),
                            valeur.getStatutvaleur(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());
                    out.println(line);
                    ValeurIterator.remove();
                }
            }
        });
    }

    private String LineDataFixe(LocalDate date, String currentEchan, String curentCodeCulture, String currentParcelle, String currentTraitement, String courentPlacette, String codePrelevee, int numero, String currentlabo) {
        String genericPattern = String.format(PATTERB_CSV_12_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentEchan,
                curentCodeCulture, currentTraitement,
                currentParcelle,
                courentPlacette, codePrelevee, numero,
                currentlabo);
        return genericPattern;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                PlanteElementaireBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        PlanteElementaireBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        PlanteElementaireBuildOutputDisplayByRow.HEADER_RAW_DATA_PLANTEBRUT));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (CollectionUtils.isEmpty(((DefaultParameter) parameters).getResults()
                .get(PlanteBrutExtractor.CST_RESULT_EXTRACTION_PLANTEBRUT_CODE)
                .getOrDefault(PlanteBrutExtractor.MAP_INDEX_PLANTEBRUT,
                        ((DefaultParameter) parameters).getResults()
                .get(PlanteBrutExtractor.CST_RESULT_EXTRACTION_PLANTEBRUT_CODE)
                .get(PlanteBrutExtractor.MAP_INDEX_0)))) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, PlanteBrutExtractor.CST_RESULT_EXTRACTION_PLANTEBRUT_CODE));
        return null;
    }

    private String getDatatype() {
        return "plante_physico-chimie_donnees_elementaires";
    }


}
