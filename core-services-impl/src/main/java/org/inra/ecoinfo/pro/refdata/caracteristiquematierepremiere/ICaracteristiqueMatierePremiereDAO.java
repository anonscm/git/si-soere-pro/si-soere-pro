package org.inra.ecoinfo.pro.refdata.caracteristiquematierepremiere;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.typecaracteristique.TypeCaracteristiques;

/**
 *
 * @author ptcherniati
 */
public interface ICaracteristiqueMatierePremiereDAO extends IDAO<CaracteristiqueMatierePremieres> {

    /**
     *
     * @return
     */
    List<CaracteristiqueMatierePremieres> getAll();

    /**
     *
     * @param cmp_nom
     * @param cmp_type
     * @return
     */
    Optional<CaracteristiqueMatierePremieres> getByNomAndType(String cmp_nom, String cmp_type);

    /**
     *
     * @param cmp_nom
     * @param tc
     * @return
     */
    Optional<CaracteristiqueMatierePremieres> getByNKey(String cmp_nom, TypeCaracteristiques tc);

}
