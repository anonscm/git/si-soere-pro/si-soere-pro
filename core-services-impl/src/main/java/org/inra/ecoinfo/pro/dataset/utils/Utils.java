/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class Utils {

    /**
     *
     */
    public static final String  MSG_NO_DATA               = "PROPERTY_MSG_NO_DATA";
    
    private static final String BUNDLE_SOURCE_PATH        = "org.inra.ecoinfo.utils.messages";
 
    private static final String PATTERN_LIGNE_VIDE        = "(?m)^$";
 
    private static final String MSG_BAD_CSV               = "PROPERTY_MSG_BAD_CSV";
   
    private static final String DOT                       = ".";
   
    private static final String COLON_REGEXP              = "((;|^)([+-]?[0-9]+)),([0-9]*([Ee][+-]?[0-9]{2})?(?=(;|$)))";

   
    private static final String PATTERN_BAD_DATES_FIND    = "((;|^)(0[1-9]|[12][0-9]|3[01]))[\\/\\-]((0?[1-9]|1[012]))[\\/\\-](\\d{4}(?=;|$))";

   
    private static final String PATTERN_BAD_DATES_REPLACE = "$1/$5/$6";
    
    /**
     *
     * @param data
     * @param localizationManager
     * @return
     * @throws BusinessException
     * @throws IOException
     */
    public static byte[] sanitizeData(final byte[] data,
            final ILocalizationManager localizationManager) throws BusinessException, IOException {
        if (data == null) {
            throw new BusinessException(localizationManager.getMessage(Utils.BUNDLE_SOURCE_PATH,
                    Utils.MSG_NO_DATA));
        }
        try {
            final String encoding = org.inra.ecoinfo.utils.Utils.detectStreamEncoding(data);
            String dataString = new String(data, encoding);
            if (Pattern.compile(Utils.PATTERN_LIGNE_VIDE).matcher(dataString).find()) {
                throw new BusinessException(localizationManager.getMessage(
                        Utils.BUNDLE_SOURCE_PATH, Utils.MSG_BAD_CSV));
            }
            dataString = Pattern.compile(Utils.COLON_REGEXP, Pattern.MULTILINE).matcher(dataString)
                    .replaceAll("$1" + Utils.DOT + "$4");
            dataString = Pattern.compile(Utils.PATTERN_BAD_DATES_FIND, Pattern.MULTILINE)
                    .matcher(dataString).replaceAll(Utils.PATTERN_BAD_DATES_REPLACE);
            return dataString.getBytes(encoding);
        } catch (final UnsupportedEncodingException e) {
            throw new BusinessException("unsupporting encoding", e);
        } catch (final IOException e) {
            throw new BusinessException("io exception", e);
        }
    }
    
    /**
     *
     */
    public Utils() {
        super();
    }

    
    
    
}
