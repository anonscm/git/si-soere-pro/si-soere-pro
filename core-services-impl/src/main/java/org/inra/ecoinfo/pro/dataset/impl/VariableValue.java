/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;



/**
 *
 * @author adiankha
 */
public class VariableValue {
    
    VariablesPRO variable;
    
    String       value;

    
    Boolean      flag = false;

    
    String       statutValeur;
    
    /**
     *
     * @param variable
     * @param value
     */
    public VariableValue(final VariablesPRO variable,final String value) {
        super();
        this.variable = variable;
        this.value = value;
    }

    /**
     *
     * @param value
     */
    public VariableValue(String value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public VariablesPRO getVariable() {
        return variable;
    }

    /**
     *
     * @param variable
     */
    public void setVariable(VariablesPRO variable) {
        this.variable = variable;
    }
 
    /**
     *
     * @return
     */
    public String getValue() {
        return this.value;
    }

    /**
     *
     * @param value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public String getStatutValeur() {
        return statutValeur;
    }

    /**
     *
     * @param statutValeur
     */
    public void setStatutValeur(String statutValeur) {
        this.statutValeur = statutValeur;
    }

    /**
     *
     * @return
     */
    public Boolean isStatutValeur() {
        return flag;
    }

    /**
     *
     * @param flag
     */
    public void setFlag(Boolean flag) {
        this.flag = flag;
    } 
    
}
