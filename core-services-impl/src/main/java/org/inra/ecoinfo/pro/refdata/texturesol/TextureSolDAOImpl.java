package org.inra.ecoinfo.pro.refdata.texturesol;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class TextureSolDAOImpl extends AbstractJPADAO<Texturesol> implements ITextureSolDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Texturesol> getAll() {
        return getAll(Texturesol.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Texturesol> getByNKey(String nom) {
        CriteriaQuery<Texturesol> query = builder.createQuery(Texturesol.class);
        Root<Texturesol> methodeProcess = query.from(Texturesol.class);
        query
                .select(methodeProcess)
                .where(
                        builder.equal(methodeProcess.get(Texturesol_.code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }

}
