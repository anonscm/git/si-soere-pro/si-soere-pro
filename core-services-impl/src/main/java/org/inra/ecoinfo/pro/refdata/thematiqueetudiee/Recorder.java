/**
 *
 */
package org.inra.ecoinfo.pro.refdata.thematiqueetudiee;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<ThematiqueEtudiee> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    IThematiqueEtudieeDAO thematiqueEtudieeDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                thematiqueEtudieeDAO.remove(thematiqueEtudieeDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get thematique etudiee")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ThematiqueEtudiee> getAllElements() throws BusinessException {
        return thematiqueEtudieeDAO.getAll(ThematiqueEtudiee.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ThematiqueEtudiee thematiqueEtudiee) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesLibelle = localizationManager.newProperties(ThematiqueEtudiee.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_THEMEETUDIEE, Locale.ENGLISH);
        
        String localizedChampLibelle = "";
        
        if (thematiqueEtudiee != null) {
            localizedChampLibelle = propertiesLibelle.containsKey(thematiqueEtudiee.getLibelle()) ? propertiesLibelle.getProperty(thematiqueEtudiee.getLibelle()) : thematiqueEtudiee.getLibelle();
        }
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(thematiqueEtudiee == null ? Constantes.STRING_EMPTY : thematiqueEtudiee.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(thematiqueEtudiee == null ? Constantes.STRING_EMPTY : localizedChampLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, ThematiqueEtudiee.TABLE_NAME);
                
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                if (libelle == null || libelle.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                ThematiqueEtudiee thematiqueEtudiee = new ThematiqueEtudiee(libelle);
                ThematiqueEtudiee dbThematiqueEtudiee = thematiqueEtudieeDAO.getByNKey(libelle).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbThematiqueEtudiee == null) {
                        thematiqueEtudieeDAO.saveOrUpdate(thematiqueEtudiee);
                    } else {
                        dbThematiqueEtudiee.setLibelle(libelle);
                        thematiqueEtudieeDAO.saveOrUpdate(dbThematiqueEtudiee);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (NoResultException e2) {
            throw new BusinessException(e2.getMessage(), e2);
        } catch (NonUniqueResultException e3) {
            throw new BusinessException(e3.getMessage(), e3);
        }
    }

    /**
     * @param thematiqueEtudieeDAO
     */
    public void setThematiqueEtudieeDAO(IThematiqueEtudieeDAO thematiqueEtudieeDAO) {
        this.thematiqueEtudieeDAO = thematiqueEtudieeDAO;
    }
    
}
