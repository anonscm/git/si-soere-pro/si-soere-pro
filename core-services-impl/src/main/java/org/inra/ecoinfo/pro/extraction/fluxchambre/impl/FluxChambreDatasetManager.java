/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.extraction.fluxchambre.IFluxChambreDAO;
import org.inra.ecoinfo.pro.extraction.fluxchambre.IFluxChambreDatatypeManager;
import org.inra.ecoinfo.pro.extraction.jsf.IDispositifManager;
import org.inra.ecoinfo.pro.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author vjkoyao
 */
public class FluxChambreDatasetManager extends DefaultDatasetManager implements IDispositifManager, IVariableManager<Dispositif> {

    static final String DISPOSITIF_FLUX_CHAMBRE_PRIVILEGE_PATTERN = "%s/*/flux_chambre/*";
    static final String VARIABLE_FLUX_CHAMBRE_PRIVILEGE_PATTERN = "%s/*/flux_chambre/%s";

    IFluxChambreDAO fluxChambreDAO;
    protected IVariableDAO variableDAO;
    IVariablesPRODAO variPRODAO;

    @Override
    public List<Dispositif> getAvailablesDispositifs() {
        return fluxChambreDAO.getAvailableDispositif(policyManager.getCurrentUser());
    }

    public IFluxChambreDAO getFluxChambreDAO() {
        return fluxChambreDAO;
    }

    public void setFluxChambreDAO(IFluxChambreDAO fluxChambreDAO) {
        this.fluxChambreDAO = fluxChambreDAO;
    }

    public IVariableDAO getVariableDAO() {
        return variableDAO;
    }

    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    /**
     *
     * @param linkedList
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByDispositif(List<Dispositif> linkedList) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        variablesByDatatype.put(IFluxChambreDatatypeManager.CODE_DATATYPE_FLUX_CHAMBRE, fluxChambreDAO.getAvailablesVariablesByDispositif(linkedList, policyManager.getCurrentUser()));
        return variablesByDatatype;

    }

}
