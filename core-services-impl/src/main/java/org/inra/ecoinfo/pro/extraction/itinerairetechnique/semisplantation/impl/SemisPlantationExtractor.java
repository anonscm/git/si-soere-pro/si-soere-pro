/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.semisplantation.impl;

import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.MesureSemisPlantation;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.entity.ValeurSemisPlantation;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKExtractor;

/**
 *
 * @author adiankha
 */
public class SemisPlantationExtractor extends AbstractITKExtractor<MesureSemisPlantation, ValeurSemisPlantation> {

    /**
     *
     */
    public static final String SEMIS_PLANTATION = "semis_plantation";

    /**
     *
     */
    protected static final String MAP_INDEX_SEMIS_PLANTATION = "semisplantation";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return SEMIS_PLANTATION;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return SEMIS_PLANTATION;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_SEMIS_PLANTATION;
    }

}
