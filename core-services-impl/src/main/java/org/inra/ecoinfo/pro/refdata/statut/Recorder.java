package org.inra.ecoinfo.pro.refdata.statut;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Statut> {
    
    /**
     *
     */
    protected IStatutDAO statutDAO;
    private Properties NomStatutEn;
    private Properties StatutComEn;
    
    private void createOrUpdateStatut(final String statut_code, final String statut_nom, String comment, final Statut dbstatut) throws BusinessException {
        if (dbstatut == null) {
            final Statut statut = new Statut(statut_nom, comment);
            statut.setStatut_code(statut_code);
            statut.setStatut_nom(statut_nom);
            statut.setStatut_comment(comment);
            createStatut(statut);
        } else {
            updateBDStatut(statut_nom, comment, dbstatut);
        }
    }

    /**
     *
     * @param statut
     * @throws BusinessException
     */
    private void createStatut(final Statut statut) throws BusinessException {
        try {
            statutDAO.saveOrUpdate(statut);
            statutDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create statut");
        }
    }

    /**
     *
     * @param statut
     * @param dbstatut
     */
    private void updateBDStatut(final String statut_nom, String comment, final Statut dbstatut) throws BusinessException {
        try {
            dbstatut.setStatut_nom(statut_nom);
            dbstatut.setStatut_comment(comment);
            statutDAO.saveOrUpdate(dbstatut);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update statut");
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                statutDAO.remove(statutDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get statuts")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected List<Statut> getAllElements() throws BusinessException {
        return statutDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Statut statut) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statut == null || statut.getStatut_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : statut.getStatut_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statut == null || statut.getStatut_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : NomStatutEn.getProperty(statut.getStatut_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statut == null || statut.getStatut_comment() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : statut.getStatut_comment(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statut == null || statut.getStatut_comment() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : StatutComEn.getProperty(statut.getStatut_comment()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @return
     */
    public IStatutDAO getStatutDAO() {
        return statutDAO;
    }

    /**
     *
     * @param errorsreport
     * @param code
     * @param nom
     * @throws BusinessException
     */
    private void persistStatut(final String code, String nom, String comment) throws BusinessException {
        final Statut dbstatut = statutDAO.getByNKey(nom).orElse(null);
        createOrUpdateStatut(code, nom, comment, dbstatut);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            // Parcourir chaque ligne du fichier
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Statut.NAME_ENTITY_JPA);
                // Parcourir chaque colonne d'une ligne

                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String comment = tokenizerValues.nextToken();
                persistStatut(code, nom, comment);
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected ModelGridMetadata<Statut> initModelGridMetadata() {
        NomStatutEn = localizationManager.newProperties(Statut.NAME_ENTITY_JPA, Statut.JPA_COLUMN_NAME, Locale.ENGLISH);
        StatutComEn = localizationManager.newProperties(Statut.NAME_ENTITY_JPA, Statut.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    /**
     *
     * @param statutDAO
     */
    public void setStatutDAO(IStatutDAO statutDAO) {
        this.statutDAO = statutDAO;
    }
    
    /**
     *
     * @param NomStatutEn
     */
    public void setNomStatutEn(Properties NomStatutEn) {
        this.NomStatutEn = NomStatutEn;
    }
    
    /**
     *
     * @return
     */
    public Properties getNomStatutEn() {
        return NomStatutEn;
    }
    
}
