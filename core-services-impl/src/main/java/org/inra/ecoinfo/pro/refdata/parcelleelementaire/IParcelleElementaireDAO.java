/**
 *
 */
package org.inra.ecoinfo.pro.refdata.parcelleelementaire;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;

/**
 * @author sophie
 *
 */
public interface IParcelleElementaireDAO extends IDAO<ParcelleElementaire> {

    /**
     *
     * @return
     */
    List<ParcelleElementaire> getAll();

    /**
     *
     * @param dispositif
     * @return
     */
    List<ParcelleElementaire> getByDispositif(Dispositif dispositif);

    /**
     *
     * @param geolocalisation
     * @return
     */
    Optional<ParcelleElementaire> getByGeolocalisation(Geolocalisation geolocalisation);

    /**
     *
     * @param code
     * @param dispositif
     * @return
     */
    Optional<ParcelleElementaire> getByNKey(String nom, Bloc bloc);
    
    Optional<ParcelleElementaire> getByNKey(CodeParcelleElementaire codePE, Dispositif dispositif);

}
