/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractPhysicoChimieParameter extends DefaultParameter implements IParameter {
    
    List<Dispositif> selectedDispositifs = new LinkedList();
    String commentaires;
    IntervalDate intervalDate;
    int affichage;

{
        
    }

    /**
     *
     * @param metadatasMap
     */
    public AbstractPhysicoChimieParameter(Map<String, Object> metadatasMap) {
        setParameters(metadatasMap);
        setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }

    abstract public String getExtractionTypeCode();

    /**
     *
     * @return
     */
    public List<Dispositif> getSelectedDispositifs() {
        return selectedDispositifs;
    }

    /**
     *
     * @param selectedDispositifs
     */
    public void setSelectedDispositifs(List<Dispositif> selectedDispositifs) {
        this.selectedDispositifs = selectedDispositifs;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     *
     * @param commentaires
     */
    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

    /**
     *
     * @return
     */
    public IntervalDate getIntervalDate() {
        return intervalDate;
    }

    /**
     *
     * @param intervalDate
     */
    public void setIntervalDate(IntervalDate intervalDate) {
        this.intervalDate = intervalDate;
    }
    
}
