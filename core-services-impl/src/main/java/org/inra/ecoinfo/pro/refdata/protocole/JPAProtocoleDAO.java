/**
 *
 */
package org.inra.ecoinfo.pro.refdata.protocole;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPAProtocoleDAO extends AbstractJPADAO<Protocole> implements IProtocoleDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.protocole.IProtocoleDAO#getByNom(java.lang .String)
     */

    /**
     *
     * @param nom
     * @return
     */

    @Override
    public Optional<Protocole> getByNKey(String nom) {
        CriteriaQuery<Protocole> query = builder.createQuery(Protocole.class);
        Root<Protocole> protocole = query.from(Protocole.class);
        query
                .select(protocole)
                .where(
                        builder.equal(protocole.get(Protocole_.nom), nom)
                );
        return getOptional(query);
    }

}
