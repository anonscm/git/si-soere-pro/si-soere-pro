/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.ISessionPropertiesIncubation;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class TestHeadersIncubationSolMoy extends GenericTestHeader {

    /**
     *
     */
    public TestHeadersIncubationSolMoy() {
        super();
    }

    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return
     * @throws BusinessException
     */
    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor) throws BusinessException {

        super.testHeaders(parser, versionFile, sessionProperties, encoding, badsFormatsReport, datasetDescriptor);
        final ISessionPropertiesIncubation sessionPropertiesIncubation = (ISessionPropertiesIncubation) sessionProperties;
        sessionPropertiesIncubation.initDate();
        long lineNumber = 0;

        try {
            lineNumber = this.readDispositif(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    IIncubationSolMoyDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_MOY);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, sessionProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 0);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, sessionProperties);
            lineNumber = this.jumpLines(parser, lineNumber, 0);

        } catch (final IOException e) {
            LOGGER.debug("can't read header", e);
            badsFormatsReport.addException(e);
        }
        return (int) lineNumber;
    }

}
