/*
 *
 */
package org.inra.ecoinfo.pro.refdata.horizonsystematique;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author Vivianne
 */
public class Recorder extends AbstractCSVMetadataRecorder<HorizonSystematique> {
    
    /**
     *
     */
    protected IHorizonSystematiqueDAO horizonsystematiqueDAO;

    /**
     *
     */
    protected Properties propertiesNom;
    
    private void createHorizonSystematique(final HorizonSystematique horizonsystematique) throws BusinessException {
        try {
            horizonsystematiqueDAO.saveOrUpdate(horizonsystematique);
            horizonsystematiqueDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create HorizonSystematique");
        }
    }
    
    private void updateHorizonSystematique(String horizon_systematique_nom, String horizon_systematique_code, final HorizonSystematique dbhorizonsystematique) throws BusinessException {
        try {
            dbhorizonsystematique.setHorizon_systematique_code(horizon_systematique_code);
            dbhorizonsystematique.setHorizon_systematique_nom(horizon_systematique_nom);
            horizonsystematiqueDAO.saveOrUpdate(dbhorizonsystematique);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update HorizonSystematique");
        }
    }
    
    private void createOrUpdateHorizonSystematique(String horizon_systematique_code, String horizon_systematique_nom, final HorizonSystematique dbHorizonSystematique) throws BusinessException {
        
        if (dbHorizonSystematique == null) {
            
            final HorizonSystematique horizonsystematique = new HorizonSystematique(horizon_systematique_code, horizon_systematique_nom);
            horizonsystematique.setHorizon_systematique_code(horizon_systematique_code);
            horizonsystematique.setHorizon_systematique_nom(horizon_systematique_nom);
            createHorizonSystematique(horizonsystematique);
            
        } else {
            
            updateHorizonSystematique(horizon_systematique_code, horizon_systematique_nom, dbHorizonSystematique);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, HorizonSystematique.NAME_ENTITY_JPA);
                final String horizon_systematique_nom = tokenizerValues.nextToken();
                horizonsystematiqueDAO.remove(horizonsystematiqueDAO.getByNKey(horizon_systematique_nom)
                        .orElseThrow(() -> new BusinessException("can't find HorizonSystematique")));
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<HorizonSystematique> getAllElements() throws BusinessException {
        return horizonsystematiqueDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(HorizonSystematique horizonsystematique) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(horizonsystematique == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : horizonsystematique.getHorizon_systematique_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(horizonsystematique == null || horizonsystematique.getHorizon_systematique_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : propertiesNom.getProperty(horizonsystematique.getHorizon_systematique_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
        
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<HorizonSystematique> initModelGridMetadata() {
        propertiesNom = localizationManager.newProperties(HorizonSystematique.NAME_ENTITY_JPA, HorizonSystematique.JPA_COLUMN_NOM, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
    
    private void persistPropfilLocalisation(String horizon_systematique_code, String horizon_systematique_nom) throws BusinessException {
        final HorizonSystematique dbhorizonsystematique = horizonsystematiqueDAO.getByNKey(horizon_systematique_nom).orElse(null);
        createOrUpdateHorizonSystematique(horizon_systematique_code, horizon_systematique_nom, dbhorizonsystematique);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, HorizonSystematique.NAME_ENTITY_JPA);
                final String horizon_systematique_nom = tokenizerValues.nextToken();
                final String horizon_systematique_code = Utils.createCodeFromString(horizon_systematique_nom);
                persistPropfilLocalisation(horizon_systematique_code, horizon_systematique_nom);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    /**
     *
     * @return
     */
    public IHorizonSystematiqueDAO getHorizonsystematiqueDAO() {
        return horizonsystematiqueDAO;
    }
    
    /**
     *
     * @param horizonsystematiqueDAO
     */
    public void setHorizonsystematiqueDAO(
            IHorizonSystematiqueDAO horizonsystematiqueDAO) {
        this.horizonsystematiqueDAO = horizonsystematiqueDAO;
    }
    
    /**
     *
     * @return
     */
    public Properties getPropertiesNom() {
        return propertiesNom;
    }
    
    /**
     *
     * @param propertiesNom
     */
    public void setPropertiesNom(Properties propertiesNom) {
        this.propertiesNom = propertiesNom;
    }
    
}
