/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typenomenclature;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class TypeNomenclatureDaoImpl extends AbstractJPADAO<TypeNomenclature> implements ITypeNomenclatureDAO {

    /**
     *
     * @return
     */
    @Override
    public List<TypeNomenclature> getAll() {
        return getAll(TypeNomenclature.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<TypeNomenclature> getByNKey(String nom) {
        CriteriaQuery<TypeNomenclature> query = builder.createQuery(TypeNomenclature.class);
        Root<TypeNomenclature> typeNomenclature = query.from(TypeNomenclature.class);
        query
                .select(typeNomenclature)
                .where(
                        builder.equal(typeNomenclature.get(TypeNomenclature_.tn_code), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }

}
