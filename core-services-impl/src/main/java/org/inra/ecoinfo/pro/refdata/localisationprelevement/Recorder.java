/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationprelevement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<LocalisationPrelevement> {

    ILocalisationPrelevementDAO localisationDAO;
    Properties nomEn;

    private void createLocalisation(final LocalisationPrelevement localisationPrelevement) throws BusinessException {
        try {
            localisationDAO.saveOrUpdate(localisationPrelevement);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create localisationPrelevement");
        }
        localisationDAO.flush();
    }

    private void updateBDLocalisation(final String code, String nom, final LocalisationPrelevement dbLocalisation) throws BusinessException {
        try {
            dbLocalisation.setCode(code);
            dbLocalisation.setNom(nom);
            localisationDAO.saveOrUpdate(dbLocalisation);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update localisationPrelevement");
        }
    }

    private void createOrUpdateHorizon(final String code, String nom, final LocalisationPrelevement dbLocalisation) throws BusinessException {
        if (dbLocalisation == null) {
            final LocalisationPrelevement localisationprelevement = new LocalisationPrelevement(nom);
            localisationprelevement.setCode(code);
            localisationprelevement.setNom(nom);

            createLocalisation(localisationprelevement);
        } else {
            updateBDLocalisation(code, nom, dbLocalisation);
        }
    }

    private void persistLocalisation(final String code, final String nom) throws BusinessException, BusinessException {
        final LocalisationPrelevement dbLocalisation = localisationDAO.getByNKey(nom).orElse(null);
        createOrUpdateHorizon(code, nom, dbLocalisation);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, LocalisationPrelevement.JPA_NAME_ENTITY);
                final String nom = tokenizerValues.nextToken();
                final LocalisationPrelevement dbLocalisation = localisationDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't find localisationPrelevement"));
                localisationDAO.remove(dbLocalisation);
                values = csvp.getLine();

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, LocalisationPrelevement.JPA_NAME_ENTITY);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                if (!errorsReport.hasErrors()) {
                    persistLocalisation(code, nom);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<LocalisationPrelevement> getAllElements() throws BusinessException {
        return localisationDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(LocalisationPrelevement localisation) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(localisation == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : localisation.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(localisation == null || localisation.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomEn.getProperty(localisation.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<LocalisationPrelevement> initModelGridMetadata() {
        nomEn = localizationManager.newProperties(LocalisationPrelevement.JPA_NAME_ENTITY, LocalisationPrelevement.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param localisationDAO
     */
    public void setLocalisationDAO(ILocalisationPrelevementDAO localisationDAO) {
        this.localisationDAO = localisationDAO;
    }

}
