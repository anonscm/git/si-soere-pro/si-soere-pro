/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import java.io.File;
import java.io.PrintStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 * @param <M>
 */
public abstract class AbstractITKOutputBuilder<M> extends AbstractOutputBuilder {

    Properties listeItinerarieValues;

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        listeItinerarieValues
                = localizationManager.newProperties(ListeItineraire.NAME_ENTITY_JPA, ListeItineraire.JPA_COLUMN_LISTE_VALEUR, localizationManager.getUserLocale().getLocale());
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedRecolteCoupeVariables = (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(ITKParameterVO.RECOLTECOUPE));
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<M> mesureMap = resultsDatasMap.get(getIndexResults());
        final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, M>>> mesuresMap = new TreeMap();
        try {
            this.buildmap(mesureMap, mesuresMap);
        } catch (ParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        final Set<String> datatypeNames = new HashSet();
        selectedDispositifs.stream().filter((dispositif) -> !(!mesuresMap.containsKey(dispositif.getId()))).forEach((dispositif) -> {
            datatypeNames.add(getDispositifDatatypeKey(dispositif));
        });
        final Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().stream().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        this.readMap(selectedDispositifs, selectedRecolteCoupeVariables, selectedIntervalDate, outputPrintStreamMap, mesuresMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }

    /**
     *
     * @param mesure
     * @param mesuresMap
     * @throws ParseException
     */
    abstract protected void buildmap(List<M> mesure, SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, M>>> mesuresMap) throws ParseException;

    /**
     *
     * @param selectedDispositifs
     * @param selectedRecolteCoupeVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresMap
     */
    abstract protected void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedRecolteCoupeVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, M>>> mesuresMap);

    /**
     *
     * @return
     */
    abstract protected String getIndexResults();

    /**
     *
     * @param dispositif
     * @return
     */
    protected String getDispositifDatatypeKey(Dispositif dispositif) {
        return String.format("%s_%s", getDatatypeName(), dispositif.getName());
    }

    /**
     *
     * @return
     */
    abstract protected String getDatatypeName();
}
