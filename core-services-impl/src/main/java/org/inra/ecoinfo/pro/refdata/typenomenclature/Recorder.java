/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typenomenclature;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.GrandTypeProduit.GrandTypeProduits;
import org.inra.ecoinfo.pro.refdata.grandtypeproduit.IGrandTypeProduitDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeNomenclature> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_NOMENCLATURE_BAD_GTP = "PROPERTY_MSG_NOMENCLATURE_BAD_GTP";
    ITypeNomenclatureDAO typenomenclatureDAO;

    /**
     *
     */
    protected IGrandTypeProduitDAO gtpDAO;
    private Properties nomTNEn;
    private Properties commentEn;

    private String[] listeGrandTypeProduitsPossibles;

    private void updateNamesGrandTypeProduitsPossibles() {
        List<GrandTypeProduits> groupegtp = gtpDAO.getAll(GrandTypeProduits.class);
        String[] lGTPPossibles = new String[groupegtp.size() + 1];
        lGTPPossibles[0] = "";
        int index = 1;
        for (GrandTypeProduits gtp : groupegtp) {
            lGTPPossibles[index++] = gtp.getGtp_nom();
        }
        this.listeGrandTypeProduitsPossibles = lGTPPossibles;
    }

    private void createTypeNomen(final TypeNomenclature tnomen) throws BusinessException {
        try {
            typenomenclatureDAO.saveOrUpdate(tnomen);
            typenomenclatureDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create tnomen");
        }
    }

    private void updateTypeNomen(String code, final String nom, GrandTypeProduits gtp, final TypeNomenclature dbtnomen, String commentaire) throws BusinessException {
        try {
            dbtnomen.setTn_nom(nom);
            dbtnomen.setGrandtypeproduits(gtp);
            dbtnomen.setTn_code(code);
            dbtnomen.setCommentaire(commentaire);
            typenomenclatureDAO.saveOrUpdate(dbtnomen);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update tnomen");
        }
    }

    private void createOrUpdateTypeNomen(final String code, final String nom, GrandTypeProduits gtp, final TypeNomenclature dbtnomen, String commentaire) throws BusinessException {
        if (dbtnomen == null) {
            final TypeNomenclature tnomen = new TypeNomenclature(nom, gtp, commentaire);
            tnomen.setTn_code(code);
            tnomen.setTn_nom(nom);
            tnomen.setGrandtypeproduits(gtp);
            createTypeNomen(tnomen);
        } else {
            updateTypeNomen(code, nom, gtp, dbtnomen, commentaire);
        }
    }

    private void persistTypeNomen(final String code, final String nom, GrandTypeProduits gtp, String commentaire) throws BusinessException, BusinessException {
        final TypeNomenclature dbtypenomen = typenomenclatureDAO.getByNKey(nom).orElse(null);
        createOrUpdateTypeNomen(code, nom, gtp, dbtypenomen, commentaire);
    }

    private void gestionErreurs(final ErrorsReport errorsReport) throws BusinessException {
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                TypeNomenclature tnomenclature = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nom = tokenizerValues.nextToken();
                tnomenclature = typenomenclatureDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get type nomen"));
                typenomenclatureDAO.remove(tnomenclature);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<TypeNomenclature> getAllElements() throws BusinessException {
        return typenomenclatureDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            long line = 0;
            String[] values = csvp.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, TypeNomenclature.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String gtp = tokenizerValues.currentToken();
                final String com = tokenizerValues.nextToken();
                long indexf = tokenizerValues.currentTokenIndex();
                persistTypeNomenclature(gtp, errorsReport, line, indexf, code, nom, com);
                values = csvp.getLine();
            }
            gestionErreurs(errorsReport);

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void persistTypeNomenclature(final String gtp, final ErrorsReport errorsReport, long line, long indexf, final String code, final String nom, String commentaire) throws BusinessException, BusinessException {
        GrandTypeProduits GTP = verifGrandTypeProduit(gtp, errorsReport, line, indexf);
        if (!errorsReport.hasErrors()) {
            persistTypeNomen(code, nom, GTP, commentaire);
        }
    }

    private GrandTypeProduits verifGrandTypeProduit(final String gtp, final ErrorsReport errorsReport, long line, long indexf) throws BusinessException {
        GrandTypeProduits GTP = gtpDAO.getByNKey(gtp).orElse(null);
        if (GTP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_NOMENCLATURE_BAD_GTP), line, indexf, gtp));
        }
        return GTP;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeNomenclature typenomenclature) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typenomenclature == null || typenomenclature.getTn_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : typenomenclature.getTn_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typenomenclature == null || typenomenclature.getTn_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomTNEn.getProperty(typenomenclature.getTn_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typenomenclature == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typenomenclature.getGrandtypeproduits().getGtp_nom() != null ? typenomenclature.getGrandtypeproduits().getGtp_nom() : "",
                        listeGrandTypeProduitsPossibles,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typenomenclature == null || typenomenclature.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : typenomenclature.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typenomenclature == null || typenomenclature.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : commentEn.getProperty(typenomenclature.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<TypeNomenclature> initModelGridMetadata() {
        updateNamesGrandTypeProduitsPossibles();
        nomTNEn = localizationManager.newProperties(TypeNomenclature.NAME_ENTITY_JPA, TypeNomenclature.JPA_COLUMN_NAME, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(TypeNomenclature.NAME_ENTITY_JPA, TypeNomenclature.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param typenomenclatureDAO
     */
    public void setTypenomenclatureDAO(ITypeNomenclatureDAO typenomenclatureDAO) {
        this.typenomenclatureDAO = typenomenclatureDAO;
    }

    /**
     *
     * @param gtpDAO
     */
    public void setGtpDAO(IGrandTypeProduitDAO gtpDAO) {
        this.gtpDAO = gtpDAO;
    }

}
