/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.especeplante;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.famille.Famille;
import org.inra.ecoinfo.pro.refdata.famille.IFamilleDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<EspecePlante> {

    private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_BAD_FAMILLE_ITK = "PROPERTY_MSG_BAD_FAMILLE_ITK";

    IEspecePlanteDAO especePlanteDAO;
    IFamilleDAO familleDAO;
    private Properties commentEn;
    private Properties nomEspece;
    private String[] ListeTypePossibles;

    private void createEspece(final EspecePlante especePlante) throws BusinessException {
        try {
            especePlanteDAO.saveOrUpdate(especePlante);
            especePlanteDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create espece plante");
        }
    }

    private void updateBDEspece(String code, String nom, String commentaire, String descriptif, String source, Famille famille, EspecePlante dbespece) throws BusinessException {
        try {
            dbespece.setCode(code);
            dbespece.setNom(nom);
            dbespece.setCommentaire(commentaire);
            dbespece.setDescriptif(descriptif);
            dbespece.setSource(source);
            dbespece.setFamille(famille);
            especePlanteDAO.saveOrUpdate(dbespece);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update espece plante");
        }
    }

    private void createOrUpdateEspece(final String code, String nom, String commentaire, String descriptif, String source, Famille famille, final EspecePlante dbespece) throws BusinessException {
        if (dbespece == null) {
            final EspecePlante especePlante = new EspecePlante(nom, code, commentaire, descriptif, source, famille);
            especePlante.setCode(code);
            especePlante.setNom(nom);
            especePlante.setCommentaire(commentaire);
            especePlante.setDescriptif(descriptif);
            especePlante.setSource(source);
            especePlante.setFamille(famille);
            createEspece(especePlante);
        } else {
            updateBDEspece(code, nom, commentaire, descriptif, source, famille, dbespece);
        }
    }

    private void persistEtapes(final String nom, final String code, Famille famille, String descriptif, String source, String commentaire) throws BusinessException, BusinessException {
        final EspecePlante dbEspece = especePlanteDAO.getByNKey(code).orElse(null);
        createOrUpdateEspece(code, nom, commentaire, descriptif, source, famille, dbEspece);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final EspecePlante dbEspece = especePlanteDAO.getByNKey(code)
                        .orElseThrow(()->new BusinessException("can't get espece plante"));
                if (dbEspece != null) {
                    especePlanteDAO.remove(dbEspece);
                    values = csvp.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            long line = 0;
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EspecePlante.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String nom_famille = tokenizerValues.nextToken();
                final String descriptif = tokenizerValues.nextToken();
                final String source = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();

                Famille famille = familleDAO.getByNKey(nom_famille).orElse(null);
                if (famille == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_FAMILLE_ITK), line, nom_famille));
                }
                if (!errorsReport.hasErrors()) {
                    persistEtapes(nom, code, famille, descriptif, source, commentaire);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<EspecePlante> getAllElements() throws BusinessException {
        return especePlanteDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EspecePlante espece) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(espece == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : espece.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(espece == null || espece.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomEspece.getProperty(espece.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(espece == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : (espece.getFamille() != null ? espece.getFamille().getFamille_nom() : ""), ListeTypePossibles,
                        null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(espece == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : espece.getDescriptif(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(espece == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : espece.getSource(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(espece == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : espece.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(espece == null || espece.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(espece.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    public void setEspecePlanteDAO(IEspecePlanteDAO especePlanteDAO) {
        this.especePlanteDAO = especePlanteDAO;
    }

    public void setFamilleDAO(IFamilleDAO familleDAO) {
        this.familleDAO = familleDAO;
    }

    public void setListeTypePossibles(String[] ListeTypePossibles) {
        this.ListeTypePossibles = ListeTypePossibles;
    }

    @Override
    protected ModelGridMetadata<EspecePlante> initModelGridMetadata() {
        commentEn = localizationManager.newProperties(EspecePlante.NAME_ENTITY_JPA, EspecePlante.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        nomEspece = localizationManager.newProperties(EspecePlante.NAME_ENTITY_JPA, EspecePlante.JPA_COLUMN_NOM, Locale.ENGLISH);
        FamillePossibles();
        return super.initModelGridMetadata();

    }

    private void FamillePossibles() {

        List<Famille> groupetype = familleDAO.getAll();
        String[] listeTypePossibles = new String[groupetype.size() + 1];
        listeTypePossibles[0] = "";
        int index = 1;
        for (Famille famille : groupetype) {
            listeTypePossibles[index++] = famille.getFamille_nom();
        }
        this.ListeTypePossibles = listeTypePossibles;
    }

}
