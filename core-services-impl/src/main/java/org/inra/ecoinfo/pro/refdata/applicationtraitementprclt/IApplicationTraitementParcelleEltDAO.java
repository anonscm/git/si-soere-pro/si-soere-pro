package org.inra.ecoinfo.pro.refdata.applicationtraitementprclt;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.modalite.Modalite;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 * @author sophie
 *
 */
public interface IApplicationTraitementParcelleEltDAO extends IDAO<ApplicationTraitementParcelleElt> {

    /**
     *
     * @return
     */
    List<ApplicationTraitementParcelleElt> getAll();

    /**
     *
     * @param descriptionTraitement
     * @param parcelleElementaire
     * @param modalite
     * @return
     */
    Optional<ApplicationTraitementParcelleElt> getByNKey(DescriptionTraitement descriptionTraitement, ParcelleElementaire parcelleElementaire, Modalite modalite);
}
