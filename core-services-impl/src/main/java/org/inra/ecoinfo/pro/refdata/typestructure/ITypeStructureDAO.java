/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typestructure;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeStructureDAO extends IDAO<TypeStructure> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeStructure> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<TypeStructure> getByCode(String code);

}
