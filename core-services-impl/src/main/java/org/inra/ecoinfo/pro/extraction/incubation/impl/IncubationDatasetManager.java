/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolProDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolProMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.incubation.IIncubationDAO;
import org.inra.ecoinfo.pro.extraction.jsf.IDispositifManager;
import org.inra.ecoinfo.pro.extraction.jsf.IVariableManager;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author vjkoyao
 */
public class IncubationDatasetManager extends DefaultDatasetManager implements IDispositifManager, IVariableManager<Dispositif> {

    static final String VARIABLE_SOl_PRIVILEGE_PATTERN = "%s/%s/*/incubation_sol/%s";
    static final String VARIABLE_SOL_MOY_PRIVILEGE_PATTERN = "%s/%s/*/incubation_sol_moyenne/%s";
    static final String VARIABLE_SOL_PRO_PRIVILEGE_PATTERN = "%s/%s/*/incubation_sol_pro/%s";
    static final String VARIABLE_SOL_PRO_MOY_PRIVILEGE_PATTERN = "%s/%s/*/incubation_sol_pro_moyenne/%s";

    Map<String, IIncubationDAO> extractionsDAO = new HashMap();

    IIncubationDAO incubationSolDAO;
    IIncubationDAO incubationSolProDAO;
    IIncubationDAO incubationSolMoyDAO;
    IIncubationDAO incubationSolProMoyDAO;

    /**
     *
     * @return
     */
    @Override
    public List<Dispositif> getAvailablesDispositifs() {
        final List<Dispositif> availablesDispositif = new LinkedList();
        final List<Dispositif> dispositif = this.incubationSolDAO.getAvailablesDispositifs(policyManager.getCurrentUser());
        dispositif.addAll(this.incubationSolProDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        dispositif.addAll(this.incubationSolMoyDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        dispositif.addAll(this.incubationSolProMoyDAO.getAvailablesDispositifs(policyManager.getCurrentUser()));
        return availablesDispositif;
    }

    /**
     *
     * @param incubationSolDAO
     */
    public void setIncubationSolDAO(IIncubationDAO incubationSolDAO) {
        this.incubationSolDAO = incubationSolDAO;
        this.extractionsDAO.put(IIncubationSolDatatypeManager.CODE_DATATYPE_INCUBATION_SOL, incubationSolDAO);
    }

    /**
     *
     * @param incubationSolProDAO
     */
    public void setIncubationSolProDAO(IIncubationDAO incubationSolProDAO) {
        this.incubationSolProDAO = incubationSolProDAO;
        this.extractionsDAO.put(IIncubationSolProDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_PRO, incubationSolProDAO);
    }

    /**
     *
     * @param incubationSolMoyDAO
     */
    public void setIncubationSolMoyDAO(IIncubationDAO incubationSolMoyDAO) {
        this.incubationSolMoyDAO = incubationSolMoyDAO;
        this.extractionsDAO.put(IIncubationSolMoyDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_MOY, incubationSolMoyDAO);
    }

    /**
     *
     * @param incubationSolProMoyDAO
     */
    public void setIncubationSolProMoyDAO(IIncubationDAO incubationSolProMoyDAO) {
        this.incubationSolProMoyDAO = incubationSolProMoyDAO;
        this.extractionsDAO.put(IIncubationSolProMoyDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_PRO_MOY, incubationSolProMoyDAO);
    }

    /**
     *
     * @param dispositifs
     * @param intervals
     * @return
     */
    @Override
    public Map<String, List<NodeDataSet>> getAvailableVariablesByDispositif(List<Dispositif> dispositifs) {
        Map<String, List<NodeDataSet>> variablesByDatatype = new HashMap();
        extractionsDAO.entrySet().forEach((entry) -> {
            String datatype = entry.getKey();
            IIncubationDAO incubationDAO = entry.getValue();
            variablesByDatatype.put(datatype, incubationDAO.getAvailablesVariablesByDispositif(dispositifs, policyManager.getCurrentUser()));
        });
        return variablesByDatatype;
    }

}
