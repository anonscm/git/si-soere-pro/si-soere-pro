/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.IMesureApportDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordApport extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordApport.class);
    protected static final String BUNDLE_PATH_APPORT = "org.inra.ecoinfo.pro.dataset.itk.messages";

    private static final String MSG_ERROR_OBJET_NOT_DB = "MSG_ERROR_OBJET_NOT_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_LP_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_LP_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_CE_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_CE_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_PR_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_PR_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_CT_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_CT_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_VV_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_VV_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_NA_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_NA_DB";
    private static final String MSG_ERROR_APPORT_LISTEITINERAIRE_MA_DB = "MSG_ERROR_APPORT_LISTEITINERAIRE_MA_DB";

    private static final String MSG_ERROR_APPORT_QA_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_QA_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_PROFONDEUR_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_PROFONDEUR_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_LP_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_LP_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_CE_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_CE_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_MA_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_MA_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_CA_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_CA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_CONDITIONT_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_CONDITIONT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_VENTV_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_VENTV_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_APPORT_NIVEAU_NOT_FOUND_DVU_DB = "MSG_ERROR_APPORT_NIVEAU_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_TRAITEMENT_APPORT_NOT_FOUND_IN_DB = "MSG_ERROR_TRAITEMENT_APPORT_NOT_FOUND_IN_DB";
    protected IMesureApportDAO<MesureApport> mesureApportDAO;
    IListeItineraireDAO listeItineraireDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    public ProcessRecordApport() {
        super();
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<ApportLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<ApportLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur, datasetDescriptorPRO);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordApport.class.getName()).error(ex.getMessage(), ex);
        }
    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureApportDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    private void buildLines(VersionFile versionFile, ISessionPropertiesPRO sessionProperties, ErrorsReport errorsReport, Map<LocalDate, List<ApportLineRecord>> lines,
            SortedSet<ApportLineRecord> ligneEnErreur, DatasetDescriptorPRO datasetDescriptorPRO) throws PersistenceException, InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<ApportLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<ApportLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (ApportLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties, datasetDescriptorPRO);
            }

        }
    }

    private long readLines(CSVParser parser, Map<LocalDate, List<ApportLineRecord>> lines, long lineCount, ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate datedebut = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codedisp = cleanerValues.nextToken();
            final String codetraitement = cleanerValues.nextToken();
            final String parcelle = cleanerValues.nextToken();
            final String placette = cleanerValues.nextToken();
            final String localisationpresice = cleanerValues.nextToken();
            final LocalDate datefin = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String typeintervention = cleanerValues.nextToken();
            final String nomintervention = cleanerValues.nextToken();
            final String compositangrais = cleanerValues.nextToken();
            final String modeapport = cleanerValues.nextToken();
            final String nomcommercial = cleanerValues.nextToken();
            final float qteapport = Float.parseFloat(cleanerValues.nextToken());
            final String uniteapport = cleanerValues.nextToken();
            final String materielapport = cleanerValues.nextToken();
            final float largeurapport = Float.parseFloat(cleanerValues.nextToken());
            final float profondeur = Float.parseFloat(cleanerValues.nextToken());
            final String culture = cleanerValues.nextToken();
            final String bbch = cleanerValues.nextToken();
            final String precisionstade = cleanerValues.nextToken();
            final String condhumidite = cleanerValues.nextToken();
            final String condtemp = cleanerValues.nextToken();
            final String vitessevent = cleanerValues.nextToken();
            final String typeobs = cleanerValues.nextToken();
            final String nomobs = cleanerValues.nextToken();
            final String niveauatteint = cleanerValues.nextToken();
            final String commentaire = cleanerValues.nextToken();

            final ApportLineRecord line = new ApportLineRecord(lineCount, codedisp, codetraitement, parcelle, placette, localisationpresice,
                    datedebut, datefin, typeintervention, nomintervention, compositangrais, modeapport, nomcommercial, qteapport,
                    uniteapport, materielapport, largeurapport, profondeur, culture, bbch, precisionstade, condhumidite, condtemp,
                    vitessevent, typeobs, nomobs, niveauatteint, commentaire);
            try {
                if (!lines.containsKey(datedebut)) {
                    lines.put(datedebut, new LinkedList<ApportLineRecord>());
                }
                lines.get(datedebut).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        datedebut, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void buildMesure(ApportLineRecord line, VersionFile versionFile, SortedSet<ApportLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, ISessionPropertiesPRO sessionProperties, DatasetDescriptorPRO datasetDescriptorPRO) throws PersistenceException {
        final LocalDate datedebut = line.getDate_debut();
        final String codedisp = line.getCode_dispositif();
        String keydisp = Utils.createCodeFromString(sessionProperties.getDispositif().getCode());
        final String codeTrait = Utils.createCodeFromString(line.getCode_traitement());
        String codeunique = keydisp + "_" + codeTrait;
        DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByCodeUnique(codeunique).orElse(null);
        if (dbTraitement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_TRAITEMENT_APPORT_NOT_FOUND_IN_DB), codeTrait, codedisp));
        }
        final String codeParcelle = line.getCode_parcelle();
        final String nomplacette = line.getNom_placette();
        final String localisationprecise = Utils.createCodeFromString(line.getLocalisation_precise());
        final LocalDate datefin = line.getDate_fin();
        final String typeintervention = line.getType_intervention();
        final String intervention = line.getIntervention_apport();
        final String engrais = Utils.createCodeFromString(line.getComposition_angrais());
        final String modeapport = Utils.createCodeFromString(line.getMode_apport());
        final String nomcommercialproduit = line.getNom_commercial();
        float quantiteapport = line.getQuantite_apport();
        final String materiel = line.getMateriel_apport();
        float largeurtravail = line.getLargeur_apport();
        float profondeur = line.getProfondeur();

        final String culture = line.getCulture();
        final String codebbch = line.getCode_bbch();
        final String precisionstade = line.getStade_precision();
        final String conditionhumidite = Utils.createCodeFromString(line.getCondition_humidite());
        final String conditiontemperature = Utils.createCodeFromString(line.getCondition_temperature());
        final String vitessevent = Utils.createCodeFromString(line.getVitesse_vent());
        final String observationqualite = line.getType_observation();
        final String nomobservation = line.getNom_observation();
        final String niveauatteint = (line.getNiveau_atteint());
        final String commentaire = line.getCommentaire();
        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String qapport = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(12));
        String localisation = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(5));
        String compengrais = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(9));
        String modeapp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(10));
        String conditionair = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(20));
        String conditiontemp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(21));
        String vitesse = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(22));
        String largeur = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(15));
        String profond = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(16));

        DatatypeVariableUnitePRO dbdvuLP = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, localisation).orElse(null);
        if (dbdvuLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LP_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, localisation));
        }
        RealNode dbdvuLPRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuLP.getCode())).orElse(null);

        ListeItineraire dbLP = listeItineraireDAO.getByKKey(localisation, localisationprecise).orElse(null);
        if (dbLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LISTEITINERAIRE_LP_DB), localisation, localisationprecise));
        }

        DatatypeVariableUnitePRO dbdvuQA = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, qapport).orElse(null);
        if (dbdvuQA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_QA_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, qapport));
        }
        RealNode dbdvuQARealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuQA.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuCE = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, compengrais).orElse(null);
        if (dbdvuCE == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_CE_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, compengrais));
        }
        RealNode dbdvuCERealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuCE.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvumLT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, largeur).orElse(null);
        if (dbdvumLT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_NIVEAU_NOT_FOUND_DVU_DB), cdatatype, largeur));

        }
        RealNode dbdvumLTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvumLT.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvumPR = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, profond).orElse(null);
        if (dbdvumPR == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_PROFONDEUR_NOT_FOUND_DVU_DB), cdatatype, profond));

        }

        RealNode dbdvumPRRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvumPR.getCode())).orElse(null);

        ListeItineraire dbCE = listeItineraireDAO.getByKKey(compengrais, engrais).orElse(null);
        if (dbCE == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LISTEITINERAIRE_CE_DB), compengrais, engrais));
        }

        DatatypeVariableUnitePRO dbdvuMA = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, modeapp).orElse(null);
        if (dbdvuMA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_MA_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, modeapp));
        }

        RealNode dbdvuMARealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuMA.getCode())).orElse(null);

        ListeItineraire dbMA = listeItineraireDAO.getByKKey(modeapp, modeapport).orElse(null);
        if (dbMA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LISTEITINERAIRE_MA_DB), modeapp, modeapport));
        }

        DatatypeVariableUnitePRO dbdvCA = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditionair).orElse(null);
        if (dbdvCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_CA_NOT_FOUND_DVU_DB), cdatatype, conditionair));
        }

        RealNode dbdvCARealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvCA.getCode())).orElse(null);

        ListeItineraire dbCA = listeItineraireDAO.getByKKey(conditionair, conditionhumidite).orElse(null);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LISTEITINERAIRE_PR_DB), conditionair, conditionhumidite));
        }

        DatatypeVariableUnitePRO dbdvuCT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditiontemp).orElse(null);
        if (dbdvuCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_CONDITIONT_NOT_FOUND_DVU_DB), cdatatype, conditiontemp));

        }
        RealNode dbdvuCTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuCT.getCode())).orElse(null);

        ListeItineraire dbCT = listeItineraireDAO.getByKKey(conditiontemp, conditiontemperature).orElse(null);
        if (dbCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LISTEITINERAIRE_CT_DB), conditiontemp, conditiontemperature));
        }

        DatatypeVariableUnitePRO dbdvuVV = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, vitesse).orElse(null);
        if (dbdvuVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_VENTV_NOT_FOUND_DVU_DB), cdatatype, vitesse));

        }

        RealNode dbdvuVVRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuVV.getCode())).orElse(null);

        ListeItineraire dbVV = listeItineraireDAO.getByKKey(vitesse, vitessevent).orElse(null);
        if (dbVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordApport.BUNDLE_PATH_APPORT,
                    ProcessRecordApport.MSG_ERROR_APPORT_LISTEITINERAIRE_VV_DB), vitesse, vitessevent));
        }

        MesureApport mesureApport = getOrCreate(datedebut, codedisp, codeTrait, codeParcelle, nomplacette, nomcommercialproduit, typeintervention, intervention,
                dbTraitement, datefin, materiel, precisionstade, culture, codebbch, nomobservation, niveauatteint, versionFile, commentaire, observationqualite);

        ValeurApport valeurApportqa = new ValeurApport(quantiteapport, dbdvuQARealNode, mesureApport);
        valeurApportqa.setRealNode(dbdvuQARealNode);
        valeurApportqa.setValeur(quantiteapport);
        valeurApportqa.setMesureinterventionapport(mesureApport);
        mesureApport.getValeurapport().add(valeurApportqa);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApport = new ValeurApport(dbLP, dbdvuLPRealNode, mesureApport);
        valeurApport.setRealNode(dbdvuLPRealNode);
        valeurApport.setListeItineraire(dbLP);
        valeurApport.setMesureinterventionapport(mesureApport);
        mesureApport.getValeurapport().add(valeurApport);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportce = new ValeurApport(dbCE, dbdvuCERealNode, mesureApport);
        valeurApportce.setRealNode(dbdvuCERealNode);
        valeurApportce.setListeItineraire(dbCE);
        mesureApport.getValeurapport().add(valeurApportce);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportma = new ValeurApport(dbMA, dbdvuMARealNode, mesureApport);
        valeurApportma.setRealNode(dbdvuMARealNode);
        valeurApportma.setListeItineraire(dbMA);
        mesureApport.getValeurapport().add(valeurApportma);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportca = new ValeurApport(dbCA, dbdvCARealNode, mesureApport);
        valeurApportca.setRealNode(dbdvCARealNode);
        valeurApportca.setListeItineraire(dbCA);
        mesureApport.getValeurapport().add(valeurApportca);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportct = new ValeurApport(dbCT, dbdvuCTRealNode, mesureApport);
        valeurApportct.setRealNode(dbdvuCTRealNode);
        valeurApportct.setListeItineraire(dbCT);
        valeurApportct.setMesureinterventionapport(mesureApport);
        mesureApport.getValeurapport().add(valeurApportct);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportvv = new ValeurApport(dbVV, dbdvuVVRealNode, mesureApport);
        valeurApportvv.setRealNode(dbdvuVVRealNode);
        valeurApportvv.setListeItineraire(dbVV);
        valeurApportvv.setMesureinterventionapport(mesureApport);
        mesureApport.getValeurapport().add(valeurApportvv);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportna = new ValeurApport(largeurtravail, dbdvumLTRealNode, mesureApport);
        valeurApportna.setValeur(largeurtravail);
        valeurApportna.setRealNode(dbdvumLTRealNode);
        valeurApportna.setMesureinterventionapport(mesureApport);
        mesureApport.getValeurapport().add(valeurApportna);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }

        ValeurApport valeurApportpro = new ValeurApport(profondeur, dbdvumPRRealNode, mesureApport);
        valeurApportpro.setValeur(profondeur);
        valeurApportpro.setRealNode(dbdvumPRRealNode);
        valeurApportpro.setMesureinterventionapport(mesureApport);
        mesureApport.getValeurapport().add(valeurApportpro);
        if (!errorsReport.hasErrors()) {
            mesureApportDAO.saveOrUpdate(mesureApport);
        }
    }

    private MesureApport getOrCreate(final LocalDate datedebut, final String codedisp, final String codeTrait, final String codeParcelle, final String nomplacette, final String nomcommercialproduit, final String typeintervention, final String intervention, DescriptionTraitement dbTraitement, final LocalDate datefin, final String materiel, final String precisionstade, final String culture, final String codebbch, final String nomobservation, final String niveauatteint, VersionFile versionFile, final String commentaire, final String observationqualite) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(datedebut, format);
        String key = dateString + "_" + codedisp + "_" + codeTrait + "_" + codeParcelle + "_" + nomplacette + "_" + nomcommercialproduit + "_" + typeintervention + "_" + intervention;
        MesureApport mesureApport = mesureApportDAO.getByKeys(key).orElse(null);
        if (mesureApport == null) {

            mesureApport = new MesureApport(codedisp, dbTraitement, codeParcelle, nomplacette, datedebut, datefin, typeintervention, intervention,
                    nomcommercialproduit, materiel, precisionstade, culture, codebbch, nomobservation,
                    nomobservation, niveauatteint, versionFile, commentaire);
            mesureApport.setCodedispositif(codedisp);
            mesureApport.setDescriptionTraitement(dbTraitement);
            mesureApport.setNomparcelle(codeParcelle);
            mesureApport.setNomplacette(nomplacette);
            mesureApport.setDatedebut(datedebut);
            mesureApport.setDatefin(datefin);
            mesureApport.setCodebbch(codebbch);
            mesureApport.setNomculture(culture);
            mesureApport.setTypeintervention(typeintervention);
            mesureApport.setNomcommercialproduit(nomcommercialproduit);
            mesureApport.setNiveauatteint(niveauatteint);
            mesureApport.setIntervention(intervention);
            mesureApport.setPrecisionstade(precisionstade);
            mesureApport.setMaterielapport(materiel);
            mesureApport.setTypeobservation(observationqualite);
            mesureApport.setNomobservation(nomobservation);
            mesureApport.setCommentaire(commentaire);
            mesureApport.setKeymesure(key);

        }
        return mesureApport;
    }

    public void setMesureApportDAO(IMesureApportDAO<MesureApport> mesureApportDAO) {
        this.mesureApportDAO = mesureApportDAO;
    }

    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

}
