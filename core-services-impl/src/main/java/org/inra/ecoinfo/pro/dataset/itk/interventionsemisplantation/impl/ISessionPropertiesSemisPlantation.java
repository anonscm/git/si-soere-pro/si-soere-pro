/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.impl;

import java.text.ParseException;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;

/**
 *
 * @author adiankha
 */
public interface ISessionPropertiesSemisPlantation extends ISessionPropertiesPRO{

    /**
     *
     * @param date
     * @param time
     * @return
     * @throws ParseException
     */
    String dateToString(String date, String time) throws ParseException;
    
}
