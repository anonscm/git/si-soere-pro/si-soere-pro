package org.inra.ecoinfo.pro.refdata.booleans;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Booleans> {

    IBooleanDAO boolDAO;
    Properties LibelleEn;

    private void createBoolean(final Booleans bool) throws BusinessException {
        try {
            boolDAO.saveOrUpdate(bool);
            boolDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void updateBDBoolean(final String code, final String libelle, final Booleans dbbool) throws BusinessException {
        try {
            dbbool.setCode(code);
            dbbool.setLibelle(libelle);
            boolDAO.saveOrUpdate(dbbool);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update booleans");
        }
    }

    private void createOrUpdateBoolean(final String code, final String libelle, final Booleans dbbool) throws BusinessException {
        if (dbbool == null) {
            Booleans bool = new Booleans(libelle);
            bool.setCode(code);
            bool.setLibelle(libelle);
            createBoolean(bool);
        } else {
            updateBDBoolean(code, libelle, dbbool);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Booleans.NAME_ENTITY_JPA);
                final String faux = tokenizerValues.nextToken();
                boolDAO.remove(boolDAO.getByNKey(faux).orElseThrow(PersistenceException::new));
                values = parser.getLine();
            }
        } catch (final PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Booleans> getAllElements() throws BusinessException {
        return boolDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IBooleanDAO getBoolDAO() {
        return boolDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Booleans booleans) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(booleans == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING : booleans.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(booleans == null || booleans.getLibelle() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : LibelleEn.getProperty(booleans.getLibelle()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));

        return lineModelGridMetadata;
    }

    private void persistBoolean(final String libelle, final String code) throws BusinessException {
        final Booleans dbboole = boolDAO.getByNKey(libelle).orElse(null);
        createOrUpdateBoolean(code, libelle, dbboole);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Booleans.NAME_ENTITY_JPA);
                final String libelle = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(libelle);
                persistBoolean(libelle, code);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    @Override
    protected ModelGridMetadata<Booleans> initModelGridMetadata() {
        LibelleEn = localizationManager.newProperties(Booleans.NAME_ENTITY_JPA, Booleans.JPA_COLUMN_LIBELLE, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

}
