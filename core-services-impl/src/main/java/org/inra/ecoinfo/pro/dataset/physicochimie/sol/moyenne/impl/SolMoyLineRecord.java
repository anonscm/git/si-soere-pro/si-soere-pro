/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.impl;

import java.time.LocalDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author adiankha
 */
public class SolMoyLineRecord implements Comparable<SolMoyLineRecord> {

    LocalDate dateprelevement;
    Long originalLineNumber;
    String codetraitement;
    String nomlabo;
    String nomcouche;
    double limitsup;
    double limitinf;
    int numerorepet;
    String codevariable;
    String codeunite;
    String codemethode;
    String codehumidite;
    float moyenne;
    float ecarttype;

    /**
     *
     * @param dateprelevement
     * @param originalLineNumber
     * @param codetraitement
     * @param nomlabo
     * @param nomcouche
     * @param limitsup
     * @param limitinf
     * @param numerorepet
     * @param codevariable
     * @param moyenne
     * @param ecarttype
     * @param codeunite
     * @param codemethode
     * @param codehumidite
     */
    public SolMoyLineRecord(LocalDate dateprelevement, Long originalLineNumber, String codetraitement, String nomlabo, String nomcouche, double limitsup, double limitinf, int numerorepet, String codevariable, float moyenne,
            float ecarttype, String codeunite, String codemethode,
            String codehumidite) {
        this.dateprelevement = dateprelevement;
        this.originalLineNumber = originalLineNumber;
        this.codetraitement = codetraitement;
        this.nomlabo = nomlabo;
        this.nomcouche = nomcouche;
        this.limitsup = limitsup;
        this.limitinf = limitinf;
        this.numerorepet = numerorepet;
        this.codehumidite = codehumidite;
        this.codemethode = codemethode;
        this.moyenne = moyenne;
        this.ecarttype = ecarttype;
        this.codeunite = codeunite;
        this.codevariable = codevariable;

    }

    /**
     *
     * @param line
     */
    public void copy(SolMoyLineRecord line) {
        this.dateprelevement = line.getDateprelevement();
        this.nomcouche = line.getNomcouche();
        this.limitsup = line.getLimitsup();
        this.limitinf = line.getLimitinf();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.numerorepet = line.getNumerorepet();
        this.nomlabo = line.getNomlabo();
        this.codevariable = line.getCodevariable();
        this.codehumidite = line.getCodehumidite();
        this.moyenne = line.getMoyenne();
        this.codeunite = line.getCodeunite();
        this.ecarttype = line.getEcarttype();
        this.codemethode = line.getCodemethode();

    }

    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public int compareTo(SolMoyLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        }
        returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        return returnValue;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    /**
     *
     * @param dateprelevement
     */
    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodetraitement() {
        return codetraitement;
    }

    /**
     *
     * @param codetraitement
     */
    public void setCodetraitement(String codetraitement) {
        this.codetraitement = codetraitement;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public String getNomcouche() {
        return nomcouche;
    }

    /**
     *
     * @param nomcouche
     */
    public void setNomcouche(String nomcouche) {
        this.nomcouche = nomcouche;
    }

    /**
     *
     * @return
     */
    public double getLimitsup() {
        return limitsup;
    }

    /**
     *
     * @param limitsup
     */
    public void setLimitsup(double limitsup) {
        this.limitsup = limitsup;
    }

    /**
     *
     * @return
     */
    public double getLimitinf() {
        return limitinf;
    }

    /**
     *
     * @param limitinf
     */
    public void setLimitinf(double limitinf) {
        this.limitinf = limitinf;
    }

    /**
     *
     * @return
     */
    public int getNumerorepet() {
        return numerorepet;
    }

    /**
     *
     * @param numerorepet
     */
    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public float getMoyenne() {
        return moyenne;
    }

    /**
     *
     * @param moyenne
     */
    public void setMoyenne(float moyenne) {
        this.moyenne = moyenne;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

}
