/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.apport.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport_;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.jpa.JPAITKDAO;
import org.inra.ecoinfo.pro.synthesis.apport.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPAApportDAO extends JPAITKDAO<MesureApport, ValeurApport> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurApport> getValeurITKClass() {
        return ValeurApport.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureApport> getMesureITKClass() {
        return MesureApport.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurApport, MesureApport> getMesureAttribute() {
        return ValeurApport_.mesureinterventionapport;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

}
