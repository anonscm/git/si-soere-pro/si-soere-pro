/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.utils.filenamecheckers;

/**
 *
 * @author adiankha
 */
public class FileNameChekerddmmyyyyhhmmss extends AbstractPROFileNameChecker {

    public static final String DATE_PATTERN = "dd-MM-yyyy-HHmmss" ;

    @Override
    protected String getDatePattern() {
        return DATE_PATTERN;
    }

}
