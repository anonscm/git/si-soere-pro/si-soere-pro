/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dureeconservation;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IDureeConcervationDAO extends IDAO<DureeConservation>{

    /**
     *
     * @return
     */
    List<DureeConservation> gelAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<DureeConservation> getByNKey(String nom);
    
}
