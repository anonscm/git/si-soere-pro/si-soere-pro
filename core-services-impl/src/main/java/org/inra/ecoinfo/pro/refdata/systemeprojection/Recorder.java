package org.inra.ecoinfo.pro.refdata.systemeprojection;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<SystemeProjection> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ISystemeProjectionDAO systemeProjectionDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String nom = tokenizerValues.nextToken();
                
                systemeProjectionDAO.remove(systemeProjectionDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get systeme projection")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<SystemeProjection> getAllElements() throws BusinessException {
        return systemeProjectionDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(SystemeProjection systemeProjection) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesDefinition = localizationManager.newProperties(SystemeProjection.TABLE_NAME, RefDataConstantes.COLUMN_DEFINITION_SYSTEMEPROJ, Locale.ENGLISH);
        
        String localizedChampDefinition = "";
        
        if (systemeProjection != null) {
            localizedChampDefinition = propertiesDefinition.containsKey(systemeProjection.getDefinition()) ? propertiesDefinition.getProperty(systemeProjection.getDefinition()) : systemeProjection.getDefinition();
        }
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(systemeProjection == null ? Constantes.STRING_EMPTY : systemeProjection.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(systemeProjection == null ? Constantes.STRING_EMPTY : systemeProjection.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(systemeProjection == null ? Constantes.STRING_EMPTY : localizedChampDefinition, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values, SystemeProjection.TABLE_NAME);
                
                String nom = tokenizerValues.nextToken();
                String definition = tokenizerValues.nextToken();
                
                String code = Utils.createCodeFromString(nom);
                
                SystemeProjection systemeProjection = new SystemeProjection(nom, code, definition);
                
                SystemeProjection dbSystemeProjection = systemeProjectionDAO.getByCode(code).orElse(null);
                
                if (dbSystemeProjection == null) {
                    systemeProjectionDAO.saveOrUpdate(systemeProjection);
                } else {
                    dbSystemeProjection.setDefinition(definition);
                    
                    systemeProjectionDAO.saveOrUpdate(dbSystemeProjection);
                }
                
                values = parser.getLine();
                
            }            
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (NoResultException e2) {
            throw new BusinessException(e2.getMessage(), e2);
        } catch (NonUniqueResultException e3) {
            throw new BusinessException(e3.getMessage(), e3);
        }
    }

    /**
     * @param systemeProjectionDAO the systemeProjectionDAO to set
     */
    public void setSystemeProjectionDAO(ISystemeProjectionDAO systemeProjectionDAO) {
        this.systemeProjectionDAO = systemeProjectionDAO;
    }
    
}
