/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport.impl;

import java.time.LocalDate;

/**
 *
 * @author vjkoyao
 */
public class ApportLineRecord implements Comparable<ApportLineRecord> {

    Long originalLineNumber;
    String code_dispositif;
    String code_traitement;
    String code_parcelle;
    String nom_placette;
    String localisation_precise;
    LocalDate date_debut;
    LocalDate date_fin;
    String type_intervention;
    String intervention_apport;
    String composition_angrais;
    String mode_apport;
    String nom_commercial;
    float quantite_apport;
    String unite_apport;
    String materiel_apport;
    float largeur_apport;
    float profondeur;
    String culture;
    String code_bbch;
    String stade_precision;
    String condition_humidite;
    String condition_temperature;
    String vitesse_vent;
    String type_observation;
    String nom_observation;
    String niveau_atteint;
    String commentaire;

    /**
     *
     */
    public ApportLineRecord() {
        super();
    }

    /**
     *
     * @param originalLineNumber
     * @param code_dispositif
     * @param code_traitement
     * @param code_parcelle
     * @param nom_placette
     * @param localisation_precise
     * @param date_debut
     * @param date_fin
     * @param type_intervention
     * @param intervention_apport
     * @param composition_angrais
     * @param mode_apport
     * @param nom_commercial
     * @param quantite_apport
     * @param unite_apport
     * @param materiel_apport
     * @param largeur_apport
     * @param profondeur
     * @param culture
     * @param code_bbch
     * @param stade_precision
     * @param condition_humidite
     * @param condition_temperature
     * @param vitesse_vent
     * @param type_observation
     * @param nom_observation
     * @param niveau_atteint
     * @param commentaire
     */
    public ApportLineRecord(Long originalLineNumber, String code_dispositif, String code_traitement, String code_parcelle, String nom_placette, String localisation_precise, LocalDate date_debut, LocalDate date_fin, String type_intervention, String intervention_apport, String composition_angrais, String mode_apport, String nom_commercial, float quantite_apport, String unite_apport, String materiel_apport, float largeur_apport, float profondeur, String culture, String code_bbch, String stade_precision, String condition_humidite, String condition_temperature, String vitesse_vent, String type_observation, String nom_observation, String niveau_atteint, String commentaire) {
        this.originalLineNumber = originalLineNumber;
        this.code_dispositif = code_dispositif;
        this.code_traitement = code_traitement;
        this.code_parcelle = code_parcelle;
        this.nom_placette = nom_placette;
        this.localisation_precise = localisation_precise;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.type_intervention = type_intervention;
        this.intervention_apport = intervention_apport;
        this.composition_angrais = composition_angrais;
        this.mode_apport = mode_apport;
        this.nom_commercial = nom_commercial;
        this.quantite_apport = quantite_apport;
        this.unite_apport = unite_apport;
        this.materiel_apport = materiel_apport;
        this.largeur_apport = largeur_apport;
        this.profondeur = profondeur;
        this.culture = culture;
        this.code_bbch = code_bbch;
        this.stade_precision = stade_precision;
        this.condition_humidite = condition_humidite;
        this.condition_temperature = condition_temperature;
        this.vitesse_vent = vitesse_vent;
        this.type_observation = type_observation;
        this.nom_observation = nom_observation;
        this.niveau_atteint = niveau_atteint;
        this.commentaire = commentaire;
    }

    void copy(final ApportLineRecord line) {
        this.originalLineNumber = line.getOriginalLineNumber();
        this.code_dispositif = line.getCode_dispositif();
        this.code_traitement = line.getCode_traitement();
        this.code_parcelle = line.getCode_parcelle();
        this.nom_placette = line.getNom_placette();
        this.localisation_precise = line.getLocalisation_precise();
        this.date_debut = line.getDate_debut();
        this.date_fin = line.getDate_fin();
        this.type_intervention = line.getType_intervention();
        this.intervention_apport = line.getIntervention_apport();
        this.composition_angrais = line.getComposition_angrais();
        this.mode_apport = line.getMode_apport();
        this.nom_commercial = line.getNom_commercial();
        this.quantite_apport = line.getQuantite_apport();
        this.unite_apport = line.getUnite_apport();
        this.materiel_apport = line.getMateriel_apport();
        this.largeur_apport = line.getLargeur_apport();
        this.culture = line.getCulture();
        this.code_bbch = line.getCode_bbch();
        this.stade_precision = line.getStade_precision();
        this.condition_humidite = line.getCondition_humidite();
        this.condition_temperature = line.getCondition_temperature();
        this.vitesse_vent = line.getVitesse_vent();
        this.type_observation = line.getType_observation();
        this.nom_observation = line.getNom_observation();
        this.niveau_atteint = line.getNiveau_atteint();
        this.commentaire = line.getCommentaire();

    }

    @Override
    public int compareTo(ApportLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCode_dispositif() {
        return code_dispositif;
    }

    /**
     *
     * @param code_dispositif
     */
    public void setCode_dispositif(String code_dispositif) {
        this.code_dispositif = code_dispositif;
    }

    /**
     *
     * @return
     */
    public String getCode_traitement() {
        return code_traitement;
    }

    /**
     *
     * @param code_traitement
     */
    public void setCode_traitement(String code_traitement) {
        this.code_traitement = code_traitement;
    }

    /**
     *
     * @return
     */
    public String getCode_parcelle() {
        return code_parcelle;
    }

    /**
     *
     * @param code_parcelle
     */
    public void setCode_parcelle(String code_parcelle) {
        this.code_parcelle = code_parcelle;
    }

    /**
     *
     * @return
     */
    public String getNom_placette() {
        return nom_placette;
    }

    /**
     *
     * @param nom_placette
     */
    public void setNom_placette(String nom_placette) {
        this.nom_placette = nom_placette;
    }

    /**
     *
     * @return
     */
    public String getLocalisation_precise() {
        return localisation_precise;
    }

    /**
     *
     * @param localisation_precise
     */
    public void setLocalisation_precise(String localisation_precise) {
        this.localisation_precise = localisation_precise;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_debut() {
        return date_debut;
    }

    /**
     *
     * @param date_debut
     */
    public void setDate_debut(LocalDate date_debut) {
        this.date_debut = date_debut;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_fin() {
        return date_fin;
    }

    /**
     *
     * @param date_fin
     */
    public void setDate_fin(LocalDate date_fin) {
        this.date_fin = date_fin;
    }

    /**
     *
     * @return
     */
    public String getType_intervention() {
        return type_intervention;
    }

    /**
     *
     * @param type_intervention
     */
    public void setType_intervention(String type_intervention) {
        this.type_intervention = type_intervention;
    }

    /**
     *
     * @return
     */
    public String getIntervention_apport() {
        return intervention_apport;
    }

    /**
     *
     * @param intervention_apport
     */
    public void setIntervention_apport(String intervention_apport) {
        this.intervention_apport = intervention_apport;
    }

    /**
     *
     * @return
     */
    public String getComposition_angrais() {
        return composition_angrais;
    }

    /**
     *
     * @param composition_angrais
     */
    public void setComposition_angrais(String composition_angrais) {
        this.composition_angrais = composition_angrais;
    }

    /**
     *
     * @return
     */
    public String getMode_apport() {
        return mode_apport;
    }

    /**
     *
     * @param mode_apport
     */
    public void setMode_apport(String mode_apport) {
        this.mode_apport = mode_apport;
    }

    /**
     *
     * @return
     */
    public String getNom_commercial() {
        return nom_commercial;
    }

    /**
     *
     * @param nom_commercial
     */
    public void setNom_commercial(String nom_commercial) {
        this.nom_commercial = nom_commercial;
    }

    /**
     *
     * @return
     */
    public Float getQuantite_apport() {
        return quantite_apport;
    }

    /**
     *
     * @param quantite_apport
     */
    public void setQuantite_apport(Float quantite_apport) {
        this.quantite_apport = quantite_apport;
    }

    /**
     *
     * @return
     */
    public String getUnite_apport() {
        return unite_apport;
    }

    /**
     *
     * @param unite_apport
     */
    public void setUnite_apport(String unite_apport) {
        this.unite_apport = unite_apport;
    }

    /**
     *
     * @return
     */
    public String getMateriel_apport() {
        return materiel_apport;
    }

    /**
     *
     * @param materiel_apport
     */
    public void setMateriel_apport(String materiel_apport) {
        this.materiel_apport = materiel_apport;
    }

    /**
     *
     * @return
     */
    public Float getLargeur_apport() {
        return largeur_apport;
    }

    /**
     *
     * @param largeur_apport
     */
    public void setLargeur_apport(Float largeur_apport) {
        this.largeur_apport = largeur_apport;
    }

    /**
     *
     * @return
     */
    public String getCulture() {
        return culture;
    }

    /**
     *
     * @param culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     *
     * @return
     */
    public String getCode_bbch() {
        return code_bbch;
    }

    /**
     *
     * @param code_bbch
     */
    public void setCode_bbch(String code_bbch) {
        this.code_bbch = code_bbch;
    }

    /**
     *
     * @return
     */
    public String getStade_precision() {
        return stade_precision;
    }

    /**
     *
     * @param stade_precision
     */
    public void setStade_precision(String stade_precision) {
        this.stade_precision = stade_precision;
    }

    /**
     *
     * @return
     */
    public String getCondition_humidite() {
        return condition_humidite;
    }

    /**
     *
     * @param condition_humidite
     */
    public void setCondition_humidite(String condition_humidite) {
        this.condition_humidite = condition_humidite;
    }

    /**
     *
     * @return
     */
    public String getCondition_temperature() {
        return condition_temperature;
    }

    /**
     *
     * @param condition_temperature
     */
    public void setCondition_temperature(String condition_temperature) {
        this.condition_temperature = condition_temperature;
    }

    /**
     *
     * @return
     */
    public String getVitesse_vent() {
        return vitesse_vent;
    }

    /**
     *
     * @param vitesse_vent
     */
    public void setVitesse_vent(String vitesse_vent) {
        this.vitesse_vent = vitesse_vent;
    }

    /**
     *
     * @return
     */
    public String getType_observation() {
        return type_observation;
    }

    /**
     *
     * @param type_observation
     */
    public void setType_observation(String type_observation) {
        this.type_observation = type_observation;
    }

    /**
     *
     * @return
     */
    public String getNom_observation() {
        return nom_observation;
    }

    /**
     *
     * @param nom_observation
     */
    public void setNom_observation(String nom_observation) {
        this.nom_observation = nom_observation;
    }

    /**
     *
     * @return
     */
    public String getNiveau_atteint() {
        return niveau_atteint;
    }

    /**
     *
     * @param niveau_atteint
     */
    public void setNiveau_atteint(String niveau_atteint) {
        this.niveau_atteint = niveau_atteint;
    }

    /**
     *
     * @return
     */
    public String getCommentaire() {
        return commentaire;
    }

    /**
     *
     * @param commentaire
     */
    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     *
     * @param quantite_apport
     */
    public void setQuantite_apport(float quantite_apport) {
        this.quantite_apport = quantite_apport;
    }

    /**
     *
     * @param largeur_apport
     */
    public void setLargeur_apport(float largeur_apport) {
        this.largeur_apport = largeur_apport;
    }

    /**
     *
     * @return
     */
    public float getProfondeur() {
        return profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(float profondeur) {
        this.profondeur = profondeur;
    }

}
