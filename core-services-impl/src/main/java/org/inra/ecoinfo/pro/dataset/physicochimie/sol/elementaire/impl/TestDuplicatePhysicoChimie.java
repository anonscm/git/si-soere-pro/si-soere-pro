/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;

        

/**
 *
 * @author adiankha
 */
public class TestDuplicatePhysicoChimie extends AbstractTestDuplicate {
    protected static final String BUNDLE_PATH_SOL_BRUT = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.messages";
    static final String  PROPERTY_MSG_DUPLICATE_LINE_SOL_ELEMENTAIRE ="PROPERTY_MSG_DUPLICATE_LINE_SOL_ELEMENTAIRE";
        
     SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine = null;
     final SortedMap<String, Long>   line;
     final SortedMap<String, String> lineCouvert;
    
    /**
     *
     */
    public TestDuplicatePhysicoChimie() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
     } 
    
    /**
     *
     * @param date
     * @param codeEchan
     * @param nomLabo
     * @param codevariable
     * @param methode
     * @param code_unite
     * @param code_humidite
     * @param lineNumber
     */
    protected void addLine( final String date,String codeEchan, final String nomLabo,
                  final String codevariable,final String methode,final String code_unite, final String code_humidite ,final long lineNumber) {
             final String key = this.getKey( date,codeEchan, nomLabo,codevariable,methode,code_unite,code_humidite);
             final String keySol = this.getKey(date,codeEchan,codevariable,methode,code_unite,code_humidite);
             if (!this.line.containsKey(key)) {
                 this.line.put(key, lineNumber);
                 if (!this.lineCouvert.containsKey(keySol)) {
                     this.lineCouvert.put(keySol, nomLabo);
                 } else if (!this.lineCouvert.get(keySol).equals(nomLabo)) {
                     this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle
                             (TestDuplicatePhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                                     TestDuplicatePhysicoChimie.PROPERTY_MSG_DUPLICATE_LINE_SOL_ELEMENTAIRE), lineNumber,
                                      date,codeEchan,codevariable,methode, this.lineCouvert.get(keySol),nomLabo));
                 }
             } else {
                 this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                         TestDuplicatePhysicoChimie.BUNDLE_PATH_SOL_BRUT,
                         TestDuplicatePhysicoChimie.PROPERTY_MSG_DUPLICATE_LINE_SOL_ELEMENTAIRE),lineNumber,date, codeEchan,nomLabo,codevariable,
                         methode,code_unite,code_humidite,this.line.get(key)));
        

   
    }
}

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0],values[1],values[2],values[5],values[8],values[9],values[10],lineNumber);
    }
}
