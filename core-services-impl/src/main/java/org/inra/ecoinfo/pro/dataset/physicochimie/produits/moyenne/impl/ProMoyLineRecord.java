/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.impl;

import java.time.LocalDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author adiankha
 */
public class ProMoyLineRecord implements Comparable<ProMoyLineRecord>{
    
    
    LocalDate dateprelevement;
    Long originalLineNumber;
    String codeprod;
    String nomlieu;
    String nomlabo;
    int numerorepet ;
    String codevariable;
    String codeunite;
    String codemethode;
    String codehumidite;
    float moyenne;
    float ecarttype;
    
    /**
     *
     */
    public ProMoyLineRecord(){
        super();
    }

    /**
     *
     * @param dateprelevement
     * @param originalLineNumber
     * @param codeprod
     * @param nomlieu
     * @param nomlabo
     * @param numerorepet
     * @param codevariable
     * @param codeunite
     * @param codemethode
     * @param codehumidite
     * @param moyenne
     * @param ecarttype
     */
    public ProMoyLineRecord(LocalDate dateprelevement, Long originalLineNumber, String codeprod, String nomlieu, String nomlabo, int numerorepet,
                      String codevariable,String codeunite,String codemethode,
    String codehumidite, float moyenne,
    float ecarttype ) {
        this.dateprelevement = dateprelevement;
        this.originalLineNumber = originalLineNumber;
        this.codeprod = codeprod;
        this.nomlieu = nomlieu;
        this.nomlabo = nomlabo;
        this.numerorepet = numerorepet;
        this.codehumidite = codehumidite;
        this.codemethode = codemethode;
        this.moyenne = moyenne;
        this.ecarttype = ecarttype;
        this.codeunite = codeunite;
        this.codevariable = codevariable;
    }
    
    /**
     *
     * @param line
     */
    public void copy(ProMoyLineRecord line){
       this.dateprelevement = line.getDateprelevement();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.codeprod = line.getCodeprod();
        this.nomlieu = line.getNomlieu();
        this.nomlabo = line.getNomlabo();
        this.numerorepet = line.getNumerorepet();
        this.codevariable = line.getCodevariable();
        this.codehumidite = line.getCodehumidite();
        this.moyenne = line.getMoyenne();
        this.codeunite = line.getCodeunite();
        this.ecarttype = line.getEcarttype();
        this.codemethode = line.getCodemethode();
   }

    @Override
    public int compareTo(ProMoyLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }
    
    @Override
    public boolean equals(Object obj) {
         return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    /**
     *
     * @param dateprelevement
     */
    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodeprod() {
        return codeprod;
    }

    /**
     *
     * @param codeprod
     */
    public void setCodeprod(String codeprod) {
        this.codeprod = codeprod;
    }

    /**
     *
     * @return
     */
    public String getNomlieu() {
        return nomlieu;
    }

    /**
     *
     * @param nomlieu
     */
    public void setNomlieu(String nomlieu) {
        this.nomlieu = nomlieu;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public int getNumerorepet() {
        return numerorepet;
    }

    /**
     *
     * @param numerorepet
     */
    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public float getMoyenne() {
        return moyenne;
    }

    /**
     *
     * @param moyenne
     */
    public void setMoyenne(float moyenne) {
        this.moyenne = moyenne;
    }

    /**
     *
     * @return
     */
    public float getEcarttype() {
        return ecarttype;
    }

    /**
     *
     * @param ecarttype
     */
    public void setEcarttype(float ecarttype) {
        this.ecarttype = ecarttype;
    }

   

   
    
    
    
    
}
