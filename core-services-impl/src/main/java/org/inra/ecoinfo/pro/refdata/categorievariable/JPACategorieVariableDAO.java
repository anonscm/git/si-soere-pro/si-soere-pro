/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.categorievariable;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPACategorieVariableDAO extends AbstractJPADAO<CategorieVariable> implements ICategorieVariableDAO {

    /**
     *
     * @return
     */
    @Override
    public List<CategorieVariable> gettAll() {
        return getAllBy(CategorieVariable.class, CategorieVariable::getCvariable_mycode);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<CategorieVariable> getByName(String nom) {
        CriteriaQuery<CategorieVariable> query = builder.createQuery(CategorieVariable.class);
        Root<CategorieVariable> categorieVariable = query.from(CategorieVariable.class);
        query
                .select(categorieVariable)
                .where(
                        builder.equal(categorieVariable.get(CategorieVariable_.cvariable_nom), nom)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codecvariable
     * @return
     */
    @Override
    public Optional<CategorieVariable> getByNKey(String codecvariable) {
        CriteriaQuery<CategorieVariable> query = builder.createQuery(CategorieVariable.class);
        Root<CategorieVariable> categorieVariable = query.from(CategorieVariable.class);
        query
                .select(categorieVariable)
                .where(
                        builder.equal(categorieVariable.get(CategorieVariable_.cvariable_code), codecvariable)
                );
        return getOptional(query);
    }

    /**
     *
     * @param mycode
     * @return
     */
    @Override
    public Optional<CategorieVariable> getByMyKey(String mycode) {
        CriteriaQuery<CategorieVariable> query = builder.createQuery(CategorieVariable.class);
        Root<CategorieVariable> categorieVariable = query.from(CategorieVariable.class);
        query
                .select(categorieVariable)
                .where(
                        builder.equal(categorieVariable.get(CategorieVariable_.cvariable_mycode), mycode)
                );
        return getOptional(query);
    }

}
