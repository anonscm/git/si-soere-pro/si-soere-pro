/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public interface IDeleteRecord {

    /**
     *
     * @param versionFile
     * @throws BusinessException
     */
    void deleteRecord(VersionFile versionFile) throws BusinessException;
}
