package org.inra.ecoinfo.pro.refdata.activiteindustrielle;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IActiviteIndustrielleDAO extends IDAO<Activiteindustrielle>{

    /**
     *
     * @return
     */
    List<Activiteindustrielle> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Activiteindustrielle> getByNKey(String nom);

}
