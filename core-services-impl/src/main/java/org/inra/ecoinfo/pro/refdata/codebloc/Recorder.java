package org.inra.ecoinfo.pro.refdata.codebloc;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<CodeBloc> {

    ICodeBlocDAO codeBlocDAO;

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String code = tokenizerValues.nextToken();

                codeBlocDAO.remove(codeBlocDAO.getByNKey(code).orElseThrow(PersistenceException::new));

                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<CodeBloc> getAllElements() throws BusinessException {
        return codeBlocDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CodeBloc codebloc) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(codebloc == null ? Constantes.STRING_EMPTY : codebloc.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        return lineModelGridMetadata;
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String code = tokenizerValues.nextToken();

                CodeBloc codeBloc = new CodeBloc(code);

                CodeBloc dbCodeBloc = codeBlocDAO.getByNKey(code).orElse(null);

                if (dbCodeBloc == null) {
                    codeBlocDAO.saveOrUpdate(codeBloc);
                }

                values = parser.getLine();
            }
        } catch (PersistenceException | IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param codeBlocDAO the codeBlocDAO to set
     */
    public void setCodeBlocDAO(ICodeBlocDAO codeBlocDAO) {
        this.codeBlocDAO = codeBlocDAO;
    }

}
