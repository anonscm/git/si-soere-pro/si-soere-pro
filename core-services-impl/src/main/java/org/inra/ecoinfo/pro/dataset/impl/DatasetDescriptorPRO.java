/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class DatasetDescriptorPRO extends DatasetDescriptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatasetDescriptorPRO.class
            .getName());

    int enTete;

    int headersLine = -1;

    String variableName;

    /**
     *
     * @param nom
     * @return
     */
    public Column getColumn(final String nom) {
        for (final Column column : this.getColumns()) {
            if (column.getName().equals(nom)) {
                return column;
            }
        }
        return null;
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    public String getColumnName(final int columnIndex) {
        return this.getColumns().get(columnIndex).getName();
    }

    /**
     *
     * @return
     */
    public int getEnTete() {
        return this.enTete;
    }

    /**
     *
     * @return
     */
    public int getHeadersLine() {
        return this.headersLine;
    }

    /**
     *
     * @param columnIndex
     * @return
     */
    public int getReferencedColumn(final int columnIndex) {
        if (columnIndex > this.getColumns().size()) {
            return -1;
        }
        final String referencedName = this.getColumns().get(columnIndex).getRefVariableName();
        if (referencedName != null) {
            return this.getColumns().indexOf(this.getColumn(referencedName));
        }
        return -1;
    }

    /**
     *
     * @return
     */
    public String getVariableName() {
        return this.variableName;
    }

    /**
     *
     * @param enTete
     */
    public void setEnTete(final int enTete) {
        this.enTete = enTete;
    }

    /**
     *
     * @param enTete
     */
    public void setEnTete(final String enTete) {
        this.enTete = Integer.parseInt(enTete);
    }

    /**
     *
     * @param headersLine
     */
    public void setHeadersLine(final String headersLine) {
        try {
            this.headersLine = Integer.parseInt(headersLine);
        } catch (final NumberFormatException e) {
            DatasetDescriptorPRO.LOGGER.info("can't parse headersLine");
        }
    }

    /**
     *
     * @param variableName
     */
    public void setVariableName(final String variableName) {
        this.variableName = variableName;
    }

    /**
     *
     * @param headersLine
     */
    public void setHeadersLine(int headersLine) {
        this.headersLine = headersLine;
    }
}
