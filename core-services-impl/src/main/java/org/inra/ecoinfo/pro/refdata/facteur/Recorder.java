/**
 *
 */
package org.inra.ecoinfo.pro.refdata.facteur;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.typefacteur.ITypeFacteurDAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Facteur> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ITypeFacteurDAO typeFacteurDAO;
    IFacteurDAO facteurDAO;

    /**
     * @param typeFacteur
     * @param libelle
     * @param isInfosSupplApportFert
     * @param dbFacteur
     * @param facteur
     * @throws PersistenceException
     */
    private void createOrUpdate(TypeFacteur typeFacteur, String libelle, String code, Boolean isInfosSupplApportFert, Boolean isAssocNomen, Facteur dbFacteur, Facteur facteur) throws BusinessException {
        try {
            if (dbFacteur == null) {
                facteurDAO.saveOrUpdate(facteur);
            } else {
                dbFacteur.setLibelle(libelle);
                dbFacteur.setCode(code);
                dbFacteur.setTypeFacteur(typeFacteur);
                dbFacteur.setIsInfosSupplApportFert(isInfosSupplApportFert);
                dbFacteur.setIsAssocNomenclature(isAssocNomen);

                facteurDAO.saveOrUpdate(dbFacteur);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create or update facteur");
        }
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getLibelleTypeFacteurPossibles() throws BusinessException {
        List<TypeFacteur> lstTypeFacteurs = typeFacteurDAO.getAll();
        String[] typeFacteurPossibles = new String[lstTypeFacteurs.size()];
        int index = 0;
        for (TypeFacteur typeFacteur : lstTypeFacteurs) {
            typeFacteurPossibles[index++] = typeFacteur.getLibelle();
        }
        return typeFacteurPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String libelle = tokenizerValues.nextToken();
                facteurDAO.remove(facteurDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get facteur")));
                values = parser.getLine();
            }
        } catch (IOException |PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Facteur> getAllElements() throws BusinessException {
        return facteurDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Facteur facteur) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesNameLibelle = localizationManager.newProperties(Facteur.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_FCT, Locale.ENGLISH);

        String localizedChampNameLibelle = "";

        if (facteur != null) {
            localizedChampNameLibelle = propertiesNameLibelle.containsKey(facteur.getLibelle())
                    ? propertiesNameLibelle.getProperty(facteur.getLibelle()) : facteur.getLibelle();
        }

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(facteur == null
                ? Constantes.STRING_EMPTY : facteur.getTypeFacteur().getLibelle(), getLibelleTypeFacteurPossibles(), null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(facteur == null
                ? Constantes.STRING_EMPTY : facteur.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(facteur == null
                ? Constantes.STRING_EMPTY : localizedChampNameLibelle,
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        String isInfosSupplApportFertilisant = facteur == null ? ""
                : (facteur.getIsInfosSupplApportFert() ? RefDataConstantes.valBooleanObligatoire[0] : RefDataConstantes.valBooleanObligatoire[1]);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(facteur == null
                ? Constantes.STRING_EMPTY : isInfosSupplApportFertilisant, RefDataConstantes.valBooleanObligatoireInv, null, true, false, true));

        String isAssocNomenclature = facteur == null ? ""
                : (facteur.getIsAssocNomenclature() ? RefDataConstantes.valBooleanObligatoire[0] : RefDataConstantes.valBooleanObligatoire[1]);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(facteur == null
                ? Constantes.STRING_EMPTY : isAssocNomenclature, RefDataConstantes.valBooleanObligatoireInv, null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Facteur.TABLE_NAME);

                String libelleTypeFacteur = tokenizerValues.nextToken();
                int indextype = tokenizerValues.currentTokenIndex();
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                String isInfosSupplApportFertilisant = tokenizerValues.nextToken();
                String isAssocNomenclature = tokenizerValues.nextToken();

                String code = Utils.createCodeFromString(libelle);

                TypeFacteur typeFacteur = typeFacteurDAO.getByNKey(libelleTypeFacteur).orElse(null);
                if (typeFacteur == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "TYPEFACTEUR_NONDEFINI"), line + 1, indextype, libelleTypeFacteur));
                }

                if (libelle == null || libelle.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(1).getName()));
                }

                Facteur facteur = new Facteur(typeFacteur, libelle, code);

                Boolean isInfosSupplApportFert = isInfosSupplApportFertilisant.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[0]) ? Boolean.TRUE : Boolean.FALSE;
                facteur.setIsInfosSupplApportFert(isInfosSupplApportFert);

                Boolean isAssocNomen = isAssocNomenclature.equalsIgnoreCase(RefDataConstantes.valBooleanObligatoire[0]) ? Boolean.TRUE : Boolean.FALSE;
                facteur.setIsAssocNomenclature(isAssocNomen);

                if (!errorsReport.hasErrors()) {
                    Facteur dbFacteur = facteurDAO.getByCodeFct(code).orElse(null);
                    createOrUpdate(typeFacteur, libelle, code, isInfosSupplApportFert, isAssocNomen, dbFacteur, facteur);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param facteurDAO
     */
    public void setFacteurDAO(IFacteurDAO facteurDAO) {
        this.facteurDAO = facteurDAO;
    }

    /**
     * @param typeFacteurDAO
     */
    public void setTypeFacteurDAO(ITypeFacteurDAO typeFacteurDAO) {
        this.typeFacteurDAO = typeFacteurDAO;
    }

}
