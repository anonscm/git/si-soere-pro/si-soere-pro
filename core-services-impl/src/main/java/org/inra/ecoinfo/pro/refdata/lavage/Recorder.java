/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.lavage;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Lavage> {
    
    ILavageDAO lavageDAO;
    private Properties commentEn;
    
    private void createLavage(final Lavage especePlante) throws BusinessException {
        try {
            lavageDAO.saveOrUpdate(especePlante);
            lavageDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create lavage");
        }
    }
    
    private void updateBDLavage(String code, String nom, String commentaire, Lavage dbespece) throws BusinessException {
        try {
            dbespece.setCode(code);
            dbespece.setNom(nom);
            dbespece.setCommentaire(commentaire);
            lavageDAO.saveOrUpdate(dbespece);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update lavage");
        }
    }
    
    private void createOrUpdateLavage(final String code, String nom, String commentaire, final Lavage dbespece) throws BusinessException {
        if (dbespece == null) {
            final Lavage especePlante = new Lavage(nom, code, commentaire);
            especePlante.setCode(code);
            especePlante.setNom(nom);
            especePlante.setCommentaire(commentaire);
            createLavage(especePlante);
        } else {
            updateBDLavage(code, nom, commentaire, dbespece);
        }
    }
    
    private void persistLavage(final String code, final String nom, String commentaire) throws BusinessException {
        final Lavage dbMethode = lavageDAO.getByName(nom).orElse(null);
        createOrUpdateLavage(code, nom, commentaire, dbMethode);
    }
    
    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final Lavage dbEspece = lavageDAO.getByNKey(code).orElse(null);
                if (dbEspece != null) {
                    lavageDAO.remove(dbEspece);
                    values = csvp.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Lavage.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistLavage(code, nom, commentaire);
                }
                values = csvp.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<Lavage> getAllElements() throws BusinessException {
        return lavageDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Lavage lavage) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(lavage == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : lavage.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(lavage == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : lavage.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(lavage == null || lavage.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(lavage.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<Lavage> initModelGridMetadata() {
        commentEn = localizationManager.newProperties(Lavage.NAME_ENTITY_JPA, Lavage.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    /**
     *
     * @param lavageDAO
     */
    public void setLavageDAO(ILavageDAO lavageDAO) {
        this.lavageDAO = lavageDAO;
    }
    
}
