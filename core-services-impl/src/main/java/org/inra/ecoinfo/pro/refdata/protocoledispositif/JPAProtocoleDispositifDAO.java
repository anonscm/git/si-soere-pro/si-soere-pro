package org.inra.ecoinfo.pro.refdata.protocoledispositif;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;

/**
 * @author sophie
 *
 */
public class JPAProtocoleDispositifDAO extends AbstractJPADAO<ProtocoleDispositif> implements IProtocoleDispositifDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.protocoledispositif.IProtocoleDispositifDAO #getByProtocoleDispositif (org.inra.ecoinfo.pro.refdata.protocole.Protocole, org.inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */

    /**
     *
     * @param protocole
     * @param dispositif
     * @return
     */

    @Override
    public Optional<ProtocoleDispositif> getByNKey(Protocole protocole, Dispositif dispositif) {
        CriteriaQuery<ProtocoleDispositif> query = builder.createQuery(ProtocoleDispositif.class);
        Root<ProtocoleDispositif> protocoleDispositif = query.from(ProtocoleDispositif.class);
        Join<ProtocoleDispositif, Dispositif> dispo = protocoleDispositif.join(ProtocoleDispositif_.dispositif);
        Join<ProtocoleDispositif, Protocole> prot = protocoleDispositif.join(ProtocoleDispositif_.protocole);
        query
                .select(protocoleDispositif)
                .where(
                        builder.equal(dispo, dispositif),
                        builder.equal(prot, protocole)
                );
        return getOptional(query);
    }

}
