package org.inra.ecoinfo.pro.refdata.faitremarquable;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IFaitRemarquableDAO extends IDAO<Faitremarquable> {

    /**
     *
     * @return
     */
    List<Faitremarquable> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Faitremarquable> getByNKey(String nom);

}
