package org.inra.ecoinfo.pro.refdata.statut;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class StatutDAOImpl extends AbstractJPADAO<Statut> implements IStatutDAO {

    private static final String QUERY_STATUT_KEY = "from  Statut s where s.statut_nom = :statut_nom";

    /**
     *
     * @return
     */
    @Override
    public List<Statut> getAll() {
        return getAll(Statut.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Statut> getByNKey(String nom) {
        CriteriaQuery<Statut> query = builder.createQuery(Statut.class);
        Root<Statut> statut = query.from(Statut.class);
        query
                .select(statut)
                .where(
                        builder.equal(statut.get(Statut_.statut_nom), nom)
                );
        return getOptional(query);

    }

}
