/*
 *
 */
package org.inra.ecoinfo.pro.refdata.methodeprocess;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * The Class Recorder.
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<MethodeProcess> {
    
    /**
     *
     */
    protected IMethodeProcessDAO methodeprocessDAO;
    
    private Properties MPNomEN;
    private Properties commentEn;
    
    private void createMethodeProcess(final MethodeProcess methodeprocess) throws BusinessException {
        try {
            methodeprocessDAO.saveOrUpdate(methodeprocess);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create methodeprocess");
        }
        methodeprocessDAO.flush();
    }
    
    private void updateBDMethodeProcess(final String methodep_nom, String code, String commentaire, final MethodeProcess dbmethodeprocess) throws BusinessException {
        try {
            dbmethodeprocess.setMethodep_nom(methodep_nom);
            dbmethodeprocess.setMethodep_code(code);
            dbmethodeprocess.setCommentaire(commentaire);
            methodeprocessDAO.saveOrUpdate(dbmethodeprocess);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update methodeprocess");
        }
        
    }
    
    private void createOrUpdateMethodeProcess(final String code, String methodep_nom, String commentaire, final MethodeProcess dbmethodeprocess) throws BusinessException {
        if (dbmethodeprocess == null) {
            final MethodeProcess methodeprocess = new MethodeProcess(code, commentaire);
            methodeprocess.setMethodep_code(code);
            methodeprocess.setMethodep_nom(methodep_nom);
            methodeprocess.setCommentaire(commentaire);
            createMethodeProcess(methodeprocess);
        } else {
            updateBDMethodeProcess(methodep_nom, code, commentaire, dbmethodeprocess);
        }
    }
    
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String libelle = tokenizerValues.nextToken();
                MethodeProcess mp = methodeprocessDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't find methodeprocess"));
                methodeprocessDAO.remove(mp);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    @Override
    protected List<MethodeProcess> getAllElements() throws BusinessException {
        return methodeprocessDAO.getAll();
    }
    
    /**
     *
     * @return
     */
    public IMethodeProcessDAO getMethodeprocessDAO() {
        return methodeprocessDAO;
    }
    
    /**
     *
     * @return
     */
    public Properties getMPNomEN() {
        return MPNomEN;
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MethodeProcess mprocess) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mprocess == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : mprocess.getMethodep_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mprocess == null || mprocess.getMethodep_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : MPNomEN.getProperty(mprocess.getMethodep_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mprocess == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : mprocess.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(mprocess == null || mprocess.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : commentEn.getProperty(mprocess.getCommentaire()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        
        return lineModelGridMetadata;
    }
    
    @Override
    protected ModelGridMetadata<MethodeProcess> initModelGridMetadata() {
        MPNomEN = localizationManager.newProperties(MethodeProcess.JPA_NAME_ENTITY, MethodeProcess.JPA_COLUMN_NAME, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(MethodeProcess.JPA_NAME_ENTITY, MethodeProcess.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
    private void persistMethodeProcess(final String code, final String methodep_nom, String commentaire) throws BusinessException, BusinessException {
        final MethodeProcess dbmethodeprocess = methodeprocessDAO.getByNKey(methodep_nom).orElse(null);
        createOrUpdateMethodeProcess(code, methodep_nom, commentaire, dbmethodeprocess);
    }
    
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, MethodeProcess.JPA_NAME_ENTITY);
                final String methodep_nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(methodep_nom);
                final String commentaire = tokenizerValues.nextToken();
                persistMethodeProcess(code, methodep_nom, commentaire);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
        
    }
    
    /**
     *
     * @param methodeprocessDAO
     */
    public void setMethodeprocessDAO(IMethodeProcessDAO methodeprocessDAO) {
        this.methodeprocessDAO = methodeprocessDAO;
    }
    
    /**
     *
     * @param mPNomEN
     */
    public void setMPNomEN(Properties mPNomEN) {
        MPNomEN = mPNomEN;
    }
    
}
