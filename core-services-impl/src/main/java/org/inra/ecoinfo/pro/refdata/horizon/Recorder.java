/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizon;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Horizon> {

    IHorizonDAO horizonDAO;
    Properties horizonEn;

    private void createHorizon(final Horizon horizon) throws BusinessException {
        try {
            horizonDAO.saveOrUpdate(horizon);
            horizonDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create horizon");
        }
    }

    private void updateBDHorizon(final String code, String nom, final Horizon dbhorizon) throws BusinessException {
        try {
            dbhorizon.setHorizon_code(code);
            dbhorizon.setHorizon_nom(nom);
            horizonDAO.saveOrUpdate(dbhorizon);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update horizon");
        }
    }

    private void createOrUpdateHorizon(final String code, String nom, final Horizon dbhorizon) throws BusinessException {
        if (dbhorizon == null) {
            final Horizon horizon = new Horizon(nom);
            horizon.setHorizon_code(code);
            horizon.setHorizon_nom(nom);

            createHorizon(horizon);
        } else {
            updateBDHorizon(code, nom, dbhorizon);
        }
    }

    private void persistHorizon(final String code, final String nom) throws BusinessException, BusinessException {
        final Horizon dbHorizon = horizonDAO.getByNKey(nom).orElse(null);
        createOrUpdateHorizon(code, nom, dbHorizon);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();
                final Horizon dbHorizon = horizonDAO.getByNKey(nom).orElse(null);
                if (dbHorizon != null) {
                    horizonDAO.remove(dbHorizon);
                    values = csvp.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Horizon.JPA_NAME_ENTITY);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                if (!errorsReport.hasErrors()) {
                    persistHorizon(code, nom);
                }
                values = csvp.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Horizon> getAllElements() throws BusinessException {
        return horizonDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Horizon horizon) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(horizon == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : horizon.getHorizon_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(horizon == null || horizon.getHorizon_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : horizonEn.getProperty(horizon.getHorizon_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param horizonDAO
     */
    public void setHorizonDAO(IHorizonDAO horizonDAO) {
        this.horizonDAO = horizonDAO;
    }

    @Override
    protected ModelGridMetadata<Horizon> initModelGridMetadata() {
        horizonEn = localizationManager.newProperties(Horizon.JPA_NAME_ENTITY, Horizon.JPA_COLUMN_NAME_HORIZON, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

}
