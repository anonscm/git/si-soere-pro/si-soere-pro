/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.extraction.DatesFormParamVO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author vjkoyao
 */
public class IncubationParameterVO extends DefaultParameter implements IParameter{
    
    /**
     *
     */
    public static final String  INCUBATIONSOL                   = "Incubationsol" ;

    /**
     *
     */
    public static final String  INCUBATIONSOLPRO                = "Incubationsolpro" ;

    /**
     *
     */
    public static final String  INCUBATIONSOLMOY                = "Incubationsolmoy" ;
    
    /**
     *
     */
    public static final String  INCUBATIONSOLPROMOY             = "Incubationsolpromoy" ;

    static final String         INCUBATION_EXTRACTION_TYPE_CODE = "incubations";
    
    List<Dispositif>              selectedDispositif           = new LinkedList();

  
    String                      commentaires;

    
    DatesFormParamVO           datesFormParamVO;

   
    int                         affichage;

    /**
     *
     */
    public IncubationParameterVO() {
    }

    /**
     *
     * @param metadatasMap
     */
    public IncubationParameterVO(final Map<String, Object> metadatasMap) {
        this.setParameters(metadatasMap);
        this.setCommentaire((String) metadatasMap.get(IExtractionManager.KEYMAP_COMMENTS));
    }
    

    @Override
    public String getExtractionTypeCode() {
       return IncubationParameterVO.INCUBATION_EXTRACTION_TYPE_CODE;
    }

    /**
     *
     * @return
     */
    public List<Dispositif> getSelectedDispositif() {
        return selectedDispositif;
    }

    /**
     *
     * @param selectedDispositif
     */
    public void setSelectedDispositif(List<Dispositif> selectedDispositif) {
        this.selectedDispositif = selectedDispositif;
    }

    /**
     *
     * @return
     */
    public String getCommentaires() {
        return commentaires;
    }

    /**
     *
     * @param commentaires
     */
    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     *
     * @return
     */
    public DatesFormParamVO getDatesFormParamVO() {
        return datesFormParamVO;
    }

    /**
     *
     * @param datesFormParamVO
     */
    public void setDatesFormParamVO(DatesFormParamVO datesFormParamVO) {
        this.datesFormParamVO = datesFormParamVO;
    }

    /**
     *
     * @return
     */
    public int getAffichage() {
        return affichage;
    }

    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }
    
    
    
}
