/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionpourcentage;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IPrecisionPourcentageDAO extends IDAO<PrecisionPourcentage> {

    /**
     *
     * @return
     */
    List<PrecisionPourcentage> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<PrecisionPourcentage> getBYNKey(String libelle);

}
