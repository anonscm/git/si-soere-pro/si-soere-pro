package org.inra.ecoinfo.pro.refdata.structureobservatoire;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * @author sophie
 *
 */
public interface IStructureObservatoireDAO extends IDAO<StructureObservatoire> {

    /**
     *
     * @param structure
     * @param observatoire
     * @return
     */
    Optional<StructureObservatoire> getByNKey(Structure structure, Observatoire observatoire);

}
