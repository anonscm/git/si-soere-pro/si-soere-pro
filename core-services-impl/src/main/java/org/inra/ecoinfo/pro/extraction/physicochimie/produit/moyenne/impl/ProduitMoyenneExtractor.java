/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.moyenne.impl;

import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePROMoyDatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.IPhysicoChimieDAO;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieExtractor;

/**
 *
 * @author adiankha
 */
public class ProduitMoyenneExtractor extends AbstractPhysicoChimieExtractor<MesurePhysicoChimiePROMoy> {

    /**
     *
     */
    protected static final String MAP_INDEX_PRODUITMOYENNE = "produitmoyennes";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    public static final String CST_RESULTS = "extractionResults";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_PRODUITMOY_CODE = "extractionResultProduitMoyenne";

    private IPhysicoChimieDAO produitMoyenneDAO;

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return CST_RESULT_EXTRACTION_PRODUITMOY_CODE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IPhysicoChimiePROMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO_MOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_PRODUITMOYENNE;
    }
}
