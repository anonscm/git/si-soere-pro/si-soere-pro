/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.contexteclimatdispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.contextclimatiquedispositif.ContexteClimatiqueDispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 *
 * @author adiankha
 */
public interface IContexteClimatDispositifDAO extends IDAO<ContexteClimatiqueDispositif> {

    /**
     *
     * @return
     */
    List<ContexteClimatiqueDispositif> getAll();

    /**
     *
     * @param dispositif
     * @param precipitation
     * @return
     */
    Optional<ContexteClimatiqueDispositif> getByNKey(Dispositif dispositif, double precipitation);

}
