/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;

/**
 *
 * @author adiankha
 * @param <T>
 */
public interface IMesurePhysicoChimieDAO<T> extends IDAO<MesurePhysicoChimieSols> {
    
    /**
     *
     * @param nomlabo
     * @param dateprelevement
     * @return
     */
    Optional<MesurePhysicoChimieSols> getByNKeys( String nomlabo, LocalDate dateprelevement);
    
    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesurePhysicoChimieSols> getByKeys(String keymesure);
}
