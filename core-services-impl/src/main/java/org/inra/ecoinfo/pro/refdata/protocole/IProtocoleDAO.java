/**
 *
 */
package org.inra.ecoinfo.pro.refdata.protocole;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IProtocoleDAO extends IDAO<Protocole> {

    /**
     *
     * @param nom
     * @return
     */
    Optional<Protocole> getByNKey(String nom);

}
