/**
 *
 */
package org.inra.ecoinfo.pro.refdata.bloc;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 * @author sophie
 *
 */
public interface IBlocDAO extends IDAO<Bloc> {

    /**
     *
     * @return
     */
    List<Bloc> getAll();

    /**
     *
     * @param nom
     * @param dispositif
     * @return
     */
    Optional<Bloc> getByNKey(String nom, Dispositif dispositif);

    /**
     *
     * @param dispositif
     * @return
     */
    List<Bloc> getLstBlocByDispositif(Dispositif dispositif);

    /**
     *
     * @param nombloc
     * @param codedisp
     * @return
     */
    Optional<Bloc> getByFindBloc(String nombloc, String codedisp);

}
