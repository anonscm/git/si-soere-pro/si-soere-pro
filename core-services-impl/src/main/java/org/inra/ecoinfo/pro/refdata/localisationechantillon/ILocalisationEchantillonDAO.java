/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationechantillon;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface ILocalisationEchantillonDAO extends IDAO<LocalisationEchantillon> {

    /**
     *
     * @return
     */
    List<LocalisationEchantillon> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<LocalisationEchantillon> getByNKey(String code);

}
