package org.inra.ecoinfo.pro.refdata.typesolarvalis;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface ITypeSolArvalisDAO extends IDAO<Typesolarvalis> {

    /**
     *
     * @return
     */
    List<Typesolarvalis> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Typesolarvalis> getByNKey(String nom);

}
