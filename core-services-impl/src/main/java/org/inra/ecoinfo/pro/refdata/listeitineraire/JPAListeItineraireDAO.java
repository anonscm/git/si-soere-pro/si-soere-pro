/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeitineraire;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.refdata.valeurqualitative.JPAValeurQualitativeDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAListeItineraireDAO extends JPAValeurQualitativeDAO implements IListeItineraireDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ListeItineraire> getAll() {
        return entityManager.createQuery("from ListeItineraire").getResultList();
    }

    /**
     *
     * @param liste_nom
     * @param liste_valeur
     * @return
     */
    @Override
    public Optional<ListeItineraire> getByNKey(String liste_nom, String liste_valeur) {
        CriteriaQuery<ListeItineraire> query = builder.createQuery(ListeItineraire.class);
        Root<ListeItineraire> listeItineraire = query.from(ListeItineraire.class);
        query
                .select(listeItineraire)
                .where(
                        builder.equal(listeItineraire.get(ListeItineraire_.liste_nom), liste_nom),
                        builder.equal(listeItineraire.get(ListeItineraire_.liste_valeur), liste_valeur)
                );
        return getOptional(query);
    }

    /**
     *
     * @param knom
     * @param kvaleur
     * @return
     */
    @Override
    public Optional<ListeItineraire> getByKKey(String knom, String kvaleur) {
        CriteriaQuery<ListeItineraire> query = builder.createQuery(ListeItineraire.class);
        Root<ListeItineraire> listeItineraire = query.from(ListeItineraire.class);
        query
                .select(listeItineraire)
                .where(
                        builder.equal(listeItineraire.get(ListeItineraire_.knom), knom),
                        builder.equal(listeItineraire.get(ListeItineraire_.kvaleur), kvaleur)
                );
        return getOptional(query);
    }

}
