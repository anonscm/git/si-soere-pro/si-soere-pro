/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols_;
import org.inra.ecoinfo.pro.synthesis.physicochimiesol.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.physicochimiesol.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class SolDonneesElementairesSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurPhysicoChimieSols, MesurePhysicoChimieSols> {

    @Override
    Class<ValeurPhysicoChimieSols> getValueClass() {
        return ValeurPhysicoChimieSols.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurPhysicoChimieSols, MesurePhysicoChimieSols> getMesureAttribute() {
        return ValeurPhysicoChimieSols_.mesurePhysicoChimieSols;
    }
}
