/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy.IValeurIncubationSolProMoyDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAValeurIncubationSolProMoyDAO extends AbstractJPADAO<ValeurIncubationSolProMoy> implements IValeurIncubationSolProMoyDAO {

    /**
     *
     * @param realNode
     * @param valeur_ecart_type
     * @param mesureIncubationSolProMoy
     * @return
     */
    @Override
    public Optional<ValeurIncubationSolProMoy> getByNkeys(RealNode realNode, float valeur_ecart_type, MesureIncubationSolProMoy mesureIncubationSolProMoy) {
        CriteriaQuery<ValeurIncubationSolProMoy> query = builder.createQuery(ValeurIncubationSolProMoy.class);
        Root<ValeurIncubationSolProMoy> v = query.from(ValeurIncubationSolProMoy.class);
        Join<ValeurIncubationSolProMoy, RealNode> rnVariable = v.join(ValeurIncubationSolProMoy_.realNode);
        Join<ValeurIncubationSolProMoy, MesureIncubationSolProMoy> m = v.join(ValeurIncubationSolProMoy_.mesureIncubationSolProMoy);
        query
                .select(v)
                .where(
                        builder.equal(rnVariable, realNode),
                        builder.equal(m, mesureIncubationSolProMoy),
                        builder.equal(v.get(ValeurIncubationSolProMoy_.ecart_type), valeur_ecart_type)
                );
        return getOptional(query);

    }

}
