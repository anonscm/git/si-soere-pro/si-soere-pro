/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.famille;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author vjkoyao
 */
public interface IFamilleDAO extends IDAO<Famille> {

    //public CategorieITK getByNKey(String categorie_nom) {

    /**
     *
     * @return
     */
    List<Famille> getAll();

    /**
     *
     * @param famille_nom
     * @return
     */
    Optional<Famille> getByNKey(String famille_nom);

}
