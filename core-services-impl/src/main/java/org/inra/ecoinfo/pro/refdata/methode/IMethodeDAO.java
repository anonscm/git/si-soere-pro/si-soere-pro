/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.methode;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IMethodeDAO extends IDAO<Methode> {

    /**
     *
     * @return
     */
    List<Methode> getAll();

    /**
     *
     * @param methode_nom
     * @return
     */
    Optional<Methode> getByName(String methode_nom);

    /**
     *
     * @param methode_mycode
     * @return
     */
    Optional<Methode> getByNKey(String methode_mycode);

}
