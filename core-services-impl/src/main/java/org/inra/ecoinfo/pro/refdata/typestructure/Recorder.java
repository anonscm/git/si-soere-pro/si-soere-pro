/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typestructure;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeStructure> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ITypeStructureDAO typeStructureDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, TypeStructure.TABLE_NAME);
                
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                String code = Utils.createCodeFromString(libelle);

                //libellé vérifié car à cause de libellé en anglais, la colonne Libellé du type de structure est indiquée comme possiblement null dans le dataset-descriptor
                if (libelle == null || libelle.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                TypeStructure typeStructure = new TypeStructure(libelle, code);
                TypeStructure dbTypeStructure = typeStructureDAO.getByCode(code).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbTypeStructure == null) {
                        typeStructureDAO.saveOrUpdate(typeStructure);
                    } else {
                        dbTypeStructure.setLibelle(libelle);
                        dbTypeStructure.setCode(code);
                        typeStructureDAO.saveOrUpdate(dbTypeStructure);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (NoResultException e2) {
            throw new BusinessException(e2.getMessage(), e2);
        } catch (NonUniqueResultException e3) {
            throw new BusinessException(e3.getMessage(), e3);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                typeStructureDAO.remove(typeStructureDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get type structure")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<TypeStructure> getAllElements() throws BusinessException {
        return typeStructureDAO.getAll(TypeStructure.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeStructure typeStructure) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesNameLibelle = localizationManager.newProperties(TypeStructure.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_TPSTR, Locale.ENGLISH);
        
        String localizedChampNameLibelle = "";
        
        if (typeStructure != null) {
            localizedChampNameLibelle = propertiesNameLibelle.containsKey(typeStructure.getLibelle()) ? propertiesNameLibelle.getProperty(typeStructure.getLibelle()) : typeStructure.getLibelle();
        }
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeStructure == null ? Constantes.STRING_EMPTY : typeStructure.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeStructure == null ? Constantes.STRING_EMPTY : localizedChampNameLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /**
     * @param typeStructureDAO the typeStructureDAO to set
     */
    public void setTypeStructureDAO(ITypeStructureDAO typeStructureDAO) {
        this.typeStructureDAO = typeStructureDAO;
    }
    
}
