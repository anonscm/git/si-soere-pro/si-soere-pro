/**
 *
 */
package org.inra.ecoinfo.pro.refdata.thematiqueetudiee;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface IThematiqueEtudieeDAO extends IDAO<ThematiqueEtudiee> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<ThematiqueEtudiee> getByNKey(String libelle);

}
