/*
 *
 */
package org.inra.ecoinfo.pro.refdata.horizonsystematique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author Vivianne
 *
 */
// TODO: Auto-generated Javadoc
/**
 * The Interface IReferenceBibliographiqueDAO.
 */
public interface IHorizonSystematiqueDAO extends IDAO<HorizonSystematique> {

    /**
     *
     * @return
     */
    List<HorizonSystematique> getAll();

    /**
     *
     * @param horizon_systematique_nom
     * @return
     */
    Optional<HorizonSystematique> getByNKey(String horizon_systematique_nom);
}
