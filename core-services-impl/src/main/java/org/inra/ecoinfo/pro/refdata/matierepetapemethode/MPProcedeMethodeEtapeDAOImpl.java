package org.inra.ecoinfo.pro.refdata.matierepetapemethode;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.etape.Etapes_;
import org.inra.ecoinfo.pro.refdata.matierepremiere.MatieresPremieres_;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.melange.Melange_;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.procedemethodeprodmp.ProcedeMethodeProdMp;
import org.inra.ecoinfo.pro.refdata.procedemethodeprodmp.ProcedeMethodeProdMp_;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.process.Process_;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produit.Produits_;

/**
 *
 * @author ptcherniati
 */
public class MPProcedeMethodeEtapeDAOImpl extends AbstractJPADAO<ProcedeMethodeEtapeMP> implements IMPProcedeMethodeEtapeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ProcedeMethodeEtapeMP> getAll() {
        return getAll(ProcedeMethodeEtapeMP.class);
    }

    /**
     *
     * @param mpp
     * @param etapes
     * @param methodeetapes
     * @param ordre
     * @return
     */
    @Override
    public Optional<ProcedeMethodeEtapeMP> getByNKey(ProcedeMethodeProdMp mpp, Etapes etapes, MethodeEtapes methodeetapes, int ordre) {
        CriteriaQuery<ProcedeMethodeEtapeMP> query = builder.createQuery(ProcedeMethodeEtapeMP.class);
        Root<ProcedeMethodeEtapeMP> procedeMethodeEtapeMP = query.from(ProcedeMethodeEtapeMP.class);
        Join<ProcedeMethodeEtapeMP, ProcedeMethodeProdMp> procedemethodepropm = procedeMethodeEtapeMP.join(ProcedeMethodeEtapeMP_.procedemethodepropm);
        Join<ProcedeMethodeEtapeMP, MethodeEtapes> mEtp = procedeMethodeEtapeMP.join(ProcedeMethodeEtapeMP_.methodeetapes);
        Join<ProcedeMethodeEtapeMP, Etapes> etp = procedeMethodeEtapeMP.join(ProcedeMethodeEtapeMP_.etapes);
        query
                .select(procedeMethodeEtapeMP)
                .where(
                        builder.equal(procedemethodepropm, mpp),
                        builder.equal(etp, etapes),
                        builder.equal(mEtp, methodeetapes),
                        builder.equal(procedeMethodeEtapeMP.get(ProcedeMethodeEtapeMP_.ordre), ordre)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeProduit
     * @param codeMatierePremiere
     * @param pourcentage
     * @param intituleProcede
     * @param ordreProcede
     * @param intituleEtape
     * @param codeMethodeEtape
     * @param ordreEtape
     * @return
     */
    @Override
    public Optional<ProcedeMethodeEtapeMP> getByNKey(
            String codeProduit,
            String codeMatierePremiere,
            Double pourcentage,
            String intituleProcede,
            int ordreProcede,
            String intituleEtape,
            String codeMethodeEtape,
            int ordreEtape
    ) {
        CriteriaQuery<ProcedeMethodeEtapeMP> query = builder.createQuery(ProcedeMethodeEtapeMP.class);
        Root<ProcedeMethodeEtapeMP> procedeMethodeEtapeMP = query.from(ProcedeMethodeEtapeMP.class);
        Join<ProcedeMethodeEtapeMP, MethodeEtapes> mEtp = procedeMethodeEtapeMP.join(ProcedeMethodeEtapeMP_.methodeetapes);
        Join<ProcedeMethodeEtapeMP, Etapes> etp = procedeMethodeEtapeMP.join(ProcedeMethodeEtapeMP_.etapes);
        Join<ProcedeMethodeEtapeMP, ProcedeMethodeProdMp> procedemethodepropm = procedeMethodeEtapeMP.join(ProcedeMethodeEtapeMP_.procedemethodepropm);
        Join<ProcedeMethodeProdMp, Process> process = procedemethodepropm.join(ProcedeMethodeProdMp_.process);
        Join<ProcedeMethodeProdMp, Melange> melange = procedemethodepropm.join(ProcedeMethodeProdMp_.melange);
        Join<Melange, Produits> produit = melange.join(Melange_.produits);
        Join<Melange, Composant> matierePremiere = melange.join(Melange_.composant);

        query
                .select(procedeMethodeEtapeMP)
                .where(
                        builder.equal(produit.get(Produits_.codecomposant), codeProduit),
                        builder.equal(produit.get(MatieresPremieres_.codecomposant), codeMatierePremiere),
                        builder.equal(melange.get(Melange_.pourcentage), pourcentage),
                        builder.equal(process.get(Process_.process_intitule), intituleProcede),
                        builder.equal(procedemethodepropm.get(ProcedeMethodeProdMp_.odreprocede), ordreProcede),
                        builder.equal(etp.get(Etapes_.etape_intitule), intituleEtape),
                        builder.equal(mEtp.get(MethodeEtapes.JPA_COLUMN_NAME), codeMethodeEtape),
                        builder.equal(procedeMethodeEtapeMP.get(ProcedeMethodeEtapeMP_.ordre), ordreEtape)
                );
        return getOptional(query);
    }
}
