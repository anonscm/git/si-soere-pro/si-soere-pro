package org.inra.ecoinfo.pro.refdata.protocoledispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.protocole.IProtocoleDAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProtocoleDispositif> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IProtocoleDAO protocoleDAO;
    IDispositifDAO dispositifDAO;
    IProtocoleDispositifDAO protocoleDispositifDAO;
    private String[] dispositifPossibles;
    private String[] protocolePossibles;

    /**
     * @return @throws PersistenceException
     */
    private String[] getDispositifPossibles() {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll();
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) {
            dispositifPossibles[index++] = dispositif.getNomDispositif_nomLieu();
        }
        return dispositifPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getProtocolePossibles() {
        List<Protocole> lstProtocoles = protocoleDAO.getAll(Protocole.class);
        String[] protocolePossibles = new String[lstProtocoles.size()];
        int index = 0;
        for (Protocole protocole : lstProtocoles) {
            protocolePossibles[index++] = protocole.getNom();
        }
        return protocolePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomProtocole = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                Protocole protocole = protocoleDAO.getByNKey(nomProtocole).orElse(null);

                ProtocoleDispositif protocoleDispositif = protocoleDispositifDAO.getByNKey(protocole, dispositif)
                        .orElseThrow(() -> new BusinessException("can't get protocoledispositif"));
                protocoleDispositifDAO.remove(protocoleDispositif);
                protocoleDispositifDAO.flush();

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ProtocoleDispositif> getAllElements() throws BusinessException {
        return protocoleDispositifDAO.getAll(ProtocoleDispositif.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProtocoleDispositif protocoleDispositif) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String codeDipositifLieu = protocoleDispositif == null ? "" : protocoleDispositif.getDispositif().getNomDispositif_nomLieu();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocoleDispositif == null ? Constantes.STRING_EMPTY : codeDipositifLieu, dispositifPossibles, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(protocoleDispositif == null ? Constantes.STRING_EMPTY : protocoleDispositif.getProtocole().getNom(), protocolePossibles, null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexdispo = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                int indexpro = tokenizerValues.currentTokenIndex();
                String nomProtocole = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = verifieLieu(nomLieu, line + 1, indexdispo + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line + 1, indexdispo + 1, errorsReport);
                Protocole protocole = verifieProtocole(nomProtocole, line + 1, indexpro + 1, errorsReport);

                ProtocoleDispositif protocoleDispositif = new ProtocoleDispositif(protocole, dispositif);
                ProtocoleDispositif dbProtocoleDispositif = protocoleDispositifDAO.getByNKey(protocole, dispositif).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbProtocoleDispositif == null) {
                        protocoleDispositifDAO.saveOrUpdate(protocoleDispositif);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }

        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }

        return dispositif;
    }

    /**
     * @param nomProtocole
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Protocole verifieProtocole(String nomProtocole, long line, int index, ErrorsReport errorsReport) {
        Protocole protocole = protocoleDAO.getByNKey(nomProtocole).orElse(null);
        if (protocole == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "PROTOCOLE_NONDEFINI"), line, index, nomProtocole));
        }

        return protocole;
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param protocoleDAO the protocoleDAO to set
     */
    public void setProtocoleDAO(IProtocoleDAO protocoleDAO) {
        this.protocoleDAO = protocoleDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param protocoleDispositifDAO the protocoleDispositifDAO to set
     */
    public void setProtocoleDispositifDAO(
            IProtocoleDispositifDAO protocoleDispositifDAO) {
        this.protocoleDispositifDAO = protocoleDispositifDAO;
    }

    @Override
    protected ModelGridMetadata<ProtocoleDispositif> initModelGridMetadata() {
        dispositifPossibles = getDispositifPossibles();
        protocolePossibles = getProtocolePossibles();
        return super.initModelGridMetadata();
    }

}
