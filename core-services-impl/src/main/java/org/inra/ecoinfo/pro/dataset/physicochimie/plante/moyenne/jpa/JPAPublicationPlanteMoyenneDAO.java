/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne_;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationPlanteMoyenneDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurPlanteMoyenne> deleteValeurs = builder.createCriteriaDelete(ValeurPlanteMoyenne.class);
        Root<ValeurPlanteMoyenne> valeur = deleteValeurs.from(ValeurPlanteMoyenne.class);
        Subquery<MesurePlanteMoyenne> subquery = deleteValeurs.subquery(MesurePlanteMoyenne.class);
        Root<MesurePlanteMoyenne> m = subquery.from(MesurePlanteMoyenne.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesurePlanteMoyenne_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurPlanteMoyenne_.mesurePlanteMoyenne).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesurePlanteMoyenne> deleteSequence = builder.createCriteriaDelete(MesurePlanteMoyenne.class);
        Root<MesurePlanteMoyenne> sequence = deleteSequence.from(MesurePlanteMoyenne.class);
        deleteSequence.where(builder.equal(sequence.get(MesurePlanteMoyenne_.versionfile), version));
        delete(deleteSequence);
    }

}
