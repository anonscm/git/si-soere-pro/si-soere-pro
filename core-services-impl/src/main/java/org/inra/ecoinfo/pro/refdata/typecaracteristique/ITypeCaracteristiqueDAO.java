/*
 *
 */
package org.inra.ecoinfo.pro.refdata.typecaracteristique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

// TODO: Auto-generated Javadoc
/**
 * The Interface ITypeCaracteristiqueDAO.
 */
public interface ITypeCaracteristiqueDAO extends IDAO<TypeCaracteristiques> {

    /**
     * Findby name.
     *
     * @param nom the nom
     * @return the type caracteristiques
     */
    Optional<TypeCaracteristiques> getByNKey(String nom);

    /**
     * Alltype.
     *
     * @return the list< type caracteristiques>
     */
    List<TypeCaracteristiques> getAll();

}
