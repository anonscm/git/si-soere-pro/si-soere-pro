package org.inra.ecoinfo.pro.refdata.typetraitement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeTraitementDAO extends IDAO<TypeTraitement> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeTraitement> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<TypeTraitement> getByCode(String code);

    List<TypeTraitement> getAll();

}
