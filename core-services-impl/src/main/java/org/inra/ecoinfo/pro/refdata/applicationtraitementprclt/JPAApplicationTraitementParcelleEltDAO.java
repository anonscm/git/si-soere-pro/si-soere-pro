package org.inra.ecoinfo.pro.refdata.applicationtraitementprclt;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.modalite.Modalite;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 * @author sophie
 *
 */
public class JPAApplicationTraitementParcelleEltDAO extends AbstractJPADAO<ApplicationTraitementParcelleElt> implements IApplicationTraitementParcelleEltDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ApplicationTraitementParcelleElt> getAll() {
        return getAllBy(ApplicationTraitementParcelleElt.class, ApplicationTraitementParcelleElt::getParcelleElementaire);
    }

    /**
     *
     * @param descriptionTraitement
     * @param parcelleElementaire
     * @param modalite
     * @return
     */
    @Override
    public Optional<ApplicationTraitementParcelleElt> getByNKey(DescriptionTraitement descriptionTraitement, ParcelleElementaire parcelleElementaire, Modalite modalite) {
        CriteriaQuery<ApplicationTraitementParcelleElt> query = builder.createQuery(ApplicationTraitementParcelleElt.class);
        Root<ApplicationTraitementParcelleElt> atpe = query.from(ApplicationTraitementParcelleElt.class);
        query
                .select(atpe)
                .where(
                        builder.equal(atpe.get(ApplicationTraitementParcelleElt_.descriptionTraitement), descriptionTraitement),
                        builder.equal(atpe.get(ApplicationTraitementParcelleElt_.modalite), modalite),
                        builder.equal(atpe.get(ApplicationTraitementParcelleElt_.parcelleElementaire), parcelleElementaire)
                );
        return getOptional(query);
    }
}
