/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro_;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO_;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.DataType_;

/**
 *
 * @author adiankha
 */
public class JPADataTypeVariableQualifiantDAO extends AbstractJPADAO<DatatypeVariableUnitePRO> implements IDataTypeVariableQualifiantDAO {

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @param uniteCode
     * @param methode
     * @param humiditeExpression
     * @return
     */
    @Override
    public Optional<DatatypeVariableUnitePRO> getByNKey(String datatypeCode, String variableCode, String uniteCode, Methode methode, HumiditeExpression humiditeExpression) {
        CriteriaQuery<DatatypeVariableUnitePRO> query = builder.createQuery(DatatypeVariableUnitePRO.class);
        Root<DatatypeVariableUnitePRO> datatypeVariableUnite = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, Unitepro> unite = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.unitepro);
        Join<DatatypeVariableUnitePRO, DataType> datatype = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.datatype);
        Join<DatatypeVariableUnitePRO, VariablesPRO> variable = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.variablespro);
        Join<DatatypeVariableUnitePRO, Methode> met = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.methode);
        Join<DatatypeVariableUnitePRO, HumiditeExpression> humExp = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.humiditeexpression);
        query
                .select(datatypeVariableUnite)
                .where(
                        builder.equal(unite.get(Unitepro_.code), uniteCode),
                        builder.equal(datatype.get(DataType_.code), datatypeCode),
                        builder.equal(variable.get(VariablesPRO_.code), variableCode),
                        builder.equal(met, methode),
                        builder.equal(humExp, humiditeExpression)
                );
        return getOptional(query);
    }

    /**
     *
     * @param dataType
     * @param variablesPRO
     * @param unitepro
     * @param methode
     * @param humiditeExpression
     * @return
     */
    @Override
    public Optional<DatatypeVariableUnitePRO> getByNKey(DataType dataType, VariablesPRO variablesPRO, Unitepro unitepro, Methode methode, HumiditeExpression humiditeExpression) {
        CriteriaQuery<DatatypeVariableUnitePRO> query = builder.createQuery(DatatypeVariableUnitePRO.class);
        Root<DatatypeVariableUnitePRO> datatypeVariableUnite = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, Unitepro> uni = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.unitepro);
        Join<DatatypeVariableUnitePRO, DataType> dty = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.datatype);
        Join<DatatypeVariableUnitePRO, VariablesPRO> var = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.variablespro);
        Join<DatatypeVariableUnitePRO, Methode> met = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.methode);
        Join<DatatypeVariableUnitePRO, HumiditeExpression> humExp = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.humiditeexpression);
        query
                .select(datatypeVariableUnite)
                .where(
                        builder.equal(uni, unitepro),
                        builder.equal(dty, dataType),
                        builder.equal(var, variablesPRO),
                        builder.equal(met, methode),
                        builder.equal(humExp, humiditeExpression)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<DatatypeVariableUnitePRO> getAll() {
        return getAllBy(DatatypeVariableUnitePRO.class, DatatypeVariableUnitePRO::getCode);
    }

    /**
     *
     * @param datatypeCode
     * @return
     */
    @Override
    public Map<String, DatatypeVariableUnitePRO> getAllDatatypeVariableUnitePROMethodeandCategorie(String datatypeCode) {
        CriteriaQuery<DatatypeVariableUnitePRO> query = builder.createQuery(DatatypeVariableUnitePRO.class);
        Root<DatatypeVariableUnitePRO> datatypeVariableUnite = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, DataType> dty = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.datatype);
        query
                .select(datatypeVariableUnite)
                .where(
                        builder.equal(dty.get(DataType_.code), datatypeCode)
                );
        return getResultListToStream(query).collect(
                Collectors.toMap(k -> k.getCode(), v -> v));
    }

    /**
     *
     * @param variableCode
     * @return
     */
    @Override
    public Optional<DatatypeVariableUnitePRO> getByNameVariable(String variableCode) {
        CriteriaQuery<DatatypeVariableUnitePRO> query = builder.createQuery(DatatypeVariableUnitePRO.class);
        Root<DatatypeVariableUnitePRO> datatypeVariableUnite = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, VariablesPRO> var = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.variablespro);
        query
                .select(datatypeVariableUnite)
                .where(
                        builder.equal(var.get(VariablesPRO_.code), variableCode)
                );
        return getOptional(query);
    }

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @return
     */
    @Override
    public Optional<DatatypeVariableUnitePRO> getSemisKey(String datatypeCode, String variableCode) {
        CriteriaQuery<DatatypeVariableUnitePRO> query = builder.createQuery(DatatypeVariableUnitePRO.class);
        Root<DatatypeVariableUnitePRO> datatypeVariableUnite = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, DataType> dty = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.datatype);
        Join<DatatypeVariableUnitePRO, VariablesPRO> var = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.variablespro);
        query
                .select(datatypeVariableUnite)
                .where(
                        builder.equal(dty.get(DataType_.code), datatypeCode),
                        builder.equal(var.get(VariablesPRO_.code), variableCode)
                );
        return getOptional(query);
    }

    /**
     *
     * @param currentDatatype
     * @return
     */
    public Optional<String> getNameVariableForDatatypeName(String currentDatatype) {
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<DatatypeVariableUnitePRO> datatypeVariableUnite = query.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, DataType> dty = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.datatype);
        Join<DatatypeVariableUnitePRO, VariablesPRO> var = datatypeVariableUnite.join(DatatypeVariableUnitePRO_.variablespro);
        query
                .select(var.get(VariablesPRO_.code))
                .where(
                        builder.equal(dty.get(DataType_.code), currentDatatype)
                );
        return getOptional(query);

    }
}
