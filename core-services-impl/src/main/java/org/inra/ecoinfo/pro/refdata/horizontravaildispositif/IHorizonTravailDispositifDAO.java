/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizontravaildispositif;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.horizontravailledispositif.HorizonTravailDispositif;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.Teneurcalcaireinitial;

/**
 *
 * @author adiankha
 */
public interface IHorizonTravailDispositifDAO extends IDAO<HorizonTravailDispositif> {

    /**
     *
     * @return
     */
    List<HorizonTravailDispositif> getAll();

    /**
     *
     * @param dispositif
     * @param teneurcalcaireinitial
     * @param teneurcarbone
     * @return
     */
    Optional<HorizonTravailDispositif> getByNKey(Dispositif dispositif, Teneurcalcaireinitial teneurcalcaireinitial, double teneurcarbone);

}
