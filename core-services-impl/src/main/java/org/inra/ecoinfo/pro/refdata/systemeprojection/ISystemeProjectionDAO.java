package org.inra.ecoinfo.pro.refdata.systemeprojection;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ISystemeProjectionDAO extends IDAO<SystemeProjection> {

    /**
     *
     * @param nom
     * @return
     */
    Optional<SystemeProjection> getByNKey(String nom);

    /**
     *
     * @param code
     * @return
     */
    Optional<SystemeProjection> getByCode(String code);

    public List<SystemeProjection> getAll();
}
