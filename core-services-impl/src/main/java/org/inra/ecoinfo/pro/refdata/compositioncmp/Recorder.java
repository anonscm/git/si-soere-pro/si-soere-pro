package org.inra.ecoinfo.pro.refdata.compositioncmp;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.component.Composant;
import org.inra.ecoinfo.pro.refdata.component.IComposantDAO;
import org.inra.ecoinfo.pro.refdata.melange.IMelangeDAO;
import org.inra.ecoinfo.pro.refdata.melange.Melange;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.IValeurCaracteristiqueMPDAO;
import org.inra.ecoinfo.pro.refdata.valeurcaracteristiquemp.ValeurCaracteristiqueMP;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<CompositionsCMP> {

    private static final String PROPERTY_MSG_CMPV_BAD_COMP = "PROPERTY_MSG_CMPV_BAD_COMP";

    private static final String PROPERTY_MSG_CMPV_BAD_TNV = "PROPERTY_MSG_CMPV_BAD_TNV";

    private static final String PRODUIT_CODE_N_EXISTE_PAS = "PRODUIT_CODE_N_EXISTE_PAS";

    private static final String CODECOMPOSANT_N_EXISTE_PAS = "CODECOMPOSANT_N_EXISTE_PAS";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected ICompositionCMPDAO ccmpDAO;

    /**
     *
     */
    protected IMelangeDAO melangeDAO;
    IProduitDAO produitDAO;
    IComposantDAO composantDAO;
    IValeurCaracteristiqueMPDAO vcmpDAO;

    /**
     * The blocs and repetition possibles.
     */
    private String[] listeTypeCPMPossibles;
    private ConcurrentMap<String, String[]> listeCaracteristiquesCMPPossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeValeurPossibles = new ConcurrentHashMap<>();
    Properties commentaireEn;

    private void createCCMP(final CompositionsCMP compositioncmp) throws BusinessException {
        try {
            ccmpDAO.saveOrUpdate(compositioncmp);
            ccmpDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create dbcompositioncmp");
        }
    }

    private void createOrUpdateCCMP(final Melange melange, ValeurCaracteristiqueMP vcmp, String commentaire, final CompositionsCMP dbcompositioncmp) throws BusinessException {
        if (dbcompositioncmp == null) {
            CompositionsCMP cmp = new CompositionsCMP(melange, vcmp, commentaire);
            cmp.setMelange(melange);
            cmp.setValeurcaracteristiquemp(vcmp);
            cmp.setCommentaire(commentaire);
            createCCMP(cmp);
        } else {
            updateBDCCMP(melange, vcmp, commentaire, dbcompositioncmp);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            long line =0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeProduit = tokenizerValues.nextToken();
                final String codeComposant = tokenizerValues.nextToken();
                final Double pourcentage = verifieDouble(tokenizerValues, line, true, errorsReport);
                final String type = tokenizerValues.nextToken();
                final String nomc = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                Produits produits = produitDAO.getByNKey(codeProduit).orElse(null);
                Composant composant = composantDAO.getByCodeComposant(codeComposant).orElse(null);
                Melange dbmelange = melangeDAO.getByNKey(produits, composant, pourcentage).orElse(null);
                if (dbmelange == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_COMP), codeProduit, codeComposant));
                }
                ValeurCaracteristiqueMP dbvcmp = vcmpDAO.getByNKey(type, nomc, valeur).orElse(null);
                if (dbvcmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_TNV), type, nomc, valeur));
                }

                final CompositionsCMP dbccmp = ccmpDAO.getByNKey(dbmelange, dbvcmp)
                        .orElseThrow(PersistenceException::new);

                ccmpDAO.remove(dbccmp);
                values = parser.getLine();

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<CompositionsCMP> getAllElements() throws BusinessException {
        return ccmpDAO.getAll();
    }

    /**
     *
     * @return
     */
    public ICompositionCMPDAO getCcmpDAO() {
        return ccmpDAO;
    }

    /**
     *
     * @return
     */
    public IMelangeDAO getMelangeDAO() {
        return melangeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(CompositionsCMP compositioncmp) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        org.inra.ecoinfo.pro.refdata.melange.Recorder.initNewLine(lineModelGridMetadata, compositioncmp == null ? null : compositioncmp.getMelange());

        String type = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur() != null ? compositioncmp.getValeurcaracteristiquemp().getCaracteristiquematierepremieres().getTypecaracteristique().getTcar_nom() : "";
        ColumnModelGridMetadata columnType = new ColumnModelGridMetadata(type, listeTypeCPMPossibles, null, true, false, true);

        String caracteristique = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur() != null ? compositioncmp.getValeurcaracteristiquemp().getCaracteristiquematierepremieres().getCmp_nom() : "";
        ColumnModelGridMetadata columnCaracteristique = new ColumnModelGridMetadata(caracteristique, listeCaracteristiquesCMPPossibles, null, true, false, true);

        String valeur = compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur() != null ? compositioncmp.getValeurcaracteristiquemp().getCaracteristiquevaleur().getCv_nom() : "";
        ColumnModelGridMetadata columnValeur = new ColumnModelGridMetadata(valeur, listeValeurPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> typeRefs = new LinkedList<ColumnModelGridMetadata>();
        typeRefs.add(columnCaracteristique);
        typeRefs.add(columnValeur);
        List<ColumnModelGridMetadata> nomrefs = new LinkedList<ColumnModelGridMetadata>();
        nomrefs.add(columnValeur);
        columnType.setRefs(typeRefs);
        columnCaracteristique.setRefs(nomrefs);
        columnCaracteristique.setRefBy(columnType);
        columnCaracteristique.setValue(caracteristique);
        columnValeur.setRefBy(columnCaracteristique);
        columnValeur.setValue(valeur);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnType);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnCaracteristique);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnValeur);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(compositioncmp == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : compositioncmp.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(compositioncmp == null || compositioncmp.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(compositioncmp.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IValeurCaracteristiqueMPDAO getVcmpDAO() {
        return vcmpDAO;
    }

    @Override
    protected ModelGridMetadata<CompositionsCMP> initModelGridMetadata() {
        try {
            org.inra.ecoinfo.pro.refdata.melange.Recorder.initMelangesPossibles(melangeDAO);
            initTypeCMPPossibles();
        } catch (BusinessException e) {
        }
        commentaireEn = localizationManager.newProperties(CompositionsCMP.NAME_ENTITY_JPA, CompositionsCMP.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    private void initTypeCMPPossibles() throws BusinessException {
        Map<String, Map<String, List<String>>> valeursMap = new HashMap<String, Map<String, List<String>>>();
        List<ValeurCaracteristiqueMP> groupevcmp = vcmpDAO.getAll();
        groupevcmp.forEach((vcmp) -> {
            String vcmptype = vcmp.getCaracteristiquematierepremieres().getTypecaracteristique().getTcar_nom();
            String vcmpnom = vcmp.getCaracteristiquematierepremieres().getCmp_nom();
            String vcmpvaleur = vcmp.getCaracteristiquevaleur().getCv_nom();
            if (!valeursMap.containsKey(vcmptype)) {
                valeursMap.put(vcmptype, new HashMap<String, List<String>>());
            }
            if (!valeursMap.get(vcmptype).containsKey(vcmpnom)) {
                valeursMap.get(vcmptype).put(vcmpnom, new LinkedList<String>());
            }
            valeursMap.get(vcmptype).get(vcmpnom).add(vcmpvaleur);
        });
        listeTypeCPMPossibles = valeursMap.keySet().toArray(new String[]{});
        listeCaracteristiquesCMPPossibles = new ConcurrentHashMap<String, String[]>();
        listeValeurPossibles = new ConcurrentHashMap<String, String[]>();
        valeursMap.entrySet().forEach((entryType) -> {
            String type = entryType.getKey();
            Set<String> caracteristiques = entryType.getValue().keySet();
            listeCaracteristiquesCMPPossibles.put(type, caracteristiques.toArray(new String[]{}));
            entryType.getValue().entrySet().forEach((entryCar) -> {
                String car = entryCar.getKey();
                List<String> values = entryCar.getValue();
                String typeCaracteristique = String.format("%s/%s", type, car);
                listeValeurPossibles.put(typeCaracteristique, values.toArray(new String[]{}));
            });
        });
    }

    private void persistCMP(Melange melange, ValeurCaracteristiqueMP vcmp, String commentaire) throws BusinessException, BusinessException {
        final CompositionsCMP dbccmp = ccmpDAO.getByNKey(melange, vcmp).orElse(null);

        createOrUpdateCCMP(melange, vcmp, commentaire, dbccmp);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, CompositionsCMP.NAME_ENTITY_JPA);
                int indexcode = tokenizerValues.currentTokenIndex();
                final String codeProduit = tokenizerValues.nextToken();
                final String codeComposant = tokenizerValues.nextToken();
                final Double pourcentage = verifieDouble(tokenizerValues, line, true, errorsReport);
                int index = tokenizerValues.currentTokenIndex();
                final String type = tokenizerValues.nextToken();
                final String nomc = tokenizerValues.nextToken();
                final String valeur = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                //todo r3458 manque pourcentage
                Melange dbmelange = melangeDAO.getByNKey(codeProduit, codeComposant, pourcentage).orElse(null);
                if (dbmelange == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_COMP), line, 2, codeProduit, codeComposant));
                }

                ValeurCaracteristiqueMP dbvcmp = vcmpDAO.getByNKey(type, nomc, valeur).orElse(null);
                if (dbvcmp == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_CMPV_BAD_TNV), line, index, nomc, type, valeur));
                }
                if (!errorsReport.hasErrors()) {
                    persistCMP(dbmelange, dbvcmp, commentaire);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param ccmpDAO
     */
    public void setCcmpDAO(ICompositionCMPDAO ccmpDAO) {
        this.ccmpDAO = ccmpDAO;
    }

    /**
     *
     * @param melangeDAO
     */
    public void setMelangeDAO(IMelangeDAO melangeDAO) {
        this.melangeDAO = melangeDAO;
    }

    /**
     *
     * @param vcmpDAO
     */
    public void setVcmpDAO(IValeurCaracteristiqueMPDAO vcmpDAO) {
        this.vcmpDAO = vcmpDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param composantDAO
     */
    public void setComposantDAO(IComposantDAO composantDAO) {
        this.composantDAO = composantDAO;
    }

    private void updateBDCCMP(final Melange melange, ValeurCaracteristiqueMP vcmp, String commentaire, final CompositionsCMP dbcompositioncmp) throws BusinessException {
        try {
            dbcompositioncmp.setMelange(melange);
            dbcompositioncmp.setValeurcaracteristiquemp(vcmp);
            dbcompositioncmp.setCommentaire(commentaire);
            vcmpDAO.saveOrUpdate(vcmp);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update dbcompositioncmp");
        }
    }

}
