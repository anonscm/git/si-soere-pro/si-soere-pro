/*
 *
 */
package org.inra.ecoinfo.pro.refdata.profillocalisation;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class JPAProfilLocalisationDAO extends AbstractJPADAO<ProfilLocalisation> implements IProfilLocalisationDAO {

    /**
     *
     * @return
     */
    @Override
    public List<ProfilLocalisation> getAll() {
        return getAll(ProfilLocalisation.class);
    }

    /**
     *
     * @param profil_localisation_nom
     * @return
     */
    @Override
    public Optional<ProfilLocalisation> getByNkey(String profil_localisation_nom) {
        CriteriaQuery<ProfilLocalisation> query = builder.createQuery(ProfilLocalisation.class);
        Root<ProfilLocalisation> profilLocalisation = query.from(ProfilLocalisation.class);
        query
                .select(profilLocalisation)
                .where(
                        builder.equal(profilLocalisation.get(ProfilLocalisation_.profil_localisation_nom), profil_localisation_nom)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeProfilInterne
     * @return
     */
    @Override
    public Optional<ProfilLocalisation> getByNkey(int codeProfilInterne) {
        CriteriaQuery<ProfilLocalisation> query = builder.createQuery(ProfilLocalisation.class);
        Root<ProfilLocalisation> profilLocalisation = query.from(ProfilLocalisation.class);
        query
                .select(profilLocalisation)
                .where(
                        builder.equal(profilLocalisation.get(ProfilLocalisation_.code_profil_donesol), codeProfilInterne)
                );
        return getOptional(query);
    }

}
