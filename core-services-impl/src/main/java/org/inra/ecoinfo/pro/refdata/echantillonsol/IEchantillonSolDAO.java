/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsol;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.prelevementsol.PrelevementSol;

/**
 *
 * @author adiankha
 */
public interface IEchantillonSolDAO extends IDAO<EchantillonsSol> {

    /**
     *
     * @return
     */
    List<EchantillonsSol> getAll();

    /**
     *
     * @param baseechantillon
     * @param prelevementSol
     * @return
     */
    Optional<EchantillonsSol> getByNKey(Long baseechantillon, PrelevementSol prelevementSol);

    /**
     *
     * @param codeEchantillonSol
     * @return
     */
    Optional<EchantillonsSol> getByNKey(String codeEchantillonSol);

}
