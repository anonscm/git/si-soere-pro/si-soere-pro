/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeintervention;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author vjkoyao
 */
public class JPATypeInterventionDAO extends AbstractJPADAO<TypeIntervention> implements ITypeInterventionDAO {

    /**
     *
     * @param type_intervention_nom
     * @return
     */
    @Override
    public Optional<TypeIntervention> getByNKey(String type_intervention_nom) {
        CriteriaQuery<TypeIntervention> query = builder.createQuery(TypeIntervention.class);
        Root<TypeIntervention> typeIntervention = query.from(TypeIntervention.class);
        query
                .select(typeIntervention)
                .where(
                        builder.equal(typeIntervention.get(TypeIntervention_.type_intervention_nom), type_intervention_nom)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<TypeIntervention> getAll() {
        return getAll(TypeIntervention.class);
    }

}
