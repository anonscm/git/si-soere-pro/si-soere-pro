/*
 *
 */
package org.inra.ecoinfo.pro.refdata.profillocalisation;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author Vivianne
 *
 */
// TODO: Auto-generated Javadoc
/**
 * The Interface IReferenceBibliographiqueDAO.
 */
public interface IProfilLocalisationDAO extends IDAO<ProfilLocalisation> {

    /**
     *
     * @return
     */
    List<ProfilLocalisation> getAll();

    /**
     *
     * @param codeProfilInterne
     * @return
     */
    Optional<ProfilLocalisation> getByNkey(int codeProfilInterne);

    /**
     *
     * @param profil_localisation_nom
     * @return
     */
    Optional<ProfilLocalisation> getByNkey(String profil_localisation_nom);
}
