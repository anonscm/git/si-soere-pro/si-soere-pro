/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.partieprelevee;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<PartiePrelevee> {
    
    IPartiePreleveeDAO partiePreleveeDAO;
    private Properties commentEn;
    
    private void createPartie(final PartiePrelevee partie) throws BusinessException {
        try {
            partiePreleveeDAO.saveOrUpdate(partie);
            partiePreleveeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create partie");
        }
    }
    
    private void updateBDPartie(String code, String nom, String code_util, String commentaire, PartiePrelevee dbpartie) throws BusinessException {
        try {
            dbpartie.setCode(code);
            dbpartie.setNom(nom);
            dbpartie.setCode_util(code_util);
            dbpartie.setCommentaire(commentaire);
            partiePreleveeDAO.saveOrUpdate(dbpartie);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update partie");
        }
    }
    
    private void createOrUpdatePartie(final String code, String nom, String code_util, String commentaire, final PartiePrelevee dbespece) throws BusinessException {
        if (dbespece == null) {
            final PartiePrelevee partie = new PartiePrelevee(code, nom, code_util, commentaire);
            partie.setCode(code);
            partie.setNom(nom);
            partie.setCode_util(code_util);
            partie.setCommentaire(commentaire);
            
            createPartie(partie);
        } else {
            updateBDPartie(code, nom, code_util, commentaire, dbespece);
        }
    }
    
    private void persistEtapes(final String code, final String nom, String code_util, String commentaire) throws BusinessException, BusinessException {
        final PartiePrelevee dbEspece = partiePreleveeDAO.getByNKey(nom).orElse(null);
        createOrUpdatePartie(code, nom, code_util, commentaire, dbEspece);
    }
    
    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final PartiePrelevee dbEspece = partiePreleveeDAO.getByNKey(code)
                        .orElseThrow(() -> new BusinessException("can't get partie"));
                partiePreleveeDAO.remove(dbEspece);
                
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, PartiePrelevee.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String code_util = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistEtapes(code, nom, code_util, commentaire);
                }
                values = csvp.getLine();
            }
            
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<PartiePrelevee> getAllElements() throws BusinessException {
        return partiePreleveeDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PartiePrelevee partie) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(partie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : partie.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(partie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : partie.getCode_util(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(partie == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : partie.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(partie == null || partie.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(partie.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @param partiePreleveeDAO
     */
    public void setPartiePreleveeDAO(IPartiePreleveeDAO partiePreleveeDAO) {
        this.partiePreleveeDAO = partiePreleveeDAO;
    }
    
    @Override
    protected ModelGridMetadata<PartiePrelevee> initModelGridMetadata() {
        commentEn = localizationManager.newProperties(PartiePrelevee.NAME_ENTITY_JPA, PartiePrelevee.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
        
    }
    
}
