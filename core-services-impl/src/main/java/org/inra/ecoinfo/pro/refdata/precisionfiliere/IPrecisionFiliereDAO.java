/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionfiliere;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IPrecisionFiliereDAO extends IDAO<Precisionfiliere> {

    /**
     *
     * @return
     */
    List<Precisionfiliere> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Precisionfiliere> getByNKey(String nom);

}
