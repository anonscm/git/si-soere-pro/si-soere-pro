/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationechantillon;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<LocalisationEchantillon> {

    ILocalisationEchantillonDAO lEchantillonDAO;
    Properties lEchanNomEn;
    Properties lEchanComEn;

    private void cretateLocalisationEchantillon(LocalisationEchantillon localisationEchantillon) throws BusinessException {
        try {
            lEchantillonDAO.saveOrUpdate(localisationEchantillon);
            lEchantillonDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void updateLocalisationEchantillon(String code, String nom, String commentaire, LocalisationEchantillon dbLocalisationEchantillon) throws BusinessException {
        try {
            dbLocalisationEchantillon.setCode(code);
            dbLocalisationEchantillon.setNom(nom);
            dbLocalisationEchantillon.setCommentaire(commentaire);
            lEchantillonDAO.saveOrUpdate(dbLocalisationEchantillon);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void createOrUpdateLEchantillon(String code, String nom, String commentaire, LocalisationEchantillon dbLocalisationEchantillon) throws BusinessException {
        if (dbLocalisationEchantillon == null) {
            LocalisationEchantillon lEchantillon = new LocalisationEchantillon(code, nom, commentaire);
            lEchantillon.setCode(code);
            lEchantillon.setNom(nom);
            lEchantillon.setCommentaire(commentaire);
            cretateLocalisationEchantillon(lEchantillon);
        } else {
            updateLocalisationEchantillon(code, nom, commentaire, dbLocalisationEchantillon);
        }

    }

    private void persitLEchantillon(String code, String nom, String commentaire) throws BusinessException, BusinessException {
        LocalisationEchantillon localisation = lEchantillonDAO.getByNKey(code).orElse(null);
        createOrUpdateLEchantillon(code, nom, commentaire, localisation);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String nom = tokenizerValues.nextToken();
                LocalisationEchantillon lieu = lEchantillonDAO.getByNKey(Utils.createCodeFromString(nom))
                        .orElseThrow(() -> new BusinessException("can't find Echantillon"));
                lEchantillonDAO.remove(lieu);
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<LocalisationEchantillon> getAllElements() throws BusinessException {
        return lEchantillonDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(LocalisationEchantillon localisationEchantillon) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(localisationEchantillon == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : localisationEchantillon.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(localisationEchantillon == null || localisationEchantillon.getNom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : lEchanNomEn.getProperty(localisationEchantillon.getNom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(localisationEchantillon == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : localisationEchantillon.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(localisationEchantillon == null || localisationEchantillon.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : lEchanComEn.getProperty(localisationEchantillon.getCommentaire()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        return lineModelGridMetadata;
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, LocalisationEchantillon.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                persitLEchantillon(code, nom, commentaire);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected ModelGridMetadata<LocalisationEchantillon> initModelGridMetadata() {
        lEchanNomEn = localizationManager.newProperties(LocalisationEchantillon.NAME_ENTITY_JPA, LocalisationEchantillon.JPA_COLUMN_NAME_LOCALISATION, Locale.ENGLISH);
        lEchanComEn = localizationManager.newProperties(LocalisationEchantillon.NAME_ENTITY_JPA, LocalisationEchantillon.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param lEchantillonDAO
     */
    public void setlEchantillonDAO(ILocalisationEchantillonDAO lEchantillonDAO) {
        this.lEchantillonDAO = lEchantillonDAO;
    }

}
