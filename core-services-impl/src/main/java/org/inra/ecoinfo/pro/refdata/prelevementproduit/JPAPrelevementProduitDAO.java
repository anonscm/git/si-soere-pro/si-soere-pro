/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementproduit;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAPrelevementProduitDAO extends AbstractJPADAO<PrelevementProduit> implements IPrelevementProduitDAO {

    /**
     *
     * @return
     */
    @Override
    public List<PrelevementProduit> getAll() {
        return getAllBy(PrelevementProduit.class, PrelevementProduit::getCodeprelevement);
    }

    /**
     *
     * @param date
     * @param numeroRepetition
     * @param produitdispositif
     * @param traitement
     * @param bloc
     * @param placette
     * @return
     */
    @Override
    public Optional<PrelevementProduit> getByNkey(
            LocalDate date,
            int numeroRepetition,
            ProduitDispositif produitdispositif,
            DescriptionTraitement traitement,
            Bloc bloc,
            Placette placette
    ) {
        CriteriaQuery<PrelevementProduit> query = builder.createQuery(PrelevementProduit.class);
        Root<PrelevementProduit> prelevementProduit = query.from(PrelevementProduit.class);
        Join<PrelevementProduit, ProduitDispositif> prodDisp = prelevementProduit.join(PrelevementProduit_.produitdispositif);
        Join<PrelevementProduit, DescriptionTraitement> descTrait = prelevementProduit.join(PrelevementProduit_.traitement);
        Join<PrelevementProduit, Bloc> blo = prelevementProduit.join(PrelevementProduit_.bloc);
        Join<PrelevementProduit, Placette> plac = prelevementProduit.join(PrelevementProduit_.placette);
        query
                .select(prelevementProduit)
                .where(
                        builder.equal(prelevementProduit.get(PrelevementProduit_.date_prelevement), date),
                        builder.equal(prelevementProduit.get(PrelevementProduit_.numerorepetition), numeroRepetition),
                        builder.equal(prodDisp, produitdispositif),
                        builder.equal(descTrait, traitement),
                        builder.equal(blo, bloc),
                        builder.equal(plac, placette)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeprelevement
     * @return
     */
    @Override
    public Optional<PrelevementProduit> getByNKey(String codeprelevement) {
        CriteriaQuery<PrelevementProduit> query = builder.createQuery(PrelevementProduit.class);
        Root<PrelevementProduit> prelevementProduit = query.from(PrelevementProduit.class);
        query
                .select(prelevementProduit)
                .where(
                        builder.equal(prelevementProduit.get(PrelevementProduit_.codeprelevement), Utils.createCodeFromString(codeprelevement))
                );
        return getOptional(query);
    }
}
