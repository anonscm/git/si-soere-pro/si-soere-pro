package org.inra.ecoinfo.pro.refdata.methodeetape;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class MethodeEtapeDAOImpl extends AbstractJPADAO<MethodeEtapes> implements IMethodeEtapeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<MethodeEtapes> getAll() {
        return getAll(MethodeEtapes.class);
    }

    /**
     *
     * @param me_nom
     * @return
     */
    @Override
    public Optional<MethodeEtapes> getByNKey(String me_nom) {
        CriteriaQuery<MethodeEtapes> query = builder.createQuery(MethodeEtapes.class);
        Root<MethodeEtapes> methodeEtapes = query.from(MethodeEtapes.class);
        query
                .select(methodeEtapes)
                .where(
                        builder.equal(methodeEtapes.get(MethodeEtapes_.me_nom), me_nom)
                );
        return getOptional(query);
    }
}
