package org.inra.ecoinfo.pro.refdata.prelevementsol;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.echelleprelevement.IEchellePrelevementDAO;
import org.inra.ecoinfo.pro.refdata.horizon.Horizon;
import org.inra.ecoinfo.pro.refdata.horizon.IHorizonDAO;
import org.inra.ecoinfo.pro.refdata.localisationprelevement.ILocalisationPrelevementDAO;
import org.inra.ecoinfo.pro.refdata.localisationprelevement.LocalisationPrelevement;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.IPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author Vivianne adiankha
 */
public class Recorder extends AbstractGenericRecorder<PrelevementSol> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_CODEBLOC = "PROPERTY_MSG_PRE_SOL_BAD_CODEBLOC";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_CODEPE = "PROPERTY_MSG_PRE_SOL_BAD_CODEPE";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_CODEPLACETTE = "PROPERTY_MSG_PRE_SOL_BAD_CODEPLACETTE";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_DISPOSITIF = "PROPERTY_MSG_PRE_SOL_BAD_DISPOSITIF";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_HORIZON = "PROPERTY_MSG_PRE_SOL_BAD_HORIZON";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_TRAITEMENT = "PROPERTY_MSG_PRE_SOL_BAD_TRAITEMENT";
    private static final String PROPERTY_MSG_PRE_SOL_BAD_LOCA_PRELEVEMENT = "PROPERTY_MSG_PRE_SOL_BAD_LOCA_PRELEVEMENT";
    private static final String PROPERTY_MESSAGE_BAD_DATE_FORMAT = "PROPERTY_MESSAGE_BAD_DATE_FORMAT";

    IPrelevementSolDAO prelevementsolDAO;
    IEchellePrelevementDAO echellePrelevementDAO;
    IDispositifDAO dispositifDAO;
    IBlocDAO blocDAO;
    IHorizonDAO horizonDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IPlacetteDAO placetteDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;
    ILocalisationPrelevementDAO localisationDAO;

    private String[] listeDispoPossible;
    private String[] listeHorizonPossibles;
    private String[] listeLocalisationPossible;
    private ConcurrentMap<String, String[]> listeTraitPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listePlacettesPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listeBlocsPossibles = new ConcurrentHashMap();
    private ConcurrentMap<String, String[]> listeParcelleEPossibles = new ConcurrentHashMap();
    private ConcurrentMap<Dispositif, String> dispositifsKeys = new ConcurrentHashMap();
    private String[] listeBooleansPossible;
    Properties commentaireEn;

    /**
     *
     * @param prelevement
     * @throws BusinessException
     */
    public void createPrelevement(PrelevementSol prelevement) throws BusinessException {
        try {
            prelevementsolDAO.saveOrUpdate(prelevement);
            prelevementsolDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prelevement");
        }
    }

    private void updatePrelevement(int limit_superieur, int limit_inferieur, String point, ParcelleElementaire pelementaire,
            String outils_prelevement, PrelevementSol dbprelevementsol, LocalisationPrelevement localisation,
            String commentaire, Horizon horizon, Bloc bloc,
            Placette placette) throws BusinessException {
        try {
            dbprelevementsol.setLimit_superieur(limit_superieur);
            dbprelevementsol.setLimit_inferieur(limit_inferieur);
            dbprelevementsol.setReferencement(point);
            dbprelevementsol.setOutils_prelevement(outils_prelevement);
            dbprelevementsol.setCommentaire(commentaire);
            dbprelevementsol.setBloc(bloc);
            dbprelevementsol.setHorizon(horizon);
            dbprelevementsol.setPelementaire(pelementaire);
            dbprelevementsol.setPlacette(placette);
            dbprelevementsol.setLocalisationprelevement(localisation);
            prelevementsolDAO.saveOrUpdate(dbprelevementsol);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create prelevement");
        }
    }

    private void createOrUpdatePrelevement(int limit_superieur, int limit_inferieur, LocalDate date_prelevement, String point, String codesol, LocalisationPrelevement localisation,
            String outils_prelevement, PrelevementSol dbprelevement, DescriptionTraitement traitement, Dispositif dispositif,
            String commentaire, Bloc bloc, Horizon horizon, ParcelleElementaire pelementaire, Placette placette) throws BusinessException {
        if (dbprelevement == null) {
            final PrelevementSol prelevement = new PrelevementSol();
            prelevement.setTraitement(traitement);
            prelevement.setDispositif(dispositif);
            prelevement.setDate_prelevement(date_prelevement);
            prelevement.setCodesol(codesol);
            prelevement.setLimit_superieur(limit_superieur);
            prelevement.setLimit_inferieur(limit_inferieur);
            prelevement.setReferencement(point);
            prelevement.setOutils_prelevement(outils_prelevement);
            prelevement.setCommentaire(commentaire);
            prelevement.setBloc(bloc);
            prelevement.setHorizon(horizon);
            prelevement.setPelementaire(pelementaire);
            prelevement.setPlacette(placette);
            prelevement.setLocalisationprelevement(localisation);
            createPrelevement(prelevement);
        } else {
            updatePrelevement(limit_superieur, limit_inferieur, point, pelementaire, outils_prelevement, dbprelevement, localisation, commentaire, horizon, bloc, placette);
        }
    }

    private void persistPrelevementsols(int limit_superieur, int limit_inferieur, LocalDate date_prelevement, String point, String codesol, LocalisationPrelevement localisation,
            String outils_prelevement, DescriptionTraitement traitement, Dispositif dispositif, ParcelleElementaire pelementaire, Placette placette,
            String commentaire, Bloc bloc, Horizon horizon) throws BusinessException, BusinessException {

        final PrelevementSol dbprelevement = prelevementsolDAO.getByNKey(date_prelevement, limit_superieur, limit_inferieur, traitement, dispositif, bloc, pelementaire, placette).orElse(null);
        createOrUpdatePrelevement(limit_superieur, limit_inferieur, date_prelevement, point, codesol, localisation, outils_prelevement, dbprelevement, traitement, dispositif, commentaire, bloc, horizon, pelementaire, placette);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String date = tokenizerValues.nextToken();
                String dispo = tokenizerValues.nextToken();
                String trait = tokenizerValues.nextToken();
                String bloc = tokenizerValues.nextToken();
                String pe = tokenizerValues.nextToken();
                String placette = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                int sup = Integer.parseInt(tokenizerValues.nextToken());
                int inf = Integer.parseInt(tokenizerValues.nextToken());
                String codeDispositif = Dispositif.getNomDispositif(dispo);
                String nomLieu = Dispositif.getNomLieu(dispo);
                String codePrelevement = PrelevementSol.buildCodePrelevement(date, codeDispositif, nomLieu, trait, bloc, pe, placette, sup, inf);
                PrelevementSol dbprelsol = prelevementsolDAO.getByNKey(codePrelevement)
                        .orElseThrow(() -> new BusinessException("can't get prelevement sol"));
                prelevementsolDAO.remove(dbprelsol);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void listeHorizonPossibles() {
        List<Horizon> groupehorizon = horizonDAO.getAll(Horizon.class);
        String[] Listehorizon = new String[groupehorizon.size() + 1];
        Listehorizon[0] = "";
        int index = 1;
        for (Horizon horizon : groupehorizon) {
            Listehorizon[index++] = horizon.getHorizon_nom();
        }
        this.listeHorizonPossibles = Listehorizon;
    }

    private void listeLocalisationPossibles() {
        List<LocalisationPrelevement> groupelocalisation = localisationDAO.getAll(LocalisationPrelevement.class);
        String[] Listelocalisation = new String[groupelocalisation.size() + 1];
        Listelocalisation[0] = "";
        int index = 1;
        for (LocalisationPrelevement localisation : groupelocalisation) {
            Listelocalisation[index++] = localisation.getNom();
        }
        this.listeLocalisationPossible = Listelocalisation;
    }

    private void initTraitementDispositifPossibles() {
        Map<String, List<String>> dispositifTraitList = new HashMap<>();
        descriptionTraitementDAO.getAll().stream()
                .forEach(dt -> {
                    List<String> traitementList = dispositifTraitList
                            .computeIfAbsent(dt.getDispositif().getNomDispositif_nomLieu(), k -> new LinkedList<>());
                    if(!traitementList.contains(dt.getTypeTraitement().getCode())){
                        traitementList.add(dt.getTypeTraitement().getCode());
                    }
                });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeTraitPossibles);
        String[] listeDispoPossible2 = readMapOfValuesPossibles(dispositifTraitList, listOfMapOfValuesPossibles, new LinkedList<String>());
        Set<String> s1 = new HashSet<String>(Arrays.asList(listeDispoPossible));
        Set<String> s2 = new HashSet<String>(Arrays.asList(listeDispoPossible2));
        s1.retainAll(s2);
        listeDispoPossible = s1.toArray(new String[s1.size()]);
    }

    private void initDispositifBlocPossibles() {
        Map<String, Map<String, Map<String, List<String>>>> dispBlocPEPlacette = new HashMap<String, Map<String, Map<String, List<String>>>>();
        placetteDAO.getAll(Placette.class)
                .stream()
                .forEach(pl -> {
                    dispBlocPEPlacette
                            .computeIfAbsent(pl.getParcelleElementaire().getBloc().getDispositif().getNomDispositif_nomLieu(), k -> new HashMap<String, Map<String, List<String>>>())
                            .computeIfAbsent(pl.getParcelleElementaire().getBloc().getNom(), k -> new HashMap<String, List<String>>())
                            .computeIfAbsent(pl.getParcelleElementaire().getName(), k -> new LinkedList<String>())
                            .add(pl.getNom());
                });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeBlocsPossibles);
        listOfMapOfValuesPossibles.add(listeParcelleEPossibles);
        listOfMapOfValuesPossibles.add(listePlacettesPossibles);
        listeDispoPossible = readMapOfValuesPossibles(dispBlocPEPlacette, listOfMapOfValuesPossibles, new LinkedList<String>());
    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    /*Verification du format date*/
    private LocalDate verifDate(final String date_prelevement) {
        LocalDate madate = null;

        try {
            madate = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, date_prelevement);

        } catch (DateTimeParseException e) {
            LOGGER.info("can't parse date");
        }
        return madate;
    }

    String buildStringDateMiseEnService(final PrelevementSol prelevementSol) {
        String dateDebut = org.apache.commons.lang.StringUtils.EMPTY;
        if (prelevementSol != null && prelevementSol.getDate_prelevement() != null) {
            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(prelevementSol.getDate_prelevement(), this.datasetDescriptor.getColumns().get(0).getFormatType());
        }
        return dateDebut;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(
            PrelevementSol prelevementsol) throws BusinessException {

        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : this.
                                buildStringDateMiseEnService(prelevementsol),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        String valeurdisp = prelevementsol == null || prelevementsol.getBloc() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getBloc().getDispositif().getNomDispositif_nomLieu();
        ColumnModelGridMetadata columnDisp = new ColumnModelGridMetadata(valeurdisp, listeDispoPossible, null, true, false, true);
        String valeurTrait = prelevementsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getTraitement().getCode() != null ? prelevementsol.getTraitement().getCode() : "";
        ColumnModelGridMetadata columnTrait = new ColumnModelGridMetadata(valeurTrait, listeTraitPossibles, null, true, false, true);
        String nomBloc = prelevementsol == null ? "" : prelevementsol.getBloc().getNom();
        ColumnModelGridMetadata columnBloc = new ColumnModelGridMetadata(prelevementsol == null ? Constantes.STRING_EMPTY : nomBloc, listeBlocsPossibles,
                null, true, false, true);
        String valeurPE = prelevementsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getPelementaire().getName() != null ? prelevementsol.getPelementaire().getName() : "";
        ColumnModelGridMetadata columnPE = new ColumnModelGridMetadata(valeurPE, listeParcelleEPossibles, null, true, false, true);

        String valeurPlacette = prelevementsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getPlacette().getNom() != null ? prelevementsol.getPlacette().getNom() : "";
        ColumnModelGridMetadata columnPlacette = new ColumnModelGridMetadata(valeurPlacette, listePlacettesPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsDispositif = new LinkedList<ColumnModelGridMetadata>();

        List<ColumnModelGridMetadata> refsBloc = new LinkedList<ColumnModelGridMetadata>();

        List<ColumnModelGridMetadata> refsPE = new LinkedList<ColumnModelGridMetadata>();

        refsDispositif.add(columnTrait);
        refsDispositif.add(columnBloc);
        refsBloc.add(columnPE);
        refsPE.add(columnPlacette);

        columnTrait.setRefBy(columnDisp);
        columnBloc.setRefBy(columnDisp);
        columnDisp.setRefs(refsDispositif);

        columnPE.setRefBy(columnBloc);
        columnBloc.setRefs(refsBloc);

        columnPlacette.setRefBy(columnPE);
        columnPE.setRefs(refsPE);

        columnTrait.setValue(valeurTrait);
        columnBloc.setValue(nomBloc);
        columnPE.setValue(valeurPE);
        columnPlacette.setValue(valeurPlacette);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDisp);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnTrait);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnBloc);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPE);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPlacette);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null || prelevementsol.getReferencement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getReferencement() != null
                        ? prelevementsol.getReferencement() : "",
                        listeBooleansPossible, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null || prelevementsol.getLocalisationprelevement().getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getLocalisationprelevement().getNom() != null
                        ? prelevementsol.getLocalisationprelevement().getNom()
                        : "", listeLocalisationPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null|| prelevementsol.getHorizon() == null || prelevementsol.getHorizon().getHorizon_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getHorizon().getHorizon_nom() != null
                        ? prelevementsol.getHorizon().getHorizon_nom()
                        : "", listeHorizonPossibles, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getLimit_superieur(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getLimit_inferieur(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null || prelevementsol.getOutils_prelevement() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getOutils_prelevement(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null || prelevementsol.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : prelevementsol.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(prelevementsol == null || prelevementsol.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(prelevementsol.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        return lineModelGridMetadata;
    }

    @SuppressWarnings("static-access")
    @Override
    public void processRecord(CSVParser parser, File file, String encoding)
            throws BusinessException {

        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            long line = 0;
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, PrelevementSol.JPA_NAME_ENTITY);
                String madate = tokenizerValues.nextToken();
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String dispo = tokenizerValues.nextToken();
                int indextrait = tokenizerValues.currentTokenIndex();
                final String codetrait = tokenizerValues.nextToken();
                int indexbloc = tokenizerValues.currentTokenIndex();
                String bloc = tokenizerValues.nextToken();
                int indexpe = tokenizerValues.currentTokenIndex();
                String nomParcelle = tokenizerValues.nextToken();
                int indexpla = tokenizerValues.currentTokenIndex();
                String placette = tokenizerValues.nextToken();
                final String point = tokenizerValues.nextToken();
                int indexloca = tokenizerValues.currentTokenIndex();
                String localisation = tokenizerValues.nextToken();
                int indexhori = tokenizerValues.currentTokenIndex();
                String horizon = tokenizerValues.nextToken();

                int limit_sup = verifieInt(tokenizerValues, line, true, errorsReport);
                int limit_inf = verifieInt(tokenizerValues, line, true, errorsReport);
                final String outils_prelevement = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(dispo);
                String nomLieu = Dispositif.getNomLieu(dispo);

                LocalDate date = checkDateFormat(madate, errorsReport, line);

                Dispositif dbdispo = dispositifDAO.getByNKey(codeDispositif, nomLieu).orElse(null);
                if (dbdispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_DISPOSITIF), line, indexdispo, dispo));
                }

                DescriptionTraitement dbtraitement = descriptionTraitementDAO.getByNKey(codetrait, dbdispo).orElse(null);
                if (dbtraitement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_TRAITEMENT), line, indextrait, codetrait));
                }

                Bloc dbbloc = blocDAO.getByFindBloc(bloc, codeDispositif).orElse(null);
                if (dbbloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_CODEBLOC), line, indexbloc, bloc));
                }

                LocalisationPrelevement dblocalisation = localisationDAO.getByNKey(localisation).orElse(null);
                if (dblocalisation == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_LOCA_PRELEVEMENT), line, indexloca, localisation));
                }

                Horizon dbhorizon = horizonDAO.getByNKey(horizon).orElse(null);
                if (dbbloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_HORIZON), line, indexhori, horizon));
                }

                ParcelleElementaire dbpe = parcelleElementaireDAO.getByNKey(nomParcelle, dbbloc).orElse(null);
                if (dbpe == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_CODEPE), line, indexpe, nomParcelle));
                }

                Placette dbplacette = placetteDAO.getByNameAndParcelle(placette, dbpe).orElse(null);
                if (dbplacette == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PRE_SOL_BAD_CODEPLACETTE), line, indexpla, placette));
                }

                String codefinal = PrelevementSol.buildCodePrelevement(madate, codeDispositif, nomLieu, codetrait, bloc, nomParcelle, placette, limit_sup, limit_inf);

                if (!errorsReport.hasErrors()) {
                    persistPrelevementsols(limit_sup, limit_inf, date, point, codefinal, dblocalisation, outils_prelevement, dbtraitement, dbdispo, dbpe, dbplacette, commentaire, dbbloc, dbhorizon);
                }

                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private LocalDate checkDateFormat(String madate, final ErrorsReport errorsReport, long line) throws NumberFormatException {
        try {
            return DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, madate);
        } catch (DateTimeParseException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(PRO_TYPO_SOURCE_PATH, PROPERTY_MESSAGE_BAD_DATE_FORMAT), line, madate, DateUtil.DD_MM_YYYY));
            return null;
        }
    }

    @Override
    protected ModelGridMetadata<PrelevementSol> initModelGridMetadata() {
        listeHorizonPossibles();
        listeBooleanPossibles();
        listeLocalisationPossibles();
        initDispositifBlocPossibles();
        initTraitementDispositifPossibles();
        commentaireEn = localizationManager.newProperties(PrelevementSol.JPA_NAME_ENTITY, PrelevementSol.JPA_COLUMN_COMMENTAIRE, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    @Override
    protected List<PrelevementSol> getAllElements() throws BusinessException {
        return prelevementsolDAO.getAll();
    }

    /**
     *
     * @param prelevementsolDAO
     */
    public void setPrelevementsolDAO(IPrelevementSolDAO prelevementsolDAO) {
        this.prelevementsolDAO = prelevementsolDAO;
    }

    /**
     *
     * @param echellePrelevementDAO
     */
    public void setEchellePrelevementDAO(IEchellePrelevementDAO echellePrelevementDAO) {
        this.echellePrelevementDAO = echellePrelevementDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param blocDAO
     */
    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    /**
     *
     * @param horizonDAO
     */
    public void setHorizonDAO(IHorizonDAO horizonDAO) {
        this.horizonDAO = horizonDAO;
    }

    /**
     *
     * @param parcelleElementaireDAO
     */
    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     *
     * @param placetteDAO
     */
    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    /**
     *
     * @param descriptionTraitementDAO
     */
    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param localisationDAO
     */
    public void setLocalisationDAO(ILocalisationPrelevementDAO localisationDAO) {
        this.localisationDAO = localisationDAO;
    }

}
