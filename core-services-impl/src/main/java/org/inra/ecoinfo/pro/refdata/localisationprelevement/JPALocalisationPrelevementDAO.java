/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.localisationprelevement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPALocalisationPrelevementDAO extends AbstractJPADAO<LocalisationPrelevement> implements ILocalisationPrelevementDAO {

    /**
     *
     * @return
     */
    @Override
    public List<LocalisationPrelevement> getAll() {
        return getAll(LocalisationPrelevement.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<LocalisationPrelevement> getByNKey(String nom) {
        CriteriaQuery<LocalisationPrelevement> query = builder.createQuery(LocalisationPrelevement.class);
        Root<LocalisationPrelevement> localisationPrelevement = query.from(LocalisationPrelevement.class);
        query
                .select(localisationPrelevement)
                .where(
                        builder.equal(localisationPrelevement.get(LocalisationPrelevement_.nom), nom)
                );
        return getOptional(query);
    }

}
