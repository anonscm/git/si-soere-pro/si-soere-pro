package org.inra.ecoinfo.pro.refdata.methodeprocede;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.process.Process_;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produit.Produits_;

/**
 *
 * @author adiankha
 *
 */
public class MethodeProcedesDAOImpl extends AbstractJPADAO<MethodeProcedes> implements IMethodeProcedesDAO {

    /**
     *
     * @return
     */
    @Override
    public List<MethodeProcedes> getAll() {
        return getAll(MethodeProcedes.class);
    }

    /**
     *
     * @param produits
     * @param process
     * @param pmp_ordre
     * @return
     */
    @Override
    public Optional<MethodeProcedes> getByNKey(Produits produits, Process process, int pmp_ordre) {
        CriteriaQuery<MethodeProcedes> query = builder.createQuery(MethodeProcedes.class);
        Root<MethodeProcedes> methodeProcedes = query.from(MethodeProcedes.class);
        Join<MethodeProcedes, Process> proc = methodeProcedes.join(MethodeProcedes_.process);
        Join<MethodeProcedes, Produits> prod = methodeProcedes.join(MethodeProcedes_.produits);
        query
                .select(methodeProcedes)
                .where(
                        builder.equal(prod, produits),
                        builder.equal(proc, process),
                        builder.equal(methodeProcedes.get(MethodeProcedes_.pmp_ordre), pmp_ordre)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeProduits
     * @param intituleProcess
     * @param pmp_ordre
     * @return
     */
    @Override
    public Optional<MethodeProcedes> getByNKey(String codeProduits, String intituleProcess, int pmp_ordre) {
        CriteriaQuery<MethodeProcedes> query = builder.createQuery(MethodeProcedes.class);
        Root<MethodeProcedes> methodeProcedes = query.from(MethodeProcedes.class);
        Join<MethodeProcedes, Process> proc = methodeProcedes.join(MethodeProcedes_.process);
        Join<MethodeProcedes, Produits> prod = methodeProcedes.join(MethodeProcedes_.produits);
        query
                .select(methodeProcedes)
                .where(
                        builder.equal(prod.get(Produits_.codecomposant), codeProduits),
                        builder.equal(proc.get(Process_.process_intitule), intituleProcess),
                        builder.equal(methodeProcedes.get(MethodeProcedes_.pmp_ordre), pmp_ordre)
                );
        return getOptional(query);
    }
}
