package org.inra.ecoinfo.pro.refdata.placette;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.ICodeParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.IParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.statutplacette.IStatutPlacetteDAO;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public class Recorder extends AbstractGenericRecorder<Placette> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    ICodeParcelleElementaireDAO codeParcelleElementaireDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;
    IStatutPlacetteDAO statutPlacetteDAO;
    IPlacetteDAO placetteDAO;
    IBlocDAO blocDAO;
    private String[] listeDispoPossible;
    private ConcurrentMap<String, String[]> listeBlocsPossibles = new ConcurrentHashMap<>();
    private ConcurrentMap<String, String[]> listeParcelleEPossibles = new ConcurrentHashMap<>();


    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexPEDispLieu = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                int indexbloc = tokenizerValues.currentTokenIndex();
                String nomBloc = tokenizerValues.nextToken();
                int indexpe = tokenizerValues.currentTokenIndex();
                String nomParcelle = tokenizerValues.nextToken();
                String nomPlacette = tokenizerValues.nextToken();
                int indexCodePlacette = tokenizerValues.currentTokenIndex();
                String codePlacette = tokenizerValues.nextToken();
                int indexAnneeDebut = tokenizerValues.currentTokenIndex();
                String anneeDebut = tokenizerValues.nextToken();
                int indexAnneeFin = tokenizerValues.currentTokenIndex();
                String anneeFin = tokenizerValues.nextToken();
                int indexProj = tokenizerValues.currentTokenIndex();
                String systemeProjection = tokenizerValues.nextToken();
                String latitude = tokenizerValues.nextToken();
                String longitude = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();

                int index1 = codeDispositifLieu.indexOf('(');
                int index2 = codeDispositifLieu.indexOf(')');
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = verifieLieu(nomLieu, line + 1, indexPEDispLieu + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line + 1, indexPEDispLieu + 1, errorsReport);

                verifieCodePlacette(codePlacette, line + 1, indexCodePlacette + 1, errorsReport);
                verifieAnnees(anneeDebut, anneeFin, nomPlacette, line + 1, indexAnneeDebut + 1, indexAnneeFin + 1, errorsReport);

                Bloc dbBloc = blocDAO.getByFindBloc(nomBloc, codeDispositif).orElse(null);
                if (dbBloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "BLOC_NONDEFINI"), line, indexbloc, nomBloc));
                }
                ParcelleElementaire dbPE = parcelleElementaireDAO.getByNKey(nomParcelle, dbBloc).orElse(null);
                if (dbPE == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PRCELT_NONDEFINI"), line, indexpe, nomParcelle, codeDispositifLieu));
                }

                Placette dbPlacette = placetteDAO.getByNameAndParcelle(nomPlacette, dbPE).orElse(null);
                // La géolocalisation n'est pas obligatoire
                Geolocalisation geolocalisation = null;
                if (codePlacette != null && !codePlacette.isEmpty() && dbPE != null) {
                    //on traite la géolocalisation si pas d'erreur sur code placette, parcelleElementaire et donc pas sur dbPlacette
                    Geolocalisation ancienGeolocZone = dbPlacette != null ? dbPlacette.getGeolocalisation() : null;
                    geolocalisation = traiteGeolocalisation(systemeProjection, latitude, longitude, geolocalisation, dbPlacette, ancienGeolocZone, errorsReport, line + 1, indexProj + 1);
                }

                if (!errorsReport.hasErrors()) {
                    Placette placette = new Placette(dbPE, geolocalisation, nomPlacette, codePlacette, anneeDebut, anneeFin, commentaire);
                    createOrUpdate(dbPE, geolocalisation, nomPlacette, codePlacette, anneeDebut, anneeFin, commentaire, dbPlacette, placette);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }

        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }

        return dispositif;
    }

    /**
     * @param codePE
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private CodeParcelleElementaire verifieCodeParcelleElementaire(String codePE, long line, int index, ErrorsReport errorsReport) {
        CodeParcelleElementaire codeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(codePE).orElse(null);
        if (codeParcelleElementaire == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPARCELLEELT_NONDEFINI"), line, index, codePE));
        }

        return codeParcelleElementaire;
    }

    /**
     * @param codePE
     * @param codeDispositif
     * @param codeParcelleElementaire
     * @param dispositif
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    /* private ParcelleElementaire verifieParcelleElementaire(String codePE, String codeDispositif, CodeParcelleElementaire codeParcelleElementaire, Dispositif dispositif, long line, int index, ErrorsReport errorsReport)
    {
    	ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByCodeDispositif(codeParcelleElementaire, dispositif);
    	if (parcelleElementaire == null) 
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PRCELT_NONDEFINI"), line, index, codePE, codeDispositif));
    	}
    	
    	return parcelleElementaire;
    }*/
    /**
     * @param codePlacette
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieCodePlacette(String codePlacette, long line, int index, ErrorsReport errorsReport) {
        if (codePlacette == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line, index, datasetDescriptor.getColumns().get(2).getName()));
        }

        try {
            Integer codePlt = Integer.parseInt(codePlacette);
            if (codePlt > 30) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPLACETTE_BADFORMAT"), line, index, codePlacette));
            }
        } catch (NumberFormatException e) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPLACETTE_BADFORMAT"), line, index, codePlacette));
        }
    }

    /**
     * @param annee
     * @param line
     * @param index
     * @param errorsReport
     */
    public void verifieAnneeFormat(String annee, long line, int index, ErrorsReport errorsReport) {
        if (annee != null) {
            try {
                DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, String.format("01/01/%s", annee));
            } catch (DateTimeParseException e) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
            }
        }
    }

    /**
     * @param anneeDebut
     * @param anneeFin
     * @param nomPlacette
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieAnnees(String anneeDebut, String anneeFin, String nomPlacette, long line, int indexD, int indexF, ErrorsReport errorsReport) {
        verifieAnneeFormat(anneeFin, line, indexF, errorsReport);
        verifieAnneeFormat(anneeDebut, line, indexD, errorsReport);

        if (anneeFin != null && new Integer(anneeFin) < new Integer(anneeDebut)) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_PLACETTE"), line, indexF, anneeFin, nomPlacette, anneeDebut));
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#verifLocalisationExiste(org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation, java.lang.Object, org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport)
     */
    /**
     *
     * @param dbGeolocalisation
     * @param dbPlacette
     * @param errorsReport
     * @return
     * @throws BusinessException
     */
    @Override
    public Geolocalisation verifLocalisationExiste(Geolocalisation dbGeolocalisation, Placette dbPlacette, ErrorsReport errorsReport) throws BusinessException {
        Geolocalisation geolocalisation = null;

        // vérifie si cette géolocalisation est déjà associée à une placette et retrouve à laquelle
        Placette placetteGeolocalise = placetteDAO.getByGeolocalisation(dbGeolocalisation).orElse(null);

        if (placetteGeolocalise != null) {
            if (dbPlacette != null) {
                // la géolocalisation de la placette est à modifier en base mais la géolocalisation saisie
                // est déjà associée à une autre placette en base. Dans ce cas erreur, deux placettes différentes
                // ne pourront pas être géolocalisés au même endroit
                if (!dbPlacette.equals(placetteGeolocalise)) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PLACETTEGEOLOC_EXISTE"), placetteGeolocalise.getCode()));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                } else // la géolocalisation de la placette sera à modifier en base
                {
                    geolocalisation = dbGeolocalisation;
                }
            }
        }

        return geolocalisation;
    }

    /**
     * @param parcelleElementaire
     * @param geolocalisation
     * @param nomPlacette
     * @param codePlacette
     * @param anneeDebut
     * @param anneeFin
     * @param commentaire
     * @param dbPlacette
     * @param placette
     * @throws BusinessException
     */
    private void createOrUpdate(ParcelleElementaire parcelleElementaire, Geolocalisation geolocalisation, String nomPlacette, String codePlacette, String anneeDebut, String anneeFin, String commentaire, Placette dbPlacette, Placette placette)
            throws BusinessException {
        try {
            if (dbPlacette == null) {
                placetteDAO.saveOrUpdate(placette);
            } else {
                // si on met à jour la placette en lui enlevant sa geolocalisation, alors supprimer cette ancienne geolocalisation de la base
                if (geolocalisation == null) {
                    Geolocalisation dbDispGeolocalisation = dbPlacette.getGeolocalisation();
                    if (dbDispGeolocalisation != null) {
                        geolocalisationDAO.remove(dbDispGeolocalisation);
                    }
                }

                dbPlacette.setParcelleElementaire(parcelleElementaire);
                dbPlacette.setGeolocalisation(geolocalisation);
                dbPlacette.setNom(nomPlacette);
                dbPlacette.setCode(codePlacette);
                dbPlacette.setAnneeDebut(anneeDebut);
                dbPlacette.setAnneeFin(anneeFin);
                dbPlacette.setCommentaire(commentaire);

                placetteDAO.saveOrUpdate(dbPlacette);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create placette");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomBloc = tokenizerValues.nextToken();
                String nomPE = tokenizerValues.nextToken();
                String codePlacette = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositif);

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElseThrow(() -> new BusinessException("can't find dispositif"));
                Bloc bloc = blocDAO.getByNKey(nomBloc, dispositif).orElseThrow(() -> new BusinessException("can't find bloc"));

                ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByNKey(nomPE, bloc).orElseThrow(() -> new BusinessException("can't find parcelle elentaire"));

                Placette placette = placetteDAO.getByNameAndParcelle(codePlacette, parcelleElementaire)
                        .orElseThrow(() -> new BusinessException("can't get placette"));
                Geolocalisation geolocalisation = placette.getGeolocalisation();

                placetteDAO.remove(placette);

                // si on supprime la placette, on supprime également sa géolocalisation
                if (geolocalisation != null) {
                    geolocalisationDAO.remove(geolocalisation);
                }

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getSystemeProjectionPossibles() {
        List<SystemeProjection> lstSystemeProjections = systemeProjectionDAO.getAll();
        String[] systemeProjectionPossibles = new String[lstSystemeProjections.size() + 1];
        systemeProjectionPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (SystemeProjection systemeProjection : lstSystemeProjections) {
            systemeProjectionPossibles[index++] = systemeProjection.getNom();
        }
        return systemeProjectionPossibles;
    }

    private void initDispositifBlocPossibles() {
        Map<String, Map<String, List<String>>> dispositifBloc = new HashMap<String, Map<String, List<String>>>();
        List<ParcelleElementaire> lstPE = parcelleElementaireDAO.getAll();
        lstPE.forEach((pelementaire) -> {
            String codedisp = pelementaire.getDispositif().getNomDispositif_nomLieu();
            String codeBloc = pelementaire.getBloc().getNom();
            String pElementaire = pelementaire.getName();
            dispositifBloc
                    .computeIfAbsent(codedisp, k -> new HashMap<String, List<String>>())
                    .computeIfAbsent(codeBloc, k -> new LinkedList<String>())
                    .add(pElementaire);
        });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(listeBlocsPossibles);
        listOfMapOfValuesPossibles.add(listeParcelleEPossibles);
        this.listeDispoPossible = readMapOfValuesPossibles(dispositifBloc, listOfMapOfValuesPossibles, new LinkedList<String>());

    }

    public void initNewLine(LineModelGridMetadata lineModelGridMetadata, Placette placette) {

        String valeurdisp = placette == null || placette.getParcelleElementaire() == null || placette.getParcelleElementaire().getDispositif() == null
                ? AbstractCSVMetadataRecorder.EMPTY_STRING
                : placette.getParcelleElementaire().getDispositif().getNomDispositif_nomLieu();
        ColumnModelGridMetadata columnDisp = new ColumnModelGridMetadata(valeurdisp, listeDispoPossible, null, true, false, true);

        String nomBloc = placette == null ? "" : placette.getParcelleElementaire().getBloc().getNom();
        ColumnModelGridMetadata columnBloc = new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : nomBloc, listeBlocsPossibles,
                null, true, false, true);
        String valeurPE = placette == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : placette.getParcelleElementaire().getName() != null ? placette.getParcelleElementaire().getName() : "";
        ColumnModelGridMetadata columnPE = new ColumnModelGridMetadata(valeurPE, listeParcelleEPossibles, null, true, false, true);

        List<ColumnModelGridMetadata> refsDispositif = new LinkedList<ColumnModelGridMetadata>();
        Deque<ColumnModelGridMetadata> columnsToBeLinked = new LinkedList();
        columnsToBeLinked.add(columnDisp);
        columnsToBeLinked.add(columnBloc);
        columnsToBeLinked.add(columnPE);
        setColumnRefRecursive(columnsToBeLinked);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDisp);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnBloc);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Placette placette) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        initNewLine(lineModelGridMetadata, placette);

        // Nom de la placette
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : placette.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // Code de la placette
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : placette.getCode(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // Année de début
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : placette.getAnneeDebut(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        // Année de fin
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : placette.getAnneeFin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        // Géolocalisation : système de projection
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : (placette.getGeolocalisation() == null ? Constantes.STRING_EMPTY
                        : (placette.getGeolocalisation().getSystemeProjection() == null ? Constantes.STRING_EMPTY : placette.getGeolocalisation().getSystemeProjection().getNom())),
                        getSystemeProjectionPossibles(), null, false, false, false));
        // Géolocalisation : latitude
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : (placette.getGeolocalisation() == null ? Constantes.STRING_EMPTY : placette.getGeolocalisation().getLatitude()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));
        // Géolocalisation : longitude
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : (placette.getGeolocalisation() == null ? Constantes.STRING_EMPTY : placette.getGeolocalisation().getLongitude()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));

        // Commentaire
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(placette == null ? Constantes.STRING_EMPTY : placette.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Placette> getAllElements() throws BusinessException {
        return placetteDAO.getAll(Placette.class);
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param codeParcelleElementaireDAO the codeParcelleElementaireDAO to set
     */
    public void setCodeParcelleElementaireDAO(
            ICodeParcelleElementaireDAO codeParcelleElementaireDAO) {
        this.codeParcelleElementaireDAO = codeParcelleElementaireDAO;
    }

    /**
     * @param parcelleElementaireDAO the parcelleElementaireDAO to set
     */
    public void setParcelleElementaireDAO(IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    /**
     * @param statutPlacetteDAO the statutPlacetteDAO to set
     */
    public void setStatutPlacetteDAO(IStatutPlacetteDAO statutPlacetteDAO) {
        this.statutPlacetteDAO = statutPlacetteDAO;
    }

    /**
     * @param placetteDAO the placetteDAO to set
     */
    public void setPlacetteDAO(IPlacetteDAO placetteDAO) {
        this.placetteDAO = placetteDAO;
    }

    @Override
    protected ModelGridMetadata<Placette> initModelGridMetadata() {
        initDispositifBlocPossibles();

        return super.initModelGridMetadata();

    }

    /**
     *
     * @param blocDAO
     */
    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

}
