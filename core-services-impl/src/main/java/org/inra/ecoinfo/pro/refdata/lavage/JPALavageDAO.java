/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.lavage;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPALavageDAO extends AbstractJPADAO<Lavage> implements ILavageDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Lavage> getAll() {
        return getAllBy(Lavage.class, Lavage::getCode);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Lavage> getByName(String nom) {
        CriteriaQuery<Lavage> query = builder.createQuery(Lavage.class);
        Root<Lavage> lavage = query.from(Lavage.class);
        query
                .select(lavage)
                .where(
                        builder.equal(lavage.get(Lavage_.nom), nom)
                );
        return getOptional(query);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<Lavage> getByNKey(String code) {
        CriteriaQuery<Lavage> query = builder.createQuery(Lavage.class);
        Root<Lavage> lavage = query.from(Lavage.class);
        query
                .select(lavage)
                .where(
                        builder.equal(lavage.get(Lavage_.code), code)
                );
        return getOptional(query);
    }
}
