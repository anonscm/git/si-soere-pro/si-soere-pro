package org.inra.ecoinfo.pro.refdata.nomregionalsol;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.region.IRegionDAO;
import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.pro.refdata.typepedelogique.Typepedologique;
import org.inra.ecoinfo.pro.refdata.typepedologique.ITypePedologiqueDAO;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.ITypeSolArvalisDAO;
import org.inra.ecoinfo.pro.refdata.typesolarvalis.Typesolarvalis;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Nomregionalsol> {

    private static final String PROPERTY_MSG_BAD_REGION = "PROPERTY_MSG_BAD_REGION";
    private static final String PROPERTY_MSG_BAD_TYPEPEDO = "PROPERTY_MSG_BAD_TYPEPEDO";
    private static final String PROPERTY_MSG_BAD_ARVALIS = "PROPERTY_MSG_BAD_ARVALIS";
    private static final String PROPERTY_MSG_BAD_ARVALIS_AND_PEDO_FOR_NAME = "PROPERTY_MSG_BAD_ARVALIS_AND_PEDO_FOR_NAME";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected INomRegionalSolDAO nomregionalDAO;

    /**
     *
     */
    protected IRegionDAO regionDAO;

    /**
     *
     */
    protected ITypeSolArvalisDAO tsarvalisDAO;

    /**
     *
     */
    protected ITypePedologiqueDAO typepedoDAO;
    private Properties nregionalEN;

    private String[] listeRegionPossibles;
    private String[] listeTypepedologiquePossibles;
    private String[] listeArvalisPossibles;

    private void createNomregionalsol(final Nomregionalsol nomregionalsol) throws BusinessException {
        try {
            nomregionalDAO.saveOrUpdate(nomregionalsol);
            nomregionalDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create nomregionalsol");
        }
    }

    private void updateNomregionalsol(final String code, String nom, Nomregionalsol dbnomregionalsol, Typepedologique typo,
            Typesolarvalis typearvalis, Region region) throws BusinessException {
        try {
            dbnomregionalsol.setNom(nom);
            dbnomregionalsol.setCode(code);
            dbnomregionalsol.setRegion(region);
            dbnomregionalsol.setTypepedologique(typo);
            dbnomregionalsol.setTypesolarvalis(typearvalis);
            nomregionalDAO.saveOrUpdate(dbnomregionalsol);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update nomregionalsol");
        }

    }

    private void createOrUpdateNomregionalsol(final String code, String nom, Nomregionalsol dbnomregionalsol, Typepedologique typo,
            Typesolarvalis typearvalis, Region region) throws BusinessException, BusinessException {
        if (dbnomregionalsol == null) {
            final Nomregionalsol nomregionalsol = new Nomregionalsol(nom, typo, typearvalis, region);
            nomregionalsol.setCode(code);
            nomregionalsol.setNom(nom);
            nomregionalsol.setRegion(region);
            nomregionalsol.setTypepedologique(typo);
            nomregionalsol.setTypesolarvalis(typearvalis);
            createNomregionalsol(nomregionalsol);
        } else {
            updateNomregionalsol(code, nom, dbnomregionalsol, typo, typearvalis, region);
        }
    }

    private void persistNomregionalsol(Nomregionalsol dbNomregionalsol, final String code, final String nom, Typepedologique typo,
            Typesolarvalis solarvalis, Region region) throws BusinessException, BusinessException {
        createOrUpdateNomregionalsol(code, nom, dbNomregionalsol, typo, solarvalis, region);
    }

    private void listeNomRegion() {
        List<Region> lesregions = regionDAO.getAll();
        String[] Listeregion = new String[lesregions.size() + 1];
        Listeregion[0] = "";
        int index = 1;
        for (Region region : lesregions) {
            Listeregion[index++] = region.getNom();
        }
        this.listeRegionPossibles = Listeregion;
    }

    private void listeNomArvalis() {
        List<Typesolarvalis> lesarvalis = tsarvalisDAO.getAll(Typesolarvalis.class);
        String[] Listearvalis = new String[lesarvalis.size() + 1];
        Listearvalis[0] = "";
        int index = 1;
        for (Typesolarvalis typesolarvalis : lesarvalis) {
            Listearvalis[index++] = typesolarvalis.getNom();
        }
        this.listeArvalisPossibles = Listearvalis;
    }

    private void listeNomTypePedo() {
        List<Typepedologique> lestypepedo = typepedoDAO.getAll(Typepedologique.class);
        String[] Listetypepedo = new String[lestypepedo.size() + 1];
        Listetypepedo[0] = "";
        int index = 1;
        for (Typepedologique typepedologique : lestypepedo) {
            Listetypepedo[index++] = typepedologique.getNom();
        }
        this.listeTypepedologiquePossibles = Listetypepedo;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Nomregionalsol nomregionalsol) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomregionalsol.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null || nomregionalsol.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nregionalEN.getProperty(nomregionalsol.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomregionalsol.getRegion().getNom() != null
                        ? nomregionalsol.getRegion().getNom() : "",
                        listeRegionPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomregionalsol.getTypesolarvalis().getNom() != null
                        ? nomregionalsol.getTypesolarvalis().getNom() : "",
                        listeArvalisPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(nomregionalsol == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : nomregionalsol.getTypepedologique() != null
                        ? nomregionalsol.getTypepedologique().getNom() : "",
                        listeTypepedologiquePossibles, null, true, false, true));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nomregionalsol.NAME_ENTITY_JPA);
               
                final String nomRegionaSol = tokenizerValues.nextToken();
                
                final String region = tokenizerValues.nextToken();
                
                final String arvalis = tokenizerValues.nextToken();
                
                final String typepedo = tokenizerValues.nextToken();
                
                Nomregionalsol nas = nomregionalDAO.getByNKey(arvalis, typepedo, region, nomRegionaSol)
                        .orElseThrow(() -> new BusinessException("can't find Nomregionalsol"));
                nomregionalDAO.remove(nas);
                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Nomregionalsol> getAllElements() throws BusinessException {
        return nomregionalDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nomregionalsol.NAME_ENTITY_JPA);
                final String nomRegionaSol = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nomRegionaSol);
                int index = tokenizerValues.currentTokenIndex();
                final String region = tokenizerValues.nextToken();
                int indexarvalis = tokenizerValues.currentTokenIndex();
                final String arvalis = tokenizerValues.nextToken();
                int indexpedo = tokenizerValues.currentTokenIndex();
                final String typepedo = tokenizerValues.nextToken();
                Region dbregion = regionDAO.getByNom(region).orElse(null);
                if (dbregion == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_REGION), line, index, region));
                }

                Typepedologique dbtypepedo = typepedoDAO.getByNKey(typepedo).orElse(null);
                if (dbtypepedo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_TYPEPEDO), line, indexpedo, typepedo));
                }

                Typesolarvalis dbtypearvalis = tsarvalisDAO.getByNKey(arvalis).orElse(null);
                if (dbtypearvalis == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_ARVALIS), line, indexarvalis, arvalis));
                }
                final Nomregionalsol dbnomregionalsol = nomregionalDAO.getByNKey(dbtypearvalis, dbtypepedo,dbregion,nomRegionaSol).orElse(null);
               
                if (!errorsReport.hasErrors()) {
                    persistNomregionalsol(dbnomregionalsol, code, nomRegionaSol, dbtypepedo, dbtypearvalis, dbregion);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected ModelGridMetadata<Nomregionalsol> initModelGridMetadata() {
        nregionalEN = localizationManager.newProperties(Nomregionalsol.NAME_ENTITY_JPA, Nomregionalsol.JPA_COLUMN_NAME, Locale.ENGLISH);
        listeNomArvalis();
        listeNomRegion();
        listeNomTypePedo();
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public INomRegionalSolDAO getNomregionalDAO() {
        return nomregionalDAO;
    }

    /**
     *
     * @param nomregionalDAO
     */
    public void setNomregionalDAO(INomRegionalSolDAO nomregionalDAO) {
        this.nomregionalDAO = nomregionalDAO;
    }

    /**
     *
     * @return
     */
    public Properties getNregionalEN() {
        return nregionalEN;
    }

    /**
     *
     * @param nregionalEN
     */
    public void setNregionalEN(Properties nregionalEN) {
        this.nregionalEN = nregionalEN;
    }

    /**
     *
     * @return
     */
    public IRegionDAO getRegionDAO() {
        return regionDAO;
    }

    /**
     *
     * @return
     */
    public ITypeSolArvalisDAO getTsarvalisDAO() {
        return tsarvalisDAO;
    }

    /**
     *
     * @return
     */
    public ITypePedologiqueDAO getTypepedoDAO() {
        return typepedoDAO;
    }

    /**
     *
     * @param regionDAO
     */
    public void setRegionDAO(IRegionDAO regionDAO) {
        this.regionDAO = regionDAO;
    }

    /**
     *
     * @param tsarvalisDAO
     */
    public void setTsarvalisDAO(ITypeSolArvalisDAO tsarvalisDAO) {
        this.tsarvalisDAO = tsarvalisDAO;
    }

    /**
     *
     * @param typepedoDAO
     */
    public void setTypepedoDAO(ITypePedologiqueDAO typepedoDAO) {
        this.typepedoDAO = typepedoDAO;
    }

}
