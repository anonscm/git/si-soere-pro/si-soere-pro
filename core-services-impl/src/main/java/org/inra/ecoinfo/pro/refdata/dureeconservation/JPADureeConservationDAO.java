/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dureeconservation;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPADureeConservationDAO extends AbstractJPADAO<DureeConservation> implements IDureeConcervationDAO {

    /**
     *
     * @return
     */
    @Override
    public List<DureeConservation> gelAll() {
        return getAllBy(DureeConservation.class, DureeConservation::getDc_code);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<DureeConservation> getByNKey(String nom) {
        CriteriaQuery<DureeConservation> query = builder.createQuery(DureeConservation.class);
        Root<DureeConservation> dureeConservation = query.from(DureeConservation.class);
        query
                .select(dureeConservation)
                .where(
                        builder.equal(dureeConservation.get(DureeConservation_.dc_nom), nom)
                );
        return getOptional(query);
    }

}
