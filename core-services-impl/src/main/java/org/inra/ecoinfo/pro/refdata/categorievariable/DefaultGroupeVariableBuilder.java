/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.categorievariable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.pro.extraction.vo.GroupeVariableVO;
import org.inra.ecoinfo.pro.extraction.vo.VariableVO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;

/**
 *
 * @author adiankha
 */
public class DefaultGroupeVariableBuilder implements IGroupeVariableBuilder {

    List<GroupeVariableVO> groupesVariables = new ArrayList<>();
    Map<String, GroupeVariableVO> groupesVariablesVOsMap = new HashMap<String, GroupeVariableVO>();

    /**
     *
     * @param variables
     * @return
     */
    @Override
    public List<GroupeVariableVO> build(List<VariablesPRO> variables) {
        variables.forEach((variable) -> {
            VariableVO variableVO = new VariableVO(variable);
            if (variable.getCategorievariable() != null) {
                GroupeVariableVO groupeVariableVO = retrieveOrCreateGroupesVariablesVO(variable.getCategorievariable());
                variableVO.setGroupeVariable(groupeVariableVO);
                if (!groupeVariableVO.getVariables().contains(variableVO)) {
                    groupeVariableVO.getVariables().add(variableVO);
                }
            }
        });
        return groupesVariables;
    }

    private GroupeVariableVO fillRecursiveGroupeVariableVO(CategorieVariable groupeVariable) {
        GroupeVariableVO groupeVariableVO = new GroupeVariableVO(groupeVariable);
        if (groupeVariable.getParent() == null) {
            groupesVariables.add(groupeVariableVO);

        } else {
            GroupeVariableVO parentGroupeVariableVO = retrieveOrCreateGroupesVariablesVO(groupeVariable.getParent());
            groupeVariableVO.setParent(parentGroupeVariableVO);
            parentGroupeVariableVO.getChildren().add(groupeVariableVO);
        }

        return groupeVariableVO;
    }

    private GroupeVariableVO retrieveOrCreateGroupesVariablesVO(CategorieVariable groupeVariable) {
        if (groupesVariablesVOsMap.containsKey(groupeVariable.getCvariable_code())) {
            return groupesVariablesVOsMap.get(groupeVariable.getCvariable_code());
        } else {
            GroupeVariableVO groupeVariableVO = fillRecursiveGroupeVariableVO(groupeVariable);
            groupesVariablesVOsMap.put(groupeVariable.getCvariable_code(), groupeVariableVO);
            return groupeVariableVO;
        }
    }

}
