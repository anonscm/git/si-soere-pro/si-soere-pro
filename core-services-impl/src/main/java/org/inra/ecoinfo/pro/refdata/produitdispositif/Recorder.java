package org.inra.ecoinfo.pro.refdata.produitdispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<ProduitDispositif> {

    private static final String PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE = "PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE";

    private static final String PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU = "PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU";

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    private String[] listeProduitsPossibles;
    private String[] listeDispositifsPossibles;
    private Map<String, String[]> listeLieuPossibles;

    /**
     *
     */
    protected IProduitDisposotifDAO produitdispositifDAO;

    /**
     *
     */
    protected IProduitDAO produitDAO;

    /**
     *
     */
    protected IDispositifDAO dispositifDAO;

    Properties CommentaireEn;

    private void createOrUpdateProduitDispositif(Produits prod, Dispositif dispo, String commentaire, String code, String codeproduit, final ProduitDispositif dbproduitdisposotif) throws BusinessException {
        if (dbproduitdisposotif == null) {
            ProduitDispositif prodisp = new ProduitDispositif(code, codeproduit, prod, dispo, commentaire);
            prodisp.setCode(code);
            prodisp.setCodeproduit(codeproduit);
            prodisp.setProduits(prod);
            prodisp.setDispositif(dispo);
            prodisp.setCommentaire(commentaire);
            createProduitDispositif(prodisp);
        } else {
            updateBDProduitDisposotif(prod, dispo, commentaire, code, codeproduit, dbproduitdisposotif);
        }
    }

    private void createProduitDispositif(final ProduitDispositif produitdisposotif) throws BusinessException {
        try {
            produitdispositifDAO.saveOrUpdate(produitdisposotif);
            produitdispositifDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create produitdisposotif");
        }
    }

    private void updateBDProduitDisposotif(Produits prod, Dispositif dispo, String commentaire, String code, String codeproduit, final ProduitDispositif dbproduitdisposotif) throws BusinessException {
        try {
            dbproduitdisposotif.setProduits(prod);
            dbproduitdisposotif.setDispositif(dispo);
            dbproduitdisposotif.setCommentaire(commentaire);
            dbproduitdisposotif.setCode(code);
            dbproduitdisposotif.setCodeproduit(codeproduit);
            produitdispositifDAO.saveOrUpdate(dbproduitdisposotif);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update produitdisposotif");
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file,
            String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProduitDispositif.NAME_ENTITY_JPA);
                final String codep = tokenizerValues.nextToken();
                final String code = tokenizerValues.nextToken();
                final String lieu = tokenizerValues.nextToken();
                Produits dbproduits = produitDAO.getByNKey(codep).orElse(null);
                if (dbproduits == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE), codep));
                }
                Dispositif dbdispo = dispositifDAO.getByNKey(code, lieu).orElse(null);
                if (dbdispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU), code));
                }

                final ProduitDispositif dbproduitdispositif = produitdispositifDAO.getByNKey(dbproduits, dbdispo)
                        .orElseThrow(() -> new BusinessException("can't find produit-dispositif"));
                produitdispositifDAO.remove(dbproduitdispositif);

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    private void initDispositifPossibles() {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCodeDispo();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifsPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    @Override
    protected List<ProduitDispositif> getAllElements() throws BusinessException {
        return produitdispositifDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProduitDispositif produitdispositif) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produitdispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : produitdispositif.getProduits() != null ? produitdispositif.getProduits().getProd_key()
                        : "", listeProduitsPossibles, null, true, false, true));

        String valeurProduit = produitdispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : produitdispositif.getDispositif() != null ? produitdispositif.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifsPossibles, null, true, false, true);
        String valeurProcede = produitdispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : produitdispositif.getDispositif() != null ? produitdispositif.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produitdispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : produitdispositif.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(produitdispositif == null || produitdispositif.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : CommentaireEn.getProperty(produitdispositif.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    /**
     *
     * @return
     */
    public IProduitDisposotifDAO getProduitdispositifDAO() {
        return produitdispositifDAO;
    }

    @Override
    protected ModelGridMetadata<ProduitDispositif> initModelGridMetadata() {
        initDispositifPossibles();
        listeProduitsPossibles();
        CommentaireEn = localizationManager.newProperties(ProduitDispositif.NAME_ENTITY_JPA, ProduitDispositif.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void listeProduitsPossibles() {
        List<Produits> groupeproduits = produitDAO.getAll();
        String[] listeProduitPossibles = new String[groupeproduits.size() + 1];
        listeProduitPossibles[0] = "";
        int index = 1;
        for (Produits produits : groupeproduits) {
            listeProduitPossibles[index++] = produits.getProd_key();
        }
        this.listeProduitsPossibles = listeProduitPossibles;
    }

    private void persistProduitDispositif(Produits produits, Dispositif dispositif, String commentaire, String code, String codeproduit) throws BusinessException, BusinessException {
        final ProduitDispositif dbproduitdisposotif = produitdispositifDAO.getByNKey(produits, dispositif).orElse(null);
        createOrUpdateProduitDispositif(produits, dispositif, commentaire, code, codeproduit, dbproduitdisposotif);

    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProduitDispositif.NAME_ENTITY_JPA);
                int indexprod = tokenizerValues.currentTokenIndex();
                final String prod_code = tokenizerValues.nextToken();
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String code = tokenizerValues.nextToken();
                int indexlieu = tokenizerValues.currentTokenIndex();
                final String lieu = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();
                Produits dbprod = produitDAO.getByNKey(prod_code).orElse(null);
                if (dbprod == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_PROCUITCODE), line, indexprod, prod_code));
                }

                Dispositif dbdispo = dispositifDAO.getByNKey(code, lieu).orElse(null);
                if (dbdispo == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_DISPO_BAD_DISPOSIF_LIEU), line, indexdispo, code, indexlieu, lieu));
                }
                String codefinal = Utils.createCodeFromString(prod_code + "_" + code + "_" + lieu);
                String codeproduit = prod_code;
                if (!errorsReport.hasErrors()) {

                    persistProduitDispositif(dbprod, dbdispo, commentaire, codefinal, codeproduit);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param produitdispositifDAO
     */
    public void setProduitdispositifDAO(IProduitDisposotifDAO produitdispositifDAO) {
        this.produitdispositifDAO = produitdispositifDAO;
    }

}
