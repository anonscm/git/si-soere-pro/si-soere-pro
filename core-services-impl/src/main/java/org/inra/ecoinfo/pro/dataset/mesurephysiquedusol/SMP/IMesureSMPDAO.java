/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SMP;

;
import java.time.LocalTime;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.MesureSMP;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SMP.SousSequenceSMP;

/**
 *
 * @author vjkoyao
 * @param <T>
 */


public interface IMesureSMPDAO<T> extends IDAO<MesureSMP> {

    /**
     *
     * @param heure
     * @param sousSequenceMPS
     * @return
     */
    Optional<MesureSMP> getByNKeys(LocalTime heure, SousSequenceSMP sousSequenceMPS);

}
