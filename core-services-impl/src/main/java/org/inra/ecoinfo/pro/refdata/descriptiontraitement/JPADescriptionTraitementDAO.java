/**
 *
 */
package org.inra.ecoinfo.pro.refdata.descriptiontraitement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 * @author sophie
 *
 */
public class JPADescriptionTraitementDAO extends AbstractJPADAO<DescriptionTraitement> implements IDescriptionTraitementDAO {

    private static final String QUERY_TRAITEMENT_MODIFIE = "from DescriptionTraitement dctrt where dctrt.code = :code";
    private static final String QUERY_TRAITEMENT_DISPOSITIF = "from DescriptionTraitement dctrt where dctrt.codeunique = :codeunique";

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<DescriptionTraitement> getAll() {
        return getAllBy(DescriptionTraitement.class, DescriptionTraitement::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO #getByCodeDispositif(java.lang.String)
     */

    /**
     *
     * @param code
     * @param dispositif
     * @return
     */

    @Override
    public Optional<DescriptionTraitement> getByNKey(String code, Dispositif dispositif) {
        CriteriaQuery<DescriptionTraitement> query = builder.createQuery(DescriptionTraitement.class);
        Root<DescriptionTraitement> descriptionTraitement = query.from(DescriptionTraitement.class);
        Join<DescriptionTraitement, Dispositif> dispo = descriptionTraitement.join(DescriptionTraitement_.dispositif);
        query
                .select(descriptionTraitement)
                .where(
                        builder.equal(descriptionTraitement.get(DescriptionTraitement_.code), code),
                        builder.equal(dispo, dispositif)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO #getByDispositif(org.inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */

    /**
     *
     * @param dispositif
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    //public List<DescriptionTraitement> getByDispositif(Dispositif dispositif)  {
    public List<DescriptionTraitement> getByDispositif(Dispositif dispositif) {
        CriteriaQuery<DescriptionTraitement> query = builder.createQuery(DescriptionTraitement.class);
        Root<DescriptionTraitement> descriptionTraitement = query.from(DescriptionTraitement.class);
        Join<DescriptionTraitement, Dispositif> dispo = descriptionTraitement.join(DescriptionTraitement_.dispositif);
        query
                .select(descriptionTraitement)
                .where(
                        builder.equal(dispo, dispositif)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO #getByTraitementOrigineCodeDisp(org.inra.ecoinfo.pro.refdata. descriptiontraitement.DescriptionTraitement, org.inra.ecoinfo.pro.refdata.dispositif.Dispositif)
     */

    /**
     *
     * @param traitementOrigine
     * @param dispositif
     * @return
     */

    @Override
    public Optional<DescriptionTraitement> getByTraitementOrigineCodeDisp(DescriptionTraitement traitementOrigine, Dispositif dispositif) {
        CriteriaQuery<DescriptionTraitement> query = builder.createQuery(DescriptionTraitement.class);
        Root<DescriptionTraitement> descriptionTraitement = query.from(DescriptionTraitement.class);
        Join<DescriptionTraitement, DescriptionTraitement> treatment = descriptionTraitement.join(DescriptionTraitement_.traitementOrigine);
        Join<DescriptionTraitement, Dispositif> dispo = descriptionTraitement.join(DescriptionTraitement_.dispositif);

        query
                .select(descriptionTraitement)
                .where(
                        builder.equal(treatment, traitementOrigine),
                        builder.equal(dispo, dispositif)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeunique
     * @return
     */
    @Override
    public Optional<DescriptionTraitement> getByCodeUnique(String codeunique) {
        CriteriaQuery<DescriptionTraitement> query = builder.createQuery(DescriptionTraitement.class);
        Root<DescriptionTraitement> descriptionTraitement = query.from(DescriptionTraitement.class);

        query
                .select(descriptionTraitement)
                .where(
                        builder.equal(descriptionTraitement.get(DescriptionTraitement_.codeunique), codeunique)
                );
        return getOptional(query);
    }
}
