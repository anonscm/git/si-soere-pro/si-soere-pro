package org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.texturesol.ITextureSolDAO;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Teneurcalcaireinitial> {

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_TEXTURE = "PROPERTY_MSG_TEXTURE";
    ITeneurCalcaireInitialDAO tcinitialDAO;
    private Properties tcinitialEN;
    ITextureSolDAO textureDAO;
    private String[] listeTexturesolPossibles;

    private void createTCInitial(final Teneurcalcaireinitial teneurcalcaireinitial) throws BusinessException {
        try {
            tcinitialDAO.saveOrUpdate(teneurcalcaireinitial);
            tcinitialDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create teneurcalcaireinitial");
        }
    }

    private void updateTCInitial(final String nom, Texturesol texture, final Teneurcalcaireinitial dbteneurcalcaireinitial) throws BusinessException {
        try {
            dbteneurcalcaireinitial.setNom(nom);
            dbteneurcalcaireinitial.setTexturesol(texture);
            tcinitialDAO.saveOrUpdate(dbteneurcalcaireinitial);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update teneurcalcaireinitial");
        }
    }

    private void createOrUpdateTCInitial(final String code, String nom, Texturesol texture, final Teneurcalcaireinitial dbteneurcalcaireinitial) throws BusinessException {
        if (dbteneurcalcaireinitial == null) {
            final Teneurcalcaireinitial tci = new Teneurcalcaireinitial(code, nom, texture);
            tci.setCode(code);
            tci.setNom(nom);
            tci.setTexturesol(texture);
            createTCInitial(tci);
        } else {
            updateTCInitial(nom, texture, dbteneurcalcaireinitial);
        }
    }

    private void persistTCInitial(final String code, final String nom, Texturesol texture) throws BusinessException {
        final Teneurcalcaireinitial dbtci = tcinitialDAO.getByNKey(nom, texture).orElse(null);
        createOrUpdateTCInitial(code, nom, texture, dbtci);
    }

    private void listeTSPossibles() {
        List<Texturesol> groupets = textureDAO.getAll(Texturesol.class);
        String[] listetexture = new String[groupets.size() + 1];
        listetexture[0] = "";
        int index = 1;
        for (Texturesol teneur : groupets) {
            listetexture[index++] = teneur.getNom();
        }
        this.listeTexturesolPossibles = listetexture;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Teneurcalcaireinitial teneurcalcaireinitial) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(teneurcalcaireinitial == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : teneurcalcaireinitial.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(teneurcalcaireinitial == null || teneurcalcaireinitial.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : tcinitialEN.getProperty(teneurcalcaireinitial.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(teneurcalcaireinitial == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : teneurcalcaireinitial.getTexturesol().getNom() != null ? teneurcalcaireinitial.getTexturesol().getNom() : "",
                        listeTexturesolPossibles, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Teneurcalcaireinitial.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                int indextex = tokenizerValues.currentTokenIndex();
                final String texture = tokenizerValues.nextToken();
                final Texturesol dbteneur = textureDAO.getByNKey(texture)
                        .orElseThrow(() -> new BusinessException("can't get texture"));
                final Teneurcalcaireinitial dbtcinitial = tcinitialDAO.getByNKey(nom, dbteneur)
                        .orElseThrow(() -> new BusinessException("can't get teneur calcaire initial"));
                tcinitialDAO.remove(dbtcinitial);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    public List<Teneurcalcaireinitial> getAllElements() {
        return tcinitialDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Teneurcalcaireinitial.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                int indextex = tokenizerValues.currentTokenIndex();
                final String texture = tokenizerValues.nextToken();
                final Texturesol dbteneur = textureDAO.getByNKey(texture).orElse(null);
                if (dbteneur == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_TEXTURE), line, indextex, texture));
                }
                persistTCInitial(code, nom, dbteneur);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    public ModelGridMetadata<Teneurcalcaireinitial> initModelGridMetadata() {
        listeTSPossibles();
        tcinitialEN = localizationManager.newProperties(Teneurcalcaireinitial.NAME_ENTITY_JPA, Teneurcalcaireinitial.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public ITeneurCalcaireInitialDAO getTcinitialDAO() {
        return tcinitialDAO;
    }

    /**
     *
     * @param tcinitialDAO
     */
    public void setTcinitialDAO(ITeneurCalcaireInitialDAO tcinitialDAO) {
        this.tcinitialDAO = tcinitialDAO;
    }

    /**
     *
     * @return
     */
    public Properties getTcinitialEN() {
        return tcinitialEN;
    }

    /**
     *
     * @param tcinitialEN
     */
    public void setTcinitialEN(Properties tcinitialEN) {
        this.tcinitialEN = tcinitialEN;
    }

    /**
     *
     * @param textureDAO
     */
    public void setTextureDAO(ITextureSolDAO textureDAO) {
        this.textureDAO = textureDAO;
    }

}
