/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.cultures;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPACulturesDAO extends AbstractJPADAO<Cultures> implements ICulturesDAO {

    @Override
    public List<Cultures> getAll() {
        return getAllBy(Cultures.class, Cultures::getCode);
    }

    @Override
    public Optional<Cultures> getByNKey(String nom) {
        CriteriaQuery<Cultures> query = builder.createQuery(Cultures.class);
        Root<Cultures> cultures = query.from(Cultures.class);
        query
                .select(cultures)
                .where(
                        builder.equal(cultures.get(Cultures_.mycode), Utils.createCodeFromString(nom))
                );
        return getOptional(query);
    }
}
