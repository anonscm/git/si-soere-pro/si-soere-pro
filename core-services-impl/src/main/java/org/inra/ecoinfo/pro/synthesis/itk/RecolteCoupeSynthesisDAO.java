/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.itk;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe_;
import org.inra.ecoinfo.pro.synthesis.recoltecoupe.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.recoltecoupe.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class RecolteCoupeSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurRecolteCoupe, MesureRecolteCoupe> {

    @Override
    Class<ValeurRecolteCoupe> getValueClass() {
        return ValeurRecolteCoupe.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurRecolteCoupe, MesureRecolteCoupe> getMesureAttribute() {
        return ValeurRecolteCoupe_.mesureinterventionrecolte;
    }

    @Override
    Boolean isValue() {
        return Boolean.FALSE;
    }

}
