/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.IMesureTravailduSolDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordRecolteTravailSol extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordRecolteTravailSol.class);

    protected static final String BUNDLE_PATH_TRAVAILSOL = "org.inra.ecoinfo.pro.dataset.itk.messages";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_NA_DB = "MSG_ERROR_RC_LISTEITINERAIRE_NA_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_VV_DB = "MSG_ERROR_RC_LISTEITINERAIRE_VV_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_PR_DB = "MSG_ERROR_RC_LISTEITINERAIRE_PR_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_LP_DB = "MSG_ERROR_RC_LISTEITINERAIRE_LP_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_CT_DB = "MSG_ERROR_RC_LISTEITINERAIRE_CT_DB";
    private static final String MSG_ERROR_RC_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_CA_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_CA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_CONDITIONT_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_CONDITIONT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_VENTV_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_VENTV_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_NIVEAU_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_NIVEAU_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_PT_LISTEITINERAIRE_LP_DB = "MSG_ERROR_PT_LISTEITINERAIRE_LP_DB";
    private static final String MSG_ERROR_PT_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_PT_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_TRAITEMENT_TRAVAIL_NOT_FOUND_IN_DB = "MSG_ERROR_TRAITEMENT_TRAVAIL_NOT_FOUND_IN_DB";
    IMesureTravailduSolDAO<MesureTravailDuSol> mesureTravailSolDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    IListeItineraireDAO listeItineraireDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    public ProcessRecordRecolteTravailSol() {
        super();
    }

    private long readLines(final CSVParser parser, final Map<LocalDate, List<TravailSolLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate datedebut = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codedispositif = cleanerValues.nextToken();
            final String codetraitement = cleanerValues.nextToken();
            final String codeparcelle = cleanerValues.nextToken();
            final String nomplacette = cleanerValues.nextToken();
            final String localisationprecide = cleanerValues.nextToken();
            final LocalDate datefin = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String interventionsol = cleanerValues.nextToken();
            final String profondeurtravail = cleanerValues.nextToken();
            final String materieltravail1 = cleanerValues.nextToken();
            final String materieltravail2 = cleanerValues.nextToken();
            final String materieltravail3 = cleanerValues.nextToken();
            final String largeurtravail = cleanerValues.nextToken();
            final String culture = cleanerValues.nextToken();
            final String codebbch = cleanerValues.nextToken();
            final String precisionstade = cleanerValues.nextToken();
            final String conditionhumidite = cleanerValues.nextToken();
            final String conditiontemperature = cleanerValues.nextToken();
            final String vitessevent = cleanerValues.nextToken();
            final String observationqualite = cleanerValues.nextToken();
            final String nomobservation = cleanerValues.nextToken();
            final String niveauatteint = cleanerValues.nextToken();
            final String commentaire = cleanerValues.nextToken();

            final TravailSolLineRecord line = new TravailSolLineRecord(lineCount, datedebut, codedispositif, codetraitement, codeparcelle, nomplacette, localisationprecide, datefin, interventionsol, profondeurtravail, materieltravail1, materieltravail2, materieltravail3, largeurtravail, culture, codebbch, precisionstade, conditionhumidite, conditiontemperature, vitessevent, observationqualite, nomobservation, niveauatteint, commentaire);
            try {
                if (!lines.containsKey(datedebut)) {
                    lines.put(datedebut, new LinkedList<TravailSolLineRecord>());
                }
                lines.get(datedebut).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        datedebut, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void recordErrors(final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<TravailSolLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<TravailSolLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, datasetDescriptorPRO, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordRecolteTravailSol.class).error(ex.getMessage(), ex);
        }
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<TravailSolLineRecord>> lines, DatasetDescriptorPRO datasetDescriptorPRO,
            final SortedSet<TravailSolLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<TravailSolLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<TravailSolLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (TravailSolLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties, datasetDescriptorPRO);
            }

        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureTravailduSolDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public void setMesureTravailSolDAO(IMesureTravailduSolDAO<MesureTravailDuSol> mesureTravailSolDAO) {
        this.mesureTravailSolDAO = mesureTravailSolDAO;
    }

    private void buildMesure(TravailSolLineRecord line, VersionFile versionFile, SortedSet<TravailSolLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, ISessionPropertiesPRO sessionProperties, DatasetDescriptorPRO datasetDescriptorPRO)
            throws PersistenceException,
            InsertionDatabaseException {
        final LocalDate datedebut = line.getDatedebut();
        final String codedisp = line.getCodedispositif();
        String keydisp = Utils.createCodeFromString(sessionProperties.getDispositif().getCode());
        final String codeTrait = Utils.createCodeFromString(line.getCodetraitement());
        String codeunique = keydisp + "_" + codeTrait;
        DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByCodeUnique(codeunique).orElse(null);
        if (dbTraitement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_TRAITEMENT_TRAVAIL_NOT_FOUND_IN_DB), codeTrait, keydisp));
        }
        // final String codeTrait = line.getCodetraitement();
        final String codeParcelle = line.getCodeparcelle();
        final String nomplacette = line.getNomplacette();
        final String localisationprecise = Utils.createCodeFromString(line.getLocalisationprecide());
        final LocalDate datefin = line.getDatefin();
        final String intervention = line.getInterventionsol();
        final String profondeur = Utils.createCodeFromString(line.getProfondeurtravail());
        final String materielplantes1 = line.getMaterieltravail1();
        final String materielplantes2 = line.getMaterieltravail2();
        final String materielplantes3 = line.getMaterieltravail3();
        final String largeurtravail = line.getLargeurtravail();
        float largeurtra = Float.parseFloat(largeurtravail);
        final String culture = line.getCulture();
        final String codebbch = line.getCodebbch();
        final String precisionstade = line.getPrecisionstade();
        final String conditionhumidite = Utils.createCodeFromString(line.getConditionhumidite());
        final String conditiontemperature = Utils.createCodeFromString(line.getConditiontemperature());
        final String vitessevent = Utils.createCodeFromString(line.getVitessevent());
        final String observationqualite = line.getObservationqualite();
        final String nomobservation = line.getNomobservation();
        final String niveauatteint = Utils.createCodeFromString(line.getNiveauatteint());
        final String commentaire = line.getCommentaire();
        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String localisation = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(5));
        String profondeurtravail = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(8));
        String conditionair = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(16));
        String conditiontemp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(17));
        String vitesse = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(18));
        String travail = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(12));

        MesureTravailDuSol mesureTravailDuSol = getOrCreate(datedebut, codedisp, codeTrait, codeParcelle, nomplacette, culture, intervention, dbTraitement, datefin, materielplantes1, materielplantes2, materielplantes3, niveauatteint, codebbch, vitesse, nomobservation, versionFile, commentaire, precisionstade, observationqualite);

        ListeItineraire dbLP = listeItineraireDAO.getByKKey(localisation, localisationprecise).orElse(null);
        if (dbLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_LISTEITINERAIRE_LP_DB), localisation, localisationprecise));
        }

        ListeItineraire dbPT = listeItineraireDAO.getByKKey(profondeurtravail, profondeur).orElse(null);
        if (dbPT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_PT_LISTEITINERAIRE_LP_DB), profondeurtravail, profondeur));
        }

        ListeItineraire dbCA = listeItineraireDAO.getByKKey(conditionair, conditionhumidite).orElse(null);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_LISTEITINERAIRE_PR_DB), conditionair, conditionhumidite));
        }
        ListeItineraire dbCT = listeItineraireDAO.getByKKey(conditiontemp, conditiontemperature).orElse(null);
        if (dbCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_LISTEITINERAIRE_CT_DB), conditiontemp, conditiontemperature));
        }

        ListeItineraire dbVV = listeItineraireDAO.getByKKey(vitesse, vitessevent).orElse(null);
        if (dbVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_LISTEITINERAIRE_VV_DB), vitesse, vitessevent));
        }
        /* ListeItineraire dbNA = listeItineraireDAO.getByNKeys(travail, largeurtravail);
        if (dbNA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_ITK,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_LISTEITINERAIRE_NA_DB), travail, largeurtravail));
        }*/

        DatatypeVariableUnitePRO dbdvuLP = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, localisation).orElse(null);
        if (dbdvuLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, localisation));
        }

        RealNode dbdvuLPRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuLP.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuPT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, profondeurtravail).orElse(null);
        if (dbdvuPT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_PT_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, profondeurtravail));
        }

        RealNode dbdvuPTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuPT.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvCA = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditionair).orElse(null);
        if (dbdvCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_CA_NOT_FOUND_DVU_DB), cdatatype, conditionair));

        }
        RealNode dbdvCARealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvCA.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuCT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditiontemp).orElse(null);
        if (dbdvuCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_CONDITIONT_NOT_FOUND_DVU_DB), cdatatype, conditiontemp));

        }
        RealNode dbdvuCTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuCT.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuVV = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, vitesse).orElse(null);
        if (dbdvuVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_VENTV_NOT_FOUND_DVU_DB), cdatatype, vitesse));

        }
        RealNode dbdvuVVRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuVV.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvumTS = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, travail).orElse(null);
        if (dbdvumTS == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteTravailSol.BUNDLE_PATH_TRAVAILSOL,
                    ProcessRecordRecolteTravailSol.MSG_ERROR_RC_NIVEAU_NOT_FOUND_DVU_DB), cdatatype, travail));

        }
        RealNode dbdvumTSRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvumTS.getCode())).orElse(null);

        ValeurTravailDuSol valeurTravailDuSollp = new ValeurTravailDuSol(mesureTravailDuSol, dbLP, dbdvuLPRealNode);
        valeurTravailDuSollp.setRealNode(dbdvuLPRealNode);
        valeurTravailDuSollp.setListeItineraire(dbLP);
        valeurTravailDuSollp.setMesuretravaildusol(mesureTravailDuSol);
        mesureTravailDuSol.getValeurtravailsol().add(valeurTravailDuSollp);
        if (!errorsReport.hasErrors()) {
            mesureTravailSolDAO.saveOrUpdate(mesureTravailDuSol);
        }

        ValeurTravailDuSol valeurTravailDuSolpt = new ValeurTravailDuSol(mesureTravailDuSol, dbPT, dbdvuPTRealNode);
        valeurTravailDuSolpt.setRealNode(dbdvuPTRealNode);
        valeurTravailDuSolpt.setListeItineraire(dbPT);
        valeurTravailDuSolpt.setMesuretravaildusol(mesureTravailDuSol);
        mesureTravailDuSol.getValeurtravailsol().add(valeurTravailDuSolpt);
        if (!errorsReport.hasErrors()) {
            mesureTravailSolDAO.saveOrUpdate(mesureTravailDuSol);
        }

        ValeurTravailDuSol valeurTravailDuSolca = new ValeurTravailDuSol(mesureTravailDuSol, dbCA, dbdvCARealNode);
        valeurTravailDuSolca.setRealNode(dbdvCARealNode);
        valeurTravailDuSolca.setListeItineraire(dbCA);
        valeurTravailDuSolca.setMesuretravaildusol(mesureTravailDuSol);
        mesureTravailDuSol.getValeurtravailsol().add(valeurTravailDuSolca);
        if (!errorsReport.hasErrors()) {
            mesureTravailSolDAO.saveOrUpdate(mesureTravailDuSol);
        }

        ValeurTravailDuSol valeurTravailDuSolct = new ValeurTravailDuSol(mesureTravailDuSol, dbCT, dbdvuCTRealNode);
        valeurTravailDuSolct.setRealNode(dbdvuCTRealNode);
        valeurTravailDuSolct.setListeItineraire(dbCT);
        valeurTravailDuSolct.setMesuretravaildusol(mesureTravailDuSol);
        mesureTravailDuSol.getValeurtravailsol().add(valeurTravailDuSolct);
        if (!errorsReport.hasErrors()) {
            mesureTravailSolDAO.saveOrUpdate(mesureTravailDuSol);
        }

        ValeurTravailDuSol valeurTravailDuSolvv = new ValeurTravailDuSol(mesureTravailDuSol, dbVV, dbdvuVVRealNode);
        valeurTravailDuSolvv.setRealNode(dbdvuVVRealNode);
        valeurTravailDuSolvv.setListeItineraire(dbVV);
        valeurTravailDuSolvv.setMesuretravaildusol(mesureTravailDuSol);
        mesureTravailDuSol.getValeurtravailsol().add(valeurTravailDuSolvv);
        if (!errorsReport.hasErrors()) {
            mesureTravailSolDAO.saveOrUpdate(mesureTravailDuSol);
        }

        ValeurTravailDuSol valeurTravailDuSolna = new ValeurTravailDuSol(largeurtra, mesureTravailDuSol, dbdvumTSRealNode);
        valeurTravailDuSolna.setRealNode(dbdvumTSRealNode);
        valeurTravailDuSolna.setValeur(largeurtra);
        valeurTravailDuSolna.setMesuretravaildusol(mesureTravailDuSol);
        mesureTravailDuSol.getValeurtravailsol().add(valeurTravailDuSolna);
        if (!errorsReport.hasErrors()) {
            mesureTravailSolDAO.saveOrUpdate(mesureTravailDuSol);
        }
    }

    private MesureTravailDuSol getOrCreate(final LocalDate datedebut, final String codedisp, final String codeTrait, final String codeParcelle, final String nomplacette, final String culture, final String intervention, DescriptionTraitement dbTraitement, final LocalDate datefin, final String materielplantes1, final String materielplantes2, final String materielplantes3, final String niveauatteint, final String codebbch, String vitesse, final String nomobservation, VersionFile versionFile, final String commentaire, final String precisionstade, final String observationqualite) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(datedebut, format);
        String key = dateString + "_" + codedisp + "_" + codeTrait + "_" + codeParcelle + "_" + nomplacette + "_" + culture + "_" + intervention;
        MesureTravailDuSol mesureTravailDuSol = mesureTravailSolDAO.getByKeys(key).orElse(null);
        if (mesureTravailDuSol == null) {

            mesureTravailDuSol = new MesureTravailDuSol(codedisp, dbTraitement, nomplacette, nomplacette, datedebut, datefin, intervention, materielplantes1, materielplantes2, materielplantes3, niveauatteint, culture, codebbch, vitesse, nomobservation, nomobservation, versionFile, commentaire);
            mesureTravailDuSol.setCodedispositif(codedisp);
            mesureTravailDuSol.setDescriptionTraitement(dbTraitement);
            mesureTravailDuSol.setNomparcelle(codeParcelle);
            mesureTravailDuSol.setNomplacette(nomplacette);
            mesureTravailDuSol.setDatedebut(datedebut);
            mesureTravailDuSol.setDatefin(datefin);
            mesureTravailDuSol.setNiveauatteint(niveauatteint);
            mesureTravailDuSol.setCodebbch(codebbch);
            mesureTravailDuSol.setNomculture(culture);
            mesureTravailDuSol.setNomintervention(intervention);
            mesureTravailDuSol.setPrecisionstade(precisionstade);
            mesureTravailDuSol.setMaterieltravailsol1(materielplantes1);
            mesureTravailDuSol.setMaterieltravailsol2(materielplantes2);
            mesureTravailDuSol.setMaterieltravailsol3(materielplantes3);
            mesureTravailDuSol.setTypeobservation(observationqualite);
            mesureTravailDuSol.setNomobservation(nomobservation);
            mesureTravailDuSol.setNiveauatteint(niveauatteint);
            mesureTravailDuSol.setKeymesure(key);
            mesureTravailDuSol.setCommentaire(commentaire);
        }
        return mesureTravailDuSol;
    }

    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

}
