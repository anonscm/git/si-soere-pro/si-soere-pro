/**
 *
 */
package org.inra.ecoinfo.pro.refdata.parcelleelementaire;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.bloc.IBlocDAO;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.CodeParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.codeparcelleelt.ICodeParcelleElementaireDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<ParcelleElementaire> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    ICodeParcelleElementaireDAO codeParcelleElementaireDAO;
    IBlocDAO blocDAO;
    IParcelleElementaireDAO parcelleElementaireDAO;

    ConcurrentMap<String, String[]> blocPossibles = new ConcurrentHashMap<String, String[]>();
    private String[] codeParcelleElementairePossibles;
    private String[] dispositifsPossibles;
    private String[] systemeProjectionPossibles;

    /**
     * @return @throws PersistenceException
     */
    private String[] getCodeParcelleElementairePossibles() {
        List<CodeParcelleElementaire> lstCodeParcelleElementaires = codeParcelleElementaireDAO.getAll();
        String[] codeParcelleEltPossibles = new String[lstCodeParcelleElementaires.size()];
        int index = 0;
        for (CodeParcelleElementaire codeParcelleElementaire : lstCodeParcelleElementaires) {
            codeParcelleEltPossibles[index++] = codeParcelleElementaire.getCode();
        }
        return codeParcelleEltPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getSystemeProjectionPossibles(){
        List<SystemeProjection> lstSystemeProjections = systemeProjectionDAO.getAll();
        String[] systemeProjectionPossibles = new String[lstSystemeProjections.size() + 1];
        systemeProjectionPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (SystemeProjection systemeProjection : lstSystemeProjections) {
            systemeProjectionPossibles[index++] = systemeProjection.getNom();
        }
        return systemeProjectionPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);int indexDispLieu = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                String nomBloc = tokenizerValues.nextToken();
                String nomPE = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, nomLieu).orElseThrow(() -> new BusinessException("can't find dispositif"));
                Bloc bloc = blocDAO.getByNKey(nomBloc, dispositif).orElseThrow(() -> new BusinessException("can't find bloc"));

                ParcelleElementaire parcelleElementaire = parcelleElementaireDAO.getByNKey(nomPE, bloc)
                        .orElseThrow(() -> new BusinessException("can't find parcelle elementaire"));
                Geolocalisation geolocalisation = parcelleElementaire.getGeolocalisation();

                parcelleElementaireDAO.remove(parcelleElementaire);

                if (geolocalisation != null) {
                    geolocalisationDAO.remove(geolocalisation);
                }

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#getAllElements()
     */
    @Override
    protected List<ParcelleElementaire> getAllElements() throws BusinessException {
        return parcelleElementaireDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ParcelleElementaire parcelleElementaire) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String codeDipositifLieu = parcelleElementaire == null ? "" : parcelleElementaire.getDispositif().getNomDispositif_nomLieu();
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : codeDipositifLieu, dispositifsPossibles, null, true, false, true);

        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);

        String nomCodebloc = parcelleElementaire == null ? "" : parcelleElementaire.getBloc() == null ? "" : parcelleElementaire.getBloc().getNom() + " (" + parcelleElementaire.getBloc().getCode().getCode() + ")";
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : nomCodebloc, blocPossibles, null, true, false, true);

        refsColonne1.add(colonne2);

        colonne1.setRefs(refsColonne1);

        colonne2.setValue(nomCodebloc);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : parcelleElementaire.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : parcelleElementaire.getCodeParcelleElementaire().getCode(),codeParcelleElementairePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : parcelleElementaire.getGeolocalisation() == null ? Constantes.STRING_EMPTY : parcelleElementaire.getGeolocalisation().getSystemeProjection() == null
                        ? Constantes.STRING_EMPTY
                        : parcelleElementaire.getGeolocalisation().getSystemeProjection().getNom(), systemeProjectionPossibles, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : parcelleElementaire.getGeolocalisation() == null ? Constantes.STRING_EMPTY : parcelleElementaire.getGeolocalisation().getLatitude(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(parcelleElementaire == null ? Constantes.STRING_EMPTY : parcelleElementaire.getGeolocalisation() == null ? Constantes.STRING_EMPTY : parcelleElementaire.getGeolocalisation().getLongitude(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /**
     * @param dispositif
     * @param bloc
     * @param geolocalisation
     * @param nomPE
     * @param codeParcelleElementaire
     * @param dbParcelleElementaire
     * @param parcelleElementaire
     * @throws PersistenceException
     */
    private void createOrUpdate(Dispositif dispositif, Bloc bloc, Geolocalisation geolocalisation, String nomPE, CodeParcelleElementaire codeParcelleElementaire, ParcelleElementaire dbParcelleElementaire, ParcelleElementaire parcelleElementaire) throws BusinessException {
        try {
            if (dbParcelleElementaire == null) {
                parcelleElementaireDAO.saveOrUpdate(parcelleElementaire);
            } else {
                // si on met à jour la parcelle élémentaire en lui enlevant sa geolocalisation, alors supprimer cette ancienne geolocalisation de la base
                if (geolocalisation == null) {
                    Geolocalisation dbDispGeolocalisation = dbParcelleElementaire.getGeolocalisation();
                    if (dbDispGeolocalisation != null) {
                        geolocalisationDAO.remove(dbDispGeolocalisation);
                    }
                }

                dbParcelleElementaire.setDispositif(dispositif);
                dbParcelleElementaire.setBloc(bloc);
                dbParcelleElementaire.setGeolocalisation(geolocalisation);
                dbParcelleElementaire.setNom(nomPE);
                dbParcelleElementaire.setCodeParcelleElementaire(codeParcelleElementaire);

                parcelleElementaireDAO.saveOrUpdate(dbParcelleElementaire);
            }
        } catch (PersistenceException e) {
            throw new BusinessException("can't save parcelle elementaire");
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexDispLieu = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                int indexCodeBloc = tokenizerValues.currentTokenIndex();
                String nomCodeBloc = tokenizerValues.nextToken();
                String nomPE = tokenizerValues.nextToken();
                int indexCode = tokenizerValues.currentTokenIndex();
                String codePE = tokenizerValues.nextToken();

                int indexProj = tokenizerValues.currentTokenIndex();
                String systemeProjection = tokenizerValues.nextToken();
                String latitude = tokenizerValues.nextToken();
                String longitude = tokenizerValues.nextToken();
                long parcelle_id;
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = verifieLieu(nomLieu, line + 1, indexDispLieu + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line + 1, indexDispLieu + 1, errorsReport);
                CodeParcelleElementaire codeParcelleElementaire = verifieCodeParcelleElementaire(codePE, line + 1, indexCode + 1, errorsReport);
                Bloc bloc = verifieBloc(nomCodeBloc, codeDispositif, dispositif, line + 1, indexCodeBloc + 1, errorsReport);

                ParcelleElementaire dbParcelleElementaire = parcelleElementaireDAO.getByNKey(nomPE, bloc).orElse(null);

                // La géolocalisation n'est pas obligatoire
                Geolocalisation geolocalisation = null;
                if (codeParcelleElementaire != null && dispositif != null) {
                    //on traite la géolocalisation si pas d'erreur sur code parcelle, dispositif et donc pas sur dbParcelleElementaire
                    Geolocalisation ancienGeolocZone = dbParcelleElementaire != null ? dbParcelleElementaire.getGeolocalisation() : null;
                    geolocalisation = traiteGeolocalisation(systemeProjection, latitude, longitude, geolocalisation, dbParcelleElementaire, ancienGeolocZone, errorsReport, line + 1, indexProj + 1);
                }

                if (!errorsReport.hasErrors()) {

                    ParcelleElementaire parcelleElementaire = new ParcelleElementaire(dispositif, bloc, geolocalisation, nomPE, codeParcelleElementaire);
                    createOrUpdate(dispositif, bloc, geolocalisation, nomPE, codeParcelleElementaire, dbParcelleElementaire, parcelleElementaire);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }

        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }

        return dispositif;
    }

    /**
     * @param codePE
     * @param line
     * @param indexcode
     * @param errorsReport
     * @return
     */
    private CodeParcelleElementaire verifieCodeParcelleElementaire(String codePE, long line, int index, ErrorsReport errorsReport) {
        CodeParcelleElementaire codeParcelleElementaire = codeParcelleElementaireDAO.getByNKey(codePE).orElse(null);
        if (codeParcelleElementaire == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "CODEPARCELLEELT_NONDEFINI"), line, index, codePE));
        }

        return codeParcelleElementaire;
    }

    /**
     * @param nomCodeBloc
     * @param codeDispositif
     * @param dispositif
     * @param line
     * @param indexCodeBloc
     * @param errorsReport
     * @return
     */
    private Bloc verifieBloc(String nomCodeBloc, String codeDispositif, Dispositif dispositif, long line, int index, ErrorsReport errorsReport) throws BusinessException {
        // Le bloc n'est pas obligatoire
        String nomBloc = null;
        Bloc bloc = null;

        if (nomCodeBloc != null && !nomCodeBloc.isEmpty()) {
            int index3 = nomCodeBloc.indexOf('(');
            nomBloc = index3 != -1 ? nomCodeBloc.substring(0, index3).trim() : nomCodeBloc;

            if (nomBloc != null) {
                bloc = blocDAO.getByFindBloc(nomBloc, codeDispositif).orElse(null);

                if (bloc == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "BLOC_NONDEFINI"), line, index, nomBloc, codeDispositif));
                }
            }
        }

        return bloc;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder#verifLocalisationExiste(org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation, java.lang.Object, org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport)
     */

    /**
     *
     * @param dbGeolocalisation
     * @param dbParcelleElementaire
     * @param errorsReport
     * @return
     * @throws BusinessException
     */

    @Override
    public Geolocalisation verifLocalisationExiste(Geolocalisation dbGeolocalisation, ParcelleElementaire dbParcelleElementaire, ErrorsReport errorsReport) throws BusinessException {
        Geolocalisation geolocalisation = null;

        // vérifie si cette géolocalisation est déjà associée à une parcelle élémentaire et retrouve à laquelle
        ParcelleElementaire parcelleElementaireGeolocalise = parcelleElementaireDAO.getByGeolocalisation(dbGeolocalisation).orElse(null);

        if (parcelleElementaireGeolocalise != null) {
            if (dbParcelleElementaire != null) {
                // la géolocalisation de la parcelle élémentaire est à modifier en base mais la géolocalisation saisie
                // est déjà associée à une autre parcelle élémentaire en base. Dans ce cas erreur, deux parcelles élémentaires diférentes
                // ne pourront pas être géolocalisés au même endroit
                if (!dbParcelleElementaire.equals(parcelleElementaireGeolocalise)) {
                    String codeDispositifNomLieu = parcelleElementaireGeolocalise.getDispositif().getNomDispositif_nomLieu();
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "PARCELTGEOLOC_EXISTE"), parcelleElementaireGeolocalise.getCodeParcelleElementaire().getCode(), codeDispositifNomLieu));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                } else // la géolocalisation de la parcelle élémentaire sera à modifier en base
                {
                    geolocalisation = dbGeolocalisation;
                }
            }
        }

        return geolocalisation;
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param codeParcelleElementaireDAO the codeParcelleElementaireDAO to set
     */
    public void setCodeParcelleElementaireDAO(
            ICodeParcelleElementaireDAO codeParcelleElementaireDAO) {
        this.codeParcelleElementaireDAO = codeParcelleElementaireDAO;
    }

    /**
     * @param blocDAO the blocDAO to set
     */
    public void setBlocDAO(IBlocDAO blocDAO) {
        this.blocDAO = blocDAO;
    }

    /**
     * @param parcelleElementaireDAO the parcelleElementaireDAO to set
     */
    public void setParcelleElementaireDAO(
            IParcelleElementaireDAO parcelleElementaireDAO) {
        this.parcelleElementaireDAO = parcelleElementaireDAO;
    }

    private void initDispositifBlocParcellePossibles() {
        Map<String, List<String>> dispositifBloc = new HashMap<String, List<String>>();
        List<ParcelleElementaire> lstPE = parcelleElementaireDAO.getAll();
        lstPE.forEach((pelementaire) -> {
            String codedisp = pelementaire.getDispositif().getNomDispositif_nomLieu();
            String codeBloc = pelementaire.getBloc().getNom();
            dispositifBloc
                    .computeIfAbsent(codedisp, k -> new LinkedList<String>())
                    .add(codeBloc);
        });
        LinkedList<ConcurrentMap<String, String[]>> listOfMapOfValuesPossibles = new LinkedList();
        listOfMapOfValuesPossibles.add(blocPossibles);
        this.dispositifsPossibles = readMapOfValuesPossibles(dispositifBloc, listOfMapOfValuesPossibles, new LinkedList<String>());

    }

    @Override
    protected ModelGridMetadata<ParcelleElementaire> initModelGridMetadata() {
        initDispositifBlocParcellePossibles();
        codeParcelleElementairePossibles = getCodeParcelleElementairePossibles();
        systemeProjectionPossibles = getSystemeProjectionPossibles();
        return super.initModelGridMetadata();
    }
}
