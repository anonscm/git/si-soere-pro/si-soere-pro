/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementplante;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.cultures.Cultures;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.partieprelevee.PartiePrelevee;
import org.inra.ecoinfo.pro.refdata.placette.Placette;

/**
 *
 * @author adiankha
 */
public interface IPrelevementPlanteDAO extends IDAO<PrelevementPlante> {

    /**
     *
     * @return
     */
    List<PrelevementPlante> getAll();

    /**
     *
     * @param datePrelevement
     * @param descriptionTraitement
     * @param placette
     * @param bloc
     * @param cultures
     * @param partiePrelevee
     * @return
     */
    Optional<PrelevementPlante> getByNKey(
            LocalDate datePrelevement,
            DescriptionTraitement descriptionTraitement,
            Placette placette,
            Bloc bloc,
            Cultures cultures,
            PartiePrelevee partiePrelevee
    );

    /**
     *
     * @param codePlante
     * @return
     */
    Optional<PrelevementPlante> getByNKey(String codePlante);
}
