package org.inra.ecoinfo.pro.refdata.prelevementsol;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class JPAPrelevementSolDAO extends AbstractJPADAO<PrelevementSol> implements IPrelevementSolDAO {

    /**
     *
     * @return
     */
    @Override
    public List<PrelevementSol> getAll() {
        return getAllBy(PrelevementSol.class, PrelevementSol::getCodesol);
    }

    /**
     *
     * @param date_prelevement
     * @param limit_superieur
     * @param limit_inferieur
     * @param traitement
     * @param dispositif
     * @param bloc
     * @param pelementataire
     * @param placette
     * @return
     */
    @Override
    public Optional<PrelevementSol> getByNKey(LocalDate date_prelevement, int limit_superieur, int limit_inferieur, DescriptionTraitement traitement, Dispositif dispositif, Bloc bloc, ParcelleElementaire pelementataire, Placette placette) {

        CriteriaQuery<PrelevementSol> query = builder.createQuery(PrelevementSol.class);
        Root<PrelevementSol> prelevementSol = query.from(PrelevementSol.class);
        Join<PrelevementSol, Dispositif> disp = prelevementSol.join(PrelevementSol_.dispositif);
        Join<PrelevementSol, DescriptionTraitement> descTra = prelevementSol.join(PrelevementSol_.traitement);
        Join<PrelevementSol, Bloc> blo = prelevementSol.join(PrelevementSol_.bloc);
        Join<PrelevementSol, ParcelleElementaire> pe = prelevementSol.join(PrelevementSol_.pelementaire);
        Join<PrelevementSol, Placette> pl = prelevementSol.join(PrelevementSol_.placette);
        query
                .select(prelevementSol)
                .where(
                        builder.equal(prelevementSol.get(PrelevementSol_.date_prelevement), date_prelevement),
                        builder.equal(prelevementSol.get(PrelevementSol_.limit_inferieur), limit_inferieur),
                        builder.equal(prelevementSol.get(PrelevementSol_.limit_superieur), limit_superieur),
                        builder.equal(disp, dispositif),
                        builder.equal(descTra, traitement),
                        builder.equal(blo, bloc),
                        builder.equal(pe, pelementataire),
                        builder.equal(pl, placette)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codePrelevement
     * @return
     */
    @Override
    public Optional<PrelevementSol> getByNKey(String codePrelevement) {
        CriteriaQuery<PrelevementSol> query = builder.createQuery(PrelevementSol.class);
        Root<PrelevementSol> prelevementSol = query.from(PrelevementSol.class);
        query
                .select(prelevementSol)
                .where(
                        builder.equal(prelevementSol.get(PrelevementSol_.codesol), Utils.createCodeFromString(codePrelevement))
                );
        return getOptional(query);
    }
}
