package org.inra.ecoinfo.pro.refdata.annees;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IAnneesDAO extends IDAO<Annees> {

    /**
     *
     * @return
     */
    List<Annees> getAll();

    /**
     *
     * @param valeur
     * @return
     */
    Optional<Annees> getByNKey(String valeur);

}
