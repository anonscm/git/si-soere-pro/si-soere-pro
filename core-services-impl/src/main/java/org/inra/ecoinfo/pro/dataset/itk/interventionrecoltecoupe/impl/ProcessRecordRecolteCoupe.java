/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import static org.inra.ecoinfo.AbstractJPADAO.LOGGER;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.IMesureRecolteCoupeDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionsemisplantation.impl.ProcessRecordSemisPlantation;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.IListeItineraireDAO;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordRecolteCoupe extends AbstractProcessRecord {

    protected static final String BUNDLE_PATH_RECOLTE_COUPE = "org.inra.ecoinfo.pro.dataset.itk.messages";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_NA_DB = "MSG_ERROR_RC_LISTEITINERAIRE_NA_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_VV_DB = "MSG_ERROR_RC_LISTEITINERAIRE_VV_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_PR_DB = "MSG_ERROR_RC_LISTEITINERAIRE_PR_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_LP_DB = "MSG_ERROR_RC_LISTEITINERAIRE_LP_DB";
    private static final String MSG_ERROR_RC_LISTEITINERAIRE_CT_DB = "MSG_ERROR_RC_LISTEITINERAIRE_CT_DB";
    private static final String MSG_ERROR_RC_LOCALISATION_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_LOCALISATION_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_CA_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_CA_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_CONDITIONT_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_CONDITIONT_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_VENTV_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_VENTV_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_RC_NIVEAU_NOT_FOUND_DVU_DB = "MSG_ERROR_RC_NIVEAU_NOT_FOUND_DVU_DB";
    private static final String MSG_ERROR_TRAITEMENT_RECOLTECOUPE_NOT_FOUND_IN_DB = "MSG_ERROR_TRAITEMENT_RECOLTECOUPE_NOT_FOUND_IN_DB";
    IMesureRecolteCoupeDAO<MesureRecolteCoupe> mesureRecolteCoupeDAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    IListeItineraireDAO listeItineraireDAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;

    public ProcessRecordRecolteCoupe() {
        super();
    }

    private long readLines(final CSVParser parser, final Map<LocalDate, List<RecolteCoupeLineRecord>> lines, long lineCount,
            ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate datedebut = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codedispositif = cleanerValues.nextToken();
            final String codetraitement = cleanerValues.nextToken();
            final String nomparcelle = cleanerValues.nextToken();
            final String nomplacette = cleanerValues.nextToken();
            final String localisationprecide = cleanerValues.nextToken();
            final LocalDate datefin = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String culture = cleanerValues.nextToken();
            final String codebbch = cleanerValues.nextToken();
            final String precisionstade = cleanerValues.nextToken();
            final String interventionrecolte = cleanerValues.nextToken();
            final String materielplante1 = cleanerValues.nextToken();
            final String materielplante2 = cleanerValues.nextToken();
            final String materielplante3 = cleanerValues.nextToken();
            final String conditionhumidite = cleanerValues.nextToken();
            final String conditiontemperature = cleanerValues.nextToken();
            final String vitessevent = cleanerValues.nextToken();
            final String observationqualite = cleanerValues.nextToken();
            final String nomobservation = cleanerValues.nextToken();
            final String niveauatteint = cleanerValues.nextToken();
            final String commentaire = cleanerValues.nextToken();

            final RecolteCoupeLineRecord line = new RecolteCoupeLineRecord(lineCount, datedebut, codedispositif, codetraitement, nomparcelle, nomplacette, localisationprecide, datefin, culture, codebbch, precisionstade, interventionrecolte, materielplante1, materielplante2, materielplante3, conditionhumidite, conditiontemperature, vitessevent, observationqualite, nomobservation, niveauatteint, commentaire);
            try {
                if (!lines.containsKey(datedebut)) {
                    lines.put(datedebut, new LinkedList<>());
                }
                lines.get(datedebut).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        datedebut, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    private void recordErrors(final org.inra.ecoinfo.pro.utils.ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties,
            String fileEncoding, DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<RecolteCoupeLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<RecolteCoupeLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, datasetDescriptorPRO, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordSemisPlantation.class).error(ex.getMessage(), ex);
        }
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<RecolteCoupeLineRecord>> lines, DatasetDescriptorPRO datasetDescriptorPRO,
            final SortedSet<RecolteCoupeLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<RecolteCoupeLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<RecolteCoupeLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (RecolteCoupeLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties, datasetDescriptorPRO);
            }

        }
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesureRecolteCoupeDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public void setMesureRecolteCoupeDAO(IMesureRecolteCoupeDAO<MesureRecolteCoupe> mesureRecolteCoupeDAO) {
        this.mesureRecolteCoupeDAO = mesureRecolteCoupeDAO;
    }

    private void buildMesure(RecolteCoupeLineRecord line, VersionFile versionFile, SortedSet<RecolteCoupeLineRecord> ligneEnErreur,
            ErrorsReport errorsReport, ISessionPropertiesPRO sessionProperties, DatasetDescriptorPRO datasetDescriptorPRO)
            throws PersistenceException,
            InsertionDatabaseException {
        final LocalDate datedebut = line.getDatedebut();
        final String codedisp = line.getCodedispositif();
        String keydisp = Utils.createCodeFromString(sessionProperties.getDispositif().getCode());
        final String codeTrait = Utils.createCodeFromString(line.getCodetraitement());
        String codeunique = keydisp + "_" + codeTrait;
        DescriptionTraitement dbTraitement = descriptionTraitementDAO.getByCodeUnique(codeunique).orElse(null);
        if (dbTraitement == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_TRAITEMENT_RECOLTECOUPE_NOT_FOUND_IN_DB), codeTrait, keydisp));
        }
        final String codeParcelle = line.getCodeparcelle();
        final String nomplacette = line.getNomplacette();
        final String localisationprecise = Utils.createCodeFromString(line.getLocalisationprecise());
        final LocalDate datefin = line.getDatefin();
        final String culture = line.getCulture();
        final String codebbch = line.getCodebbch();
        final String precisionstade = line.getPrecisionstade();
        final String interventionrecolte = line.getInterventionrecolte();
        final String materielplantes1 = line.getMaterielplante1();
        final String materielplantes2 = line.getMaterielplante2();
        final String materielplantes3 = line.getMaterielplante3();
        final String conditionhumidite = Utils.createCodeFromString(line.getConditionhumidite());
        final String conditiontemperature = Utils.createCodeFromString(line.getConditiontemperature());
        final String vitessevent = Utils.createCodeFromString(line.getVitessevent());
        final String observationqualite = line.getObservationqualite();
        final String nomobservation = line.getNomobservation();
        final String niveauatteint = Utils.createCodeFromString(line.getNiveauatteint());
        final String commentaire = line.getCommentaire();
        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);

        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String localisation = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(5));
        String conditionair = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(14));
        String conditiontemp = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(15));
        String vitesse = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(16));
        // String niveau = Utils.createCodeFromString(datasetDescriptorPRO.getColumnName(19));

        MesureRecolteCoupe mesureRecolteCoupe = getOrCreate(datedebut, codedisp, codeTrait, codeParcelle, nomplacette, culture, interventionrecolte, datefin, dbTraitement, codebbch, precisionstade, materielplantes1, materielplantes2, materielplantes3, nomobservation, niveauatteint, commentaire, versionFile, observationqualite);

        ListeItineraire dbLP = listeItineraireDAO.getByKKey(localisation, localisationprecise).orElse(null);
        if (dbLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_LISTEITINERAIRE_LP_DB), localisation, localisationprecise));
        }

        ListeItineraire dbCA = listeItineraireDAO.getByKKey(conditionair, conditionhumidite).orElse(null);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_LISTEITINERAIRE_PR_DB), conditionair, conditionhumidite));
        }
        ListeItineraire dbCT = listeItineraireDAO.getByKKey(conditiontemp, conditiontemperature).orElse(null);
        if (dbCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_LISTEITINERAIRE_CT_DB), conditiontemp, conditiontemperature));
        }

        ListeItineraire dbVV = listeItineraireDAO.getByKKey(vitesse, vitessevent).orElse(null);
        if (dbCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_LISTEITINERAIRE_VV_DB), vitesse, vitessevent));
        }
//        ListeItineraire dbNA = listeItineraireDAO.getByKKey(niveau, niveauatteint);
//        if (dbNA == null) {
//            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_ITK,
//                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_LISTEITINERAIRE_NA_DB), niveau, niveauatteint));
//        }

        DatatypeVariableUnitePRO dbdvuLP = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, localisation).orElse(null);
        if (dbdvuLP == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_LOCALISATION_NOT_FOUND_DVU_DB), cdatatype, localisation));
        }
        RealNode dbdvuLPRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuLP.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvCA = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditionair).orElse(null);
        if (dbdvCA == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_CA_NOT_FOUND_DVU_DB), cdatatype, conditionair));

        }
        RealNode dbdvCARealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvCA.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuCT = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, conditiontemp).orElse(null);
        if (dbdvuCT == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_CONDITIONT_NOT_FOUND_DVU_DB), cdatatype, conditiontemp));

        }
        RealNode dbdvuCTRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuCT.getCode())).orElse(null);

        DatatypeVariableUnitePRO dbdvuVV = dataTypeVariableUnitePRODAO.getSemisKey(cdatatype, vitesse).orElse(null);
        if (dbdvuVV == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_RECOLTE_COUPE,
                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_VENTV_NOT_FOUND_DVU_DB), cdatatype, vitesse));

        }
//        DatatypeVariableUnitePRO dbdvumNA = dataTypeVariableUnitePRODAO.getSemisKey(datatype, niveau);
//        if (dbdvumNA == null) {
//            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordRecolteCoupe.BUNDLE_PATH_ITK,
//                    ProcessRecordRecolteCoupe.MSG_ERROR_RC_NIVEAU_NOT_FOUND_DVU_DB), cdatatype, niveau));
//
//        }
        RealNode dbdvuVVRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvuVV.getCode())).orElse(null);

        ValeurRecolteCoupe valeurRecolteCoupelp = new ValeurRecolteCoupe(mesureRecolteCoupe, dbLP, dbdvuLPRealNode);
        valeurRecolteCoupelp.setRealNode(dbdvuLPRealNode);
        valeurRecolteCoupelp.setListeItineraire(dbLP);
        valeurRecolteCoupelp.setMesureinterventionrecolte(mesureRecolteCoupe);
        mesureRecolteCoupe.getValeurrecolte().add(valeurRecolteCoupelp);
        if (!errorsReport.hasErrors()) {
            mesureRecolteCoupeDAO.saveOrUpdate(mesureRecolteCoupe);
        }

        ValeurRecolteCoupe valeurRecolteCoupeca = new ValeurRecolteCoupe(mesureRecolteCoupe, dbCA, dbdvCARealNode);
        valeurRecolteCoupeca.setRealNode(dbdvCARealNode);
        valeurRecolteCoupeca.setListeItineraire(dbCA);
        valeurRecolteCoupeca.setMesureinterventionrecolte(mesureRecolteCoupe);
        mesureRecolteCoupe.getValeurrecolte().add(valeurRecolteCoupeca);
        if (!errorsReport.hasErrors()) {
            mesureRecolteCoupeDAO.saveOrUpdate(mesureRecolteCoupe);
        }

        ValeurRecolteCoupe valeurRecolteCoupect = new ValeurRecolteCoupe(mesureRecolteCoupe, dbCT, dbdvuCTRealNode);
        valeurRecolteCoupect.setRealNode(dbdvuCTRealNode);
        valeurRecolteCoupect.setListeItineraire(dbCT);
        valeurRecolteCoupect.setMesureinterventionrecolte(mesureRecolteCoupe);
        mesureRecolteCoupe.getValeurrecolte().add(valeurRecolteCoupect);
        if (!errorsReport.hasErrors()) {
            mesureRecolteCoupeDAO.saveOrUpdate(mesureRecolteCoupe);
        }

        ValeurRecolteCoupe valeurRecolteCoupevv = new ValeurRecolteCoupe(mesureRecolteCoupe, dbVV, dbdvuVVRealNode);
        valeurRecolteCoupevv.setRealNode(dbdvuVVRealNode);
        valeurRecolteCoupevv.setListeItineraire(dbVV);
        valeurRecolteCoupevv.setMesureinterventionrecolte(mesureRecolteCoupe);
        mesureRecolteCoupe.getValeurrecolte().add(valeurRecolteCoupevv);
        if (!errorsReport.hasErrors()) {
            mesureRecolteCoupeDAO.saveOrUpdate(mesureRecolteCoupe);
        }

//        ValeurRecolteCoupe valeurRecolteCoupena = new ValeurRecolteCoupe(mesureRecolteCoupe, dbNA, dbdvumNA);
//        valeurRecolteCoupena.setDatatypeVariableUnitePRO(dbdvumNA);
//        valeurRecolteCoupena.setListeItineraires(dbNA);
//        valeurRecolteCoupena.setMesureinterventionrecolte(mesureRecolteCoupe);
//        mesureRecolteCoupe.getValeurrecolte().add(valeurRecolteCoupena);
//        if(!errorsReport.hasErrors()){
//        mesureRecolteCoupeDAO.saveOrUpdate(mesureRecolteCoupe);
//        }
    }

    private MesureRecolteCoupe getOrCreate(final LocalDate datedebut, final String codedisp, final String codeTrait, final String codeParcelle, final String nomplacette, final String culture, final String interventionrecolte, final LocalDate datefin, DescriptionTraitement dbTraitement, final String codebbch, final String precisionstade, final String materielplantes1, final String materielplantes2, final String materielplantes3, final String nomobservation, String niveauatteint, final String commentaire, VersionFile versionFile, final String observationqualite) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(datedebut, format);
        String key = dateString + "_" + codedisp + "_" + codeTrait + "_" + codeParcelle + "_" + nomplacette + "_" + culture + "_" + interventionrecolte;
        MesureRecolteCoupe mesureRecolteCoupe = mesureRecolteCoupeDAO.getByKeys(key).orElse(null);
        if (mesureRecolteCoupe == null) {
            mesureRecolteCoupe = new MesureRecolteCoupe(datedebut, datefin, codedisp, dbTraitement, nomplacette, nomplacette, culture, codebbch, precisionstade, materielplantes1, materielplantes2, materielplantes3, interventionrecolte, nomobservation, nomobservation, niveauatteint, commentaire, versionFile);
            mesureRecolteCoupe.setCodedispositif(codedisp);
            mesureRecolteCoupe.setDescriptionTraitement(dbTraitement);
            mesureRecolteCoupe.setNomparcelle(codeParcelle);
            mesureRecolteCoupe.setNomplacette(nomplacette);
            mesureRecolteCoupe.setDatedebut(datedebut);
            mesureRecolteCoupe.setDatefin(datefin);
            mesureRecolteCoupe.setCodebbch(codebbch);
            mesureRecolteCoupe.setNomculture(culture);
            mesureRecolteCoupe.setNomintervention(interventionrecolte);
            mesureRecolteCoupe.setStadeprecision(precisionstade);
            mesureRecolteCoupe.setMaterielplante1(materielplantes1);
            mesureRecolteCoupe.setMaterielplante2(materielplantes2);
            mesureRecolteCoupe.setMaterielplante3(materielplantes3);
            mesureRecolteCoupe.setTypeobservation(observationqualite);
            mesureRecolteCoupe.setNomobservation(nomobservation);
            mesureRecolteCoupe.setNiveauatteint(niveauatteint);
            mesureRecolteCoupe.setKeymesure(key);
            mesureRecolteCoupe.setCommentaire(commentaire);
        }
        return mesureRecolteCoupe;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

}
