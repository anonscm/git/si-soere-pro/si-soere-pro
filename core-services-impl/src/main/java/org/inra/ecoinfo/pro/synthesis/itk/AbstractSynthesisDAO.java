/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.itk;

import java.time.LocalDate;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK;
import org.inra.ecoinfo.pro.dataset.itk.entity.MesureITK_;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK;
import org.inra.ecoinfo.pro.dataset.itk.entity.ValeurITK_;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire;
import org.inra.ecoinfo.pro.refdata.listeitineraire.ListeItineraire_;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author ptcherniati
 */
public abstract class AbstractSynthesisDAO<SV extends AbstractProSynthesisValue, SD extends GenericSynthesisDatatype,V extends ValeurITK, M extends MesureITK> extends AbstractSynthesis<SV, SD> {

    @Override
    public Stream<SV> getSynthesisValue() {
        CriteriaQuery<SV> query = builder.createQuery(getSynthesisValueClass());
        Root<V> v = query.from(getValueClass());
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> varRn2 = node.join(NodeDataSet_.realNode);

        Join<V, M> m = v.join(getMesureAttribute());
        Join<V, ListeItineraire> listeItineraire = v.join(ValeurITK_.listeItineraire);
        Join<V, RealNode> varRn = v.join(ValeurITK_.realNode);
        final Join<RealNode, RealNode> datatypeRn = varRn.join(RealNode_.parent);
        final Join<RealNode, RealNode> themeRn = datatypeRn.join(RealNode_.parent);
//        Join<RealNode, RealNode> parcelleRn = datatypeRn.join(RealNode_.parent);
//        Join<RealNode, RealNode> dispositifRn = parcelleRn.join(RealNode_.parent);
        Join<RealNode, RealNode> dispositifRn = themeRn.join(RealNode_.parent);
        query.distinct(true);
        Path<Float> valuePath = null;
        
        Predicate whereValueExpression = null;
        if (isValue()) {
            valuePath = v.get(ValeurITK_.valeur);
            whereValueExpression = builder.and(
                    builder.isNull(valuePath),
                    builder.gt(valuePath, -9999)
            );
        }
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = m.get(MesureITK_.datedebut);
        final Path<String> dispositifPath = dispositifRn.get(RealNode_.path);
        Path<String> listeItineraireValuePath = listeItineraire.get(ListeItineraire_.kvaleur);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query.select(
                builder.construct(
                        getSynthesisValueClass(),
                        isValue()
                                ? new Selection[]{
                            dateMesure.alias("date"),
                            dispositifPath.alias("dispositif"),
                            variableCode.alias("variable"),
                            listeItineraireValuePath.alias("qualitative"),
                            builder.avg(valuePath).alias("valeur"),
                            idNode.alias("node")
                        }
                        : new Selection[]{
                            dateMesure.alias("date"),
                            dispositifPath.alias("dispositif"),
                            variableCode.alias("variable"),
                            listeItineraireValuePath.alias("qualitative"),
                            idNode.alias("node")
                        }
                )
        )
                .where(
                        builder.equal(varRn2, varRn),
                        builder.or(
                                whereValueExpression == null
                                ? builder.isNull(listeItineraire).not()
                                : whereValueExpression
                        )
                )
                .groupBy(
                        dateMesure,
                        dispositifPath,
                        variableCode,
                        listeItineraireValuePath,
                        idNode
                )
                .orderBy(
                        builder.asc(dispositifPath),
                        builder.asc(variableCode),
                        builder.asc(listeItineraireValuePath),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

    abstract Boolean isValue();

    abstract Class<V> getValueClass();

    abstract Class<SV> getSynthesisValueClass();

    abstract protected SingularAttribute<V, M> getMesureAttribute();

}
