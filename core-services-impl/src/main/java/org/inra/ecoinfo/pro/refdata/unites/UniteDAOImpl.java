package org.inra.ecoinfo.pro.refdata.unites;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class UniteDAOImpl extends AbstractJPADAO<Unites> implements IUniteDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Unites> getAll() {
        return getAll(Unites.class);
    }

    /**
     *
     * @param unite
     * @return
     */
    @Override
    public Optional<Unites> getByunite(String unite) {
        CriteriaQuery<Unites> query = builder.createQuery(Unites.class);
        Root<Unites> unites = query.from(Unites.class);
        query
                .select(unites)
                .where(
                        builder.equal(unites.get(Unites_.unite), unite)
                );
        return getOptional(query);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<Unites> getByNKey(String code) {
        CriteriaQuery<Unites> query = builder.createQuery(Unites.class);
        Root<Unites> unites = query.from(Unites.class);
        query
                .select(unites)
                .where(
                        builder.equal(unites.get(Unites_.code), code)
                );
        return getOptional(query);
    }
}
