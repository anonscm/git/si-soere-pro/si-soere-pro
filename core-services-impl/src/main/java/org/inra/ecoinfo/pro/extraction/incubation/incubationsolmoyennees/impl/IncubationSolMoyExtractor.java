/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolmoyennees.impl;

import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationExtractor;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationParameterVO;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolMoyExtractor extends AbstractIncubationExtractor<MesureIncubationSolMoy> {

    /**
     *
     */
    public static final String INCUBATION_SOL_MOY = "incubation_sol_moy";

    /**
     *
     */
    protected static final String MAP_INDEX_INCUBATION_SOL_MOY = "incubationsolmoy";

    /**
     *
     */
    protected static final String MAP_INDEX_0 = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return INCUBATION_SOL_MOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return IncubationParameterVO.INCUBATIONSOLMOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_INCUBATION_SOL_MOY;
    }
}
