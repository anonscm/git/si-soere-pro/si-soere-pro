/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.biomasse;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.biomasseprelevee.BiomassePrelevee;

/**
 *
 * @author adiankha
 */
public interface IBiomasseDAO extends IDAO<BiomassePrelevee> {

    /**
     *
     * @return
     */
    List<BiomassePrelevee> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<BiomassePrelevee> getByName(String nom);

    /**
     *
     * @param code
     * @return
     */
    Optional<BiomassePrelevee> getByKey(String code);

}
