/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.impl;


import java.time.LocalDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 *
 * @author adiankha
 */
public class SolLineRecord  implements Comparable<SolLineRecord>{
    
    LocalDate dateprelevement;
    String codeechantillon;
    String nomlabo;
    int numerolabo;
    int numerorepet ;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
     Long originalLineNumber;

    /**
     *
     */
    public SolLineRecord() {
        super();
    }

    /**
     *
     * @param dateprelevement
     * @param originalLineNumber
     * @param codeechan
     * @param nomlabo
     * @param numerolabo
     * @param numerorepet
     * @param codevariable
     * @param valeurvariable
     * @param statutvaleur
     * @param codeunite
     * @param codemethode
     * @param codehumidite
     */
    public SolLineRecord(LocalDate dateprelevement, Long originalLineNumber,String codeechan,String nomlabo,int numerolabo,int numerorepet,String codevariable,
    float valeurvariable,
    String statutvaleur,
      String codeunite,
    String codemethode,
    String codehumidite) {
        this.dateprelevement = dateprelevement;
        this.originalLineNumber = originalLineNumber;
        this.nomlabo = nomlabo;
        this.numerolabo = numerolabo;
        this.numerorepet = numerorepet;
        this.codeechantillon = codeechan;
        this.codehumidite = codehumidite;
       this.codemethode = codemethode;
       this.codevariable = codevariable;
       this.statutvaleur = statutvaleur;
       this.codeunite = codeunite;
       this.valeurvariable = valeurvariable;
    }
    
    
    

    @Override
    public int compareTo(SolLineRecord o) {
         int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }
    
    /**
     *
     * @param line
     */
    public void copy(final SolLineRecord line) {
        this.dateprelevement = line.getDateprelevement();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.nomlabo = line.getNomlabo();
        this.numerolabo = line.getNumerolabo();
        this.numerorepet = line.getNumerorepet();
        this.codeechantillon = line.getCodeechantillon();
        this.codehumidite = line.getCodehumidite();
        this.codemethode = line.getCodemethode();
        this.codevariable = line.getCodevariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codeunite = line.getCodeunite();
        this.valeurvariable = line.getValeurvariable();
    }

    /**
     *
     * @return
     */
    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    /**
     *
     * @param dateprelevement
     */
    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }


    @Override
    public boolean equals(Object obj) {
         return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public int getNumerorepet() {
        return numerorepet;
    }

    /**
     *
     * @param numerorepet
     */
    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    /**
     *
     * @return
     */
    public int getNumerolabo() {
        return numerolabo;
    }

    /**
     *
     * @param numerolabo
     */
    public void setNumerolabo(int numerolabo) {
        this.numerolabo = numerolabo;
    }

    /**
     *
     * @return
     */
    public String getCodeechantillon() {
        return codeechantillon;
    }

    /**
     *
     * @param codeechantillon
     */
    public void setCodeechantillon(String codeechantillon) {
        this.codeechantillon = codeechantillon;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeurvariable() {
        return valeurvariable;
    }

    /**
     *
     * @param valeurvariable
     */
    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }
    
    
    
}
