/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolprobrut.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro_;
import org.inra.ecoinfo.pro.extraction.incubation.IIncubationDatatypeManager;
import org.inra.ecoinfo.pro.extraction.incubation.jpa.AbstractJPAIncubationDAO;
import org.inra.ecoinfo.pro.synthesis.incubationsolpro.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author vjkoyao
 */
public class JPAIncubationSolProDAO extends AbstractJPAIncubationDAO<MesureIncubationSolPro, ValeurIncubationSolPro>{

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurIncubationSolPro> getValeurIncubationClass() {
        return ValeurIncubationSolPro.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureIncubationSolPro> getMesureIncubationClass() {
        return MesureIncubationSolPro.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurIncubationSolPro, MesureIncubationSolPro> getMesureAttribute() {
        return ValeurIncubationSolPro_.mesureIncubationSolPro;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IIncubationDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_PRO;
    }
}
