/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.recoltecoupe.impl;

import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.ValeurRecolteCoupe;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKExtractor;

/**
 *
 * @author adiankha
 */
public class RecolteCoupeExtractor extends AbstractITKExtractor<MesureRecolteCoupe, ValeurRecolteCoupe> {

    /**
     *
     */
    public static final String RECOLTE_COUPE = "recolte_coupe";

    /**
     *
     */
    protected static final String MAP_INDEX_RECOLTE_COUPE = "recoltecoupe";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return RECOLTE_COUPE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return RECOLTE_COUPE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_RECOLTE_COUPE;
    }

}
