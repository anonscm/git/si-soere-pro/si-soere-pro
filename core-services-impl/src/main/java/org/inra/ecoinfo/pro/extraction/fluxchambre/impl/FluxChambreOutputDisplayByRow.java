/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre.impl;

import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vjkoyao
 */
public class FluxChambreOutputDisplayByRow extends FluxChambreOutputBuildersResolver {

    IOutputBuilder fluxChambreBuildOutputByRow;

    public FluxChambreOutputDisplayByRow() {
    }
  
    
    @Override
    public RObuildZipOutputStream buildOutput(final IParameter parameters) {
        try {
            super.setFluxChambreOutputBuilder(this.fluxChambreBuildOutputByRow);
            return super.buildOutput(parameters);
        } catch (BusinessException e) {
            LoggerFactory.getLogger(getClass()).error("error while building output", e);
            return null;
        }
    }

    public void setFluxChambreBuildOutputByRow(IOutputBuilder fluxChambreBuildOutputByRow) {
        this.fluxChambreBuildOutputByRow = fluxChambreBuildOutputByRow;
    }
    
    
    
}
