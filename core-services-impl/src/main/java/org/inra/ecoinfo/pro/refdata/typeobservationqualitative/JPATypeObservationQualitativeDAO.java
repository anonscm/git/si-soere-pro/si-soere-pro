/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeobservationqualitative;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author vjkoyao
 */
public class JPATypeObservationQualitativeDAO extends AbstractJPADAO<TypeObservationQualitative> implements ITypeObservationQualitativeDAO{

    /**
     *
     * @return
     */
    @Override
    public List<TypeObservationQualitative> getAll()  {
       return getAll(TypeObservationQualitative.class);
    }

    /**
     *
     * @param type_observation_nom
     * @return
     */
    @Override
    public Optional<TypeObservationQualitative> getByNKey(String type_observation_nom) {
        CriteriaQuery<TypeObservationQualitative> query = builder.createQuery(TypeObservationQualitative.class);
        Root<TypeObservationQualitative> typeObservationQualitative = query.from(TypeObservationQualitative.class);
        query
                .select(typeObservationQualitative)
                .where(
                        builder.equal(typeObservationQualitative.get(TypeObservationQualitative_.type_observation_nom), type_observation_nom)
                );
        return getOptional(query);
    }
    
}
