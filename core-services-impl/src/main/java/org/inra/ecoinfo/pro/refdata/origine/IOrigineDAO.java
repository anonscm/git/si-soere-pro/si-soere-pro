/*
 *
 */
package org.inra.ecoinfo.pro.refdata.origine;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

// TODO: Auto-generated Javadoc
/**
 * The Interface IOrigineDAO.
 */
public interface IOrigineDAO extends IDAO<Origines> {

    /**
     *
     * @return
     */
    List<Origines> getAll();

    /**
     *
     * @param origine_nom
     * @return
     */
    Optional<Origines> getByNKey(String origine_nom);

}
