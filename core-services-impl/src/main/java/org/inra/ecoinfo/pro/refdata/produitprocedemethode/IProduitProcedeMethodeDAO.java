package org.inra.ecoinfo.pro.refdata.produitprocedemethode;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.methodeprocess.MethodeProcess;
import org.inra.ecoinfo.pro.refdata.produit.Produits;

/**
 *
 * @author ptcherniati
 */
public interface IProduitProcedeMethodeDAO extends IDAO<ProduitProcedeMethode> {

    /**
     *
     * @return
     */
    List<ProduitProcedeMethode> getAll();

    /**
     *
     * @param prod
     * @param mprocess
     * @param duree
     * @return
     */
    Optional<ProduitProcedeMethode> getByNKey(Produits prod, MethodeProcess mprocess, int duree);

}
