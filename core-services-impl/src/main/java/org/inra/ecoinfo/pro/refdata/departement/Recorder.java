/**
 * 
 */
package org.inra.ecoinfo.pro.refdata.departement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.region.IRegionDAO;
import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Departement> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    IRegionDAO regionDAO;
    IDepartementDAO departementDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
    	ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try 
        {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) 
            {
            	line++;
            	TokenizerValues tokenizerValues = new TokenizerValues(values);
                int indexRegion = tokenizerValues.currentTokenIndex();
                String nomRegion = tokenizerValues.nextToken();
                String nomDepartement = tokenizerValues.nextToken();
                int indexNoDepartement = tokenizerValues.currentTokenIndex();
                String noDepartement = tokenizerValues.nextToken();
                
                // (nom département est obligatoire.)
                //Pour les pays qui n'ont pas de département, c'est le nom de région qui est utilisé.
                if (nomDepartement == null || nomDepartement.trim().isEmpty()) 
                {
                    nomDepartement = nomRegion;
                }
                
                String code = Utils.createCodeFromString(nomDepartement);
                
                Region region = verifieRegion(nomRegion, line+1, indexRegion+1, errorsReport);
                verifieNoDepartement(noDepartement, line+1, indexNoDepartement+1, errorsReport);
                
                //numéro département de "01" à "09" si écrit "1" à "9" dans le fichier
            	if (noDepartement != null && noDepartement.length() < 2 && !noDepartement.equals("0")) {
                    noDepartement = "0" + noDepartement;
                }
                
                Departement dbDepartement = departementDAO.getByNKey(code).orElse(null);
                if (dbDepartement == null)
                {
            		verifieDoublonNoDepartement(noDepartement, line+1, indexNoDepartement+1, errorsReport);
                }
                
                if(!errorsReport.hasErrors())
                {
                	if (dbDepartement == null)
                    {
                		Departement departement = new Departement(nomDepartement, code, noDepartement, region);
                		departementDAO.saveOrUpdate(departement);
                    }
                	else
                    {
                		dbDepartement.setNoDepartement(noDepartement);
                		departementDAO.saveOrUpdate(dbDepartement);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } 
        catch (IOException e1) 
        {
        	throw new BusinessException(e1.getMessage(), e1);
        } 
        catch (PersistenceException e){throw new BusinessException(e.getMessage(), e);}
        catch (NoResultException e2){throw new BusinessException(e2.getMessage(), e2);}
        catch (NonUniqueResultException e3){throw new BusinessException(e3.getMessage(), e3);}
    }    
    
    /**
     * @param nomRegion
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Region verifieRegion(String nomRegion, long line, int index, ErrorsReport errorsReport)
    {
    	Region region = regionDAO.getByNom(nomRegion).orElse(null);
        if (region == null) 
        {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "REGION_NONDEFINI"),line, index, nomRegion));
        }
        
    	return region;
    }
    
    /**
     * @param noDepartement
     * @param line
     * @param index
     * @param errorsReport
     */
    private void verifieNoDepartement(String noDepartement, long line, int index, ErrorsReport errorsReport)
    {
    	if(!noDepartement.equals("2A") && !noDepartement.equals("2B"))
    	{
    		try
    		{
    			Integer.parseInt(noDepartement);
    		}
    		catch(NumberFormatException e)
    		{
    			errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "NODEP_INCORRECT"), line, index));
    		}
    	}
    	
    	// verifie longueur max. no département (longueur acceptée en base 3 caractères max à cause des départements d'outre-mer en France)
    	if (noDepartement != null && noDepartement.length() > 3) 
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "LONGUEUR_NODEP"), line, index, noDepartement));
    	}
    }
    
    /**
     * @param noDepartement
     * @param line
     * @param index
     * @param errorsReport
     * @throws PersistenceException
     */
    private void verifieDoublonNoDepartement(String noDepartement, long line, int index, ErrorsReport errorsReport)throws BusinessException
    {
    	List<String> lstNoDepartement = departementDAO.getAllNoDepartement();
    	if(lstNoDepartement.contains(noDepartement))
    	{
    		errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "NUMERODEP_EXISTE"), line, index, noDepartement));
    	}
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException 
    {
        try 
        {
            String[] values = parser.getLine();
            while (values != null) 
            {
                
            	TokenizerValues tokenizerValues = new TokenizerValues(values);

                tokenizerValues.nextToken();
                String nomDepartement = tokenizerValues.nextToken();

                Departement departement = departementDAO.getByNom(nomDepartement)
                        .orElseThrow(()->new PersistenceException("no department"));

                departementDAO.remove(departement);
                
                values = parser.getLine();
            }
        } 
        catch (PersistenceException| IOException e) 
        {
            throw new BusinessException(e.getMessage(), e);
        }
    }    
    
    /**
     * @return
     * @throws PersistenceException
     */
    private String[] getRegionPossibles()throws BusinessException 
    {
        List<Region> lstRegions = regionDAO.getAll();
        String[] regionPossibles = new String[lstRegions.size()];
        int index = 0;
        for (Region region : lstRegions) 
        {
        	regionPossibles[index++] = region.getNom();
        }
        
        return regionPossibles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Departement departement) throws BusinessException 
    {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        // Nom de la région
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
        		new ColumnModelGridMetadata(departement == null ? Constantes.STRING_EMPTY : 
        				(departement.getRegion() == null ? Constantes.STRING_EMPTY : departement.getRegion().getNom()), 
        						getRegionPossibles(), null, true, false, true));

        // Nom du département
        // (cas France) Le nom du département est obligatoire et unique
        // Pour les pays étrangers, l'utilisateur entrera obligatoirement un nom (celui de la région)
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
        		new ColumnModelGridMetadata(departement == null ? Constantes.STRING_EMPTY : departement.getNomDepartement(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // No du département
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
        		new ColumnModelGridMetadata(departement == null ? Constantes.STRING_EMPTY : departement.getNoDepartement(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Departement> getAllElements() throws BusinessException {
        return departementDAO.getAll();
    }

	/**
     * @param regionDAO
     *            the regionDAO to set
     */
    public void setRegionDAO(IRegionDAO regionDAO) {
        this.regionDAO = regionDAO;
    }

    /**
     * @param departementDAO
     *            the departementDAO to set
     */
    public void setDepartementDAO(IDepartementDAO departementDAO) {
        this.departementDAO = departementDAO;
    }
    

}
