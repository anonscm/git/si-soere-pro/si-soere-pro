package org.inra.ecoinfo.pro.extraction.incubation.incubationsolpromoyennees.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolProMoyDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolProMoy;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationOutputBuilder;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolProMoyBuildOutputDisplayByRow extends AbstractIncubationOutputBuilder<MesureIncubationSolProMoy> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_INCUBATIONSOLPROMOY";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.incubation.incubation";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";

    static final String PATTERN_CSV_N_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String LIST_COLUMN_VARIABLE_INCUBATION_SOL_PRO_MOY = "LIST_COLUMN_VARIABLE_INCUBATION_SOL_PRO_MOY";

    final ComparatorVariable comparator = new ComparatorVariable();
    IVariablesPRODAO variPRODAO;

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return IncubationSolProMoyExtractor.MAP_INDEX_INCUBATION_SOL_PRO_MOY;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IIncubationSolProMoyDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_PRO_MOY;
    }

    /**
     *
     * @param mesure
     * @param mesuresMap
     */
    @Override
    protected void buildmap(List<MesureIncubationSolProMoy> mesure,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolProMoy>>> mesuresMap) {

        java.util.Iterator<MesureIncubationSolProMoy> itMesure = mesure
                .iterator();
        while (itMesure.hasNext()) {
            MesureIncubationSolProMoy mesureIncubationSolProMoy = itMesure
                    .next();
            EchantillonsSol echan = mesureIncubationSolProMoy.getEchantillonsSol();
            Long siteId = echan.getPrelevementsol().getDispositif().getId();
            if (mesuresMap.get(siteId) == null) {
                mesuresMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresMap.get(siteId).get(echan) == null) {
                mesuresMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresMap.get(siteId).get(echan).put(mesureIncubationSolProMoy.getLocalDate_debut_incub(), mesureIncubationSolProMoy);
            itMesure.remove();
        }
    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedIncubationVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresMap
     */
    @Override
    protected void readMap(List<Dispositif> selectedDispositifs,
            List<VariablesPRO> selectedIncubationVariables,
            IntervalDate selectedIntervalDate,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolProMoy>>> mesuresMap) {

        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                IncubationSolProMoyBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        IncubationSolProMoyBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        IncubationSolProMoyBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_MOY_CODE)
                .get(IncubationSolProMoyExtractor.MAP_INDEX_INCUBATION_SOL_PRO_MOY) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_MOY_CODE)
                        .get(IncubationSolProMoyExtractor.MAP_INDEX_INCUBATION_SOL_PRO_MOY).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_MOY_CODE));
        return null;
    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolProMoy>>> mesuresMap,
            IntervalDate selectedIntervalDate) {

        String currentDispositif;
        String currentEchantillon;
        int ordre_manip;
        float Masse_de_sol_sec;
        LocalDate datePrelPro;
        String currentEchantillonProCode;
        float massePro;
        String condition_incubation;
        LocalDate date_debut_incub;
        int numero_rep_analyse;
        int jour_incub;

        List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                IncubationSolProMoyBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                IncubationSolProMoyBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_INCUBATION_SOL_PRO_MOY).split(";"));

        for (final Dispositif dispositif : selectedDispositifs) {
            PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
            final SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolProMoy>> mesureIncubSolProMoyByDisp = mesuresMap
                    .get(dispositif.getId());

            if (mesureIncubSolProMoyByDisp == null) {
                continue;
            }

            Iterator<Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolProMoy>>> itEchan = mesureIncubSolProMoyByDisp
                    .entrySet().iterator();

            while (itEchan.hasNext()) {

                java.util.Map.Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolProMoy>> echanEntry = itEchan.next();

                SortedMap<LocalDate, MesureIncubationSolProMoy> mesureEchantMap = echanEntry.getValue()
                        .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());

                for (Entry<LocalDate, MesureIncubationSolProMoy> entrySet : mesureEchantMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesureIncubationSolProMoy mesureIncubationSolProMoy = entrySet.getValue();
                    currentDispositif = mesureIncubationSolProMoy.getEchantillonsSol().getPrelevementsol().getDispositif().getNomDispo();
                    currentEchantillon = mesureIncubationSolProMoy.getEchantillonsSol().getCodeechsol();
                    ordre_manip = mesureIncubationSolProMoy.getOrdre_manip();
                    Masse_de_sol_sec = mesureIncubationSolProMoy.getMasse_de_sol();
                    datePrelPro = mesureIncubationSolProMoy.getLocalDate_prel_pro();
                    currentEchantillonProCode = mesureIncubationSolProMoy.getEchantillonsProduit().getCode_unique();
                    massePro = mesureIncubationSolProMoy.getMasse_de_pro();
                    condition_incubation = mesureIncubationSolProMoy.getCondition_incub();
                    date_debut_incub = mesureIncubationSolProMoy.getLocalDate_debut_incub();
                    numero_rep_analyse = mesureIncubationSolProMoy.getNumero_rep_analyse();
                    jour_incub = mesureIncubationSolProMoy.getJour_incub();
                    String genericPattern = LineDataFixe(date, currentDispositif, currentEchantillon, ordre_manip, Masse_de_sol_sec, datePrelPro,
                            currentEchantillonProCode, massePro, condition_incubation, date_debut_incub, numero_rep_analyse, jour_incub);
                    out.print(genericPattern);
                    LineDataVariable(mesureIncubationSolProMoy, out, listVariable);
                }
                itEchan.remove();

            }
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentEchantillon, int ordre_manip, float Masse_de_sol_sec, LocalDate datePrelPro,
            String currentEchantillonProCode, float massePro, String condition_incubation, LocalDate date_debut_incub, int numero_rep_analyse, int jour_incub) {
        String genericPattern = String.format(PATTERN_CSV_N_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentEchantillon,
                ordre_manip,
                Masse_de_sol_sec,
                datePrelPro,
                currentEchantillonProCode,
                massePro,
                condition_incubation,
                date_debut_incub,
                numero_rep_analyse,
                jour_incub);
        return genericPattern;
    }

    private void LineDataVariable(MesureIncubationSolProMoy mesureIncubationSolProMoy, PrintStream out, List<String> listVariable) {

        final List<ValeurIncubationSolProMoy> valeurIncubationSolProMoy = mesureIncubationSolProMoy.getValeurIncubationSolProMoy();
        listVariable.forEach((_item) -> {
            for (Iterator<ValeurIncubationSolProMoy> ValeurIterator = valeurIncubationSolProMoy.iterator(); ValeurIterator.hasNext();) {
                ValeurIncubationSolProMoy valeur = ValeurIterator.next();
//                if (!((DatatypeVariableUnitePRO)valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
//                    continue;
//                }
String line;
line = String.format(";%s;%s;%s;%s;%s;%s",
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
        getValeurToString(valeur),
        valeur.getEcart_type(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());

out.print(line);
ValeurIterator.remove();
            }
        });
        out.println();
    }

    private String getValeurToString(ValeurIncubationSolProMoy valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
