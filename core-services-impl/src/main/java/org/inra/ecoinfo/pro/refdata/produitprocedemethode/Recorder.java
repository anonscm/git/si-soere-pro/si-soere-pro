package org.inra.ecoinfo.pro.refdata.produitprocedemethode;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.methodeprocess.IMethodeProcessDAO;
import org.inra.ecoinfo.pro.refdata.methodeprocess.MethodeProcess;
import org.inra.ecoinfo.pro.refdata.process.IProcessDAO;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.unites.IUniteDAO;
import org.inra.ecoinfo.pro.refdata.unites.Unites;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<ProduitProcedeMethode> {

    private static final String PROPERTY_MSG_METHODE_BAD_NOM = "PROPERTY_MSG_METHODE_BAD_NOM";

    private static final String PROPERTY_MSG_METHODE_BAD_CODE = "PROPERTY_MSG_METHODE_BAD_CODE";
    private static final String PROPERTY_MSG_METHODE_BAD_PRODUIT = "PROPERTY_MSG_METHODE_BAD_PRODUIT";
    private static final String PROPERTY_MSG_METHODE_BAD_UNITE = "PROPERTY_MSG_METHODE_BAD_UNITE";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    protected IProduitProcedeMethodeDAO ppmDAO;

    /**
     *
     */
    protected IMethodeProcessDAO methodeprocessDAO;
    IProduitDAO produitDAO;
    IProcessDAO processDAO;
    IUniteDAO unitesDAO;
    Properties comentEn;

    private String[] listeMethodePossibles;
    private String[] listeUnitePossibles;
    private String[] listeProduitsPossibles;

    private void createOrupdateProduitProcedeMethode(final Produits prod, MethodeProcess methodeprocess, Unites unite, int duree, String commentaire, ProduitProcedeMethode pmp) throws BusinessException {
        if (pmp == null) {
            ProduitProcedeMethode ppm = new ProduitProcedeMethode(prod, methodeprocess, duree, unite, commentaire);
            ppm.setProduits(prod);
            ppm.setMethodeprocess(methodeprocess);
            ppm.setUnite(unite);
            ppm.setDuree(duree);
            ppm.setCommentaire(commentaire);
            createPPM(ppm);
        } else {
            updateBDPPM(prod, methodeprocess, duree, unite, commentaire, pmp);
        }
    }

    private void createPPM(final ProduitProcedeMethode ppm) throws BusinessException {
        try {
            ppmDAO.saveOrUpdate(ppm);
            ppmDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create ProduitProcedeMethode");
        }

    }

    private void updateBDPPM(final Produits prod, MethodeProcess methodeprocess, int duree, Unites unite, String commentaire, final ProduitProcedeMethode dbppm) throws BusinessException {
        try {
            dbppm.setProduits(prod);
            dbppm.setUnite(unite);
            dbppm.setDuree(duree);
            dbppm.setCommentaire(commentaire);
            dbppm.setMethodeprocess(methodeprocess);
            ppmDAO.saveOrUpdate(dbppm);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update ProduitProcedeMethode");
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeProduit = tokenizerValues.nextToken();
                final String methodPrcodeNom = tokenizerValues.nextToken();
                Produits dbprod = produitDAO.getByNKey(codeProduit).orElse(null);
                String duree = tokenizerValues.nextToken();
                int dure = Integer.parseInt(duree);
                MethodeProcess dbmprocess = methodeprocessDAO.getByNKey(methodPrcodeNom).orElse(null);
                final ProduitProcedeMethode dbppm = ppmDAO.getByNKey(dbprod, dbmprocess, dure)
                        .orElseThrow(() -> new BusinessException("can't get ProduitProcedeMethode"));
                ppmDAO.remove(dbppm);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<ProduitProcedeMethode> getAllElements() throws BusinessException {
        return ppmDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IMethodeProcessDAO getMethodeprocessDAO() {
        return methodeprocessDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ProduitProcedeMethode ppm) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        /* String valeurProduit = ppm == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : ppm.getMethodeprocedes() != null ? ppm.getMethodeprocedes().getProduits().getProd_key() : "";
        ColumnModelGridMetadata columnProduit = new ColumnModelGridMetadata(valeurProduit, listeProduitsPossibles, null, true, false, true);
        String valeurProcede = ppm == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : ppm.getMethodeprocedes() != null ? ppm.getMethodeprocedes().getProcess().getProcess_intitule() : "";
        ColumnModelGridMetadata columnProcede = new ColumnModelGridMetadata(valeurProcede, listeProcedesPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnProcede);
        columnProcede.setValue(valeurProcede);
        columnProduit.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnProduit);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnProcede);*/
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ppm == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ppm.getProduits().getProd_key() != null ? ppm.getProduits().getProd_key()
                        : "", listeProduitsPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ppm == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ppm.getMethodeprocess().getMethodep_nom() != null ? ppm.getMethodeprocess().getMethodep_nom()
                        : "", listeMethodePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ppm == null || ppm.getDuree() == EMPTY_INT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ppm.getDuree(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ppm == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : ppm.getUnite().getUnite() != null ? ppm.getUnite().getUnite()
                        : "", listeUnitePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ppm == null || ppm.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ppm.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(ppm == null || ppm.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comentEn.getProperty(ppm.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProduitProcedeMethodeDAO getPpmDAO() {
        return ppmDAO;
    }

    @Override
    protected ModelGridMetadata<ProduitProcedeMethode> initModelGridMetadata() {
        produitPossibles();
        methodePossibles();
        listeUnitePossibles();
        comentEn = localizationManager.newProperties(ProduitProcedeMethode.NAME_ENTITY_JPA, ProduitProcedeMethode.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /*  private void initProduitsPossibles() throws BusinessException {
        Map<String, String[]> produitsProcedes = new HashMap<>();
        Map<String, Set<String>> produitsProcedesList = new HashMap<>();
        List<MethodeProcedes> groupeproduits = methodeprocedeDAO.getAll(MethodeProcedes.class);
        for (MethodeProcedes methodeProcede : groupeproduits) {
            String produitNom = methodeProcede.getProduits().getProd_key();
            String procedeNom = methodeProcede.getProcess().getProcess_intitule();
            if (!produitsProcedesList.containsKey(produitNom)) {
                produitsProcedesList.put(produitNom, new TreeSet());
            }
            produitsProcedesList.get(produitNom).add(procedeNom);
        }
        for (Entry<String, Set<String>> entryProduit : produitsProcedesList.entrySet()) {
            produitsProcedes.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        }
        this.listeProcedesPossibles = produitsProcedes;
        listeProduitsPossibles = produitsProcedes.keySet().toArray(new String[]{});
    }*/
    private void produitPossibles() {
        List<Produits> groupeproduit = produitDAO.getAll();
        String[] Listeproduit = new String[groupeproduit.size() + 1];
        Listeproduit[0] = "";
        int index = 1;
        for (Produits produit : groupeproduit) {
            Listeproduit[index++] = produit.getProd_key();
        }
        this.listeProduitsPossibles = Listeproduit;
    }

    private void methodePossibles() {
        List<MethodeProcess> groupemethode = methodeprocessDAO.getAll(MethodeProcess.class);
        String[] Listemethode = new String[groupemethode.size() + 1];
        Listemethode[0] = "";
        int index = 1;
        for (MethodeProcess mprocess : groupemethode) {
            Listemethode[index++] = mprocess.getMethodep_nom();
        }
        this.listeMethodePossibles = Listemethode;
    }

    private void listeUnitePossibles() {
        List<Unites> groupeunite = unitesDAO.getAll(Unites.class);
        String[] Listeunites = new String[groupeunite.size() + 1];
        Listeunites[0] = "";
        int index = 1;
        for (Unites unite : groupeunite) {
            Listeunites[index++] = unite.getUnite();
        }
        this.listeUnitePossibles = Listeunites;
    }

    private void persistPPM(Produits prod, Unites unite, int duree, String commentaire, MethodeProcess mprocess) throws BusinessException, BusinessException {
        final ProduitProcedeMethode dbppm = ppmDAO.getByNKey(prod, mprocess, duree).orElse(null);
        createOrupdateProduitProcedeMethode(prod, mprocess, unite, duree, commentaire, dbppm);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ProduitProcedeMethode.NAME_ENTITY_JPA);
                int indexcode = tokenizerValues.currentTokenIndex();
                final String codeProduit = tokenizerValues.nextToken();
                int indexnom = tokenizerValues.currentTokenIndex();
                final String codeMethodeProcess = tokenizerValues.nextToken();
                int dure = verifieInt(tokenizerValues, line, false, errorsReport);
                int indexunite = tokenizerValues.currentTokenIndex();
                String unite = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();

                Produits dbprod = produitDAO.getByNKey(codeProduit).orElse(null);
                if (dbprod == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_METHODE_BAD_PRODUIT), line, indexcode, codeProduit));
                }
                MethodeProcess dbmprocess = methodeprocessDAO.getByNKey(codeMethodeProcess).orElse(null);

                if (dbmprocess == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_METHODE_BAD_NOM), line, indexnom, codeMethodeProcess));
                }

                Unites dbunite = unitesDAO.getByunite(unite).orElse(null);
                if (dbunite == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_METHODE_BAD_UNITE), line, indexunite, unite));
                }

                if (!errorsReport.hasErrors()) {
                    persistPPM(dbprod, dbunite, dure, commentaire, dbmprocess);
                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);

        }

    }

    /**
     *
     * @param methodeprocessDAO
     */
    public void setMethodeprocessDAO(IMethodeProcessDAO methodeprocessDAO) {
        this.methodeprocessDAO = methodeprocessDAO;
    }

    /**
     *
     * @param ppmDAO
     */
    public void setPpmDAO(IProduitProcedeMethodeDAO ppmDAO) {
        this.ppmDAO = ppmDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param unitesDAO
     */
    public void setUnitesDAO(IUniteDAO unitesDAO) {
        this.unitesDAO = unitesDAO;
    }

}
