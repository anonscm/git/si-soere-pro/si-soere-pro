
package org.inra.ecoinfo.pro.extraction.fluxchambre.impl;

import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.extraction.fluxchambre.IFluxChambreDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class ChambreExtractor extends AbstractExtractor{

    /**
     *
     */
    public static final String    FLUX_CHAMBRE                    = "flux_chambre" ;

    /**
     *
     */
    protected static final String MAP_INDEX_FLUX_CHAMBRE              = "fluxchambre";

    /**
     *
     */
    protected static final String MAP_INDEX_0                       = "0";

    /**
     *
     */
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";
    private IFluxChambreDAO fluxChambreDAO;
    
    @Override
    public void extract(IParameter parameters) throws BusinessException {
          this.prepareRequestMetadatas(parameters.getParameters());
        final Map<String, List> resultsDatasMap = this.extractDatas(parameters.getParameters());
        if (((FluxChambreParameterVO) parameters).getResults().get(
                FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE) == null
                || ((FluxChambreParameterVO) parameters).getResults()
                .get(FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE).isEmpty()) {
            ((DefaultParameter) parameters).getResults().put(
                    FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE, resultsDatasMap);
        } else {
            ((FluxChambreParameterVO) parameters)
            .getResults()
            .get(FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE)
            .put(ChambreExtractor.FLUX_CHAMBRE, resultsDatasMap.get(ChambreExtractor.MAP_INDEX_FLUX_CHAMBRE));
        }
        ((FluxChambreParameterVO) parameters)
        .getResults()
        .get(FluxChambreExtractor.CST_RESULT_EXTRACTION_FLUX_CHAMBRE_CODE)
        .put(ChambreExtractor.MAP_INDEX_0, resultsDatasMap.get(ChambreExtractor.MAP_INDEX_FLUX_CHAMBRE));
    }

    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Map<String, List> extractedDatasMap = new HashMap();
        try {
            final IntervalDate intervalDate = retrieveDates(requestMetadatasMap);
            final List<INodeable> selectedDispositif = retrieveDispositif(requestMetadatasMap);
            final List<VariablesPRO> selectedVariables = retrieveVariables(requestMetadatasMap);
            final List<MesureFluxChambres> mesuresFluxChambres = this.fluxChambreDAO
                    .extractFluxChambre(selectedDispositif, selectedVariables, intervalDate, policyManager.getCurrentUser());
            if (mesuresFluxChambres == null || mesuresFluxChambres.isEmpty()) {
                throw new NoExtractionResultException(this.localizationManager.getMessage(
                        NoExtractionResultException.BUNDLE_SOURCE_PATH,
                        ChambreExtractor.PROPERTY_MSG_NO_EXTRACTION_RESULT));
            }
            extractedDatasMap.put(ChambreExtractor.MAP_INDEX_FLUX_CHAMBRE, mesuresFluxChambres);
        } catch (final DateTimeParseException| BadExpectedValueException | BusinessException  e) {
            AbstractExtractor.LOGGER.error(e.getMessage());
            throw new BusinessException(e);
        }
        return extractedDatasMap;
    }

    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {
        this.sortSelectedVariables(requestMetadatasMap);
    }

    private void sortSelectedVariables(final Map<String, Object> requestMetadatasMap) {
        Collections.sort((List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(FluxChambreParameterVO.FLUXCHAMBRE)), (final VariablesPRO o1, final VariablesPRO o2) -> o1.getId().toString().compareTo(o2.getId().toString()));
    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
         try {
            final Map<String, Object> requestMetadatasMap = parameters.getParameters();
            final IntervalDate intervalDate = retrieveDates(requestMetadatasMap);
            final List<INodeable> selectedDispositif = retrieveDispositif(requestMetadatasMap);
            final List<VariablesPRO> selectedVariables = retrieveVariables(requestMetadatasMap);
            return fluxChambreDAO.sizeFluxChambre(selectedDispositif, selectedVariables, intervalDate, policyManager.getCurrentUser());
        } catch (DateTimeParseException | BadExpectedValueException ex) {
            return -1l;
        } 
    }
    
    /**
     *
     * @param fluxChambreDAO
     */
    public void setFluxChambreDAO(IFluxChambreDAO fluxChambreDAO) {
        this.fluxChambreDAO = fluxChambreDAO;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     */
    protected List<VariablesPRO> retrieveVariables(Map<String, Object> requestMetadatasMap) {
        return (List<VariablesPRO>) requestMetadatasMap
                .get(FLUX_CHAMBRE);
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     */
    protected List<INodeable> retrieveDispositif(Map<String, Object> requestMetadatasMap) {
        return (List<INodeable>) requestMetadatasMap
                .get(Dispositif.class.getSimpleName());
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BadExpectedValueException
     */
    protected IntervalDate retrieveDates(Map<String, Object> requestMetadatasMap) throws BadExpectedValueException {
        return (IntervalDate) requestMetadatasMap
                .get(IntervalDate.class.getSimpleName());
    }
       
    
}
