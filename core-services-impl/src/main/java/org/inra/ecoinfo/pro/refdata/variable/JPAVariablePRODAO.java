/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.variable;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.refdata.variable.JPAVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAVariablePRODAO extends JPAVariableDAO implements IVariablesPRODAO{
    
    private static final String QUERY_VARIABLE_NOM = "from VariablesPRO v where v.nom = :nom";
    private static final String QUERY_VARIABLE_KEY = "from VariablesPRO v where v.code = :key";
    private static final String QUERY_VARIABLE_KEY_NOM = "from VariablesPRO va where va.code = :key and va.nom =:nom";
    static final String                           QUERY_GET_BY_AFFICHAGE = "from Variable v where v.affichage = :affichage";
    static final String                           QUERY_GET_ALL          = "from VariablesPRO";

    @Override
    public List<Variable> getAll() {
        return getAllBy(Variable.class, Variable::getCode); 
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<VariablesPRO> getByNames(String nom) {
        CriteriaQuery<VariablesPRO> query = builder.createQuery(VariablesPRO.class);
        Root<VariablesPRO> variablesPRO = query.from(VariablesPRO.class);
        query
                .select(variablesPRO)
                .where(
                        builder.equal(variablesPRO.get(VariablesPRO_.code), nom)
                );
        return getOptional(query);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<VariablesPRO> betByNKey(String code) {
        CriteriaQuery<VariablesPRO> query = builder.createQuery(VariablesPRO.class);
        Root<VariablesPRO> variablesPRO = query.from(VariablesPRO.class);
        query
                .select(variablesPRO)
                .where(
                        builder.equal(variablesPRO.get(VariablesPRO_.code), code)
                );
        return getOptional(query);
    }

    /**
     *
     * @param codeUser
     * @return
     */
    @Override
    public Optional<VariablesPRO> getByCodeUser(String codeUser) {
        CriteriaQuery<VariablesPRO> query = builder.createQuery(VariablesPRO.class);
        Root<VariablesPRO> variablesPRO = query.from(VariablesPRO.class);
        query
                .select(variablesPRO)
                .where(
                        builder.equal(variablesPRO.get(VariablesPRO_.codeUser), Utils.createCodeFromString(codeUser))
                );
        return getOptional(query);
     }

    /**
     *
     * @return
     */
    @Override
    public List<VariablesPRO> getAllVariablePRO()  {
        return getAll(VariablesPRO.class);
    }
}
