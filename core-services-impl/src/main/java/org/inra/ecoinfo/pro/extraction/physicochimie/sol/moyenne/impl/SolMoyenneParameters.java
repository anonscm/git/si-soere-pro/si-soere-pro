/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.sol.moyenne.impl;

import java.util.Map;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieParameter;

/**
 *
 * @author adiankha
 */
public class SolMoyenneParameters extends AbstractPhysicoChimieParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_SOLMOYENNE = "physico_chimie_des_sols_moyennes";

    /**
     *
     */
    public static final String SOLMOYENNE = "SolMoyenne";

    /**
     *
     * @param metadatasMap
     */
    public SolMoyenneParameters(Map<String, Object> metadatasMap) {
        super(metadatasMap);
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_SOLMOYENNE;
    }
}
