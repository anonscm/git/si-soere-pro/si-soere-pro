package org.inra.ecoinfo.pro.refdata.typeclimat;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface ITypeClimatDAO extends IDAO<Typeclimat> {

    /**
     *
     * @return
     */
    List<Typeclimat> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Typeclimat> getByNKey(String nom);

}
