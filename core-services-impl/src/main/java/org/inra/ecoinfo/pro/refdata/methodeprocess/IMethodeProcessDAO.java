/*
 *
 */
package org.inra.ecoinfo.pro.refdata.methodeprocess;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;


// TODO: Auto-generated Javadoc
/**
 * The Interface IMethodeProcessDAO.
 */
public interface IMethodeProcessDAO extends IDAO<MethodeProcess> {

    /**
     *
     * @return
     */
    List<MethodeProcess> getAll();

    /**
     *
     * @param methodep_nom
     * @return
     */
    Optional<MethodeProcess> getByNKey(String methodep_nom);

}
