/**
 *
 */
package org.inra.ecoinfo.pro.refdata.dispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu_;

/**
 * @author sophie
 *
 */
public class JPADispositifDAO extends AbstractJPADAO<Dispositif> implements IDispositifDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<Dispositif> getAll() {
        return getAllBy(Dispositif.class, Dispositif::getNomDispositif_nomLieu);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO#getByNKey( java.lang.String, java.lang.String)
     */

    /**
     *
     * @param codeDispo
     * @param lieu
     * @return
     */

    @Override
    public Optional<Dispositif> getByNKey(String codeDispo, Lieu lieu) {
        CriteriaQuery<Dispositif> query = builder.createQuery(Dispositif.class);
        Root<Dispositif> dispositif = query.from(Dispositif.class);
        Join<Dispositif, Lieu> dblieu = dispositif.join(Dispositif_.lieu);
        query
                .select(dispositif)
                .where(
                        builder.equal(dispositif.get(Dispositif_.codeDispo), codeDispo),
                        builder.equal(dblieu, lieu)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO#getByNKey( java.lang.String, java.lang.String)
     */

    /**
     *
     * @param code
     * @param nomLieu
     * @return
     */

    @Override
    public Optional<Dispositif> getByNKey(String code, String nomLieu) {
        CriteriaQuery<Dispositif> query = builder.createQuery(Dispositif.class);
        Root<Dispositif> dispositif = query.from(Dispositif.class);
        Join<Dispositif, Lieu> dblieu = dispositif.join(Dispositif_.lieu);
        query
                .select(dispositif)
                .where(
                        builder.equal(dispositif.get(Dispositif_.codeDispo), code),
                        builder.equal(dblieu.get(Lieu_.nom), nomLieu)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO#getByGeolocalisation (org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation)
     */

    /**
     *
     * @param geolocalisation
     * @return
     */

    @Override
    public Optional<Dispositif> getByGeolocalisation(Geolocalisation geolocalisation) {
        CriteriaQuery<Dispositif> query = builder.createQuery(Dispositif.class);
        Root<Dispositif> dispositif = query.from(Dispositif.class);
        Join<Dispositif, Geolocalisation> gL = dispositif.join(Dispositif_.geolocalisation);
        query
                .select(dispositif)
                .where(
                        builder.equal(gL, geolocalisation)
                );
        return getOptional(query);
    }

}
