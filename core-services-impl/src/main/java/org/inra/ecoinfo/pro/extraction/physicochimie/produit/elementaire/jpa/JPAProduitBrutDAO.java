/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.elementaire.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO_;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePRODatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.jpa.JPAPhysicoChimieDAO;
import org.inra.ecoinfo.pro.synthesis.physicochimiepro.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author adiankha
 */
public class JPAProduitBrutDAO extends JPAPhysicoChimieDAO<MesurePhysicoChimiePRO, ValeurPhysicoChimiePRO> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurPhysicoChimiePRO> getValeurPhysicoChimieClass() {
        return ValeurPhysicoChimiePRO.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesurePhysicoChimiePRO> getMesurePhysicoChimieClass() {
        return MesurePhysicoChimiePRO.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurPhysicoChimiePRO, MesurePhysicoChimiePRO> getMesureAttribute() {
        return ValeurPhysicoChimiePRO_.mesurePhysicoChimiePRO;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
       return IPhysicoChimiePRODatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO;
    }

}
