/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsol;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.dureeconservation.IDureeConcervationDAO;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.prelevementsol.IPrelevementSolDAO;
import org.inra.ecoinfo.pro.refdata.prelevementsol.PrelevementSol;
import org.inra.ecoinfo.pro.refdata.typeechantillon.ITypeEchantillon;
import org.inra.ecoinfo.pro.refdata.typeechantillon.TypeEchantillon;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder.ErrorsReport;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha Vivianne
 */
public class Recorder extends AbstractGenericRecorder<EchantillonsSol> {

    private static final String PROPERTY_MSG_ECHANTILLOSOL_BAD_PRELEVEMNT = "PROPERTY_MSG_ECHANTILLOSOL_BAD_PRELEVEMNT";
    private static final String PROPERTY_MSG_TYPEECHANTILLON_BAD_PRELEVEMNT = "PROPERTY_MSG_TYPEECHANTILLON_BAD_PRELEVEMNT";
    private static final String PROPERTY_MSG_DUREECONSERVATION_BAD_PRELEVEMNT = "PROPERTY_MSG_DUREECONSERVATION_BAD_PRELEVEMNT";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    IEchantillonSolDAO echantillonSolDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;
    IPrelevementSolDAO prelevementsolDAO;
    ITypeEchantillon typeechantillonDAO;
    IDureeConcervationDAO dureeconservDAO;

    private String[] listeBooleansPossible;
    private String[] listeCodePrelevementPossible;
    private String[] listeTypeEchantillonPossible;
    private String[] listeDureeConservPossible;

    Properties commentaireEn;

    private void createEnchantillonsSol(EchantillonsSol echantillonssol) throws BusinessException {
        try {
            echantillonSolDAO.saveOrUpdate(echantillonssol);
            echantillonSolDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create EnchantillonsSol");
        }
    }

    private void updateEchantillonsSol(EchantillonsSol dbES, TypeEchantillon typeechantillon, String codelabo,
            String statut, String homogeneisation, String emottage, float tamisage, float temperaturesechage, float broyage,
            float tempConServation, String conservationapresanalyse, float tempconservationEchantillontheque,
            float masseavantenvoie, String commentaire) throws BusinessException {

        try {
            dbES.setMasseavantenvoie(masseavantenvoie);
            dbES.setStatut(statut);
            dbES.setTamisage(tamisage);
            dbES.setTemperaturesechage(temperaturesechage);
            dbES.setBroyage(broyage);
            dbES.setHomogeneisation(homogeneisation);
            dbES.setTempConservation(tempConServation);
            dbES.setConservationapresanalyse(conservationapresanalyse);
            dbES.setTempconservationEchantillontheque(tempconservationEchantillontheque);
            dbES.setCommentaire(commentaire);
            dbES.setTypeechantillon(typeechantillon);
            dbES.setEmottage(emottage);
            echantillonSolDAO.saveOrUpdate(dbES);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create EnchantillonsSol");
        }
    }

    private void createOrUpdateEchantillonsSol(EchantillonsSol dbES, PrelevementSol prelevementsol, long codebaseechantillon, String codeechsol, TypeEchantillon typeechantillon, String codelabo,
            String statut, String homogeneisation, String emottage, float tamisage, float temperaturesechage, float broyage,
            float tempconcervation, String conservationapresanalyse, float tempconservationEchantillontheque,
            float masseavantenvoie, String commentaire) throws BusinessException {
        if (dbES == null) {
            EchantillonsSol echantillonssol = new EchantillonsSol(prelevementsol, codebaseechantillon, codeechsol, typeechantillon, codelabo,
                    statut, homogeneisation, emottage, tamisage, temperaturesechage, broyage,
                    tempconcervation, conservationapresanalyse, tempconservationEchantillontheque,
                    masseavantenvoie, commentaire);
            createEnchantillonsSol(echantillonssol);

        } else {
            updateEchantillonsSol(dbES, typeechantillon, codelabo,
                    statut, homogeneisation, emottage, tamisage, temperaturesechage, broyage,
                    tempconcervation, conservationapresanalyse, tempconservationEchantillontheque,
                    masseavantenvoie, commentaire);
        }

    }

    private void persistEchantillonsSol(PrelevementSol prelevementsol, long codebaseechantillon, String codeechsol, TypeEchantillon typeechantillon, String codelabo,
            String statut, String homogeneisation, String emottage, float tamisage, float temperaturesechage, float broyage,
            float tempconcervation, String conservationapresanalyse, float tempconservationEchantillontheque,
            float masseavantenvoie, String commentaire) throws BusinessException, BusinessException {
        EchantillonsSol dbEchantillonsSol = echantillonSolDAO.getByNKey(codebaseechantillon, prelevementsol).orElse(null);
        createOrUpdateEchantillonsSol(dbEchantillonsSol, prelevementsol, codebaseechantillon, codeechsol, typeechantillon, codelabo,
                statut, homogeneisation, emottage, tamisage, temperaturesechage, broyage,
                tempconcervation, conservationapresanalyse, tempconservationEchantillontheque,
                masseavantenvoie, commentaire);

    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    private void listePSolPossibles() {
        List<PrelevementSol> groupespsol = prelevementsolDAO.getAll();
        String[] listepsolPossibles = new String[groupespsol.size() + 1];
        listepsolPossibles[0] = "";
        int index = 1;
        for (PrelevementSol psol : groupespsol) {
            listepsolPossibles[index++] = psol.getCodesol();
        }
        this.listeCodePrelevementPossible = listepsolPossibles;
    }

    private void listeTypeEchantillonPossibles() {
        List<TypeEchantillon> groupescode = typeechantillonDAO.getAll();
        String[] listypePossibles = new String[groupescode.size() + 1];
        listypePossibles[0] = "";
        int index = 1;
        for (TypeEchantillon codeprel : groupescode) {
            listypePossibles[index++] = codeprel.getTe_nom();
        }
        this.listeTypeEchantillonPossible = listypePossibles;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                String codepsol = values[0];
                final String ceb1 = values[1]; //numero de l'echantillon 
                long ceb = Long.parseLong(ceb1);
                PrelevementSol dbpsol = prelevementsolDAO.getByNKey(codepsol).orElse(null);
                echantillonSolDAO.remove(echantillonSolDAO.getByNKey(ceb, dbpsol)
                        .orElseThrow(() -> new BusinessException("Can't find echantillon sol")));
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EchantillonsSol.NAME_ENTITY_JPA);
                int indexsol = tokenizerValues.currentTokenIndex();
                String codePrelevementSol = tokenizerValues.nextToken();
                long ordreEchantillon = verifieLong(tokenizerValues, line, true, errorsReport);
                int indextype = tokenizerValues.currentTokenIndex();
                String codeEchantillonSI = tokenizerValues.nextToken();
                final String typeEchantillon = tokenizerValues.nextToken();
                final String referenceEchantillonInterne = tokenizerValues.nextToken(); //code interne labo
                final String statut = tokenizerValues.nextToken();
                final String homogeneisation = tokenizerValues.nextToken();
                final String emottage = tokenizerValues.nextToken();
                float tamisage = verifieFloat(tokenizerValues, line, false, errorsReport);
                float temperatureSechage = verifieFloat(tokenizerValues, line, false, errorsReport);
                float broyage = verifieFloat(tokenizerValues, line, false, errorsReport);
                float temperatureConservation = verifieFloat(tokenizerValues, line, false, errorsReport);
                final String conservationApresAnalyse = tokenizerValues.nextToken();
                float temperatureConservationEchantilontheque = verifieFloat(tokenizerValues, line, false, errorsReport);
                float masseAvantEnvoi = verifieFloat(tokenizerValues, line, false, errorsReport);
                final String commentaire = tokenizerValues.nextToken();
                PrelevementSol dbPrelevementSol = prelevementsolDAO.getByNKey(codePrelevementSol).orElse(null);
                if (dbPrelevementSol == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ECHANTILLOSOL_BAD_PRELEVEMNT), line, indexsol, codePrelevementSol));
                }

                TypeEchantillon dbTypeEchantillon = typeechantillonDAO.getByNkey(typeEchantillon).orElse(null);
                if (dbTypeEchantillon == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_TYPEECHANTILLON_BAD_PRELEVEMNT), line, indextype, typeEchantillon));
                }

                String codelabo = codeEchantillonBase(ordreEchantillon, codePrelevementSol);
                final String codeesol = Utils.createCodeFromString(codelabo);

                if (!errorsReport.hasErrors()) {
                    persistEchantillonsSol(dbPrelevementSol, ordreEchantillon, codeEchantillonSI, dbTypeEchantillon, codelabo,
                            statut, homogeneisation, emottage, tamisage, temperatureSechage, broyage,
                            temperatureConservation, conservationApresAnalyse, temperatureConservationEchantilontheque,
                            masseAvantEnvoi, commentaire);

                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private String codeEchantillonBase(long ordre, String codesol) {
        String home = "a_b";
        String hom = home.substring(1, 2);
        String keys = codesol + hom + ordre;
        return keys;
    }

    @Override
    protected List<EchantillonsSol> getAllElements() throws BusinessException {
        return echantillonSolDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EchantillonsSol echantillonsSol) throws BusinessException {

        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getPrelevementsol().getCodesol() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getPrelevementsol().getCodesol() != null
                        ? echantillonsSol.getPrelevementsol().getCodesol() : "",
                        listeCodePrelevementPossible, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getCodebaseechantillon() == 0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getCodebaseechantillon(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getCodeechsol() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getCodeechsol(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getTypeechantillon().getTe_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getTypeechantillon().getTe_nom() != null
                        ? echantillonsSol.getTypeechantillon().getTe_nom() : "",
                        listeTypeEchantillonPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getCodelabo() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getCodelabo(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getStatut() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getStatut(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getHomogeneisation() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getHomogeneisation() != null
                        ? echantillonsSol.getHomogeneisation() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getEmottage() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getEmottage() != null
                        ? echantillonsSol.getEmottage() : "",
                        listeBooleansPossible, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getTamisage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getTamisage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getTemperaturesechage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getTemperaturesechage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getBroyage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getBroyage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getTempConservation() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getTempConservation(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getConservationapresanalyse() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getConservationapresanalyse() != null
                        ? echantillonsSol.getConservationapresanalyse() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getTempconservationEchantillontheque() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getTempconservationEchantillontheque(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getMasseavantenvoie() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getMasseavantenvoie(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillonsSol.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillonsSol == null || echantillonsSol.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(echantillonsSol.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param echantillonSolDAO
     */
    public void setEchantillonSolDAO(IEchantillonSolDAO echantillonSolDAO) {
        this.echantillonSolDAO = echantillonSolDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param prelevementsolDAO
     */
    public void setPrelevementsolDAO(IPrelevementSolDAO prelevementsolDAO) {
        this.prelevementsolDAO = prelevementsolDAO;
    }

    /**
     *
     * @param typeechantillonDAO
     */
    public void setTypeechantillonDAO(ITypeEchantillon typeechantillonDAO) {
        this.typeechantillonDAO = typeechantillonDAO;
    }

    /**
     *
     * @param dureeconservDAO
     */
    public void setDureeconservDAO(IDureeConcervationDAO dureeconservDAO) {
        this.dureeconservDAO = dureeconservDAO;
    }

    @Override
    protected ModelGridMetadata<EchantillonsSol> initModelGridMetadata() {
        listeBooleanPossibles();
        listeTypeEchantillonPossibles();
        listePSolPossibles();
        commentaireEn = localizationManager.newProperties(EchantillonsSol.NAME_ENTITY_JPA, EchantillonsSol.JPA_COLUMN_COMMENTAIRE, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

}
