/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.physicochimie;

import java.time.LocalDate;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.MesurePhysicoChimie_;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie;
import org.inra.ecoinfo.pro.dataset.physicochimie.entity.ValeurPhysicoChimie_;
import org.inra.ecoinfo.pro.synthesis.AbstractProSynthesisValue;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;

/**
 *
 * @author ptcherniati
 * @param <SD>
 */
public abstract class AbstractSynthesisDAO<SV extends AbstractProSynthesisValue, SD extends GenericSynthesisDatatype, V extends ValeurPhysicoChimie, M extends MesurePhysicoChimie> extends AbstractSynthesis<SV, SD> {

    /**
     *
     * @return
     */
    @Override
    public Stream<SV> getSynthesisValue() {
        CriteriaQuery<SV> query = builder.createQuery(getSynthesisValueClass());
        Root<V> v = query.from(getValueClass());
        Root<NodeDataSet> node = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> varRn2 = node.join(NodeDataSet_.realNode);

        Join<V, M> m = v.join(getMesureAttribute());
        Join<V, RealNode> varRn = v.join(ValeurPhysicoChimie_.realNode);
        final Join<RealNode, RealNode> datatypeRn = varRn.join(RealNode_.parent);
        final Join<RealNode, RealNode> themeRn = datatypeRn.join(RealNode_.parent);
//        Join<RealNode, RealNode> parcelleRn = datatypeRn.join(RealNode_.parent);
//        Join<RealNode, RealNode> dispositifRn = parcelleRn.join(RealNode_.parent);
        Join<RealNode, RealNode> dispositifRn = themeRn.join(RealNode_.parent);
        query.distinct(true);
        Path<Float> valuePath = v.get(ValeurPhysicoChimie_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = m.get(MesurePhysicoChimie_.localDatePrelevement);
        final Path<String> dispositifPath = dispositifRn.get(RealNode_.path);
        Path<Long> idNode = node.get(NodeDataSet_.id);
        query
                .select(builder.construct(
                        getSynthesisValueClass(),
                        dateMesure,
                        dispositifPath,
                        variableCode,
                        builder.avg(valuePath),
                        idNode
                    )
                )
                .where(builder.equal(varRn2, varRn),
                        builder.or(
                                builder.isNull(valuePath),
                                builder.gt(valuePath, -9999)
                        )
                )
                .groupBy(
                        dateMesure,
                        dispositifPath,
                        variableCode,
                        idNode
                )
                .orderBy(
                        builder.asc(dispositifPath),
                        builder.asc(variableCode),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }

    abstract Class<V> getValueClass();

    abstract Class<SV> getSynthesisValueClass();

    /**
     *
     * @return
     */
    abstract protected SingularAttribute<V, M> getMesureAttribute();

}
