/**
 *
 */
package org.inra.ecoinfo.pro.refdata.modalite;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;

/**
 * @author sophie
 *
 */
public class JPAModaliteDAO extends AbstractJPADAO<Modalite> implements IModaliteDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.modalite.IModaliteDAO#getAll()
     */
    @Override
    public List<Modalite> getAll() {
        return getAllBy(Modalite.class, Modalite::getValeur);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.modalite.IModaliteDAO#getByValeurFacteur (java.lang.String, org.inra.ecoinfo.pro.refdata.facteur.Facteur)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Optional<Modalite> getByValeurFacteur(String valeur, Facteur facteur) {
        CriteriaQuery<Modalite> query = builder.createQuery(Modalite.class);
        Root<Modalite> modalite = query.from(Modalite.class);
        Join<Modalite, Facteur> fac = modalite.join(Modalite_.facteur);
        query
                .select(modalite)
                .where(
                        builder.equal(fac, facteur),
                        builder.equal(modalite.get(Modalite_.valeur), valeur)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.modalite.IModaliteDAO#getLstModByFacteur (org.inra.ecoinfo.pro.refdata.facteur.Facteur)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Modalite> getLstModByFacteur(Facteur facteur) {
        CriteriaQuery<Modalite> query = builder.createQuery(Modalite.class);
        Root<Modalite> modalite = query.from(Modalite.class);
        Join<Modalite, Facteur> fac = modalite.join(Modalite_.facteur);
        query
                .select(modalite)
                .where(
                        builder.equal(fac, facteur)
                );
        return getResultList(query);
    }
}
