/**
 *
 */
package org.inra.ecoinfo.pro.refdata.descriptiontraitement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;

/**
 * @author sophie
 *
 */
public interface IDescriptionTraitementDAO extends IDAO<DescriptionTraitement> {

    /**
     *
     * @return
     */
    List<DescriptionTraitement> getAll();

    /*
     * public DescriptionTraitement getByCodeDispositif(String code, Dispositif dispositif) {
     */

    /**
     *
     * @param code
     * @param dispositif
     * @return
     */

    Optional<DescriptionTraitement> getByNKey(String code, Dispositif dispositif);

    /**
     *
     * @param dispositif
     * @return
     */
    List<DescriptionTraitement> getByDispositif(Dispositif dispositif);

    /**
     *
     * @param codeunique
     * @return
     */
    Optional<DescriptionTraitement> getByCodeUnique(String codeunique);

    /**
     *
     * @param traitementOrigine
     * @param dispositif
     * @return
     */
    Optional<DescriptionTraitement> getByTraitementOrigineCodeDisp(DescriptionTraitement traitementOrigine, Dispositif dispositif);

}
