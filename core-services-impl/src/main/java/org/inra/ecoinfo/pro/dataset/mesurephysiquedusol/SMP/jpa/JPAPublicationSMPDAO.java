/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SMP.jpa;

import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationSMPDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     */
    public static String QUERY_DELETE_MEASURE = "delete from MesureSMP m where m.versionfile=:version";

    /**
     *
     */
    public static String QUERY_DELETE_VALEUR = "delete MesureSMP in (select id from MesureSMP msmp where msmp.versionfile=:version)";

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
    }

}
