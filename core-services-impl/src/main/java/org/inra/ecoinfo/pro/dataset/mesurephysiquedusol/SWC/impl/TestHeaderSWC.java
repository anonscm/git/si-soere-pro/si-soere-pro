/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestHeader;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.ISWCDatatypeManager;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.impl.ISessionPropertiesMPS;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class TestHeaderSWC extends GenericTestHeader {
    
    static final long           serialVersionUID = 1L;

    /**
     *
     */
    public TestHeaderSWC() {
        super();
    }
    
    /**
     *
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @return
     * @throws BusinessException
     */
    @Override
    public long testHeaders(final CSVParser parser, final VersionFile versionFile,final ISessionPropertiesPRO sessionProperties,
            final String encoding,final BadsFormatsReport badsFormatsReport,final DatasetDescriptorPRO datasetDescriptor) 
            throws BusinessException {
        super.testHeaders(parser, versionFile, sessionProperties, encoding, badsFormatsReport, datasetDescriptor);
        final  ISessionPropertiesMPS SessionPropertiesMPS = (ISessionPropertiesMPS) sessionProperties;
        SessionPropertiesMPS.initDate();
         long lineNumber = 0;
  
         
         try {
          lineNumber = this.readDispositif(versionFile, badsFormatsReport, parser, lineNumber, sessionProperties);
            lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber,
                    ISWCDatatypeManager.CODE_DATATYPE_SWC_PRO);
            lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser,
                    lineNumber, sessionProperties);
            lineNumber = this.readCommentaire(parser, lineNumber, sessionProperties);
            lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
            lineNumber = this.jumpLines(parser, lineNumber, 1);
            lineNumber = this.readLineHeader(badsFormatsReport, parser, lineNumber,
                    datasetDescriptor, sessionProperties);
           
        } catch (final IOException e) {
            LOGGER.debug("can't read header",e);
            badsFormatsReport.addException(e);
        }
    return lineNumber;
    }
}
