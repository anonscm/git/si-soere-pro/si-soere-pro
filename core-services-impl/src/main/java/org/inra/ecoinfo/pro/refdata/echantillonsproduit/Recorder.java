/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsproduit;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.booleans.Booleans;
import org.inra.ecoinfo.pro.refdata.booleans.IBooleanDAO;
import org.inra.ecoinfo.pro.refdata.dureeconservation.IDureeConcervationDAO;
import org.inra.ecoinfo.pro.refdata.prelevementproduit.IPrelevementProduitDAO;
import org.inra.ecoinfo.pro.refdata.prelevementproduit.PrelevementProduit;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.typeechantillon.ITypeEchantillon;
import org.inra.ecoinfo.pro.refdata.typeechantillon.TypeEchantillon;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha Vivianne
 */
public class Recorder extends AbstractGenericRecorder<EchantillonsProduit> {

    private static final String PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_CODEPRELEVEMENTPROD = "PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_CODEPRELEVEMENTPROD";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_ECHANTILLONS_BAD_C_CONSERVATION = "PROPERTY_MSG_ECHANTILLONS_BAD_C_CONSERVATION";
    private static final String PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_TYPEECHANTILLON = "PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_TYPEECHANTILLON";
    private static final String PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_DUREECONSERVATION = "PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_DUREECONSERVATION";

    IEchantillonsProduitDAO echantillonsproduitDAO;

    /**
     *
     */
    protected IProduitDAO produitDAO;
    ITypeEchantillon typeechantillonDAO;
    IDureeConcervationDAO dureeconservDAO;
    IPrelevementProduitDAO prelDAO;

    /**
     *
     */
    protected IBooleanDAO boolDAO;

    private String[] listeProduitsPossibles;
    private LocalDate[] listeDatePrelevementPossibles;
    private String[] listeBooleansPossible;
    private String[] listeTypeEchantillonPossible;
    private String[] listeDureeConservPossible;
    Properties commentEn;
    String omega = "a|b";
    String mega = omega.substring(1, 2);
    String keys = "";

    /**
     *
     * @param echantillons
     * @throws BusinessException
     */
    public void createEchantillons(EchantillonsProduit echantillons) throws BusinessException {
        try {
            echantillonsproduitDAO.saveOrUpdate(echantillons);
            echantillonsproduitDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create echantillonsproduit");
        }
    }

    private void updateEchantillons(
            String statut, String homoge, EchantillonsProduit dbechantillonsproduit,
            double temperatureConservationAvantAnalyse, float broyage, double temperatureConservationApresAnalyse, String type, 
            float sechage, float tamisage,
            String conservapresanalyse, float masseavantenvoie,String commentaire) throws BusinessException {
        try {
            dbechantillonsproduit.setMasseavantenvoie(masseavantenvoie);
            dbechantillonsproduit.setTemperatureConservationAvantAnalyse(temperatureConservationAvantAnalyse);
            dbechantillonsproduit.setHomogeneisation(homoge);
            dbechantillonsproduit.setSechage(sechage);
            dbechantillonsproduit.setBroyage(broyage);
            dbechantillonsproduit.setStatut(statut);
            dbechantillonsproduit.setTypeechantillon(type);
            dbechantillonsproduit.setTemperatureConservationApresAnalyse(temperatureConservationApresAnalyse);
            dbechantillonsproduit.setTamisage(tamisage);
            dbechantillonsproduit.setConservationapresanalyse(conservapresanalyse);
            dbechantillonsproduit.setCommentaire(commentaire);

            echantillonsproduitDAO.saveOrUpdate(dbechantillonsproduit);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update echantillonsproduit");
        }
    }

    private void createOrUpdateEchantillons(EchantillonsProduit dbechantillonsproduit, String codeEchantillon, PrelevementProduit prelevementproduit,
            String statut, String homogeneisation, String codepro,
            double temperatureConservationAvantAnalyse, float broyage, double temperatureConservationApresAnalyse, String type, float sechage, float tamisage,
            String conservapresanalyse, long numechantillon, float masseavantenvoie, String commentaire) throws BusinessException {
        if (dbechantillonsproduit == null) {
            final EchantillonsProduit echantillons = new EchantillonsProduit(prelevementproduit, numechantillon, codeEchantillon, type, 
                    codepro, statut, homogeneisation, tamisage, sechage, broyage, 
                    temperatureConservationAvantAnalyse, conservapresanalyse, tamisage, masseavantenvoie, commentaire);
        } else {
            updateEchantillons(statut, homogeneisation, dbechantillonsproduit, temperatureConservationAvantAnalyse, broyage, temperatureConservationApresAnalyse, type, sechage, tamisage, conservapresanalyse, masseavantenvoie, commentaire);
        }
    }

    private void persistEchantillons(String codeEchantillon, PrelevementProduit prelevementproduit,
            String statut, String homgeneisation,
            double temperatureConservationAvantAnalyse, float broyage, double temperatureConservationApresAnalyse, 
            String type,  float sechage, float tamisage,
            String conservapresanalyse, long numechantillon, String code_unique, float masseavantenvoie, String commentaire, String codePro) throws BusinessException, BusinessException {
        final EchantillonsProduit dbechantillons = echantillonsproduitDAO.getByECH(code_unique).orElse(null);
        createOrUpdateEchantillons(dbechantillons, codeEchantillon, prelevementproduit, statut, homgeneisation, codePro, 
                temperatureConservationAvantAnalyse, broyage, temperatureConservationApresAnalyse, 
                type, sechage, tamisage, conservapresanalyse, numechantillon, masseavantenvoie, commentaire);
    }

    private void produitsPossibles() {
        List<PrelevementProduit> groupeproduits = prelDAO.getAll();
        String[] Listeproduits = new String[groupeproduits.size() + 1];
        Listeproduits[0] = "";
        int index = 1;
        for (PrelevementProduit prleproduits : groupeproduits) {
            Listeproduits[index++] = prleproduits.getCodeprelevement();
        }
        this.listeProduitsPossibles = Listeproduits;
    }

    private void listeTypeEchantillonPossibles() {
        List<TypeEchantillon> groupescode = typeechantillonDAO.getAll();
        String[] listypePossibles = new String[groupescode.size() + 1];
        listypePossibles[0] = "";
        int index = 1;
        for (TypeEchantillon codeprel : groupescode) {
            listypePossibles[index++] = codeprel.getTe_nom();
        }
        this.listeTypeEchantillonPossible = listypePossibles;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String num = tokenizerValues.nextToken();
                long numero = Long.parseLong(num);
                EchantillonsProduit dbep = echantillonsproduitDAO.getByNumEchantillon(numero)
                        .orElseThrow(() -> new BusinessException("can't find echantillonsproduit"));
                echantillonsproduitDAO.remove(dbep);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<EchantillonsProduit> getAllElements() throws BusinessException {
        return echantillonsproduitDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EchantillonsProduit echantillons) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : echantillons.getPrelevementproduit().getCodeprelevement() != null
                        ? echantillons.getPrelevementproduit().getCodeprelevement() : "",
                        listeProduitsPossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getNumeroechantillon() == 0
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getNumeroechantillon(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getCodePro()== null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getCodePro(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : echantillons.getTypeechantillon() != null
                        ? echantillons.getTypeechantillon() : "",
                        listeTypeEchantillonPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getCodelabo() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getCodelabo(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getStatut() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getStatut(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : echantillons.getHomogeneisation() != null
                        ? echantillons.getHomogeneisation() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getTamisage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getTamisage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getSechage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getSechage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getBroyage() == EMPTY_FLOAT_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getBroyage(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getTemperatureConservationAvantAnalyse()== EMPTY_DOUBLE_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getTemperatureConservationAvantAnalyse(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : echantillons.getConservationapresanalyse() != null
                        ? echantillons.getConservationapresanalyse() : "",
                        listeBooleansPossible, null, false, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getTemperatureConservationApresAnalyse()== EMPTY_DOUBLE_VALUE
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : echantillons.getTemperatureConservationApresAnalyse(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : echantillons.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(echantillons == null || echantillons.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(echantillons.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    private void listeBooleanPossibles() {
        List<Booleans> groupesbool = boolDAO.getAll();
        String[] listedesboolPossibles = new String[groupesbool.size() + 1];
        listedesboolPossibles[0] = "";
        int index = 1;
        for (Booleans booleans : groupesbool) {
            listedesboolPossibles[index++] = booleans.getLibelle();
        }
        this.listeBooleansPossible = listedesboolPossibles;
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            long line = 0;
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EchantillonsProduit.JPA_NAME_ENTITY);
                int indexpprod = tokenizerValues.currentTokenIndex();
                final String codePrelevement = tokenizerValues.nextToken();
                long numechantillon = verifieLong(tokenizerValues, line, true, errorsReport);
                final String codeEchantillonSi = tokenizerValues.nextToken();
                int indextype = tokenizerValues.currentTokenIndex();
                final String typeEchantillon = tokenizerValues.nextToken();
                String referenceEchantillonInterne = tokenizerValues.nextToken();
                String statut = tokenizerValues.nextToken();
                String homogeneite = tokenizerValues.nextToken();
                float tamisage =verifieFloat(tokenizerValues, line, false, errorsReport);
                float sechage = verifieFloat(tokenizerValues, line, false, errorsReport);
                float broyage = verifieFloat(tokenizerValues, line, false, errorsReport);
                double temperatureConservationAvantAnalyse = verifieDouble(tokenizerValues, line, false, errorsReport);
                String conservationApresAnalyse = tokenizerValues.nextToken();
                double temperatureConservationApresAnalyse = verifieDouble(tokenizerValues, line, false, errorsReport);
                float masseAvantEnvoi = verifieFloat(tokenizerValues, line, false, errorsReport);
                String commentaire = tokenizerValues.nextToken();
                TypeEchantillon dbtypeech = typeechantillonDAO.getByNkey(typeEchantillon).orElse(null);
                if (dbtypeech == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_TYPEECHANTILLON), line, indextype, typeEchantillon));
                }
                PrelevementProduit dbprelevement = prelDAO.getByNKey(codePrelevement).orElse(null);
                if (dbprelevement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_ECHANTILLONPRODUIT_BAD_CODEPRELEVEMENTPROD), line, indexpprod, codePrelevement));
                }
                String code_unique = EchantillonsProduit.buildCodeUnique(codePrelevement, numechantillon);
                final String codePro = Utils.createCodeFromString(code_unique);

                if (!errorsReport.hasErrors()) {
                    persistEchantillons(codeEchantillonSi, dbprelevement, statut, homogeneite, 
                            temperatureConservationAvantAnalyse, broyage, temperatureConservationApresAnalyse, typeEchantillon, sechage, tamisage, 
                            conservationApresAnalyse, numechantillon, code_unique, masseAvantEnvoi, commentaire, codePro);

                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param echantillonsproduitDAO
     */
    public void setEchantillonsproduitDAO(IEchantillonsProduitDAO echantillonsproduitDAO) {
        this.echantillonsproduitDAO = echantillonsproduitDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    @Override
    protected ModelGridMetadata<EchantillonsProduit> initModelGridMetadata() {
        produitsPossibles();
        listeBooleanPossibles();
        listeTypeEchantillonPossibles();
        commentEn = localizationManager.newProperties(EchantillonsProduit.JPA_NAME_ENTITY, EchantillonsProduit.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param prelDAO
     */
    public void setPrelDAO(IPrelevementProduitDAO prelDAO) {
        this.prelDAO = prelDAO;
    }

    /**
     *
     * @param boolDAO
     */
    public void setBoolDAO(IBooleanDAO boolDAO) {
        this.boolDAO = boolDAO;
    }

    /**
     *
     * @param typeechantillonDAO
     */
    public void setTypeechantillonDAO(ITypeEchantillon typeechantillonDAO) {
        this.typeechantillonDAO = typeechantillonDAO;
    }

    /**
     *
     * @param dureeconservDAO
     */
    public void setDureeconservDAO(IDureeConcervationDAO dureeconservDAO) {
        this.dureeconservDAO = dureeconservDAO;
    }

}
