package org.inra.ecoinfo.pro.refdata.matierepetapemethode;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.procedemethodeprodmp.ProcedeMethodeProdMp;

/**
 *
 * @author ptcherniati
 */
public interface IMPProcedeMethodeEtapeDAO extends IDAO<ProcedeMethodeEtapeMP> {

    /**
     *
     * @return
     */
    List<ProcedeMethodeEtapeMP> getAll();

    /**
     *
     * @param mpp
     * @param etapes
     * @param methodeetapes
     * @param ordreEtape
     * @return
     */
    Optional<ProcedeMethodeEtapeMP> getByNKey(ProcedeMethodeProdMp mpp, Etapes etapes, MethodeEtapes methodeetapes, int ordreEtape);

    /**
     *
     * @param codeProduit
     * @param codeMatierePremiere
     * @param pourcentage
     * @param intituleProcede
     * @param ordreProcede
     * @param intituleEtape
     * @param codeMethodeEtape
     * @param ordreEtape
     * @return
     */
    Optional<ProcedeMethodeEtapeMP> getByNKey(
            String codeProduit,
            String codeMatierePremiere,
            Double pourcentage,
            String intituleProcede,
            int ordreProcede,
            String intituleEtape,
            String codeMethodeEtape,
            int ordreEtape
    );
}
