package org.inra.ecoinfo.pro.refdata.structureobservatoire;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.observatoire.IObservatoireDAO;
import org.inra.ecoinfo.pro.refdata.observatoire.Observatoire;
import org.inra.ecoinfo.pro.refdata.structure.IStructureDAO;
import org.inra.ecoinfo.pro.refdata.structure.Structure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<StructureObservatoire> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IStructureDAO structureDAO;
    IObservatoireDAO observatoireDAO;
    IStructureObservatoireDAO structureObservatoireDAO;

    /**
     * @return @throws BusinessException
     */
    private String[] getNomStructurePossibles() throws BusinessException {
        List<Structure> lstStructures = structureDAO.getAll();

        List<String> lstNomStructures = new ArrayList<String>();

        lstStructures.stream().filter((structure) -> (!lstNomStructures.contains(structure.getNom()))).forEachOrdered((structure) -> {
            lstNomStructures.add(structure.getNom());
        });

        String[] nomStructurePossibles = new String[lstNomStructures.size()];
        int index = 0;
        for (String nomStructure : lstNomStructures) {
            nomStructurePossibles[index++] = nomStructure;
        }

        return nomStructurePossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getObservatoirePossibles() throws BusinessException {
        List<Observatoire> lstObservatoires = observatoireDAO.getAll();
        String[] observatoirePossibles = new String[lstObservatoires.size()];
        int index = 0;
        for (Observatoire observatoire : lstObservatoires) {
            observatoirePossibles[index++] = observatoire.getNom();
        }
        return observatoirePossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private Map<String, String[]> getPrecisionStructurePossibles() throws BusinessException {
        Map<String, String[]> precisionPossibles = new TreeMap<String, String[]>();

        String[] nomStructurePossibles = getNomStructurePossibles();
        List<String> lstNomStructures = Arrays.asList(nomStructurePossibles);
        lstNomStructures.forEach((nomStructure) -> {
            List<Structure> lstPrecisionStructures = structureDAO.getByNom(nomStructure);
            String[] precisionParNomPossibles = new String[lstPrecisionStructures.size()];
            int index = 0;
            for (Structure structure : lstPrecisionStructures) {
                precisionParNomPossibles[index++] = structure.getPrecision();
            }

            precisionPossibles.put(nomStructure, precisionParNomPossibles);
        });

        return precisionPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nomStructure = tokenizerValues.nextToken();
                String precisionStructure = tokenizerValues.nextToken();
                String nomObservatoire = tokenizerValues.nextToken();

                Structure structure = structureDAO.getByNKey(nomStructure, precisionStructure).orElse(null);
                Observatoire observatoire = observatoireDAO.getByNKey(nomObservatoire).orElse(null);

                StructureObservatoire structureObservatoire = structureObservatoireDAO.getByNKey(structure, observatoire)
                        .orElseThrow(() -> new BusinessException("can't get Structure observatoire"));

                structureObservatoireDAO.remove(structureObservatoire);

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<StructureObservatoire> getAllElements() throws BusinessException {
        return structureObservatoireDAO.getAll(StructureObservatoire.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StructureObservatoire structureObservatoire) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Nom structure
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(structureObservatoire == null ? Constantes.STRING_EMPTY : structureObservatoire.getStructure().getNom(), getNomStructurePossibles(), null, true, false, true);
        //Precision structure
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(structureObservatoire == null ? Constantes.STRING_EMPTY : structureObservatoire.getStructure().getPrecision(), getPrecisionStructurePossibles(), null, true, false, true);

        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();

        refsColonne1.add(colonne2);
        colonne1.setRefs(refsColonne1);

        colonne2.setValue(structureObservatoire == null ? "" : structureObservatoire.getStructure().getPrecision());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);

        //Nom de l'observatoire
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(structureObservatoire == null ? Constantes.STRING_EMPTY : structureObservatoire.getObservatoire().getNom(), getObservatoirePossibles(), null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexstru = tokenizerValues.currentTokenIndex();
                String nomStructure = tokenizerValues.nextToken();
                String precisionStructure = tokenizerValues.nextToken();
                int indexobser = tokenizerValues.currentTokenIndex();
                String nomObservatoire = tokenizerValues.nextToken();

                Structure structure = structureDAO.getByNKey(nomStructure, precisionStructure).orElse(null);
                if (structure == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "STRUCT_NONDEFINI"), line + 1, indexstru + 1, nomStructure + " - " + precisionStructure));
                }

                Observatoire observatoire = observatoireDAO.getByNKey(nomObservatoire).orElse(null);
                if (observatoire == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "OBSERVATOIRE_NONDEFINI"), line + 1, indexobser + 1, nomObservatoire));
                }

                StructureObservatoire structureObservatoire = new StructureObservatoire(structure, observatoire);
                StructureObservatoire dbStructureObservatoire = structureObservatoireDAO.getByNKey(structure, observatoire).orElse(null);

                if (dbStructureObservatoire == null) {
                    if (!errorsReport.hasErrors()) {
                        structureObservatoireDAO.saveOrUpdate(structureObservatoire);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param structureDAO the structureDAO to set
     */
    public void setStructureDAO(IStructureDAO structureDAO) {
        this.structureDAO = structureDAO;
    }

    /**
     * @param observatoireDAO the observatoireDAO to set
     */
    public void setObservatoireDAO(IObservatoireDAO observatoireDAO) {
        this.observatoireDAO = observatoireDAO;
    }

    /**
     * @param structureObservatoireDAO the structureObservatoireDAO to set
     */
    public void setStructureObservatoireDAO(
            IStructureObservatoireDAO structureObservatoireDAO) {
        this.structureObservatoireDAO = structureObservatoireDAO;
    }

}
