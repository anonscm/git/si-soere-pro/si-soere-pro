/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structure;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.adresse.Adresse;

/**
 * @author sophie
 *
 */
public interface IStructureDAO extends IDAO<Structure> {

    /*
     * Optional<Structure> getByNomPrecAdr(String nom, String precision, Adresse adresse);
     */

    /**
     *
     * @param adresse
     * @return
     */

    List<Structure> getByAdr(Adresse adresse);

    /**
     *
     * @param nom
     * @return
     */
    List<Structure> getByNom(String nom);

    /**
     *
     * @param nom
     * @param precision
     * @return
     */
    Optional<Structure> getByNKey(String nom, String precision);

    List<Structure> getAll();

}
