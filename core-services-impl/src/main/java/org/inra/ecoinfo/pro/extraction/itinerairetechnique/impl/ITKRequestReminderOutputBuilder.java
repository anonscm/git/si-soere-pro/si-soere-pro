/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.LoggerForExtraction;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class ITKRequestReminderOutputBuilder extends AbstractOutputBuilder{

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_CODE = "itkRequestReminder" ;

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.itk.itk-messages";

    static final String PROPERTY_MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    static final String PROPERTY_MSG_EXTRACTION_DATE = "PROPERTY_MSG_EXTRACTION_DATE";

    static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";

    static final String PROPERTY_MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";

    static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_RECOLTECOUPE = "PROPERTY_MSG_SELECTED_VARIABLES_RECOLTECOUPE";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_TRAVAILSOL = "PROPERTY_MSG_SELECTED_VARIABLES_TRAVAILSOL";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_SEMISPLANTATION = "PROPERTY_MSG_SELECTED_VARIABLES_SEMISPLANTATION";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_APPORT = "PROPERTY_MSG_SELECTED_VARIABLES_APPORT";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_PROETUDIE = "PROPERTY_MSG_SELECTED_VARIABLES_PROETUDIE";

    static final String PROPERTY_MSG_SELECTED_DISPOSITIFS = "PROPERTY_MSG_SELECTED_DISPOSITIFS";

    static final String PATTERN_STRING_SITES_SUMMARY = "   %s";

    static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";

    static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    static final String KEYMAP_COMMENTS = "comments";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Map<String, File> reminderMap = new HashMap();
        final File reminderFile = this.buildOutputFile(AbstractOutputBuilder.FILENAME_REMINDER,
                AbstractOutputBuilder.EXTENSION_TXT);
        PrintStream reminderPrintStream;
        try {
            reminderPrintStream = new PrintStream(reminderFile,
                    StandardCharsets.ISO_8859_1.displayName());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();
            final LocalDateTime date = LocalDateTime.now();
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_EXTRACTION_DATE));
            reminderPrintStream.println(String.format("%s %s", DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY), DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY_HH_MM_SS)));
            reminderPrintStream.println();
            this.printDispositifSummary(
                    (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName()),
                    reminderPrintStream);
            this.printVariablesSummary(
                    (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(
                            ITKParameterVO.RECOLTECOUPE)),
                            (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(
                                    ITKParameterVO.TRAVAILSOL)),
                                    (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(
                                            ITKParameterVO.SEMISPLANTATION)),
                                   (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(
                                            ITKParameterVO.APPORT)),
                                   (List<VariablesPRO>) requestMetadatasMap.get(VariablesPRO.class.getSimpleName().concat(
                                            ITKParameterVO.PROETUDIE)), reminderPrintStream);
            this.printDatesSummary((IntervalDate) requestMetadatasMap
                    .get(IntervalDate.class.getSimpleName()), reminderPrintStream);
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_EXTRACTION_COMMENTS));
            reminderPrintStream
            .println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_COMMENTS_SUMMARY,
                    requestMetadatasMap
                    .get(ITKRequestReminderOutputBuilder.KEYMAP_COMMENTS)));
        } catch (final FileNotFoundException | UnsupportedEncodingException e) {
            LoggerFactory.getLogger(getClass()).error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderMap.put(AbstractOutputBuilder.FILENAME_REMINDER, reminderFile);
        reminderPrintStream.flush();
        reminderPrintStream.close();
        LoggerForExtraction.logRequest((Utilisateur) policyManager.getCurrentUser(), reminderFile);
        return reminderMap;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
         return this.getLocalizationManager().getMessage(
                ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                ITKRequestReminderOutputBuilder.PROPERTY_MSG_HEADER);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters,
                        ITKRequestReminderOutputBuilder.CST_RESULT_EXTRACTION_CODE));
        return null;
    }
    
    
     void printDatesSummary(final IntervalDate datesRequestParamVO,
            final PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_PERIODS));
        try {
            datesRequestParamVO.toLocalString(reminderPrintStream, localizationManager);
        } catch (final DateTimeParseException e) {
            LoggerFactory.getLogger(getClass()).error(e.getMessage(), e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }
     
       void printDispositifSummary(final List<Dispositif> list, final PrintStream reminderPrintStream) {
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DISPOSITIFS));
        list.stream().forEach((dispositif) -> {
            reminderPrintStream.println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_SITES_SUMMARY,
                    dispositif.getName()));
        });
        reminderPrintStream.println();
    }
       
        void printVariablesSummary(final List<VariablesPRO> variablesRecolte,
            final List<VariablesPRO> variablesTravail,
            final List<VariablesPRO> variablesSemis,
            final List<VariablesPRO> variablesApport,
            final List<VariablesPRO> variablesEtudie,
            final PrintStream reminderPrintStream) {
        final Properties propertiesVariableName = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(DatatypeVariableUnitePRO.class), Nodeable.ENTITE_COLUMN_NAME);
        String localizedName;
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES));
        if (!variablesRecolte.isEmpty()) {
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_RECOLTECOUPE));
        }
        for (final VariablesPRO variable : variablesRecolte) {
            localizedName = propertiesVariableName.getProperty(variable.getName());
            reminderPrintStream.println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getName(): localizedName));
        }
        if (!variablesTravail.isEmpty()) {
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_TRAVAILSOL));
        }
        for (final VariablesPRO variable : variablesTravail) {
            localizedName = propertiesVariableName.getProperty(variable.getName());
            reminderPrintStream.println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getName(): localizedName));
        }
        if (!variablesSemis.isEmpty()) {
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_SEMISPLANTATION));
        }
        for (final VariablesPRO variable : variablesSemis) {
            localizedName = propertiesVariableName.getProperty(variable.getName());
            reminderPrintStream.println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getName() : localizedName));
        }
        if (!variablesApport.isEmpty()) {
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_APPORT));
        }
        for (final VariablesPRO variable : variablesApport) {
            localizedName = propertiesVariableName.getProperty(variable.getName());
            reminderPrintStream.println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getName() : localizedName));
        }
        if (!variablesEtudie.isEmpty()) {
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    ITKRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    ITKRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_PROETUDIE));
        }
        for (final VariablesPRO variable : variablesEtudie) {
            localizedName = propertiesVariableName.getProperty(variable.getName());
            reminderPrintStream.println(String.format(
                    ITKRequestReminderOutputBuilder.PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getName() : localizedName));
        }
        
        reminderPrintStream.println();
    }


}
