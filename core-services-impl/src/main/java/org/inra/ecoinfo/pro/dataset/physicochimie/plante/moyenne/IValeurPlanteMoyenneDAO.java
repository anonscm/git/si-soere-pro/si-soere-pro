/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;

/**
 *
 * @author adiankha
 */
public interface IValeurPlanteMoyenneDAO extends IDAO<ValeurPlanteMoyenne> {

    /**
     *
     * @param realNode
     * @param mesurePlanteMoyenne
     * @param ecartype
     * @return
     */
    Optional<ValeurPlanteMoyenne> getByNKeys(RealNode realNode, MesurePlanteMoyenne mesurePlanteMoyenne, Float ecartype);
}
