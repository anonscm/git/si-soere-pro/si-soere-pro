/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.partieprelevee;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author adiankha
 */
public class JPAPArtiePreleveeDAO extends AbstractJPADAO<PartiePrelevee> implements IPartiePreleveeDAO {

    /**
     *
     * @return
     */
    @Override
    public List<PartiePrelevee> getAll() {
        return getAll(PartiePrelevee.class);
    }

    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<PartiePrelevee> getByNKey(String code) {
        CriteriaQuery<PartiePrelevee> query = builder.createQuery(PartiePrelevee.class);
        Root<PartiePrelevee> partiePrelevee = query.from(PartiePrelevee.class);
        query
                .select(partiePrelevee)
                .where(
                        builder.equal(partiePrelevee.get(PartiePrelevee_.code_util_mycode), Utils.createCodeFromString(code))
                );
        return getOptional(query);
    }

}
