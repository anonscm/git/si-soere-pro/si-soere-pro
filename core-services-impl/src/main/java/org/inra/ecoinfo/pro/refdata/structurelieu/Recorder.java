package org.inra.ecoinfo.pro.refdata.structurelieu;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.structure.IStructureDAO;
import org.inra.ecoinfo.pro.refdata.structure.Structure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<StructureLieu> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    IStructureDAO structureDAO;
    ILieuDAO lieuDAO;
    IStructureLieuDAO structureLieuDAO;

    /**
     * @return @throws BusinessException
     */
    private String[] getLieuPossibles() throws BusinessException {
        List<Lieu> lstLieux = lieuDAO.getAll();
        String[] lieuPossibles = new String[lstLieux.size()];
        int index = 0;
        for (Lieu lieu : lstLieux) {
            if (lieu.getCommune() != null) {
                lieuPossibles[index++] = lieu.getNom() + " (" + lieu.getCommune().getNomCommune() + ")";
            } else {
                lieuPossibles[index++] = lieu.getNom();
            }
        }
        
        return lieuPossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getNomStructurePossibles() throws BusinessException {
        List<Structure> lstStructures = structureDAO.getAll();
        
        List<String> lstNomStructures = new ArrayList<String>();
        
        lstStructures.stream().filter((structure) -> (!lstNomStructures.contains(structure.getNom()))).forEachOrdered((structure) -> {
            lstNomStructures.add(structure.getNom());
        });
        
        String[] nomStructurePossibles = new String[lstNomStructures.size()];
        int index = 0;
        for (String nomStructure : lstNomStructures) {
            nomStructurePossibles[index++] = nomStructure;
        }
        
        return nomStructurePossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private Map<String, String[]> getPrecisionStructurePossibles() throws BusinessException {
        Map<String, String[]> precisionPossibles = new TreeMap<String, String[]>();
        
        String[] nomStructurePossibles = getNomStructurePossibles();
        List<String> lstNomStructures = Arrays.asList(nomStructurePossibles);
        lstNomStructures.forEach((nomStructure) -> {
            List<Structure> lstPrecisionStructures = structureDAO.getByNom(nomStructure);
            String[] precisionParNomPossibles = new String[lstPrecisionStructures.size()];
            int index = 0;
            for (Structure structure : lstPrecisionStructures) {
                precisionParNomPossibles[index++] = structure.getPrecision();
            }
            
            precisionPossibles.put(nomStructure, precisionParNomPossibles);
        });
        
        return precisionPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String nomStructure = tokenizerValues.nextToken();
                String precisionStructure = tokenizerValues.nextToken();
                String nomLieuCommune = tokenizerValues.nextToken();
                
                int index1 = nomLieuCommune.indexOf('(');
                String nomLieu = index1 != -1 ? nomLieuCommune.substring(0, index1).trim() : nomLieuCommune;
                
                Structure structure = structureDAO.getByNKey(nomStructure, precisionStructure).orElse(null);
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                
                StructureLieu structureLieu = structureLieuDAO.getByNKey(structure, lieu)
                        .orElseThrow(() -> new BusinessException("can't get structure lieu"));
                
                structureLieuDAO.remove(structureLieu);
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<StructureLieu> getAllElements() throws BusinessException {
        return structureLieuDAO.getAll(StructureLieu.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StructureLieu structureLieu) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Nom de la structure
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(structureLieu == null ? Constantes.STRING_EMPTY : structureLieu.getStructure().getNom(), getNomStructurePossibles(), null, true, false, true);
        //Précision de la structure
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(structureLieu == null ? Constantes.STRING_EMPTY : structureLieu.getStructure().getPrecision(), getPrecisionStructurePossibles(), null, true, false, true);
        
        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();
        
        refsColonne1.add(colonne2);
        colonne1.setRefs(refsColonne1);
        colonne2.setValue(structureLieu == null ? "" : structureLieu.getStructure().getPrecision());
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);

        //Nom du lieu (Commune)
        String nomLieu = structureLieu == null ? "" : structureLieu.getLieu() == null ? "" : structureLieu.getLieu().getCommune() == null ? structureLieu.getLieu().getNom() : structureLieu.getLieu().getNom() + " ("
                + structureLieu.getLieu().getCommune().getNomCommune() + ")";
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structureLieu == null ? Constantes.STRING_EMPTY : nomLieu, getLieuPossibles(), null, true, false, true));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                int indexstr = tokenizerValues.currentTokenIndex();
                String nomStructure = tokenizerValues.nextToken();
                String precisionStructure = tokenizerValues.nextToken();
                int indexcom = tokenizerValues.currentTokenIndex();
                String nomLieuCommune = tokenizerValues.nextToken();
                
                int index1 = nomLieuCommune.indexOf('(');
                String nomLieu = index1 != -1 ? nomLieuCommune.substring(0, index1).trim() : nomLieuCommune;
                
                Structure structure = structureDAO.getByNKey(nomStructure, precisionStructure).orElse(null);
                if (structure == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "STRUCT_NONDEFINI"), line + 1, indexstr + 1, nomStructure + " - " + precisionStructure));
                }
                
                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                if (lieu == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line + 1, indexcom + 1, nomLieu));
                }
                
                StructureLieu structureLieu = new StructureLieu(structure, lieu);
                StructureLieu dbStructureLieu = structureLieuDAO.getByNKey(structure, lieu).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbStructureLieu == null) {
                        structureLieuDAO.saveOrUpdate(structureLieu);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param structureDAO the structureDAO to set
     */
    public void setStructureDAO(IStructureDAO structureDAO) {
        this.structureDAO = structureDAO;
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param structureLieuDAO the structureLieuDAO to set
     */
    public void setStructureLieuDAO(IStructureLieuDAO structureLieuDAO) {
        this.structureLieuDAO = structureLieuDAO;
    }
    
}
