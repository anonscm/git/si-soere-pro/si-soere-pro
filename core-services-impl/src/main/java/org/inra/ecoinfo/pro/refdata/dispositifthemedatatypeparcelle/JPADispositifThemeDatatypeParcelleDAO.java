/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.dispositifthemedatatypeparcelle;

import com.google.common.base.Strings;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.AbstractBranchNode;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.datatype.DataType;

/**
 *
 * @author vjkoyao
 */
public class JPADispositifThemeDatatypeParcelleDAO extends AbstractJPADAO<INode> implements IDispositifThemeDatatypeParcelleDAO {

    /**
     *
     */
    public static String QUERY_ALL_DISP_THEME_DATATYPE_PARCELLE = "from DispositifThemeDatatypeParcelle dtdp";

    /**
     *
     */
    public static String QUERY_BY_NKEY_LONG = "from DispositifThemeDatatypeParcelle dtdp where dtdp.parcelleElementaire.nom =:codePE and  dtdp.dispositifThemeDataType.datatype.code = :codeDatatype and dtdp.dispositifThemeDataType.dispositiftheme.theme.code = :codeTheme and dtdp.dispositifThemeDataType.dispositiftheme.dispositif.code =:codeDispositif";

    /**
     *
     */
    public static String QUERY_BY_NKEY_LONG_WITH_NULL_PARCELLE = "from DispositifThemeDatatypeParcelle dtdp where dtdp.parcelleElementaire is null and dtdp.dispositifThemeDataType.datatype.code = :codeDatatype and dtdp.dispositifThemeDataType.dispositiftheme.theme.code = :codeTheme and dtdp.dispositifThemeDataType.dispositiftheme.dispositif.code =:codeDispositif";

    /**
     *
     */
    public static String QUERY_BY_DTD_AND_PE = "from DispositifThemeDatatypeParcelle dtdp  where dtdp.dispositifThemeDataType.dispositiftheme.dispositif.code =:codedisp and dtdp.dispositifThemeDataType.dispositiftheme.theme.name =:codetheme and dtdp.dispositifThemeDataType.datatype.code =:codedata and dtdp.parcelleElementaire.nom =:codeparcelle";

    /**
     *
     * @param codeDispositif
     * @param codeTheme
     * @param codeDatatype
     * @param codePE
     * @return
     */
    @Override
    public List<INode> getDispThemeDatatypeParcelleCodeNode(String codeDispositif, String codeTheme, String codeDatatype, String codePE) {
        CriteriaQuery<Tuple> query = builder.createTupleQuery();
        Root<NodeDataSet> dtyNode = query.from(NodeDataSet.class);
        List<Predicate> and = new LinkedList();
        Path<Nodeable> dty = dtyNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        and.add(builder.equal(dty.get(Nodeable_.code), codeDatatype));
        Join<NodeDataSet, AbstractBranchNode> theNode = dtyNode.join(NodeDataSet_.parent, JoinType.LEFT);
        Path<Nodeable> the = theNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        and.add(builder.equal(the.get(Nodeable_.code), codeTheme));
        Join<AbstractBranchNode, AbstractBranchNode> parNode = null, sitNode;
        Path<Nodeable> par, sit;
        if (Strings.isNullOrEmpty(codePE)) {
            sitNode = theNode.join(NodeDataSet_.parent, JoinType.LEFT);
            sit = sitNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        } else {
            parNode = theNode.join(NodeDataSet_.parent, JoinType.LEFT);
            par = parNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
            and.add(builder.equal(par.get(Nodeable_.code), codePE));
            sitNode = parNode.join(NodeDataSet_.parent, JoinType.LEFT);
            sit = sitNode.get(NodeDataSet_.realNode).get(RealNode_.nodeable);
        }
        and.add(builder.equal(sit.get(Nodeable_.code), codeDispositif));
        query.where(builder.and(and.toArray(new Predicate[and.size()])));
        query.multiselect(
                dtyNode,
                theNode,
                sitNode,
                parNode
        );
        Tuple result = getFirstOrNull(query);
        return result == null
                ? null
                : result.getElements().stream()
                        .map(t -> (INode) result.get(t))
                        .collect(Collectors.toList());
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getPathes() {
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<RealNode> rn = query.from(RealNode.class);
        query.select(rn.get(RealNode_.path));
        return getResultList(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<RealNode> loadRealDatatypeNodes() {
        CriteriaQuery<RealNode> query = builder.createQuery(RealNode.class);
        Root<RealNode> rn = query.from(RealNode.class);
        Join<RealNode, Nodeable> rnNodeable = rn.join(RealNode_.nodeable);
        query
                .select(rn)
                .where(builder.equal(rnNodeable.type(), builder.literal(DataType.class)))
                .orderBy(builder.asc(rn.get(RealNode_.path)));
        return getResultList(query);
    }
}
