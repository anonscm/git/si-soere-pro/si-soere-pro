/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.TS;

import java.sql.Time;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.MesureTS;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.TS.SousSequenceTS;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureTSDAO  <T> extends IDAO<MesureTS>{
    
    /**
     *
     * @param heure
     * @param sousSequenceTS
     * @return
     */
    Optional<MesureTS> getByNKeys (Time heure, SousSequenceTS sousSequenceTS);

}
