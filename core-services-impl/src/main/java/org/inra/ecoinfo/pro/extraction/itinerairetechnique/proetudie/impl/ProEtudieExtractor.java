/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.proetudie.impl;

import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.ValeurProEtudie;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKExtractor;

/**
 *
 * @author adiankha
 */
public class ProEtudieExtractor extends AbstractITKExtractor<MesureProEtudie, ValeurProEtudie> {

    /**
     *
     */
    public static final String PRO_ETUDIE = "pro_etudie";

    /**
     *
     */
    protected static final String MAP_INDEX_PRO_ETUDIE = "proetudie";

    /**
     *
     * @return
     */
    @Override
    protected String getResultExtractionCode() {
        return PRO_ETUDIE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getVariableParameter() {
        return PRO_ETUDIE;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_PRO_ETUDIE;
    }

}
