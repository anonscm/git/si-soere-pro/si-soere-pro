/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.variable;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;

/**
 *
 * @author adiankha
 */
public interface IVariablesPRODAO extends IVariableDAO {

    List<VariablesPRO> getAllVariablePRO();

    Optional<VariablesPRO> getByNames(String nom);

    Optional<VariablesPRO> betByNKey(String code);

    Optional<VariablesPRO> getByCodeUser(final String affichage);

}
