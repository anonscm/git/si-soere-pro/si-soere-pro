/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsproduit;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IEchantillonsProduitDAO extends IDAO<EchantillonsProduit> {

    /**
     *
     * @return
     */
    List<EchantillonsProduit> getAll();

    /**
     *
     * @param numeroechantillon
     * @return
     */
    Optional<EchantillonsProduit> getByCode(String numeroechantillon);

    /**
     *
     * @param code_unique
     * @return
     */
    Optional<EchantillonsProduit> getByECH(String code_unique);

    /**
     *
     * @param numero
     * @return
     */
    Optional<EchantillonsProduit> getByNumEchantillon(long numero);

}
