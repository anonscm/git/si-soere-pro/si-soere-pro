/**
 *
 */
package org.inra.ecoinfo.pro.refdata.facteur;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur;

/**
 * @author sophie
 *
 */
public class JPAFacteurDAO extends AbstractJPADAO<Facteur> implements IFacteurDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<Facteur> getAll() {
        return getAllBy(Facteur.class, Facteur::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO#getByLibelleFct(java .lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<Facteur> getByNKey(String libelle) {
        CriteriaQuery<Facteur> query = builder.createQuery(Facteur.class);
        Root<Facteur> facteur = query.from(Facteur.class);
        query
                .select(facteur)
                .where(
                        builder.equal(facteur.get(Facteur_.libelle), libelle)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO#getByCodeFct(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public Optional<Facteur> getByCodeFct(String code) {
        CriteriaQuery<Facteur> query = builder.createQuery(Facteur.class);
        Root<Facteur> facteur = query.from(Facteur.class);
        query
                .select(facteur)
                .where(
                        builder.equal(facteur.get(Facteur_.code), code)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO# getLstFacteurByIsInfosApportFert(boolean)
     */

    /**
     *
     * @param isInfosApportFert
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Facteur> getLstFacteurByIsInfosApportFert(boolean isInfosApportFert) {
        CriteriaQuery<Facteur> query = builder.createQuery(Facteur.class);
        Root<Facteur> facteur = query.from(Facteur.class);
        query
                .select(facteur)
                .where(
                        builder.equal(facteur.get(Facteur_.isInfosSupplApportFert), isInfosApportFert)
                );
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO#getLstFacteurByTPFacteur (org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur)
     */

    /**
     *
     * @param typeFacteur
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Facteur> getLstFacteurByTPFacteur(TypeFacteur typeFacteur) {
        CriteriaQuery<Facteur> query = builder.createQuery(Facteur.class);
        Root<Facteur> facteur = query.from(Facteur.class);
        Path<TypeFacteur> tf = facteur.get(Facteur_.typeFacteur);
        query
                .select(facteur)
                .where(
                        builder.equal(tf, typeFacteur)
                );
        return getResultList(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO#getLstFacteurByIsAssocNomenclature(boolean)
     */

    /**
     *
     * @param isAssocNomenclature
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Facteur> getLstFacteurByIsAssocNomenclature(boolean isAssocNomenclature) {
        CriteriaQuery<Facteur> query = builder.createQuery(Facteur.class);
        Root<Facteur> facteur = query.from(Facteur.class);
        query
                .select(facteur)
                .where(
                        builder.equal(facteur.get(Facteur_.isAssocNomenclature), isAssocNomenclature)
                );
        return getResultList(query);
    }

}
