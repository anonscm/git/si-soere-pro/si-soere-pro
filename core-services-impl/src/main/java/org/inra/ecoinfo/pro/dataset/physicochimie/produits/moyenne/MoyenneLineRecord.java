/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne;

import java.time.LocalDate;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.pro.dataset.impl.VariableValueMoy;

/**
 *
 * @author adiankha
 */
public class MoyenneLineRecord  implements Comparable<MoyenneLineRecord>{
    
    
     LocalDate dateprelevement;
     Long   originalLineNumber;
     String codeprod;
     String nomlieu;
     String nomlabo;
     int numerorepet ;
     List<VariableValueMoy> variablesValuesmoy;

    /**
     *
     * @param dateprelevement
     * @param originalLineNumber
     * @param codeprod
     * @param nomlieu
     * @param nomlabo
     * @param numerorepet
     * @param variablesValuesmoy
     */
    public MoyenneLineRecord(LocalDate dateprelevement, Long originalLineNumber,String codeprod,String nomlieu, String nomlabo, int numerorepet, List<VariableValueMoy> variablesValuesmoy) {
        this.dateprelevement = dateprelevement;
        this.originalLineNumber = originalLineNumber;
        this.codeprod = codeprod;
        this.nomlieu = nomlieu;
        this.nomlabo = nomlabo;
        this.numerorepet = numerorepet;
        this.variablesValuesmoy = variablesValuesmoy;
    }

    /**
     *
     * @param line
     */
    public void copy(MoyenneLineRecord line){
        this.dateprelevement = line.getDateprelevement();
       this.codeprod = line.getCodeprod();
       this.nomlieu = line.getNomlieu();
        this.variablesValuesmoy = line.getVariablesValuesmoy();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.numerorepet = line.getNumerorepet();
        this.nomlabo = line.getNomlabo();
        
    }
     
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
    
     @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
    
    @Override
    public int compareTo(MoyenneLineRecord o) {
       int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        }
        returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        return returnValue;
    }

    /**
     *
     * @return
     */
    public LocalDate getDateprelevement() {
        return dateprelevement;
    }

    /**
     *
     * @param dateprelevement
     */
    public void setDateprelevement(LocalDate dateprelevement) {
        this.dateprelevement = dateprelevement;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodeprod() {
        return codeprod;
    }

    /**
     *
     * @param codeprod
     */
    public void setCodeprod(String codeprod) {
        this.codeprod = codeprod;
    }

    /**
     *
     * @return
     */
    public String getNomlieu() {
        return nomlieu;
    }

    /**
     *
     * @param nomlieu
     */
    public void setNomlieu(String nomlieu) {
        this.nomlieu = nomlieu;
    }

    /**
     *
     * @return
     */
    public String getNomlabo() {
        return nomlabo;
    }

    /**
     *
     * @param nomlabo
     */
    public void setNomlabo(String nomlabo) {
        this.nomlabo = nomlabo;
    }

    /**
     *
     * @return
     */
    public int getNumerorepet() {
        return numerorepet;
    }

    /**
     *
     * @param numerorepet
     */
    public void setNumerorepet(int numerorepet) {
        this.numerorepet = numerorepet;
    }

    /**
     *
     * @return
     */
    public List<VariableValueMoy> getVariablesValuesmoy() {
        return variablesValuesmoy;
    }

    /**
     *
     * @param variablesValuesmoy
     */
    public void setVariablesValuesmoy(List<VariableValueMoy> variablesValuesmoy) {
        this.variablesValuesmoy = variablesValuesmoy;
    }

    
    
    
    
    
    
}
