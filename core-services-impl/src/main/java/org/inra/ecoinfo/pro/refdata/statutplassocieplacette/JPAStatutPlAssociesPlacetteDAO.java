package org.inra.ecoinfo.pro.refdata.statutplassocieplacette;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.statutplacette.StatutPlacette;

/**
 *
 * @author ptcherniati
 */
public class JPAStatutPlAssociesPlacetteDAO extends AbstractJPADAO<StatutPlacettesAssociesPlacette> implements IStatutPlAssociesPlacetteDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.statutplassocieplacette.IStatutPlAssociesPlacetteDAO#getByPlacetteStatutPlacette(org.inra.ecoinfo.pro.refdata.placette.Placette, org.inra.ecoinfo.pro.refdata.statutplacette.StatutPlacette)
     */

    /**
     *
     * @param placette
     * @param statutPlacette
     * @return
     */

    @Override
    public Optional<StatutPlacettesAssociesPlacette> getByNKey(Placette placette, StatutPlacette statutPlacette) {
        CriteriaQuery<StatutPlacettesAssociesPlacette> query = builder.createQuery(StatutPlacettesAssociesPlacette.class);
        Root<StatutPlacettesAssociesPlacette> statutPlacettesAssociesPlacette = query.from(StatutPlacettesAssociesPlacette.class);
        Join<StatutPlacettesAssociesPlacette, Placette> placet = statutPlacettesAssociesPlacette.join(StatutPlacettesAssociesPlacette_.placette);
        Join<StatutPlacettesAssociesPlacette, StatutPlacette> statplacet = statutPlacettesAssociesPlacette.join(StatutPlacettesAssociesPlacette_.statutPlacette);
        query
                .select(statutPlacettesAssociesPlacette)
                .where(
                        builder.equal(placet, placette),
                        builder.equal(statplacet, statutPlacette)
                );
        return getOptional(query);
    }

}
