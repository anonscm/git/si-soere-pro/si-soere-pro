/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeitineraire;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.refdata.valeurqualitative.IValeurQualitativeDAO;

/**
 *
 * @author vjkoyao
 */
public interface IListeItineraireDAO extends IValeurQualitativeDAO {

    /**
     *
     * @return
     */
    List<ListeItineraire> getAll();

    /**
     *
     * @param liste_nom
     * @param liste_valeur
     * @return
     */
    Optional<ListeItineraire> getByNKey(String liste_nom, String liste_valeur);

    /**
     *
     * @param knom
     * @param kvaleur
     * @return
     */
    Optional<ListeItineraire> getByKKey(String knom, String kvaleur);

}
