package org.inra.ecoinfo.pro.refdata.faitremarquable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

public class Recorder extends AbstractCSVMetadataRecorder<Faitremarquable> {

    IFaitRemarquableDAO faitremarquableDAO;
    private Properties fremarquableomEN;

    private void createFaitremarquable(final Faitremarquable faitremarquable) throws BusinessException {
        try {
            faitremarquableDAO.saveOrUpdate(faitremarquable);
            faitremarquableDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create faitremarquable");
        }
    }

    private void updateFaitremarquable(final String nom, String code, final Faitremarquable dbfaitremarquable) throws BusinessException {
        try {
            dbfaitremarquable.setNom(nom);
            dbfaitremarquable.setCode(code);
            faitremarquableDAO.saveOrUpdate(dbfaitremarquable);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update faitremarquable");
        }
    }

    private void createOrUpdateFaitremarquable(final String code, String nom, final Faitremarquable dbfaitremarquable) throws BusinessException {
        if (dbfaitremarquable == null) {
            final Faitremarquable faitremarquable = new Faitremarquable(code, nom);
            faitremarquable.setCode(code);
            faitremarquable.setNom(nom);
            createFaitremarquable(faitremarquable);
        } else {
            updateFaitremarquable(nom, code, dbfaitremarquable);
        }
    }

    private void persistFaitremarquable(final String code, final String nom) throws BusinessException, BusinessException {
        final Faitremarquable dbfaitremarq = faitremarquableDAO.getByNKey(nom).orElse(null);
        createOrUpdateFaitremarquable(code, nom, dbfaitremarq);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Faitremarquable fremarquable) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(fremarquable == null || fremarquable.getNom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : fremarquable.getNom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(fremarquable == null || fremarquable.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : fremarquableomEN.getProperty(fremarquable.getNom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();
                final Faitremarquable dbfr = faitremarquableDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get fait remarquable"));
                faitremarquableDAO.remove(dbfr);
                values = parser.getLine();
            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Faitremarquable> getAllElements() throws BusinessException {
        return faitremarquableDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Faitremarquable.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistFaitremarquable(code, nom);
                values = parser.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    public Properties getFremarquableomEN() {
        return fremarquableomEN;
    }

    public IFaitRemarquableDAO getFaitremarquableDAO() {
        return faitremarquableDAO;
    }

    public void setFaitremarquableDAO(IFaitRemarquableDAO faitremarquableDAO) {
        this.faitremarquableDAO = faitremarquableDAO;
    }

    @Override
    protected ModelGridMetadata<Faitremarquable> initModelGridMetadata() {

        fremarquableomEN = localizationManager.newProperties(Faitremarquable.NAME_ENTITY_JPA, Faitremarquable.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
}
