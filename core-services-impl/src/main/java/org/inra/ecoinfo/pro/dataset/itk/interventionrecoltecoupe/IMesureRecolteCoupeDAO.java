/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionrecoltecoupe.entity.MesureRecolteCoupe;

/**
 *
 * @author adiankha
 * @param <T>
 */
public interface IMesureRecolteCoupeDAO<T> extends IDAO<MesureRecolteCoupe> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesureRecolteCoupe> getByKeys(String keymesure);
}
