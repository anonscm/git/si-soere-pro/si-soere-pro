/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;

/**
 *
 * @author vjkoyao
 */
public class TestDuplicatePhysicoChimiePROMoy extends AbstractTestDuplicate {

    protected static final String BUNDLE_SOURCE_PATH_MOY_PRO = "org.inra.ecoinfo.pro.dataset.physicochimie.produit.moyenne.messages";
    static final String PROPERTY_MSG_DUPLICATE_LINE_PROMOY = "PROPERTY_MSG_DUPLICATE_LINE_PROMOY";

    SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine = null;
    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    /**
     *
     */
    public TestDuplicatePhysicoChimiePROMoy() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    /**
     *
     * @param date
     * @param codeProd
     * @param lieu
     * @param codevariable
     * @param methode
     * @param lineNumber
     */
    protected void addLine(final String date, String codeProd, final String lieu,
            final String codevariable, String methode, final long lineNumber) {
        final String key = this.getKey(date, codeProd, lieu, codevariable, methode);
        final String keySol = this.getKey(date, codeProd, codevariable, methode);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, lieu);
            } else if (!this.lineCouvert.get(keySol).equals(lieu)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicatePhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                        TestDuplicatePhysicoChimiePROMoy.PROPERTY_MSG_DUPLICATE_LINE_PROMOY), lineNumber,
                        date, codeProd, codevariable, methode, this.lineCouvert.get(keySol), codeProd));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicatePhysicoChimiePROMoy.BUNDLE_SOURCE_PATH_MOY_PRO,
                    TestDuplicatePhysicoChimiePROMoy.PROPERTY_MSG_DUPLICATE_LINE_PROMOY), lineNumber, date, codeProd, lieu, codevariable,
                    methode, this.line.get(key)));

        }
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], values[5], values[9], lineNumber);
    }

}
