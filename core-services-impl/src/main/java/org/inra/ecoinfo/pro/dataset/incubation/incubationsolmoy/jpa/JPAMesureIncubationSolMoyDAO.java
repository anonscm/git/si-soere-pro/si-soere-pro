/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy.IMesureIncubationSolMoyDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAMesureIncubationSolMoyDAO extends AbstractJPADAO<MesureIncubationSolMoy> implements IMesureIncubationSolMoyDAO<MesureIncubationSolMoy>{

    /**
     *
     * @param key
     * @return
     */
    @Override    
    public Optional<MesureIncubationSolMoy> getByKey(String key) {
        CriteriaQuery<MesureIncubationSolMoy> query = builder.createQuery(MesureIncubationSolMoy.class);
        Root<MesureIncubationSolMoy> m = query.from(MesureIncubationSolMoy.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureIncubationSolMoy_.keymesure), key)
                );
        return getOptional(query);
    }
}
