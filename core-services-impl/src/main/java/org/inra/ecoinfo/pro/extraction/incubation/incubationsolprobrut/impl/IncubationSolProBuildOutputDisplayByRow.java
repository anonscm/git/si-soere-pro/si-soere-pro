package org.inra.ecoinfo.pro.extraction.incubation.incubationsolprobrut.impl;

import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.incubation.IIncubationSolProDatatypeManager;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationOutputBuilder;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonssol.EchantillonsSol;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolProBuildOutputDisplayByRow extends AbstractIncubationOutputBuilder<MesureIncubationSolPro> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_INCUBATIONSOLPRO";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.incubation.incubation";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";

    static final String PATTERN_CSV_N_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String LIST_COLUMN_VARIABLE_INCUBATION_SOL_PRO = "LIST_COLUMN_VARIABLE_INCUBATION_SOL_PRO";

    final ComparatorVariable comparator = new ComparatorVariable();
    IVariablesPRODAO variPRODAO;

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return IncubationSolProExtractor.MAP_INDEX_INCUBATION_SOL_PRO;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IIncubationSolProDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_PRO;
    }

    /**
     *
     * @param mesure
     * @param mesuresMap
     */
    @Override
    protected void buildmap(List<MesureIncubationSolPro> mesure, SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolPro>>> mesuresMap) {

        java.util.Iterator<MesureIncubationSolPro> itMesure = mesure
                .iterator();
        while (itMesure.hasNext()) {
            MesureIncubationSolPro mesureIncubationSolPro = itMesure
                    .next();
            EchantillonsSol echan = mesureIncubationSolPro.getEchantillonsSol();
            Long siteId = echan.getPrelevementsol().getDispositif().getId();
            if (mesuresMap.get(siteId) == null) {
                mesuresMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresMap.get(siteId).get(echan) == null) {
                mesuresMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresMap.get(siteId).get(echan).put(mesureIncubationSolPro.getLocalDate_debut_incub(), mesureIncubationSolPro);
            itMesure.remove();
        }
    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedIncubationVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresMap
     */
    @Override
    protected void readMap(List<Dispositif> selectedDispositifs,
            List<VariablesPRO> selectedIncubationVariables,
            IntervalDate selectedIntervalDate,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolPro>>> mesuresMap) {

        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                IncubationSolProBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        IncubationSolProBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        IncubationSolProBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_CODE)
                .get(IncubationSolProExtractor.MAP_INDEX_INCUBATION_SOL_PRO) == null
                || ((DefaultParameter) parameters).getResults()
                        .get(IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_CODE)
                        .get(IncubationSolProExtractor.MAP_INDEX_INCUBATION_SOL_PRO).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, IncubationExtractor.CST_RESULT_EXTRACTION_INCUBATION_SOL_PRO_CODE));
        return null;
    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolPro>>> mesuresMap, IntervalDate selectedIntervalDate) {

        String currentDispositif;
        String currentEchantillon;
        int ordre_manip;
        float Masse_de_sol_sec;
        LocalDate datePrelPro;
        String currentEchantillonProCode;
        float massePro;
        String condition_incubation;
        LocalDate date_debut_incub;
        int numero_rep_analyse;
        int jour_incub;

        List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                IncubationSolProBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                IncubationSolProBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_INCUBATION_SOL_PRO).split(";"));

        for (final Dispositif dispositif : selectedDispositifs) {
            PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
            final SortedMap<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolPro>> mesureIncubSolProByDisp = mesuresMap
                    .get(dispositif.getId());

            if (mesureIncubSolProByDisp == null) {
                continue;
            }

            Iterator<Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolPro>>> itEchan = mesureIncubSolProByDisp
                    .entrySet().iterator();

            while (itEchan.hasNext()) {

                java.util.Map.Entry<EchantillonsSol, SortedMap<LocalDate, MesureIncubationSolPro>> echanEntry = itEchan.next();
                SortedMap<LocalDate, MesureIncubationSolPro> mesureEchantMap = echanEntry.getValue()
                        .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());

                for (Entry<LocalDate, MesureIncubationSolPro> entrySet : mesureEchantMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesureIncubationSolPro mesureIncubationSolPro = entrySet.getValue();
                    currentDispositif = mesureIncubationSolPro.getEchantillonsSol().getPrelevementsol().getDispositif().getNomDispo();
                    currentEchantillon = mesureIncubationSolPro.getEchantillonsSol().getCodeechsol();
                    ordre_manip = mesureIncubationSolPro.getOrdre_manip();
                    Masse_de_sol_sec = mesureIncubationSolPro.getMasse_de_sol();
                    datePrelPro = mesureIncubationSolPro.getLocalDate_prel_pro();
                    currentEchantillonProCode = mesureIncubationSolPro.getEchantillonsProduit().getCode_unique();
                    massePro = mesureIncubationSolPro.getMasse_de_pro();
                    condition_incubation = mesureIncubationSolPro.getCondition_incub();
                    date_debut_incub = mesureIncubationSolPro.getLocalDate_debut_incub();
                    numero_rep_analyse = mesureIncubationSolPro.getNumero_rep_analyse();
                    jour_incub = mesureIncubationSolPro.getJour_incub();
                    String genericPattern = LineDataFixe(date, currentDispositif, currentEchantillon, ordre_manip, Masse_de_sol_sec, datePrelPro,
                            currentEchantillonProCode, massePro, condition_incubation, date_debut_incub, numero_rep_analyse, jour_incub);
                    out.print(genericPattern);
                    LineDataVariable(mesureIncubationSolPro, out, listVariable);
                }
                itEchan.remove();
            }
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentEchantillon, int ordre_manip, float Masse_de_sol_sec, LocalDate datePrelPro,
            String currentEchantillonProCode, float massePro, String condition_incubation, LocalDate date_debut_incub, int numero_rep_analyse, int jour_incub) {
        String genericPattern = String.format(PATTERN_CSV_N_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentEchantillon,
                ordre_manip,
                Masse_de_sol_sec,
                datePrelPro,
                currentEchantillonProCode,
                massePro,
                condition_incubation,
                date_debut_incub,
                numero_rep_analyse,
                jour_incub);
        return genericPattern;
    }

    private void LineDataVariable(MesureIncubationSolPro mesureIncubationSolPro, PrintStream out, List<String> listVariable) {
        final List<ValeurIncubationSolPro> valeurIncubationSols = mesureIncubationSolPro.getValeurIncubationSolPro();
        listVariable.forEach((_item) -> {
            for (Iterator<ValeurIncubationSolPro> ValeurIterator = valeurIncubationSols.iterator(); ValeurIterator.hasNext();) {
                ValeurIncubationSolPro valeur = ValeurIterator.next();
//                if (!((DatatypeVariableUnitePRO)valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
//                    continue;
//                }
String line;
line = String.format(";%s;%s;%s;%s;%s;%s",
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
        getValeurToString(valeur),
        valeur.getStatutvaleur(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
        ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());

out.print(line);
ValeurIterator.remove();
            }
        });
        out.println();
    }

    private String getValeurToString(ValeurIncubationSolPro valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    public ComparatorVariable getComparator() {
        return comparator;
    }

    /**
     *
     * @return
     */
    public IVariablesPRODAO getVariPRODAO() {
        return variPRODAO;
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(IncubationSolProBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {

        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
