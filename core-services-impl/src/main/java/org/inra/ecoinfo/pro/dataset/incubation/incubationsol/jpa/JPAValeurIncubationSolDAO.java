/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsol.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsol.IValeurIncubationSolDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAValeurIncubationSolDAO extends AbstractJPADAO<ValeurIncubationSol> implements IValeurIncubationSolDAO {

    /**
     *
     * @param realNode
     * @param mesureIncubationSol
     * @param statutvaleur
     * @return
     */
    @Override
    public Optional<ValeurIncubationSol> getByNkeys(RealNode realNode, MesureIncubationSol mesureIncubationSol, String statutvaleur) {
        CriteriaQuery<ValeurIncubationSol> query = builder.createQuery(ValeurIncubationSol.class);
        Root<ValeurIncubationSol> v = query.from(ValeurIncubationSol.class);
        Join<ValeurIncubationSol, MesureIncubationSol> m = v.join(ValeurIncubationSol_.mesureIncubationSol);
        Join<ValeurIncubationSol, RealNode> rnVariable = v.join(ValeurIncubationSol_.realNode);
        query
                .select(v)
                .where(
                        builder.equal(rnVariable, realNode),
                        builder.equal(m, mesureIncubationSol),
                        builder.equal(v.get(ValeurIncubationSol_.statutvaleur), statutvaleur)
                );
        return getOptional(query);
    }

}
