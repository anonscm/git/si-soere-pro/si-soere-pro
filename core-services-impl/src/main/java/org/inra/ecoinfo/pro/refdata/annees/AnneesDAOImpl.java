package org.inra.ecoinfo.pro.refdata.annees;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class AnneesDAOImpl extends AbstractJPADAO<Annees> implements IAnneesDAO {

    private static final String QUERY_ANNEES_KEY = "from  Annees f where f.annee_valeur = :valeur";

    /**
     *
     * @return
     */
    @Override
    public List<Annees> getAll() {
        return getAllBy(Annees.class, Annees::getAnnee_valeur);
    }

    /**
     *
     * @param valeur
     * @return
     */
    @Override
    public Optional<Annees> getByNKey(String valeur) {
        CriteriaQuery<Annees> query = builder.createQuery(Annees.class);
        Root<Annees> annees = query.from(Annees.class);
        query
                .select(annees)
                .where(
                        builder.equal(annees.get(Annees_.annee_valeur), valeur)
                );
        return getOptional(query);
    }

}
