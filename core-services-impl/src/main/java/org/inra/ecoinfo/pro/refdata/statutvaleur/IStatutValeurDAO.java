/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.statutvaleur;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IStatutValeurDAO extends IDAO<StatutValeur> {

    /**
     *
     * @return
     */
    List<StatutValeur> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<StatutValeur> getByNKey(String nom);

}
