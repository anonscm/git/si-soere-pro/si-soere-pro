/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.SWC;

import java.sql.Time;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.MesureSWC;
import org.inra.ecoinfo.pro.dataset.mesurephysiquedusol.entity.SWC.SousSequenceSWC;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureSWCDAO  <T> extends IDAO<MesureSWC>{
    
    /**
     *
     * @param heure
     * @param sousSequenceSWC
     * @return
     */
    Optional<MesureSWC> getByNKeys (Time heure, SousSequenceSWC sousSequenceSWC);

}
