/**
 *
 */
package org.inra.ecoinfo.pro.refdata.programme;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.List;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Programme> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IProgrammeDAO programmeDAO;

    /**
     * @param acronyme
     * @param nom
     * @param anneeDebut
     * @param anneeFin
     * @param numConvention
     * @param commentFinanctOrig
     * @param dbProgramme
     * @param programme
     * @throws BusinessException
     */
    private void createOrUpdate(String acronyme, String nom, String anneeDebut, String anneeFin, String numConvention, String commentFinanctOrig, Programme dbProgramme, Programme programme) throws BusinessException {
        try {
            if (dbProgramme == null) {
                programmeDAO.saveOrUpdate(programme);
            } else {
                dbProgramme.setAcronyme(acronyme);
                dbProgramme.setNom(nom);
                dbProgramme.setDateDebut(anneeDebut);
                dbProgramme.setDateFin(anneeFin);
                dbProgramme.setNumConvention(numConvention);
                dbProgramme.setCommentaireFinanctOrig(commentFinanctOrig);

                programmeDAO.saveOrUpdate(dbProgramme);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create programme");
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String acronyme = tokenizerValues.nextToken();
                String nom = tokenizerValues.nextToken();

                programmeDAO.remove(programmeDAO.getByNKey(acronyme, nom)
                        .orElseThrow(() -> new BusinessException("can't get programme")));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Programme> getAllElements() throws BusinessException {
        return programmeDAO.getAll(Programme.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Programme programme) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Acronyme
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(programme == null ? Constantes.STRING_EMPTY : programme.getAcronyme(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        //Nom
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(programme == null ? Constantes.STRING_EMPTY : programme.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        //Date de début
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(programme == null ? Constantes.STRING_EMPTY : programme.getDateDebut(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        //Date de fin
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(programme == null ? Constantes.STRING_EMPTY : programme.getDateFin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        //Numéro convention
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(programme == null ? Constantes.STRING_EMPTY : programme.getNumConvention(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        //Commentaire
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(programme == null ? Constantes.STRING_EMPTY : programme.getCommentaireFinanctOrig(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }
    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String acronyme = tokenizerValues.nextToken();
                String nom = tokenizerValues.nextToken();
                int indexand = tokenizerValues.currentTokenIndex();
                String anneeDebut = tokenizerValues.nextToken();
                int indexanf = tokenizerValues.currentTokenIndex();
                String anneeFin = tokenizerValues.nextToken();
                String numConvention = tokenizerValues.nextToken();
                String commentFinanctOrig = tokenizerValues.nextToken();

                verifieAnnees(anneeDebut, anneeFin, nom, acronyme, line + 1, indexand + 1, indexanf + 1, errorsReport);

                Programme programme = new Programme(acronyme, nom, anneeDebut, anneeFin, numConvention, commentFinanctOrig);
                Programme dbProgramme = programmeDAO.getByNKey(acronyme, nom).orElse(null);

                if (!errorsReport.hasErrors()) {
                    createOrUpdate(acronyme, nom, anneeDebut, anneeFin, numConvention, commentFinanctOrig, dbProgramme, programme);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param anneeDebut
     * @param anneeFin
     * @param nom
     * @param acronyme
     * @param line
     * @param indexD
     * @param indexF
     * @param errorsReport
     */
    private void verifieAnnees(String anneeDebut, String anneeFin, String nom, String acronyme, long line, int indexD, int indexF, ErrorsReport errorsReport) {
        verifieAnneeFormat(anneeFin, line, indexF, errorsReport);
        verifieAnneeFormat(anneeDebut, line, indexD, errorsReport);

        if (anneeFin != null && anneeDebut != null && new Integer(anneeFin) < new Integer(anneeDebut)) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ERROR_DATE_PROG"), line, indexF, anneeFin, nom + " (" + acronyme + ")", anneeDebut));
        }
    }

    /**
     * @param annee
     * @param line
     * @param index
     * @param errorsReport
     */
    public void verifieAnneeFormat(String annee, long line, int index, ErrorsReport errorsReport) {
        if (annee != null) {
            try {
                DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, String.format("01/01/%s", annee));
            } catch (DateTimeParseException e) {
                errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERROR_DATE_LENGTH"), line, index));
            }
        }
    }

    /**
     * @param programmeDAO the programmeDAO to set
     */
    public void setProgrammeDAO(IProgrammeDAO programmeDAO) {
        this.programmeDAO = programmeDAO;
    }

}
