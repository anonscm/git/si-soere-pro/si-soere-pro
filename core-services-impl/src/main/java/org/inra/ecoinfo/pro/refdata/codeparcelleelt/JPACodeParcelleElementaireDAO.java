package org.inra.ecoinfo.pro.refdata.codeparcelleelt;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPACodeParcelleElementaireDAO extends AbstractJPADAO<CodeParcelleElementaire> implements ICodeParcelleElementaireDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.codeparcelleelt.ICodeParcelleElementaireDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<CodeParcelleElementaire> getAll() {
        return getAllBy(CodeParcelleElementaire.class, CodeParcelleElementaire::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.codeparcelleelt.ICodeParcelleElementaireDAO #getByNKey(java.lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @Override
    public Optional<CodeParcelleElementaire> getByNKey(String code) {
        CriteriaQuery<CodeParcelleElementaire> query = builder.createQuery(CodeParcelleElementaire.class);
        Root<CodeParcelleElementaire> codeParcelleElementaire = query.from(CodeParcelleElementaire.class);
        query
                .select(codeParcelleElementaire)
                .where(
                        builder.equal(codeParcelleElementaire.get(CodeParcelleElementaire_.code), code)
                );
        return getOptional(query);
    }

}
