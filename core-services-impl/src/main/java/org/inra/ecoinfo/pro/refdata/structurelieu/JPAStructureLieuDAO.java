package org.inra.ecoinfo.pro.refdata.structurelieu;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.structure.Structure;

/**
 * @author sophie
 *
 */
public class JPAStructureLieuDAO extends AbstractJPADAO<StructureLieu> implements IStructureLieuDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.structurelieu.IStructureLieuDAO# getByStructureObservatoire (org.inra.ecoinfo.pro.refdata.structure.Structure, org.inra.ecoinfo.pro.refdata.lieu.Lieu)
     */

    /**
     *
     * @param structure
     * @param lieu
     * @return
     */

    @Override
    public Optional<StructureLieu> getByNKey(Structure structure, Lieu lieu) {
        CriteriaQuery<StructureLieu> query = builder.createQuery(StructureLieu.class);
        Root<StructureLieu> structureLieu = query.from(StructureLieu.class);
        Join<StructureLieu, Lieu> l = structureLieu.join(StructureLieu_.lieu);
        Join<StructureLieu, Structure> str = structureLieu.join(StructureLieu_.structure);
        query
                .select(structureLieu)
                .where(
                        builder.equal(str, structure),
                        builder.equal(l, lieu)
                );
        return getOptional(query);
    }

}
