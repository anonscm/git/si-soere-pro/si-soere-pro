/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.statutvaleur;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<StatutValeur> {
    
    IStatutValeurDAO statutValeurDAO;
    Properties codeEn;
    Properties nomEn;
    
    ;
    
    
    private void createStatutValeur(final StatutValeur statutvaleur) throws BusinessException {
        try {
            statutValeurDAO.saveOrUpdate(statutvaleur);
            statutValeurDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create statutvaleur");
        }
    }
    
    private void createOrUpdateStatut(final String code, final String nom, final StatutValeur dbstatutValeur) throws BusinessException {
        if (dbstatutValeur == null) {
            final StatutValeur statutvaleur = new StatutValeur(code, nom);
            createStatutValeur(statutvaleur);
        }
    }
    
    private void persistStatutValeur(final String code, String nom) throws BusinessException {
        final StatutValeur dbstatutValeur = statutValeurDAO.getByNKey(nom).orElse(null);
        createOrUpdateStatut(code, nom, dbstatutValeur);
    }
    
    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                statutValeurDAO.remove(statutValeurDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get status valeur")));
                
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, StatutValeur.NAME_ENTITY_JPA);
                final String code = tokenizerValues.nextToken();
                final String nom = tokenizerValues.nextToken();
                persistStatutValeur(code, nom);
                values = csvp.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<StatutValeur> getAllElements() throws BusinessException {
        return statutValeurDAO.getAll();
    }
    
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StatutValeur statutvaleur) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statutvaleur == null || statutvaleur.getSv_code() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : statutvaleur.getSv_code(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statutvaleur == null || statutvaleur.getSv_code() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : codeEn.getProperty(statutvaleur.getSv_code()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statutvaleur == null || statutvaleur.getSv_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : statutvaleur.getSv_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(statutvaleur == null || statutvaleur.getSv_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : nomEn.getProperty(statutvaleur.getSv_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }
    
    /**
     *
     * @param statutValeurDAO
     */
    public void setStatutValeurDAO(IStatutValeurDAO statutValeurDAO) {
        this.statutValeurDAO = statutValeurDAO;
    }
    
    @Override
    protected ModelGridMetadata<StatutValeur> initModelGridMetadata() {
        codeEn = localizationManager.newProperties(StatutValeur.NAME_ENTITY_JPA, StatutValeur.JPA_COLUMN_KEY, Locale.ENGLISH);
        nomEn = localizationManager.newProperties(StatutValeur.NAME_ENTITY_JPA, StatutValeur.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }
    
}
