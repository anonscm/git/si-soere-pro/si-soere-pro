/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.impl;

import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;

/**
 *
 * @author adiankha
 */
public interface ISessionPropertiesPlanteMoyenne extends ISessionPropertiesPRO {

    /**
     *
     * @param date
     * @param time
     * @return
     */
    String dateToString(String date, String time);

}
