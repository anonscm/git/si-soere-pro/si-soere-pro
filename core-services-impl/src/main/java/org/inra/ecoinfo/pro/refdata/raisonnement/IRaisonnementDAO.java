/**
 *
 */
package org.inra.ecoinfo.pro.refdata.raisonnement;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement;

/**
 * @author sophie
 *
 */
public interface IRaisonnementDAO extends IDAO<Raisonnement> {

    /**
     *
     * @param libelle
     * @param listeRaisonnement
     * @return
     */
    Optional<Raisonnement> getByNKey(String libelle, ListeRaisonnement listeRaisonnement);

    /**
     *
     * @param listeRaisonnement
     * @return
     */
    List<Raisonnement> getByListeRaisonnement(ListeRaisonnement listeRaisonnement);

}
