/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;

/**
 *
 * @author adiankha
 */
public class TestDuplicatePlanteBrut extends AbstractTestDuplicate {

    private static final String BUNDLE_PATH_PLANTE_BRUT = "org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.messages";
    static final String PROPERTY_MSG_DUPLICATE_LINE_PLANTE_ELEMENTAIRE = "PROPERTY_MSG_DUPLICATE_LINE_PLANTE_ELEMENTAIRE";

    SortedMap<String, SortedMap<String, SortedSet<Long>>> dateTimeLine = null;
    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    /**
     *
     */
    public TestDuplicatePlanteBrut() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    /**
     *
     * @param date
     * @param codeEchan
     * @param nomLabo
     * @param repetition
     * @param codevariable
     * @param methode
     * @param lineNumber
     */
    protected void addLine(final String date, String codeEchan, final String nomLabo, String repetition,
            final String codevariable, String methode, final long lineNumber) {
        final String key = this.getKey(date, codeEchan, nomLabo, codevariable, methode);
        final String keySol = this.getKey(date, codeEchan, repetition, codevariable, methode);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, nomLabo);
            } else if (!this.lineCouvert.get(keySol).equals(nomLabo)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicatePlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                        TestDuplicatePlanteBrut.PROPERTY_MSG_DUPLICATE_LINE_PLANTE_ELEMENTAIRE), lineNumber,
                        date, codeEchan, repetition, codevariable, methode, this.lineCouvert.get(keySol), nomLabo));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicatePlanteBrut.BUNDLE_PATH_PLANTE_BRUT,
                    TestDuplicatePlanteBrut.PROPERTY_MSG_DUPLICATE_LINE_PLANTE_ELEMENTAIRE), lineNumber, date, codeEchan, nomLabo, repetition, codevariable,
                    methode, this.line.get(key)));

        }
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param dates
     * @param versionFile
     */
    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], values[4], values[5], values[8], lineNumber);
    }
}
