/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne.impl;

import com.Ostermiller.util.CSVParser;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class TestValuesPhysicoChimiePROMoy extends GenericTestValues {

    /**
     *
     */
    public TestValuesPhysicoChimiePROMoy() {
        super();
    }

    /**
     *
     * @param startline
     * @param parser
     * @param versionFile
     * @param sessionProperties
     * @param encoding
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @throws BusinessException
     */
    @Override
    public void testValues(long startline, CSVParser parser, VersionFile versionFile,
            ISessionPropertiesPRO sessionProperties, String encoding,
            BadsFormatsReport badsFormatsReport, DatasetDescriptorPRO datasetDescriptor,
            String datatypeName) throws BusinessException {
        super.testValues(startline, parser, versionFile, sessionProperties,
                encoding, badsFormatsReport, datasetDescriptor, datatypeName);
    }

    /**
     *
     * @param values
     * @param badsFormatsReport
     * @param lineNumber
     * @param index
     * @param value
     * @param column
     * @param variablesTypesDonnees
     * @param datasetDescriptor
     * @param sessionPropertiesPRO
     * @return
     */
    @Override
    protected LocalDate checkDateTypeValue(String[] values, BadsFormatsReport badsFormatsReport,
            long lineNumber,
            int index, String value,
            Column column,
            Map<String, DatatypeVariableUnitePRO> variablesTypesDonnees,
            DatasetDescriptorPRO datasetDescriptor,
            ISessionPropertiesPRO sessionPropertiesPRO) {
        final LocalDate date = super.checkDateTypeValue(values, badsFormatsReport, lineNumber, index,
                value, column, variablesTypesDonnees, datasetDescriptor, sessionPropertiesPRO);
        if (date == null) {
            return null;
        }
        try {
            String dateToString = ((ISessionPropertiesPhysicoChimiePROMoy) sessionPropertiesPRO)
                    .dateToString(values[index], "00:00:00");
            sessionPropertiesPRO.addDate(dateToString);
        } catch (final BadExpectedValueException e) {
            badsFormatsReport.addException(new BadExpectedValueException(e.getMessage()));
        } catch (final DateTimeException e) {
            badsFormatsReport
                    .addException(new BadExpectedValueException(String.format(
                            RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE),
                            value, lineNumber, index + 1, column.getName(),
                            sessionPropertiesPRO.getDateFormat())));
        }
        return date;
    }
}
