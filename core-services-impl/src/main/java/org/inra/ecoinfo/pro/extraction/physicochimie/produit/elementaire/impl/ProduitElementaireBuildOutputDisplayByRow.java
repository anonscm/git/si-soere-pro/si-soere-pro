/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.elementaire.impl;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePRODatatypeManager;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.PhysicoChimieOutputDisplayByRow;
import static org.inra.ecoinfo.pro.extraction.physicochimie.produit.elementaire.impl.ProduitElementaireBuildOutputDisplayByRow.PATTERB_CSV_12_FIELD;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class ProduitElementaireBuildOutputDisplayByRow extends PhysicoChimieOutputDisplayByRow {

    static final String PATTERN_WORD = "%s";
    static final String PATTERB_CSV_12_FIELD = "%s;%s;%s;%s;%s;%s;%s;%%s;%%s;%%s;%%s;%%s;%%s";
    static final String PATTERB_CSV_7_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.produit.messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final List<Dispositif> selectedDispositifs = (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName());
        final List<VariablesPRO> selectedProduitBrutVariables = getVariablesSelected(requestMetadatasMap, IPhysicoChimiePRODatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO);
        final IntervalDate selectedIntervalDate = (IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName());
        final List<MesurePhysicoChimiePRO> mesuresProduitBrut = getMesures(resultsDatasMap, ProduitBrutExtractor.MAP_INDEX_PRODUITBRUT);
        final Set<String> dispositifsNames = buildListOfDispositifsForDatatype(selectedDispositifs, getDatatype());
        final Map<String, File> filesMap = this.buildOutputsFiles(dispositifsNames, AbstractOutputBuilder.EXTENSION_CSV);
        final Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        outputPrintStreamMap.entrySet().forEach((datatypeNamEntry) -> {
            outputPrintStreamMap.get(datatypeNamEntry.getKey()).println(headers);
        });
        final SortedMap<Long, SortedMap<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>>> mesuresProduitBrutsMap = new TreeMap();

        try {
            this.buildmap(mesuresProduitBrut, mesuresProduitBrutsMap);
        } catch (DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug("can't parse date", e);
        }
        this.readMap(selectedDispositifs, selectedProduitBrutVariables, selectedIntervalDate,
                outputPrintStreamMap, mesuresProduitBrutsMap);
        this.closeStreams(outputPrintStreamMap);
        return filesMap;
    }
    

    private void buildmap(
            final List<MesurePhysicoChimiePRO> mesuresProduitBruts,
            final SortedMap<Long, SortedMap<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>>> mesuresProduitBrutMap) {
        java.util.Iterator<MesurePhysicoChimiePRO> itMesure = mesuresProduitBruts
                .iterator();
        while (itMesure.hasNext()) {
            MesurePhysicoChimiePRO mesureProduitBrut = itMesure
                    .next();
            EchantillonsProduit echan = mesureProduitBrut.getEchantillonpro();
            Long siteId = echan.getPrelevementproduit().getTraitement().getDispositif().getId();
            if (mesuresProduitBrutMap.get(siteId) == null) {
                mesuresProduitBrutMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresProduitBrutMap.get(siteId).get(echan) == null) {
                mesuresProduitBrutMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresProduitBrutMap.get(siteId).get(echan).put(mesureProduitBrut.getDatePrelevement(), mesureProduitBrut);
            itMesure.remove();
        }
    }

    private void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitBrutVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>>> mesuresProduitBrutsMap) {
        try {
            BuildDataLine(selectedDispositifs, selectedProduitBrutVariables, outputPrintStreamMap, mesuresProduitBrutsMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private void BuildDataLine(final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedProduitBrutVariables,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>>> mesuresProduitBrutsMap,
            final IntervalDate selectedIntervalDate) {
        PrintStream out;
        String currentDispositif;
        String currentEchan;
        String currentPro;
        String currentParcelle;
        String currentTraitement;
        int numero;
        String currentlabo;
        for (final Dispositif dispositif : selectedDispositifs) {
            out = outputPrintStreamMap.get(getDispositifDatatype(dispositif, getDatatype()));
            final SortedMap<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>> mesureProduitBrutsMapByDisp = mesuresProduitBrutsMap
                    .get(dispositif.getId());
            if (mesureProduitBrutsMapByDisp == null) {
                continue;
            }
            Iterator<Entry<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>>> itEchan = mesureProduitBrutsMapByDisp
                    .entrySet().iterator();
            while (itEchan.hasNext()) {
                java.util.Map.Entry<EchantillonsProduit, SortedMap<LocalDate, MesurePhysicoChimiePRO>> echanEntry = itEchan
                        .next();
                EchantillonsProduit echantillon = echanEntry.getKey();
                SortedMap<LocalDate, MesurePhysicoChimiePRO> mesureProduitBrutsMap = echanEntry.getValue()
                        .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                for (Entry<LocalDate, MesurePhysicoChimiePRO> entrySet : mesureProduitBrutsMap.entrySet()) {
                    LocalDate date = entrySet.getKey();
                    MesurePhysicoChimiePRO mesureProduitBrut = entrySet.getValue();
                    currentDispositif = echantillon.getPrelevementproduit().getTraitement().getDispositif().getName();
                    currentEchan = echantillon.getCode_unique() != null ? echantillon.getCode_unique()
                            : org.apache.commons.lang.StringUtils.EMPTY;
                    currentPro = echantillon.getPrelevementproduit().getProduitdispositif().getProduits() != null ? echantillon.getPrelevementproduit()
                            .getProduitdispositif().getProduits().getProd_key()
                            : org.apache.commons.lang.StringUtils.EMPTY;
                    currentParcelle = echantillon.getPrelevementproduit().getPelementaire().getCodeParcelleElementaire().getCode();
                    currentTraitement = mesureProduitBrut.getEchantillonpro().getPrelevementproduit().getTraitement().getCode();
                    numero = mesureProduitBrut.getNumero_repetition();
                    currentlabo = String.format(
                            ProduitElementaireBuildOutputDisplayByRow.PATTERN_WORD,
                            mesureProduitBrut.getNom_laboratoire());

                    String genericPattern = LineDataFixe(date,
                            currentEchan,
                            currentPro, currentTraitement,
                            currentParcelle,
                            numero,
                            currentlabo);
                    LineDataVariable(mesureProduitBrut, genericPattern, out, selectedProduitBrutVariables);
                }
                itEchan.remove();
            }
        }
    }

    private void LineDataVariable(MesurePhysicoChimiePRO mesureProduitBrut, String genericPattern, PrintStream out, final List<VariablesPRO> selectedProduitBrutVariables) {
        selectedProduitBrutVariables.forEach((variablesPRO) -> {
            for (Iterator<ValeurPhysicoChimiePRO> ValeurIterator = mesureProduitBrut.getValeurPhysicoChimiePRO().iterator(); ValeurIterator.hasNext();) {
                ValeurPhysicoChimiePRO valeur = ValeurIterator.next();
                if (((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getId().equals(variablesPRO.getId())) {
                    String line = String.format(genericPattern,
                            valeur.getValeur(),
                            valeur.getStatutvaleur(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getVariablespro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getMethode().getMethode_code(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getUnitepro().getCode(),
                            ((DatatypeVariableUnitePRO) valeur.getRealNode().getNodeable()).getHumiditeexpression().getCode());
                    out.println(line);
                    ValeurIterator.remove();
                }
            }
        });
    }

    private String LineDataFixe(LocalDate date, String currentEchan, String currentPro, String currentTraitement, String currentParcelle, int numero, String currentlabo) {
        String genericPattern = String.format(PATTERB_CSV_12_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentEchan,
                currentPro, currentTraitement,
                currentParcelle,
                numero,
                currentlabo);
        return genericPattern;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                ProduitElementaireBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        ProduitElementaireBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        ProduitElementaireBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (CollectionUtils.isEmpty(((DefaultParameter) parameters).getResults()
                .get(ProduitBrutExtractor.CST_RESULT_EXTRACTION_PRODUITBRUT_CODE)
                .getOrDefault(ProduitBrutExtractor.MAP_INDEX_PRODUITBRUT,
                        ((DefaultParameter) parameters).getResults()
                .get(ProduitBrutExtractor.CST_RESULT_EXTRACTION_PRODUITBRUT_CODE)
                .get(ProduitBrutExtractor.MAP_INDEX_0)))) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ProduitBrutExtractor.CST_RESULT_EXTRACTION_PRODUITBRUT_CODE));
        return null;
    }

    static class ErrorsReport {

        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;

        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(ProduitElementaireBuildOutputDisplayByRow.CST_NEW_LINE);
        }

        public String getErrorsMessages() {
            return this.errorsMessages;
        }

        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    private String getDatatype() {
        return "pro_physico-chimie_donnees_elementaires";
    }

}
