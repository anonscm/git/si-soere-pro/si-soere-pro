/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolbrut.impl;

import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.extraction.incubation.impl.AbstractIncubationExtractor;
import org.inra.ecoinfo.pro.extraction.incubation.impl.IncubationParameterVO;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolExtractor extends AbstractIncubationExtractor<MesureIncubationSol>{
    
    public static final String   INCUBATION_SOL                    = "incubation_sol" ;

  
    protected static final String MAP_INDEX_INCUBATION_SOL              = "incubationsol";

   
    protected static final String MAP_INDEX_0                       = "0";

    
    protected static final String PROPERTY_MSG_NO_EXTRACTION_RESULT = "PROPERTY_MSG_NO_EXTRACTION_RESULT";

    @Override
    protected String getResultExtractionCode() {
        return INCUBATION_SOL;
    }

    @Override
    protected String getVariableParameter() {
       return IncubationParameterVO.INCUBATIONSOL;
    }

    @Override
    protected String getResultMapIndex() {
        return MAP_INDEX_INCUBATION_SOL;
    }
}
