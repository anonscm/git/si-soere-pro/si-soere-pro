/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.materiel;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.typeintervention.TypeIntervention;

/**
 *
 * @author vjkoyao
 */
public class JPAMaterielDAO extends AbstractJPADAO<Materiel> implements IMaterielDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Materiel> getAll() {
        return getAll(Materiel.class);
    }

    /**
     *
     * @param materiel_nom
     * @param typeIntervention
     * @return
     */
    @Override
    public Optional<Materiel> getByNKey(String materiel_nom, TypeIntervention typeIntervention) {
        CriteriaQuery<Materiel> query = builder.createQuery(Materiel.class);
        Root<Materiel> materiel = query.from(Materiel.class);
        Join<Materiel, TypeIntervention> ti = materiel.join(Materiel_.typeIntervention);
        query
                .select(materiel)
                .where(
                        builder.equal(ti, typeIntervention),
                        builder.equal(materiel.get(Materiel_.materiel_nom), materiel_nom)
                );
        return getOptional(query);
    }

}
