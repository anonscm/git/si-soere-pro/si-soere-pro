/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionapport;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureApportDAO<T> extends IDAO<MesureApport> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesureApport> getByKeys(String keymesure);

}
