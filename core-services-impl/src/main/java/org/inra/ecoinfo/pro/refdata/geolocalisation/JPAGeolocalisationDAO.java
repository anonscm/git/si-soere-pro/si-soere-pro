/**
 *
 */
package org.inra.ecoinfo.pro.refdata.geolocalisation;

import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.systemeprojection.SystemeProjection_;

/**
 * @author sophie
 *
 */
public class JPAGeolocalisationDAO extends AbstractJPADAO<Geolocalisation> implements IGeolocalisationDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.geolocalisation.IGeolocalisationDAO# getBySystProjectLatitudeLongitude(java.lang.String, java.lang.String, java.lang.String)
     */

    /**
     *
     * @param systemeProjection
     * @param latitude
     * @param longitude
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Geolocalisation> getBySystProjectLatitudeLongitude(String systemeProjection, String latitude, String longitude) {
        CriteriaQuery<Geolocalisation> query = builder.createQuery(Geolocalisation.class);
        Root<Geolocalisation> geolocalisation = query.from(Geolocalisation.class);
        query
                .select(geolocalisation)
                .where(
                        builder.equal(geolocalisation.get(Geolocalisation_.systemeProjection).get(SystemeProjection_.nom), systemeProjection),
                        builder.equal(geolocalisation.get(Geolocalisation_.latitude), latitude),
                        builder.equal(geolocalisation.get(Geolocalisation_.longitude), longitude)
                );
        return getResultList(query);
    }

}
