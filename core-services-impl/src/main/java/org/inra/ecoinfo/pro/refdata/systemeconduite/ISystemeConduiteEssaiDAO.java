/**
 *
 */
package org.inra.ecoinfo.pro.refdata.systemeconduite;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ISystemeConduiteEssaiDAO extends IDAO<SystemeConduiteEssai> {

    List<SystemeConduiteEssai> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<SystemeConduiteEssai> getByNKey(String libelle);

}
