/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.precisionfiliere;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<Precisionfiliere> {

    IPrecisionFiliereDAO precisionfiliereDAO;
    Properties NonprecisionfiliereEN;

    /**
     *
     * @param precisionfiliere
     * @throws BusinessException
     */
    public void createPrecisionfiliere(Precisionfiliere precisionfiliere) throws BusinessException {
        try {
            precisionfiliereDAO.saveOrUpdate(precisionfiliere);
            precisionfiliereDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create precisionfiliere");
        }
    }

    private void updatePrecisionfiliere(final String nom, final Precisionfiliere dbprecisionfiliere) throws BusinessException {
        try {
            dbprecisionfiliere.setPfiliere_nom(nom);
            precisionfiliereDAO.saveOrUpdate(dbprecisionfiliere);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update precisionfiliere");
        }

    }

    private void createOrUpdatePrecisionfiliere(final String code, String nom, final Precisionfiliere dbprecisionfiliere) throws BusinessException {
        if (dbprecisionfiliere == null) {
            final Precisionfiliere precisionfiliere = new Precisionfiliere(nom);
            precisionfiliere.setPfiliere_code(code);
            precisionfiliere.setPfiliere_nom(nom);

            createPrecisionfiliere(precisionfiliere);
        } else {
            updatePrecisionfiliere(nom, dbprecisionfiliere);
        }
    }

    private void persistPrecisionfiliere(final String code, final String nom) throws BusinessException, BusinessException {
        final Precisionfiliere dbprecisionfiliere = precisionfiliereDAO.getByNKey(nom).orElse(null);
        createOrUpdatePrecisionfiliere(code, nom, dbprecisionfiliere);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();

                final Precisionfiliere dbprecisionfiliere = precisionfiliereDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get precisionfiliere"));
                precisionfiliereDAO.remove(dbprecisionfiliere);
                values = csvp.getLine();

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Precisionfiliere> getAllElements() throws BusinessException {
        return precisionfiliereDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String string) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Precisionfiliere.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistPrecisionfiliere(code, nom);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Precisionfiliere precisionfiliere) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(precisionfiliere == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : precisionfiliere.getPfiliere_nom(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(precisionfiliere == null || precisionfiliere.getPfiliere_nom() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : NonprecisionfiliereEN.getProperty(precisionfiliere.getPfiliere_nom()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<Precisionfiliere> initModelGridMetadata() {
        NonprecisionfiliereEN = localizationManager.newProperties(Precisionfiliere.NAME_ENTITY_JPA, Precisionfiliere.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @return
     */
    public IPrecisionFiliereDAO getPrecisionfiliereDAO() {
        return precisionfiliereDAO;
    }

    /**
     *
     * @param precisionfiliereDAO
     */
    public void setPrecisionfiliereDAO(IPrecisionFiliereDAO precisionfiliereDAO) {
        this.precisionfiliereDAO = precisionfiliereDAO;
    }

}
