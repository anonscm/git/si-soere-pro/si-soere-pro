package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.ValeurPlanteMoyenne;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.IMesurePlanteMoyenneDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.IValeurPlanteMoyenneDAO;
import org.inra.ecoinfo.pro.refdata.cultures.Cultures;
import org.inra.ecoinfo.pro.refdata.cultures.ICulturesDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.IDescriptionTraitementDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.partieprelevee.IPartiePreleveeDAO;
import org.inra.ecoinfo.pro.refdata.partieprelevee.PartiePrelevee;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author adiankha
 */
public class ProcessRecordPlanteMoyenne extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPlanteMoyenne.class);
    private static final String BUNDLE_PATH_PLANTE_MOY = "org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne.messages";
    private static final String MSG_ERROR_DVUMU_PLANTEMOY_NOT_FOUND_IN_DB = "MSG_ERROR_DVUMU_PLANTEMOY_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_PLANTEMOY_NOT_VARIABLEPRO_DB = "MSG_ERROR_PLANTEMOY_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_PLANTEMOY_NOT_FOUND_METHODE_DB = "MSG_ERROR_PLANTEMOY_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_PLANTEMOY_NOT_UNITEPRO_DB = "MSG_ERROR_PLANTEMOY_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_PLANTEMOY_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_PLANTEMOY_NOT_FOUND_HUMIDITE_DB";
    private static final String MSG_ERROR_TRAITEMENT_PLANTEMOY_NOT_FOUND_IN_DB = "MSG_ERROR_TRAITEMENT_PLANTEMOY_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_CULTURES_PLANTEMOY_NOT_FOUND_IN_DB = "MSG_ERROR_CULTURES_PLANTEMOY_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_PARTIEPRELEVEE_PLANTEMOY_NOT_FOUND_IN_DB = "MSG_ERROR_PARTIEPRELEVEE_PLANTEMOY_NOT_FOUND_IN_DB";
    protected IMesurePlanteMoyenneDAO<MesurePlanteMoyenne> mesurePlanteMoyenneDAO;
    protected IProduitDAO produitDAO;
    IDispositifDAO dispositifDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IDescriptionTraitementDAO descriptionTraitementDAO;
    ICulturesDAO culturesDAO;
    IPartiePreleveeDAO partiePreleveeDAO;
    IValeurPlanteMoyenneDAO valeurPlanteMoyenneDAO;

    public ProcessRecordPlanteMoyenne() {
        super();
    }

    void buildMesure(final PlanteMoyLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<PlanteMoyLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException {

        LocalDate dateprelevement = mesureLines.getDateprelevement();
        String codetrait = Utils.createCodeFromString(mesureLines.getCodeTraitement());
        String codedisp = Utils.createCodeFromString(sessionPropertiesPRO.getDispositif().getCode());
        String lieu = Utils.createCodeFromString(sessionPropertiesPRO.getDispositif().getLieu().getNom());
        String culture = mesureLines.getCodeCulture();
        String partie = mesureLines.getCodePartie();
        double hauteur = mesureLines.getHauteur();
        String nomlabo = mesureLines.getNomlabo();
        int numerorepet = mesureLines.getNumerorepet();
        Long fichier = mesureLines.getOriginalLineNumber();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);

        Cultures culturesBD = culturesDAO.getByNKey(Utils.createCodeFromString(culture)).orElse(null);
        if (culturesBD == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_CULTURES_PLANTEMOY_NOT_FOUND_IN_DB), culture));
        }

        PartiePrelevee partieBD = partiePreleveeDAO.getByNKey(Utils.createCodeFromString(partie)).orElse(null);
        if (partieBD == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_PARTIEPRELEVEE_PLANTEMOY_NOT_FOUND_IN_DB), partie));
        }

        String codeunique = codedisp + "_" + codetrait;

        DescriptionTraitement destrait = descriptionTraitementDAO.getByCodeUnique(codeunique).orElse(null);
        if (destrait == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_TRAITEMENT_PLANTEMOY_NOT_FOUND_IN_DB), codeunique));
        }

        String variable = mesureLines.getCodevariable();
        float moyenne = mesureLines.getMoyenne();
        float ecarttype = mesureLines.getEcarttype();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();
        String humidite = mesureLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);
        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cunite = Utils.createCodeFromString(unite);
        String cmethode = Utils.createCodeFromString(methode);
        String chumitite = Utils.createCodeFromString(humidite);
        String cvariable = Utils.createCodeFromString(variable);

        VariablesPRO dbvariable = variPRODAO.getByCodeUser(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_PLANTEMOY_NOT_VARIABLEPRO_DB), cvariable));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_PLANTEMOY_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_PLANTEMOY_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
//        HumiditeExpression dbhumidite2 = humiditeDAO.getOptionalByName(chumitite).orElse(null);
//        HumiditeExpression dbhumidite3 = humiditeDAO.getOptionalByName(chumitite).orElseThrow(PersistenceException::new);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_PLANTEMOY_NOT_FOUND_HUMIDITE_DB), chumitite));
        }
        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(RecorderPRO.getDatatypeFromVersion(versionFile), dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPlanteMoyenne.BUNDLE_PATH_PLANTE_MOY,
                    ProcessRecordPlanteMoyenne.MSG_ERROR_DVUMU_PLANTEMOY_NOT_FOUND_IN_DB), cdatatype, cvariable, cunite, cmethode, chumitite));

        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesurePlanteMoyenne mesurePlanteMoy = getOrCreate(dateprelevement, numerorepet, codetrait, nomlabo, culture, partie, destrait, culturesBD, partieBD, hauteur, versionFile, fichier, versionFileDB);
            persitValeurMoyenne(dbdvumRealNode, mesurePlanteMoy, moyenne, ecarttype);
        }
    }

    private MesurePlanteMoyenne getOrCreate(LocalDate dateprelevement, int numerorepet, String codetrait, String nomlabo, String culture, String partie, DescriptionTraitement destrait, Cultures culturesBD, PartiePrelevee partieBD, double hauteur, final VersionFile versionFile, Long fichier, final VersionFile versionFileDB) throws PersistenceException {
        String dateString = null;
        String format = "dd/MMM/yyyy";
        dateString = DateUtil.getUTCDateTextFromLocalDateTime(dateprelevement, format);
        String intString1 = null;

        intString1 = Integer.toString(numerorepet);
        String culture_code = Utils.createCodeFromString(culture);
        String key = dateString + "_" + codetrait + "_" + nomlabo + "_" + culture_code + "_" + partie + "_" + intString1;
        MesurePlanteMoyenne mesurePlanteMoyenne = mesurePlanteMoyenneDAO.getByKeys(key).orElse(null);
        if (mesurePlanteMoyenne == null) {
            mesurePlanteMoyenne = new MesurePlanteMoyenne(fichier, dateprelevement, destrait, culturesBD, partieBD, hauteur, nomlabo, numerorepet, versionFile, fichier);
            mesurePlanteMoyenne.setDatePrelevement(dateprelevement);
            mesurePlanteMoyenne.setDescriptionTraitement(destrait);
            mesurePlanteMoyenne.setLigneFichierEchange(fichier);
            mesurePlanteMoyenne.setNomlabo(nomlabo);
            mesurePlanteMoyenne.setRepetition(numerorepet);
            mesurePlanteMoyenne.setVersionfile(versionFileDB);
            mesurePlanteMoyenne.setHauteurcoup(hauteur);
            mesurePlanteMoyenne.setKeymesure(key);
            mesurePlanteMoyenneDAO.saveOrUpdate(mesurePlanteMoyenne);
        }
        return mesurePlanteMoyenne;
    }

    private void create(ValeurPlanteMoyenne valeurPlanteMoyenne) throws PersistenceException {
        valeurPlanteMoyenneDAO.saveOrUpdate(valeurPlanteMoyenne);
    }

    void createPlanteMoyenne(RealNode dvuRealNode, MesurePlanteMoyenne mpm, float moyenne, float ecarttype, ValeurPlanteMoyenne dbcpm) throws PersistenceException {
        if (dbcpm == null) {
            ValeurPlanteMoyenne valeurPlanteMoyenne = new ValeurPlanteMoyenne(moyenne, ecarttype, mpm, dvuRealNode);
            valeurPlanteMoyenne.setValeur(moyenne);
            valeurPlanteMoyenne.setEcarttype(ecarttype);
            valeurPlanteMoyenne.setMesurePlanteMoyenne(mpm);
            valeurPlanteMoyenne.setRealNode(dvuRealNode);
            create(valeurPlanteMoyenne);
        }
    }

    void persitValeurMoyenne(RealNode dvuRealNode, MesurePlanteMoyenne mpm, float moyenne, float ecarttype) throws PersistenceException {
        ValeurPlanteMoyenne valeurPlanteMoyenne = valeurPlanteMoyenneDAO.getByNKeys(dvuRealNode, mpm, ecarttype).orElse(null);
        createPlanteMoyenne(dvuRealNode, mpm, moyenne, ecarttype, valeurPlanteMoyenne);
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<PlanteMoyLineRecord>> lines,
            final SortedSet<PlanteMoyLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<PlanteMoyLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<PlanteMoyLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (PlanteMoyLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, ISessionPropertiesPRO sessionProperties, String fileEncoding,
            DatasetDescriptorPRO datasetDescriptor) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptor);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptor);
            final Map<LocalDate, List<PlanteMoyLineRecord>> mesuresMapLines = new HashMap();
            final long lineCount = datasetDescriptor.getEnTete();
            this.readLines(parser, dbVariables, mesuresMapLines, lineCount, errorsReport);
            final SortedSet<PlanteMoyLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LOGGER.error(null, ex);
            throw new BusinessException(ex);
        }
    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    private long readLines(final CSVParser parser, final List<DatatypeVariableUnitePRO> dbVariables, final Map<LocalDate, List<PlanteMoyLineRecord>> lines,
            long lineCount, ErrorsReport errorsReport) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final LocalDate dateprelevement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codetrait = cleanerValues.nextToken();
            final String codecul = cleanerValues.nextToken();
            final String partie = cleanerValues.nextToken();
            final String hauteur = cleanerValues.nextToken();
            double haut = Double.parseDouble(hauteur);
            final String nomlabo = cleanerValues.nextToken();
            final int numerorepe = Integer.parseInt(cleanerValues.nextToken());
            final String codevpro = cleanerValues.nextToken();
            final float valeurpro = Float.parseFloat(cleanerValues.nextToken());
            final float ecart = Float.parseFloat(cleanerValues.nextToken());
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();
            final String codehumidite = cleanerValues.nextToken();

            final PlanteMoyLineRecord line = new PlanteMoyLineRecord(dateprelevement, lineCount, codetrait, codecul, partie, haut, nomlabo, numerorepe, codevpro, codeunite, codemethode, codehumidite, valeurpro, ecart);
            try {
                if (!lines.containsKey(dateprelevement)) {
                    lines.put(dateprelevement, new LinkedList<PlanteMoyLineRecord>());
                }
                lines.get(dateprelevement).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        dateprelevement, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesurePlanteMoyenneDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;
    }

    public IMesurePlanteMoyenneDAO<MesurePlanteMoyenne> getMesurePlanteMoyenneDAO() {
        return mesurePlanteMoyenneDAO;
    }

    public void setMesurePlanteMoyenneDAO(IMesurePlanteMoyenneDAO<MesurePlanteMoyenne> mesurePlanteMoyenneDAO) {
        this.mesurePlanteMoyenneDAO = mesurePlanteMoyenneDAO;
    }

    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    public IMethodeDAO getMethodeDAO() {
        return methodeDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public IUniteproDAO getUniteproDAO() {
        return uniteproDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public IHumiditeExpressionDAO getHumiditeDAO() {
        return humiditeDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public void setDescriptionTraitementDAO(IDescriptionTraitementDAO descriptionTraitementDAO) {
        this.descriptionTraitementDAO = descriptionTraitementDAO;
    }

    public void setCulturesDAO(ICulturesDAO culturesDAO) {
        this.culturesDAO = culturesDAO;
    }

    public void setPartiePreleveeDAO(IPartiePreleveeDAO partiePreleveeDAO) {
        this.partiePreleveeDAO = partiePreleveeDAO;
    }

    public IDataTypeVariableQualifiantDAO getDataTypeVariableUnitePRODAO() {
        return dataTypeVariableUnitePRODAO;
    }

    public IDescriptionTraitementDAO getDescriptionTraitementDAO() {
        return descriptionTraitementDAO;
    }

    public ICulturesDAO getCulturesDAO() {
        return culturesDAO;
    }

    public IPartiePreleveeDAO getPartiePreleveeDAO() {
        return partiePreleveeDAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setValeurPlanteMoyenneDAO(IValeurPlanteMoyenneDAO valeurPlanteMoyenneDAO) {
        this.valeurPlanteMoyenneDAO = valeurPlanteMoyenneDAO;
    }

}
