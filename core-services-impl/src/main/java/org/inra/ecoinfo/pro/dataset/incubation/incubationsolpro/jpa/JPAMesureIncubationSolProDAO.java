/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro_;
import org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.IMesureIncubationSolProDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAMesureIncubationSolProDAO extends AbstractJPADAO<MesureIncubationSolPro> implements IMesureIncubationSolProDAO<MesureIncubationSolPro>{

    /**
     *
     * @param key
     * @return
     */
    @Override
    public Optional<MesureIncubationSolPro> getByKeys(String key) {
        CriteriaQuery<MesureIncubationSolPro> query = builder.createQuery(MesureIncubationSolPro.class);
        Root<MesureIncubationSolPro> m = query.from(MesureIncubationSolPro.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureIncubationSolPro_.keymesure), key)
                );
        return getOptional(query);
    }
    
}