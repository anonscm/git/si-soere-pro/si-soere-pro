package org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;

/**
 *
 * @author ptcherniati
 */
public interface ITeneurCalcaireInitialDAO extends IDAO<Teneurcalcaireinitial> {

    /**
     *
     * @return
     */
    List<Teneurcalcaireinitial> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Teneurcalcaireinitial> getByNKey(String nom, Texturesol texturesol);
}
