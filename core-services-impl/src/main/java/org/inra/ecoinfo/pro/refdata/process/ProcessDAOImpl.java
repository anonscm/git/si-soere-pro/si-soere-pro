package org.inra.ecoinfo.pro.refdata.process;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class ProcessDAOImpl extends AbstractJPADAO<Process> implements IProcessDAO {

    @Override
    public List<Process> getAll() {
        return getAll(Process.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Optional<Process> getByNKey(String process_intitule) {
        CriteriaQuery<Process> query = builder.createQuery(Process.class);
        Root<Process> process = query.from(Process.class);
        query
                .select(process)
                .where(
                        builder.equal(process.get(Process_.process_code), Utils.createCodeFromString(process_intitule))
                );
        return getOptional(query);
    }
}
