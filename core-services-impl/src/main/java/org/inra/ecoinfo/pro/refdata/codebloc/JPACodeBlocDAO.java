package org.inra.ecoinfo.pro.refdata.codebloc;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPACodeBlocDAO extends AbstractJPADAO<CodeBloc> implements ICodeBlocDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.codebloc.ICodeBlocDAO#getAll()
     */

    /**
     *
     * @return
     */

    @Override
    public List<CodeBloc> getAll() {
        return getAllBy(CodeBloc.class, CodeBloc::getCode);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.codebloc.ICodeBlocDAO#getByNKey(java.lang .String)
     */

    /**
     *
     * @param code
     * @return
     */

    @Override
    public Optional<CodeBloc> getByNKey(String code) {
        CriteriaQuery<CodeBloc> query = builder.createQuery(CodeBloc.class);
        Root<CodeBloc> codeBloc = query.from(CodeBloc.class);
        query
                .select(codeBloc)
                .where(
                        builder.equal(codeBloc.get(CodeBloc_.code), code)
                );
        return getOptional(query);
    }

}
