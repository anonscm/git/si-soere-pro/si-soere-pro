/**
 *
 */
package org.inra.ecoinfo.pro.refdata.observatoire;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Observatoire> {

    IObservatoireDAO observatoireDAO;

    /**
     * @param nom
     * @param definition
     * @param dbObservatoire
     * @param observatoire
     * @throws BusinessException
     */
    private void createOrUpdate(String nom, String code, String definition, Observatoire dbObservatoire, Observatoire observatoire) throws BusinessException {
        try {
            if (dbObservatoire == null) {
                observatoireDAO.saveOrUpdate(observatoire);
            } else {
                dbObservatoire.setNom(nom);
                dbObservatoire.setCode(code);
                dbObservatoire.setDefinition(definition);

                observatoireDAO.saveOrUpdate(dbObservatoire);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create observatoire");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com .Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();

                String code = Utils.createCodeFromString(nom);

                observatoireDAO.remove(observatoireDAO.getByNKey(code)
                        .orElseThrow(() -> new BusinessException("can't find observatoire")));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Observatoire> getAllElements() throws BusinessException {
        return observatoireDAO.getAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata (java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Observatoire observatoire) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesDefinition = localizationManager.newProperties(Observatoire.TABLE_NAME, RefDataConstantes.COLUMN_DEFINITION_OBS, Locale.ENGLISH);

        String localizedChampDefinition = "";

        if (observatoire != null) {
            localizedChampDefinition = propertiesDefinition.containsKey(observatoire.getDefinition()) ? propertiesDefinition.getProperty(observatoire.getDefinition()) : observatoire.getDefinition();
        }

        // Nom du programme
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(observatoire == null ? Constantes.STRING_EMPTY : observatoire.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        // Définition
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(observatoire == null ? Constantes.STRING_EMPTY : observatoire.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(observatoire == null ? Constantes.STRING_EMPTY : localizedChampDefinition, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com .Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values, Observatoire.TABLE_NAME);
                String nom = tokenizerValues.nextToken();
                String definition = tokenizerValues.nextToken();

                String code = Utils.createCodeFromString(nom);

                Observatoire observatoire = new Observatoire(nom, code, definition);
                Observatoire dbObservatoire = observatoireDAO.getByNKey(code).orElse(null);

                createOrUpdate(nom, code, definition, dbObservatoire, observatoire);

                values = parser.getLine();
            }
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param observatoireDAO the observatoireDAO to set
     */
    public void setObservatoireDAO(IObservatoireDAO observatoireDAO) {
        this.observatoireDAO = observatoireDAO;
    }

}
