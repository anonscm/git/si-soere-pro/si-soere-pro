/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.materiel;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.typeintervention.TypeIntervention;

/**
 *
 * @author vjkoyao
 */
public interface IMaterielDAO extends IDAO<Materiel> {

    /**
     *
     * @return
     */
    List<Materiel> getAll();

    /**
     *
     * @param materiel_nom
     * @param typeIntervention
     * @return
     */
    Optional<Materiel> getByNKey(String materiel_nom, TypeIntervention typeIntervention);

}
