/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpromoy;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolProMoy;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureIncubationSolProMoyDAO<T> extends IDAO<MesureIncubationSolProMoy> {

    /**
     *
     * @param key
     * @return
     */
    Optional<MesureIncubationSolProMoy> getByKeys(String key);

}
