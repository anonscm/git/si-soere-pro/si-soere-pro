package org.inra.ecoinfo.pro.refdata.methodeprocede;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.process.IProcessDAO;
import org.inra.ecoinfo.pro.refdata.process.Process;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.produitetapecaracteristique.EtapeCaracterisitique;
import org.inra.ecoinfo.pro.refdata.unites.IUniteDAO;
import org.inra.ecoinfo.pro.refdata.unites.Unites;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractGenericRecorder<MethodeProcedes> {

    private static final String PROPERTY_MSG_PROD_PROCESS_BAD_NOM = "PROPERTY_MSG_PROD_PROCESS_BAD_NOM";

    private static final String PROPERTY_MSG_PROD_PROCESS_BAD_KEY = "PROPERTY_MSG_PROD_PROCESS_BAD_KEY";

    private static final String PROPERTY_MSG_PROD_PROCESS_BAD_ORDER = "PROPERTY_MSG_PROD_PROCESS_BAD_ORDER";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    /**
     *
     */
    public static String[] listeProduitsPossibles;

    /**
     *
     */
    public static Map<String, String[]> listeProcedesPossibles;

    /**
     *
     */
    public static Map<String, String[]> listeOrdresPossibles;

    /**
     *
     * @param methodeProcedesDAO
     */
    public static void initMethodeProcedesPossibles(IMethodeProcedesDAO methodeProcedesDAO) {
        Map<String, Map<String, List<String>>> methodeProcedesMap = new HashMap<String, Map<String, List<String>>>();
        List<MethodeProcedes> methodeProcedes = methodeProcedesDAO.getAll();
        methodeProcedes.forEach((methodeProcede) -> {
            String codeProduit = methodeProcede.getProduits().getCodecomposant();
            String intituleProcess = methodeProcede.getProcess().getProcess_intitule();
            String order = Integer.toString(methodeProcede.getPmp_ordre());
            methodeProcedesMap
                    .computeIfAbsent(codeProduit, k -> new HashMap<String, List<String>>())
                    .computeIfAbsent(intituleProcess, k -> new LinkedList<>())
                    .add(order);
        });
        Recorder.setListeProduitsPossibles(methodeProcedesMap.keySet().toArray(new String[]{}));
        ConcurrentHashMap<String, String[]> listeProcedesPossibles = new ConcurrentHashMap<String, String[]>();
        ConcurrentHashMap<String, String[]> listeOrdresPossibles = new ConcurrentHashMap<String, String[]>();
        methodeProcedesMap.entrySet().forEach((entryProduit) -> {
            String codeProduit = entryProduit.getKey();
            Set<String> intitulesProcedes = entryProduit.getValue().keySet();
            listeProcedesPossibles.put(codeProduit, intitulesProcedes.toArray(new String[]{}));
            entryProduit.getValue().entrySet().forEach((entryProcess) -> {
                String intituleProcede = entryProcess.getKey();
                List<String> ordres = entryProcess.getValue();
                String codeOrdre = String.format("%s/%s", codeProduit, intituleProcede);
                listeOrdresPossibles.put(codeOrdre, ordres.toArray(new String[]{}));
            });
        });
        Recorder.setListeProcedesPossibles(listeProcedesPossibles);
        Recorder.setListeordresPossibles(listeOrdresPossibles);
    }

    /**
     *
     * @param lineModelGridMetadata
     * @param methodeProcedes
     */
    public static void initNewLine(LineModelGridMetadata lineModelGridMetadata, Optional<MethodeProcedes> methodeProcedes) {
        final Object codeProduit = methodeProcedes
                .map(mp->mp.getProduits())
                .map(pr->pr.getProd_key())
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata produitColumn = new ColumnModelGridMetadata(codeProduit, listeProduitsPossibles, null, true, false, true);
        final String codeComposant = methodeProcedes
                .map(mp->mp.getProcess())
                .map(pr->pr.getProcess_intitule())
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        final String percent = methodeProcedes
                .map(mp->mp.getPmp_ordre())
                .map(pmp->Integer.toString(pmp))
                .orElse(AbstractCSVMetadataRecorder.EMPTY_STRING);
        ColumnModelGridMetadata processColumn = new ColumnModelGridMetadata(codeComposant, listeProcedesPossibles, null, true, false, true);
        ColumnModelGridMetadata orderColumn = new ColumnModelGridMetadata(percent, listeOrdresPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> produitsRefs = new LinkedList<ColumnModelGridMetadata>();
        produitsRefs.add(processColumn);
        processColumn.setRefBy(produitColumn);
        produitColumn.setRefs(produitsRefs);
        processColumn.setValue(codeComposant);
        List<ColumnModelGridMetadata> processRefs = new LinkedList<ColumnModelGridMetadata>();
        processRefs.add(orderColumn);
        orderColumn.setRefBy(processColumn);
        processColumn.setRefs(processRefs);
        orderColumn.setValue(percent);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(produitColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(processColumn);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(orderColumn);
    }

    /**
     *
     * @param listeProduitsPossibles
     */
    public static void setListeProduitsPossibles(String[] listeProduitsPossibles) {
        Recorder.listeProduitsPossibles = listeProduitsPossibles;
    }

    /**
     *
     * @param listeProcedesPossibles
     */
    public static void setListeProcedesPossibles(Map<String, String[]> listeProcedesPossibles) {
        Recorder.listeProcedesPossibles = listeProcedesPossibles;
    }

    /**
     *
     * @param listeordresPossibles
     */
    public static void setListeordresPossibles(Map<String, String[]> listeordresPossibles) {
        Recorder.listeOrdresPossibles = listeordresPossibles;
    }

    /**
     *
     */
    protected IMethodeProcedesDAO methodeprocedeDAO;

    /**
     *
     */
    protected IProduitDAO produitDAO;

    /**
     *
     */
    protected IProcessDAO processDAO;

    Properties ComentEn;
    private String[] listeProcedesPossiblesSimple;

    /**
     *
     */
    protected IUniteDAO unitesDAO;
    private String[] listeUnitesPossibles;

    /**
     *
     * @param methodeprocedes
     * @throws BusinessException
     */
    private void CreateMethodeProcedes(final MethodeProcedes methodeprocedes) throws BusinessException {
        try {
            methodeprocedeDAO.saveOrUpdate(methodeprocedes);
            methodeprocedeDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create methodeprocedes");
        }

    }

    private void updateBDMethodeProcedes(Produits prod, Process process, int ordre, String comment, String duree, String unite, final MethodeProcedes dbmethodeprocedes) throws BusinessException {
        try {
            dbmethodeprocedes.setProduits(prod);
            dbmethodeprocedes.setProcess(process);
            dbmethodeprocedes.setPmp_ordre(ordre);
            dbmethodeprocedes.setCommentaire(comment);
            dbmethodeprocedes.setPmp_duree(duree);
            dbmethodeprocedes.setPmp_unite(unite);
            methodeprocedeDAO.saveOrUpdate(dbmethodeprocedes);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update methodeprocedes");
        }

    }

    /**
     *
     * @param etapearacteristiques
     * @param dbEtapeCaracteristiques
     * @throws BusinessException
     */
    private void createOrUpdateMethodeProcedes(Produits prod, Process process, int ordre, String comment, String duree, String unite, final MethodeProcedes dbmethodeprocedes) throws BusinessException {
        if (dbmethodeprocedes == null) {
            final MethodeProcedes methodeprocedes = new MethodeProcedes(prod, process, ordre, comment, duree, unite);
            methodeprocedes.setProduits(prod);
            methodeprocedes.setProcess(process);
            methodeprocedes.setPmp_ordre(ordre);
            methodeprocedes.setCommentaire(comment);
            methodeprocedes.setPmp_unite(unite);
            CreateMethodeProcedes(methodeprocedes);
        } else {
            updateBDMethodeProcedes(prod, process, ordre, comment, duree, unite, dbmethodeprocedes);
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String codeprod = tokenizerValues.nextToken();
                final String intitule = tokenizerValues.nextToken();
                final String ordre = tokenizerValues.nextToken();
                int order = Integer.parseInt(ordre);
                Produits dbproduits = produitDAO.getByNKey(codeprod).orElse(null);
                final Process dbprocess = processDAO.getByNKey(intitule).orElse(null);
                final MethodeProcedes dbmprocedes = methodeprocedeDAO.getByNKey(dbproduits, dbprocess, order)
                        .orElseThrow(() -> new BusinessException("can't find methodeprocedes"));
                methodeprocedeDAO.remove(dbmprocedes);
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<MethodeProcedes> getAllElements() throws BusinessException {
        return methodeprocedeDAO.getAll();
    }

    /**
     *
     * @return
     */
    public IMethodeProcedesDAO getMethodeprocedeDAO() {
        return methodeprocedeDAO;
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(MethodeProcedes methodeprocedes) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : methodeprocedes.getProduits().getProd_key() != null
                        ? methodeprocedes.getProduits().getProd_key() : "", listeProduitsPossibles,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedes.getProcess().getProcess_intitule() != null ? methodeprocedes.getProcess().getProcess_intitule() : "",
                        listeProcedesPossiblesSimple, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null || methodeprocedes.getPmp_ordre() == EMPTY_INT_VALUE ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedes.getPmp_ordre(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false,
                        true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null || methodeprocedes.getPmp_duree() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedes.getPmp_duree(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedes.getPmp_unite() != null ? methodeprocedes.getPmp_unite() : "", listeUnitesPossibles, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null || methodeprocedes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : methodeprocedes.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true,
                        false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(methodeprocedes == null || methodeprocedes.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : ComentEn.getProperty(methodeprocedes.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProcessDAO getProcessDAO() {
        return processDAO;
    }

    /**
     *
     * @return
     */
    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    /**
     *
     * @return
     */
    public IUniteDAO getUnitesDAO() {
        return unitesDAO;
    }

    @Override
    protected ModelGridMetadata<MethodeProcedes> initModelGridMetadata() {
        procedesPossibles();
        produitsPossibles();
        unitesNomPossibles();

        ComentEn = localizationManager.newProperties(MethodeProcedes.NAME_ENTITY_JAP, MethodeProcedes.JPA_NAME_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void persistMethodeProcedes(Produits produits, Process process, int ordre, String comment, String duree, String unite) throws BusinessException, BusinessException {
        final MethodeProcedes dbmethodeprocedes = methodeprocedeDAO.getByNKey(produits, process, ordre).orElse(null);
        createOrUpdateMethodeProcedes(produits, process, ordre, comment, duree, unite, dbmethodeprocedes);

    }

    private void procedesPossibles() {
        List<Process> groupeprocedes = processDAO.getAll(Process.class);
        String[] Listeprocedes = new String[groupeprocedes.size() + 1];
        Listeprocedes[0] = "";
        int index = 1;
        for (Process process : groupeprocedes) {
            Listeprocedes[index++] = process.getProcess_intitule();
        }
        this.listeProcedesPossiblesSimple = Listeprocedes;
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, EtapeCaracterisitique.NAME_ENTITY_JAP);
                line++;
                int indexpro = tokenizerValues.currentTokenIndex();
                final String pcode = tokenizerValues.nextToken();
                int indexnom = tokenizerValues.currentTokenIndex();
                final String process_intitule = tokenizerValues.nextToken();
                int indexorder = tokenizerValues.currentTokenIndex();
                int ordre = verifieInt(tokenizerValues, line, false, errorsReport);
                if (ordre == 0) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_PROCESS_BAD_ORDER), line, indexorder, ordre));

                }
                //  Date date = testDate(errorsReport, line, tokenizerValues);
                final String dure = tokenizerValues.nextToken();

                final String unite = tokenizerValues.nextToken();
                final String commentaire = tokenizerValues.nextToken();
                Produits dbproduits = produitDAO.getByNKey(pcode).orElse(null);
                if (dbproduits == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_PROCESS_BAD_KEY), line, indexpro, pcode));

                }
                Process dbprocess = processDAO.getByNKey(process_intitule).orElse(null);
                if (dbprocess == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_PROCESS_BAD_NOM), line, indexnom, process_intitule));

                }
                if (!errorsReport.hasErrors()) {
                    persistMethodeProcedes(dbproduits, dbprocess, ordre, commentaire, dure, unite);
                }
                values = parser.getLine();

            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);

        }

    }

    private void produitsPossibles() {
        List<Produits> groupeproduits = produitDAO.getAll();
        String[] Listeproduits = new String[groupeproduits.size() + 1];
        Listeproduits[0] = "";
        int index = 1;
        for (Produits produits : groupeproduits) {
            Listeproduits[index++] = produits.getProd_key();
        }
        this.listeProduitsPossibles = Listeproduits;
    }

    /**
     *
     * @param methodeprocedeDAO
     */
    public void setMethodeprocedeDAO(IMethodeProcedesDAO methodeprocedeDAO) {
        this.methodeprocedeDAO = methodeprocedeDAO;
    }

    /**
     *
     * @param processDAO
     */
    public void setProcessDAO(IProcessDAO processDAO) {
        this.processDAO = processDAO;
    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param unitesDAO
     */
    public void setUnitesDAO(IUniteDAO unitesDAO) {
        this.unitesDAO = unitesDAO;
    }

    private void unitesNomPossibles() {
        List<Unites> groupeunite = unitesDAO.getAll(Unites.class);
        String[] Listeunite = new String[groupeunite.size() + 1];
        Listeunite[0] = "";
        int index = 1;
        for (Unites unites : groupeunite) {
            Listeunite[index++] = unites.getUnite();
        }
        this.listeUnitesPossibles = Listeunite;
    }


}
