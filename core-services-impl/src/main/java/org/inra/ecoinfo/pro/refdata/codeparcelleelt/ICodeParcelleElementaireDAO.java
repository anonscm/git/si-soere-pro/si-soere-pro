package org.inra.ecoinfo.pro.refdata.codeparcelleelt;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ICodeParcelleElementaireDAO extends IDAO<CodeParcelleElementaire> {

    /**
     *
     * @return
     */
    List<CodeParcelleElementaire> getAll();

    /**
     *
     * @param code
     * @return
     */
    Optional<CodeParcelleElementaire> getByNKey(String code);

}
