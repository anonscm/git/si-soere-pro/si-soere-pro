/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSols;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.ValeurPhysicoChimieSols;

/**
 *
 * @author adiankha
 */
public interface IValeurPhysicoChimieSol extends IDAO<ValeurPhysicoChimieSols>{

    /**
     *
     * @param realNode
     * @param mesureSol
     * @param statut
     * @return
     */
    Optional<ValeurPhysicoChimieSols> getByNKeys(RealNode realNode,MesurePhysicoChimieSols mesureSol,String statut);
    
}
