/**
 *
 */
package org.inra.ecoinfo.pro.refdata.adresse;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.commune.Commune;

/**
 * @author sophie
 *
 */
public class JPAAdresseDAO extends AbstractJPADAO<Adresse> implements IAdresseDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.adresse.IAdresseDAO#getByNKey(java.lang .String, java.lang.String, org.inra.ecoinfo.pro.refdata.pays.Commune)
     */

    /**
     *
     * @param noNomRue
     * @param commune
     * @return
     */

    @Override
    public Optional<Adresse> getByNKey(String noNomRue, Commune commune) {
        CriteriaQuery<Adresse> query = builder.createQuery(Adresse.class);
        Root<Adresse> adresse = query.from(Adresse.class);
        query
                .select(adresse)
                .where(
                        builder.equal(adresse.get(Adresse_.noNomRue), noNomRue),
                        builder.equal(adresse.get(Adresse_.commune), commune)
                );
        return getOptional(query);
    }

}
