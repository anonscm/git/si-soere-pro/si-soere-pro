/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;

/**
 *
 * @author adiankha
 * @param <T>
 */
public interface IMesureTravailduSolDAO<T> extends IDAO<MesureTravailDuSol> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesureTravailDuSol> getByKeys(String keymesure);
}
