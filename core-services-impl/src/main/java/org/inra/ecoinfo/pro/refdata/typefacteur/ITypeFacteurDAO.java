/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typefacteur;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeFacteurDAO extends IDAO<TypeFacteur> {

    List<TypeFacteur> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeFacteur> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<TypeFacteur> getByCode(String code);

}
