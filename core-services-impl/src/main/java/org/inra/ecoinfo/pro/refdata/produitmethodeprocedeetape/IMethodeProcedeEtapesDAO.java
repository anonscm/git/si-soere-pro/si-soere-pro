package org.inra.ecoinfo.pro.refdata.produitmethodeprocedeetape;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.etape.Etapes;
import org.inra.ecoinfo.pro.refdata.methodeetape.MethodeEtapes;
import org.inra.ecoinfo.pro.refdata.methodeprocede.MethodeProcedes;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public interface IMethodeProcedeEtapesDAO extends IDAO<MethodeProcedeEtapes> {

    /**
     *
     * @return
     */
    List<MethodeProcedeEtapes> getAll();

    /**
     *
     * @param mp
     * @param methodeEtapes
     * @param etape
     * @param ordre
     * @return
     */
    Optional<MethodeProcedeEtapes> getByNKey(MethodeProcedes mp, MethodeEtapes methodeEtapes, Etapes etape, int ordre);

    /**
     *
     * @param codeProduits
     * @param intituleProcede
     * @param ordreProcede
     * @param methodeEtape
     * @param intituleEtape
     * @param ordreEtape
     * @return
     */
    Optional<MethodeProcedeEtapes> getByNKey(String codeProduits, String intituleProcede, int ordreProcede, String methodeEtape, String intituleEtape, int ordreEtape);

}
