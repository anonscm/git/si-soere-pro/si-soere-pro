/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.pedologie;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.nomregionalsol.Nomregionalsol;
import org.inra.ecoinfo.pro.refdata.substratpedologique.Substratpedologique;

/**
 *
 * @author adiankha
 */
public class PedologieDAOImpl extends AbstractJPADAO<Pedologie> implements IPedologieDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Pedologie> getAll() {
        return getAll(Pedologie.class);
    }

    /**
     *
     * @param dispositif
     * @param nomregionalsol
     * @param substratpedologique
     * @return
     */
    @Override
    public Optional<Pedologie> getByNKey(Dispositif dispositif, Substratpedologique substratpedologique) {
        CriteriaQuery<Pedologie> query = builder.createQuery(Pedologie.class);
        Root<Pedologie> pedologie = query.from(Pedologie.class);
        Path<Dispositif> dispo = pedologie.get(Pedologie_.dispositif);
       
        Path<Substratpedologique> subPed = pedologie.get(Pedologie_.substratpedologique);
        query
                .select(pedologie)
                .where(
                        builder.equal(dispo, dispositif),
                     
                        builder.equal(subPed, substratpedologique)
                );
        return getOptional(query);
    }
}
