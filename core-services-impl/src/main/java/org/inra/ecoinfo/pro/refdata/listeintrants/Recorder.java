/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeintrants;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<ListeIntrants> {

    IListeIntrantsDAO intrantsDAO;

    private void createListeIntrants(ListeIntrants listeIntrants) throws BusinessException {
        try {
            intrantsDAO.saveOrUpdate(listeIntrants);
            intrantsDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void updateListeIntrants(String nomcommercial, String knomcommercial, String nomelement, String knomelement, int concentration, String unite, ListeIntrants dbListeIntrants) throws BusinessException {
        try {
            dbListeIntrants.setNomcommercial(nomcommercial);
            dbListeIntrants.setKnomcommercial(knomcommercial);
            dbListeIntrants.setNomelement(nomelement);
            dbListeIntrants.setKnomelement(knomelement);
            dbListeIntrants.setConcentration(concentration);
            dbListeIntrants.setUnite(unite);
            intrantsDAO.saveOrUpdate(dbListeIntrants);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void saveOrUpdateListeIntrants(String nomcommercial, String knomcommercial, String nomelement, String knomelement, int concentration, String unite, ListeIntrants dbListeIntrants) throws BusinessException {
        if (dbListeIntrants == null) {
            ListeIntrants listeIntrants = new ListeIntrants(nomcommercial, knomcommercial, nomelement, knomelement, concentration, unite);
            listeIntrants.setNomcommercial(nomcommercial);
            listeIntrants.setKnomcommercial(knomcommercial);
            listeIntrants.setNomelement(nomelement);
            listeIntrants.setKnomelement(knomelement);
            listeIntrants.setConcentration(concentration);
            listeIntrants.setUnite(unite);
            createListeIntrants(listeIntrants);
        } else {
            updateListeIntrants(nomcommercial, knomcommercial, nomelement, knomelement, concentration, unite, dbListeIntrants);
        }
    }

    private void persistListeintrants(String nomcommercial, String knomcommercial, String nomelement, String knomelement, int concentration, String unite) throws BusinessException {
        ListeIntrants listeIntrants = intrantsDAO.getByNKey(nomcommercial, nomelement).orElse(null);
        saveOrUpdateListeIntrants(nomcommercial, knomcommercial, nomelement, knomelement, concentration, unite, listeIntrants);
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();
                String element = tokenizerValues.nextToken();
                intrantsDAO.remove(intrantsDAO.getByNKey(nom, element)
                        .orElseThrow(() -> new BusinessException("can't find intrants")));
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            int line =0;
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ListeIntrants.JPA_NAME_TABLE);

                final String nomcommercial = tokenizerValues.nextToken();
                final String knomcommercial = Utils.createCodeFromString(nomcommercial);
                final String nomelement = tokenizerValues.nextToken();
                final String knomelement = Utils.createCodeFromString(nomelement);
                int concen = verifieInt(tokenizerValues, line, false, errorsReport);
                final String unite = tokenizerValues.nextToken();

                if (!errorsReport.hasErrors()) {

                    persistListeintrants(nomcommercial, knomcommercial, nomelement, knomelement, concen, unite);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<ListeIntrants> getAllElements() throws BusinessException {
        return intrantsDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ListeIntrants listeIntrants) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeIntrants == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeIntrants.getNomcommercial(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeIntrants == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeIntrants.getNomelement(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeIntrants == null || listeIntrants.getConcentration() == EMPTY_INT_VALUE ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeIntrants.getConcentration(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeIntrants == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeIntrants.getUnite(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, false, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @param intrantsDAO
     */
    public void setIntrantsDAO(IListeIntrantsDAO intrantsDAO) {
        this.intrantsDAO = intrantsDAO;
    }

}
