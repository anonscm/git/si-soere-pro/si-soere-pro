package org.inra.ecoinfo.pro.refdata.rolepersonneressourcedispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.lieu.ILieuDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.personneressource.IPersonneRessourceDAO;
import org.inra.ecoinfo.pro.refdata.personneressource.PersonneRessource;
import org.inra.ecoinfo.pro.refdata.rolepersonneressource.IRolePersonneRessourceDAO;
import org.inra.ecoinfo.pro.refdata.rolepersonneressource.RolePersonneRessource;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<RolePersonneRessourceDispositif> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ILieuDAO lieuDAO;
    IDispositifDAO dispositifDAO;
    IPersonneRessourceDAO personneRessourceDAO;
    IRolePersonneRessourceDAO rolePersonneRessourceDAO;
    IRolePersonneRessourceDispositifDAO rolePersonneRessourceDispositifDAO;

    /**
     * @return @throws PersistenceException
     */
    private String[] getDispositifPossibles() {
        List<Dispositif> lstDispositifs = dispositifDAO.getAll();
        String[] dispositifPossibles = new String[lstDispositifs.size()];
        int index = 0;
        for (Dispositif dispositif : lstDispositifs) {
            dispositifPossibles[index++] = dispositif.getCode() + " (" + dispositif.getLieu().getNom() + ")";
        }
        return dispositifPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getEmailPersonneRessourcePossibles() {
        List<PersonneRessource> lstPersonneRessources = personneRessourceDAO.getAll(PersonneRessource.class);
        String[] emailParNomPrenomPossibles = new String[lstPersonneRessources.size()];
        int index = 0;
        for (PersonneRessource personneRessource : lstPersonneRessources) {
            emailParNomPrenomPossibles[index++] = personneRessource.getEmail();
        }

        return emailParNomPrenomPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private Map<String, String[]> getNomPersonneRessourcePossibles() {
        Map<String, String[]> nomPossibles = new TreeMap<String, String[]>();

        List<PersonneRessource> lstPersonneRessources = personneRessourceDAO.getAll(PersonneRessource.class);

        lstPersonneRessources.forEach((personneRessource) -> {
            PersonneRessource nomPersonneRessource = personneRessourceDAO.getByEMail(personneRessource.getEmail()).orElse(null);
            String[] nomPersonneRessourcePossibles = new String[1];

            nomPersonneRessourcePossibles[0] = nomPersonneRessource.getNom();

            nomPossibles.put(personneRessource.getEmail(), nomPersonneRessourcePossibles);
        });

        return nomPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private Map<String, String[]> getPrenomPersonneRessourcePossibles() {
        Map<String, String[]> prenomPossibles = new TreeMap<String, String[]>();

        List<PersonneRessource> lstPersonneRessources = personneRessourceDAO.getAll(PersonneRessource.class);

        lstPersonneRessources.forEach((personneRessource) -> {
            PersonneRessource prenomPersonneRessource = personneRessourceDAO.getByEMail(personneRessource.getEmail()).orElse(null);
            String[] prenomPersonneRessourcePossibles = new String[1];

            prenomPersonneRessourcePossibles[0] = prenomPersonneRessource.getPrenom();

            prenomPossibles.put(personneRessource.getEmail(), prenomPersonneRessourcePossibles);
        });

        return prenomPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getRolePersonneRessourcePossibles() {
        List<RolePersonneRessource> lstRoles = rolePersonneRessourceDAO.getAll(RolePersonneRessource.class);
        String[] rolePossibles = new String[lstRoles.size()];
        int index = 0;
        for (RolePersonneRessource rolePersonneRessource : lstRoles) {
            rolePossibles[index++] = rolePersonneRessource.getLibelle();
        }
        return rolePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String codeDispositifLieu = tokenizerValues.nextToken();
                String eMailPersonneRessource = tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                tokenizerValues.nextToken();
                String libelleRole = tokenizerValues.nextToken();
                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
                Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
                PersonneRessource personneRessource = personneRessourceDAO.getByEMail(eMailPersonneRessource).orElse(null);
                RolePersonneRessource rolePersonneRessource = rolePersonneRessourceDAO.getByNKey(libelleRole).orElse(null);

                rolePersonneRessourceDispositifDAO.remove(rolePersonneRessourceDispositifDAO.getByPersonneRessourceDispositifRole(personneRessource, dispositif, rolePersonneRessource)
                        .orElseThrow(() -> new BusinessException("can't get rolePersonneRessourceDispositif")));

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<RolePersonneRessourceDispositif> getAllElements() throws BusinessException {
        return rolePersonneRessourceDispositifDAO.getAll(RolePersonneRessourceDispositif.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(RolePersonneRessourceDispositif rolePersonneRessourceDispositif) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Code dispositif (Nom du lieu)
        String codeDipositifLieu = rolePersonneRessourceDispositif == null ? "" : rolePersonneRessourceDispositif.getDispositif().getNomDispositif_nomLieu();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(rolePersonneRessourceDispositif == null ? Constantes.STRING_EMPTY : codeDipositifLieu, getDispositifPossibles(), null, true, false, true));
        //Email de la persone ressource
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(rolePersonneRessourceDispositif == null ? Constantes.STRING_EMPTY : rolePersonneRessourceDispositif.getPersonneRessource().getEmail(), getEmailPersonneRessourcePossibles(),
                null, true, false, true);

        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);

        //Nom de la personne ressource
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(rolePersonneRessourceDispositif == null ? Constantes.STRING_EMPTY : rolePersonneRessourceDispositif.getPersonneRessource().getNom(), getNomPersonneRessourcePossibles(), null,
                true, false, true);

        //Prénon de la personne ressource
        ColumnModelGridMetadata colonne3 = new ColumnModelGridMetadata(rolePersonneRessourceDispositif == null ? Constantes.STRING_EMPTY : rolePersonneRessourceDispositif.getPersonneRessource().getPrenom(), getPrenomPersonneRessourcePossibles(),
                null, true, false, true);

        refsColonne1.add(colonne2);
        refsColonne1.add(colonne3);

        colonne1.setRefs(refsColonne1);

        colonne2.setValue(rolePersonneRessourceDispositif == null ? "" : rolePersonneRessourceDispositif.getPersonneRessource().getNom());
        colonne3.setValue(rolePersonneRessourceDispositif == null ? "" : rolePersonneRessourceDispositif.getPersonneRessource().getPrenom());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne3);

        //Libellé du rôle de la personne ressource
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(rolePersonneRessourceDispositif == null ? Constantes.STRING_EMPTY : rolePersonneRessourceDispositif.getRole().getLibelle(), getRolePersonneRessourcePossibles(), null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexlieu = tokenizerValues.currentTokenIndex();
                String codeDispositifLieu = tokenizerValues.nextToken();
                String eMailPersonneRessource = tokenizerValues.nextToken();
                int indexnom = tokenizerValues.currentTokenIndex();
                String nomPersonneRessource = tokenizerValues.nextToken();
                String prenomPersonneRessource = tokenizerValues.nextToken();
                int indexrole = tokenizerValues.currentTokenIndex();
                String libelleRole = tokenizerValues.nextToken();

                String codeDispositif = Dispositif.getNomDispositif(codeDispositifLieu);
                String nomLieu = Dispositif.getNomLieu(codeDispositifLieu);

                Lieu lieu = verifieLieu(nomLieu, line + 1, indexlieu + 1, errorsReport);
                Dispositif dispositif = verifieDispositif(codeDispositif, lieu, line + 1, indexlieu + 1, errorsReport);
                RefDataUtil.verifieEmail(eMailPersonneRessource, errorsReport, localizationManager);
                PersonneRessource personneRessource = verifiePersonneRessource(eMailPersonneRessource, nomPersonneRessource, prenomPersonneRessource, line + 1, indexnom + 1, errorsReport);
                RolePersonneRessource rolePersonneRessource = verifieRolePersonneRessource(libelleRole, line + 1, indexrole + 1, errorsReport);

                RolePersonneRessourceDispositif rolePersonneRessourceDispositif = new RolePersonneRessourceDispositif(personneRessource, dispositif, rolePersonneRessource);
                RolePersonneRessourceDispositif dbRolePersonneRessourceDispositif = rolePersonneRessourceDispositifDAO.getByPersonneRessourceDispositifRole(personneRessource, dispositif, rolePersonneRessource)
                        .orElse(null);
                if (dbRolePersonneRessourceDispositif == null && !errorsReport.hasErrors()) {
                    rolePersonneRessourceDispositifDAO.saveOrUpdate(rolePersonneRessourceDispositif);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param nomLieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Lieu verifieLieu(String nomLieu, long line, int index, ErrorsReport errorsReport) {
        Lieu lieu = lieuDAO.getByNKey(nomLieu).orElse(null);
        if (lieu == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LIEU_NONDEFINI"), line, index, nomLieu));
        }

        return lieu;
    }

    /**
     * @param codeDispositif
     * @param lieu
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Dispositif verifieDispositif(String codeDispositif, Lieu lieu, long line, int index, ErrorsReport errorsReport) {
        Dispositif dispositif = dispositifDAO.getByNKey(codeDispositif, lieu).orElse(null);
        if (dispositif == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DISPOSITIF_NONDEFINI"), line, index, codeDispositif));
        }

        return dispositif;
    }

    /**
     * @param eMailPersonneRessource
     * @param nomPersonneRessource
     * @param prenomPersonneRessource
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private PersonneRessource verifiePersonneRessource(String eMailPersonneRessource, String nomPersonneRessource, String prenomPersonneRessource, long line, int index, ErrorsReport errorsReport) {
        PersonneRessource personneRessource = personneRessourceDAO.getByEMail(eMailPersonneRessource).orElse(null);
        if (personneRessource == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "PERSONNERES_NONDEFINI"), line, index, nomPersonneRessource + " " + prenomPersonneRessource));
        }

        return personneRessource;
    }

    /**
     * @param libelleRole
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private RolePersonneRessource verifieRolePersonneRessource(String libelleRole, long line, int index, ErrorsReport errorsReport) {
        RolePersonneRessource rolePersonneRessource = rolePersonneRessourceDAO.getByNKey(libelleRole).orElse(null);
        if (rolePersonneRessource == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ROLEPERSONNERES_NONDEFINI"), line, index, libelleRole));
        }

        return rolePersonneRessource;
    }

    /**
     * @param lieuDAO the lieuDAO to set
     */
    public void setLieuDAO(ILieuDAO lieuDAO) {
        this.lieuDAO = lieuDAO;
    }

    /**
     * @param dispositifDAO the dispositifDAO to set
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     * @param personneRessourceDAO the personneRessourceDAO to set
     */
    public void setPersonneRessourceDAO(IPersonneRessourceDAO personneRessourceDAO) {
        this.personneRessourceDAO = personneRessourceDAO;
    }

    /**
     * @param rolePersonneRessourceDAO the rolePersonneRessourceDAO to set
     */
    public void setRolePersonneRessourceDAO(
            IRolePersonneRessourceDAO rolePersonneRessourceDAO) {
        this.rolePersonneRessourceDAO = rolePersonneRessourceDAO;
    }

    /**
     * @param rolePersonneRessourceDispositifDAO the
     * rolePersonneRessourceDispositifDAO to set
     */
    public void setRolePersonneRessourceDispositifDAO(
            IRolePersonneRessourceDispositifDAO rolePersonneRessourceDispositifDAO) {
        this.rolePersonneRessourceDispositifDAO = rolePersonneRessourceDispositifDAO;
    }

}
