/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.listeitineraire;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<ListeItineraire> {

    /**
     *
     */
    public IListeItineraireDAO listeItineraireDAO;
    Properties valeurEn;
    Properties libelleEn;
    Properties comment;

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                ListeItineraire listeItineraire = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String liste_nom = tokenizerValues.nextToken();
                String liste_valeur = tokenizerValues.nextToken();
                String liste_commentaire = tokenizerValues.nextToken();

                listeItineraireDAO.remove(listeItineraireDAO.getByNKey(liste_nom, liste_valeur)
                        .orElseThrow(() -> new BusinessException("can't find liste itinéraire")));

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);

            String[] values = parser.getLine();

            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, ListeItineraire.NAME_ENTITY_JPA);

                final String liste_nom = tokenizerValues.nextToken();
                final String knom = Utils.createCodeFromString(liste_nom);
                final String liste_valeur = tokenizerValues.nextToken();
                final String kvaleur = Utils.createCodeFromString(liste_valeur);
                final String liste_commentaire = tokenizerValues.nextToken();

                if (!errorsReport.hasErrors()) {

                    persistListeITK(liste_nom, knom, liste_valeur, kvaleur, liste_commentaire);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<ListeItineraire> getAllElements() throws BusinessException {
        return listeItineraireDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ListeItineraire listeItineraire) throws BusinessException {

        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeItineraire.getListe_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null || listeItineraire.getListe_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(listeItineraire.getListe_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeItineraire.getListe_valeur(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null || listeItineraire.getListe_valeur() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : valeurEn.getProperty(listeItineraire.getListe_valeur()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : listeItineraire.getListe_commentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES,
                        null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(listeItineraire == null || listeItineraire.getListe_commentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comment.getProperty(listeItineraire.getListe_commentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    private void persistListeITK(String liste_nom, String knom, String liste_valeur, String kvaleur, String liste_commentaire) throws BusinessException {
        final ListeItineraire dblisteItk = listeItineraireDAO.getByNKey(liste_nom, liste_valeur).orElse(null);
        createOrUpdateListeITK(liste_nom, knom, liste_valeur, kvaleur, liste_commentaire, dblisteItk);
    }

    private void createOrUpdateListeITK(String liste_nom, String knom, String liste_valeur, String kvaleur, String liste_commentaire, ListeItineraire dblisteItk) throws BusinessException {
        if (dblisteItk == null) {
            ListeItineraire listeItineraire = new ListeItineraire(liste_nom, liste_valeur, liste_commentaire);

            listeItineraire.setListe_nom(liste_nom);
            listeItineraire.setKnom(knom);
            listeItineraire.setListe_valeur(liste_valeur);
            listeItineraire.setKvaleur(kvaleur);
            listeItineraire.setListe_commentaire(liste_commentaire);

            createListeITK(listeItineraire);

        } else {
            updateListeITK(liste_nom, knom, liste_valeur, kvaleur, liste_commentaire, dblisteItk);
        }
    }

    private void createListeITK(ListeItineraire listeItineraire) throws BusinessException {
        try {
            listeItineraireDAO.saveOrUpdate(listeItineraire);
            listeItineraireDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create listeItineraire");
        }
    }

    private void updateListeITK(String liste_nom, String knom, String liste_valeur, String kvaleur, String liste_commentaire, ListeItineraire dblisteItk) throws BusinessException {
        try {
            dblisteItk.setListe_nom(liste_nom);
            dblisteItk.setKnom(knom);
            dblisteItk.setListe_valeur(liste_valeur);
            dblisteItk.setKvaleur(kvaleur);
            dblisteItk.setListe_commentaire(liste_commentaire);
            listeItineraireDAO.saveOrUpdate(dblisteItk);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update listeItineraire");
        }
    }

    /**
     *
     * @return
     */
    public IListeItineraireDAO getListeItineraireDAO() {
        return listeItineraireDAO;
    }

    /**
     *
     * @param listeItineraireDAO
     */
    public void setListeItineraireDAO(IListeItineraireDAO listeItineraireDAO) {
        this.listeItineraireDAO = listeItineraireDAO;
    }

    @Override
    protected ModelGridMetadata<ListeItineraire> initModelGridMetadata() {

        valeurEn = localizationManager.newProperties(ListeItineraire.NAME_ENTITY_JPA, ListeItineraire.JPA_COLUMN_LISTE_VALEUR, Locale.ENGLISH);
        libelleEn = localizationManager.newProperties(ListeItineraire.NAME_ENTITY_JPA, ListeItineraire.JPA_COLUMN_LISTE_NAME, Locale.ENGLISH);
        comment = localizationManager.newProperties(ListeItineraire.NAME_ENTITY_JPA, ListeItineraire.JPA_COLUMN_LISTE_COMMENTAIRE, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

}
