package org.inra.ecoinfo.pro.refdata.heterogeinite;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author ptcherniati
 */
public interface IHeterogeiniteDAO extends IDAO<Heterogeinite> {

    /**
     *
     * @return
     */
    List<Heterogeinite> getAll();

    /**
     *
     * @param nom
     * @return
     */
    Optional<Heterogeinite> getByNKey(String nom);

}
