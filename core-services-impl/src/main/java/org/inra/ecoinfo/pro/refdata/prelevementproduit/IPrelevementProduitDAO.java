/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.prelevementproduit;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.bloc.Bloc;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.placette.Placette;
import org.inra.ecoinfo.pro.refdata.produitdispositif.ProduitDispositif;

/**
 *
 * @author adiankha
 */
public interface IPrelevementProduitDAO extends IDAO<PrelevementProduit> {

    /**
     *
     * @return
     */
    List<PrelevementProduit> getAll();

    /**
     *
     * @param codeprelevement
     * @return
     */
    Optional<PrelevementProduit> getByNKey(String codeprelevement);

    /**
     *
     * @param date
     * @param numeroRepetition
     * @param produitdispositif
     * @param traitement
     * @param bloc
     * @param placette
     * @return
     */
    Optional<PrelevementProduit> getByNkey(
            LocalDate date,
            int numeroRepetition,
            ProduitDispositif produitdispositif,
            DescriptionTraitement traitement,
            Bloc bloc,
            Placette placette
    );

}
