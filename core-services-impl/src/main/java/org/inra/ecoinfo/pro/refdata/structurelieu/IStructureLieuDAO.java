package org.inra.ecoinfo.pro.refdata.structurelieu;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.lieu.Lieu;
import org.inra.ecoinfo.pro.refdata.structure.Structure;


/**
 * @author sophie
 * 
 */
public interface IStructureLieuDAO extends IDAO<StructureLieu> {

    /**
     *
     * @param structure
     * @param lieu
     * @return
     */
    Optional<StructureLieu> getByNKey(Structure structure, Lieu lieu);

}
