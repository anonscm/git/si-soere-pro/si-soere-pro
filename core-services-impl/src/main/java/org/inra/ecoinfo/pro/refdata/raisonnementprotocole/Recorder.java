package org.inra.ecoinfo.pro.refdata.raisonnementprotocole;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.IListeRaisonnementDAO;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement;
import org.inra.ecoinfo.pro.refdata.protocole.IProtocoleDAO;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;
import org.inra.ecoinfo.pro.refdata.raisonnement.IRaisonnementDAO;
import org.inra.ecoinfo.pro.refdata.raisonnement.Raisonnement;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<RaisonnementProtocole> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    IListeRaisonnementDAO listeRaisonnementDAO;
    IRaisonnementDAO raisonnementDAO;
    IProtocoleDAO protocoleDAO;
    IRaisonnementProtocoleDAO raisonnementProtocoleDAO;
    
    Map<String, String[]> raisonntPossibles = new TreeMap<String, String[]>();

    /**
     * @return @throws BusinessException
     */
    private String[] getLibelleListeRaisonnementPossibles() {
        List<ListeRaisonnement> lstListeRaisonnements = listeRaisonnementDAO.getAll(ListeRaisonnement.class);
        String[] listeRaisonnementPossibles = new String[lstListeRaisonnements.size()];
        int index = 0;
        for (ListeRaisonnement listeRaisonnement : lstListeRaisonnements) {
            listeRaisonnementPossibles[index++] = listeRaisonnement.getLibelle();
        }
        return listeRaisonnementPossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getProtocolePossibles() {
        List<Protocole> lstProtocoles = protocoleDAO.getAll(Protocole.class);
        String[] protocolePossibles = new String[lstProtocoles.size()];
        int index = 0;
        for (Protocole protocole : lstProtocoles) {
            protocolePossibles[index++] = protocole.getNom();
        }
        return protocolePossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private Map<String, String[]> getRaisonnementPossibles() {
        List<ListeRaisonnement> lstListeRaisonnements = listeRaisonnementDAO.getAll(ListeRaisonnement.class);
        lstListeRaisonnements.forEach((listeRaisonnement) -> {
            List<Raisonnement> lstRaisonnements = raisonnementDAO.getByListeRaisonnement(listeRaisonnement);
            String[] raisonnementPossibles = new String[lstRaisonnements.size()];
            int index = 0;
            for (Raisonnement raisonnement : lstRaisonnements) {
                raisonnementPossibles[index++] = raisonnement.getLibelle();
            }
            
            raisonntPossibles.put(listeRaisonnement.getLibelle(), raisonnementPossibles);
        });
        
        return raisonntPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String nomProtocole = tokenizerValues.nextToken();
                String libelleLstRaisonnement = tokenizerValues.nextToken();
                String libelleRaisonnement = tokenizerValues.nextToken();
                
                ListeRaisonnement listeRaisonnement = listeRaisonnementDAO.getByNKey(libelleLstRaisonnement).orElse(null);
                Raisonnement raisonnement = raisonnementDAO.getByNKey(libelleRaisonnement, listeRaisonnement).orElse(null);
                Protocole protocole = protocoleDAO.getByNKey(nomProtocole).orElse(null);
                
                RaisonnementProtocole raisonnementProtocole = raisonnementProtocoleDAO.getByNKey(raisonnement, protocole)
                        .orElseThrow(() -> new BusinessException("can't get raisonnement protocole"));
                raisonnementProtocoleDAO.remove(raisonnementProtocole);
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<RaisonnementProtocole> getAllElements() throws BusinessException {
        return raisonnementProtocoleDAO.getAll(RaisonnementProtocole.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(RaisonnementProtocole raisonnementProtocole) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Nom du protocole général
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(raisonnementProtocole == null ? Constantes.STRING_EMPTY : raisonnementProtocole.getProtocole().getNom(), getProtocolePossibles(), null, true, false, true));
        //Libellé de la liste de raisonnement
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(raisonnementProtocole == null ? Constantes.STRING_EMPTY : raisonnementProtocole.getRaisonnement().getListeRaisonnement().getLibelle(), getLibelleListeRaisonnementPossibles(),
                null, true, false, true);
        //Libellé de la valeur du raisonnement
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(raisonnementProtocole == null ? Constantes.STRING_EMPTY : raisonnementProtocole.getRaisonnement().getLibelle(), getRaisonnementPossibles(), null, true, false, true);
        
        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();
        
        refsColonne1.add(colonne2);
        colonne1.setRefs(refsColonne1);
        
        colonne2.setValue(raisonnementProtocole == null ? "" : raisonnementProtocole.getRaisonnement().getLibelle());
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(raisonnementProtocole == null ? Constantes.STRING_EMPTY : raisonnementProtocole.getCommentaire() == null ? Constantes.STRING_EMPTY : raisonnementProtocole.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                int indexpro = tokenizerValues.currentTokenIndex();
                String nomProtocole = tokenizerValues.nextToken();
                int indexli = tokenizerValues.currentTokenIndex();
                String libelleLstRaisonnement = tokenizerValues.nextToken();
                int indexlib = tokenizerValues.currentTokenIndex();
                String libelleRaisonnement = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();
                
                Protocole protocole = protocoleDAO.getByNKey(nomProtocole).orElse(null);
                if (protocole == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "PROTOCOLE_NONDEFINI"), line + 1, indexpro + 1, nomProtocole));
                }
                
                ListeRaisonnement listeRaisonnement = listeRaisonnementDAO.getByNKey(libelleLstRaisonnement).orElse(null);
                if (listeRaisonnement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "LISTERAISONNEMENT_NONDEFINI"), line + 1, indexli + 1, libelleLstRaisonnement));
                }
                
                Raisonnement raisonnement = raisonnementDAO.getByNKey(libelleRaisonnement, listeRaisonnement).orElse(null);
                if (raisonnement == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "RAISONNEMENT_NONDEFINI"), line + 1, indexlib + 1, libelleLstRaisonnement, libelleRaisonnement));
                }
                
                RaisonnementProtocole raisonnementProtocole = new RaisonnementProtocole(raisonnement, protocole, commentaire);
                RaisonnementProtocole dbRaisonnementProtocole = raisonnementProtocoleDAO.getByNKey(raisonnement, protocole).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbRaisonnementProtocole == null) {
                        raisonnementProtocoleDAO.saveOrUpdate(raisonnementProtocole);
                    } else {
                        dbRaisonnementProtocole.setCommentaire(commentaire);
                        raisonnementProtocoleDAO.saveOrUpdate(dbRaisonnementProtocole);
                    }
                }                
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param listeRaisonnementDAO the listeRaisonnementDAO to set
     */
    public void setListeRaisonnementDAO(IListeRaisonnementDAO listeRaisonnementDAO) {
        this.listeRaisonnementDAO = listeRaisonnementDAO;
    }

    /**
     * @param raisonnementDAO the raisonnementDAO to set
     */
    public void setRaisonnementDAO(IRaisonnementDAO raisonnementDAO) {
        this.raisonnementDAO = raisonnementDAO;
    }

    /**
     * @param protocoleDAO the protocoleDAO to set
     */
    public void setProtocoleDAO(IProtocoleDAO protocoleDAO) {
        this.protocoleDAO = protocoleDAO;
    }

    /**
     * @param raisonnementProtocoleDAO the raisonnementProtocoleDAO to set
     */
    public void setRaisonnementProtocoleDAO(
            IRaisonnementProtocoleDAO raisonnementProtocoleDAO) {
        this.raisonnementProtocoleDAO = raisonnementProtocoleDAO;
    }
    
}
