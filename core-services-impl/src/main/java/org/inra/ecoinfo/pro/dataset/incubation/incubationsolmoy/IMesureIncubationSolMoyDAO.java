/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolmoy;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesureIncubationSolMoyDAO<T> extends IDAO<MesureIncubationSolMoy> {

    /**
     *
     * @param key
     * @return
     */
    Optional<MesureIncubationSolMoy> getByKey(String key);
}
