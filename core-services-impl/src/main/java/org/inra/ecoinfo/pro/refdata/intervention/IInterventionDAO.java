/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.intervention;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author vjkoyao
 */
public interface IInterventionDAO extends IDAO<Intervention> {

    /**
     *
     * @return
     */
    List<Intervention> getAll();

    /**
     *
     * @param intervention_nom
     * @return
     */
    Optional<Intervention> getByNKey(String intervention_nom);

}
