/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.entity.MesurePhysicoChimieSolsMoy_;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.moyenne.IMesurePhysicoChimieMoyDAO;

/**
 *
 * @author vjkoyao
 */
public class JPAMesurePhysicoChimieMoyDAO extends AbstractJPADAO<MesurePhysicoChimieSolsMoy> implements IMesurePhysicoChimieMoyDAO<MesurePhysicoChimieSolsMoy> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesurePhysicoChimieSolsMoy> getByKeys(String keymesure) {
        CriteriaQuery<MesurePhysicoChimieSolsMoy> query = builder.createQuery(MesurePhysicoChimieSolsMoy.class);
        Root<MesurePhysicoChimieSolsMoy> m = query.from(MesurePhysicoChimieSolsMoy.class);
        query
                .select(m)
                .where(builder.equal(m.get(MesurePhysicoChimieSolsMoy_.keymesuremoyenne), keymesure));
        return getOptional(query);
    }

}
