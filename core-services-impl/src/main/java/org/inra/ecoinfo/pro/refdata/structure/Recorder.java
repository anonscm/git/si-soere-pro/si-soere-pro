/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structure;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.adresse.Adresse;
import org.inra.ecoinfo.pro.refdata.adresse.IAdresseDAO;
import org.inra.ecoinfo.pro.refdata.commune.Commune;
import org.inra.ecoinfo.pro.refdata.commune.ICommuneDAO;
import org.inra.ecoinfo.pro.refdata.typestructure.ITypeStructureDAO;
import org.inra.ecoinfo.pro.refdata.typestructure.TypeStructure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Structure> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ICommuneDAO communeDAO;
    ITypeStructureDAO typeStructureDAO;
    IAdresseDAO adresseDAO;
    IStructureDAO structureDAO;

    Map<String, String[]> communePossibles = new TreeMap<String, String[]>();

    /**
     * @param adresse
     * @param typeStructure
     * @param noTel
     * @param noFax
     * @param dbStructure
     * @param structure
     * @param estSignataire
     * @param commentaire
     * @throws BusinessException
     */
    private void createOrUpdate(Adresse adresse, TypeStructure typeStructure, String noTel, String noFax, Structure dbStructure, Structure structure, Boolean estSignataire, String commentaire) throws BusinessException {
        try {

            createAdresse(adresse);

            if (dbStructure == null) {
                structureDAO.saveOrUpdate(structure);
            } else {
                //on supprime l'ancienne adresse de la table Adresse si n'est plus utilisée
                List<Structure> lstStructure = structureDAO.getByAdr(dbStructure.getAdresse());
                if (lstStructure != null && lstStructure.size() == 1) {
                    adresseDAO.remove(dbStructure.getAdresse());
                }

                dbStructure.setTypeStructure(typeStructure);
                dbStructure.setAdresse(adresse);
                dbStructure.setNoTel(noTel);
                dbStructure.setNoFax(noFax);
                dbStructure.setEstSignataire(estSignataire);
                dbStructure.setCommentaire(commentaire);
                structureDAO.saveOrUpdate(dbStructure);
            }
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create structure");
        }
    }

    /**
     * // Créer l'adresse si elle n'a pas encore été enrgistrée. // La table
     * Adresse n'est pas remplie au préalable
     *
     * @param adresse
     * @throws PersistenceException
     */
    private void createAdresse(Adresse adresse) throws PersistenceException {
        if (adresse.getId() == null) {
            adresseDAO.saveOrUpdate(adresse);
        }
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getCodePostalPossibles() throws BusinessException {
        final Function<Commune, String> name = Commune::getCodePostal;
        communeDAO.getAllBy(Commune.class, Commune::getCodePostal);
        List<Commune> lstCodePostals = communeDAO.getAll(Commune.class);
        String[] codepostalPossibles = new String[lstCodePostals.size() + 1];
        codepostalPossibles[0] = (String) Constantes.STRING_EMPTY;
        int index = 1;
        for (Commune commune : lstCodePostals) {
            codepostalPossibles[index++] = commune.getCodePostal();
        }
        return codepostalPossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private Map<String, String[]> getCommunePossibles() throws BusinessException {
        communePossibles = new TreeMap<String, String[]>();
        List<Commune> lstCodePostals = communeDAO.getAllBy(Commune.class, Commune::getNomCommune);
        lstCodePostals.stream()
                .map(c -> c.getCodePostal())
                .forEach((cp) -> {
                    List<Commune> lstCommunes = communeDAO.getByCodePostal(cp);
                    String[] communeParCodePostPossibles = new String[lstCommunes.size() + 1];
                    communeParCodePostPossibles[0] = (String) Constantes.STRING_EMPTY;
                    int index = 1;
                    for (Commune commune : lstCommunes) {
                        communeParCodePostPossibles[index++] = commune.getNomCommune();
                    }
                    communePossibles.put(cp, communeParCodePostPossibles);
                });
        return communePossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getTypeStructurePossibles() throws BusinessException {
        List<TypeStructure> lstTypeStructures = typeStructureDAO.getAll(TypeStructure.class);
        String[] typeStructurePossibles = new String[lstTypeStructures.size()];
        int index = 0;
        for (TypeStructure typeStructure : lstTypeStructures) {
            typeStructurePossibles[index++] = typeStructure.getLibelle();
        }

        return typeStructurePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                tokenizerValues.nextToken();
                String nom = tokenizerValues.nextToken();
                String precision = tokenizerValues.nextToken();
                String noNomRue = tokenizerValues.nextToken();
                String codePostal = tokenizerValues.nextToken();
                String nomCommune = tokenizerValues.nextToken();

                Commune commune = communeDAO.getByNKey(nomCommune, codePostal).orElse(null);
                Adresse adresse = adresseDAO.getByNKey(noNomRue, commune).orElse(null);

                List<Structure> lstStructure = structureDAO.getByAdr(adresse);

                structureDAO.remove(structureDAO.getByNKey(nom, precision)
                        .orElseThrow(() -> new BusinessException("can't get structure")));

                // si on supprime la structure et que l'adresse n'est pas utilisée par d'autres structures, alors 
                //on suprime cette adresse de la table Adresse.
                if (lstStructure != null && lstStructure.size() == 1) {
                    adresseDAO.remove(adresse);
                }

                values = parser.getLine();

            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Structure> getAllElements() throws BusinessException {
        return structureDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Structure structure) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Libellé du type de structure
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getTypeStructure().getLibelle(), getTypeStructurePossibles(), null, false, false, true));
        //Nom de la structure
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        //Précision de la structure
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getPrecision(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        //Nom de la rue
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getAdresse().getNoNomRue(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, true));
        //Code postal
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getAdresse().getCommune().getCodePostal(), getCodePostalPossibles(), null, false, false, true);
        //Commune
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getAdresse().getCommune().getNomCommune(), getCommunePossibles(), null, false, false, true);

        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();

        refsColonne1.add(colonne2);
        colonne1.setRefs(refsColonne1);

        colonne2.setValue(structure == null ? "" : structure.getAdresse().getCommune().getNomCommune() == null ? "" : structure.getAdresse().getCommune().getNomCommune());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);

        //Numéro de téléphone
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getNoTel(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        //Numéro de fax
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getNoFax(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        //Est signataire de la convention Réseau PRO
        String estSignataireString = structure == null ? ""
                : (structure.getEstSignataire() == null ? ""
                : (structure.getEstSignataire() ? RefDataConstantes.valBooleanNonObligatoire[1] : RefDataConstantes.valBooleanNonObligatoire[2]));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : estSignataireString, RefDataConstantes.valBooleanNonObligatoire, null, false, false, false));
        //Commentaire
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(structure == null ? Constantes.STRING_EMPTY : structure.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                int indexLibelle = tokenizerValues.currentTokenIndex();
                String libelleTpStr = tokenizerValues.nextToken();
                String nom = tokenizerValues.nextToken();
                String precision = tokenizerValues.nextToken();
                String noNomRue = tokenizerValues.nextToken();
                String codePostal = tokenizerValues.nextToken();
                int indexCommune = tokenizerValues.currentTokenIndex();
                String nomCommune = tokenizerValues.nextToken();
                String noTel = tokenizerValues.nextToken();
                String noFax = tokenizerValues.nextToken();
                int indexEstSignataire = tokenizerValues.currentTokenIndex();
                String estSignataireString = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();

                Commune commune = verifieCommune(nomCommune, codePostal, line + 1, indexCommune + 1, errorsReport);

                Adresse adresse = adresseDAO.getByNKey(noNomRue, commune).orElse(null);
                if (adresse == null) {
                    // nouvelle adresse
                    adresse = new Adresse(noNomRue, commune);
                }

                RefDataUtil.verifieNotelNoFax(noTel, noFax, errorsReport, localizationManager);
                TypeStructure typeStructure = verifieTypeStructure(libelleTpStr, line + 1, indexLibelle + 1, errorsReport);
                verifieEstSignataire(estSignataireString, line + 1, indexEstSignataire + 1, errorsReport);

                Boolean estSignataire = estSignataireString == null ? null : (estSignataireString.equalsIgnoreCase(RefDataConstantes.valBooleanNonObligatoire[1]) ? Boolean.valueOf("true") : Boolean.valueOf("false"));
                Structure structure = new Structure(nom, precision, noTel, noFax, adresse, typeStructure, estSignataire, commentaire);

                Structure dbStructure = structureDAO.getByNKey(nom, precision).orElse(null);

                if (!errorsReport.hasErrors()) {
                    createOrUpdate(adresse, typeStructure, noTel, noFax, dbStructure, structure, estSignataire, commentaire);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param nomCommune
     * @param codePostal
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private Commune verifieCommune(String nomCommune, String codePostal, long line, int index, ErrorsReport errorsReport) {
        Commune commune = communeDAO.getByNKey(nomCommune, codePostal).orElse(null);
        if (commune == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "COMMUNE_NONDEFINI"), line, index, nomCommune + " (" + codePostal + ")"));
        }

        return commune;
    }

    /**
     * @param libelleTpStr
     * @param line
     * @param index
     * @param errorsReport
     * @return
     */
    private TypeStructure verifieTypeStructure(String libelleTpStr, long line, int index, ErrorsReport errorsReport) {
        TypeStructure typeStructure = typeStructureDAO.getByNKey(libelleTpStr).orElse(null);
        if (typeStructure == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "TYPSTRUCT_NONDEFINI"), line, index, libelleTpStr));
        }

        return typeStructure;
    }

    private void verifieEstSignataire(String estSignataireString, long line, int index, ErrorsReport errorsReport) {
        if (estSignataireString != null && !estSignataireString.equalsIgnoreCase(RefDataConstantes.valBooleanNonObligatoire[1]) && !estSignataireString.equalsIgnoreCase(RefDataConstantes.valBooleanNonObligatoire[2])) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "ERROR_BOOLEAN"), line, index, datasetDescriptor.getColumns().get(8).getName()));
        }
    }

    /**
     * @param communeDAO the communeDAO to set
     */
    public void setCommuneDAO(ICommuneDAO communeDAO) {
        this.communeDAO = communeDAO;
    }

    /**
     * @param typeStructureDAO the typeStructureDAO to set
     */
    public void setTypeStructureDAO(ITypeStructureDAO typeStructureDAO) {
        this.typeStructureDAO = typeStructureDAO;
    }

    /**
     * @param adresseDAO the adresseDAO to set
     */
    public void setAdresseDAO(IAdresseDAO adresseDAO) {
        this.adresseDAO = adresseDAO;
    }

    /**
     * @param structureDAO the structureDAO to set
     */
    public void setStructureDAO(IStructureDAO structureDAO) {
        this.structureDAO = structureDAO;
    }

}
