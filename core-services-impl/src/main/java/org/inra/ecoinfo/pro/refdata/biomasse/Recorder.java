/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.biomasse;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.biomasseprelevee.BiomassePrelevee;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractCSVMetadataRecorder<BiomassePrelevee> {

    IBiomasseDAO biomasseDAO;
    private Properties commentEn;

    private void createBiomasse(final BiomassePrelevee especePlante) throws BusinessException {
        try {
            biomasseDAO.saveOrUpdate(especePlante);
            biomasseDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create biomasse");
        }
    }

    private void updateBDBiomasse(String code, String nom, String commentaire, BiomassePrelevee dbespece) throws BusinessException {
        try {
            dbespece.setCode(code);
            dbespece.setNom(nom);
            dbespece.setCommentaire(commentaire);
            biomasseDAO.saveOrUpdate(dbespece);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update biomasse");
        }
    }

    private void createOrUpdateBiomasse(final String code, String nom, String commentaire, final BiomassePrelevee dbespece) throws BusinessException {
        if (dbespece == null) {
            final BiomassePrelevee especePlante = new BiomassePrelevee(nom, code, commentaire);
            especePlante.setCode(code);
            especePlante.setNom(nom);
            especePlante.setCommentaire(commentaire);
            createBiomasse(especePlante);
        } else {
            updateBDBiomasse(code, nom, commentaire, dbespece);
        }
    }

    private void persistBiomasse(final String code, final String nom, String commentaire) throws BusinessException {
        final BiomassePrelevee dbMethode = biomasseDAO.getByName(nom).orElse(null);
        createOrUpdateBiomasse(code, nom, commentaire, dbMethode);
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            final ErrorsReport errorsReport = new ErrorsReport();
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String intitule = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(intitule);
                final BiomassePrelevee dbEspece = biomasseDAO.getByKey(code).orElse(null);
                if (dbEspece != null) {
                    biomasseDAO.remove(dbEspece);
                    values = csvp.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser csvp, File file, String string) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(csvp);
            String[] values = csvp.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, BiomassePrelevee.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                if (!errorsReport.hasErrors()) {
                    persistBiomasse(code, nom, commentaire);
                }
                values = csvp.getLine();
            }

        } catch (final IOException  e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<BiomassePrelevee> getAllElements() throws BusinessException {
        return biomasseDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(BiomassePrelevee biomasse) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(biomasse == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : biomasse.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas()
                .add(new ColumnModelGridMetadata(biomasse == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : biomasse.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(biomasse == null || biomasse.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(biomasse.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<BiomassePrelevee> initModelGridMetadata() {
        commentEn = localizationManager.newProperties(BiomassePrelevee.NAME_ENTITY_JPA, BiomassePrelevee.JPA_COLUMN_COMMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @param biomasseDAO
     */
    public void setBiomasseDAO(IBiomasseDAO biomasseDAO) {
        this.biomasseDAO = biomasseDAO;
    }

}
