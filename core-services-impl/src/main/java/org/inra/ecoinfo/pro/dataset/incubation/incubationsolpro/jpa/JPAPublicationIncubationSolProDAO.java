/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsolpro.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolPro_;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolPro_;

/**
 *
 * @author vjkoyao
 */
public class JPAPublicationIncubationSolProDAO extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurIncubationSolPro> deleteValeurs = builder.createCriteriaDelete(ValeurIncubationSolPro.class);
        Root<ValeurIncubationSolPro> valeur = deleteValeurs.from(ValeurIncubationSolPro.class);
        Subquery<MesureIncubationSolPro> subquery = deleteValeurs.subquery(MesureIncubationSolPro.class);
        Root<MesureIncubationSolPro> m = subquery.from(MesureIncubationSolPro.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureIncubationSolPro_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurIncubationSolPro_.mesureIncubationSolPro).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureIncubationSolPro> deleteSequence = builder.createCriteriaDelete(MesureIncubationSolPro.class);
        Root<MesureIncubationSolPro> sequence = deleteSequence.from(MesureIncubationSolPro.class);
        deleteSequence.where(builder.equal(sequence.get(MesureIncubationSolPro_.versionfile), version));
        delete(deleteSequence);
    }
}
