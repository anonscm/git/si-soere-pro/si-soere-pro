/**
 *
 */
package org.inra.ecoinfo.pro.refdata.placette;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.geolocalisation.Geolocalisation;
import org.inra.ecoinfo.pro.refdata.parcelleelementaire.ParcelleElementaire;

/**
 * @author sophie
 *
 */
public interface IPlacetteDAO extends IDAO<Placette> {

    /**
     *
     * @param code
     * @param parcelleElementaire
     * @return
     */
    Optional<Placette> getByNameAndParcelle(String code, ParcelleElementaire parcelleElementaire);

    Optional<Placette> getByNKey(String code, ParcelleElementaire parcelleElementaire);

    /**
     *
     * @param parcelleElementaire
     * @return
     */
    List<Placette> getByParcelleElt(ParcelleElementaire parcelleElementaire);

    /**
     *
     * @param geolocalisation
     * @return
     */
    Optional<Placette> getByGeolocalisation(Geolocalisation geolocalisation);

}
