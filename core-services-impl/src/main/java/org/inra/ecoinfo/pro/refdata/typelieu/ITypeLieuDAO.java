/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typelieu;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 * @author sophie
 *
 */
public interface ITypeLieuDAO extends IDAO<TypeLieu> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<TypeLieu> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<TypeLieu> getByCode(String code);

}
