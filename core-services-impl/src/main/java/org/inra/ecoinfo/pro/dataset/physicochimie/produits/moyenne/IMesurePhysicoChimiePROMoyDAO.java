/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;

/**
 *
 * @author vjkoyao
 * @param <T>
 */
public interface IMesurePhysicoChimiePROMoyDAO<T> extends IDAO<MesurePhysicoChimiePROMoy> {

    /**
     *
     * @param keymesure
     * @return
     */
    Optional<MesurePhysicoChimiePROMoy> getByKeys(String keymesure);

}
