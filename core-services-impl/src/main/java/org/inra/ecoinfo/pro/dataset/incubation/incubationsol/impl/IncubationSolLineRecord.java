/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.incubation.incubationsol.impl;

import java.time.LocalDate;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.inra.ecoinfo.pro.dataset.impl.VariableValue;

/**
 *
 * @author vjkoyao
 */
public class IncubationSolLineRecord implements Comparable<IncubationSolLineRecord> {

    LocalDate date_prel_sol;
    String codeech;
    LocalDate date_debut_incub;
    int jour_incub;
    int ordre_manip;
    String humidite_incub;
    float temperature_incub;
    float masse_de_sol;
    String condition_incub;
    String code_interne_labo;
    int numero_rep_analyse;
    String labo_analyse;
    float n_mineral;
    Long originalLineNumber;
    String codevariable;
    float valeurvariable;
    String statutvaleur;
    String codemethode;
    String codeunite;
    String codehumidite;
    List<VariableValue> variablesValues;

    /**
     *
     */
    public IncubationSolLineRecord() {
        super();
    }

    /**
     *
     * @param date_prel_sol
     * @param codeech
     * @param date_debut_incub
     * @param jour_incub
     * @param ordre_manip
     * @param humidite_incub
     * @param temperature_incub
     * @param masse_de_sol
     * @param condition_incub
     * @param code_interne_labo
     * @param numero_rep_analyse
     * @param labo_analyse
     * @param n_mineral
     * @param codevariable
     * @param valeurvariable
     * @param statutvaleur
     * @param codemethode
     * @param codeunite
     * @param codehumidite
     */
    public IncubationSolLineRecord(LocalDate date_prel_sol, String codeech, LocalDate date_debut_incub, int jour_incub, int ordre_manip, String humidite_incub, float temperature_incub, float masse_de_sol, String condition_incub, String code_interne_labo, int numero_rep_analyse, String labo_analyse, float n_mineral, String codevariable, float valeurvariable, String statutvaleur, String codemethode, String codeunite, String codehumidite) {
        this.date_prel_sol = date_prel_sol;
        this.codeech = codeech;
        this.date_debut_incub = date_debut_incub;
        this.jour_incub = jour_incub;
        this.ordre_manip = ordre_manip;
        this.humidite_incub = humidite_incub;
        this.temperature_incub = temperature_incub;
        this.masse_de_sol = masse_de_sol;
        this.condition_incub = condition_incub;
        this.code_interne_labo = code_interne_labo;
        this.numero_rep_analyse = numero_rep_analyse;
        this.labo_analyse = labo_analyse;
        this.n_mineral = n_mineral;
        this.codevariable = codevariable;
        this.valeurvariable = valeurvariable;
        this.statutvaleur = statutvaleur;
        this.codemethode = codemethode;
        this.codeunite = codeunite;
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @param line
     */
    public void copy(final IncubationSolLineRecord line) {
        this.date_prel_sol = line.getDate_prel_sol();
        this.codeech = line.getCodeech();
        this.date_debut_incub = line.getDate_debut_incub();
        this.jour_incub = line.getJour_incub();
        this.ordre_manip = line.getOrdre_manip();
        this.humidite_incub = line.getHumidite_incub();
        this.temperature_incub = line.getTemperature_incub();
        this.masse_de_sol = line.getMasse_de_sol();
        this.condition_incub = line.getCondition_incub();
        this.code_interne_labo = line.getCode_interne_labo();
        this.numero_rep_analyse = line.getNumero_rep_analyse();
        this.labo_analyse = line.getLabo_analyse();
        this.n_mineral = line.getN_mineral();
        this.codevariable = line.getCodevariable();
        this.valeurvariable = line.getValeurvariable();
        this.statutvaleur = line.getStatutvaleur();
        this.codemethode = line.getCodemethode();
        this.codeunite = line.getCodeunite();
        this.codehumidite = line.getCodehumidite();
    }

    @Override
    public int compareTo(IncubationSolLineRecord o) {
        int returnValue = -1;
        if (this.equals(o)) {
            assert this.equals(o) : "compareTo inconsistent with equals.";
            returnValue = 0;
        } else {
            returnValue = this.originalLineNumber < o.getOriginalLineNumber() ? 1 : -1;
        }
        return returnValue;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_prel_sol() {
        return date_prel_sol;
    }

    /**
     *
     * @param date_prel_sol
     */
    public void setDate_prel_sol(LocalDate date_prel_sol) {
        this.date_prel_sol = date_prel_sol;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_debut_incub() {
        return date_debut_incub;
    }

    /**
     *
     * @param date_debut_incub
     */
    public void setDate_debut_incub(LocalDate date_debut_incub) {
        this.date_debut_incub = date_debut_incub;
    }

    /**
     *
     * @return
     */
    public int getJour_incub() {
        return jour_incub;
    }

    /**
     *
     * @param jour_incub
     */
    public void setJour_incub(int jour_incub) {
        this.jour_incub = jour_incub;
    }

    /**
     *
     * @return
     */
    public String getCodeech() {
        return codeech;
    }

    /**
     *
     * @param codeech
     */
    public void setCodeech(String codeech) {
        this.codeech = codeech;
    }

    /**
     *
     * @return
     */
    public String getHumidite_incub() {
        return humidite_incub;
    }

    /**
     *
     * @param humidite_incub
     */
    public void setHumidite_incub(String humidite_incub) {
        this.humidite_incub = humidite_incub;
    }

    /**
     *
     * @return
     */
    public float getTemperature_incub() {
        return temperature_incub;
    }

    /**
     *
     * @param temperature_incub
     */
    public void setTemperature_incub(float temperature_incub) {
        this.temperature_incub = temperature_incub;
    }

    /**
     *
     * @return
     */
    public float getMasse_de_sol() {
        return masse_de_sol;
    }

    /**
     *
     * @param masse_de_sol
     */
    public void setMasse_de_sol(float masse_de_sol) {
        this.masse_de_sol = masse_de_sol;
    }

    /**
     *
     * @return
     */
    public String getCondition_incub() {
        return condition_incub;
    }

    /**
     *
     * @param condition_incub
     */
    public void setCondition_incub(String condition_incub) {
        this.condition_incub = condition_incub;
    }

    /**
     *
     * @return
     */
    public String getCode_interne_labo() {
        return code_interne_labo;
    }

    /**
     *
     * @param code_interne_labo
     */
    public void setCode_interne_labo(String code_interne_labo) {
        this.code_interne_labo = code_interne_labo;
    }

    /**
     *
     * @return
     */
    public int getNumero_rep_analyse() {
        return numero_rep_analyse;
    }

    /**
     *
     * @param numero_rep_analyse
     */
    public void setNumero_rep_analyse(int numero_rep_analyse) {
        this.numero_rep_analyse = numero_rep_analyse;
    }

    /**
     *
     * @return
     */
    public String getLabo_analyse() {
        return labo_analyse;
    }

    /**
     *
     * @param labo_analyse
     */
    public void setLabo_analyse(String labo_analyse) {
        this.labo_analyse = labo_analyse;
    }

    /**
     *
     * @return
     */
    public float getN_mineral() {
        return n_mineral;
    }

    /**
     *
     * @param n_mineral
     */
    public void setN_mineral(float n_mineral) {
        this.n_mineral = n_mineral;
    }

    /**
     *
     * @return
     */
    public Long getOriginalLineNumber() {
        return originalLineNumber;
    }

    /**
     *
     * @param originalLineNumber
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     *
     * @return
     */
    public String getCodevariable() {
        return codevariable;
    }

    /**
     *
     * @param codevariable
     */
    public void setCodevariable(String codevariable) {
        this.codevariable = codevariable;
    }

    /**
     *
     * @return
     */
    public float getValeurvariable() {
        return valeurvariable;
    }

    /**
     *
     * @param valeurvariable
     */
    public void setValeurvariable(float valeurvariable) {
        this.valeurvariable = valeurvariable;
    }

    /**
     *
     * @return
     */
    public String getStatutvaleur() {
        return statutvaleur;
    }

    /**
     *
     * @param statutvaleur
     */
    public void setStatutvaleur(String statutvaleur) {
        this.statutvaleur = statutvaleur;
    }

    /**
     *
     * @return
     */
    public String getCodemethode() {
        return codemethode;
    }

    /**
     *
     * @param codemethode
     */
    public void setCodemethode(String codemethode) {
        this.codemethode = codemethode;
    }

    /**
     *
     * @return
     */
    public String getCodeunite() {
        return codeunite;
    }

    /**
     *
     * @param codeunite
     */
    public void setCodeunite(String codeunite) {
        this.codeunite = codeunite;
    }

    /**
     *
     * @return
     */
    public String getCodehumidite() {
        return codehumidite;
    }

    /**
     *
     * @param codehumidite
     */
    public void setCodehumidite(String codehumidite) {
        this.codehumidite = codehumidite;
    }

    /**
     *
     * @return
     */
    public List<VariableValue> getVariablesValues() {
        return variablesValues;
    }

    /**
     *
     * @param variablesValues
     */
    public void setVariablesValues(List<VariableValue> variablesValues) {
        this.variablesValues = variablesValues;
    }

    /**
     *
     * @return
     */
    public int getOrdre_manip() {
        return ordre_manip;
    }

    /**
     *
     * @param ordre_manip
     */
    public void setOrdre_manip(int ordre_manip) {
        this.ordre_manip = ordre_manip;
    }

}
