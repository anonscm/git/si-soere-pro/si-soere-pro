/**
 *
 */
package org.inra.ecoinfo.pro.refdata.modalite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.facteur.Facteur;
import org.inra.ecoinfo.pro.refdata.facteur.IFacteurDAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.ITypeFacteurDAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Modalite> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    ITypeFacteurDAO typeFacteurDAO;
    IFacteurDAO facteurDAO;
    IModaliteDAO modaliteDAO;
    /*ITypeNomenclatureDAO typenomenclatureDAO;
    protected INomenclatureDAO nomenDAO;

    Map<String, String[]> nomenPossibles = new TreeMap<String, String[]>();
     */
    Map<String, String[]> facteursPossibles = new TreeMap<String, String[]>();
    //Boolean nKeyNomen = false;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                Facteur facteur = null;
                Modalite modalite = null;

                TokenizerValues tokenizerValues = new TokenizerValues(values);
                tokenizerValues.nextToken();
                String libelleFacteur = tokenizerValues.nextToken();
                String valeur = tokenizerValues.nextToken();
                facteur = facteurDAO.getByNKey(libelleFacteur).orElse(null);
                modalite = modaliteDAO.getByValeurFacteur(valeur, facteur)
                        .orElseThrow(() -> new BusinessException("can(t get modalite"));
                modaliteDAO.remove(modalite);

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return @throws BusinessException
     */
    private String[] getLibelleTypeFacteurPossibles() {
        List<TypeFacteur> lstTypeFacteurs = typeFacteurDAO.getAll();

        String[] typeFacteurPossibles = new String[lstTypeFacteurs.size()];
        int index = 0;
        for (TypeFacteur typeFacteur : lstTypeFacteurs) {
            typeFacteurPossibles[index++] = typeFacteur.getLibelle();
        }
        return typeFacteurPossibles;
    }

    /**
     * @return @throws BusinessException
     */
    private Map<String, String[]> getLibelleFacteurPossibles() {
        List<TypeFacteur> lstTypeFacteurs = typeFacteurDAO.getAll();

        lstTypeFacteurs.forEach((typeFacteur) -> {
            List<Facteur> lstFacteurs = facteurDAO.getLstFacteurByTPFacteur(typeFacteur);
            String[] tpFacteurPossibles = new String[lstFacteurs.size()];
            int index = 0;
            for (Facteur facteur : lstFacteurs) {
                tpFacteurPossibles[index++] = facteur.getLibelle();
            }

            facteursPossibles.put(typeFacteur.getLibelle(), tpFacteurPossibles);
        });

        return facteursPossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<Modalite> getAllElements() throws BusinessException {
        return modaliteDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Modalite modalite) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //type de facteur
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(modalite == null
                ? Constantes.STRING_EMPTY : modalite.getFacteur().getTypeFacteur().getLibelle(), getLibelleTypeFacteurPossibles(), null, true, false, true);

        //facteur
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(modalite == null
                ? Constantes.STRING_EMPTY : modalite.getFacteur().getLibelle(), getLibelleFacteurPossibles(), null, true, false, true);

        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();

        refsColonne1.add(colonne2);

        colonne1.setRefs(refsColonne1);
        colonne2.setValue(modalite == null ? "" : modalite.getFacteur() == null ? "" : modalite.getFacteur().getLibelle());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(modalite == null ? Constantes.STRING_EMPTY : modalite.getValeur(),
                ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(modalite == null || modalite.getCommentaire() == null ? Constantes.STRING_EMPTY : modalite.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /**
     * @param errorsReport
     * @param facteur
     * @param valeur
     * @param nomen
     * @param commentaire
     * @throws BusinessException
     * @throws BusinessException
     */
    private void createOrupdateModalite(ErrorsReport errorsReport, Facteur facteur, String valeur, String commentaire) throws BusinessException, BusinessException {
        Modalite modalite = new Modalite(facteur, valeur, commentaire); //new Modalite(facteur, typeNomenclature, nomen, valeur, commentaire);

        Modalite dbModalite = modaliteDAO.getByValeurFacteur(valeur, facteur).orElse(null);
        try {
            if (dbModalite != null) {
                dbModalite.setFacteur(facteur);
                dbModalite.setValeur(valeur);
                dbModalite.setCommentaire(commentaire);
                modaliteDAO.saveOrUpdate(dbModalite);
            } else {
                modaliteDAO.saveOrUpdate(modalite);
            }
        } catch (PersistenceException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);
            String[] values = null;
            values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Modalite.TABLE_NAME);

                int indexTypeFac = tokenizerValues.currentTokenIndex();
                String libelleTypeFacteur = tokenizerValues.nextToken();
                int indexFac = tokenizerValues.currentTokenIndex();
                String libelleFacteur = tokenizerValues.nextToken();
                String valeur = tokenizerValues.nextToken();
                String commentaire = tokenizerValues.nextToken();

                verifieTypeFacteur(libelleTypeFacteur, line + 1, indexTypeFac + 1, errorsReport);
                Facteur facteur = verifieFacteur(libelleFacteur, line + 1, indexFac + 1, errorsReport);

                String valeurModalite = (valeur == null) ? "" : valeur;

                if (!errorsReport.hasErrors()) {
                    createOrupdateModalite(errorsReport, facteur, valeurModalite, commentaire);
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        }
    }

    /**
     * @param libelleTypeFacteur
     * @param line
     * @param index
     * @param errorsReport
     * @throws BusinessException
     */
    private void verifieTypeFacteur(String libelleTypeFacteur, long line, int index, ErrorsReport errorsReport) throws BusinessException {
        TypeFacteur typeFacteur = typeFacteurDAO.getByNKey(libelleTypeFacteur).orElse(null);
        if (typeFacteur == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "TYPEFACTEUR_NONDEFINI"), line, index, libelleTypeFacteur));
        }
    }

    /**
     * @param libelleFacteur
     * @param line
     * @param index
     * @param errorsReport
     * @return
     * @throws BusinessException
     */
    private Facteur verifieFacteur(String libelleFacteur, long line, int index, ErrorsReport errorsReport) throws BusinessException {
        Facteur facteur = facteurDAO.getByNKey(libelleFacteur).orElse(null);
        if (facteur == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "FACTEUR_NONDEFINI"), line, index, libelleFacteur));
        }

        return facteur;
    }

    /**
     * @param typeFacteurDAO the typeFacteurDAO to set
     */
    public void setTypeFacteurDAO(ITypeFacteurDAO typeFacteurDAO) {
        this.typeFacteurDAO = typeFacteurDAO;
    }

    /**
     * @param facteurDAO the facteurDAO to set
     */
    public void setFacteurDAO(IFacteurDAO facteurDAO) {
        this.facteurDAO = facteurDAO;
    }

    /**
     * @param modaliteDAO the modaliteDAO to set
     */
    public void setModaliteDAO(IModaliteDAO modaliteDAO) {
        this.modaliteDAO = modaliteDAO;
    }
}
