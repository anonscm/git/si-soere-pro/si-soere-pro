/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.produits.moyenne;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePROMoy;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePROMoy;

/**
 *
 * @author adiankha
 */
public interface IValeurProduitMoyenneDAO extends IDAO<ValeurPhysicoChimiePROMoy> {

    /**
     *
     * @param dvum
     * @param mesuremoyenne
     * @param ecartype
     * @return
     */
    Optional<ValeurPhysicoChimiePROMoy> getByNKeys(RealNode dvum, MesurePhysicoChimiePROMoy mesuremoyenne, Float ecartype);

}
