/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.echantillonsproduit;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPAEchantillonsProduitDAO extends AbstractJPADAO<EchantillonsProduit> implements IEchantillonsProduitDAO {

    /**
     *
     * @return
     */
    @Override
    public List<EchantillonsProduit> getAll() {
        return getAllBy(EchantillonsProduit.class, EchantillonsProduit::getCode_unique);
    }

    /**
     *
     * @param codeechantillon
     * @return
     */
    @Override
    public Optional<EchantillonsProduit> getByCode(String codeechantillon) {
        CriteriaQuery<EchantillonsProduit> query = builder.createQuery(EchantillonsProduit.class);
        Root<EchantillonsProduit> echantillonsProduit = query.from(EchantillonsProduit.class);
        query
                .select(echantillonsProduit)
                .where(
                        builder.equal(echantillonsProduit.get(EchantillonsProduit_.codelabo), codeechantillon)
                );
        return getOptional(query);
    }

    /**
     *
     * @param code_unique
     * @return
     */
    @Override
    public Optional<EchantillonsProduit> getByECH(String code_unique) {
        CriteriaQuery<EchantillonsProduit> query = builder.createQuery(EchantillonsProduit.class);
        Root<EchantillonsProduit> echantillonsProduit = query.from(EchantillonsProduit.class);
        query
                .select(echantillonsProduit)
                .where(
                        builder.equal(echantillonsProduit.get(EchantillonsProduit_.code_unique), code_unique)
                );
        return getOptional(query);
    }

    /**
     *
     * @param numero
     * @return
     */
    @Override
    public Optional<EchantillonsProduit> getByNumEchantillon(long numero) {
        CriteriaQuery<EchantillonsProduit> query = builder.createQuery(EchantillonsProduit.class);
        Root<EchantillonsProduit> echantillonsProduit = query.from(EchantillonsProduit.class);
        query
                .select(echantillonsProduit)
                .where(
                        builder.equal(echantillonsProduit.get(EchantillonsProduit_.numeroechantillon), numero)
                );
        return getOptional(query);
    }
}
