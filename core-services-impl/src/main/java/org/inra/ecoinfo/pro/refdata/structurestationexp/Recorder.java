/**
 *
 */
package org.inra.ecoinfo.pro.refdata.structurestationexp;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.IStationExperimentaleDAO;
import org.inra.ecoinfo.pro.refdata.stationexperimentale.StationExperimentale;
import org.inra.ecoinfo.pro.refdata.structure.IStructureDAO;
import org.inra.ecoinfo.pro.refdata.structure.Structure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<StructureStationExperimentale> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IStructureDAO structureDAO;
    IStationExperimentaleDAO stationExperimentaleDAO;
    IStructureStationExpDAO structureStationExpDAO;

    /**
     * @return @throws PersistenceException
     */
    private String[] getNomStructurePossibles() {
        List<Structure> lstStructures = structureDAO.getAll();

        List<String> lstNomStructures = new ArrayList<String>();

        lstStructures.stream().filter((structure) -> (!lstNomStructures.contains(structure.getNom()))).forEachOrdered((structure) -> {
            lstNomStructures.add(structure.getNom());
        });

        String[] nomStructurePossibles = new String[lstNomStructures.size()];
        int index = 0;
        for (String nomStructure : lstNomStructures) {
            nomStructurePossibles[index++] = nomStructure;
        }
        return nomStructurePossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private Map<String, String[]> getPrecisionStructurePossibles() {
        Map<String, String[]> precisionPossibles = new TreeMap<String, String[]>();

        String[] nomStructurePossibles = getNomStructurePossibles();
        List<String> lstNomStructures = Arrays.asList(nomStructurePossibles);
        lstNomStructures.forEach((nomStructure) -> {
            List<Structure> lstPrecisionStructures = structureDAO.getByNom(nomStructure);
            String[] precisionParNomPossibles = new String[lstPrecisionStructures.size()];
            int index = 0;
            for (Structure structure : lstPrecisionStructures) {
                precisionParNomPossibles[index++] = structure.getPrecision();
            }

            precisionPossibles.put(nomStructure, precisionParNomPossibles);
        });

        return precisionPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] getStationExperimentalePossibles() {
        List<StationExperimentale> lstStationExperimentales = stationExperimentaleDAO.getAll(StationExperimentale.class);
        String[] stationExperimentalePossibles = new String[lstStationExperimentales.size()];
        int index = 0;
        for (StationExperimentale stationExperimentale : lstStationExperimentales) {
            stationExperimentalePossibles[index++] = stationExperimentale.getNom();
        }

        return stationExperimentalePossibles;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nomStructure = tokenizerValues.nextToken();
                String precisionStructure = tokenizerValues.nextToken();
                String nomStationExp = tokenizerValues.nextToken();

                Structure structure = structureDAO.getByNKey(nomStructure, precisionStructure).orElse(null);
                StationExperimentale stationExperimentale = stationExperimentaleDAO.getByNKey(nomStationExp).orElse(null);

                StructureStationExperimentale structureStationExperimentale = structureStationExpDAO.getByNKey(structure, stationExperimentale)
                        .orElseThrow(() -> new BusinessException("can't get structure station experimentale"));

                structureStationExpDAO.remove(structureStationExperimentale);

                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<StructureStationExperimentale> getAllElements() throws BusinessException {
        return structureStationExpDAO.getAll(StructureStationExperimentale.class);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StructureStationExperimentale structureStationExperimentale) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        //Nom structure
        ColumnModelGridMetadata colonne1 = new ColumnModelGridMetadata(structureStationExperimentale == null ? Constantes.STRING_EMPTY : structureStationExperimentale.getStructure().getNom(), getNomStructurePossibles(), null, true, false, true);
        //Précision structure
        ColumnModelGridMetadata colonne2 = new ColumnModelGridMetadata(structureStationExperimentale == null ? Constantes.STRING_EMPTY : structureStationExperimentale.getStructure().getPrecision(), getPrecisionStructurePossibles(), null, true,
                false, true);

        List<ColumnModelGridMetadata> refsColonne1 = new LinkedList<ColumnModelGridMetadata>();

        refsColonne1.add(colonne2);
        colonne1.setRefs(refsColonne1);

        colonne2.setValue(structureStationExperimentale == null ? "" : structureStationExperimentale.getStructure().getPrecision());

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne1);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(colonne2);

        //Nom de la station expérimentale
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(structureStationExperimentale == null ? Constantes.STRING_EMPTY : structureStationExperimentale.getStationExperimentale().getNom(), getStationExperimentalePossibles(), null, true, false, true));

        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                int indexstru = tokenizerValues.currentTokenIndex();
                String nomStructure = tokenizerValues.nextToken();
                String precisionStructure = tokenizerValues.nextToken();
                int indexstation = tokenizerValues.currentTokenIndex();
                String nomStationExp = tokenizerValues.nextToken();

                Structure structure = structureDAO.getByNKey(nomStructure, precisionStructure).orElse(null);
                if (structure == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "STRUCT_NONDEFINI"), line + 1, indexstru + 1, nomStructure + " - " + precisionStructure));
                }

                StationExperimentale stationExperimentale = stationExperimentaleDAO.getByNKey(nomStationExp).orElse(null);
                if (stationExperimentale == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "STATIONEXP_NONDEFINI"), line + 1, indexstation + 1, nomStationExp));
                }

                StructureStationExperimentale structureStationExperimentale = new StructureStationExperimentale(structure, stationExperimentale);
                StructureStationExperimentale dbStructureStationExperimentale = structureStationExpDAO.getByNKey(structure, stationExperimentale).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbStructureStationExperimentale == null) {
                        structureStationExpDAO.saveOrUpdate(structureStationExperimentale);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param structureDAO the structureDAO to set
     */
    public void setStructureDAO(IStructureDAO structureDAO) {
        this.structureDAO = structureDAO;
    }

    /**
     * @param stationExperimentaleDAO the stationExperimentaleDAO to set
     */
    public void setStationExperimentaleDAO(
            IStationExperimentaleDAO stationExperimentaleDAO) {
        this.stationExperimentaleDAO = stationExperimentaleDAO;
    }

    /**
     * @param structureStationExpDAO the structureStationExpDAO to set
     */
    public void setStructureStationExpDAO(
            IStructureStationExpDAO structureStationExpDAO) {
        this.structureStationExpDAO = structureStationExpDAO;
    }

}
