/*
 *
 */
package org.inra.ecoinfo.pro.refdata.typecaracteristique;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

// TODO: Auto-generated Javadoc
/**
 * The Class Recorder.
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeCaracteristiques> {

    /**
     *
     */
    protected ITypeCaracteristiqueDAO typecaracteristiqueDAO;

    private Properties TCNomEN;
    private Properties commentEn;

    private void createOrUpdateTypeCaracteristiques(final String code, String nom, String commentaire, final TypeCaracteristiques dbTypecaracteristiques) throws BusinessException {
        if (dbTypecaracteristiques == null) {
            final TypeCaracteristiques typecaracteristiques = new TypeCaracteristiques(nom, commentaire);
            typecaracteristiques.setTcar_code(code);
            typecaracteristiques.setTcar_nom(nom);
            typecaracteristiques.setCommentaire(commentaire);
            createTypeCaracteristiques(typecaracteristiques);
        } else {
            updateBDTypeCaracteristiques(commentaire, dbTypecaracteristiques);
        }
    }

    private void createTypeCaracteristiques(final TypeCaracteristiques typecaracteristiques) throws BusinessException {
        try {
            typecaracteristiqueDAO.saveOrUpdate(typecaracteristiques);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create typecaracteristiques");
        }
    }

    private void updateBDTypeCaracteristiques(final String commentaire, final TypeCaracteristiques dbTypecaracteristiques) throws BusinessException {
        try {
            dbTypecaracteristiques.setCommentaire(commentaire);
            typecaracteristiqueDAO.saveOrUpdate(dbTypecaracteristiques);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update typecaracteristiques");
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, TypeCaracteristiques.JPA_ENTITY_NAME);
                final String nom = tokenizerValues.nextToken();
                final TypeCaracteristiques dbTcar = typecaracteristiqueDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("can't get type caracteristique"));
                typecaracteristiqueDAO.remove(dbTcar);
                values = parser.getLine();
            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    public List<TypeCaracteristiques> getAllElements() {
        return typecaracteristiqueDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeCaracteristiques typecaracteristiques) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typecaracteristiques == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typecaracteristiques.getTcar_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typecaracteristiques == null || typecaracteristiques.getTcar_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : TCNomEN.getProperty(typecaracteristiques.getTcar_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typecaracteristiques == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : typecaracteristiques.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(typecaracteristiques == null || typecaracteristiques.getTcar_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentEn.getProperty(typecaracteristiques.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public Properties getTCNomEN() {
        return TCNomEN;
    }

    /**
     *
     * @return
     */
    public ITypeCaracteristiqueDAO getTypecaracteristiqueDAO() {
        return typecaracteristiqueDAO;
    }

    @Override
    public ModelGridMetadata<TypeCaracteristiques> initModelGridMetadata() {
        TCNomEN = localizationManager.newProperties(TypeCaracteristiques.JPA_ENTITY_NAME, TypeCaracteristiques.JPA_COLUMN_NAME, Locale.ENGLISH);
        commentEn = localizationManager.newProperties(TypeCaracteristiques.JPA_ENTITY_NAME, TypeCaracteristiques.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void persistTypeCaracteristiques(final String code, final String nom, String commentaire) throws BusinessException {
        final TypeCaracteristiques dbtTypeCaracteristiques = typecaracteristiqueDAO.getByNKey(nom).orElse(null);
        createOrUpdateTypeCaracteristiques(code, nom, commentaire, dbtTypeCaracteristiques);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, TypeCaracteristiques.JPA_ENTITY_NAME);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                final String commentaire = tokenizerValues.nextToken();
                persistTypeCaracteristiques(code, nom, commentaire);
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param tCNomEN
     */
    public void setTCNomEN(Properties tCNomEN) {
        TCNomEN = tCNomEN;
    }

    /**
     *
     * @param typecaracteristiqueDAO
     */
    public void setTypecaracteristiqueDAO(ITypeCaracteristiqueDAO typecaracteristiqueDAO) {
        this.typecaracteristiqueDAO = typecaracteristiqueDAO;
    }

}
