/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.itinerairetechnique.apport.impl;

import java.io.PrintStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.MesureApport;
import org.inra.ecoinfo.pro.dataset.itk.interventionapport.entity.ValeurApport;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.IITKDatatypeManager;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.AbstractITKOutputBuilder;
import org.inra.ecoinfo.pro.extraction.itinerairetechnique.impl.ITKExtractor;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.descriptiontraitement.DescriptionTraitement;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class ApportBuildOutputDisplayByRow extends AbstractITKOutputBuilder<MesureApport> {

    private static final int TEN = 10;

    /**
     *
     */
    protected static final String HEADER_RAW_DATA = "PROPERTY_HEADER_RAW_DATA_APPORT";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.itk.itk-messages";

    static final String MSG_MISSING_VARIABLE_IN_REFERENCES_DATAS = "PROPERTY_MSG_UNAVAILABLE_VARIABLE";

    static final String PATTERN_WORD = "%s";
    static final String PATTERN_FLOAT_FORMATTED = "%.3f";

    static final String PATTERB_CSV_27_FIELD = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s";

    static final String CST_NEW_LINE = "\n";

    static final String PATTERN_CSV_FIELD = ";%s";

    static final String LIST_COLUMN_VARIABLE_APPORT = "LIST_COLUMN_VARIABLE_APPORT";

    static final String CST_CVS_6_EMPTY_FIELD = ";;;;;;";

    static final String CST_0 = "0";

    final ComparatorVariable comparator = new ComparatorVariable();

    IVariablesPRODAO variPRODAO;

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return String.format(
                ApportBuildOutputDisplayByRow.PATTERN_WORD,
                this.getLocalizationManager().getMessage(
                        ApportBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                        ApportBuildOutputDisplayByRow.HEADER_RAW_DATA));
    }

     @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        if (((DefaultParameter) parameters).getResults().get(ITKExtractor.CST_RESULT_EXTRACTION_APPORT_CODE)
                .get(ApportExtractor.APPORT) == null
                || ((DefaultParameter) parameters).getResults()
                .get(ITKExtractor.CST_RESULT_EXTRACTION_APPORT_CODE)
                .get(ApportExtractor.MAP_INDEX_APPORT).isEmpty()) {
            return null;
        }
        ((DefaultParameter) parameters).getFilesMaps()
                .add(super.buildOutput(parameters, ITKExtractor.CST_RESULT_EXTRACTION_APPORT_CODE));
        return null;
    }

    /**
     *
     * @param mesuresApport
     * @param mesuresRecolteCoupesMap
     * @throws ParseException
     */
    @Override
    protected void buildmap(List<MesureApport> mesuresApport, SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureApport>>> mesuresRecolteCoupesMap) throws ParseException {
        java.util.Iterator<MesureApport> itMesure = mesuresApport
                .iterator();
        while (itMesure.hasNext()) {
            MesureApport mesureapport = itMesure
                    .next();
            DescriptionTraitement echan = mesureapport.getDescriptionTraitement();
            Long siteId = echan.getDispositif().getId();
            if (mesuresRecolteCoupesMap.get(siteId) == null) {
                mesuresRecolteCoupesMap.put(siteId,
                        new TreeMap<>());
            }
            if (mesuresRecolteCoupesMap.get(siteId).get(echan) == null) {
                mesuresRecolteCoupesMap.get(siteId).put(echan,
                        new TreeMap<>());
            }
            mesuresRecolteCoupesMap.get(siteId).get(echan).put(mesureapport.getDatedebut(), mesureapport);
            itMesure.remove();
        }

    }

    /**
     *
     * @param selectedDispositifs
     * @param selectedRecolteCoupeVariables
     * @param selectedIntervalDate
     * @param outputPrintStreamMap
     * @param mesuresApportsMap
     */
    protected void readMap(
            final List<Dispositif> selectedDispositifs,
            final List<VariablesPRO> selectedRecolteCoupeVariables,
            final IntervalDate selectedIntervalDate,
            final Map<String, PrintStream> outputPrintStreamMap,
            final SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureApport>>> mesuresApportsMap) {
        try {
            BuildDataLine(selectedDispositifs, outputPrintStreamMap, mesuresApportsMap, selectedIntervalDate);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }

    }

    private void BuildDataLine(List<Dispositif> selectedDispositifs,
            Map<String, PrintStream> outputPrintStreamMap,
            SortedMap<Long, SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureApport>>> mesuresApportsMap, IntervalDate selectedIntervalDate) {
        try {
            String currentDispositif;
            String curentplacette;
            String currentTraitement;
            String currentParcelle;
            LocalDate datefin;
            String typeintervention;
            String interventionapport;
            String commercial;
            String materielapport;
            String culture;
            String bbch;
            String stade;
            String typeobs;
            String observation;
             String niveau;
            String commentaire;

            List<String> listVariable = Arrays.asList(this.getLocalizationManager().getMessage(
                    ApportBuildOutputDisplayByRow.BUNDLE_SOURCE_PATH,
                    ApportBuildOutputDisplayByRow.LIST_COLUMN_VARIABLE_APPORT).split(";"));
            for (final Dispositif dispositif : selectedDispositifs) {
                PrintStream out = outputPrintStreamMap.get(getDispositifDatatypeKey(dispositif));
                final SortedMap<DescriptionTraitement, SortedMap<LocalDate, MesureApport>> mesureApportsMapByDisp = mesuresApportsMap
                        .get(dispositif.getId());
                if (mesureApportsMapByDisp == null) {
                    continue;
                }
                Iterator<Entry<DescriptionTraitement, SortedMap<LocalDate, MesureApport>>> itEchan = mesureApportsMapByDisp
                        .entrySet().iterator();
                while (itEchan.hasNext()) {
                    java.util.Map.Entry<DescriptionTraitement, SortedMap<LocalDate, MesureApport>> echanEntry = itEchan
                            .next();
                    DescriptionTraitement echantillon = echanEntry.getKey();
                    SortedMap<LocalDate, MesureApport> mesureProduitBrutsMap = echanEntry.getValue()
                            .subMap(selectedIntervalDate.getBeginDate().toLocalDate(), selectedIntervalDate.getEndDate().toLocalDate());
                    for (Entry<LocalDate, MesureApport> entrySet : mesureProduitBrutsMap.entrySet()) {
                        LocalDate date = entrySet.getKey();
                        MesureApport mesureApport = entrySet.getValue();
                        currentDispositif = mesureApport.getCodedispositif();
                        currentTraitement = mesureApport.getDescriptionTraitement().getCode();
                        currentParcelle = mesureApport.getNomparcelle();
                        curentplacette = mesureApport.getNomplacette();
                        datefin = mesureApport.getDatefin();
                        typeintervention = mesureApport.getTypeintervention();
                        interventionapport = mesureApport.getIntervention();
                         commercial = mesureApport.getNomcommercialproduit();
                        materielapport = mesureApport.getMaterielapport();
                        niveau = mesureApport.getNiveauatteint();
                        culture = mesureApport.getNomculture();
                        bbch = mesureApport.getCodebbch();
                        stade = mesureApport.getPrecisionstade();

                        typeobs = mesureApport.getTypeobservation();
                        observation = mesureApport.getNomobservation();
                        commentaire = mesureApport.getCommentaire();
                        String genericPattern = LineDataFixe(date, currentDispositif, currentTraitement, currentParcelle, curentplacette,
                                datefin, typeintervention, interventionapport, commercial, materielapport,  culture,
                                bbch, stade, typeobs, observation,niveau, commentaire);
                        out.print(genericPattern);
                        LineDataVariable(mesureApport, out, listVariable);
                    }
                    itEchan.remove();
                }
            }
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.debug(e.getMessage(), e);
        }
    }

    private String LineDataFixe(LocalDate date, String currentDispositif, String currentTraitement, String currentParcelle,
            String courentPlacette, LocalDate datefin,String typeintervention, String interventionapport, String commercial, String materiel1, 
            String culture, String bbch, String stade,
            String typeobs, String observation,String niveau,
            String commentaire) {
        String genericPattern = String.format(PATTERB_CSV_27_FIELD,
                DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                currentDispositif,
                currentTraitement,
                currentParcelle,
                courentPlacette,
                datefin,typeintervention, interventionapport, commercial,materiel1,
                culture,
                bbch, stade, typeobs, observation, niveau, commentaire);
        return genericPattern;
    }

    private void LineDataVariable(MesureApport mesureApport, PrintStream out, List<String> listVariable) {
        final List<ValeurApport> valeurapport = mesureApport.getValeurapport();
        listVariable.forEach((variableColumnName) -> {
            for (Iterator<ValeurApport> ValeurIterator = valeurapport.iterator(); ValeurIterator.hasNext();) {
                ValeurApport valeur = ValeurIterator.next();
                if (!((DatatypeVariableUnitePRO)valeur.getRealNode().getNodeable()).getVariablespro().getCode().equals(Utils.createCodeFromString(variableColumnName))) {
                    continue;
                }
                String line;
                line = String.format(";%s", getValeurToString(valeur));
                out.print(line);
                ValeurIterator.remove();
            }
        });
        out.println();
    }

    /**
     *
     * @return
     */
    @Override
    protected String getIndexResults() {
        return ApportExtractor.MAP_INDEX_APPORT;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeName() {
        return IITKDatatypeManager.CODE_DATATYPE_APPORT;
    }

    private String getValeurToString(ValeurApport valeur) {
        if (valeur.getValeur() != null) {
            return valeur.getValeur().toString();
        } else if (valeur.getListeItineraire() != null) {
            return valeur.getListeItineraire().getListe_valeur();
        } else {
            return "NA";
        }
    }

    /**
     *
     * @param variPRODAO
     */
    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    @Override
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        super.setDatatypeDAO(datatypeDAO); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        super.setConfiguration(configuration); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager); //To change body of generated methods, choose Tools | Templates.
    }
    static class ErrorsReport {
        
        String errorsMessages = org.apache.commons.lang.StringUtils.EMPTY;
        
        public void addErrorMessage(final String errorMessage) {
            this.errorsMessages = this.errorsMessages.concat("-").concat(errorMessage)
                    .concat(ApportBuildOutputDisplayByRow.CST_NEW_LINE);
        }
        
        public String getErrorsMessages() {
            return this.errorsMessages;
        }
        
        public boolean hasErrors() {
            return this.errorsMessages.length() > 0;
        }
    }

    /**
     *
     */
    protected static class ComparatorVariable implements Comparator<VariablesPRO> {
        
        @Override
        public int compare(final VariablesPRO arg0, final VariablesPRO arg1) {
            if (arg0.getId().equals(arg1.getId())) {
                return 0;
            }
            return arg0.getId() > arg1.getId() ? 1 : 0;
        }
    }

}
