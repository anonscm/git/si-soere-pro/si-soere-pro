/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.digester.Digester;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
/**
 *
 * @author adiankha
 */
public class DatasetDescriptorBuilderPRO {
    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger
            .getLogger(DatasetDescriptorBuilderPRO.class
                    .getName());
    
    
     public static DatasetDescriptorPRO buildDescriptorPRO(final File datasetDescriptorFile)
            throws IOException,SAXException {
        DatasetDescriptorPRO datasetDescriptorPRO;
        final Digester digester = DatasetDescriptorBuilderPRO.buildDigester();

        datasetDescriptorPRO = (DatasetDescriptorPRO) digester.parse(datasetDescriptorFile);

        return datasetDescriptorPRO;
    }
     
     
     
     public static DatasetDescriptorPRO buildDescriptorPRO(
            final InputStream datasetDescriptorStream) throws IOException,SAXException {
        DatasetDescriptorPRO datasetDescriptorPRO;
        final Digester digester = DatasetDescriptorBuilderPRO.buildDigester();

        datasetDescriptorPRO = (DatasetDescriptorPRO) digester.parse(datasetDescriptorStream);

        return datasetDescriptorPRO;
    } 

     
     
    public static Digester buildDigester() {
        
       final Digester digester = new Digester();
       digester.setNamespaceAware(true);
       digester.setUseContextClassLoader(true);
       digester.addObjectCreate("dataset-descriptor", DatasetDescriptorPRO.class);
       DatasetDescriptorBuilderPRO.setEnTete(digester);
       DatasetDescriptorBuilderPRO.setName(digester);
       DatasetDescriptorBuilderPRO.setColumns(digester);
     //  DatasetDescriptorBuilderPRO.setDispositif(digester);
       DatasetDescriptorBuilderPRO.setVariableName(digester);
       DatasetDescriptorBuilderPRO.setUndefinedColumn(digester);
       
        return digester;
    }
    public static void setColumns(Digester digester) {
     
        digester.addObjectCreate("dataset-descriptor/column", Column.class);
        digester.addSetNext("dataset-descriptor/column", "addColumn");
        digester.addCallMethod("dataset-descriptor/column/name", "setName", 0);
        digester.addCallMethod("dataset-descriptor/column/not-null", "setNullable", 0);
        digester.addCallMethod("dataset-descriptor/column/value-type", "setValueType", 0);
        digester.addCallMethod("dataset-descriptor/column/variable", "setVariable", 0);
//        digester.addCallMethod("dataset-descriptor/column/flag", "setFlag", 0);
//        digester.addCallMethod("dataset-descriptor/column/flag-type", "setFlagType", 0);
//        digester.addCallMethod("dataset-descriptor/column/ref-variable-name", "setRefVariableName", 0);
        digester.addCallMethod("dataset-descriptor/column/format-type", "setFormatType", 0);
       
    }

    public  static void setEnTete(Digester digester) {
        digester.addCallMethod("dataset-descriptor/en-tete", "setEnTete", 0);
        digester.addCallMethod("dataset-descriptor/headersLine", "setHeadersLine", 0);
//        digester.addCallMethod("dataset-descriptor/methode", "setMethode", 0);
//        digester.addCallMethod("dataset-descriptor/categorie", "setCategorie", 0);
    }

    public static void setName(Digester digester) {
       digester.addCallMethod("dataset-descriptor/name", "setName", 0);
       
   }
    public static void setDispositif(Digester digester) {
        digester.addCallMethod("dataset-descriptor/dispositif", "setDispositif", 0);
    }

    public static void setVariableName(Digester digester) {
        digester.addCallMethod("dataset-descriptor/variableName", "setVariableName", 0);
    }

    public static void setUndefinedColumn(Digester digester) {
        digester.addCallMethod("dataset-descriptor/undefined-column", "setUndefinedColumn", 0);
    }
    
     static void skipHeader(final BufferedReader bufferedReader) throws IOException {
        for (int i = 0; i < 3; i++) {
            bufferedReader.readLine();
        }

    }
     
      public static void testInputStream(final InputStream is,
            final DatasetDescriptor datasetDescriptor) {
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
        try {
            DatasetDescriptorBuilderPRO.skipHeader(bufferedReader);

            final String line = bufferedReader.readLine();
            final String[] columns = line == null ? new String[0] : line.split(";");
            for (int i = 0; i < columns.length; i++) {
                datasetDescriptor.getColumns().get(i).getName();
            }
        } catch (final IOException e) {
            LoggerFactory.getLogger(DatasetDescriptor.class.getName()).error(e.getMessage());
        }
      }
      
      public DatasetDescriptorBuilderPRO (){
    super();}
      
    
}
