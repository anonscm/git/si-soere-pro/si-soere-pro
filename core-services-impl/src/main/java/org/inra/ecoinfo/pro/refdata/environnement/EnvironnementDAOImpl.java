package org.inra.ecoinfo.pro.refdata.environnement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author ptcherniati
 */
public class EnvironnementDAOImpl extends AbstractJPADAO<Environnement> implements IEnvironnementDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Environnement> getAll() {
        return getAllBy(Environnement.class, Environnement_.activiteindustrielle);
    }

    /**
     *
     * @param axe
     * @param agglo
     * @param vent
     * @return
     */
    @Override
    public Optional<Environnement> getByNKey(double axe, double agglo, String vent) {
        CriteriaQuery<Environnement> query = builder.createQuery(Environnement.class);
        Root<Environnement> environnement = query.from(Environnement.class);
        query
                .select(environnement)
                .where(
                        builder.equal(environnement.get(Environnement_.directionventdominant), vent),
                        builder.equal(environnement.get(Environnement_.distanceagglomeration), agglo),
                        builder.equal(environnement.get(Environnement_.distanceaxeroutier), axe)
                );
        return getOptional(query);
    }
}
