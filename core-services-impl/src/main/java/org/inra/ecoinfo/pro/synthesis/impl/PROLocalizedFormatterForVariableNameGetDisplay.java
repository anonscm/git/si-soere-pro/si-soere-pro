/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.impl;

import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.synthesis.ILocalizedFormatter;

/**
 *
 * @author tcherniatinsky
 */
class PROLocalizedFormatterForVariableNameGetDisplay implements ILocalizedFormatter<DatatypeVariableUnitePRO> {

    ILocalizationManager localizationManager;

    public PROLocalizedFormatterForVariableNameGetDisplay() {
    }

    @Override
    public String format(DatatypeVariableUnitePRO nodeable, Locale locale, Object... arguments) {
        final Properties propertiesVariablesNames = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariablesPRO.class), Nodeable.ENTITE_COLUMN_NAME, locale);
        return Optional.ofNullable(nodeable).map(nodeabledvu -> nodeabledvu.getVariablespro().getName())
                .map(name -> propertiesVariablesNames.getProperty(name, name))
                .map(definition -> String.format("<i>%s</i>", definition))
                .orElse("Error while retrieving variable display)");
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

}
