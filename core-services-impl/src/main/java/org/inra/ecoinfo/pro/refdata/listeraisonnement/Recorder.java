/**
 *
 */
package org.inra.ecoinfo.pro.refdata.listeraisonnement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<ListeRaisonnement> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    IListeRaisonnementDAO listeRaisonnementDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                listeRaisonnementDAO.remove(listeRaisonnementDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't find liste raisonnement")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<ListeRaisonnement> getAllElements() throws BusinessException {
        return listeRaisonnementDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(ListeRaisonnement listeRaisonnement) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesLibelle = localizationManager.newProperties(ListeRaisonnement.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_LRAIS, Locale.ENGLISH);
        
        String localizedChampLibelle = "";
        
        if (listeRaisonnement != null) {
            localizedChampLibelle = propertiesLibelle.containsKey(listeRaisonnement.getLibelle()) ? propertiesLibelle.getProperty(listeRaisonnement.getLibelle()) : listeRaisonnement.getLibelle();
        }
        
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(listeRaisonnement == null ? Constantes.STRING_EMPTY : listeRaisonnement.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(listeRaisonnement == null ? Constantes.STRING_EMPTY : localizedChampLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, ListeRaisonnement.TABLE_NAME);
                
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                if (libelle == null || libelle.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                ListeRaisonnement listeRaisonnement = new ListeRaisonnement(libelle);
                ListeRaisonnement dbListeRaisonnement = listeRaisonnementDAO.getByNKey(libelle).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbListeRaisonnement == null) {
                        listeRaisonnementDAO.saveOrUpdate(listeRaisonnement);
                    } else {
                        dbListeRaisonnement.setLibelle(libelle);
                        listeRaisonnementDAO.saveOrUpdate(dbListeRaisonnement);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param listeRaisonnementDAO
     */
    public void setListeRaisonnementDAO(IListeRaisonnementDAO listeRaisonnementDAO) {
        this.listeRaisonnementDAO = listeRaisonnementDAO;
    }
    
}
