package org.inra.ecoinfo.pro.refdata.produitstructurerole;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.produit.IProduitDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.rolestructure.IRoleStructureDAO;
import org.inra.ecoinfo.pro.refdata.rolestructure.RoleStructure;
import org.inra.ecoinfo.pro.refdata.structure.IStructureDAO;
import org.inra.ecoinfo.pro.refdata.structure.Structure;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<StructureProduitRole> {

    private static final String PROPERTY_MSG_PROD_STRUC_BAD_ROLE_LIB = "PROPERTY_MSG_PROD_STRUC_BAD_ROLE_LIB";

    private static final String PROPERTY_MSG_PROD_STRUC_BAD_PROCUITCODE = "PROPERTY_MSG_PROD_STRUC_BAD_PROCUITCODE";

    private static final String PROPERTY_MSG_PROD_STRUC_BAD_STRUC_NOM = "PROPERTY_MSG_PROD_STRUC_BAD_STRUC_NOM";

    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    private String[] mListeProduitsPossibles;
    private String[] mListeStructurePossibles;
    private String[] mListeRolePossibles;
    private Map<String, String[]> listePrécisionPossibles;

    /**
     *
     */
    protected IProduitStructureRoleDAO produitstructureroleDAO;

    /**
     *
     */
    protected IProduitDAO produitDAO;

    /**
     *
     */
    protected IStructureDAO structureDAO;

    /**
     *
     */
    protected IRoleStructureDAO roleStructureDAO;
    Properties commentaireEn;

    private void createOrupdateProduitStructureRole(Produits prod, Structure structure, RoleStructure role, String commentaire, final StructureProduitRole dbstructureproduitrole) throws BusinessException {
        if (dbstructureproduitrole == null) {
            StructureProduitRole stprorole = new StructureProduitRole(prod, structure, role, commentaire);
            stprorole.setProduits(prod);
            stprorole.setStructure(structure);
            stprorole.setRole(role);
            stprorole.setCommentaire(commentaire);
            createProduitStructureRole(stprorole);
        } else {
            updateBDProduitStructureRole(prod, structure, role, commentaire, dbstructureproduitrole);
        }
    }

    private void createProduitStructureRole(final StructureProduitRole structureproduitrole) throws BusinessException {
        try {
            produitstructureroleDAO.saveOrUpdate(structureproduitrole);
            produitstructureroleDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create structureproduitrole");
        }
    }

    private void updateBDProduitStructureRole(Produits prod, Structure structure, RoleStructure role, String commentaire, final StructureProduitRole dbstructureproduitrole) throws BusinessException {
        try {
            dbstructureproduitrole.setProduits(prod);
            dbstructureproduitrole.setStructure(structure);
            dbstructureproduitrole.setRole(role);
            dbstructureproduitrole.setCommentaire(commentaire);
            produitstructureroleDAO.saveOrUpdate(dbstructureproduitrole);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update structureproduitrole");
        }
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, StructureProduitRole.NAME_ENTITY_JPA);
                final String prod_code = tokenizerValues.nextToken();
                final String strNomStrucure = tokenizerValues.nextToken();
                final String str_precision = tokenizerValues.nextToken();
                final String role_libelle = tokenizerValues.nextToken();

                Produits produits = produitDAO.getByNKey(prod_code).orElse(null);
                if (produits == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_STRUC_BAD_PROCUITCODE), prod_code));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                Structure structure = structureDAO.getByNKey(strNomStrucure, str_precision).orElse(null);
                if (structure == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_STRUC_BAD_STRUC_NOM), strNomStrucure, str_precision));
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
                RoleStructure role = roleStructureDAO.getByNKey(role_libelle).orElse(null);

                final StructureProduitRole dbspr = produitstructureroleDAO.getByNKey(produits, structure, role)
                        .orElseThrow(() -> new BusinessException("can't get StructureProduitRole"));
                if (dbspr != null) {
                    produitstructureroleDAO.remove(dbspr);
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<StructureProduitRole> getAllElements() throws BusinessException {
        return produitstructureroleDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(StructureProduitRole structureproduitrole) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(structureproduitrole == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : structureproduitrole.getProduits() != null ? structureproduitrole.getProduits().getProd_key()
                        : "", mListeProduitsPossibles, null,
                        true, false, true));
        String valeurNom = structureproduitrole == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : structureproduitrole.getStructure() != null ? structureproduitrole.getStructure().getNom() : "";
        ColumnModelGridMetadata columnNom = new ColumnModelGridMetadata(valeurNom, mListeStructurePossibles, null, true, false, true);
        String valeurPrecision = structureproduitrole == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : structureproduitrole.getStructure() != null ? structureproduitrole.getStructure().getPrecision() : "";
        ColumnModelGridMetadata columnPrecision = new ColumnModelGridMetadata(valeurPrecision, listePrécisionPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnPrecision);
        columnPrecision.setValue(valeurPrecision);
        columnNom.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnNom);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnPrecision);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(structureproduitrole == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : structureproduitrole.getRole() != null ? structureproduitrole.getRole().getLibelle() : "", mListeRolePossibles, null, true, false,
                        true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(structureproduitrole == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : structureproduitrole.getCommentaire(),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(structureproduitrole == null || structureproduitrole.getCommentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : commentaireEn.getProperty(structureproduitrole.getCommentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    public IProduitDAO getProduitDAO() {
        return produitDAO;
    }

    /**
     *
     * @return
     */
    public IProduitStructureRoleDAO getProduitstructureroleDAO() {
        return produitstructureroleDAO;
    }

    /**
     *
     * @return
     */
    public IRoleStructureDAO getRoleStructureDAO() {
        return roleStructureDAO;
    }

    /**
     *
     * @return
     */
    public IStructureDAO getStructureDAO() {
        return structureDAO;
    }

    @Override
    protected ModelGridMetadata<StructureProduitRole> initModelGridMetadata() {
        initDispositifPossibles();
        listeProduitsPossibles();
        listeStructuresPossibles();
        listeRolePossibles();
        commentaireEn = localizationManager.newProperties(StructureProduitRole.NAME_ENTITY_JPA, StructureProduitRole.JPA_COLUMN_COMENT, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void listeProduitsPossibles() {
        List<Produits> groupeproduits = produitDAO.getAll();
        String[] ListeProduitsPossibles = new String[groupeproduits.size() + 1];
        ListeProduitsPossibles[0] = "";
        int index = 1;
        for (Produits produits : groupeproduits) {
            ListeProduitsPossibles[index++] = produits.getProd_key();
        }
        this.mListeProduitsPossibles = ListeProduitsPossibles;
    }

    private void listeRolePossibles() {
        List<RoleStructure> grouperole = roleStructureDAO.getAll(RoleStructure.class);
        String[] ListeRolePossibles = new String[grouperole.size() + 1];
        ListeRolePossibles[0] = "";
        int index = 1;
        for (RoleStructure role : grouperole) {
            ListeRolePossibles[index++] = role.getLibelle();
        }
        this.mListeRolePossibles = ListeRolePossibles;
    }

    private void listeStructuresPossibles() {
        List<Structure> groupestructure = structureDAO.getAll();
        String[] ListeStructurePossibles = new String[groupestructure.size() + 1];
        ListeStructurePossibles[0] = "";
        int index = 1;
        for (Structure str : groupestructure) {
            ListeStructurePossibles[index++] = str.getNom();
        }
        this.mListeStructurePossibles = ListeStructurePossibles;
    }

    private void initDispositifPossibles() {
        Map<String, String[]> structurePrecision = new HashMap<>();
        Map<String, Set<String>> structurePrecisionList = new HashMap<>();
        List<Structure> groupestructure = structureDAO.getAll();
        groupestructure.forEach((structure) -> {
            String nomstruc = structure.getNom();
            String precision = structure.getPrecision();
            if (!structurePrecisionList.containsKey(nomstruc)) {
                structurePrecisionList.put(nomstruc, new TreeSet());
            }
            structurePrecisionList.get(nomstruc).add(precision);
        });
        structurePrecisionList.entrySet().forEach((entryProduit) -> {
            structurePrecision.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listePrécisionPossibles = structurePrecision;
        mListeStructurePossibles = structurePrecision.keySet().toArray(new String[]{});
    }

    private void persistProduitStructureRole(final Produits produits, Structure structure, RoleStructure role, String commentaire) throws BusinessException, BusinessException {
        final StructureProduitRole dbstructureproduitrole = produitstructureroleDAO.getByNKey(produits, structure, role).orElse(null);
        createOrupdateProduitStructureRole(produits, structure, role, commentaire, dbstructureproduitrole);
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, StructureProduitRole.NAME_ENTITY_JPA);
                int indexprod = tokenizerValues.currentTokenIndex();
                final String prod_code = tokenizerValues.nextToken();
                int indexnom = tokenizerValues.currentTokenIndex();
                final String str_nom = tokenizerValues.nextToken();
                int indexp = tokenizerValues.currentTokenIndex();
                final String str_precision = tokenizerValues.nextToken();
                int indexrole = tokenizerValues.currentTokenIndex();
                final String role_libelle = tokenizerValues.nextToken();
                Produits produits = produitDAO.getByNKey(prod_code).orElse(null);
                String commentaire = tokenizerValues.nextToken();
                if (produits == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_STRUC_BAD_PROCUITCODE), line, indexprod, prod_code));
                }
                Structure structure = structureDAO.getByNKey(str_nom, str_precision).orElse(null);
                if (structure == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_STRUC_BAD_STRUC_NOM), line, indexnom, str_nom, indexp, str_precision));
                }
                RoleStructure role = roleStructureDAO.getByNKey(role_libelle).orElse(null);
                if (role == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_PROD_STRUC_BAD_ROLE_LIB), line, indexrole, role_libelle));
                }

                if (!errorsReport.hasErrors()) {
                    persistProduitStructureRole(produits, structure, role, commentaire);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    /**
     *
     * @param produitDAO
     */
    public void setProduitDAO(IProduitDAO produitDAO) {
        this.produitDAO = produitDAO;
    }

    /**
     *
     * @param produitstructureroleDAO
     */
    public void setProduitstructureroleDAO(IProduitStructureRoleDAO produitstructureroleDAO) {
        this.produitstructureroleDAO = produitstructureroleDAO;
    }

    /**
     *
     * @param roleStructureDAO
     */
    public void setRoleStructureDAO(IRoleStructureDAO roleStructureDAO) {
        this.roleStructureDAO = roleStructureDAO;
    }

    /**
     *
     * @param structureDAO
     */
    public void setStructureDAO(IStructureDAO structureDAO) {
        this.structureDAO = structureDAO;
    }

}
