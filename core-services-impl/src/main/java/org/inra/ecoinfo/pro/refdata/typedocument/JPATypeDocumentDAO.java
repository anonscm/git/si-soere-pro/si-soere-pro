package org.inra.ecoinfo.pro.refdata.typedocument;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPATypeDocumentDAO extends AbstractJPADAO<TypeDocument> implements ITypeDocumentDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.typedocument.ITypeDocumentDAO#getByLibelle (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    @SuppressWarnings("unchecked")
    public Optional<TypeDocument> getByNKey(String libelle) {
        CriteriaQuery<TypeDocument> query = builder.createQuery(TypeDocument.class);
        Root<TypeDocument> typeDocument = query.from(TypeDocument.class);
        query
                .select(typeDocument)
                .where(
                        builder.equal(typeDocument.get(TypeDocument_.libelle), libelle)
                );
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public List<TypeDocument> gettAll() {
        return getAll(TypeDocument.class);
    }

}
