/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolmoyennees.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSolMoy_;
import org.inra.ecoinfo.pro.extraction.incubation.IIncubationDatatypeManager;
import org.inra.ecoinfo.pro.extraction.incubation.jpa.AbstractJPAIncubationDAO;
import org.inra.ecoinfo.pro.synthesis.incubationsolmoy.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author vjkoyao
 */
public class JPAIncubationSolMoyDAO extends AbstractJPAIncubationDAO<MesureIncubationSolMoy, ValeurIncubationSolMoy>{

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurIncubationSolMoy> getValeurIncubationClass() {
        return ValeurIncubationSolMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureIncubationSolMoy> getMesureIncubationClass() {
        return MesureIncubationSolMoy.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurIncubationSolMoy, MesureIncubationSolMoy> getMesureAttribute() {
        return ValeurIncubationSolMoy_.mesureIncubationSolMoy;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IIncubationDatatypeManager.CODE_DATATYPE_INCUBATION_SOL_MOY;
    }
}
