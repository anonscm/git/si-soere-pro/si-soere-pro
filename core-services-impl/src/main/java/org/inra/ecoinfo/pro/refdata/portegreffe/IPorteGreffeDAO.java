/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.portegreffe;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IPorteGreffeDAO extends IDAO<PorteGreffe> {

    /**
     *
     * @param libelle
     * @return
     */
    Optional<PorteGreffe> getByNKey(String libelle);

    /**
     *
     * @return
     */
    List<PorteGreffe> getAll();

}
