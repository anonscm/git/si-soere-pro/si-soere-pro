package org.inra.ecoinfo.pro.refdata.departement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.region.Region;
import org.inra.ecoinfo.utils.Utils;

/**
 * @author sophie
 *
 */
public class JPADepartementDAO extends AbstractJPADAO<Departement> implements IDepartementDAO {

    /* (non-Javadoc)
		 * @see org.inra.ecoinfo.pro.refdata.departement.IDepartementDAO#getAll()
     */
    @Override
    public List<Departement> getAll() {
        CriteriaQuery<Departement> query = builder.createQuery(Departement.class);
        Root<Departement> departement = query.from(Departement.class);
        query
                .select(departement)
                .orderBy(builder.asc(departement.get(Departement_.CODE)));
        return getResultList(query);
    }

    /*
	     * (non-Javadoc)
	     * 
	     * @see org.inra.ecoinfo.pro.refdata.pays.IDepartementDAO#getByNom(java.lang. String)
     */
    @Override
    public Optional<Departement> getByNom(String nomDepartement) {
        return getByNKey(Utils.createCodeFromString(nomDepartement));
    }

    /* (non-Javadoc)
	     * @see org.inra.ecoinfo.pro.refdata.departement.IDepartementDAO#getByNKey(java.lang.String)
     */
    @Override
    public Optional<Departement> getByNKey(String code) {
        CriteriaQuery<Departement> query = builder.createQuery(Departement.class);
        Root<Departement> departement = query.from(Departement.class);
        query
                .select(departement)
                .where(
                        builder.equal(departement.get(Departement_.code), code)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
	     * @see org.inra.ecoinfo.pro.refdata.departement.IDepartementDAO#getAllNoDepartement()
     */
    @Override
    public List<String> getAllNoDepartement() {
        CriteriaQuery<String> query = builder.createQuery(String.class);
        Root<Departement> departement = query.from(Departement.class);
        final Path<String> noDepartement = departement.get(Departement_.noDepartement);
        query
                .select(noDepartement)
                .orderBy(builder.asc(noDepartement));
        return getResultList(query);
    }

    /*
	     * (non-Javadoc)
	     * 
	     * @see org.inra.ecoinfo.pro.refdata.pays.IDepartementDAO#getByRegion(org.inra .ecoinfo.pro.refdata.pays.Region)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Departement> getByRegion(Region region) {
        CriteriaQuery<Departement> query = builder.createQuery(Departement.class);
        Root<Departement> departement = query.from(Departement.class);
        Join<Departement, Region> reg = departement.join(Departement_.region);
        query
                .select(departement)
                .where(
                        builder.equal(reg, region)
                );
        return getResultList(query);
    }
}
