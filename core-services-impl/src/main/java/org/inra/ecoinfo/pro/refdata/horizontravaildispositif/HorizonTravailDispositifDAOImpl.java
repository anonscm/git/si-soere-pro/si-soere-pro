/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.horizontravaildispositif;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.horizontravailledispositif.HorizonTravailDispositif;
import org.inra.ecoinfo.pro.refdata.horizontravailledispositif.HorizonTravailDispositif_;
import org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial.Teneurcalcaireinitial;

/**
 *
 * @author adiankha
 */
public class HorizonTravailDispositifDAOImpl extends AbstractJPADAO<HorizonTravailDispositif> implements IHorizonTravailDispositifDAO {

    /**
     *
     * @return
     */
    @Override
    public List<HorizonTravailDispositif> getAll() {
        return getAll(HorizonTravailDispositif.class);
    }

    /**
     *
     * @param dispositif
     * @param teneurcalcaireinitial
     * @param teneurcarbone
     * @return
     */
    @Override
    public Optional<HorizonTravailDispositif> getByNKey(Dispositif dispositif, Teneurcalcaireinitial teneurcalcaireinitial, double teneurcarbone) {
        CriteriaQuery<HorizonTravailDispositif> query = builder.createQuery(HorizonTravailDispositif.class);
        Root<HorizonTravailDispositif> horizonTravailDispositif = query.from(HorizonTravailDispositif.class);
        Join<HorizonTravailDispositif, Dispositif> disp = horizonTravailDispositif.join(HorizonTravailDispositif_.dispositif);
        Join<HorizonTravailDispositif, Teneurcalcaireinitial> tci = horizonTravailDispositif.join(HorizonTravailDispositif_.teneur);
        query
                .select(horizonTravailDispositif)
                .where(
                        builder.equal(horizonTravailDispositif.get(HorizonTravailDispositif_.teneurcarbone), teneurcarbone),
                        builder.equal(disp, dispositif),
                        builder.equal(tci, teneurcalcaireinitial)
                );
        return getOptional(query);
    }
}
