/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class PhysicoChimieOutputDisplayByRow  extends PhysicoChimieOutputBuilderResolver{
    protected static final String MAP_INDEX_0 = "0";
    
    IOutputBuilder  produitBrutBuildOutputByRow;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IVariablesPRODAO variPRODAO;
    
   

    public PhysicoChimieOutputDisplayByRow() {
        super();
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
       try {
             super.setProduitBrutOutputBuilder(this.produitBrutOutputBuilder);
             
             return super.buildOutput(parameters);
         } catch (BusinessException e) {
             LoggerFactory.getLogger(this.getClass()).error("error while building output", e);
             return null;
         }
    }
    protected Set<String> buildListOfDispositifsForDatatype(List<Dispositif> selectedDispositifs, String datatype){
        final Set<String> dispositifsNames = new HashSet();
        selectedDispositifs.forEach((dispositif) -> {
            dispositifsNames.add(getDispositifDatatype(dispositif, datatype));
        });
        return dispositifsNames;
    }

    protected static String getDispositifDatatype(Dispositif dispositif, String datatype) {
        return String.format("%s_%s", dispositif.getCode(), datatype);
    }

    public void setProduitBrutBuildOutputByRow(IOutputBuilder produitBrutBuildOutputByRow) {
        this.produitBrutBuildOutputByRow = produitBrutBuildOutputByRow;
    }

    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    public void setVariPRODAO(IVariablesPRODAO variPRODAO) {
        this.variPRODAO = variPRODAO;
    }

    protected List getMesures(Map<String, List> resultsDatasMap, String codeMap) {
        return resultsDatasMap.getOrDefault(codeMap, resultsDatasMap.get(MAP_INDEX_0));
    }

    protected List<VariablesPRO> getVariablesSelected(Map<String, Object> requestMetadatasMap, String codeVariable) {
        return (List<VariablesPRO>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(codeVariable));
    }
    
}
