package org.inra.ecoinfo.pro.refdata.rolepersonneressource;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPARolePersonneRessourceDAO extends AbstractJPADAO<RolePersonneRessource> implements IRolePersonneRessourceDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.rolepersonneressource.IRolePersonneRessourceDAO #getByLibelle(java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<RolePersonneRessource> getByNKey(String libelle) {
        CriteriaQuery<RolePersonneRessource> query = builder.createQuery(RolePersonneRessource.class);
        Root<RolePersonneRessource> rolePersonneRessource = query.from(RolePersonneRessource.class);
        query
                .select(rolePersonneRessource)
                .where(
                        builder.equal(rolePersonneRessource.get(RolePersonneRessource_.libelle), libelle)
                );
        return getOptional(query);
    }

}
