/**
 *
 */
package org.inra.ecoinfo.pro.refdata.echelleprelevement;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<EchellePrelevement> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";

    IEchellePrelevementDAO echellePrelevementDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com .Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String nom = tokenizerValues.nextToken();

                echellePrelevementDAO.remove(echellePrelevementDAO.getByNKey(nom)
                        .orElseThrow(() -> new BusinessException("Can't find echellePrelevement")));

                values = parser.getLine();
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#getAllElements()
     */
    @Override
    protected List<EchellePrelevement> getAllElements() throws BusinessException {
        return echellePrelevementDAO.getAll();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata (java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(EchellePrelevement echellePrelevement) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        Properties propertiesNom = localizationManager.newProperties(EchellePrelevement.TABLE_NAME, RefDataConstantes.COLUMN_NOM_ECH, Locale.ENGLISH);

        String localizedChampNom = "";

        if (echellePrelevement != null) {
            localizedChampNom = propertiesNom.containsKey(echellePrelevement.getNom()) ? propertiesNom.getProperty(echellePrelevement.getNom()) : echellePrelevement.getNom();
        }

        // Nom
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(echellePrelevement == null ? Constantes.STRING_EMPTY : echellePrelevement.getNom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(echellePrelevement == null ? Constantes.STRING_EMPTY : localizedChampNom, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com .Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;

        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, EchellePrelevement.TABLE_NAME);
                String nom = tokenizerValues.nextToken();
                int indexNom = tokenizerValues.currentTokenIndex();

                String code = Utils.createCodeFromString(nom);

                if (nom == null || nom.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexNom - 1, datasetDescriptor.getColumns().get(0).getName()));
                }

                EchellePrelevement echellePrelevement = new EchellePrelevement(nom, code);
                EchellePrelevement dbEchellePrelevement = echellePrelevementDAO.getByNKey(nom).orElse(null);

                if (!errorsReport.hasErrors()) {
                    if (dbEchellePrelevement == null) {
                        echellePrelevementDAO.saveOrUpdate(echellePrelevement);
                    } else {
                        dbEchellePrelevement.setNom(nom);
                        dbEchellePrelevement.setCode(code);
                        echellePrelevementDAO.saveOrUpdate(dbEchellePrelevement);
                    }
                }

                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param echellePrelevementDAO the echellePrelevementDAO to set
     */
    public void setEchellePrelevementDAO(IEchellePrelevementDAO echellePrelevementDAO) {
        this.echellePrelevementDAO = echellePrelevementDAO;
    }

}
