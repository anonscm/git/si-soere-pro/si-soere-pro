/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.intervention;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.typeintervention.ITypeInterventionDAO;
import org.inra.ecoinfo.pro.refdata.typeintervention.TypeIntervention;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author vjkoyao
 */
public class Recorder extends AbstractCSVMetadataRecorder<Intervention> {

    private static final String PRO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";
    private static final String PROPERTY_MSG_BAD_TYPE_INTERVENTION_ITK = "PROPERTY_MSG_BAD_TYPE_INTERVENTION_ITK";

    /**
     *
     */
    public IInterventionDAO interventionDAO;
    ITypeInterventionDAO typeInterventionDAO;
    Properties comentEn;
    Properties libelleEn;
    private String[] ListeTypePossibles;

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                Intervention Int = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values);

                String intervention_nom = tokenizerValues.nextToken();
                //String type_intervention_nom = tokenizerValues.nextToken();

                interventionDAO.remove(interventionDAO.getByNKey(intervention_nom)
                        .orElseThrow(() -> new BusinessException("can't get  intervention")));
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);

            String[] values = parser.getLine();
            long line = 0;
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Intervention.NAME_ENTITY_JPA);
                final String intervention_nom = tokenizerValues.nextToken();
                final String type_intervention_nom = tokenizerValues.nextToken();
                int index = tokenizerValues.currentTokenIndex();
                final String intervention_description = tokenizerValues.nextToken();
                final String intervention_commentaire = tokenizerValues.nextToken();

                TypeIntervention type_intervention = typeInterventionDAO.getByNKey(type_intervention_nom).orElse(null);
                if (type_intervention == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_SOURCE_PATH, Recorder.PROPERTY_MSG_BAD_TYPE_INTERVENTION_ITK), line, type_intervention_nom));
                }

                if (!errorsReport.hasErrors()) {

                    persistIntervention(intervention_nom, intervention_description, intervention_commentaire, type_intervention);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<Intervention> getAllElements() throws BusinessException {
        return interventionDAO.getAll();
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Intervention intervention) throws BusinessException {

        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(intervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : intervention.getIntervention_nom(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(intervention == null || intervention.getIntervention_nom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : libelleEn.getProperty(intervention.getIntervention_nom()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(intervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : (intervention.getTypeIntervention() != null ? intervention.getTypeIntervention().getType_intervention_nom() : ""), ListeTypePossibles,
                        null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(intervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : intervention.getIntervention_description(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(intervention == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : intervention.getIntervention_commentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(intervention == null || intervention.getIntervention_commentaire() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : comentEn.getProperty(intervention.getIntervention_commentaire()),
                        ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null,
                        false, true, false));

        return lineModelGridMetadata;
    }

    private void persistIntervention(String intervention_nom, String intervention_description, String intervention_commentaire, TypeIntervention type_intervention_nom) throws PersistenceException {
        final Intervention dbInt = interventionDAO.getByNKey(intervention_nom).orElse(null);
        createOrUpdateIntervention(intervention_nom, intervention_description, intervention_commentaire, type_intervention_nom, dbInt);
    }

    private void createOrUpdateIntervention(String intervention_nom, String intervention_description, String intervention_commentaire, TypeIntervention type_intervention_nom, Intervention dbInt) throws PersistenceException {
        if (dbInt == null) {
            Intervention intervention = new Intervention(intervention_nom, intervention_description, intervention_commentaire, type_intervention_nom);

            intervention.setIntervention_nom(intervention_nom);
            intervention.setIntervention_description(intervention_description);
            intervention.setIntervention_commentaire(intervention_commentaire);

            createIntervention(intervention);

        } else {
            updateIntervention(intervention_nom, intervention_description, intervention_commentaire, dbInt);
        }
    }

    private void createIntervention(Intervention intervention) throws PersistenceException {
        interventionDAO.saveOrUpdate(intervention);
        interventionDAO.flush();
    }

    private void updateIntervention(String intervention_nom, String intervention_description, String intervention_commentaire, Intervention dbint) throws PersistenceException {
        dbint.setIntervention_nom(intervention_nom);
        dbint.setIntervention_description(intervention_description);
        dbint.setIntervention_commentaire(intervention_commentaire);
        interventionDAO.saveOrUpdate(dbint);
    }

    /**
     *
     * @return
     */
    public IInterventionDAO getInterventionDAO() {
        return interventionDAO;
    }

    /**
     *
     * @param interventionDAO
     */
    public void setInterventionDAO(IInterventionDAO interventionDAO) {
        this.interventionDAO = interventionDAO;
    }

    @Override
    protected ModelGridMetadata<Intervention> initModelGridMetadata() {
        TypeInterventionPossibles();
        comentEn = localizationManager.newProperties(Intervention.NAME_ENTITY_JPA, Intervention.JPA_COLUMN_COMMENTAIRE, Locale.ENGLISH);
        libelleEn = localizationManager.newProperties(Intervention.NAME_ENTITY_JPA, Intervention.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    private void TypeInterventionPossibles() {

        List<TypeIntervention> groupetype = typeInterventionDAO.getAll(TypeIntervention.class);
        String[] listeTypePossibles = new String[groupetype.size() + 1];
        listeTypePossibles[0] = "";
        int index = 1;
        for (TypeIntervention typeIntervention : groupetype) {
            listeTypePossibles[index++] = typeIntervention.getType_intervention_nom();
        }
        this.ListeTypePossibles = listeTypePossibles;
    }

    /**
     *
     * @return
     */
    public ITypeInterventionDAO getTypeInterventionDAO() {
        return typeInterventionDAO;
    }

    /**
     *
     * @param typeInterventionDAO
     */
    public void setTypeInterventionDAO(ITypeInterventionDAO typeInterventionDAO) {
        this.typeInterventionDAO = typeInterventionDAO;
    }

    /**
     *
     * @return
     */
    public String[] getListeTypePossibles() {
        return ListeTypePossibles;
    }

    /**
     *
     * @param ListeTypePossibles
     */
    public void setListeTypePossibles(String[] ListeTypePossibles) {
        this.ListeTypePossibles = ListeTypePossibles;
    }

}
