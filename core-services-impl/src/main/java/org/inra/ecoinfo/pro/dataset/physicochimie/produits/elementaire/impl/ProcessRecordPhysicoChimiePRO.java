package org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.exception.InsertionDatabaseException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.pro.dataset.impl.AbstractProcessRecord;
import org.inra.ecoinfo.pro.dataset.impl.CleanerValues;
import org.inra.ecoinfo.pro.dataset.impl.DatasetDescriptorPRO;
import org.inra.ecoinfo.pro.dataset.impl.ISessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.impl.VariableValue;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.IMesurePhysicoChimiePRODAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.IValeurProduitBrutDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.MesurePhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.entity.ValeurPhysicoChimiePRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.elementaire.IMesurePhysicoChimieDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.IDataTypeVariableQualifiantDAO;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.IEchantillonsProduitDAO;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.HumiditeExpression;
import org.inra.ecoinfo.pro.refdata.humiditeexpression.IHumiditeExpressionDAO;
import org.inra.ecoinfo.pro.refdata.methode.IMethodeDAO;
import org.inra.ecoinfo.pro.refdata.methode.Methode;
import org.inra.ecoinfo.pro.refdata.unitepro.IUniteproDAO;
import org.inra.ecoinfo.pro.refdata.unitepro.Unitepro;
import org.inra.ecoinfo.pro.refdata.variable.IVariablesPRODAO;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.VariableStatutValeur;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DatasetDescriptorBuilder;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author vjkoyao
 */
public class ProcessRecordPhysicoChimiePRO extends AbstractProcessRecord {

    static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecordPhysicoChimiePRO.class);
    protected static final String BUNDLE_PATH_PRO_BRUT = "org.inra.ecoinfo.pro.dataset.physicochimie.produit.elementaire.messages";
    private static final String MSG_ERROR_PRO_ECHAN_NOT_FOUND_IN_DB = "MSG_ERROR_PRO_ECHAN_NOT_FOUND_IN_DB";
    private static final String MSG_ERROR_PRO_NOT_VARIABLEPRO_DB = "MSG_ERROR_PRO_NOT_VARIABLEPRO_DB";
    private static final String MSG_ERROR_PRO_NOT_FOUND_PRO_DVU_DB = "MSG_ERROR_PRO_NOT_FOUND_PRO_DVU_DB";
    private static final String MSG_ERROR_PRO_NOT_FOUND_METHODE_DB = "MSG_ERROR_PRO_NOT_FOUND_METHODE_DB";
    private static final String MSG_ERROR_PRO_NOT_UNITEPRO_DB = "MSG_ERROR_PRO_NOT_UNITEPRO_DB";
    private static final String MSG_ERROR_PRO_NOT_FOUND_HUMIDITE_DB = "MSG_ERROR_PRO_NOT_FOUND_HUMIDITE_DB";
    protected IMesurePhysicoChimiePRODAO<MesurePhysicoChimiePRO> mesurePhysicoChimiePRODAO;

    protected IEchantillonsProduitDAO echantillonsproduitDAO;
    protected IMethodeDAO methodeDAO;
    IUniteproDAO uniteproDAO;
    IHumiditeExpressionDAO humiditeDAO;
    IVariablesPRODAO variPRODAO;
    IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO;
    IValeurProduitBrutDAO valeurProduitBrutDAO;

    public ProcessRecordPhysicoChimiePRO() {
        super();
    }

    void buildMesure(final ProLineRecord mesureLines, final VersionFile versionFile,
            final SortedSet<ProLineRecord> ligneEnErreur, final ErrorsReport errorsReport,
            final ISessionPropertiesPRO sessionPropertiesPRO) throws PersistenceException,
            InsertionDatabaseException {

        // je parcours le fichier et je recupere les differentes valeurs de chaque parametre
        LocalDate date = mesureLines.getDateprelevement();
        String codeech = mesureLines.getCodeechantillon();

        final EchantillonsProduit echantillonsPro = echantillonsproduitDAO.getByECH(Utils.createCodeFromString(codeech)).orElse(null);
        if (echantillonsPro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePRO.BUNDLE_PATH_PRO_BRUT,
                    ProcessRecordPhysicoChimiePRO.MSG_ERROR_PRO_ECHAN_NOT_FOUND_IN_DB), codeech));
        }
        String nomlabo = mesureLines.getNomlabo();
        int numerolabo = mesureLines.getNumerolabo();
        int numerorepet = mesureLines.getNumerorepet();
        String variable = mesureLines.getCodevariable();
        float valeur = mesureLines.getValeurvariable();
        String statut = mesureLines.getStatutvaleur();
        String unite = mesureLines.getCodeunite();
        String methode = mesureLines.getCodemethode();

        String humidite = mesureLines.getCodehumidite();

        DataType datatype = RecorderPRO.getDatatypeFromVersion(versionFile);
        String cdatatype = Utils.createCodeFromString(datatype.getCode());
        String cunite = Utils.createCodeFromString(unite);
        String cmethode = Utils.createCodeFromString(methode);
        String chumitite = Utils.createCodeFromString(humidite);
        String cvariable = Utils.createCodeFromString(variable);

        VariablesPRO dbvariable = variPRODAO.getByCodeUser(cvariable).orElse(null);
        if (dbvariable == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePRO.BUNDLE_PATH_PRO_BRUT,
                    ProcessRecordPhysicoChimiePRO.MSG_ERROR_PRO_NOT_VARIABLEPRO_DB), cvariable));
        }

        Unitepro dbunitepro = uniteproDAO.getByMyKey(cunite).orElse(null);
        if (dbunitepro == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePRO.BUNDLE_PATH_PRO_BRUT,
                    ProcessRecordPhysicoChimiePRO.MSG_ERROR_PRO_NOT_UNITEPRO_DB), cunite));
        }

        Methode dbmethode = methodeDAO.getByNKey(cmethode).orElse(null);
        if (dbmethode == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePRO.BUNDLE_PATH_PRO_BRUT,
                    ProcessRecordPhysicoChimiePRO.MSG_ERROR_PRO_NOT_FOUND_METHODE_DB), cmethode));
        }

        HumiditeExpression dbhumidite = humiditeDAO.getByNKey(chumitite).orElse(null);
        if (dbhumidite == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePRO.BUNDLE_PATH_PRO_BRUT,
                    ProcessRecordPhysicoChimiePRO.MSG_ERROR_PRO_NOT_FOUND_HUMIDITE_DB), chumitite));
        }

        DatatypeVariableUnitePRO dbdvum = dataTypeVariableUnitePRODAO.getByNKey(RecorderPRO.getDatatypeFromVersion(versionFile), dbvariable, dbunitepro, dbmethode, dbhumidite).orElse(null);
        if (dbdvum == null) {
            errorsReport.addErrorMessage(String.format(localizationManager.getMessage(ProcessRecordPhysicoChimiePRO.BUNDLE_PATH_PRO_BRUT,
                    ProcessRecordPhysicoChimiePRO.MSG_ERROR_PRO_NOT_FOUND_PRO_DVU_DB), cdatatype, cvariable, cunite, cmethode, chumitite));

        }
        RealNode dbdvumRealNode = mgaRecorder.getRealNodeByNKey(String.format("%s%s%s", versionFile.getDataset().getRealNode().getPath(), PatternConfigurator.PATH_SEPARATOR, dbdvum.getCode())).orElse(null);

        Long fichier = mesureLines.getOriginalLineNumber();
        final VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId()).orElse(null);

        if (!errorsReport.hasErrors()) {
            MesurePhysicoChimiePRO dbmesureProbrut = getOrCreate(date, numerorepet, codeech, nomlabo, versionFile, echantillonsPro, numerolabo, fichier, versionFileDB);
            persitProduit(dbdvumRealNode, dbmesureProbrut, valeur, statut);
        }

    }

    private MesurePhysicoChimiePRO getOrCreate(LocalDate date, int numerorepet, String codeech, String nomlabo, final VersionFile versionFile, final EchantillonsProduit echantillonsPro, int numerolabo, Long fichier, final VersionFile versionFileDB) throws PersistenceException {
        String dateString = dateString = DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY);
        String intString1 = null;
        intString1 = Integer.toString(numerorepet);
        String key = dateString + "_" + codeech + "_" + nomlabo + "_" + intString1;
        MesurePhysicoChimiePRO mesurePhysicoChimiePRO = mesurePhysicoChimiePRODAO.getByKeys(key).orElse(null);
        if (mesurePhysicoChimiePRO == null) {
            mesurePhysicoChimiePRO = new MesurePhysicoChimiePRO(date, versionFile, echantillonsPro, numerorepet, numerolabo, nomlabo, fichier);
            mesurePhysicoChimiePRO.setDatePrelevement(date);
            mesurePhysicoChimiePRO.setEchantillonpro(echantillonsPro);
            mesurePhysicoChimiePRO.setLigneFichierEchange(fichier);
            mesurePhysicoChimiePRO.setNom_laboratoire(nomlabo);
            mesurePhysicoChimiePRO.setNumero_laboratoire(numerolabo);
            mesurePhysicoChimiePRO.setNumero_repetition(numerorepet);
            mesurePhysicoChimiePRO.setKeymesure(key);
            mesurePhysicoChimiePRO.setVersionfile(versionFileDB);
            mesurePhysicoChimiePRODAO.saveOrUpdate(mesurePhysicoChimiePRO);
        }
        return mesurePhysicoChimiePRO;
    }

    void createProduitBrut(ValeurPhysicoChimiePRO mpe) throws PersistenceException {
        valeurProduitBrutDAO.saveOrUpdate(mpe);
    }

    void createOrUpdate(RealNode dvurealNode, MesurePhysicoChimiePRO mpm, float valeur, String statut, ValeurPhysicoChimiePRO vpb) throws PersistenceException {
        if (vpb == null) {
            ValeurPhysicoChimiePRO valeurPhysicoChimiePRO = new ValeurPhysicoChimiePRO(valeur, statut, mpm, dvurealNode);
            valeurPhysicoChimiePRO.setRealNode(dvurealNode);
            valeurPhysicoChimiePRO.setMesurePhysicoChimiePRO(mpm);
            mpm.getValeurPhysicoChimiePRO().add(valeurPhysicoChimiePRO);
            valeurPhysicoChimiePRO.setValeur(valeur);
            valeurPhysicoChimiePRO.setStatutvaleur(statut);
            createProduitBrut(valeurPhysicoChimiePRO);
        }
    }

    void persitProduit(RealNode dvu, MesurePhysicoChimiePRO mpm, float valeur, String statut) throws PersistenceException {
        ValeurPhysicoChimiePRO valeurPhysicoChimiePRO = valeurProduitBrutDAO.getByNKeys(dvu, mpm, statut).orElse(null);
        createOrUpdate(dvu, mpm, valeur, statut, valeurPhysicoChimiePRO);
    }

    private void buildLines(final VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, final ErrorsReport errorsReport,
            final Map<LocalDate, List<ProLineRecord>> lines,
            final SortedSet<ProLineRecord> ligneEnErreur) throws PersistenceException,
            InsertionDatabaseException {
        Iterator<Entry<LocalDate, List<ProLineRecord>>> iterator = lines.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<LocalDate, List<ProLineRecord>> lineDateEntry = iterator.next();
            LocalDate date = lineDateEntry.getKey();
            for (ProLineRecord line : lineDateEntry.getValue()) {
                this.buildMesure(line, versionFile, ligneEnErreur, errorsReport, sessionProperties);
            }

        }
    }

    private long readLines(final CSVParser parser, final List<DatatypeVariableUnitePRO> dbVariables, final Map<LocalDate, List<ProLineRecord>> lines,
            long lineCount, final Map<String, VariableStatutValeur> variablesDescriptor,
            ErrorsReport errorsReport, DatasetDescriptorPRO datasetDescriptor) throws IOException {
        String[] values;
        while ((values = parser.getLine()) != null) {
            final CleanerValues cleanerValues = new CleanerValues(values);
            lineCount++;
            final List<VariableValue> variablesValues = new LinkedList();
            final LocalDate dateprelevement = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, cleanerValues.nextToken());
            final String codeech = cleanerValues.nextToken();
            final String labonom = cleanerValues.nextToken();
            final int numeroechan = Integer.parseInt(cleanerValues.nextToken());
            final int numerorepe = Integer.parseInt(cleanerValues.nextToken());
            final String codevpro = cleanerValues.nextToken();
            final float valeurpro = Float.parseFloat(cleanerValues.nextToken());
            final String statut = cleanerValues.nextToken();
            final String codeunite = cleanerValues.nextToken();
            final String codemethode = cleanerValues.nextToken();

            final String codehumidite = cleanerValues.nextToken();

            final ProLineRecord line = new ProLineRecord(dateprelevement, lineCount, labonom, numeroechan, numerorepe, codeech, codevpro, valeurpro, statut,
                    codemethode, codeunite, codehumidite);
            try {
                if (!lines.containsKey(dateprelevement)) {
                    lines.put(dateprelevement, new LinkedList<ProLineRecord>());
                }
                lines.get(dateprelevement).add(line);
            } catch (DateTimeException e) {
                errorsReport.addErrorMessage(String.format(
                        RecorderPRO.getPROMessage(RecorderPRO.PROPERTY_MSG_INVALID_DATE_TIME),
                        dateprelevement, DateUtil.DD_MM_YYYY));
            }
        }
        return lineCount;
    }

    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile,
            final ISessionPropertiesPRO sessionProperties, String fileEncoding,
            final DatasetDescriptorPRO datasetDescriptorPRO) throws BusinessException {
        super.processRecord(parser, versionFile, sessionProperties, fileEncoding, datasetDescriptorPRO);
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            // Création du fichier de dépôt en base
            String[] values = null;
            final List<DatatypeVariableUnitePRO> dbVariables = this.buildVariablesHeaderAndSkipHeader(parser,
                    datasetDescriptorPRO);

            final Map<LocalDate, List<ProLineRecord>> mesuresMapLines = new HashMap();
            final Map<String, VariableStatutValeur> variablesDescriptor = new HashMap();
            final long lineCount = datasetDescriptorPRO.getEnTete();
            this.readLines(parser, dbVariables, mesuresMapLines, lineCount, variablesDescriptor, errorsReport, datasetDescriptorPRO);
            final SortedSet<ProLineRecord> ligneEnErreur = new TreeSet();
            if (!errorsReport.hasErrors()) {
                this.buildLines(versionFile, sessionProperties, errorsReport, mesuresMapLines, ligneEnErreur);
            }
            this.recordErrors(errorsReport);
        } catch (final IOException | DateTimeException | PersistenceException e) {
            throw new BusinessException(e);
        } catch (InsertionDatabaseException ex) {
            LoggerFactory.getLogger(ProcessRecordPhysicoChimiePRO.class).error(ex.getMessage(), ex);
        }
    }

    private void recordErrors(final ErrorsReport errorsReport) throws PersistenceException {
        if (errorsReport.hasErrors()) {
            LOGGER.debug(errorsReport.getErrorsMessages());
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    public IMesurePhysicoChimiePRODAO<MesurePhysicoChimiePRO> getMesurePhysicoChimiePRODAO() {
        return mesurePhysicoChimiePRODAO;
    }

    public void setMesurePhysicoChimiePRODAO(IMesurePhysicoChimiePRODAO<MesurePhysicoChimiePRO> mesurePhysicoChimiePRODAO) {
        this.mesurePhysicoChimiePRODAO = mesurePhysicoChimiePRODAO;
    }

    public ILocalizationManager getLocalizationManager() {
        return localizationManager;
    }

    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public IEchantillonsProduitDAO getEchantillonsproduitDAO() {
        return echantillonsproduitDAO;
    }

    public void setEchantillonsproduitDAO(IEchantillonsProduitDAO echantillonsproduitDAO) {
        this.echantillonsproduitDAO = echantillonsproduitDAO;
    }

    @Override
    public DatasetDescriptor holdDatasetDescriptor() throws IOException, SAXException {
        DatasetDescriptor datasetDescriptor = null;

        if (datasetDescriptor == null) {
            datasetDescriptor = DatasetDescriptorBuilder.buildDescriptor(IMesurePhysicoChimieDAO.class.getResource(DATASET_DESCRIPTOR_XML).openStream());
        }
        return datasetDescriptor;

    }

    @Override
    public void setDataTypeVariableUnitePRODAO(IDataTypeVariableQualifiantDAO dataTypeVariableUnitePRODAO) {
        this.dataTypeVariableUnitePRODAO = dataTypeVariableUnitePRODAO;
    }

    @Override
    public void setVariPRODAO(final IVariablesPRODAO variableDAO) {
        this.variPRODAO = variableDAO;
    }

    public void setMethodeDAO(IMethodeDAO methodeDAO) {
        this.methodeDAO = methodeDAO;
    }

    public void setUniteproDAO(IUniteproDAO uniteproDAO) {
        this.uniteproDAO = uniteproDAO;
    }

    public void setHumiditeDAO(IHumiditeExpressionDAO humiditeDAO) {
        this.humiditeDAO = humiditeDAO;
    }

    public void setValeurProduitBrutDAO(IValeurProduitBrutDAO valeurProduitBrutDAO) {
        this.valeurProduitBrutDAO = valeurProduitBrutDAO;
    }

}
