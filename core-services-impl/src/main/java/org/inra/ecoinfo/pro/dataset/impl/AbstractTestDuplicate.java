/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.impl;

import com.google.common.base.Strings;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import org.inra.ecoinfo.dataset.config.IDatasetConfiguration;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.utils.ErrorsReport;
import org.inra.ecoinfo.pro.utils.IErrorsReport;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public abstract class AbstractTestDuplicate implements ITestDuplicates{
     protected static final Logger   LOGGER           = LoggerFactory.getLogger(AbstractTestDuplicate.class);

    protected IErrorsReport errorsReport = new ErrorsReport();
    
    protected IDatasetConfiguration configuration;

    public AbstractTestDuplicate() {
        super();
    }
     
    
    protected void testLineForDuplicatesDateInDB(String[] dates, String dateString,
            String timeString, final long lineNumber, VersionFile versionFile) {
        if (dates == null || dates.length != 4 || Strings.isNullOrEmpty(dateString)
                || Strings.isNullOrEmpty(timeString)) {
            return;
        }
        if (this.isLimitDate(dates[0], dates[1], dateString, timeString)) {
            this.testLineForDuplicatesLineinDb(dates[0], dates[1], lineNumber, versionFile);
        } else if (this.isLimitDate(dates[2], dates[3], dateString, timeString)) {
            this.testLineForDuplicatesLineinDb(dates[2], dates[3], lineNumber, versionFile);
        }
    }

    
    
    protected void testLineForDuplicatesLineinDb(String dateDBString, String timeDBString,
            final long lineNumber, VersionFile versionFile) {
        
    }


    public void setConfiguration(IDatasetConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param startDateDB
     * @param startTimeDB
     * @param startDate
     * @param startTime
     * @return
     */
    protected boolean isLimitDate(String startDateDB, String startTimeDB, String startDate, String startTime) {
        try {
            startTime= (startTime + ":00").substring(0, DateUtil.HH_MM_SS.length());
            startTimeDB= (startTimeDB + ":00").substring(0, DateUtil.HH_MM_SS.length());
            LocalDateTime date = DateUtil.readLocalDateTimeFromLocalDateAndLocaltime(DateUtil.DD_MM_YYYY,  startDate, DateUtil.HH_MM_SS, startTime);
            LocalDateTime dbDate = DateUtil.readLocalDateTimeFromLocalDateAndLocaltime(DateUtil.DD_MM_YYYY,  startDateDB, DateUtil.HH_MM_SS, startTimeDB);
            return date.equals(dbDate);
        } catch (DateTimeException e) {
            AbstractTestDuplicate.LOGGER.info("pas de date", e);
            return false;
        }
    }

    protected String getKey(final String... args) {
        final StringBuffer buf = new StringBuffer();
        for (int i = 0; i < args.length; i++) {
            if (i != 0) {
                buf.append(RecorderPRO.CST_UNDERSCORE);
            }
            buf.append(args[i]);

        }
        return buf.toString();
    }

    /**
     *
     * @param dateString
     * @param timeString
     * @return
     */
    protected String getLocalValue(String dateString, String timeString) {
        return String.format("%-"
                + RecorderPRO.DD_MM_YYYY_HHMMSS_READ.length() + "."
                + RecorderPRO.DD_MM_YYYY_HHMMSS_READ.length() + "s",
                dateString + timeString + ":00");
    }

    
    
    
    
    @Override
    public void addErrors(BadsFormatsReport badsFormatsReport) {
        badsFormatsReport
        .addException(new BusinessException(this.errorsReport.getErrorsMessages()));
    }

    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        throw new UnsupportedOperationException("Not supported yet. ABSTRACT TEST DUPLICATE"); 
    }

    @Override
    public boolean hasError() {
         return this.errorsReport.hasErrors();
    }

    /**
     * Sets the errors report.
     *
     * @param errorsReport
     *            the new errors report @link(IErrorsReport) {@link ErrorsReport} the new errors
     *            report
     */
    @Override
    public final void setErrorsReport(final IErrorsReport errorsReport) {

        this.errorsReport = errorsReport;
    }


}
