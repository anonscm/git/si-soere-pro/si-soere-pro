/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.jpa;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.IMesureProEtudieDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie;
import org.inra.ecoinfo.pro.dataset.itk.interventionproetudie.entity.MesureProEtudie_;

/**
 *
 * @author adiankha
 */
public class JPAMesureProEtudieDAO extends AbstractJPADAO<MesureProEtudie> implements IMesureProEtudieDAO<MesureProEtudie> {

    /**
     *
     * @param keymesure
     * @return
     */
    @Override
    public Optional<MesureProEtudie> getByKeys(String keymesure) {
        CriteriaQuery<MesureProEtudie> query = builder.createQuery(MesureProEtudie.class);
        Root<MesureProEtudie> m = query.from(MesureProEtudie.class);
        query
                .select(m)
                .where(
                        builder.equal(m.get(MesureProEtudie_.keymesure), keymesure)
                );
        return getOptional(query);
    }

}
