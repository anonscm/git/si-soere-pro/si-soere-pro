/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.impl;

import java.util.SortedMap;
import java.util.TreeMap;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author adiankha
 */
public class TestDuplicate extends AbstractTestDuplicate {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestDuplicate.class.getName());

    static final long serialVersionUID = 1L;

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.dataset.sol.physicochimie.messages";

    static final String PROPERTY_MSG_DUPLICATE_LINE_TRAVAILDUSOL = "PROPERTY_MSG_DUPLICATE_LINE_TRAVAILDUSOL";

    final SortedMap<String, Long> line;
    final SortedMap<String, String> lineCouvert;

    public TestDuplicate() {
        this.line = new TreeMap();
        this.lineCouvert = new TreeMap();
    }

    protected void addLine(final String date, String codedisp, String codeTrait, final String parcelle, String placette, String localisation,
            final String profondeurtravail, String interventionsol, final long lineNumber) {
        final String key = this.getKey(date, codedisp, codeTrait, parcelle, placette, localisation, profondeurtravail, interventionsol);
        final String keySol = this.getKey(date, codedisp, codeTrait, parcelle, placette, localisation, profondeurtravail, interventionsol);
        if (!this.line.containsKey(key)) {
            this.line.put(key, lineNumber);
            if (!this.lineCouvert.containsKey(keySol)) {
                this.lineCouvert.put(keySol, profondeurtravail);
            } else if (!this.lineCouvert.get(keySol).equals(interventionsol)) {
                this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(TestDuplicate.BUNDLE_SOURCE_PATH,
                        TestDuplicate.PROPERTY_MSG_DUPLICATE_LINE_TRAVAILDUSOL), lineNumber,
                        date, codedisp, codeTrait, parcelle, localisation, profondeurtravail, placette, this.lineCouvert.get(keySol), interventionsol));
            }
        } else {
            this.errorsReport.addErrorMessage(String.format(RecorderPRO.getPROMessageWithBundle(
                    TestDuplicate.BUNDLE_SOURCE_PATH,
                    TestDuplicate.PROPERTY_MSG_DUPLICATE_LINE_TRAVAILDUSOL), lineNumber, date, codedisp, codeTrait, parcelle, localisation, profondeurtravail, placette,
                    interventionsol, this.line.get(key)));

        }
    }

    @Override
    public void addLine(String[] values, long lineNumber, String[] dates, VersionFile versionFile) {
        this.addLine(values[0], values[1], values[2], values[3], values[4], values[5], values[7], values[8], lineNumber);
    }
}
