/**
 *
 */
package org.inra.ecoinfo.pro.refdata.raisonnement;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement;

/**
 * @author sophie
 *
 */
public class JPARaisonnementDAO extends AbstractJPADAO<Raisonnement> implements IRaisonnementDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.raisonnement.IRaisonnementDAO# getByLibelleListeRaisonnement(java.lang.String, org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement)
     */

    /**
     *
     * @param libelle
     * @param listeRaisonnement
     * @return
     */

    @Override
    public Optional<Raisonnement> getByNKey(String libelle, ListeRaisonnement listeRaisonnement) {
        CriteriaQuery<Raisonnement> query = builder.createQuery(Raisonnement.class);
        Root<Raisonnement> raisonnement = query.from(Raisonnement.class);
        query
                .select(raisonnement)
                .where(
                        builder.equal(raisonnement.get(Raisonnement_.libelle), libelle)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.raisonnement.IRaisonnementDAO# getByListeRaisonnement (org.inra.ecoinfo.pro.refdata.listeraisonnement.ListeRaisonnement)
     */

    /**
     *
     * @param listeRaisonnement
     * @return
     */

    @SuppressWarnings("unchecked")
    @Override
    public List<Raisonnement> getByListeRaisonnement(ListeRaisonnement listeRaisonnement) {
        CriteriaQuery<Raisonnement> query = builder.createQuery(Raisonnement.class);
        Root<Raisonnement> raisonnement = query.from(Raisonnement.class);
        Join<Raisonnement, ListeRaisonnement> liste = raisonnement.join(Raisonnement_.listeRaisonnement);
        query
                .select(raisonnement)
                .where(
                        builder.equal(raisonnement.get(Raisonnement_.listeRaisonnement), listeRaisonnement)
                );
        return getResultList(query);
    }

}
