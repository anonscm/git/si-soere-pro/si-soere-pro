/**
 *
 */
package org.inra.ecoinfo.pro.refdata.rolestructure;

import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPARoleStructureDAO extends AbstractJPADAO<RoleStructure> implements IRoleStructureDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.rolestructure.IRoleStructureDAO#getByLibelle (java.lang.String)
     */

    /**
     *
     * @param libelle
     * @return
     */

    @Override
    public Optional<RoleStructure> getByNKey(String libelle) {
        CriteriaQuery<RoleStructure> query = builder.createQuery(RoleStructure.class);
        Root<RoleStructure> roleStructure = query.from(RoleStructure.class);
        query
                .select(roleStructure)
                .where(
                        builder.equal(roleStructure.get(RoleStructure_.libelle), libelle)
                );
        return getOptional(query);
    }

}
