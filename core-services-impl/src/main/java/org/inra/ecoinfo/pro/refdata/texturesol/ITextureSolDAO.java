package org.inra.ecoinfo.pro.refdata.texturesol;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

public interface ITextureSolDAO extends IDAO<Texturesol> {

    List<Texturesol> getAll();

    Optional<Texturesol> getByNKey(String nom);

}
