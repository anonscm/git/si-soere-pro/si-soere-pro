/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.fluxchambre.jpa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity;
import org.inra.ecoinfo.mga.business.composite.activities.ExtractActivity_;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.MesureFluxChambres_;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres;
import org.inra.ecoinfo.pro.dataset.fluxchambres.entity.ValeurFluxChambres_;
import org.inra.ecoinfo.pro.extraction.fluxchambre.IFluxChambreDAO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO_;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.variable.VariablesPRO;
import org.inra.ecoinfo.pro.synthesis.fluxchambre.SynthesisValue;
import org.inra.ecoinfo.pro.synthesis.physicochimieplantebrut.SynthesisValue_;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author vjkoyao
 */
public class JPAFluxChambreDAO extends AbstractJPADAO<MesureFluxChambres> implements IFluxChambreDAO {

    /**
     *
     * @param user
     * @return
     */
    @Override
    public List<Dispositif> getAvailableDispositif(IUser user) {
        ArrayList<Predicate> and = new ArrayList<>();
        CriteriaQuery<Dispositif> query = builder.createQuery(Dispositif.class);
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rnParcelle = rnt.join(RealNode_.parent);
        Join<RealNode, RealNode> rnDisp = rnParcelle.join(RealNode_.parent);
        and.add(builder.equal(ndsv, sv.get(SynthesisValue_.idNode)));
        Path<Dispositif> disp = builder.treat(rnDisp.get(RealNode_.nodeable), Dispositif.class);
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, sv.get(GenericSynthesisValue_.date));
        }
        query
                .select(disp)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    /**
     *
     * @param dispositifs
     * @param intervals
     * @param user
     * @return
     */
    @Override
    public List<NodeDataSet> getAvailablesVariablesByDispositif(List<Dispositif> dispositifs, IUser user) {
        ArrayList<Predicate> and = new ArrayList<>();
        CriteriaQuery<NodeDataSet> query = builder.createQuery(NodeDataSet.class);
        Root<NodeDataSet> ndsv = query.from(NodeDataSet.class);
        Root<SynthesisValue> sv = query.from(SynthesisValue.class);
        Join<NodeDataSet, RealNode> rnv = ndsv.join(NodeDataSet_.realNode);
        Join<RealNode, RealNode> rnd = rnv.join(RealNode_.parent);
        Join<RealNode, RealNode> rnt = rnd.join(RealNode_.parent);
        Join<RealNode, RealNode> rnParcelle = rnt.join(RealNode_.parent);
        Join<RealNode, RealNode> rnDisp = rnParcelle.join(RealNode_.parent);
        and.add(builder.equal(ndsv, sv.get(SynthesisValue_.idNode)));
        Path<Dispositif> disp = builder.treat(rnDisp.get(RealNode_.nodeable), Dispositif.class);
        and.add(disp.in(dispositifs));
        if (!user.getIsRoot()) {
            addRestrictiveRequestOnRoles(user, query, and, builder, ndsv, sv.get(GenericSynthesisValue_.date));
        }
        query
                .select(ndsv)
                .distinct(true)
                .where(builder.and(and.toArray(new Predicate[and.size()])));
        return getResultList(query);
    }

    /**
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user
     * @return
     */
    @Override
    public List<MesureFluxChambres> extractFluxChambre(List<INodeable> selectedDispositif,
            List<VariablesPRO> selectedVariables, IntervalDate interval, IUser user) {
        CriteriaQuery<MesureFluxChambres> criteria = buildQuery(selectedDispositif, selectedVariables, interval, user,
                false);
        return getResultList(criteria);
    }

    /**
     *
     * @param selectedDispositif
     * @param selectedVariables
     * @param interval
     * @param user
     * @return
     */
    @Override
    public Long sizeFluxChambre(List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariables,
            IntervalDate interval, IUser user) {
        CriteriaQuery<Long> criteria = buildQuery(selectedDispositif, selectedVariables, interval, user, true);
        return getOptional(criteria).orElse(-1L);
    }

    /**
     *
     * @param selectedDispositif
     * @param selectedVariable
     * @param intervalDate
     * @param user
     * @param isCount
     * @return
     */
    public CriteriaQuery buildQuery(final List<INodeable> selectedDispositif, List<VariablesPRO> selectedVariable,
            IntervalDate intervalDate, IUser user, boolean isCount) {

        CriteriaQuery criteria;
        if (isCount) {
            criteria = builder.createQuery(Long.class);
        } else {
            criteria = builder.createQuery(MesureFluxChambres.class);
        }
        Root<ValeurFluxChambres> v = criteria.from(ValeurFluxChambres.class);
        Join<ValeurFluxChambres, MesureFluxChambres> m = v.join(ValeurFluxChambres_.mesureFluxChambres);
        Join<ValeurFluxChambres, RealNode> rnVariable = v.join(ValeurFluxChambres_.realNode);
        Join<RealNode, RealNode> rnDatatype = rnVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> rnTheme = rnDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> rnParcelle = rnTheme.join(RealNode_.parent);
        Join<RealNode, RealNode> rnDispositif = rnParcelle.join(RealNode_.parent);
        Join<RealNode, Nodeable> ndbDispositif = rnDispositif.join(RealNode_.nodeable);
        Root<DatatypeVariableUnitePRO> dvu = criteria.from(DatatypeVariableUnitePRO.class);
        Join<DatatypeVariableUnitePRO, VariablesPRO> variable = dvu.join(DatatypeVariableUnitePRO_.variablespro);
        final Path<Nodeable> ndbDVU = rnVariable.get(RealNode_.nodeable);
        Root<NodeDataSet> vns = criteria.from(NodeDataSet.class);
        ArrayList<Predicate> predicatesAnd = new ArrayList();
        predicatesAnd.add(builder.equal(rnVariable, vns.get(NodeDataSet_.realNode)));
        Join<RealNode, Nodeable> ndbDvu = rnVariable.join(RealNode_.nodeable);
        predicatesAnd.add(builder.equal(dvu, ndbDVU));
        predicatesAnd.add(ndbDispositif.in(selectedDispositif));
        predicatesAnd.add(variable.in(selectedVariable));
        predicatesAnd.add(builder.between(m.<LocalDate>get(MesureFluxChambres_.LocalDate),
                intervalDate.getBeginDate().toLocalDate(), intervalDate.getEndDate().toLocalDate()));
        final Path<LocalDate> dateMesure = m.<LocalDate>get(MesureFluxChambres_.LocalDate);
        if (!isCount) {
            addRestrictiveRequestOnRoles(user, criteria, predicatesAnd, builder, vns, dateMesure);
        }
        Predicate where = builder.and(predicatesAnd.toArray(new Predicate[predicatesAnd.size()]));
        criteria.where(where);
        if (isCount) {
            criteria.select(builder.countDistinct(m));
        } else {
            criteria.select(m);
            List<Order> orders = new ArrayList();
            orders.add(builder.asc(ndbDispositif));
            orders.add(builder.asc(m.get(MesureFluxChambres_.LocalDate)));
            orders.add(builder.asc(ndbDVU));
            criteria.orderBy(orders);
        }
        return criteria;
    }

    /**
     *
     * @param user
     * @param criteria
     * @param predicatesAnd
     * @param builder
     * @param vns
     * @param dateMesure
     */
    @SuppressWarnings("unchecked")
    protected void addRestrictiveRequestOnRoles(IUser user, CriteriaQuery criteria, List<Predicate> predicatesAnd,
            CriteriaBuilder builder, Path<NodeDataSet> vns, final Path<? extends TemporalAdjuster> dateMesure) {
        if (!user.getIsRoot()) {
            Root<ExtractActivity> er = criteria.from(ExtractActivity.class);
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.login), user.getLogin()));
            predicatesAnd.add(builder.equal(er.get(ExtractActivity_.idNode), vns.get(NodeDataSet_.id)));
            predicatesAnd.add(
                    whereDateBetween(dateMesure, er.get(ExtractActivity_.dateStart), er.get(ExtractActivity_.dateEnd)));
        }
    }

}
