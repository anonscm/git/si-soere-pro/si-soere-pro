/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.incubationsolbrut.jpa;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.incubation.entity.MesureIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol;
import org.inra.ecoinfo.pro.dataset.incubation.entity.ValeurIncubationSol_;
import org.inra.ecoinfo.pro.extraction.incubation.IIncubationDatatypeManager;
import org.inra.ecoinfo.pro.extraction.incubation.jpa.AbstractJPAIncubationDAO;
import org.inra.ecoinfo.pro.synthesis.incubationsol.SynthesisValue;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author vjkoyao
 */
public class JPAIncubationSolDAO extends AbstractJPAIncubationDAO<MesureIncubationSol, ValeurIncubationSol>{

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurIncubationSol> getValeurIncubationClass() {
        return ValeurIncubationSol.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureIncubationSol> getMesureIncubationClass() {
        return MesureIncubationSol.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected SingularAttribute<ValeurIncubationSol, MesureIncubationSol> getMesureAttribute() {
        return ValeurIncubationSol_.mesureIncubationSol;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<? extends GenericSynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getDatatypeCode() {
        return IIncubationDatatypeManager.CODE_DATATYPE_INCUBATION_SOL;
    }
}
    

