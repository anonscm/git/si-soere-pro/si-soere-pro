package org.inra.ecoinfo.pro.refdata.protocoledispositif;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.protocole.Protocole;

/**
 * @author sophie
 *
 */
public interface IProtocoleDispositifDAO extends IDAO<ProtocoleDispositif> {

    /**
     *
     * @param protocole
     * @param dispositif
     * @return
     */
    Optional<ProtocoleDispositif> getByNKey(Protocole protocole, Dispositif dispositif);

}
