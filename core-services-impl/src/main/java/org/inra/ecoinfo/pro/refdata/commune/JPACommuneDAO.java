package org.inra.ecoinfo.pro.refdata.commune;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.departement.Departement;

/**
 * @author sophie
 *
 */
public class JPACommuneDAO extends AbstractJPADAO<Commune> implements ICommuneDAO {

    /* (non-Javadoc)
	 * @see org.inra.ecoinfo.pro.refdata.commune.ICommuneDAO#getAll()
     */
    @Override
    public List<Commune> getAll() {
        CriteriaQuery<Commune> query = builder.createQuery(Commune.class);
        Root<Commune> commune = query.from(Commune.class);
        query
                .select(commune)
                .orderBy(builder.asc(commune.get(Commune_.nomCommune)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.pays.ICommuneDAO#getByCodePostal(java.lang .String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Commune> getByCodePostal(String codePostal) {
        CriteriaQuery<Commune> query = builder.createQuery(Commune.class);
        Root<Commune> commune = query.from(Commune.class);
        query
                .select(commune)
                .where(
                        builder.equal(commune.get(Commune_.codePostal), codePostal)
                )
                .orderBy(builder.asc(commune.get(Commune_.nomCommune)));
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.pays.ICommuneDAO#getByNKey(java .lang.String, java.lang.String)
     */
    @Override
    public Optional<Commune> getByNKey(String nomCommune, String codePostal) {
        CriteriaQuery<Commune> query = builder.createQuery(Commune.class);
        Root<Commune> commune = query.from(Commune.class);
        query
                .select(commune)
                .where(
                        builder.equal(commune.get(Commune_.codePostal), codePostal),
                        builder.equal(commune.get(Commune_.nomCommune), nomCommune)
                );
        return getOptional(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.pays.ICommuneDAO#getByDepartement(org.inra .ecoinfo.pro.refdata.pays.Departement)
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Commune> getByDepartement(Departement departement) {
        CriteriaQuery<Commune> query = builder.createQuery(Commune.class);
        Root<Commune> commune = query.from(Commune.class);
        Join<Commune, Departement> dept = commune.join(Commune_.departement);
        query
                .select(commune)
                .where(
                        builder.equal(dept, departement)
                );
        return getResultList(query);
    }
    
}
