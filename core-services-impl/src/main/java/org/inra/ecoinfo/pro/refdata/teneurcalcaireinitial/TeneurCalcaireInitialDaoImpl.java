package org.inra.ecoinfo.pro.refdata.teneurcalcaireinitial;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.pro.refdata.texturesol.Texturesol;
import org.inra.ecoinfo.utils.Utils;

/**
 *
 * @author ptcherniati
 */
public class TeneurCalcaireInitialDaoImpl extends AbstractJPADAO<Teneurcalcaireinitial> implements ITeneurCalcaireInitialDAO {

    /**
     *
     * @return
     */
    @Override
    public List<Teneurcalcaireinitial> getAll() {
        return getAll(Teneurcalcaireinitial.class);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<Teneurcalcaireinitial> getByNKey(String nom, Texturesol texturesol) {
        CriteriaQuery<Teneurcalcaireinitial> query = builder.createQuery(Teneurcalcaireinitial.class);
        Root<Teneurcalcaireinitial> methodeProcess = query.from(Teneurcalcaireinitial.class);
        query
                .select(methodeProcess)
                .where(
                        builder.equal(methodeProcess.get(Teneurcalcaireinitial_.code), Utils.createCodeFromString(nom)),
                        builder.equal(methodeProcess.get(Teneurcalcaireinitial_.texturesol), texturesol)
                );
        return getOptional(query);
    }
}
