/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl;

import com.google.common.base.Strings;
import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.pro.dataset.impl.AbstractSessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author adiankha
 */
public abstract class AbstractPlanteSessionProperties extends AbstractSessionPropertiesPRO implements ISessionPropertiesPlanteBrut {

    /**
     *
     */
    public AbstractPlanteSessionProperties() {
        super();
    }

    /**
     *
     * @param dateString
     * @param timeString
     * @return
     */
    public String dateToString(String dateString, String timeString) {
        if (Strings.isNullOrEmpty(dateString)) {
            return org.apache.commons.lang.StringUtils.EMPTY;
        }
        if (Strings.isNullOrEmpty(timeString)) {
            timeString = "00:00:00";
        }
        LocalDate date;
        LocalTime time;
        date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateString);
        if ("24:00".equals(timeString) || "24:00:00".equals(timeString)) {
            date = date.plusDays(1);
            timeString = "00:00:00";
        }
        timeString = String.format("%s:00", timeString).substring(0, 8);
        time = DateUtil.readLocalTimeFromText(RecorderPRO.HH_MM_SS, timeString);
        return RecorderPRO.getDateTimeForCompareUTC(date, time);
    }

}
