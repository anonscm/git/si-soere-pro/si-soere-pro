/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.synthesis.itk;

import javax.persistence.metamodel.SingularAttribute;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol_;
import org.inra.ecoinfo.pro.synthesis.travaildusol.SynthesisDatatype;
import org.inra.ecoinfo.pro.synthesis.travaildusol.SynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class TravailDuSolSynthesisDAO extends AbstractSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurTravailDuSol, MesureTravailDuSol> {

    @Override
    Class<ValeurTravailDuSol> getValueClass() {
        return ValeurTravailDuSol.class;
    }

    @Override
    Class<SynthesisValue> getSynthesisValueClass() {
        return SynthesisValue.class;
    }

    @Override
    protected SingularAttribute<ValeurTravailDuSol, MesureTravailDuSol> getMesureAttribute() {
        return ValeurTravailDuSol_.mesuretravaildusol;
    }

    @Override
    Boolean isValue() {
        return Boolean.FALSE;
    }

}
