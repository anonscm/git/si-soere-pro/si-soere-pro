/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.incubation.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.refdata.datatypevariablequalifiant.DatatypeVariableUnitePRO;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.utils.LoggerForExtraction;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author vjkoyao
 */
public class IncubationRequestReminderOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_CODE = "incubationRequestReminder";

    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.incubation.incubation-messages";

    static final String PROPERTY_MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    static final String PROPERTY_MSG_EXTRACTION_DATE = "PROPERTY_MSG_EXTRACTION_DATE";

    static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";

    static final String PROPERTY_MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";

    static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL = "PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_PRO = "PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_PRO";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_MOY = "PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_MOY";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_PRO_MOY = "PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_PRO_MOY";

    static final String PROPERTY_MSG_SELECTED_DISPOSITIFS = "PROPERTY_MSG_SELECTED_DISPOSITIFS";

    static final String PATTERN_STRING_SITES_SUMMARY = "   %s";

    static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";

    static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    static final String KEYMAP_COMMENTS = "comments";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Map<String, File> reminderMap = new HashMap();
        final File reminderFile = this.buildOutputFile(AbstractOutputBuilder.FILENAME_REMINDER,
                AbstractOutputBuilder.EXTENSION_TXT);
        PrintStream reminderPrintStream;
        try {
            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.displayName());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();
            final LocalDateTime date = LocalDateTime.now();
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    IncubationRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    IncubationRequestReminderOutputBuilder.PROPERTY_MSG_EXTRACTION_DATE));
            String formatDate;
            String formatTime;
            reminderPrintStream.println(String.format("%s %s",
                    DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY),
                    DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.HH_MM_SS))
            );
            reminderPrintStream.println();
            this.printDispositifSummary(
                    (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName()),
                    reminderPrintStream);
            this.printVariablesSummary(
                    (List<DatatypeVariableUnitePRO>) requestMetadatasMap.get(DatatypeVariableUnitePRO.class.getSimpleName().concat(
                            IncubationParameterVO.INCUBATIONSOL)),
                    (List<DatatypeVariableUnitePRO>) requestMetadatasMap.get(DatatypeVariableUnitePRO.class.getSimpleName().concat(
                            IncubationParameterVO.INCUBATIONSOLPRO)),
                    (List<DatatypeVariableUnitePRO>) requestMetadatasMap.get(DatatypeVariableUnitePRO.class.getSimpleName().concat(
                            IncubationParameterVO.INCUBATIONSOLMOY)),
                    (List<DatatypeVariableUnitePRO>) requestMetadatasMap.get(DatatypeVariableUnitePRO.class.getSimpleName().concat(
                            IncubationParameterVO.INCUBATIONSOLPROMOY)), reminderPrintStream);
            this.printDatesSummary((IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName()), reminderPrintStream);
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    IncubationRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                    IncubationRequestReminderOutputBuilder.PROPERTY_MSG_EXTRACTION_COMMENTS));
            reminderPrintStream
                    .println(String.format(
                            IncubationRequestReminderOutputBuilder.PATTERN_STRING_COMMENTS_SUMMARY,
                            requestMetadatasMap
                                    .get(IncubationRequestReminderOutputBuilder.KEYMAP_COMMENTS)));
        } catch (final FileNotFoundException | UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderMap.put(AbstractOutputBuilder.FILENAME_REMINDER, reminderFile);
        reminderPrintStream.flush();
        reminderPrintStream.close();
        LoggerForExtraction.logRequest((Utilisateur) policyManager.getCurrentUser(), reminderFile);
        return reminderMap;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return this.getLocalizationManager().getMessage(
                IncubationRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                IncubationRequestReminderOutputBuilder.PROPERTY_MSG_HEADER);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters,
                        IncubationRequestReminderOutputBuilder.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    void printDatesSummary(final IntervalDate intervalDate,
            final PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                IncubationRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                IncubationRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_PERIODS));
        try {
            intervalDate.toLocalString(reminderPrintStream, localizationManager);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }

    void printDispositifSummary(final List<Dispositif> list, final PrintStream reminderPrintStream) {
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                IncubationRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                IncubationRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DISPOSITIFS));
        list.stream().forEach((dispositif) -> {
            reminderPrintStream.println(String.format(
                    IncubationRequestReminderOutputBuilder.PATTERN_STRING_SITES_SUMMARY,
                    dispositif.getName()));
        });
        reminderPrintStream.println();
    }

    /**
     *
     * @param variables
     * @param reminderPrintStream
     * @param propertiesVariableName
     * @param keyLocalizedVariableName
     */
    protected void printVariableSummary(
            List<DatatypeVariableUnitePRO> variables,
            PrintStream reminderPrintStream,
            final Properties propertiesVariableName,
            String keyLocalizedVariableName
    ) {
        String localizedName;
        if (!variables.isEmpty()) {
            reminderPrintStream.println(this.getLocalizationManager().getMessage(BUNDLE_SOURCE_PATH, keyLocalizedVariableName
            ));
        }
        for (final DatatypeVariableUnitePRO variable : variables) {
            localizedName = propertiesVariableName.getProperty(variable.getVariablespro().getName());
            reminderPrintStream.println(String.format(PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getVariablespro().getName() : localizedName));
        }
    }

    private void printVariablesSummary(
            final List<DatatypeVariableUnitePRO> variablesSol,
            final List<DatatypeVariableUnitePRO> variablesSolPro,
            final List<DatatypeVariableUnitePRO> variablesSolMoy,
            final List<DatatypeVariableUnitePRO> variablesSolProMoy,
            final PrintStream reminderPrintStream) {

        final Properties propertiesVariableName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(DatatypeVariableUnitePRO.class), Nodeable.ENTITE_COLUMN_NAME);
        String localizedName;
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                IncubationRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH,
                IncubationRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES));

        printVariableSummary(variablesSol, reminderPrintStream, propertiesVariableName, PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL);
        printVariableSummary(variablesSolPro, reminderPrintStream, propertiesVariableName, PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_PRO);
        printVariableSummary(variablesSolMoy, reminderPrintStream, propertiesVariableName, PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_MOY);
        printVariableSummary(variablesSolProMoy, reminderPrintStream, propertiesVariableName, PROPERTY_MSG_SELECTED_VARIABLES_INCUBATION_SOL_PRO_MOY);

        reminderPrintStream.println();
    }

}
