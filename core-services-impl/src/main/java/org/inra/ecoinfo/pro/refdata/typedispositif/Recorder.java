/**
 *
 */
package org.inra.ecoinfo.pro.refdata.typedispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.pro.dataset.Constantes;
import org.inra.ecoinfo.pro.refdata.RefDataConstantes;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<TypeDispositif> {
    
    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.messages";
    
    ITypeDispositifDAO typeDispositifDAO;

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = parser.getLine();
            while (values != null) {
                
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                
                String libelle = tokenizerValues.nextToken();
                
                typeDispositifDAO.remove(typeDispositifDAO.getByNKey(libelle)
                        .orElseThrow(() -> new BusinessException("can't get type dispositif")));
                
                values = parser.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
    
    @Override
    protected List<TypeDispositif> getAllElements() throws BusinessException {
        return typeDispositifDAO.getAll();
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(TypeDispositif typeDispositif) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        
        Properties propertiesLibelle = localizationManager.newProperties(TypeDispositif.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_TPD, Locale.ENGLISH);
        
        String localizedChampLibelle = "";
        
        if (typeDispositif != null) {
            localizedChampLibelle = propertiesLibelle.containsKey(typeDispositif.getLibelle()) ? propertiesLibelle.getProperty(typeDispositif.getLibelle()) : typeDispositif.getLibelle();
        }
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeDispositif == null ? Constantes.STRING_EMPTY : typeDispositif.getLibelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(typeDispositif == null ? Constantes.STRING_EMPTY : localizedChampLibelle, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        
        return lineModelGridMetadata;
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        long line = 0;
        
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, TypeDispositif.TABLE_NAME);
                
                String libelle = tokenizerValues.nextToken();
                int indexLibelle = tokenizerValues.currentTokenIndex();
                
                String code = Utils.createCodeFromString(libelle);
                
                if (libelle == null || libelle.isEmpty()) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "VALEUR_OBLIGATOIRE"), line + 1, indexLibelle - 1, datasetDescriptor.getColumns().get(0).getName()));
                }
                
                TypeDispositif typeDispositif = new TypeDispositif(libelle, code);
                TypeDispositif dbTypeDispositif = typeDispositifDAO.getByNKey(libelle).orElse(null);
                
                if (!errorsReport.hasErrors()) {
                    if (dbTypeDispositif == null) {
                        typeDispositifDAO.saveOrUpdate(typeDispositif);
                    } else {
                        dbTypeDispositif.setLibelle(libelle);
                        dbTypeDispositif.setCode(code);
                        typeDispositifDAO.saveOrUpdate(dbTypeDispositif);
                    }
                }
                
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (IOException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @param typeDispositifDAO
     */
    public void setTypeDispositifDAO(ITypeDispositifDAO typeDispositifDAO) {
        this.typeDispositifDAO = typeDispositifDAO;
    }
    
}
