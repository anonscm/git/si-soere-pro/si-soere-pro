/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.historiquedispositif;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.pro.refdata.AbstractGenericRecorder;
import org.inra.ecoinfo.pro.refdata.RefDataUtil;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.refdata.dispositif.IDispositifDAO;
import org.inra.ecoinfo.pro.refdata.faitremarquable.Faitremarquable;
import org.inra.ecoinfo.pro.refdata.faitremarquable.IFaitRemarquableDAO;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author adiankha
 */
public class Recorder extends AbstractGenericRecorder<HistoriqueDispositif> {

    private static final String PROPERTY_MSG_HISTODISPO_BAD_CODEDISPO = "PROPERTY_MSG_HISTODISPO_BAD_CODEDISPO";
    private static final String PROPERTY_MSG_HISTODISPO_BAD_NOMFAIT = "PROPERTY_MSG_HISTODISPO_BAD_NOMFAIT";
    private static final String PRO_TYPO_SOURCE_PATH = "org.inra.ecoinfo.pro.refdata.message";

    IDispositifDAO dispositifDAO;
    IFaitRemarquableDAO faitremarquableDAO;
    IHistoriqueDispositifDAO histodispoDAO;
    private String[] listeDispositifPossibles;
    private String[] listeFaitRemarquablePossibles;
    private Map<String, String[]> listeLieuPossibles;
    Properties CommentaireEn;

    private void createHistoriqueDispositif(final HistoriqueDispositif histodispo) throws BusinessException {
        try {
            histodispoDAO.saveOrUpdate(histodispo);
            histodispoDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void updateHistoriqueDispositif(Dispositif dispositif, Faitremarquable fait, final int datehd, String commentaire, final HistoriqueDispositif dbhistodispo) throws BusinessException {
        try {
            dbhistodispo.setHid_date(datehd);
            dbhistodispo.setCommentaire(commentaire);
            dbhistodispo.setDispositif(dispositif);
            dbhistodispo.setFaitremarquable(fait);
            histodispoDAO.saveOrUpdate(dbhistodispo);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create booleans");
        }
    }

    private void createOrUpdateHistoDispo(int date, String commentaire, Dispositif dispo, Faitremarquable faitremar, HistoriqueDispositif dbfaitremar) throws BusinessException {
        if (dbfaitremar == null) {
            final HistoriqueDispositif histodispo = new HistoriqueDispositif(date, commentaire, dispo, faitremar);
            histodispo.setHid_date(date);
            histodispo.setCommentaire(commentaire);
            histodispo.setDispositif(dispo);
            histodispo.setFaitremarquable(faitremar);
            createHistoriqueDispositif(histodispo);
        } else {
            updateHistoriqueDispositif(dispo, faitremar, date, commentaire, dbfaitremar);
        }
    }

    private void persistHistoDispo(int date, String commentaire, Dispositif dispo, Faitremarquable fait) throws PersistenceException, BusinessException {
        final HistoriqueDispositif dbhistodispo = histodispoDAO.getByNKey(fait, dispo).orElse(null);
        createOrUpdateHistoDispo(date, commentaire, dispo, fait, dbhistodispo);
    }

    private void disposifPossibles() {
        Map<String, String[]> dispositifLieu = new HashMap<>();
        Map<String, Set<String>> dispositifLieuList = new HashMap<>();
        List<Dispositif> groupedispositif = dispositifDAO.getAll();
        groupedispositif.forEach((dispositif) -> {
            String dispo = dispositif.getCode();
            String lieu = dispositif.getLieu().getNom();
            if (!dispositifLieuList.containsKey(dispo)) {
                dispositifLieuList.put(dispo, new TreeSet());
            }
            dispositifLieuList.get(dispo).add(lieu);
        });
        dispositifLieuList.entrySet().forEach((entryProduit) -> {
            dispositifLieu.put(entryProduit.getKey(), entryProduit.getValue().toArray(new String[]{}));
        });
        this.listeLieuPossibles = dispositifLieu;
        listeDispositifPossibles = dispositifLieu.keySet().toArray(new String[]{});
    }

    private void faitremarPossibles() {
        List<Faitremarquable> groupefait = faitremarquableDAO.getAll();
        String[] Listefait = new String[groupefait.size() + 1];
        Listefait[0] = "";
        int index = 1;
        for (Faitremarquable fait : groupefait) {
            Listefait[index++] = fait.getNom();
        }
        this.listeFaitRemarquablePossibles = Listefait;
    }

    @Override
    public void deleteRecord(CSVParser csvp, File file, String string) throws BusinessException {
        try {
            String[] values = csvp.getLine();
            while (values != null) {
                Dispositif dispositif = null;
                Faitremarquable fait = null;
                TokenizerValues tokenizerValues = new TokenizerValues(values, HistoriqueDispositif.NAME_ENTITY_JPA);
                String code = tokenizerValues.nextToken();
                String lieu = tokenizerValues.nextToken();
                String faitr = tokenizerValues.nextToken();
                dispositif = dispositifDAO.getByNKey(code, lieu).orElse(null);
                fait = faitremarquableDAO.getByNKey(faitr).orElse(null);
                HistoriqueDispositif histoire = histodispoDAO.getByNKey(fait, dispositif)
                        .orElseThrow(() -> new BusinessException("can't find HistoriqueDispositif"));
                histodispoDAO.remove(histoire);
                values = csvp.getLine();
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    protected List<HistoriqueDispositif> getAllElements() throws BusinessException {
        return histodispoDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            skipHeader(parser);
            long line = 0;
            String[] values = parser.getLine();
            while (values != null) {
                line++;
                final TokenizerValues tokenizerValues = new TokenizerValues(values, HistoriqueDispositif.NAME_ENTITY_JPA);
                int indexdispo = tokenizerValues.currentTokenIndex();
                final String dispo = tokenizerValues.nextToken();
                final String lieu = tokenizerValues.nextToken();
                int indexfait = tokenizerValues.currentTokenIndex();
                final String fait = tokenizerValues.nextToken();
                int dat = verifieInt(tokenizerValues, line, false, errorsReport);
                final String commentaire = tokenizerValues.nextToken();

                Dispositif dbdispositif = dispositifDAO.getByNKey(dispo, lieu).orElse(null);
                if (dbdispositif == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HISTODISPO_BAD_CODEDISPO), line, indexdispo, dispo));
                }
                Faitremarquable dbfait = faitremarquableDAO.getByNKey(fait).orElse(null);
                if (dbfait == null) {
                    errorsReport.addErrorMessage(String.format(localizationManager.getMessage(Recorder.PRO_TYPO_SOURCE_PATH, Recorder.PROPERTY_MSG_HISTODISPO_BAD_NOMFAIT), line, indexfait, fait));
                }

                if (!errorsReport.hasErrors()) {
                    persistHistoDispo(dat, commentaire, dbdispositif, dbfait);
                }
                values = parser.getLine();
            }
            RefDataUtil.gestionErreurs(errorsReport);
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(HistoriqueDispositif historiquedispositif) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();

        String valeurProduit = historiquedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : historiquedispositif.getDispositif() != null ? historiquedispositif.getDispositif().getCodeDispo(): "";
        ColumnModelGridMetadata columnDispo = new ColumnModelGridMetadata(valeurProduit, listeDispositifPossibles, null, true, false, true);
        String valeurProcede = historiquedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : historiquedispositif.getDispositif() != null ? historiquedispositif.getDispositif().getLieu().getNom() : "";
        ColumnModelGridMetadata columnLieu = new ColumnModelGridMetadata(valeurProcede, listeLieuPossibles, null, true, false, true);
        List<ColumnModelGridMetadata> refsProduits = new LinkedList<ColumnModelGridMetadata>();
        refsProduits.add(columnLieu);
        columnLieu.setValue(valeurProcede);
        columnDispo.setRefs(refsProduits);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnDispo);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(columnLieu);

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(historiquedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : historiquedispositif.getFaitremarquable().getNom() != null ? historiquedispositif.getFaitremarquable().getNom() : "",
                        listeFaitRemarquablePossibles, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(historiquedispositif == null || historiquedispositif.getHid_date() == EMPTY_INT_VALUE ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : historiquedispositif.getHid_date(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(historiquedispositif == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : historiquedispositif.getCommentaire(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(historiquedispositif == null || historiquedispositif.getCommentaire() == null ? AbstractCSVMetadataRecorder.EMPTY_STRING
                        : CommentaireEn.getProperty(historiquedispositif.getCommentaire()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false,
                        true, false));
        return lineModelGridMetadata;
    }

    @Override
    protected ModelGridMetadata<HistoriqueDispositif> initModelGridMetadata() {
        disposifPossibles();
        faitremarPossibles();
        CommentaireEn = localizationManager.newProperties(HistoriqueDispositif.NAME_ENTITY_JPA, HistoriqueDispositif.COLUMN_COMMENT_JPA, Locale.ENGLISH);
        return super.initModelGridMetadata();

    }

    /**
     *
     * @return
     */
    public IFaitRemarquableDAO getFaitremarquableDAO() {
        return faitremarquableDAO;
    }

    /**
     *
     * @return
     */
    public IHistoriqueDispositifDAO getHistodispoDAO() {
        return histodispoDAO;
    }

    /**
     *
     * @param dispositifDAO
     */
    public void setDispositifDAO(IDispositifDAO dispositifDAO) {
        this.dispositifDAO = dispositifDAO;
    }

    /**
     *
     * @param faitremarquableDAO
     */
    public void setFaitremarquableDAO(IFaitRemarquableDAO faitremarquableDAO) {
        this.faitremarquableDAO = faitremarquableDAO;
    }

    /**
     *
     * @param histodispoDAO
     */
    public void setHistodispoDAO(IHistoriqueDispositifDAO histodispoDAO) {
        this.histodispoDAO = histodispoDAO;
    }

    /**
     *
     * @return
     */
    public IDispositifDAO getDispositifDAO() {
        return dispositifDAO;
    }

    /**
     *
     * @return
     */
    public Properties getCommentaireEn() {
        return CommentaireEn;
    }

    /**
     *
     * @param CommentaireEn
     */
    public void setCommentaireEn(Properties CommentaireEn) {
        this.CommentaireEn = CommentaireEn;
    }

}
