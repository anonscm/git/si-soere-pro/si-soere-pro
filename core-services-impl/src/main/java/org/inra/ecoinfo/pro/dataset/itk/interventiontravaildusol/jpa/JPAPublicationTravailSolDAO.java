/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.pro.dataset.JPA.ILocalPublicationDAO;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.MesureTravailDuSol_;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol;
import org.inra.ecoinfo.pro.dataset.itk.interventiontravaildusol.entity.ValeurTravailDuSol_;

/**
 *
 * @author adiankha
 */
public class JPAPublicationTravailSolDAO extends JPAVersionFileDAO implements ILocalPublicationDAO{
    
    /**
     *
     * @param version
     */
    @Override
    public void removeVersion(VersionFile version) {
        deleteValeurs(version);
        deleteSequences(version);
    }

    private void deleteValeurs(VersionFile version) {
        CriteriaDelete<ValeurTravailDuSol> deleteValeurs = builder.createCriteriaDelete(ValeurTravailDuSol.class);
        Root<ValeurTravailDuSol> valeur = deleteValeurs.from(ValeurTravailDuSol.class);
        Subquery<MesureTravailDuSol> subquery = deleteValeurs.subquery(MesureTravailDuSol.class);
        Root<MesureTravailDuSol> m = subquery.from(MesureTravailDuSol.class);
        subquery.select(m)
                .where(builder.equal(m.get(MesureTravailDuSol_.versionfile), version));
        deleteValeurs.where(valeur.get(ValeurTravailDuSol_.mesuretravaildusol).in(subquery));
        delete(deleteValeurs);
    }

    private void deleteSequences(VersionFile version) {
        CriteriaDelete<MesureTravailDuSol> deleteSequence = builder.createCriteriaDelete(MesureTravailDuSol.class);
        Root<MesureTravailDuSol> sequence = deleteSequence.from(MesureTravailDuSol.class);
        deleteSequence.where(builder.equal(sequence.get(MesureTravailDuSol_.versionfile), version));
        delete(deleteSequence);
    }
    
}
