package org.inra.ecoinfo.pro.refdata.produitstructurerole;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.pro.refdata.rolestructure.RoleStructure;
import org.inra.ecoinfo.pro.refdata.structure.Structure;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IProduitStructureRoleDAO extends IDAO<StructureProduitRole> {

    /**
     *
     * @return
     */
    List<StructureProduitRole> getAll();

    /**
     *
     * @param str_nom
     * @param prod_nom
     * @param role
     * @param role_libelle
     * @return
     * @throws PersistenceException
     */
    Optional<StructureProduitRole> getByNKey(Produits produits, Structure structure, RoleStructure role);

}
