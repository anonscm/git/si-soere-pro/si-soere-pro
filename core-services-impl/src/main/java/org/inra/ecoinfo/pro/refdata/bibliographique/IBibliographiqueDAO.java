/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.bibliographique;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;

/**
 *
 * @author adiankha
 */
public interface IBibliographiqueDAO  extends IDAO<Bibliographique>{

    /**
     *
     * @return
     */
    List<Bibliographique> getAll();

    /**
     *
     * @param doi
     * @return
     */
    Optional<Bibliographique> getByNKey(String doi);
    
}
