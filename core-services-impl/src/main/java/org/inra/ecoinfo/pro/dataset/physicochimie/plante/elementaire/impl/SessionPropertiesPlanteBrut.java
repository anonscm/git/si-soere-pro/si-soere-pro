/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.elementaire.impl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.SortedMap;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.pro.dataset.impl.AbstractSessionPropertiesPRO;
import org.inra.ecoinfo.pro.dataset.impl.ITestDuplicates;
import org.inra.ecoinfo.pro.dataset.impl.RecorderPRO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteBrutDataTypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.elementaire.impl.AbtractPROSessionProperties;
import org.inra.ecoinfo.pro.refdata.echantillonsproduit.EchantillonsProduit;
import org.inra.ecoinfo.pro.refdata.produit.Produits;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 *
 * @author adiankha
 */
public class SessionPropertiesPlanteBrut extends AbtractPROSessionProperties implements ISessionPropertiesPlanteBrut {

    DataType datatype = null;

    @Override
    public String getNomDeFichier(VersionFile version) {
        if (version == null) {
            return null;
        }
        final Dataset dataset = version.getDataset();
        if (RecorderPRO.getDispositifFromVersion(version) == null
                || RecorderPRO.getDatatypeFromVersion(version) == null
                || dataset.getDateDebutPeriode() == null || dataset.getDateFinPeriode() == null) {
            return StringUtils.EMPTY;
        }
        final StringBuffer nomFichier = new StringBuffer();
        try {
            nomFichier
                    .append(RecorderPRO.getDispositifFromVersion(version).getCodeforFileName())
                    .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
                    .append(IPhysicoChimiePlanteBrutDataTypeManager.CODE_DATATYPE_PLANTE_BRUT)
                    .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateDebutPeriode(), DateUtil.DD_MM_YYYY))
                    .append(AbstractSessionPropertiesPRO.CST_UNDERSCORE)
                    .append(DateUtil.getUTCDateTextFromLocalDateTime(dataset.getDateFinPeriode(), DateUtil.DD_MM_YYYY))
                    .append(".")
                    .append(ISessionPropertiesPlanteBrut.FORMAT_FILE);
        } catch (final Exception e) {
            return StringUtils.EMPTY;
        }

        return nomFichier.toString();
    }

    @Override
    public ITestDuplicates getTestDuplicates() {
        return new TestDuplicatePlanteBrut();
    }

    @Override
    public DataType getDataType() {
        return this.datatype;
    }

    @Override
    public void setDateDeFin(final LocalDateTime dateDeFin) {
        this.setDateDeFin(dateDeFin, ChronoUnit.DAYS, 1);
    }

    @Override
    public Produits getProduit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setEchantillonProduit(EchantillonsProduit echantillonsProduit) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setProduits(Produits produits) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDateTime getLocalDateDeDebut() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDateTime getLocalDateDeFin() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SortedMap<String, Boolean> getLocalDates() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void initLocalDate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalDateDeDebut(LocalDateTime LocalDateDeDebut) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalDateDeFin(LocalDateTime LocalDateDeFin) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalDateDeFin(LocalDateTime LocalDateDeFin, int fieldStep, int step) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLocalDates(SortedMap<String, Boolean> LocalDates) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void testLocalDates(BadsFormatsReport badsFormatsReport) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void testNonMissingLocalDates(BadsFormatsReport badsFormatsReport) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LocalDate getDateDebutTraitement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setDateDebutTraitement(LocalDate dateDebutTraitement) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
