/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.refdata.typeechantillon;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 *
 * @author adiankha
 */
public class JPATypeEchantillonDAO extends AbstractJPADAO<TypeEchantillon> implements ITypeEchantillon {

    /**
     *
     * @return
     */
    @Override
    public List<TypeEchantillon> getAll() {
        return getAllBy(TypeEchantillon.class, TypeEchantillon::getTe_code);
    }

    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<TypeEchantillon> getByNkey(String nom) {
        CriteriaQuery<TypeEchantillon> query = builder.createQuery(TypeEchantillon.class);
        Root<TypeEchantillon> methodeProcess = query.from(TypeEchantillon.class);
        query
                .select(methodeProcess)
                .where(
                        builder.equal(methodeProcess.get(TypeEchantillon_.te_nom), nom)
                );
        return getOptional(query);
    }

}
