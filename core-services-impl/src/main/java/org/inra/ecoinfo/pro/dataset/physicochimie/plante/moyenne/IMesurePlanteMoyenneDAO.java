/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.dataset.physicochimie.plante.moyenne;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.entity.MesurePlanteMoyenne;

/**
 *
 * @author adiankha
 */
public interface IMesurePlanteMoyenneDAO<T> extends IDAO<MesurePlanteMoyenne> {

    Optional<MesurePlanteMoyenne> getByKeys(String keymesure);

}
