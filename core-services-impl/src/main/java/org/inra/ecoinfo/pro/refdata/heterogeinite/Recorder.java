package org.inra.ecoinfo.pro.refdata.heterogeinite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public class Recorder extends AbstractCSVMetadataRecorder<Heterogeinite> {

    /**
     *
     */
    protected IHeterogeiniteDAO heteroDAO;
    private Properties heterogeiniteEN;

    private void createHeterogeinite(final Heterogeinite heterogeinite) throws BusinessException {
        try {
            heteroDAO.saveOrUpdate(heterogeinite);
            heteroDAO.flush();
        } catch (PersistenceException ex) {
            throw new BusinessException("can't create heterogeinite");
        }
    }

    private void updateHeterogeinite(final String nom, String code, final Heterogeinite dbheterogeinite) throws BusinessException {
        try {
            dbheterogeinite.setNom(nom);
            dbheterogeinite.setCode(code);
            heteroDAO.saveOrUpdate(dbheterogeinite);
        } catch (PersistenceException ex) {
            throw new BusinessException("can't update heterogeinite");
        }
    }

    private void createOrUpdateHeterogeinite(final String code, String nom, final Heterogeinite dbheterogeinite) throws BusinessException {
        if (dbheterogeinite == null) {
            final Heterogeinite heterogeinite = new Heterogeinite(nom);
            heterogeinite.setCode(code);
            heterogeinite.setNom(nom);
            createHeterogeinite(heterogeinite);
        } else {
            updateHeterogeinite(code, nom, dbheterogeinite);
        }
    }

    private void persistHeterogeinite(final String code, final String nom) throws BusinessException, BusinessException {
        final Heterogeinite dbhetero = heteroDAO.getByNKey(nom).orElse(null);
        createOrUpdateHeterogeinite(code, nom, dbhetero);
    }

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(Heterogeinite heterogeinite) throws BusinessException {
        final LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(heterogeinite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : heterogeinite.getNom(),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, true, false, true));

        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(heterogeinite == null || heterogeinite.getNom() == null
                        ? AbstractCSVMetadataRecorder.EMPTY_STRING : heterogeiniteEN.getProperty(heterogeinite.getNom()),
                        ColumnModelGridMetadata.NULL_VALUE_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        final ErrorsReport errorsReport = new ErrorsReport();
        try {
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String nom = tokenizerValues.nextToken();
                final Heterogeinite dbhetero = heteroDAO.getByNKey(nom).orElse(null);
                if (dbhetero != null) {
                    heteroDAO.remove(dbhetero);
                    values = parser.getLine();
                } else {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }

            }
        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        } catch (final PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected List<Heterogeinite> getAllElements() throws BusinessException {

        return heteroDAO.getAll();
    }

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            skipHeader(parser);
            String[] values = parser.getLine();
            while (values != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Heterogeinite.NAME_ENTITY_JPA);
                final String nom = tokenizerValues.nextToken();
                final String code = Utils.createCodeFromString(nom);
                persistHeterogeinite(code, nom);
                values = parser.getLine();
            }

        } catch (final IOException e) {
            throw new BusinessException(e.getMessage(), e);
        }

    }

    @Override
    protected ModelGridMetadata<Heterogeinite> initModelGridMetadata() {
        heterogeiniteEN = localizationManager.newProperties(Heterogeinite.NAME_ENTITY_JPA, Heterogeinite.JPA_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     *
     * @param heteroDAO
     */
    public void setHeteroDAO(IHeterogeiniteDAO heteroDAO) {
        this.heteroDAO = heteroDAO;
    }

    /**
     *
     * @return
     */
    public Properties getHeterogeiniteEN() {
        return heterogeiniteEN;
    }

    /**
     *
     * @param heterogeiniteEN
     */
    public void setHeterogeiniteEN(Properties heterogeiniteEN) {
        this.heterogeiniteEN = heterogeiniteEN;
    }

    /**
     *
     * @return
     */
    public IHeterogeiniteDAO getHeteroDAO() {
        return heteroDAO;
    }

}
