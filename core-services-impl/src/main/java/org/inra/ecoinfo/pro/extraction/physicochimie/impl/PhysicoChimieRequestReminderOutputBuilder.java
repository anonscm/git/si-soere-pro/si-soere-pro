/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteBrutDataTypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.plante.IPhysicoChimiePlanteMoyenneDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePRODatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.produits.physicochimie.IPhysicoChimiePROMoyDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolDatatypeManager;
import org.inra.ecoinfo.pro.dataset.physicochimie.sol.physicochimie.IPhysicoChimieSolMoyDatatypeManager;
import org.inra.ecoinfo.pro.refdata.dispositif.Dispositif;
import org.inra.ecoinfo.pro.utils.LoggerForExtraction;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author adiankha
 */
public class PhysicoChimieRequestReminderOutputBuilder extends AbstractOutputBuilder {

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_CODE = "physicoChimieRequestReminder";
    static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.physicochimie.jsf.physicochimie";
    static final String EXTRACTION_BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.pro.extraction.messages";

    static final String PROPERTY_MSG_EXTRACTION_COMMENTS = "PROPERTY_MSG_EXTRACTION_COMMENTS";

    static final String PROPERTY_MSG_EXTRACTION_DATE = "PROPERTY_MSG_EXTRACTION_DATE";

    static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";

    static final String PROPERTY_MSG_SELECTED_PERIODS = "PROPERTY_MSG_SELECTED_PERIODS";

    static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_PROBRUTBRUT = "PROPERTY_MSG_SELECTED_VARIABLES_PROBRUTBRUT";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_PRODUITMOYENNE = "PROPERTY_MSG_SELECTED_VARIABLES_PRODUITMOYENNE";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_SOLBRUT = "PROPERTY_MSG_SELECTED_VARIABLES_SOLBRUT";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_SOLMOYENNE = "PROPERTY_MSG_SELECTED_VARIABLES_SOLMOYENNE";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_PLANTEMOYENNE = "PROPERTY_MSG_SELECTED_VARIABLES_PLANTEMOYENNE";

    static final String PROPERTY_MSG_SELECTED_VARIABLES_PLANTEBRUT = "PROPERTY_MSG_SELECTED_VARIABLES_PLANTEBRUT";

    static final String PROPERTY_MSG_SELECTED_DISPOSITIFS = "PROPERTY_MSG_SELECTED_DISPOSITIFS";

    static final String PATTERN_STRING_SITES_SUMMARY = "   %s";

    static final String PATTERN_STRING_VARIABLES_SUMMARY = "   %s";

    static final String PATTERN_STRING_COMMENTS_SUMMARY = "   %s";

    static final String KEYMAP_COMMENTS = "comments";

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> map, Map<String, Object> requestMetadatasMap) throws BusinessException {
        final Map<String, File> reminderMap = new HashMap();
        final File reminderFile = this.buildOutputFile(AbstractOutputBuilder.FILENAME_REMINDER,
                AbstractOutputBuilder.EXTENSION_TXT);
        PrintStream reminderPrintStream;
        try {
            reminderPrintStream = new PrintStream(reminderFile, StandardCharsets.ISO_8859_1.displayName());
            reminderPrintStream.println(headers);
            reminderPrintStream.println();
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    PhysicoChimieRequestReminderOutputBuilder.EXTRACTION_BUNDLE_SOURCE_PATH,
                    PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_EXTRACTION_DATE));
            reminderPrintStream.println(DateUtil.getUTCDateTextFromLocalDateTime(LocalDateTime.now(), DateUtil.DD_MM_YYYY_HH_MM));
            reminderPrintStream.println();
            this.printDispositifSummary(
                    (List<Dispositif>) requestMetadatasMap.get(Dispositif.class.getSimpleName()),
                    reminderPrintStream);
            this.printVariablesSummary(
                    (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(IPhysicoChimiePRODatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO)),
                    (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(IPhysicoChimiePROMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_PRO_MOY)),
                    (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(IPhysicoChimiePlanteBrutDataTypeManager.CODE_DATATYPE_PLANTE_BRUT)),
                    (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(IPhysicoChimiePlanteMoyenneDatatypeManager.CODE_DATATYPE_PLANTE_MOYENNE)),
                    (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(IPhysicoChimieSolDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE)),
                    (List<Variable>) requestMetadatasMap.get(Variable.class.getSimpleName().concat(IPhysicoChimieSolMoyDatatypeManager.CODE_DATATYPE_PHYSICO_CHIMIE_MOY)),
                    reminderPrintStream);
            this.printDatesSummary((IntervalDate) requestMetadatasMap.get(IntervalDate.class.getSimpleName()), reminderPrintStream);
            reminderPrintStream.println(this.getLocalizationManager().getMessage(
                    PhysicoChimieRequestReminderOutputBuilder.EXTRACTION_BUNDLE_SOURCE_PATH,
                    PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_EXTRACTION_COMMENTS));
            reminderPrintStream
                    .println(String.format(
                            PhysicoChimieRequestReminderOutputBuilder.PATTERN_STRING_COMMENTS_SUMMARY,
                            requestMetadatasMap
                                    .get(PhysicoChimieRequestReminderOutputBuilder.KEYMAP_COMMENTS)));
        } catch (final FileNotFoundException | UnsupportedEncodingException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        reminderMap.put(AbstractOutputBuilder.FILENAME_REMINDER, reminderFile);
        reminderPrintStream.flush();
        reminderPrintStream.close();
        LoggerForExtraction.logRequest((Utilisateur) policyManager.getCurrentUser(), reminderFile);
        return reminderMap;
    }

    /**
     *
     * @param map
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> map) throws BusinessException {
        return this.getLocalizationManager().getMessage(
                PhysicoChimieRequestReminderOutputBuilder.EXTRACTION_BUNDLE_SOURCE_PATH,
                PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_HEADER);
    }

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        ((DefaultParameter) parameters).getFilesMaps().add(
                super.buildOutput(parameters,
                        PhysicoChimieRequestReminderOutputBuilder.CST_RESULT_EXTRACTION_CODE));
        return null;
    }

    void printDispositifSummary(final List<Dispositif> list, final PrintStream reminderPrintStream) {
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                PhysicoChimieRequestReminderOutputBuilder.EXTRACTION_BUNDLE_SOURCE_PATH,
                PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DISPOSITIFS));
        list.forEach((dispositif) -> {
            reminderPrintStream.println(String.format(
                    PhysicoChimieRequestReminderOutputBuilder.PATTERN_STRING_SITES_SUMMARY,
                    dispositif.getName()));
        });
        reminderPrintStream.println();
    }

    void printDatesSummary(final IntervalDate intervalDate,
            final PrintStream reminderPrintStream) throws BusinessException {
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                PhysicoChimieRequestReminderOutputBuilder.EXTRACTION_BUNDLE_SOURCE_PATH,
                PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_PERIODS));
        try {
            intervalDate.toLocalString(reminderPrintStream, localizationManager);
        } catch (final DateTimeParseException e) {
            AbstractOutputBuilder.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }

        reminderPrintStream.println();
    }

    private void printVariablesSummary(
            List<Variable> variableProduitBrut,
            List<Variable> variableProduitMoyenne,
            List<Variable> variablePlanteBrut,
            List<Variable> variablePlanteMoyenne,
            List<Variable> variableSolBrut,
            List<Variable> variableSolMoyenne,
            PrintStream reminderPrintStream) {
        final Properties propertiesVariableName = this.localizationManager.newProperties(
                Nodeable.getLocalisationEntite(Variable.class), Nodeable.ENTITE_COLUMN_NAME);
        String localizedName;
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                PhysicoChimieRequestReminderOutputBuilder.EXTRACTION_BUNDLE_SOURCE_PATH,
                PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES));
        printVariableSummary(variableProduitBrut, reminderPrintStream, propertiesVariableName, PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_PROBRUTBRUT);
        printVariableSummary(variableProduitMoyenne, reminderPrintStream, propertiesVariableName, PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_PRODUITMOYENNE);
        printVariableSummary(variablePlanteBrut, reminderPrintStream, propertiesVariableName, PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_PLANTEBRUT);
        printVariableSummary(variablePlanteMoyenne, reminderPrintStream, propertiesVariableName, PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_PLANTEMOYENNE);
        printVariableSummary(variableSolBrut, reminderPrintStream, propertiesVariableName, PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_SOLBRUT);
        printVariableSummary(variableSolMoyenne, reminderPrintStream, propertiesVariableName, PhysicoChimieRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES_SOLMOYENNE);

        reminderPrintStream.println();
    }

    /**
     *
     * @param variables
     * @param reminderPrintStream
     * @param propertiesVariableName
     * @param keyLocalizedVariableName
     */
    protected void printVariableSummary(
            List<Variable> variables,
            PrintStream reminderPrintStream,
            final Properties propertiesVariableName,
            String keyLocalizedVariableName
    ) {
        String localizedName;
        if (CollectionUtils.isEmpty(variables)) {
            return;
        }
        reminderPrintStream.println(this.getLocalizationManager().getMessage(
                PhysicoChimieRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, keyLocalizedVariableName
        ));
        for (final Variable variable : variables) {
            localizedName = propertiesVariableName.getProperty(variable.getName());
            reminderPrintStream.println(String.format(
                    PhysicoChimieRequestReminderOutputBuilder.PATTERN_STRING_VARIABLES_SUMMARY,
                    Strings.isNullOrEmpty(localizedName) ? variable.getName() : localizedName));
        }
    }

}
