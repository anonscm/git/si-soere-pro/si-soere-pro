/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.pro.extraction.physicochimie.produit.moyenne.impl;

import java.util.Map;
import org.inra.ecoinfo.pro.extraction.physicochimie.impl.AbstractPhysicoChimieParameter;

/**
 *
 * @author adiankha
 */
public class ProduitMoyenneParameters extends AbstractPhysicoChimieParameter {

    /**
     *
     */
    public static final String CODE_EXTRACTIONTYPE_PRODUITMOYENNE = "physico_chimie_des_pro_moyennes";

    /**
     *
     */
    public static final String PRODUITMOYENNE = "ProduitMoyennes";

    /**
     *
     * @param metadatasMap
     */
    public ProduitMoyenneParameters(Map<String, Object> metadatasMap) {
        super(metadatasMap);
    }

    @Override
    public String getExtractionTypeCode() {
        return CODE_EXTRACTIONTYPE_PRODUITMOYENNE;
    }
}
