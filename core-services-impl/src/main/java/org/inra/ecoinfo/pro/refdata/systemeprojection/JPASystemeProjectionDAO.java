package org.inra.ecoinfo.pro.refdata.systemeprojection;

import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;

/**
 * @author sophie
 *
 */
public class JPASystemeProjectionDAO extends AbstractJPADAO<SystemeProjection> implements ISystemeProjectionDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.pro.refdata.systemeprojection.ISystemeProjectionDAO# getByNom(java.lang.String)
     */
    /**
     *
     * @param nom
     * @return
     */
    @Override
    public Optional<SystemeProjection> getByNKey(String nom) {
        CriteriaQuery<SystemeProjection> query = builder.createQuery(SystemeProjection.class);
        Root<SystemeProjection> systemeProjection = query.from(SystemeProjection.class);
        query
                .select(systemeProjection)
                .where(
                        builder.equal(systemeProjection.get(SystemeProjection_.nom), nom)
                );
        return getOptional(query);
    }

    /* (non-Javadoc)
     * @see org.inra.ecoinfo.pro.refdata.systemeprojection.ISystemeProjectionDAO#getByCode(java.lang.String)
     */
    /**
     *
     * @param code
     * @return
     */
    @Override
    public Optional<SystemeProjection> getByCode(String code) {
        CriteriaQuery<SystemeProjection> query = builder.createQuery(SystemeProjection.class);
        Root<SystemeProjection> systemeProjection = query.from(SystemeProjection.class);
        query
                .select(systemeProjection)
                .where(
                        builder.equal(systemeProjection.get(SystemeProjection_.code), code)
                );
        return getOptional(query);
    }
    
    @Override
    public List<SystemeProjection> getAll() {
        return getAllBy(SystemeProjection.class, SystemeProjection::getCode);
    }
    
}
