/**
 *
 */
package org.inra.ecoinfo.pro.refdata.facteur;

import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.pro.refdata.typefacteur.TypeFacteur;

/**
 * @author sophie
 *
 */
public interface IFacteurDAO extends IDAO<Facteur> {

    /**
     *
     * @return
     */
    List<Facteur> getAll();

    /**
     *
     * @param libelle
     * @return
     */
    Optional<Facteur> getByNKey(String libelle);

    /**
     *
     * @param code
     * @return
     */
    Optional<Facteur> getByCodeFct(String code);

    /**
     *
     * @param isInfosApportFert
     * @return
     */
    List<Facteur> getLstFacteurByIsInfosApportFert(boolean isInfosApportFert);

    /**
     *
     * @param typeFacteur
     * @return
     */
    List<Facteur> getLstFacteurByTPFacteur(TypeFacteur typeFacteur);

    /**
     *
     * @param isAssocNomenclature
     * @return
     */
    List<Facteur> getLstFacteurByIsAssocNomenclature(boolean isAssocNomenclature);

}
